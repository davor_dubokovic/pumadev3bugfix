@isTest 
public class EP_CreditExceptionUpdateHandler_UT { 
    public static final String ACC_NAME = 'Account1';
    public static final String TEXT = 'Test';
    public static final String ERRORCD = '12345';
    static string jsonBody =  '{"MSG":{"HeaderCommon":{"InterfaceType":"SFDC_NAV","SourceCompany":"AAF","SourceResponseAddress":"https://cs31.salesforce.com:8443/services/apexrest/v2/AcknowledgementWS2","SourceUpdateStatusAddress":"https://cs31.salesforce.com:8443/services/apexrest/v2/AcknowledgementWS2","ContinueOnError":"true","ComprehensiveLogging":"true","TransportStatus":"New","UpdateSourceOnReceive":"false","UpdateSourceOnDelivery":"false","UpdateSourceAfterProcessing":"false","UpdateDestinationOnDelivery":"true","CallDestinationForProcessing":"true","ObjectType":"Table","ObjectName":"CreditException","CommunicationType":"Async","MsgID":"788d09f3-eba2-45ff-bbd1-cb29c44a7f57","SourceGroupCompany":"null","DestinationGroupCompany":"null","DestinationCompany":"null","CorrelationID":"null","DestinationAddress":"","DestinationUpdateStatusAddress":"","MiddlewareUrlForPush":"null","EmailNotification":"null","ErrorCode":"null","ErrorDescription":"null","ProcessStatus":"null","ProcessingErrorDescription":"ProcessingErrorDescription"},"Payload":{"any0":{"creditExceptionStatus":{"status":[{"seqId":"seqId-000362","identifier":{"exceptionNo":"CER-000026","billTo":"00006683","clientId":"PA22"},"approvalStatus":"Approved","reasonForRejection":"XXX","approverId":"Tester.Te","modifiedDt":"2017-12-21T12:45:00","modifiedBy":"Tester.Test","versionNr":"20171219T234609.000"}]}}}}}';

    static testMethod void processRequest_Positivetest() {
        EP_CreditExceptionUpdateHandler localObj = new EP_CreditExceptionUpdateHandler();
        EP_Credit_Exception_Request__c creditExp = EP_TestDataUtility.createCreditExceptionRequest();
        Test.startTest();
        	String result = localObj.processRequest(jsonBody);
        Test.stopTest();
        
        System.AssertNotEquals(Null,result);
    }
    
    static testMethod void processRequest_NegativeTest() {
        EP_CreditExceptionUpdateHandler localObj = new EP_CreditExceptionUpdateHandler();
        EP_CreditExceptionUpdateStub creditExceptionStub = new EP_CreditExceptionUpdateStub();
        String jsonBody = System.JSON.serialize(creditExceptionStub );

        Test.startTest();
        	String result = localObj.processRequest(jsonBody);
        Test.stopTest();
        
		System.AssertEquals(true,EP_CreditExceptionUpdateHandler.ackResponseList.size() > 0);
    }
    static testMethod void processCreditExceptions_PositiveTest() {
        EP_CreditExceptionUpdateStub stub = (EP_CreditExceptionUpdateStub )  System.JSON.deserialize(jsonBody, EP_CreditExceptionUpdateStub.class);
        EP_Credit_Exception_Request__c creditExp = EP_TestDataUtility.createCreditExceptionRequest();
        for(EP_CreditExceptionUpdateStub.CreditExceptionStatusType stubs : stub.MSG.Payload.any0.creditExceptionStatus.status) {
            stubs.failureReason = null;
            stubs.creditExcpObj = creditExp;
        }
        
        Test.startTest();
       		EP_CreditExceptionUpdateHandler.processCreditExceptions(stub.MSG.Payload.any0.creditExceptionStatus.status);
        Test.stopTest();
        
        System.AssertEquals(true,EP_CreditExceptionUpdateHandler.ackResponseList.size() > 0);
    }
    static testMethod void processCreditExceptions_NegativeTest() {
        EP_CreditExceptionUpdateStub stub = (EP_CreditExceptionUpdateStub )  System.JSON.deserialize(jsonBody, EP_CreditExceptionUpdateStub.class);
        EP_Credit_Exception_Request__c creditExp = EP_TestDataUtility.createCreditExceptionRequest();
        for(EP_CreditExceptionUpdateStub.CreditExceptionStatusType stubs : stub.MSG.Payload.any0.creditExceptionStatus.status) {
            stubs.failureReason = 'Fail';
            stubs.creditExcpObj = creditExp;
        }
        
        Test.startTest();
        	EP_CreditExceptionUpdateHandler.processCreditExceptions(stub.MSG.Payload.any0.creditExceptionStatus.status);
        Test.stopTest();
        
        System.AssertEquals(true,EP_CreditExceptionUpdateHandler.ackResponseList.size() > 0);
    }
    static testMethod void createResponse_test() {
        EP_CreditExceptionUpdateStub creditExceptionStub = new EP_CreditExceptionUpdateStub();
        EP_CreditExceptionUpdateStub.CreditExceptionStatusType inboundStatus = new EP_CreditExceptionUpdateStub.CreditExceptionStatusType();
        String seqId = inboundStatus.seqId;
        List<Account> listAccounts = new List<Account>{new Account(Name=ACC_NAME),new Account()};
            Database.SaveResult[] srList = Database.insert(listAccounts, false);
        String errorDescription = 'errorDescription';//L4# 78534 - Test Class Fix
        String errorCode = ERRORCD;
        
        Test.startTest();
        	EP_CreditExceptionUpdateHandler.createResponse(seqId,errorCode,errorDescription);
        Test.stopTest();
        
        System.AssertEquals(true,EP_CreditExceptionUpdateHandler.ackResponseList.size() > 0);
    }
   
    static testMethod void updateCreditException_PositivetTest() {
        EP_Credit_Exception_Request__c creditExp = EP_TestDataUtility.createCreditExceptionRequest();
        LIST<order> sObjectsList = new List<SObject>{creditExp};
        Map<ID,String> credExId_SeqIdMap = new Map<ID,String>();
        credExId_SeqIdMap.put(creditExp.id, 'SEQ-123');
        
        Test.startTest();
	        creditExp.EP_Status__c = EP_Common_Constant.CREDIT_EXC_STATUS_APPROVED;
	        EP_CreditExceptionUpdateHandler.updateCreditException(sObjectsList,credExId_SeqIdMap);
        Test.stopTest();
        
        EP_Credit_Exception_Request__c creditExpUpdate = [select Id,EP_Status__c from  EP_Credit_Exception_Request__c where Id =:creditExp.Id limit 1 ];
        System.AssertEquals(creditExp.EP_Status__c,creditExpUpdate.EP_Status__c);
       
    }
    
    static testMethod void processUpsertErrors_test() {
        List<Database.Error> errorList = new List<Database.Error>();
        List<EP_Credit_Exception_Request__c> listSObject = new List<EP_Credit_Exception_Request__c>{new EP_Credit_Exception_Request__c()};
        Database.SaveResult[] saveResult = Database.insert(listSObject, false);
        
        Test.startTest();
	        for( integer index = 0 ; index < saveResult.size() ; index++ ) {
	            if(!saveResult[index].isSuccess()){
	                EP_CreditExceptionUpdateHandler.processUpsertErrors(saveResult[index].getErrors(), 'SEQ-123');
	            }
	        }
        Test.stopTest();
        
        System.AssertEquals(true,EP_CreditExceptionUpdateHandler.ackResponseList.size() > 0);
    }
}