/* 
  @Author <Accenture>
   @name <EP_OutboundIntegrationService>
   @CreateDate <18/01/2016>
   @Description <This is a abstract class of Integration Service that implements EP_IntegrationService > 
   @Novasuite Fix -- hardcoding removed 
   @Version <1.0>
*/
public class EP_OutboundIntegrationService {
    private static final string REQUEST_TIMEOUT = 'Request TimeOut';
    private String endPoint;
    private String intSystem;
    private String reqMethod;
    private String requestXML;
    
    Public static Final string REQUESTXMLMISSING = 'Cannot instantiate integration service for system because Require XML is missing';
    Public static Final string REQUESTMETHODMISSING = 'Cannot instantiate integration service for system because Requested Method is missing';
    Public static Final string ENDPOINTMISSING = 'Cannot instantiate integration service for system because End Point is missing';
    private string subscriptionKey;//Changes for #60147
    /**
     * @name <setupRequest >
     * @date  <18/01/2016>
     * @description <Method to setup the data which is require for webservice callout>
     * @param < String endPoint,String reqMethod, String requestXML>
     * @return NA
     //Changes for #60147
    */ 
    public  EP_OutboundIntegrationService (final String endPoint, final String reqMethod, final String requestXML, string subscriptionKey) {
        EP_GeneralUtility.Log('Public','EP_OutboundIntegrationService','EP_OutboundIntegrationService');
        checkBasicDataSetup(endPoint,reqMethod, requestXML);
        this.endPoint = endPoint;
        this.reqMethod = reqMethod;
        this.requestXML = requestXML;
        this.subscriptionKey = subscriptionKey;//Changes for #60147
    }
    /**
     * @name <getEndpoint >
     * @date  <18/01/2016>
     * @description <method to get End Point>
     * @param NA
     * @return String: Endpoint
    */
    public String getEndpoint() {
        EP_GeneralUtility.Log('Public','EP_OutboundIntegrationService','getEndpoint');
        return this.endPoint;
    }
     
    /**
     * @name <getRequestMethod >
     * @date  <18/01/2016>
     * @description <method to get Request Method>
     * @param NA
     * @return String: RequestMethod
    */
    public String getRequestMethod() {
        EP_GeneralUtility.Log('Public','EP_OutboundIntegrationService','getRequestMethod');
        return this.reqMethod;
    }
     
    /**
     * @name <getRequestXML >
     * @date  <18/01/2016>
     * @description <method to get RequestXML>
     * @param NA
     * @return String: RequestXML
    */
    public String getRequestXML() {
        EP_GeneralUtility.Log('Public','EP_OutboundIntegrationService','getRequestXML');
        return this.requestXML;
    }
       
    /**
     * @name <setEndpoint >
     * @date  <18/01/2016>
     * @description <method to set End Point>
     * @param <String endPoint>
     * @return NA
    */ 
    public void setEndpoint(final String endPoint) {
        EP_GeneralUtility.Log('Public','EP_OutboundIntegrationService','setEndpoint');
        this.endPoint = endPoint;
    }
 
    /**
     * @name <setRequestMethod >
     * @date  <18/01/2016>
     * @description <method to set End Point>
     * @param <String reqMethod>
     * @return NA
    */ 
    public void setRequestMethod(final String reqMethod) {
        EP_GeneralUtility.Log('Public','EP_OutboundIntegrationService','setRequestMethod');
        this.reqMethod = reqMethod;
    }
     
    /**
     * @name <setRequestXML >
     * @date  <18/01/2016>
     * @description <method to get RequestXML>
     * @param <String requestXML>
     * @return NA
    */ 
    public void setRequestXML(final String requestXML) {
        EP_GeneralUtility.Log('Public','EP_OutboundIntegrationService','setRequestXML');
        this.requestXML = requestXML;
    }
     
    /**
     * @name <EP_IntegrationServiceResult >
     * @date  <18/01/2016>
     * @description <Method is call synchronous HTTP request>
     * @param NA
     * @return NA
    */ 
    public EP_IntegrationServiceResult invokeRequest(){
        EP_GeneralUtility.Log('Public','EP_OutboundIntegrationService','invokeRequest');
        HTTPResponse res;
        EP_IntegrationServiceResult intResult = new EP_IntegrationServiceResult();
        HTTPRequest req = new HTTPRequest();
        Http http = new Http();
        //Changes for #60147
        try{
            checkBasicDataSetup(this.endPoint,this.reqMethod, this.requestXML);
            system.debug('--requestXML--'+requestXML);
            //SET ENDPOINT
            req.setEndPoint(endpoint);
            //SET subscriptionKey for API Managemnet URLs
            setSubscriptionKey(req, this.subscriptionKey);
            //Set Content Type as XML
            req.setHeader(EP_Common_Constant.CONTENTTYPE, EP_Common_Constant.CONTENTTYPE_APPLICATION_XML); 
            //SET REQUEST METHOD
            req.setMethod(reqMethod);
            //SET MAX TIMEOUT i.e. 120000 ms
            req.setTimeOut(Integer.valueOf(EP_INTEGRATION_CUSTOM_SETTING__c.getValues(REQUEST_TIMEOUT).EP_Value__c));
            //SET REQUEST BODY
            req.setBody(requestXML);
            //Set Client Certificate Name
            setClientCertificate(req);
           
            res =  http.send(req);
            //throw new EP_IntegrationException(EP_IntegrationException.EP_IntegrationErrorCode.CALLOUT_EXCEPTION,
                //'Exception in invokeRequest. Erorr:');
        }catch(Exception handledException){
            res = intResult.createErrorResponse(500, handledException.getMessage());
            system.debug('**Exception in Outbound Service**' + handledException);
            //TODO : Move messages into Custom Label
            //throw new EP_IntegrationException(EP_IntegrationException.EP_IntegrationErrorCode.CALLOUT_EXCEPTION,
                //'Exception in invokeRequest. Erorr:'+handledException.getMessage(),handledException);
            //EP_loggingService.loghandledException(handledException, EP_Common_Constant.EPUMA, 'invokeRequest', 'EP_OutboundIntegrationService',apexPages.severity.ERROR);
        }
        intResult.setResponse(res);
        system.debug('---Response---'+res);
        return intResult;                              
    }
    
    /** 
    * @Author       Accenture
    * @Name         setSubscriptionKey
    * @Date         06/28/2017
    * @Description  This method will be used to set setSubscription Key in Request Header which is define in EP_CS_OutboundMessageSetting__c custom setting for require interfaces
    * @Param        HTTPRequest, string
    * @return       NA
    */
    @TestVisible
    private void setSubscriptionKey( HTTPRequest req, string subscriptionKey) {
        if(string.isNotBlank(subscriptionKey)) {        
            req.setHeader(EP_Common_Constant.STR_API_MGMT_SUBSCRIPTION_KEY,subscriptionKey);
        }
    }
    
    /**
    * @Author       Accenture
    * @Name         setClientCertificate
    * @Date         06/28/2017
    * @Description  This method will be used to set client Certificate which is comman for all interfaces
    * @Param        HTTPRequest
    * @return       NA
    */
    @TestVisible
    private void setClientCertificate(HTTPRequest req) {
        EP_INTEGRATION_CUSTOM_SETTING__c mutualAuthObj = EP_INTEGRATION_CUSTOM_SETTING__c.getValues( EP_Common_Constant.STR_ENABLE_MUTUAL_AUTH );
        if( mutualAuthObj != null && mutualAuthObj.EP_Value__c.equalsIgnoreCase(EP_Common_Constant.STR_ENABLE_MUTUAL_AUTH_YES) ){
            req.setClientCertificateName( EP_Common_Constant.STR_SALESFORCE_OUTBOUND_CERT_NAME);
        }
    }
    /**
    * @Author       Accenture
    * @Name         processResult
    * @Date         02/07/2017
    * @Description  This method will be used to process the response of the request which was sent 
    * @Param        EP_IntegrationServiceResult, set<id>, Integer
    * @return       NA
    */
    public void processResult(EP_IntegrationServiceResult results, set<id> integrationRecords,Integer attempts, string messageType){
        EP_GeneralUtility.Log('Public','EP_OutboundIntegrationService','processResult');
        EP_IntegrationRecordMapper integrationRecordMapper = new EP_IntegrationRecordMapper();
        //Changes for #60147
        if(results != null){
            list<EP_IntegrationRecord__c> intRecordList =  integrationRecordMapper.getParentsRecords(integrationRecords);
            logResponse(this.requestXML, intRecordList,results, this.endpoint, attempts, messageType);
            processResponse(results,messageType, intRecordList);
            if(!intRecordList.isEmpty()) {
                database.update(intRecordList);
            }   
        }
    }
    
    /**
    * @Author       Accenture
    * @Name         processResponse
    * @Date         06/28/2017
    * @Description  This method is used to Handled Response for synchronous Outbound Request
    * @Param        EP_IntegrationServiceResult, String, list
    * @return       NA
    */
    @TestVisible
    private void processResponse(EP_IntegrationServiceResult results, string messageType, list<EP_IntegrationRecord__c> integrationRecords) {
        string responseBody = results.getResponse().getBody();
        system.debug('***responseBody**' + responseBody);
        if(!string.isEmpty(responseBody)) {
            EP_InboundFactory inboundFactory = new EP_InboundFactory();
            EP_InboundHandler responseHandler = inboundFactory.getResponseHandler(messageType);         
            if(responseHandler != null) {
                string processResult = responseHandler.processRequest(responseBody, integrationRecords.get(0).EP_Object_Record_Name__c);
                
                for(EP_IntegrationRecord__c intRecord : integrationRecords) {
                    intRecord.EP_Process_Result__c = processResult;
                    intRecord.EP_Response_Message__c = responseBody;
                    intRecord.EP_Status__c = EP_Common_Constant.SYNC_STATUS;
                    if(processResult.startsWith(EP_Common_Constant.FAILURE)) {
                        intRecord.EP_Status__c = EP_Common_Constant.ERROR_SYNC_STATUS;
                    }
                }
            }
        }
    }
    
    /**
    * @Author       Accenture
    * @Name         checkBasicDataSetup
    * @Date         02/07/2017
    * @Description  This method is used to check and ensure all validate data setup are done for web service callouts 
    * @Param        String, String, String
    * @return       NA
    */
    @TestVisible
    private static void checkBasicDataSetup(final String endPoint,final String reqMethod, final String requestXML) {
        EP_GeneralUtility.Log('Private','EP_OutboundIntegrationService','checkBasicDataSetup');
        //TODO : Move messages into Custom Label
        if(string.isEmpty(endPoint)) {
            throw new EP_IntegrationException(EP_IntegrationException.EP_IntegrationErrorCode.CANNOT_CREATE_INTEGRATION_SERVICE,
                ENDPOINTMISSING);
        }
        if(string.isEmpty(reqMethod)) {
            throw new EP_IntegrationException(EP_IntegrationException.EP_IntegrationErrorCode.CANNOT_CREATE_INTEGRATION_SERVICE,
                REQUESTMETHODMISSING);
        }
        if(string.isEmpty(requestXML)) {
            throw new EP_IntegrationException(EP_IntegrationException.EP_IntegrationErrorCode.CANNOT_CREATE_INTEGRATION_SERVICE,
                REQUESTXMLMISSING);
        }
    }
    
    /**
    * @Author : Accenture
    * @Name : logResponse
    * @Date : 02/07/2017
    * @Description : Method to log outbound message Response on integration Records.
    * @Param : Id , set<id>, string, EP_IntegrationServiceResult, string, string
    * @return :
    */        
    @TestVisible
    private static void logResponse(string xmlSting, list<EP_IntegrationRecord__c> integrationRecords, EP_IntegrationServiceResult results, string endPointUrl, Integer attempts, string messageType) {
        EP_GeneralUtility.Log('Public','EP_OutboundIntegrationService','logResponse');
        try {
            integer statusCode = results.getResponse().getStatusCode();
            EP_CS_OutboundMessageSetting__c  msgSetting = EP_CustomSettingsUtil.getOutboundMessageSetting(messageType);
            //Code Change for ATR - #59186
            for(EP_IntegrationRecord__c intRecord : integrationRecords) {
                intRecord.EP_XML_Message__c = xmlSting;
                intRecord.EP_Endpoint_URL__c = endPointUrl;
                intRecord.EP_Message_Type__c = messageType;
                //Update the error description to blank as it might get different error in retry logic, also update the status to SENT
                intRecord.EP_Error_Description__c = EP_Common_Constant.BLANK;
                intRecord.EP_Status__c = EP_Common_Constant.SENT_STATUS;
                intRecord.EP_Error_Code__c = string.valueOf(statusCode);
                if(!isStatusCodeValid(string.valueOf(statusCode),msgSetting.EP_Success_Status_Codes__c)) {
                    intRecord.EP_Status__c = EP_Common_Constant.ERROR_SENT_STATUS;
                    intRecord.EP_Error_Description__c = results.getResponse().toString();
                }
                
                setATRFields(intRecord,msgSetting,attempts);
                //Code Change for ATR - #59186 End
            }
        } catch(exception exp ) {
            throw new EP_IntegrationException(EP_IntegrationException.EP_IntegrationErrorCode.CANNOT_INSERT_UPDATE_RECORD, exp.getMessage());
        }
    }
    
    /** Code Change for ATR - #59186
    * @Author       Accenture
    * @Name         setATRFields
    * @Date         02/07/2017
    * @Description  This method is used to populate ATR related fields 
    * @Param        EP_IntegrationRecord__c, EP_CS_OutboundMessageSetting__c, Integer
    * @return       NA
    */
    @TestVisible
    private static void setATRFields(EP_IntegrationRecord__c intRecord, EP_CS_OutboundMessageSetting__c  msgSetting, Integer attempts){
        intRecord.EP_Queuing_Retry__c = false;
        intRecord.EP_Attempt__c = attempts == null ? ((intRecord.EP_Attempt__c==null ? 0 : intRecord.EP_Attempt__c) + 1) : attempts;
        if(isEligibleForATR(intRecord, msgSetting)) {
            intRecord.EP_Last_Run__c= System.now();         
            DateTime dtNextSchedule = system.now().addMinutes(msgSetting.EP_Earliest_Start_Time__c==null ? 2 : Integer.ValueOf(msgSetting.EP_Earliest_Start_Time__c));
            integer day = integer.valueOf(dtNextSchedule.day());
            integer month = integer.valueOf(dtNextSchedule.month());
            integer hour = integer.valueOf(dtNextSchedule.hour());
            integer minute = integer.valueOf(dtNextSchedule.minute());
            integer second = 0;
            integer year = integer.valueOf(dtNextSchedule.year());
            DateTime nextRunDateTime = DateTime.newInstance(year, month, day, hour, minute, second);
            intRecord.EP_Next_Run__c = nextRunDateTime;
            intRecord.EP_Queuing_Retry__c = true;
        } 
    }
    
    /** Code Change for ATR - #59186
    * @Author       Accenture
    * @Name         isEligibleForATR
    * @Date         14/07/2017
    * @Description  This method is used to check if this record is Eligible For ATR
    * @Param        EP_IntegrationRecord__c, EP_CS_OutboundMessageSetting__c
    * @return       boolean
    */
    @TestVisible
    private static boolean isEligibleForATR(EP_IntegrationRecord__c intRecord, EP_CS_OutboundMessageSetting__c  msgSetting) {
        //Do Retry only if following condition are match
        //  1. If Queuing Retry is enabled for given interface
        //  2. If HTTP stauts code is matched with the Codes which are valid for ATR
        EP_CS_Communication_Settings__c ATRsettings = EP_CS_Communication_Settings__c.getValues(EP_Common_Constant.DISABLE_ATR);
        if(msgSetting.EP_Queuing_Retry__c == false || !isStatusCodeValid(intRecord.EP_Error_Code__c,msgSetting.EP_ATR_Error_Codes__c)){
            return false;
        } else {
            return true;
        }
    } 
    
    /** Code Change for ATR - #59186
    * @Author       Accenture
    * @Name         isStatusCodeValid
    * @Date         14/07/2017
    * @Description  This method is used to check if given stauts code is valid and exists for Success and Failure of integration
    * @Param        string, string
    * @return       boolean
    */
    public static boolean isStatusCodeValid(string statusCode, string statusCodeStr) {
        set<String> statusCodeSet = new set<String>();
        for(string statusCodeVal : statusCodeStr.split(EP_Common_Constant.SEMICOLON)) {
            statusCodeSet.add(statusCodeVal);
        }
        
        if(statusCodeSet.contains(statusCode)) {
            return true;
        } else {
            return false;
        }
    }
}