/**
* @author <Accenture>
* @Class name EP_SellToASRejected
*/
public class EP_SellToASRejected extends EP_AccountState{
    /***NOvasuite fix constructor removed**/

    /**
    * @author <Accenture>
    * @name setAccountDomainObject
    * @Param EP_AccountDomainObject 
    */
    public override void setAccountDomainObject(EP_AccountDomainObject currentAccount)
    {
        super.setAccountDomainObject(currentAccount);
    }
    /**
    * @author <Accenture>
    * @name doOnEntry
    */
    public override void doOnEntry(){
        //L4_45352_start
        EP_GeneralUtility.Log('Public','EP_SellToASRejected','doOnEntry');
        EP_AccountService service = new EP_AccountService(this.account);
        //Chaining Fix Start       
        if(!EP_IntegrationUtil.ISERRORSYNC){
            //WP2-Pricing Engine Callout changes 
            if(!this.account.localaccount.EP_Synced_PE__c){
                system.debug('-Calling--PE--');
                service.doActionSyncCustomerToPricingEngine();
            }
            //WP2-Pricing Engine Callout changes 
            else if(!this.account.localaccount.EP_Synced_PE__c ){
                service.doActionSendCreateRequestToNav();
            }
            else if(this.account.localaccount.EP_Synced_NAV__c && !this.account.localaccount.EP_Synced_WinDMS__C){
                service.doActionSendCreateRequestToWinDMS();
            }
        }
        //Chaining Fix End 
        //L4_45352_end
    }  
    /**
    * @author <Accenture>
    * @name doOnExit
    */
    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_SellToASRejected','doOnExit');
        
    }

    /**
    * @author <Accenture>
    * @name doTransition
    * @return boolean
    */
    public override boolean doTransition(){
        EP_GeneralUtility.Log('Public','EP_SellToASRejected','doTransition');
        return super.doTransition();
    }
    /**
    * @author <Accenture>
    * @name isInboundTransitionPossible
    * @return boolean
    */
    public override boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_SellToASRejected','isInboundTransitionPossible');
        return super.isInboundTransitionPossible();
    }
}