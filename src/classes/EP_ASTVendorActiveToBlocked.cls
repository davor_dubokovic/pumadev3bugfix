/*
 *  @Author <Accenture>
 *  @Name <EP_ASTVendorActiveToBlocked>
 *  @CreateDate <6/2/2017>
 *  @Description <Handles Vendor Account status change from Active to Blocked>
 *  @Version <1.0>
 */
 public with sharing class EP_ASTVendorActiveToBlocked  extends EP_AccountStateTransition{
    public EP_ASTVendorActiveToBlocked() {
        finalState = EP_AccountConstant.Blocked;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTVendorActiveToBlocked','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTVendorActiveToBlocked','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTVendorActiveToBlocked','isGuardCondition');
              
        return true;
    }
    
    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_ASTVendorActiveToBlocked','doOnExit');

    }
}