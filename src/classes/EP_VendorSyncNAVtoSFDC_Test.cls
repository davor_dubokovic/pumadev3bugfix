/* 
  @Author <Vibhor Goel>
   @name <EP_PurchaseContractSyncNAVtoSFDC_Test>
   @CreateDate <14/11/2016>
   @Description  <This is apex test class for EP_VendorSyncNAVtoSFDC and EP_VendorSyncHandler> 
   @Version <1.0>
*/
@isTest
private class EP_VendorSyncNAVtoSFDC_Test {
    
    private static final string COMPANY_CODE1 = EP_TestDataUtility.COMPANY_CODE;
    
    private static final string PRODUCTCODE1 = 'CODE1';
    private static final string PRODUCTNAME1 = 'Name1';
    private static final string PRODUCTFAMILY1 = 'Family1';
    private static final string DESCRIPTION1 = 'Test Description 1';
    
    private static final string SEQID1 = '112345';
    private static final string LINESEQID1 = '1123456';
    
    private static final string SUPPLIER_NAVID = 'NAVS123';
    private static final string VENDORTYPE = '3rd Party Stock Supplier';
    
    private static final string PAYMENTTERM_CODE = 'COD';
    private static final string PAYMENTTERM_NAME = 'COD';
    
    private static final string CONTRACT_NAVID = 'NAVC123';
    private static final string LINENAV_NAVID = 'NAVL123'; 
    
    private static final string STOARGE_LOCATIO_NAVID = 'NAVS123456';
    
    private static String COUNTRY_NAME = 'Australia';
    private static String COUNTRY_CODE = 'AU';
    private static String COUNTRY_REGION = 'Australia';
    
    private static Company__c company1;
    private static Product2  productObj1;
    private static Account supplier;
    private static Account storageLocation;
    private static EP_Payment_Term__c paymentTerm;
    
    private static String uom;
    private static string VENDORS_STR = 'vendors';
    private static final string SFDCBASEURL = URL.getSalesforceBaseUrl().toExternalForm();
    private static final string VENDORSYNCENDPOINT = '/services/apexrest/v1/VendorSync/*';
    //private static final String CONTRACT_JSON_NODE = EP_Common_Constant.LEFT_CURLY_BRACE + EP_Common_Constant.DOUBLE_QUOTES_STRING + EP_PurchaseContractSyncHandler.PURCHASE_CONTRACT_JSONOBJECT + EP_Common_Constant.DOUBLE_QUOTES_STRING + ( EP_Common_Constant.COLON_WITH_SPACE.trim() );
    private static final String VENDOR_JSON_NODE = EP_Common_Constant.LEFT_CURLY_BRACE + EP_Common_Constant.DOUBLE_QUOTES_STRING + VENDORS_STR   + EP_Common_Constant.DOUBLE_QUOTES_STRING + ( EP_Common_Constant.COLON_WITH_SPACE.trim() );

    
    /*
    Method to create data for tesing
    of object storage location object
    */
    static void setupStorageLocationData()
    {
        BusinessHours bhrs= [Select Name from BusinessHours where IsActive =true AND IsDefault =true LIMIT 1];
        
        EP_Country__c country1 = EP_TestDataUtility.createCountryRecord( COUNTRY_NAME, COUNTRY_CODE, COUNTRY_REGION);
        Database.insert(country1);
        
        storageLocation = EP_TestDataUtility.createStorageLocAccount(country1.Id,bhrs.Id);
        storageLocation.EP_Nav_Stock_Location_Id__c = STOARGE_LOCATIO_NAVID;
        insert storageLocation;
        system.AssertNotEquals( null, storageLocation.Id );
        storageLocation.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update storageLocation;
        storageLocation.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update storageLocation;
    }
    
    /*
    Method to create data for tesing
    of object supplier object
    */
    static void setupSupplierData()
    {
        supplier = EP_TestDataUtility.createTestVendor( VENDORTYPE, SUPPLIER_NAVID );
        supplier.Name = 'Supplier';
        Database.insert(supplier);
        system.AssertNotEquals( null, supplier.Id );
    }
    
    /*
    Method to create data for tesing
    of object Company__c
    */
    static void setupCompanyData()
    {
        try{
            company1 = [Select Id, EP_Company_Code__c from Company__c limit 1 ];
        }catch( exception e )
        {
            company1 = EP_TestDataUtility.createCompany( COMPANY_CODE1 );
            Database.insert(company1);
        }
        system.AssertNotEquals( null, company1.Id );
        
    }
    
    /*
    Method to create data for tesing
    of object Product2
    */
    static void setupProductData()
    {
        String uniqueKey1 = PRODUCTCODE1 + EP_Common_Constant.STRING_HYPHEN + COMPANY_CODE1;
        productObj1 = new product2(Name = PRODUCTNAME1, EP_NAV_Product_Company__c = uniqueKey1, EP_Company_Lookup__c = company1.Id, CurrencyIsoCode = EP_Common_Constant.GBP, family = PRODUCTFAMILY1, ProductCode = PRODUCTCODE1, EP_Unit_of_Measure__c = uom);
        Database.insert(productObj1);
        system.AssertNotEquals( null, productObj1.Id );
        
    }
    
    /*
    Method to create data for tesing
    of object peyment term
    */
    static void setupPaymentTermData()
    {
        paymentTerm = new EP_Payment_Term__c(Name = PAYMENTTERM_NAME, EP_Payment_Term_Code__c = PAYMENTTERM_CODE);
        Database.insert(paymentTerm);
        system.AssertNotEquals( null, paymentTerm.Id );
        
    }
    
    private static Profile sysAdmin = [Select id from Profile
                                    Where Name =: EP_Common_Constant.ADMIN_PROFILE  
                                    limit :EP_Common_Constant.ONE];
    private static User adminUser = EP_TestDataUtility.createUser(sysAdmin.id);
    
    
    /*
    Method to test Vendor Creation
    */
    static testMethod void createUpdateVendorTest()  
    {
        Schema.DescribeFieldResult fieldResult = Product2.EP_Unit_of_Measure__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        if( !ple.isEmpty() )
        {
            uom = ple[0].getValue();
        }
        else
        {
            uom = EP_Common_Constant.UOM_STRING;
        }
        
        //insert storage location
        setupStorageLocationData();
        //insert supplier
        setupSupplierData();
        //insert company
        setupCompanyData();
        //insert product
        setupProductData();
        //insert payment term
        setupPaymentTermData();
        
        
        EP_VendorSyncHandler.WrapperIdentifier wrapperIdentLine = new EP_VendorSyncHandler.WrapperIdentifier();
        wrapperIdentLine.vendorId = SUPPLIER_NAVID;
        wrapperIdentLine.clientId = COMPANY_CODE1;
        
        
        EP_VendorSyncHandler.WrapperVendor wrapperLine = new EP_VendorSyncHandler.WrapperVendor();
        wrapperLine.seqId = LINESEQID1;
        wrapperLine.identifier  = wrapperIdentLine;
        wrapperLine.name = 'TestName1';
        wrapperLine.name2 = 'Test Name 2';
        wrapperLine.address = 'TestStreet1';
        wrapperLine.address2= 'TestStreet2';
        wrapperLine.city = 'TestCity';
        wrapperLine.postcode = '110059';
        wrapperLine.cntryCode = COUNTRY_CODE;
        /*TFS fix 45559 start added changes for vendor type field*/
        list<EP_VendorSyncHandler.WrapperVendorType> lstwraVendorType = new list<EP_VendorSyncHandler.WrapperVendorType>();
        EP_VendorSyncHandler.WrapperVendorType wraVendorType = new EP_VendorSyncHandler.WrapperVendorType();
        wraVendorType.vendorTyp = EP_VendorSyncHandler.VENDOR_TYPE_SFDC_TRSN;
        lstwraVendorType.add(wraVendorType);
        wrapperLine.vendorTypes = lstwraVendorType;
        /*TFS fix 45559 end*/
        wrapperLine.blocked = EP_Common_Constant.STRING_NO_LS;
        wrapperLine.paymentTerm = PAYMENTTERM_CODE;
        wrapperLine.currencyId = 'GBP';
        wrapperLine.transportMgmt = 'N/A';
        wrapperLine.duties = 'Excise Paid';
        
        EP_VendorSyncHandler.WrapperVendors WrapperVendors = new EP_VendorSyncHandler.WrapperVendors ();
        WrapperVendors.vendor = new list<EP_VendorSyncHandler.WrapperVendor>();
        WrapperVendors.vendor.add(wrapperLine);
        
        EP_VendorSyncHandler.WrapperVendorCls WrapperVendor = new EP_VendorSyncHandler.WrapperVendorCls();
        WrapperVendor.vendor = wrapperLine ;
        
        String requestBody = JSON.serialize(WrapperVendor); 
        requestBody = VENDOR_JSON_NODE  + requestBody + EP_Common_Constant.RIGHT_CURLY_BRACE;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        res.statusCode = Integer.valueOf( EP_Common_Constant.CODE_200 );  
        
        req.requestURI = SFDCBASEURL + VENDORSYNCENDPOINT;
        req.httpMethod = EP_Common_Constant.POST;
        req.requestBody = Blob.valueOf( requestBody );
        
        req.addHeader( EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON );
        RestContext.request = req;
        RestContext.response = res;
        
        test.startTest();
            System.runAs(adminUser)
            {
                EP_VendorSyncNAVtoSFDC.vendorSync();
            } 
        test.stopTest();        
     }
   
     /*
    Method to test Vendor Update
    */
      static testMethod void createUpdateVendor_Failure_Test()  
        {
        Schema.DescribeFieldResult fieldResult = Product2.EP_Unit_of_Measure__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        if( !ple.isEmpty() )
        {
            uom = ple[0].getValue();
        }
        else
        {
            uom = EP_Common_Constant.UOM_STRING;
        }
        
        //insert storage location
        setupStorageLocationData();
        //insert supplier
        setupSupplierData();
        //insert company
        setupCompanyData();
        //insert product
        setupProductData();
        //insert payment term
        setupPaymentTermData();
        
        
        EP_VendorSyncHandler.WrapperIdentifier wrapperIdentLine = new EP_VendorSyncHandler.WrapperIdentifier();
        wrapperIdentLine.vendorId = EP_Common_Constant.BLANK;
        wrapperIdentLine.clientId = EP_Common_Constant.BLANK;
        
        
        EP_VendorSyncHandler.WrapperVendor wrapperLine = new EP_VendorSyncHandler.WrapperVendor();
        wrapperLine.seqId = EP_Common_Constant.BLANK;
        wrapperLine.identifier  = wrapperIdentLine;
        wrapperLine.name = EP_Common_Constant.BLANK;
        wrapperLine.name2 = EP_Common_Constant.BLANK;
        wrapperLine.address = EP_Common_Constant.BLANK;

        wrapperLine.address2= EP_Common_Constant.BLANK;
        wrapperLine.city = EP_Common_Constant.BLANK;
        wrapperLine.postcode = EP_Common_Constant.BLANK;
        wrapperLine.cntryCode = EP_Common_Constant.BLANK;
        
        /*TFS fix 45559 start added changes for vendor type field*/
        list<EP_VendorSyncHandler.WrapperVendorType> lstwraVendorType = new list<EP_VendorSyncHandler.WrapperVendorType>();
        EP_VendorSyncHandler.WrapperVendorType wraVendorType = new EP_VendorSyncHandler.WrapperVendorType();
        wraVendorType.vendorTyp = EP_VendorSyncHandler.VENDOR_TYPE_SFDC_TRSN;
        lstwraVendorType.add(wraVendorType);
        wrapperLine.vendorTypes = lstwraVendorType;
        /*TFS fix 45559 end*/
        
        wrapperLine.blocked = EP_Common_Constant.BLANK;
        wrapperLine.paymentTerm = EP_Common_Constant.BLANK;
        wrapperLine.currencyId =EP_Common_Constant.BLANK;
        wrapperLine.transportMgmt = EP_Common_Constant.BLANK;
        wrapperLine.duties = EP_Common_Constant.BLANK;
        
        EP_VendorSyncHandler.WrapperVendors WrapperVendors = new EP_VendorSyncHandler.WrapperVendors ();
        WrapperVendors.vendor = new list<EP_VendorSyncHandler.WrapperVendor>();
        WrapperVendors.vendor.add(wrapperLine);
        
        EP_VendorSyncHandler.WrapperVendorCls WrapperVendor = new EP_VendorSyncHandler.WrapperVendorCls();
        WrapperVendor.vendor = wrapperLine ;
        
        String requestBody = JSON.serialize(WrapperVendor); 
        requestBody = EP_Common_Constant.BLANK + requestBody + EP_Common_Constant.RIGHT_CURLY_BRACE;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        res.statusCode = Integer.valueOf( EP_Common_Constant.CODE_200 );  
        
        req.requestURI = SFDCBASEURL + VENDORSYNCENDPOINT;
        req.httpMethod = EP_Common_Constant.POST;
        req.requestBody = Blob.valueOf( requestBody );
        
        req.addHeader( EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON );
        RestContext.request = req;
        RestContext.response = res;
        
        test.startTest();
            System.runAs(adminUser)   
            {
                EP_VendorSyncNAVtoSFDC.vendorSync(); 
            }
        test.stopTest();
         
     }


    //Code changes for Requirement - #59462(Reliable Messaging) and #59463(Message Idempotency)
    /**
    * @author kalpesh.j.thakur@accenture.com
    * @date 06/09/2017
    * @description test to check the Integration records are created for Inbound request
    */
    static testMethod void createUpdateVendor_Idempotent_Test() {
        String requestBody = '{"MSG":{"HeaderCommon":{"MsgID":"170905184721847-{04204107-C0CF-4D69-AF80-567326E6B3B0}","InterfaceType":"NAV","SourceGroupCompany":"AAF","DestinationGroupCompany":"","SourceCompany":"AAF","DestinationCompany":"","CorrelationID":"","DestinationAddress":"","SourceResponseAddress":"https://integrationservice-uat02.eu.coolfurnace.net/api/v2/ipaas/ack","SourceUpdateStatusAddress":"https://integrationservice-uat02.eu.coolfurnace.net/api/v2/ipaas/ack","DestinationUpdateStatusAddress":"","MiddlewareUrlForPush":"https://dtrafloco2k321.global.trafigura.com/EPUMA_NavToIPaaS_PushAsync/EPUMA_NavToIPaaS_PushAsync.svc","EmailNotification":"","ErrorCode":"","ErrorDescription":"","ProcessingErrorDescription":"","ContinueOnError":"true","ComprehensiveLogging":"true","TransportStatus":"Delivered","ProcessStatus":"","UpdateSourceOnReceive":"true","UpdateSourceOnDelivery":"true","UpdateSourceAfterProcessing":"true","UpdateDestinationOnDelivery":"true","CallDestinationForProcessing":"true","ObjectType":"Table","ObjectName":"Vendor Staging SF","CommunicationType":"Async"},"Payload":{"any0":{"vendors":{"vendor":[{"seqId":"{7BEE1D1F-2F7E-428D-84F4-B56869B70C31}","identifier":{"clientId":"AAF","vendorId":"600000045"},"name":"Smoke Test Vendor 02 050917","name2":null,"address":"Test","address2":null,"city":"SYDNEY","postCode":"1001","cntryCode":"AU","vendorType":"Transporter","blocked":"YES","paymentTerm":"01D","currencyId":"AUD","transportMgmt":"False","duties":""}]}}},"StatusPayload":"StatusPayload"}}';

        Test.startTest();
        string old_Response = EP_VendorSyncHandler.createUpdateVendor(requestBody); 
        Test.stopTest();
        List<EP_IntegrationRecord__c> lstIntegrationRec = [SELECT Id FROM EP_IntegrationRecord__c];
        System.assert(lstIntegrationRec.size()==1);
    }

    //Code changes for Requirement - #59462(Reliable Messaging) and #59463(Message Idempotency)
    /**
    * @author kalpesh.j.thakur@accenture.com
    * @date 06/09/2017
    * @description test to check same response from integration records is sent second time without processing the response
    */
    static testMethod void createUpdateVendor_IdempotentSameResponse_Test() {
        /*L4_45559_Start*/
        setupPaymentTermData();
        company1 = EP_TestDataUtility.createCompany( 'AAF' );
        Database.insert(company1);
        
        String requestBody = '{"MSG":{"HeaderCommon":{"MsgID":"17082471176464-{F998F29B-F4A7-4B7D-B76A-733837BF80C88}","InterfaceType":"NAV","SourceGroupCompany":"AAF","DestinationGroupCompany":"","SourceCompany":"AAF","DestinationCompany":"","CorrelationID":"","DestinationAddress":"","SourceResponseAddress":"https://integrationservice-uat02.eu.coolfurnace.net/api/v2/ipaas/ack","SourceUpdateStatusAddress":"https://integrationservice-uat02.eu.coolfurnace.net/api/v2/ipaas/ack","DestinationUpdateStatusAddress":"","MiddlewareUrlForPush":"https://dtrafloco2k321.global.trafigura.com/EPUMA_NavToIPaaS_PushAsync/EPUMA_NavToIPaaS_PushAsync.svc","EmailNotification":"","ErrorCode":"","ErrorDescription":"","ProcessingErrorDescription":"","ContinueOnError":"true","ComprehensiveLogging":"true","TransportStatus":"Delivered","ProcessStatus":"","UpdateSourceOnReceive":"true","UpdateSourceOnDelivery":"true","UpdateSourceAfterProcessing":"true","UpdateDestinationOnDelivery":"true","CallDestinationForProcessing":"true","ObjectType":"Table","ObjectName":"Vendor Staging SF","CommunicationType":"Async"},"payload": {"vendors": {"vendor": [{"seqId": "{8A9D0AF1-8923-4856-8193-AABB4136AA2A}","identifier": {"clientId": "AAF","vendorId": "00000014"},"enterpriseId": "111010000000014","name": "darfasdsae 00000014","name2": null,"address": "qweqweqw","address2": null,"city": "Murau","postCode": "AT-8850","cntryCode": "AT","VendorType": "General Supplier,Transporter and 3rd Party Stock Supplier","blocked": "YES","paymentTerm": "COD","currencyId": "GBP","transportMgmt": "N/A","duties": "Excise Free","vendorTypes": [{"vendorTyp": "Transporter"},{"vendorTyp": "3rd Party Stock Supplier"},{"vendorTyp": "General Supplier"}],"versionNr": null}]}},"StatusPayload":"StatusPayload"}}';

        Test.startTest();
        string old_Response = EP_VendorSyncHandler.createUpdateVendor(requestBody); 
        // calling second time will return the logged last response
        string new_Response = EP_VendorSyncHandler.createUpdateVendor(requestBody); 
        Test.stopTest();
        System.assert(old_Response == new_Response);
        /*L4_45559_END*/
    }

    /**
    * @author kalpesh.j.thakur@accenture.com
    * @date 06/09/2017
    * @description test to check exception handling by using wrong request
    */
    static testMethod void createUpdateVendor_IdempotentException_Test() {
        String requestBody = '{"MSG":{"HeaderCommon":{"MsgID":"170905184721847-{04204107-C0CF-4D69-AF80-567326E6B3B0}","InterfaceType":"NAV","SourceGroupCompany":"AAF","DestinationGroupCompany":"","SourceCompany":"AAF","DestinationCompany":"","CorrelationID":"","DestinationAddress":"","SourceResponseAddress":"https://integrationservice-uat02.eu.coolfurnace.net/api/v2/ipaas/ack","SourceUpdateStatusAddress":"https://integrationservice-uat02.eu.coolfurnace.net/api/v2/ipaas/ack","DestinationUpdateStatusAddress":"","MiddlewareUrlForPush":"https://dtrafloco2k321.global.trafigura.com/EPUMA_NavToIPaaS_PushAsync/EPUMA_NavToIPaaS_PushAsync.svc","EmailNotification":"","ErrorCode":"","ErrorDescription":"","ProcessingErrorDescription":"","ContinueOnError":"true","ComprehensiveLogging":"true","TransportStatus":"Delivered","ProcessStatus":"","UpdateSourceOnReceive":"true","UpdateSourceOnDelivery":"true","UpdateSourceAfterProcessing":"true","UpdateDestinationOnDelivery":"true","CallDestinationForProcessing":"true","ObjectType":"Table","ObjectName":"Vendor Staging SF","CommunicationType":"Async"},StatusPayload":"StatusPayload"}}';
        Test.startTest();
        string old_Response = EP_VendorSyncHandler.createUpdateVendor(requestBody); 
        Test.stopTest();
        List<EP_Exception_Log__c> lstExLog = [SELECT Id FROM EP_Exception_Log__c];
        System.assertEquals(lstExLog.size(),1);
     }
    //Code changes End for Requirement - #59462(Reliable Messaging) and #59463(Message Idempotency)
    
    /*TFS fix 45559 start added changes for vendor L4*/
    /**
    * @author kamendra.singh@accenture.com
    * @date 03/11/2017
    * @description test to check getVendorType method for positive scenario  
    */
    static testMethod void getVendorType_PositiveTest(){
         list<EP_VendorSyncHandler.WrapperVendorType> lstwraVendorType = new list<EP_VendorSyncHandler.WrapperVendorType>();
            
        EP_VendorSyncHandler.WrapperVendorType wraVendorType = new EP_VendorSyncHandler.WrapperVendorType();
        wraVendorType.vendorTyp = 'General Supplier';
        lstwraVendorType.add(wraVendorType);
        
        EP_VendorSyncHandler.WrapperVendorType wraVendorTypeTran = new EP_VendorSyncHandler.WrapperVendorType();
        wraVendorTypeTran.vendorTyp = 'Transporter';
        lstwraVendorType.add(wraVendorTypeTran);
        
        EP_VendorSyncHandler.WrapperVendorType wraVendorType3rdParty = new EP_VendorSyncHandler.WrapperVendorType();
        wraVendorType3rdParty.vendorTyp = '3rd Party Stock Supplier';
        lstwraVendorType.add(wraVendorType3rdParty);
        
        EP_VendorSyncHandler.WrapperVendorType wraVendorTypeEMP = new EP_VendorSyncHandler.WrapperVendorType();
        wraVendorTypeEMP.vendorTyp = 'Employee';
        lstwraVendorType.add(wraVendorTypeEMP);
         
        test.startTest();
           
        string strVendorType = EP_VendorSyncHandler.getVendorType(lstwraVendorType);
        // check assert 
        system.assertEquals(strVendorType,'Transporter;3rd Party Stock Supplier');
        test.stopTest();    
    }
    
    /* @author kamendra.singh@accenture.com
    * @date 03/11/2017
    * @description test to check getVendorType method for Negative scenario 
    */
    static testMethod void getVendorType_NegativeTest(){
        list<EP_VendorSyncHandler.WrapperVendorType> lstwraVendorType = new list<EP_VendorSyncHandler.WrapperVendorType>();
            
        EP_VendorSyncHandler.WrapperVendorType wraVendorTypeEMP = new EP_VendorSyncHandler.WrapperVendorType();
        wraVendorTypeEMP.vendorTyp = 'Employee';
        lstwraVendorType.add(wraVendorTypeEMP);
        
        EP_VendorSyncHandler.WrapperVendorType wraVendorType = new EP_VendorSyncHandler.WrapperVendorType();
        wraVendorType.vendorTyp = 'General Supplier';
        lstwraVendorType.add(wraVendorType);
        
        test.startTest();
           
        string strVendorType = EP_VendorSyncHandler.getVendorType(lstwraVendorType);
        // check assert 
        system.assertEquals(strVendorType,null);
        test.stopTest();
    }

    /**
    * @author kamendra.singh@accenture.com
    * @date 03/11/2017
    * @description test to check isTransporter method for positive scenario  
    */
    static testMethod void isTransporter_PositiveTest(){
        string strVendorType = 'Transporter';            
        test.startTest();
           
        Boolean IsVendorType = EP_VendorSyncHandler.isTransporter(strVendorType);
        // check assert 
        system.assertEquals(IsVendorType,true);
        test.stopTest();  
    }
    
    /* @author kamendra.singh@accenture.com
    * @date 03/11/2017
    * @description test to check isTransporter method for Negative scenario 
    */
    static testMethod void isTransporter_NegativeTest(){
        string strVendorType = 'test';           
        test.startTest();
           
        Boolean IsVendorType = EP_VendorSyncHandler.isTransporter(strVendorType);
        // check assert 
        system.assertEquals(IsVendorType,false);
        test.stopTest();
    }
    /**
    * @author kamendra.singh@accenture.com
    * @date 03/11/2017
    * @description test to check isSupplier method for positive scenario  
    */
    static testMethod void isSupplier_PositiveTest(){
        string strVendorType = '3rd Party Stock Supplier';           
        test.startTest();
           
        Boolean IsVendorType = EP_VendorSyncHandler.isSupplier(strVendorType);
        // check assert 
        system.assertEquals(IsVendorType,true);
        test.stopTest();  
    }
    
    /* @author kamendra.singh@accenture.com
    * @date 03/11/2017
    * @description test to check isSupplier method for Negative scenario 
    */
    static testMethod void isSupplier_NegativeTest(){
        string strVendorType = 'test';           
        test.startTest();
           
        Boolean IsVendorType = EP_VendorSyncHandler.isSupplier(strVendorType);
        // check assert 
        system.assertEquals(IsVendorType,false);
        test.stopTest();
    }
    /*TFS fix 45559 end*/
 }