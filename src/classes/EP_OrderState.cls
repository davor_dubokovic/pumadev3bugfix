/*
 *  @Author <Aravindhan Ramalingam>
 *  @Name <EP_OrderState>
 *  @CreateDate <03/02/2017>
 *  @Description <OrderState validates inbound status of order is valid, find next possible status and return the final recommended status>
 *  @Version <1.0>
 */

 public virtual class EP_OrderState {
    public EP_OrderDomainObject order;
    public EP_OrderService orderService;
    public EP_OrderEvent orderEvent;
    //public List<EP_OrderStateTransition> orderStateTransitions;
    static String orderConstant = 'Order';
    
    // For the calling context (controller/handler) to have access to the transitioned state
    public EP_OrderState transitionedState;
     
    public EP_OrderState(){

    }
    
    public virtual void doOnEntry(){
        EP_GeneralUtility.Log('Public','EP_OrderState','doOnEntry');

    }
    
    public virtual void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_OrderState','doOnExit');

    }


    /*public virtual boolean isOutboundTransitionPossible(){
        
        }*/

//removing as part of cleaning
       /* public virtual boolean doTransition(){
            EP_GeneralUtility.Log('Public','EP_OrderState','doTransition');
        //If no transitions defined for this state - return false
        if (orderStateTransitions.isEmpty())
        {
            String exceptionMsg = Label.EP_StateTransitionException;
            throw new EP_OrderStateMachineException(String.format(exceptionMsg,new String[]{order.getStatus(),orderEvent.getEventName()}));
            return false;
        }

        //Final State of Order will be
        //IF failure transitions exist - then pick the "highest priority" of failure and set the recommended state from that transition
        //IF no failure transitions - then pick the "lowest priority" of the successful transition and set the recommended state


        for(EP_OrderStateTransition ost:orderStateTransitions)
        {
            if (ost.isTransitionPossible())
            {
                    //the transitions are iterated by priority.  The first one that catches the guard condition, the status is set and exits the for.
                    this.order.setStatus(ost.finalState);
                    system.debug('finalState' + ost.finalState);
                    break;
                }
            }       

        //Finally check if the inbound transition to that state is possible
        EP_OrderState finalState = EP_OrderStateMachine.getOrderState(this.order, this.orderEvent);
        if (!finalState.isInboundTransitionPossible())
        {
                //reset the status back to it's from Status
                system.debug('***Inbound gaurd condition(s) are NOT permitting state transition to ' + finalState);
                this.orderEvent.setEventMessage('Gaurd condition(s) are NOT permitting state transition to '+ finalState);
                this.order.resetStatus();
                return false;           
                }else{
                    transitionedState = finalState;
                }


        //*********** Check **************
        
        // if the onEntry actions should be called from outside or from within State Machine
        // uncomment the following statement
        // finalState.doOnEntry();
        
        //***********till here
        
        //finally if everything passes - return true
        //this.orderEvent.setEventMessage(this.order.getStatus());
        //this.orderEvent.setIsStatusChanged(true);
        return true;
    }
    */
    
    public virtual void setOrderContext(EP_OrderDomainObject currentOrder,EP_OrderEvent currentEvent){
        EP_GeneralUtility.Log('Public','EP_OrderState','setOrderContext');
        this.orderEvent = currentEvent;
        this.order = currentOrder;
        //this.orderStateTransitions = ConvertCustomSettingsToObject(currentOrder.getStatus());
        this.orderService = new EP_OrderService(currentOrder);
    }
    
    @TestVisible
    /* private List<EP_OrderStateTransition> ConvertCustomSettingsToObject(String stateTextValue)
    {
        EP_GeneralUtility.Log('Private','EP_OrderState','ConvertCustomSettingsToObject');
        //Step 1: Fetch the definitions from the Custom settings
        List<EP_OrderStateTransition> localST = new List<EP_OrderStateTransition>();
        
        // constructing string to match the values in the Entity type of State transitions custom setting
        String ordertypename = order.getOrderTypeClassification() + orderConstant;
        system.debug('$$$$$$$$'+ordertypename);
        
        for(EP_State_Transitions__c transition:[select Transition_Class_Type__c,Transition_Priority__c from EP_State_Transitions__c where EntityType__c=:ordertypename and Event__c=:orderEvent.getEventName() and State_Name__c = :stateTextValue ORDER BY Transition_Priority__c DESC]){
            system.debug('Possible transition? ' + transition.Transition_Class_Type__c);
            
            try{
                // Get the Type corresponding to the class name
                Type t = Type.forName(transition.Transition_Class_Type__c);
                EP_OrderStateTransition OT = (EP_OrderStateTransition)t.newInstance();
                OT.setPriority(transition.Transition_Priority__c.intValue());
                OT.setOrderContext(this.order, this.orderEvent);
                localST.add(OT);
                }catch (Exception e){
                    throw new EP_OrderStateMachineException('Order State Transition not available for ' + transition.Transition_Class_Type__c);
                }

            }

            return localST;
        }*/

        public virtual void setOrderDomainObject(EP_OrderDomainObject currentOrder)
        {
            EP_GeneralUtility.Log('Public','EP_OrderState','setOrderDomainObject');
            order = currentOrder;
        } 
    }