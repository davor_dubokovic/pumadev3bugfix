/*
    @Author          Accenture
    @Name            EP_ROImportStagingService
    @CreateDate      
    @Description     virtual class is used to RO Import Staging Object
    @Version         1.0
    @Reference       NA
*/
public virtual class EP_ROImportStagingService {
    public set<Id> fileIds = new set<Id>();
    public list<EP_RO_Import_Staging__c> stagingRecords = new list<EP_RO_Import_Staging__c>();
    public map<id, list<EP_RO_Import_Staging__c>> stagingRecordsMap = new map<id, list<EP_RO_Import_Staging__c>>();
    public map<string, string> deliveryDocNoWithOrdNo = new map<string, string>();
    public map<string, Account> SellToShipToCompositeKeyMap = new map<string, Account>();
    public map<string, Account> NavStockLocationMap = new map<string, Account>();
    public map<string, csord__Order__c> OrderNumberMap = new map<string, csord__Order__c>();
    public map<string, Product2> productCodeMap = new map<string, Product2>();
    public map<string, Account> supplierMap = new map<string, Account>();
    public map<string, Account> transporterMap = new map<string, Account>();
    public map<string, Contract> ContractNumberMap = new map<string, Contract>();
    
    
    public void processStagingDataValidation(set<Id> fileIdSet){
    	try {
	    	init(fileIdSet);
	    	masterDataVerification();
	    	//updateStagingRecords();
	    	//updateFileRecord();
    	} catch(exception exp) {
    		EP_LoggingService.logHandledException(exp, EP_Common_Constant.EPUMA, 'processStagingDataValidation',EP_ROImportStagingService.class.getName(), ApexPages.Severity.ERROR);
    	}
    }

    public virtual void masterDataVerification(){
    	for(Id fileId : stagingRecordsMap.keySet()){
    		for(EP_RO_Import_Staging__c stagingRec : stagingRecordsMap.get(fileId)) {
	    		basicDataVerification(stagingRec);
	    	}
    	}
    }
    
    public virtual void basicDataVerification(EP_RO_Import_Staging__c stagingRec) {
    	/*
    	verifyDeliveryDocket(stagingRec);//Global Validation
    	duplicateDataCheck(stagingRec);//With in a File Records
    	verifySupplyLocation(stagingRec);
		verifySellToShipToRelationship(stagingRec);
		verifyProductCode(stagingRec);
		verifySellTo(stagingRec);
		verifyShipTo(stagingRec);*/
    }
    
    public virtual void updateStagingRecords() {
    	
    }
    
    @testVisible 
    private void init(set<Id> fileIdSet){
    	this.fileIds = fileIdSet;
    	getStagingRecords();
    	createDataSets();
    }
    @testVisible 
    private virtual void getStagingRecords() {
    	
    }
    
    
    @testVisible 
    private void createDataSets() {
    	set<string> acCompositeKeySet = new set<string>();
    	set<string> deliveryDocketSet = new set<string>();
    	set<string> orderNumberSet = new set<string>();
    	set<string> navStockLocationSet = new set<string>();//i.	Supply location 
    	set<string> productCodeSet = new set<string>();//EP_Composite_Id__c
    	set<string> transporterSet = new set<string>();//EP_Vendor_Unique_Id__c 
    	set<string> supplierNumberSet = new set<string>();//EP_Vendor_Unique_Id__c
    	set<string> contractNumberSet  = new set<string>();//
    	
    	for(EP_RO_Import_Staging__c stagingRec : stagingRecords) {
    		acCompositeKeySet.add(stagingRec.EP_Sell_To_Composit_key__c);
    		acCompositeKeySet.add(stagingRec.EP_Ship_To_Composit_Key__c);
    		deliveryDocketSet.add(stagingRec.EP_Delivery_Docket_Number__c);
    		orderNumberSet.add(stagingRec.EP_Order_Number__c);
    		navStockLocationSet.add(stagingRec.EP_Supply_Location_Code__c);
    		productCodeSet.add(stagingRec.EP_Product_Unique_Key__c);
    		transporterSet.add(stagingRec.EP_Transporter_Number__c);
    		supplierNumberSet.add(stagingRec.EP_Supplier_Number__c);
    		contractNumberSet.add(stagingRec.EP_Contract_Number__c);
    	}
    }
}