/**
 *  @Author <Kamal Garg>
 *  @Name <EP_ManageCustomerDeliveryDocketDocument>
 *  @CreateDate <22/07/2016>
 *  @Description <This is the apex class used to generate PDF for Delivery Docket record>
 *  @Version <1.0>
 */
public with sharing class EP_ManageCustomerDeliveryDocketDocument {
    
    
    private static final string CLASS_NAME = 'EP_ManageCustomerDeliveryDocketDocument';
    private static final string GENERATE_AT_METHOD = 'generateAttachmentForDeliveryDocket';
    
    /**
     * @author <Kamal Garg>
     * @name <generateAttachmentForDeliveryDocket>
     * @date <22/07/2016>
     * @description <This method is used to generate document for Delivery Docket>
     * @version <1.0>
     * @param EP_ManageDocument.Documents
     */
    public static void generateAttachmentForDeliveryDocket(EP_ManageDocument.Documents documents) {
        List<EP_Delivery_Docket__c> deliveryDockets = new List<EP_Delivery_Docket__c>();
        Map<String, List<Attachment>> deliveryDocNumberAttachListMap = new Map<String, List<Attachment>>();
        Map<String, String> docNrOrderIdMap = new Map<String, String>();
        Map<String, String> docNrShipToMap = new Map<String, String>();
        Set<String> shipToSet = new Set<String>();
        Set<String> orderIdSet = new Set<String>();
        try
        {	
            List<Attachment> deliveryDocNumberAttachment = null;
            for(EP_ManageDocument.Document doc : documents.document) {
                EP_ManageDocument.CustomerDeliveryDocketWrapper custDelivDocktWrap = EP_DocumentUtil.fillCustomerDeliveryDocketWrapper(doc.documentMetaData.metaDatas);
                deliveryDockets.add(generateCustomerDeliveryDocket(custDelivDocktWrap));
                
                docNrOrderIdMap.put(custDelivDocktWrap.deliveryDocNr, custDelivDocktWrap.orderId);
                String compositKey = custDelivDocktWrap.issuedFromId + EP_Common_Constant.HYPHEN + custDelivDocktWrap.issuedToId + EP_Common_Constant.HYPHEN + custDelivDocktWrap.shipTo;
                docNrShipToMap.put(custDelivDocktWrap.deliveryDocNr, compositKey.toUpperCase());
                
                shipToSet.add(compositKey);
                orderIdSet.add(custDelivDocktWrap.orderId);
                
                Attachment att = EP_DocumentUtil.generateAttachment(doc);
                String deliveryDocNr = custDelivDocktWrap.deliveryDocNr;
                if(!(deliveryDocNumberAttachListMap.containsKey(deliveryDocNr))) {
                    deliveryDocNumberAttachment = new List<Attachment>();
                    deliveryDocNumberAttachListMap.put(deliveryDocNr, deliveryDocNumberAttachment);
                }
                deliveryDocNumberAttachListMap.get(deliveryDocNr).add(att);
            }
            
            Map<String, Id> acctNumIdMap = EP_DocumentUtil.getAccountCompositIdMap(shipToSet, 
            new Set<String>{EP_Common_Constant.VMI_SHIP_TO_DEV_NAME, EP_Common_Constant.NON_VMI_SHIP_TO_DEV_NAME, EP_Common_Constant.STORAGE_SHIP_TO_DEV_NAME});
            
            Map<String, Id> ordNumIdMap = getOrderNumberIdMap(orderIdSet);
            
            for(EP_Delivery_Docket__c obj : deliveryDockets) {
                obj.EP_Order__c = ordNumIdMap.get(docNrOrderIdMap.get(obj.EP_Delivery_Doc_Number__c));
                System.debug('Order ID: ' + obj.EP_Order__c);
                String recordId = acctNumIdMap.get(docNrShipToMap.get(obj.EP_Delivery_Doc_Number__c));
                System.debug('Ship-To ID: ' + recordId);
                obj.EP_Ship_To__c = recordId;
            }
            
            Schema.SObjectField f = EP_Delivery_Docket__c.Fields.EP_Delivery_Doc_Number__c;
            List<Database.upsertResult> uResults = Database.upsert(deliveryDockets, f, false);
            for(Database.upsertResult result : uResults) {
                if(result.isSuccess() && result.isCreated()) {
                    System.debug('Successfully inserted/updated Delivery Docket, CDD ID: ' + result.getId());
                } else {
                    for(Database.Error err : result.getErrors()) {
                        System.debug('The following error has occurred.');
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('CDD fields that affected this error: ' + err.getFields());
                    }
                }
            }
            
            Map<String, Id> deliveryDocNrIdMap = new Map<String, Id>();
            Integer nRows = EP_Common_Util.getQueryLimit();
            deliveryDockets = [SELECT Id, EP_Delivery_Doc_Number__c FROM EP_Delivery_Docket__c limit :nRows];
            for(EP_Delivery_Docket__c obj : deliveryDockets) {
                deliveryDocNrIdMap.put(obj.EP_Delivery_Doc_Number__c, obj.Id);
            }
            
            List<Attachment> attachments = new List<Attachment>();
            for(String key : deliveryDocNumberAttachListMap.keySet()) {
                List<Attachment> attachList = deliveryDocNumberAttachListMap.get(key);
                for(Attachment att : attachList) {
                    att.ParentId = deliveryDocNrIdMap.get(key);
                    attachments.add(att);
                }
            }
            
            EP_DocumentUtil.deleteAttachments(attachments);
            
            Database.SaveResult[] results = Database.insert(attachments, false);
            for (Integer i = 0; i < results.size(); i++) {
                if (results[i].isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully inserted attachment. Attachment ID: ' + results[i].getId());
                    EP_ManageDocument.successRecIdMap.put(attachments[i].Name, results[i].getId());
                } else {
                    // Operation failed, so get all errors
                    for(Database.Error err : results[i].getErrors()) {
                        System.debug('The following error has occurred.');
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Attachment fields that affected this error: ' + err.getFields());
                        EP_ManageDocument.errDesMap.put(attachments[i].Name, err.getMessage());
                        EP_ManageDocument.errCodeMap.put(attachments[i].Name, String.valueOf(err.getStatusCode()));
                    }
                }
            }
        }
        catch(Exception e){
             EP_loggingService.loghandledException(e,EP_Common_Constant.EPUMA, GENERATE_AT_METHOD , CLASS_NAME,apexPages.severity.ERROR);
             throw e;
        }
    }
    
    /**
     * @author <Kamal Garg>
     * @name <getOrderNumberIdMap>
     * @date <22/07/2016>
     * @description <This method is used to get Map containing 'OrderNumber' as key and 'Id' as value>
     * @version <1.0>
     * @param Set<String>
     * @return Map<String, Id>
     */
    private static Map<String, Id> getOrderNumberIdMap(Set<String> orderIdSet) {
        Map<String, Id> ordNumIdMap = new Map<String, Id>(); 
        Integer nRows = EP_Common_Util.getQueryLimit();
        for(Order orderObj :[SELECT Id, OrderNumber FROM Order WHERE OrderNumber IN:orderIdSet Limit :nRows]) {
            ordNumIdMap.put(orderObj.OrderNumber, orderObj.Id);
        }
        
        return ordNumIdMap;
    }
    
    /**
     * @author <Kamal Garg>
     * @name <generateCustomerDeliveryDocket>
     * @date <22/07/2016>
     * @description <This method is used to generate 'EP_Delivery_Docket__c' custom object record>
     * @version <1.0>
     * @param EP_ManageDocument.CustomerDeliveryDocketWrapper
     * @return EP_Delivery_Docket__c
     */
    private static EP_Delivery_Docket__c generateCustomerDeliveryDocket(EP_ManageDocument.CustomerDeliveryDocketWrapper custDelivDocktWrap) {
        EP_Delivery_Docket__c obj = new EP_Delivery_Docket__c();
        obj.EP_Delivery_Doc_Date__c = Date.valueOf(custDelivDocktWrap.deliveryDocDate);
        obj.EP_Delivery_Doc_Number__c = custDelivDocktWrap.deliveryDocNr;
        obj.EP_Trip_Id__c = custDelivDocktWrap.tripId;
        
        return obj;
    }
}