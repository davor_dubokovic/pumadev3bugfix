/*   
     @Author Aravindhan Ramalingam
     @name <EP_OrderStatePlanned.cls>     
     @Description <Order State for Planned status>   
     @Version <1.1> 
     */

     public class EP_VmiConsignmentOrderStatePlanned extends EP_OrderState {
        
        public EP_VmiConsignmentOrderStatePlanned (){
            
        }
        public override void setOrderDomainObject(EP_OrderDomainObject currentOrder)
        {
            super.setOrderDomainObject(currentOrder);
        }
        
        public override void doOnEntry(){
            EP_GeneralUtility.Log('Public','EP_VmiConsignmentOrderStatePlanned ','doOnEntry');
            order.setOrderStockHoldingLocation();
          /*  if(!order.isInventoryLow()){
                 EP_OrderService orderService = new EP_OrderService(this.order);
                 orderService.calculatePrice();
            }  */ 
        }
        
        public override void doOnExit(){
            EP_GeneralUtility.Log('Public','EP_VmiConsignmentOrderStatePlanned ','doOnExit');
            if((EP_Common_Constant.Blank.equalsignorecase(this.order.getOrder().EP_Order_Credit_Status__c)) && !this.order.getOrder().EP_Sync_with_NAV__c){
                system.debug('In doOnExit doSyncStatusWithNav');
                this.orderService.doSyncStatusWithNav();
            }

            if(EP_Common_constant.Credit_Okay.equalsignorecase(this.order.getOrder().EP_Order_credit_status__c) 
                && EP_Common_constant.OK.equalsignorecase(this.order.getOrder().EP_Credit_Status__c) 
                && this.order.getOrder().EP_Sync_with_NAV__c){
                this.orderService.doSyncCreditCheckWithWINDMS();
            }
        }
        
    public static String getTextValue()
    {
        return EP_OrderConstant.OrderState_Planned;
    }
    

}