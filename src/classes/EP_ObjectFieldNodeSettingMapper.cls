/**
   @Author          CR Team
   @name            EP_ObjectFieldNodeSettingMapper
   @CreateDate      12/1/2017
   @Description     This class contains all SOQLs related to EP_ObjectFieldNodeSettingMapper Object
   @Version         1.0
   @reference       NA
*/

/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    -------------------------------------------------------------------------------------------------------------
    
    *************************************************************************************************************
*/
public with sharing class EP_ObjectFieldNodeSettingMapper{
   
  /**
    *  Constructor. 
    *  @name            EP_ObjectFieldNodeSettingMapper
    *  @param           NA
    *  @return          NA
    *  @throws          NA
    */ 
    public EP_ObjectFieldNodeSettingMapper() {
        
    } 
    /**  This method is used to get ObjectFieldNodeSetting based on EXTERNAL_SYSTEM 
    *  @name             
    *  @param       Integer        
    *  @return      EP_ObjectFieldNodeSetting__mdt     
    *  @throws           
    */
    public list<EP_ObjectFieldNodeSetting__mdt> getRecordsByExternalSystem (String externalSystemName ,Integer nRows ) {
    
    List<EP_ObjectFieldNodeSetting__mdt> nodeList =new List<EP_ObjectFieldNodeSetting__mdt>();
    
     for(List<EP_ObjectFieldNodeSetting__mdt> node:[SELECT EP_NODE_NAME__c
          ,EP_FIELD_API_NAME__c
          ,EP_Object_API_NAME__c
          ,EP_EXTERNAL_SYSTEM__c
          ,EP_RECORD_TYPE_NAME__c
          ,EP_Parent_Node__c
          FROM EP_ObjectFieldNodeSetting__mdt
          WHERE EP_EXTERNAL_SYSTEM__c = :EP_Common_Constant.ALL
          OR EP_EXTERNAL_SYSTEM__c = :externalSystemName
          LIMIT:nRows]) {   
                                                             
          nodeList.addAll(node);         
       }                                                      
      return nodeList ;    
     }
     
     /**  This method is used to get ObjectFieldNodeSetting based on EXTERNAL_SYSTEM 
    *  @name             
    *  @param       Integer        
    *  @return      EP_ObjectFieldNodeSetting__mdt     
    *  @throws           
    */
     public list<EP_ObjectFieldNodeSetting__mdt> getRecByNodeName(Integer nRows ) {
     List<EP_ObjectFieldNodeSetting__mdt> nodeList =new List<EP_ObjectFieldNodeSetting__mdt>();
    
     for(List<EP_ObjectFieldNodeSetting__mdt> node: [Select EP_NODE_NAME__c ,Sequence__c
          From EP_ObjectFieldNodeSetting__mdt
          Where EP_EXTERNAL_SYSTEM__c = :EP_Common_Constant.CUSTOMER_CREATE_XML
          Order By Sequence__c ASC LIMIT: nRows]){                                                      
                  nodeList.addAll(node);         
      }                                                      
     return nodeList ; 
    }
  }