@isTest
public class EP_Retrospective_UT
{
    static testMethod void getShipTos_test() {
        EP_Retrospective localObj = new EP_Retrospective();
        Account accountObj = [Select recordtype.developername from Account where Id =: EP_TestDataUtility.getShipTo().Id];
        List<Account> lstAccount = new List<Account>{accountObj};
        
        system.debug('---'+lstAccount);
        EP_ProductSoldAs productSoldAsType = new EP_ProductSoldAs();
        Test.startTest();
        LIST<Account> result = localObj.getShipTos(lstAccount,productSoldAsType);
        Test.stopTest();
        System.AssertEquals(1,result.size());
        
    }
    static testMethod void isValidLoadingDate_PositiveScenariotest() {
        EP_Retrospective localObj = new EP_Retrospective();
        csord__Order__c orderObj = EP_TestDataUtility.getRetrospectivePositiveScenario();
        Test.startTest();
        Boolean result = localObj.isValidLoadingDate(orderObj);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isValidLoadingDate_NegativeScenariotest() {
        EP_Retrospective localObj = new EP_Retrospective();
        csord__Order__c orderObj = EP_TestDataUtility.getRetrospectiveNegativeScenario();
        Test.startTest();
        Boolean result = localObj.isValidLoadingDate(orderObj);
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isValidExpectedDate_PositiveScenariotest() {
        EP_Retrospective localObj = new EP_Retrospective();
        csord__Order__c ord = EP_TestDataUtility.getRetrospectivePositiveScenario();
        Test.startTest();
        Boolean result = localObj.isValidExpectedDate(ord);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isValidExpectedDate_NegativeScenariotest() {
        EP_Retrospective localObj = new EP_Retrospective();
        csord__Order__c ord = EP_TestDataUtility.getRetrospectiveNegativeScenario();
        Test.startTest();
        Boolean result = localObj.isValidExpectedDate(ord);
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void findRecordType_test() {
        EP_Retrospective localObj = new EP_Retrospective();
        EP_NonVMIStrategy vmType = new EP_NonVMIStrategy();
        EP_Consignment consignmentType  = new EP_Consignment();
        Test.startTest();
        Id result = localObj.findRecordType(vmType,consignmentType);
        Test.stopTest();
        Id recoRrdTypeId = [Select id from RecordType where developerName =: EP_OrderConstant.nonVmiRecordType].Id;
        System.AssertEquals(recoRrdTypeId,result);
    }
    static testMethod void isValidOrderDate_PositiveScenariotest() {
        
        EP_Retrospective localObj = new EP_Retrospective();
        EP_OrderDomainObject orderDomainObj = EP_TestDataUtility.getRetrospectiveDomainObjectPositiveScenario();
        csord__Order__c ord = orderDomainObj.getOrder();
        Test.startTest();
        Boolean result = localObj.isValidOrderDate(ord);
        Test.stopTest();
        System.AssertEquals(false,result);
        
    }
    static testMethod void isValidOrderDate_NegativeScenariotest() {
        EP_Retrospective localObj = new EP_Retrospective();
        EP_OrderDomainObject orderDomainObj = EP_TestDataUtility.getRetrospectiveDomainObjectNegativeScenario();
        csord__Order__c ord = orderDomainObj.getOrder();
        Test.startTest();
        Boolean result = localObj.isValidOrderDate(ord);
        Test.stopTest();
        System.AssertEquals(true,result);
        
    }
    
    
    public static testMethod void setRetroOrderDetails_test() {
        EP_Retrospective localObj = new EP_Retrospective();
        EP_OrderDomainObject obj = EP_TestDataUtility.getRetrospectiveDomainObject();
        Test.startTest();
        localObj.setRetroOrderDetails(obj.getOrder());
        Test.stopTest();
        DateTime dT = obj.getOrder().EP_Expected_Delivery_Date__c;
        Datetime deliveryDate = dateTime.newinstance(dT.year(), dT.month(), dT.day(),0,0,0);
        System.AssertEquals(EP_PortalLibClass_R1.returnLocalDate(Date.valueOf(obj.getOrder().EP_Loading_Date__c)),obj.getOrder().EP_Expected_Loading_Date__c );
        //System.AssertEquals(EP_GeneralUtility.convertDateTimeToDecimal(String.valueOf(deliveryDate)),obj.getOrder().EP_Actual_Delivery_Date_Time__c );
    }
    
}
