/**
 *  @Author         : Kalpesh Thakur <kalpesh.j.thakur@accenture.com>
 *  @Name           : EP_GeneralUtility
 *  @CreateDate     : 04/02/2017
 *  @Description    : Domain object to store Order and Order related status
 *  @Version        : 1.0
 */
 public with sharing class EP_GeneralUtility {

  public static Integer counter;
  public static Boolean debugEnabled;
  public static String modifier;

  // Set the counter as 0 when invoked for the first time and set the cutom settings
  static{
    counter = 0;
    setLogSettings();
  }
  @TestVisible
  private static integer DAYS_TO_ADD =2;

     /**
    * @author       Accenture
    * @name         getETARangeOnOrder
    * @date         03/28/2017
    * @description  This method is to get range on order. 
    * @param        String , integer
    * @return       String
    */
     public static string getETARangeOnOrder(string estimatedDtTm, integer stsrtTm, integer endTm, string ordStatus){
      EP_GeneralUtility.Log('Public','EP_GeneralUtility','getETARangeOnOrder');
      string etaRange;
      Integer year, dt, mnth, min, hr, sec=00;
      dateTime startRange, endRange;
      if(estimatedDtTm.isNumeric()){
        year = integer.valueOf(estimatedDtTm.substring(0,4));
        dt = integer.valueOf(estimatedDtTm.substring(6,8));
        mnth = integer.valueOf(estimatedDtTm.substring(4,6));
        hr = integer.valueOf(estimatedDtTm.substring(8,10));
        min = integer.valueOf(estimatedDtTm.substring(10,12));
        Datetime etaDate = DateTime.newInstance(year, mnth, dt, hr, min, sec);
        if(ordStatus == EP_Common_Constant.delivered)
        etaRange = etaDate.format();
        else{
          startRange = etaDate.addHours(-stsrtTm);
          endRange = etaDate.addHours(endTm);
          etaRange = startRange.format() +EP_Common_Constant.STR_TO + endRange.format();
        }
      }
      else
      etaRange = EP_Common_Constant.BLANK;
      return etaRange;
    }    
    

    /**
    * @author       Accenture
    * @name         getEmailAddresses
    * @date         03/28/2017
    * @description  This method returns email addresses. 
    * @param        Set
    * @return       List
    */
     public static List<String> getEmailAddresses(set<String> grpName) {
      EP_GeneralUtility.Log('Public','EP_GeneralUtility','getEmailAddresses');
      List<String> idList = new List<String>();
      List<String> mailToAddresses = new List<String>();
      Integer nRows = EP_Common_Util.getQueryLimit();
      Group g = [SELECT (select userOrGroupId from groupMembers) FROM group WHERE name In :grpName Limit : EP_Common_Constant.ONE];
      for (GroupMember gm : g.groupMembers) {
        idList.add(gm.userOrGroupId);
      }
      for(User u :  [SELECT email FROM user WHERE id IN :idList Limit :nRows]) {
        mailToAddresses.add(u.email);
      }
      system.debug('--mailToAddresses---'+mailToAddresses);
      return mailToAddresses;
    } 

    /**
    * @author       Accenture
    * @name         sendEmail
    * @date         03/28/2017
    * @description  This method sends email. 
    * @param        String, List, ID
    * @return       NA
    */
     public static void sendEmail(String plainBody,List<String> lstemailAdrreses,Id targetId,Id templateId,String strSubject) {
      EP_GeneralUtility.Log('Public','EP_GeneralUtility','sendEmail');

      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

      mail.setSubject(strSubject);
      mail.setToAddresses(lstemailAdrreses);
      mail.setTargetObjectId(targetId);
      mail.setSaveAsActivity(false);
      mail.setTemplateId(templateId);
      mail.setPlainTextBody(plainBody);

      Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }

    /**
    * @author       Accenture
    * @name         removeHyphen
    * @date         03/28/2017
    * @description  This method removes hyphen. 
    * @param        String
    * @return       String
    */
     public static String removeHyphen(String strText){
      EP_GeneralUtility.Log('Public','EP_GeneralUtility','removeHyphen');
          return strText.replace('-',EP_Common_Constant.BLANK);  
    }


    /**
    * @author       Accenture
    * @name         convertSelectedDateToDateInstance
    * @date         03/28/2017
    * @description  This method converts SelectedDate To DateInstance. 
    * @param        String
    * @return       Date
    */
     public static Date convertSelectedDateToDateInstance(String strSelectedDate) {
      EP_GeneralUtility.Log('Public','EP_GeneralUtility','convertSelectedDateToDateInstance');

      Date dt = NULL;

      if (String.isNotBlank(strSelectedDate)) {

        String strDay = strSelectedDate.subString(0, 2);
        String strMonth = strSelectedDate.subString(3, 5);
        String strYear = strSelectedDate.subString(6, 10);
        dt = Date.newInstance(Integer.valueOf(strYear), Integer.valueOf(strMonth), Integer.valueOf(strDay));
      }

      return dt;
    }
    
    /**
    * @author       Accenture
    * @name         convertSelectedHourToTime
    * @date         03/28/2017
    * @description  This method converts selected hours into time. 
    * @param        String
    * @return       Time
    */
     public static Time convertSelectedHourToTime(String availableSlots) {
      EP_GeneralUtility.Log('Public','EP_GeneralUtility','convertSelectedHourToTime');
      Time tme = null;
      if (String.isNotBlank(availableSlots) ) {
       String strHour = availableSlots.subString( 0,2 );
       tme = Time.newInstance(Integer.valueOf(strHour),0,0,0);
     }
     else{
       tme = Time.newInstance(0,0,0,0);
     }
     return tme;
    }
    
    /**
    * @author       Accenture
    * @name         getCurrencyFormat
    * @date         03/28/2017
    * @description  This method is to get currency format. 
    * @param        NA
    * @return       String
    */
     public static String getCurrencyFormat(){
      EP_GeneralUtility.Log('Public','EP_GeneralUtility','getCurrencyFormat');
      string STR_QUERY1 = '{0,number,#';
      string STR_HASH = '###';
      string STR_HASH_1 = '##0';
      string STR_HASH_2 = '00}';
      Decimal value = 1000.10;
      String formattedValue = value.format();
      String thousandSep = formattedValue.substring(1, 2);
      String decimalSep = formattedValue.substring(5, 6);
      return STR_QUERY1 + thousandSep + STR_HASH + thousandSep + STR_HASH + thousandSep + STR_HASH + thousandSep + STR_HASH + thousandSep + STR_HASH_1 + decimalSep + STR_HASH_2;
    }  

    /**
    * @author       Accenture
    * @name         convertDateTimeToDecimal
    * @date         03/28/2017
    * @description  This method converts datetime to decimal. 
    * @param        string
    * @return       Decimal
    */
     public static Decimal convertDateTimeToDecimal(String strSelectedDateTime) {
      EP_GeneralUtility.Log('Public','EP_GeneralUtility','convertDateTimeToDecimal');
      String strYear = strSelectedDateTime.subString(0, 4);
      String strMonth = strSelectedDateTime.subString(5, 7);
      String strDay = strSelectedDateTime.subString(8, 10);
      String strHour = strSelectedDateTime.subString(11, 13);
      String strMin = strSelectedDateTime.subString(14, 16);

      return Decimal.valueOf(strYear+strMonth+strDay+strHour+strMin);      
    } 
    
    /**
    * @author       Accenture
    * @name         setDateFormat
    * @date         03/28/2017
    * @description  This method is to set a date format. 
    * @param        string
    * @return       string
    */
    public static string setDateFormat(string strSelectedDate){
        if (String.isNotBlank(strSelectedDate)) {
            String strDay = strSelectedDate.subString(8, 10);
            String strMonth = strSelectedDate.subString(5, 7);
            String strYear = strSelectedDate.subString(0, 4);
            strSelectedDate = strDay +EP_Common_Constant.STRING_HYPHEN+strMonth+EP_Common_Constant.STRING_HYPHEN+strYear;
        }
        return strSelectedDate;
    }
    
    /**
    * @author       Accenture
    * @name         updateOrderStatus
    * @date         03/28/2017
    * @description  This method updates the order status. 
    * @param        List
    * @return       NA
    */
    @invocableMethod
    public static void updateOrderStatus(List<csord__Order__c> orderList){
      EP_GeneralUtility.Log('Public','EP_GeneralUtility','updateOrderStatus');
      //EP_OrderDomainObject odo = new EP_OrderDomainObject(orderList[0].id);
      //EP_OrderEvent oe = new EP_OrderEvent(EP_Common_Constant.APPROVAL_PROCESS);
      //EP_OrderService orderService = new EP_OrderService(odo);
      //odo.previousStatus =  EP_Common_Constant.ORDER_AWAITING_INVNTRY_REVIEW_STATUS; 
      //EP_OrderTypeStateMachineFactory orderTypeStateMachineFactory = new EP_OrderTypeStateMachineFactory();
      //EP_OrderState orderState = orderTypeStateMachineFactory.getOrderStateMachine(odo).getOrderState(oe);
      //if(EP_Common_Constant.ORDER_STATUS_SUBMITTED.equalsIgnoreCase(odo.getStatus()) || EP_Common_Constant.CANCELLED_STATUS.equalsIgnoreCase(odo.getStatus())){
        //orderState.doOnExit();
      //}
      
    }    

	// Generates the readable trace logs, ensure corresponding settings are enabled in custom setting EP_Debug__C
    /**
    * @author       Accenture
    * @name         setLogSettings
    * @date         03/28/2017
    * @description  This method creates logs. 
    * @param        String
    * @return       NA
    */
    public static void Log(String publicOrPrivate, String ClassName, String MethodName){
            
            //Check whether to log Public and Private or just Public
            //Create a static counter and increment it
            //Debug Log ("Step " & counter & " Public::" & ClassName & "::" & MethodName)

            if(debugEnabled && modifier.contains(publicOrPrivate.trim().toLowerCase())){
              counter++;
              //system.debug('Step ' + counter + '::' + publicOrPrivate + '::' + ClassName + '::' + MethodName);          
            } 
    } 
  
	 // Methods retrieves custom settings and set the values in the local variable  
    /**
    * @author       Accenture
    * @name         setLogSettings
    * @date         03/28/2017
    * @description  This method sets the log settings. 
    * @param        NA
    * @return       NA
    */
    @TestVisible
    private static void setLogSettings(){
        /*EP_Debug__c debug = [select Enable__c,access_modifier__c from EP_Debug__c limit 1];
        debugEnabled = debug.Enable__c;
        modifier = (debug.access_modifier__c).toLowerCase();*/
        //List<EP_Debug__c> debugs = new List<EP_Debug__c>();
        List<EP_Debug__c> debugs = [select Enable__c,access_modifier__c from EP_Debug__c limit 1];
        if(debugs.isEmpty()){
            debugEnabled = false;
            modifier =EP_Common_Constant.BLANK;
        } else {
            debugEnabled= debugs[0].Enable__c;
            modifier = (debugs[0].access_modifier__c).toLowerCase();
        }
    }
    
    /**
    * @author       Accenture
    * @name         getNumericDisplayFormat
    * @date         03/28/2017
    * @description  This method returns value in numeric format. 
    * @param        NA
    * @return       String
    */ 
    public static string getNumericDisplayFormat(){
        Decimal value = 1000.10;
        String formattedValue = value.format();
        String thousandSep = formattedValue.substring(1, 2);
        String decimalSep = formattedValue.substring(5, 6);
        return EP_Common_Constant.STR_QUERY1 + thousandSep + EP_Common_Constant.STR_HASH + thousandSep + EP_Common_Constant.STR_HASH  
        + thousandSep + EP_Common_Constant.STR_HASH + thousandSep + EP_Common_Constant.STR_HASH + thousandSep + EP_Common_Constant.STR_HASH_1  
        + decimalSep + EP_Common_Constant.STR_HASH_2;   
    }
    
    /**
    * @author       Accenture
    * @name         getCompositeKey
    * @date         03/28/2017
    * @description  This method returns the composite key. 
    * @param        String
    * @return       String
    */
    public static string getCompositeKey(string part1, string part2){
        return part1 + EP_COMMON_CONSTANT.HYPHEN +  part2; 
    }
    
    /**
    * @author       Accenture
    * @name         getCompositeKey
    * @date         03/28/2017
    * @description  This method returns the composite key. 
    * @param        String
    * @return       String
    */
    public static string getCompositeKey(string part1, string part2, string part3){
        return part1 + EP_COMMON_CONSTANT.HYPHEN +  part2 + EP_COMMON_CONSTANT.HYPHEN + part3; 
    }
    
    /**
    * @author           Accenture
    * @name             getDMLErrorMessage
    * @date             03/13/2017
    * @description      Gets the error message from save result
    * @param            Database.SaveResult
    * @return           errorDescription
    */
    public static string getDMLErrorMessage(Database.SaveResult result){
        EP_GeneralUtility.Log('Public','EP_GeneralUtility','getDMLErrorMessage');
        string errorDescription=EP_Common_Constant.BLANK;
        for(Database.Error err : result.getErrors()){
            errorDescription = err.getMessage();
        }
       return errorDescription;
    }
    
    /**
    * @author           Accenture
    * @name             getDMLErrorMessage
    * @date             03/13/2017
    * @description      Gets the error message from upsert result
    * @param            Database.UpsertResult
    * @return           string
    */
    public static string getDMLErrorMessage(Database.UpsertResult result){
        EP_GeneralUtility.Log('Public','EP_GeneralUtility','getDMLErrorMessage');
        string errorDescription=EP_Common_Constant.BLANK;
        for(Database.Error err : result.getErrors()){
            errorDescription = err.getMessage();
        }
       return errorDescription;
    }
    
    /**
    * @author           Accenture
    * @name             getDMLErrorMessage
    * @date             03/13/2017
    * @description      Gets the error message from upsert result
    * @param            Database.UpsertResult
    * @return           DateTime
    */
    public static DateTime returnLocalDateTime(DateTime dt) {
        String T_STRING = 'T';
        DateTime dtSelectedDateTime = null;
        try{
            if (dt != NULL) {
                dtSelectedDateTime = (DateTime)JSON.deserialize(EP_Common_Constant.DOUBLE_QUOTES_STRING + dt.Year() +
                     EP_Common_Constant.hyphen + dt.Month() + EP_Common_Constant.hyphen + dt.Day() +
                     T_STRING + dt.Hour() + EP_Common_Constant.TIME_SEPARATOR_SIGN + dt.Minute() + 
                     EP_Common_Constant.TIME_SEPARATOR_SIGN + dt.Second() + 
                     EP_Common_Constant.DOUBLE_QUOTES_STRING, DateTime.class);
            } 
        }catch(Exception e){
             throw e;                                          
        }
        return dtSelectedDateTime;
    }
    
    /**
    * @author       Accenture
    * @name         getNumberOfPackagedProducts
    * @date         03/28/2017
    * @description  This method retuns number of packaged products for given pricebook. 
    * @param        Id
    * @return       Integer
    */
    public static Integer getNumberOfPackagedProducts(Id priceBook) {
        EP_GeneralUtility.Log('public','EP_GeneralUtility','getNumberOfPackagedProducts');
        //Note: This can be moved to PricebookEntry strategy/domain in future
        EP_PriceBookEntryMapper priceBookEntryMapObj = new EP_PriceBookEntryMapper();
        return priceBookEntryMapObj.getNumberOfPackagedProducts(priceBook);
    }
    
    /**
    * @author       Accenture
    * @name         formatNumber
    * @date         03/28/2017
    * @description  This method retuns the formatted value. 
    * @param        String
    * @return       String
    */
    public static string formatNumber(string value) {
        return value.replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK);
    }
    
    /**
    * @author       Accenture
    * @name         formatDate
    * @date         03/28/2017
    * @description  This method retuns the formatted value of Date. 
    * @param        String
    * @return       Date
    */
    public static Date formatDate(string dateTimeStr) {
            list<string> dateList = new  list<string>();
            if(string.isBlank(dateTimeStr)) {
                return null;
            }
            string dateStr = dateTimeStr.split(EP_Common_Constant.SPACE_STRING)[0];
            Integer day,month,year;
            dateList = dateStr.split(EP_Common_Constant.SPLIT);
            day = Integer.valueOf(dateList[0]);
            month = Integer.valueOf(dateList[1]);
            year = Integer.valueOf(dateList[2]);
            Date dTime =  Date.newInstance(year,month,day);
            return dTime;
    }
    
    /**
    * @author       Accenture
    * @name         formatNAVDate
    * @date         03/28/2017
    * @description  This method retuns the formatted value of Date. 
    * @param        String
    * @return       Date
    */
    public static Date formatNAVDate(string dateTimeStr) {
        if(string.isBlank(dateTimeStr)) {
            return null;
        }
        return Date.ValueOf(dateTimeStr.replace(EP_Common_Constant.DATE_TIME_SEPARATOR,EP_Common_Constant.SPACE_STRING));
    }
    
    /**
    * @author       Accenture
    * @name         createMessageGeneratorRecord
    * @date         03/28/2017
    * @description  This method retuns the formatted value of Message Name. 
    * @param        NA
    * @return       String
    */
    public static string createMessageGeneratorRecord(){
        EP_Message_Id_Generator__c messageGenerator = new EP_Message_Id_Generator__c();
        insert messageGenerator;
        EP_Message_Id_Generator__c msgGenerator = [Select id, Name from EP_Message_Id_Generator__c where Id =: messageGenerator.id Limit :EP_COMMON_CONSTANT.ONE];
        return msgGenerator.Name;
    }
    
    /**
    * @author       Accenture
    * @name         isDateTimeValid
    * @date         03/28/2017
    * @description  This method validates datetime. 
    * @param        String
    * @return       boolean
    */
    public static boolean isDateTimeValid(String dtime){
        return (!String.isBlank(dtime) && dtime.length() == EP_Common_Constant.NUM_TWELVE);
    }
    
    //L4 - #45514 and #45515 code changes start
    /**
    * @author       Accenture
    * @name         stringToBoolenConversion
    * @date         03/28/2017
    * @description  This method converts string into boolean values. 
    * @param        String
    * @return       boolean
    */
    public static boolean stringToBoolenConversion(String strVal){
      return (String.isNotBlank(strVal) && (EP_COMMON_Constant.YES.equalsIgnoreCase(strVal) || EP_COMMON_Constant.STRING_TRUE.equalsIgnoreCase(strVal))) ? true : false;
    }
    //L4 - #45514 and #45515 code changes End
    
}