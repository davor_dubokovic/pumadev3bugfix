/* ================================================
 * @Class Name : EP_CRM_UserAuthorization 
 * @author : TCS
 * @Purpose: This virual class is used to provide all user related information.
 * @created date: 15/11/2017
 ================================================*/
public with sharing virtual class EP_CRM_UserAuthorization {
		
    public User loggedInUserObj {get; set;}
    
    // Returns true/false based on logged in user role presence
    public Boolean getUserHasRole() {
    	if(loggedInUserObj.UserRole != null) {
    		return true;
    	}
    	return false;
    }
    
    // Returns logged in user role name
    public String getUserRoleName() {
    	if(getUserHasRole()) {
    		return loggedInUserObj.UserRole.Name;
    	}
    	return null;
    }
    
    // Returns logged in user role Id
    public Id getRoleId() {
    	if(getUserHasRole()) {
    		return loggedInUserObj.UserRoleId;
    	}
    	return null;
    }
    
    // Return logged in user name
    public String getUserName() {
    	return loggedInUserObj.Name;
    }
    
    // Returns logged in user GEO1 value
    public String getGeo1() {
    	return loggedInUserObj.EP_CRM_Geo1__c;
    }
    
    // Returns logged in user GEO2 value
    public String getGeo2() {
    	return loggedInUserObj.EP_CRM_Geo2__c;
    }
    
    // Returns logged in user role type like (Sales, Manager, Executive Sales, Executive Sales Manager, Country Executive, Regional Executive and Global Executive)
    public String getRoleType() {
    	String roleType = EP_CRM_Constants.BLANK; 
    	if(String.isNotBlank(getUserRoleName())) {
	    	if(getUserRoleName().contains(EP_CRM_Constants.GLOBAL_EXECUTIVE)) {
	    		roleType = EP_CRM_Constants.GLOBAL_EXECUTIVE;
	    	} else if(getUserRoleName().contains(EP_CRM_Constants.EXECUTIVE_SALES_MANAGER)) {
	    		roleType = EP_CRM_Constants.EXECUTIVE_SALES_MANAGER;
	    	} else if(getUserRoleName().contains(EP_CRM_Constants.EXECUTIVE_MANAGER)) {
	    		roleType = EP_CRM_Constants.EXECUTIVE_MANAGER;
	    	} else if(getUserRoleName().contains(EP_CRM_Constants.EXECUTIVE)) {
	    		if(getUserRoleName().startsWithIgnoreCase(EP_CRM_Constants.MEAP) 
	    			|| getUserRoleName().startsWithIgnoreCase(EP_CRM_Constants.US) 
	    			|| getUserRoleName().startsWithIgnoreCase(EP_CRM_Constants.AFRICA)) {
	    			roleType = EP_CRM_Constants.REGIONAL_EXECUTIVE;
	    		} else {
	    			roleType = EP_CRM_Constants.COUNTRY_EXECUTIVE;
	    		}
	    	} else if(getUserRoleName().contains(EP_CRM_Constants.SALES_MANAGER)) {
	    		roleType = EP_CRM_Constants.SALES_MANAGER;
	    	} else if(getUserRoleName().contains(EP_CRM_Constants.SALES_USER)) {
	    		roleType = EP_CRM_Constants.SALES_USER;
	    	} else {
	    		roleType = EP_CRM_Constants.BLANK;
	    	}
    	}
    	return roleType;
    }
    
    // Returns report header value based on logged in user's role
    public String getReportHeader() {
    	String headerLabel = EP_CRM_Constants.BLANK;
    	if(getRoleType() == EP_CRM_Constants.GLOBAL_EXECUTIVE) {
    		headerLabel = EP_CRM_Constants.GLOBAL_REPORT_HEADER;
    	} else if(getRoleType() == EP_CRM_Constants.REGIONAL_EXECUTIVE) {
    		headerLabel = EP_CRM_Constants.REGIONAL_REPORT_HEADER;
    	} else if(getRoleType() == EP_CRM_Constants.COUNTRY_EXECUTIVE) {
    		headerLabel = EP_CRM_Constants.COUNTRY_REPORT_HEADER;
    	} else if(getRoleType() == EP_CRM_Constants.EXECUTIVE_SALES_MANAGER) {
    		headerLabel = EP_CRM_Constants.EXECUTIVE_SALES_MANAGER_REPORT_HEADER;
    	} else if(getRoleType() == EP_CRM_Constants.EXECUTIVE_MANAGER) {
    		headerLabel = EP_CRM_Constants.EXECUTIVE_MANAGER_REPORT_HEADER;
    	} else if(getRoleType() == EP_CRM_Constants.SALES_MANAGER) {
    		headerLabel = EP_CRM_Constants.MANAGER_REPORT_HEADER;
    	} else if(getRoleType() == EP_CRM_Constants.SALES_USER) {
    		headerLabel = EP_CRM_Constants.SALES_REPORT_HEADER;
    	} else {
    		headerLabel = EP_CRM_Constants.NA;
    	}
    	return headerLabel;
    }
    
    // Returns true/false based on logged in user's role name suffix as segment name
    public Boolean getRoleSegmentType() {
    	if(String.isBlank(getRoleSegmentValue())) {
    		return false;
    	}
    	return true;
    }
    
    // Returns segment name based on logged in user's role name suffix
    public String getRoleSegmentValue() {
    	// Get all customer segement names and check with role name
    	if(String.isNotBlank(getUserRoleName())) {
	    	for(SelectOption segment: EP_CRM_ReportUtility.getPickValues(new Opportunity(), 'EP_CRM_Customer_Segment__c', null)) {
	    		if(getUserRoleName().endsWithIgnoreCase(segment.getValue())) {
	    			return segment.getValue();
	    		}
	    		if(segment.getValue() == EP_CRM_Constants.HIGH_STREET && getUserRoleName().endsWithIgnoreCase(EP_CRM_Constants.HIGHSTREET)) {
	    			return EP_CRM_Constants.HIGH_STREET;
	    		}
	    	}
    	}
    	return EP_CRM_Constants.BLANK;
    }
    
    // Returns true if logged in user is from America region
    public Boolean getAmericaUser() {
    	if(getGeo1() == EP_CRM_Constants.AMERICAS) {
    		return true;
    	}
    	return false;
    }
    
    // Returns true if logged in user is from Africa region
    public Boolean getAfricaUser() {
    	if(getGeo1() == EP_CRM_Constants.AFRICA) {
    		return true;
    	}
    	return false;
    }
    
    // Returns true if logged in user is from Australia region
    public Boolean getAustraliaUser() {
    	if(getGeo1() == EP_CRM_Constants.MEAP) {
    		return true;
    	}
    	return false;
    }
    
    // Returns true if logged in user is sales user
    public Boolean getSalesUser() {
    	if(getRoleType() == EP_CRM_Constants.SALES_USER) {
    		return true;
    	}
    	return false;
    }
    
    // Returns true if logged in user is manager user
    public Boolean getManagerUser() {
    	if(getRoleType() == EP_CRM_Constants.SALES_MANAGER) {
    		return true;
    	}
    	return false;
    }
    
    // Returns true if logged in user is country executive user
    public Boolean getCountryExecutiveUser() {
    	if(getRoleType() == EP_CRM_Constants.COUNTRY_EXECUTIVE) {
    		return true;
    	}
    	return false;
    }
    
    // Returns true if logged in user is regional executive user
    public Boolean getRegionalExecutiveUser() {
    	if(getRoleType() == EP_CRM_Constants.REGIONAL_EXECUTIVE) {
    		return true;
    	}
    	return false;
    }
    
    // Returns true if logged in user is executive manager user
    public Boolean getExecutiveManagerUser() {
    	if(getRoleType() == EP_CRM_Constants.EXECUTIVE_MANAGER) {
    		return true;
    	}
    	return false;
    }
    
    // Returns true if logged in user is executive sales manager user
    public Boolean getExecutiveSalesManagerUser() {
    	if(getRoleType() == EP_CRM_Constants.EXECUTIVE_SALES_MANAGER) {
    		return true;
    	}
    	return false;
    }
    
    // Returns true if logged in user is global user
    public Boolean getGlobalUser() {
    	if(getRoleType() == EP_CRM_Constants.GLOBAL_EXECUTIVE) {
    		return true;
    	}
    	return false;
    }
}