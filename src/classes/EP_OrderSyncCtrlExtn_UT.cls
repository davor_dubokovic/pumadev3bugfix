@isTest
public class EP_OrderSyncCtrlExtn_UT{
    private static string SFDC_TO_WINDMS_ORDER_SYNC = 'SFDC_TO_WINDMS_ORDER_SYNC';
    @testSetup static void init() {
        List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
    }

    static testMethod void setHeader_test() {
        csord__Order__c ord =EP_TestDataUtility.getCurrentOrder();
        PageReference pageRef = Page.EP_OrderConfirmationWithOPMSHeader;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGE_TYPE,'SEND_ORDER_CONFIRMATION_REQUEST');
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.SOURCE_COMPANY,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE , '1234');
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , ord.Id);
        EP_OrderSyncCtrlExtn localObj = new EP_OrderSyncCtrlExtn(sc);
        Test.startTest(); 
        localObj.setHeader();
        Test.stopTest();
        System.AssertEquals(true,localObj.headerObj.MsgID.equalsIgnoreCase(EP_Common_Constant.TEMPSTRINGWITHHYPHEN));
        System.AssertEquals(true,localObj.headerObj.SourceCompany.equalsIgnoreCase(EP_Common_Constant.TEMPSTRINGWITHHYPHEN));
    }
    static testMethod void setPayload_test() {
        csord__Order__c ord =EP_TestDataUtility.getCurrentOrder();
        PageReference pageRef = Page.EP_OrderConfirmationWithOPMSHeader;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGE_TYPE,'SEND_ORDER_CONFIRMATION_REQUEST');
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.SOURCE_COMPANY,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE , '1234');
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , ord.Id);
        EP_OrderSyncCtrlExtn localObj = new EP_OrderSyncCtrlExtn(sc);
        Test.startTest();
        localObj.setPayload();
        Test.stopTest();
        System.AssertEquals(true,localObj.encodedPayload != null);
    }
    
    
    public static testMethod void checkPageAccess_test() {//vs: updated
       csord__Order__c ord =EP_TestDataUtility.getCurrentOrder();
        PageReference pageRef = Page.EP_OrderConfirmationWithOPMSHeader;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGE_TYPE,'SEND_ORDER_CONFIRMATION_REQUEST');
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.SOURCE_COMPANY,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE , '1234');
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , ord.Id);
        EP_OrderSyncCtrlExtn localObj = new EP_OrderSyncCtrlExtn(sc);
        Test.startTest();
        PageReference result = localObj.checkPageAccess();
        Test.stopTest();
        System.AssertEquals(true,result == NULL);
    }
    
}
