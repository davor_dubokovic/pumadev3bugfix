/* 
@Author <Accenture>
@name <EP_IntegrationRecordTriggerHelper>
@CreateDate <27/10/2015>
@Description <This class contains buiseness logic of from Integration Record trigger> 
@Version <1.0>
*/
public without sharing class EP_IntegrationRecordTriggerHelper {

    //Map to store the valuues from custom setting
    private static Map<String,EP_Integration_Status_Update__c> mapIntegrationCS = EP_Integration_Status_Update__c.getAll();
    private static final String CLASS_NAME = 'EP_IntegrationRecordTriggerHelper';
    private static final String UPDATE_SENT_MTD = 'updateSentStatus';
    private static final String UPDATE_SYNC_MTD = 'updateSyncStatus';
    private static final String GET_REC_TO_UPDATE = 'getRecordstoUpdate';
    private static final String GET_STATUS = 'getStatus';
    private static final String RESEND_MESSAGE = 'resendMessage';
    private static final String UPDATE_ERR_TYPE = 'updateIntegrationErrorType';
    private static final String ORDERS = 'Orders';
    private Static final String osn_MsgIdNotation = 'OSN';
    //Jyotsna 1.178 -- start
    private static Map<Id,String> mapOrderStatus = new Map<Id,String>();
    private static Set<Id> setOrderIds = new Set<Id>();
    // 1.178--end
    private static Set<Id> setNonVmiOrderIds = new Set<Id>();
	/*L4_45352_Start*/
	private static final String CPE = 'CPE';
	private static final String CCN = 'CCN';
	private static final String CCL = 'CCL';
	private static final String ESN = 'ESN';
	private static final String CEL = 'CEL';
	private static final String CEN = 'CEN';
	/*L4_45352_End*/
    /**
    * @author <Accenture>
    * @date <27/10/2015>
    * @name <updateSentStatus>
    * @description <This method updates the data object record on insertion of integration record with 'SENT' status>
    * @param List<EP_IntegrationRecord__c>
    * @return void
    */
    public static void updateSentStatus(List<EP_IntegrationRecord__c> newIntegrationRecords){
        List<sObject> listSObjRecToUpdate = new List<sObject>();
        Set<sObject> setSObjRecToUpdate = new Set<sObject>();
        
        //Loop to create a collection of records to be updated in the process.
        for(EP_IntegrationRecord__c intRecord : newIntegrationRecords){

            EP_DebugLogger.printDebug('intRecord.EP_Object_Type__c: '+intRecord.EP_Object_Type__c);
            EP_DebugLogger.printDebug('mapIntegrationCS.get(intRecord.EP_Object_Type__c).EP_API_Name__c: ' + mapIntegrationCS.get(intRecord.EP_Object_Type__c).EP_API_Name__c);
            
            Schema.SObjectType schemaSObjType = Schema.getGlobalDescribe().get(mapIntegrationCS.get(intRecord.EP_Object_Type__c).EP_API_Name__c);
            EP_DebugLogger.printDebug('the sobjectype is: '+ schemaSObjType);
            
            SObject sObj = schemaSObjType.newSObject();
            sObj.put(EP_Common_Constant.ID,intRecord.EP_Object_ID__c);
            sObj.put(mapIntegrationCS.get(intRecord.EP_Object_Type__c).EP_Status__c,getStatus(intRecord.EP_Status__c));
            //listSObjRecToUpdate.add(sObj);
            setSObjRecToUpdate.add(sObj);
        }
        if(!setSObjRecToUpdate.isEmpty()){
        	//Start Code changes for - #59186
            try{
				EP_IntegrationUtil.ISERRORSYNC = true;
                system.debug('##updateSentStatus.ISERRORSYNC##' + EP_IntegrationUtil.ISERRORSYNC);
                listSObjRecToUpdate.addAll(setSObjRecToUpdate);
                database.update(listSObjRecToUpdate,False);
            }catch(Exception E){
                EP_LoggingService.logHandledException (E, EP_Common_Constant.EPUMA, UPDATE_SENT_MTD, CLASS_NAME, ApexPages.Severity.ERROR);
            }
            //Start Code changes for - #59186
        }
    }

    /**
    * @author <Accenture>
    * @date <27/10/2015>
    * @name <updateSyncStatus>
    * @description <This method updates the data object record on insertion of integration record.>
    * @param Map<Id,EP_IntegrationRecord__c>
    * @return void
    */
    public static void updateSyncStatus(Map<Id,EP_IntegrationRecord__c> integrationMap){
        List<sObject> listSObjRecToUpdate = new List<sObject>();
        String objType = EP_Common_Constant.BLANK;
        String status= EP_Common_Constant.BLANK;
        String msgid= EP_Common_Constant.BLANK;
        String creditStatus = EP_Common_Constant.BLANK;//JYotsna 1.178 
        String orderStatus = EP_Common_Constant.BLANK;//JYotsna 1.178 
        Map<Id,String> mapRecordsToUpdate = getRecordsToUpdate(integrationMap);
        /**SOBJECT LIST CHANGE** -START**/
        List<Id>lIntRecId = new List<Id>();
        
        if(!mapRecordsToUpdate.isEmpty()){
            lIntRecId.addAll(mapRecordsToUpdate.keyset());
            lIntRecId.sort();
        }
        //EP_DebugLogger.printDebug('===mapRecordsToUpdate ===='+mapRecordsToUpdate +integrationMap);
        
        if(!mapRecordsToUpdate.isEmpty()){
            for(Id recId : lIntRecId ){
                /**SOBJECT LIST CHANGE** -END**/        
                objType = mapRecordsToUpdate.get(recId).split(EP_Common_Constant.COLON_WITH_SPACE)[0];
                status =  mapRecordsToUpdate.get(recId).split(EP_Common_Constant.COLON_WITH_SPACE)[1];
                msgid =  mapRecordsToUpdate.get(recId).split(EP_Common_Constant.COLON_WITH_SPACE)[2];
                //EP_DebugLogger.printDebug('====='+objType +'====='+status);
                Schema.SObjectType sObjType = Schema.getGlobalDescribe().get(mapIntegrationCS.get(objType).EP_API_Name__c);
                SObject sObjInstance = sObjType.newSObject();
                sObjInstance.put(EP_Common_Constant.ID,recId);
                sObjInstance.put(mapIntegrationCS.get(objType).EP_Status__c,status);
                //Added code to invoke state machine
                system.debug('****** Objtype ' + objType + '  Status is ' + status + '** sObjType**' + sObjType);
                 
                //L4_45352_Start
				system.debug('****** msgid ' + msgid);
                if(EP_Common_Constant.SYNC_STATUS.equalsIgnoreCase(status)){
                	if(objType.equalsIgnoreCase(EP_Common_Constant.ACCOUNTS)){
	                    if(isPricingEngineAck(msgid)){
	                        sObjInstance.put('EP_Synced_PE__c',true);
	                    }
	                    else if(isNavAck(msgid)){
	                        sObjInstance.put('EP_Synced_NAV__c',true);
	                    }
	                    else if(isWindmsAck(msgid)){
	                        sObjInstance.put('EP_Synced_WINDMS__c',true);
	                    }
                	}
                }
                else{                    
                    EP_IntegrationUtil.ISERRORSYNC = true;
                    system.debug('##P_IntegrationUtil.ISERRORSYNC##' + EP_IntegrationUtil.ISERRORSYNC);
                }
                //L4_45352_End
                if(objType.equalsIgnoreCase(EP_Common_Constant.CSOrders) && (EP_Common_Constant.ERROR_SYNC_STATUS.equalsIgnoreCase(status)|| EP_Common_Constant.SYNC_STATUS.equalsIgnoreCase(status))){
                    EP_OrderMapper orderMapper = new EP_OrderMapper();
                    csord__Order__c objOrder = orderMapper.getCSRecordById(recId);
                    system.debug('****** integrationMap ' + integrationMap);
                    system.debug('****** Record id ' + recId);
                    system.debug('integrationMap.containsKey(recId) ' + integrationMap.containsKey(recId));
                    // this will never work
                    if(integrationMap.containsKey(recId)) {
                        system.debug('integrationMap.get(recId).EP_Message_ID__c' + integrationMap.get(recId).EP_Message_ID__c);
                        if(integrationMap.get(recId).EP_Message_ID__c.contains(EP_Common_Constant.PER) 
                           || integrationMap.get(recId).EP_Message_ID__c.contains('CSL')) {
                           system.debug('**integrationMap**' + integrationMap.get(recId).EP_Message_ID__c);
                           continue;
                        }
                    }
                    
                    if(!(msgid.contains(EP_Common_Constant.PER) || msgid.contains(EP_Common_Constant.CREDIT_SYNC_LS))){
                        //system.debug('**integrationMap-1**' + integrationMap.get(recId).EP_Message_ID__c);
                        creditStatus = mapOrderStatus.containsKey(recId)?mapOrderStatus.get(recId):EP_Common_Constant.OK;
                        system.debug('**creditStatus**' + creditStatus);
                        objOrder.EP_Credit_Status__c = creditStatus;
                        if(creditStatus.equals(EP_Common_Constant.OK)){
                            system.debug('Inside if of credit status is OK');
                            objOrder.EP_Order_credit_status__c = EP_Common_Constant.Credit_Okay;
                        }
                        if(msgid.contains(EP_Common_Constant.OSN_STRING)){
                            objOrder.EP_Sync_with_NAV__c = true;
                        }
                        //EP_OrderDomainObject orderDomain = new EP_OrderDomainObject(recId);
                       /* EP_OrderDomainObject orderDomain = new EP_OrderDomainObject(objOrder);
                        EP_OrderEvent orderEvent = new EP_OrderEvent(orderDomain.getStatus()+ EP_OrderConstant.ACKNOWLEDGED);
                        
                        EP_OrderService orderService = new EP_OrderService(orderDomain);
                        if(orderService.setOrderStatus(orderEvent)){
                            //update order;
                            csord__Order__c orderObj  = orderDomain.getOrder();
                            update orderObj;
                            orderService.doPostStatusChangeActions();
                        }*/
                    }
                }
                //1.178 -- end
                listSObjRecToUpdate.add(sObjInstance);
            }
            //EP_DebugLogger.printDebug('----'+mapOrderStatus+'----'+setOrderIds);
            //Jyotsna  1.178 -- start
            if(!setOrderIds.isEmpty()){
                
                //Replace this method with the new VMI Check In Process out bound method.
                //EP_OrderTriggerHelper.orderCreditCheck(setOrderIds,mapOrderStatus,true);
            }
            //1.178 -- end
            //Defect 57617 START
            if(!listSObjRecToUpdate.isEmpty()){
                
                List<Database.SaveResult> saveResult = database.update(listSObjRecToUpdate, false);
                for(Database.SaveResult result : saveResult) {
                    if(!result.isSuccess()) {
                        for(Database.Error e : result.getErrors()) {
                            EP_LoggingService.logHandledError (EP_Common_Constant.dateException, e.getMessage(), EP_Common_Constant.EPUMA, UPDATE_SYNC_MTD, CLASS_NAME, ApexPages.Severity.ERROR);
                        }
                    }
                }
            }
            //Defect 57617 END
        }
    }

    /**
    * @author <Accenture>
    * @date <27/10/2015>
    * @name <getRecordsToUpdate>
    * @description <This method handles the logic of deciding which data object records needs to be updated>
    * @param Map<Id,EP_IntegrationRecord__c>
    * @return Map<Id,String>
    */
    public static Map<Id,String> getRecordsToUpdate( Map<Id,EP_IntegrationRecord__c> mapNewIntegrationRecords ){
        Set<String> setTxId = new Set<String>();
        Set<String> setObjId = new Set<String>();
        Set<String> setTargets = new Set<String>(); 
        String status = EP_Common_Constant.BLANK;
        try{
            //Loop to get the transactionIds and Targets for fetching records to compare with the updated record. 
            for( EP_IntegrationRecord__c intRec : mapNewIntegrationRecords.values() ){
                setObjId.add(intRec.EP_Object_ID__c);  
                if( intRec.EP_Transaction_ID__c != null){
                    setTxId.add(intRec.EP_Transaction_ID__c);
                }
                else {
                    setTargets.add( intRec.EP_Target__c);
                }
            }
            //Collection to store the records which needs to be updated
            Map<Id,String> recToUpdate = new Map<Id,String>();
            List<EP_IntegrationRecord__c> dataSet = new List<EP_IntegrationRecord__c>();
            Integer nRows = EP_Common_Util.getQueryLimit();
            mapNewIntegrationRecords = new map<Id,EP_IntegrationRecord__c>([Select id
            ,name
            ,EP_Status__c
            ,EP_Transaction_ID__c
            ,EP_IsLatest__c
            ,EP_Object_Type__c
            ,EP_Object_ID__c
            ,EP_Target__c
            ,EP_DT_SENT__c
            ,EP_Message_ID__c
            ,EP_Error_Description__c 
            from EP_IntegrationRecord__c 
            where Id IN:mapNewIntegrationRecords.keySet()
            LIMIT : nRows
            FOR UPDATE]);
            //Querying the records as per the scenario...i.e with or without transaction id
            if(!setTxId.isEmpty()){
                nRows = EP_Common_Util.getQueryLimit();
                dataSet = [ Select id
                ,name
                ,EP_Status__c
                ,EP_Transaction_ID__c
                ,EP_IsLatest__c
                ,EP_Object_Type__c
                ,EP_Object_ID__c
                ,EP_Target__c
                ,EP_DT_SENT__c
                ,EP_Error_Description__c 
                ,EP_Message_ID__c
                from EP_IntegrationRecord__c 
                where EP_Transaction_ID__c IN:setTxId 
                and EP_Object_ID__c IN:setObjId 
                and EP_IsLatest__c = true
                and Id Not in:mapNewIntegrationRecords.keySet() 
                LIMIT : nRows
                FOR UPDATE];
            }
            //Outer loop iterating over the records which got updated from inbound integration handler
            for(EP_IntegrationRecord__c eRec : mapNewIntegrationRecords.values()){
                //Jyotsna updated as per Thread 1.178 -- start
                if(eRec.EP_Status__c.contains(EP_Common_Constant.SYNC_STATUS.toUpperCase()) && eRec.EP_Object_Type__c.equalsIgnoreCase(EP_Common_Constant.CSOrders) && (eRec.EP_Message_ID__c.contains(EP_Common_Constant.OCN) || eRec.EP_Message_ID__c.contains(osn_MsgIdNotation ))){
                    //new changes Start
                    if(eRec.EP_Message_ID__c.contains(EP_Common_Constant.OCN)){
                        setOrderIds.add(eRec.EP_Object_ID__c);
                    }
                    else if(eRec.EP_Message_ID__c.contains(osn_MsgIdNotation )){
                        setNonVmiOrderIds.add(eRec.EP_Object_ID__c);
                    }
                }
                //new changes Start
                // Thread 1.178 -- end
                if(!dataSet.isEmpty()){  
                    //Inner loop on the data set used to compare records with the updated records
                    //If the status is sync,data object record status will be 'sync'

                    boolean found = false;
                    for(EP_IntegrationRecord__c intRec : dataSet){
                        // EP_DebugLogger.printDebug('************'+eRec+'&&&&&&&&&&&&&'+intRec);

                        if(intRec.EP_Object_ID__c == eRec.EP_Object_ID__c
                                && intRec.EP_Transaction_ID__c == eRec.EP_Transaction_ID__c){
                            found = true;   
                            //If the status is either error-sync or error-sent ,data object record status will be failure
                            if(intRec.EP_Status__c.contains(EP_Common_Constant.ERROR_SYNC_SENT_STATUS.toUpperCase())
                                    ||eRec.EP_Status__c.contains(EP_Common_Constant.ERROR_SYNC_SENT_STATUS.toUpperCase() )){  
                                //Jyotsna updated as per Thread 1.178 -- start
                                //Sandeep:  Please correct this condition. It will never be true
                                if(EP_Common_Constant.SYNC_STATUS.toUpperCase().contains(intRec.EP_Status__c) 
                                        && EP_Common_Constant.SYNC_STATUS.toUpperCase().contains(eRec.EP_Status__c)
                                        && ORDERS.equalsIgnoreCase(intRec.EP_Object_Type__c)
                                        ){//&&intRec.EP_Message_ID__c.contains(EP_Common_Constant.OCN)){ new changes Start 
                                    status = setCreditStatus(intRec.EP_Error_Description__c);
                                    if(status<>EP_Common_Constant.BLANK){
                                        mapOrderStatus.put(intRec.EP_Object_ID__c,status);
                                    }
                                    else{
                                        if(intRec.EP_Message_ID__c.contains(EP_Common_Constant.OCN)){
                                            setOrderIds.remove(intRec.EP_Object_ID__c);
                                        }
                                        else if(intRec.EP_Message_ID__c.contains(osn_MsgIdNotation )){
                                            setNonVmiOrderIds.remove(intRec.EP_Object_ID__c);
                                        }//new changes end                                              
                                    }
                                    //1.178 --end
                                }                 
                                recToUpdate.put(eRec.EP_Object_ID__c,eRec.EP_Object_Type__c+
                                EP_Common_Constant.COLON_WITH_SPACE + EP_Common_Constant.FAILURE.toUpperCase() +
                                EP_Common_Constant.COLON_WITH_SPACE  + eRec.EP_Message_ID__c);

                            }
                            //CHANGE STATUS TO ACKNLOWDGED
                            else if(EP_Common_Constant.ACKNWLDGD.toUpperCase().equalsIgnoreCase(eRec.EP_Status__c)
                                    && EP_Common_Constant.SYNC_STATUS.toUpperCase().equalsIgnoreCase(intRec.EP_Status__c)
                                    || (EP_Common_Constant.ACKNWLDGD.toUpperCase().equalsIgnoreCase(intRec.EP_Status__c)
                                        && EP_Common_Constant.SYNC_STATUS.toUpperCase().equalsIgnoreCase(eRec.EP_Status__c))){

                                recToUpdate.put(eRec.EP_Object_ID__c,eRec.EP_Object_Type__c+
                                EP_Common_Constant.COLON_WITH_SPACE+EP_Common_Constant.ACKNWLDGD.toUpperCase()  +
                                EP_Common_Constant.COLON_WITH_SPACE  + eRec.EP_Message_ID__c);
                            }
                            else {
                                if(intRec.EP_Status__c.contains(EP_Common_Constant.SYNC_STATUS.toUpperCase()) 
                                        && eRec.EP_Status__c.contains(EP_Common_Constant.SYNC_STATUS.toUpperCase())){
                                    recToUpdate.put(eRec.EP_Object_ID__c,eRec.EP_Object_Type__c +
                                    EP_Common_Constant.COLON_WITH_SPACE+EP_Common_Constant.SYNC_STATUS.toUpperCase() +
                                    EP_Common_Constant.COLON_WITH_SPACE  + eRec.EP_Message_ID__c);
                                }
                            }
                            /*else{
                                    recToUpdate.put(eRec.EP_Object_ID__c, eRec.EP_Object_Type__c + EP_Common_Constant.COLON_WITH_SPACE + getStatus(EP_Common_Constant.SENT_STATUS));
                                }*/
                            break;
                        }
                    }
                    if(!found && !recToUpdate.containsKey(eRec.EP_Object_ID__c)){
                        recToUpdate.put(eRec.EP_Object_ID__c,eRec.EP_Object_Type__c+
                        EP_Common_Constant.COLON_WITH_SPACE+getStatus(eRec.EP_Status__c) +
                        EP_Common_Constant.COLON_WITH_SPACE  + eRec.EP_Message_ID__c);
                    }
                }
                else{//Jyotsna updated as per Thread 1.178 -- start
                    if(eRec.EP_Status__c.contains(EP_Common_Constant.ERROR_SYNC_STATUS.toUpperCase()) 
                            && eRec.EP_Object_Type__c.equalsIgnoreCase(EP_Common_Constant.CSOrders)){//&& eRec.EP_Message_ID__c.contains(EP_Common_Constant.OCN)){
                        status = setCreditStatus(eRec.EP_Error_Description__c);
                        if(status<>EP_Common_Constant.BLANK){
                            mapOrderStatus.put(eRec.EP_Object_ID__c,status);
                        }
                        else{
                            if(eRec.EP_Message_ID__c.contains(EP_Common_Constant.OCN)){
                                setOrderIds.remove(eRec.EP_Object_ID__c);
                            }   
                            else if(eRec.EP_Message_ID__c.contains(osn_MsgIdNotation )){
                                setNonVmiOrderIds.remove(eRec.EP_Object_ID__c);
                            }
                        }//1.178 --end
                    }                 
                    //the status of the data record will be same as the status of the updated record.
                    recToUpdate.put(eRec.EP_Object_ID__c,eRec.EP_Object_Type__c+
                    EP_Common_Constant.COLON_WITH_SPACE+getStatus(eRec.EP_Status__c) +
                    EP_Common_Constant.COLON_WITH_SPACE  + eRec.EP_Message_ID__c);
                }
            }
            //EP_DebugLogger.printDebug('************'+recToUpdate);
            return recToUpdate;
        }catch(Exception e){
            EP_LoggingService.logHandledException (E, EP_Common_Constant.EPUMA, GET_REC_TO_UPDATE, CLASS_NAME, ApexPages.Severity.ERROR);
        }
        return null;        
    }

    /**
    * @author <Accenture>
    * @date <27/10/2015>
    * @name <getStatus>
    * @description <This method is used to get the appropriate status that needs to be updated on the data object record.>
    * @param String
    * @return String
    */
    public static string getStatus(String status){
        String statusFinal = null;
        try{
            if(status.equalsIgnoreCase(EP_Common_Constant.ERROR_SYNC_STATUS)){
                statusFinal = EP_Common_Constant.FAILURE.toUpperCase();
            }
            else if(status.equalsIgnoreCase(EP_Common_Constant.ERROR_SENT_STATUS)){
                statusFinal = EP_Common_Constant.ERROR_SENT_STATUS.toUpperCase();
            }
            else if(status.equalsIgnoreCase(EP_Common_Constant.SENT_STATUS)){
                statusFinal = EP_Common_Constant.SENT_STATUS.toUpperCase();
            }
            else if ( status.equalsIgnoreCase( EP_Common_Constant.ACKNWLDGD ) ||  status.equalsIgnoreCase( EP_Common_Constant.ERR_ACKNWLDGD )){
                statusFinal = status.toUpperCase();
            }
            else{
                statusFinal = EP_Common_Constant.SYNC_STATUS.toUpperCase(); 
            }            
        }catch(Exception e){
            EP_LoggingService.logHandledException (E, EP_Common_Constant.EPUMA, GET_STATUS, CLASS_NAME, ApexPages.Severity.ERROR);
        }
        return statusFinal;
    }

    /**
    * @author <Jai Singh>
    * @date <27/10/2015>
    * @name <resendMessage>
    * @description <This method is used to update resend flag and call attept the callout.>
    * @param List<EP_IntegrationRecord__c>, Set<String> 
    * @return void
    */
    public static void resendMessage(List<EP_IntegrationRecord__c> listIntegrationRecords, Set<String> setUniqueMessageIds){ 
        database.update(listIntegrationRecords,False);
        //Start Code changes for - #59186
        //call the callout function here
        for( String messageId : setUniqueMessageIds ){
            ID jobID = fetchJobId(messageId);
        }
        //End Code changes for - #59186
    }

    /**
    * @author <Accenture>
    * @date <27/10/2015>
    * @name <fetchJobId>
    * @description <This message return Inqueue Job Id based in Msg Id>
    * @param String
    * @return System.enqueueJob
    */
    private static Id fetchJobId(string messageId){
        return System.enqueueJob(new EP_GenericRetryOI(messageId));
    }

    /**
    * @author <Accenture>
    * @date <27/10/2015>
    * @name <setCreditStatus>
    * @description <setCreditStatus>
    * @param String
    * @return String
    */
    private static String setCreditStatus(String errMsg){
        String creditStatus = EP_Common_Constant.BLANK;
        if(errMsg == null) return creditStatus;
        
        if(errMsg.contains(Label.EP_Credit_Fail_Overdue_Invoice_Msg)||errMsg.contains(Label.EP_Credit_Fail_Msg)){
            creditStatus = EP_Common_Constant.CREDIT_FAIL;
        }
        else if( errMsg.contains(Label.EP_Overdue_Invoice_Msg) ){
            creditStatus = EP_Common_Constant.OVERDUE_INVOICE;
        }
        else{
            creditStatus = EP_Common_Constant.BLANK; 
        }
        return creditStatus;
    }

    /**
    * @author <Accenture>
    * @date <27/10/2015>
    * @name <updateIntegrationErrorType>
    * @description <updateIntegrationErrorType>
    * @param List<EP_IntegrationRecord__c>
    * @return void
    */
    public static void updateIntegrationErrorType(List<EP_IntegrationRecord__c> newIntegrationRecords){
        //Start Code changes for - #59186
        for( EP_IntegrationRecord__c intRec :  newIntegrationRecords){
            if(EP_Common_Constant.ERROR_SENT_STATUS.equalsIgnoreCase(intRec.EP_Status__c)){
                intRec.EP_Integration_Error_Type__c = EP_Common_Constant.MIDDLEWARE_TRANS_ERROR;
            }
            else if(EP_Common_Constant.ERROR_SYNC_STATUS.equalsIgnoreCase(intRec.EP_Status__c)){
                intRec.EP_Integration_Error_Type__c = EP_Common_Constant.FUCTIONAL_ERROR;
            }
            else if((EP_Common_Constant.ERROR_RECEIVED_STATUS.equalsIgnoreCase(intRec.EP_Status__c)) 
                || (EP_Common_Constant.ERROR_ACKNOWLEDGED_STATUS.equalsIgnoreCase(intRec.EP_Status__c))){
                intRec.EP_Integration_Error_Type__c = EP_Common_Constant.TARGET_TRANS_ERROR;
            }
        }
        //End Code changes for - #59186
    }
    
    //L4-45352-start
    /**
    * @author <Accenture>
    * @date <5/12/2017>
    * @name <isPricingEngineAck>
    * @description <isPricingEngineAck>
    * @param string msgId
    * @return boolean
    */ 
    private static boolean isPricingEngineAck(string msgId){
        return (msgId.contains(CPE));
    }
    /**
    * @author <Accenture>
    * @date <5/12/2017>
    * @name <isNavAck>
    * @description <isNavAck>
    * @param string msgId
    * @return boolean
    */ 
    private static boolean isNavAck(string msgId){
         return (msgId.contains(CCN) || msgId.contains(ESN) || msgId.contains(CEN));
    }
    /**
    * @author <Accenture>
    * @date <5/12/2017>
    * @name <isWindmsAck>
    * @description <isWindmsAck>
    * @param string msgId
    * @return boolean
    */ 
    private static boolean isWindmsAck(string msgId){
         return (msgId.contains(CCL) || msgId.contains(CEL));
    }
    //l4_45352-end    
}