/*
 *  @Author <Accenture>
 *  @Name <EP_StorageLocationASDeactivate>
 *  @CreateDate <20/12/2017>
 *  @Description < Account State for  07-Deactivate Status>
 *  @Version <1.0>
 */
 public  class EP_StorageLocationASDeactivate extends EP_AccountState{

	/**
    * @author           Accenture
    * @name             setAccountDomainObject
    * @date             20/12/2017
    * @description      this is for setting accountDomain
    * @param            EP_AccountDomainObject
    * @return           NA 
    */
    public override void setAccountDomainObject(EP_AccountDomainObject currentAccount){
        EP_GeneralUtility.Log('Public','EP_StorageLocationASDeactivate','setAccountDomainObject');
        super.setAccountDomainObject(currentAccount);
    }
	
	/**
    * @author           Accenture
    * @name             doOnEntry
    * @date             20/12/2017
    * @description      this is for perform action on Entry
    * @param            NA
    * @return           NA 
    */
    public override void doOnEntry(){
        EP_GeneralUtility.Log('Public','EP_StorageLocationASDeactivate','doOnEntry');
        
    }  
	
	/**
    * @author           Accenture
    * @name             doOnExit
    * @date             20/12/2017
    * @description      this is for perform action on exit
    * @param            NA
    * @return           NA 
    */
    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_StorageLocationASDeactivate','doOnExit');
        
    }
    
    /**
    * @author           Accenture
    * @name             doTransition
    * @date             20/12/2017
    * @description      this is for Doing Transition
    * @param            NA
    * @return           boolean 
    */
    public override boolean doTransition(){
        EP_GeneralUtility.Log('Public','EP_StorageLocationASDeactivate','doTransition');
        return super.doTransition();
    }
	
	/**
    * @author           Accenture
    * @name             isInboundTransitionPossible
    * @date             20/12/2017
    * @description      check wether transition is posible or not
    * @param            EP_AccountDomainObject
    * @return           NA 
    */
    public override boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_StorageLocationASDeactivate','isInboundTransitionPossible');
        //Override this with all possible guard conditions that permits the state transition possible
        //For e.g.) Criteria based state "coming from", "Event" that's triggering this change 
        return super.isInboundTransitionPossible();

    } 
}