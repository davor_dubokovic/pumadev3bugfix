/**
@Author          CR Team
@name            EP_TankMapper
@CreateDate      12/21/2016
@Description     This class contains all SOQLs related to EP_Tank__c Object and database operation methods
@Version         1.0
@Novasuite Fixes - comments added
@reference       NA
*/
/*Novasuite Fix Constructor removed*/
/*  Version 1: Modification History  
*************************************************************************************************************
MODIFIED DATE       MODIFIED BY             REASON
-------------------------------------------------------------------------------------------------------------

*************************************************************************************************************
*/
public with sharing class EP_TankMapper{    


/*Defect 78513 start*/
Public set<string> TankToStatus = new set<string>{EP_Common_Constant.NEW_TANK_STATUS,EP_Common_Constant.TANK_STOPPED_STATUS,EP_Common_Constant.OPERATIONAL_TANK_STATUS};
/*Defect 78513 end*/

/**  This method returns Records by Ship To Ids.
*  @name             getRecordsByShipTpIds
*  @param            set<id>
*  @return           list<EP_Tank__c>
*  @throws           NA
*/
public list<EP_Tank__c> getRecordsByShipToIds(set<id> idSet) {
EP_GeneralUtility.Log('Public','EP_TankMapper','getRecordsByShipToIds');
    list<EP_Tank__c> tankList =  new list<EP_Tank__c>();
    for(list<EP_Tank__c> tankobjList : [   SELECT 
                                                id 
                                                , EP_Product__c
                                                , EP_Product__r.Name
                                                , EP_Tank_Alias__c
                                                , EP_Safe_Fill_Level__c
                                                , EP_Tank_Code__c
                                                , EP_Tank_Status__c
                                                , EP_Product__r.EP_Product_Sold_As__c 
                                            FROM EP_Tank__c 
                                            WHERE EP_Ship_To__c IN :idSet 
                                        ]){
        tankList.addAll(tankobjList);
    }                           
    return tankList;
}
 /***L4-45352 start****/
/**  This method returns Records by Ship To Ids.
*  @name             getRecordsByShipTpIds
*  @param            List<id>
*  @return           list<EP_Tank__c>
*  @throws           NA
*/
public list<EP_Tank__c> getRecordsByShipToIdsList(List<Account> ListAccount) {
EP_GeneralUtility.Log('Public','EP_TankMapper','getRecordsByShipToIds');
    list<EP_Tank__c> tankList =  new list<EP_Tank__c>();
    for(list<EP_Tank__c> tankobjList : [   SELECT 
                                                id 
                                                , EP_Product__c
                                                , EP_Product__r.Name
                                                , EP_Tank_Alias__c
                                                , EP_Safe_Fill_Level__c
                                                , EP_Tank_Code__c
                                                , EP_Tank_Status__c
                                                , EP_Product__r.EP_Product_Sold_As__c 
                                            FROM EP_Tank__c 
                                            WHERE EP_Ship_To__c IN :ListAccount
                                        ]){
        tankList.addAll(tankobjList);
    }                           
    return tankList;
}
 /***L4-45352 end****/
/* *  This method returns Tank Records for Ship To where Tank status is Operational and Tank Dip entry Mode 
 *      is set to Portal Entry
*  @name             getOperationalTankRecords
*  @param            set<id>
*  @return           list<EP_Tank__c>
*  @throws           NA
*/
public list<EP_Tank__c> getOperationalRecords(set<id> idSet) {
EP_GeneralUtility.Log('Public','EP_TankMapper','getOperationalRecords');
    list<EP_Tank__c> tankList =  new list<EP_Tank__c>();
    for(list<EP_Tank__c> tankobjList: [SELECT id, 
                                        /* #L4_45352_Start*/ EP_UnitOfMeasure__c /* #L4_45352_END*/, 
                                        EP_Last_Place_Dip_Reading_Date_Time__c, 
                                        EP_Ship_To__r.EP_Ship_To_Current_Date_Time__c 
                                        FROM EP_Tank__c 
                                        WHERE EP_Ship_To__c IN :idSet
                                        AND EP_Tank_Status__c = :EP_Common_Constant.TANK_OPERATIONAL_STATUS
                                        AND EP_Tank_Dip_Entry_Mode__c = :EP_Common_Constant.SHIP_TO_TANK_DIP_PORTAL_ENTRY_MODE
                                      ]){
            tankList.addAll(tankobjList);
    }
    return tankList;
}

/* * 
*  @name             getOperationalRecordsByShipTo
*  @param            id
*  @return           Map<Id,EP_Tank__c>
*  @throws           NA
*/
public static Map<Id,EP_Tank__c> getOperationalRecordsByShipTo(Id strSelectedShipToID) {
    EP_GeneralUtility.Log('Public','EP_TankMapper','getOperationalRecordsByShipTo');
    Map<Id,EP_Tank__c> mapOperationalTanks = new Map<Id,EP_Tank__c>([SELECT Id, EP_Product__c, EP_Product__r.Name, EP_Tank_Alias__c,EP_Safe_Fill_Level__c, EP_Tank_Code__c,
                                        EP_Tank_Status__c,EP_Product__r.EP_Product_Sold_As__c from EP_Tank__c 
                                        WHERE EP_Ship_To__c = :strSelectedShipToID
                                        AND EP_Tank_Status__c = :EP_Common_Constant.TANK_OPERATIONAL_STATUS
                                      ]);
    
    return mapOperationalTanks;
}

/* * 
*  @name             getRecordsByShipTo
*  @param            id
*  @return           Map<Id,EP_Tank__c>
*  @throws           NA
*/
public static Map<Id,EP_Tank__c> getRecordsByShipTo(Id strSelectedShipToID) {
    EP_GeneralUtility.Log('Public','EP_TankMapper','getRecordsByShipTo');
    Map<Id,EP_Tank__c> mapTanks = new Map<Id,EP_Tank__c>([SELECT Id, EP_Product__c, EP_Product__r.Name, EP_Tank_Alias__c,EP_Safe_Fill_Level__c, EP_Tank_Code__c,
                                        EP_Tank_Status__c,EP_Product__r.EP_Product_Sold_As__c from EP_Tank__c 
                                        WHERE EP_Ship_To__c = :strSelectedShipToID
                                      ]);
    
    return mapTanks;
}
/*Defect 78513 start*/
    /* * 
    *  @name             getTanksRecordsByShipToIds
    *  @param            List
    *  @return           Map<Id,List<EP_Tank__c>>
    *  @throws           NA
    */
    public Map<id,List<EP_Tank__c>> getTanksRecordsByShipToIds(List<Account> SelectedShipToRecords) {
        EP_GeneralUtility.Log('Public','EP_TankMapper','getTanksRecordsByShipToIds');
        Map<Id,List<EP_Tank__c>> shipAndTankMap =new Map<Id,List<EP_Tank__c>>();
        for(EP_tank__c tanks :[select id,EP_Ship_To__c,EP_Ship_To_Nr__c,EP_Tank_Code__c,EP_Product_Code__c,/* #L4_45352_Start*/EP_Tank_Dip_Entry_Mode__c, EP_UnitOfMeasure__c,EP_Block_From__c,EP_Block_Till__c /* #L4_45352_END*/,EP_Tank_Status__c,
        EP_Capacity__c, EP_Safe_Fill_Level__c,EP_Deadstock__c,EP_TANK_STATUS_TO_LS__c,LastModifiedDate,EP_Last_Modified_By_Name__c,/*76105*/EP_Tank_Alias__c/*76105*/ from EP_Tank__c where EP_Ship_To__c in : SelectedShipToRecords and EP_Tank_Status__c in : TankToStatus]){    
            List<EP_Tank__c> TankList = new List<EP_tank__c>();
            if(shipAndTankMap.containsKey(tanks.EP_Ship_To__c) && shipAndTankMap.get(tanks.EP_Ship_To__c)!=null ){
                TankList = shipAndTankMap.get(tanks.EP_Ship_To__c);
            }
            TankList.add(tanks);
            shipAndTankMap.put(tanks.EP_Ship_To__c,TankList);
        }
        return shipAndTankMap;
    }
    /* * 
    *  @name             getTanksRecordsByShipToId
    *  @param            Account
    *  @return           List<EP_Tank__c>
    *  @throws           NA
    */
    public List<EP_Tank__c> getTanksRecordsByShipToId(Account SelectedShipToRecord){
        EP_GeneralUtility.Log('Public','EP_TankMapper','getTanksRecordsByShipToId');
        List<EP_Tank__c> ListTank =new List<EP_Tank__c>();
        ListTank = [select id,EP_Ship_To__c,EP_Ship_To_Nr__c,EP_Tank_Code__c,EP_Product_Code__c,/* #L4_45352_Start*/EP_Tank_Dip_Entry_Mode__c, EP_UnitOfMeasure__c,EP_Block_From__c,EP_Block_Till__c /* #L4_45352_END*/,EP_Tank_Status__c,
        EP_Capacity__c, EP_Safe_Fill_Level__c,EP_Deadstock__c,EP_TANK_STATUS_TO_LS__c,LastModifiedDate,EP_Last_Modified_By_Name__c,/*76105*/EP_Tank_Alias__c/*76105*/ from EP_Tank__c where EP_Ship_To__c = : SelectedShipToRecord.id and EP_Tank_Status__c in : TankToStatus];
        return ListTank;
    }
/*Defect 78513 end*/

}