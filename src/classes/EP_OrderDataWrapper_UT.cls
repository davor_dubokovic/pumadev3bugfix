@isTest
public class EP_OrderDataWrapper_UT{
    static testMethod void constructorInvokeTest (){
        EP_OrderDataWrapper  localObj = new EP_OrderDataWrapper();
        System.assertequals(localObj.orderType, EP_Common_Constant.SALES_ORDER);
        System.assertequals(localObj.vmiType, EP_Common_Constant.NONVMI);
        System.assertequals(localObj.deliveryType, EP_Common_Constant.DELIVERY);
        System.assertequals(localObj.productSoldAsType, EP_Common_Constant.PRODUCT_BULK);
        EP_OrderDataStub.LineItem oc = new EP_OrderDataStub.LineItem('id','quan','companyCd','productCd'); 
        system.assertequals(oc.lineItemId,'id');
        system.assertequals(oc.quantity,'quan');
        system.assertequals(oc.companyCode,'companyCd');
        system.assertequals(oc.productCode,'productCd'); 
	}
}