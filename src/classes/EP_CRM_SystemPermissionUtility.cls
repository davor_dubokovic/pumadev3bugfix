public without sharing class EP_CRM_SystemPermissionUtility {

    // The permissions required by the application.
    private static List<String> getPermissionList() {
        return new List<String>{
            'PermissionsApiEnabled',
            'PermissionsModifyAllData',
            'PermissionsConvertLeads'
        };
    }

    public static Map<String, Boolean> permissionCache;
    private static User p_userWithPermissions { get; set; }

    public static User getUserWithPermissions() {

        if (p_userWithPermissions == null) {

            List<String> permissionList = EP_CRM_SystemPermissionUtility.getPermissionList();

            List<String> userFieldsList = new List<String>{ 'Id', 'Name' };
            for (String permission : permissionList) {
                userFieldsList.add('Profile.' + permission);
            }

            List<String> permissionSetAssignmentFieldsList = new List<String>();
            for (String permission : permissionList) {
                permissionSetAssignmentFieldsList.add('PermissionSet.' + permission);
            }

            p_userWithPermissions = Database.Query(
                String.join(
                    new List<String> {
                        'SELECT',
                        String.join(userFieldsList, ', '),
                        ',',
                        '(SELECT Id,',
                        String.join(permissionSetAssignmentFieldsList, ', '),
                        'FROM PermissionSetAssignments WHERE',
                        String.join(permissionSetAssignmentFieldsList, ' = TRUE OR '),
                        ' = TRUE)',
                        'FROM User WHERE Id = \'' + UserInfo.getUserId() + '\' LIMIT 1'
                    },
                    ' '
                )
            );

        }

        return p_userWithPermissions;

    }

    public static Boolean userHasPermission(String permissionName) {

        if (permissionCache == null)
            // Lazy load the permission cache.
            permissionCache = new Map<String, Boolean>();
        else {
            // Check the cache for permission.
            if (permissionCache.containsKey(permissionName) == true) {
                return permissionCache.get(permissionName);
            }
        }

        // Get the user with permissions (cached if possible)
        User u = getUserWithPermissions();

        // Determine whether the running user has a given Permission   
        Boolean hasPermission = false;

        if (u != null) {    
            // First check the Profile          
            SObject profile = u.getSObject('Profile');
            if (profile != null && Boolean.valueOf(profile.get(permissionName)) == true) {
                hasPermission = true;
            }
            // Then check Permission Set Assignments
            else {
                List<SObject> psaList = u.getSObjects('PermissionSetAssignments');
                System.Debug('psaList: ' + psaList);
                if (psaList != null && psaList.isEmpty() == false) {
                    for (SObject psa : psaList) {
                        SObject ps = psa.getSObject('PermissionSet');
                        if (ps.get(permissionName) != null && Boolean.valueOf(ps.get(permissionName)) == true) {
                            hasPermission = true;
                            break;
                        }
                    }
                }
            }
        }

        // Cache the permission and return the result.
        permissionCache.put(permissionName, hasPermission);
        return hasPermission;

    }
}