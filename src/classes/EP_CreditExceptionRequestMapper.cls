/*
   @Author          Accenture
   @Name            EP_CreditExceptionRequestMapper
   @CreateDate      26/12/2017
   @Description     This class contains all SOQLs related to EP_Credit_Exception_Request__c Object 
   @Version         1.0
   @Reference       NA
*/
public with sharing class EP_CreditExceptionRequestMapper {
	public EP_CreditExceptionRequestMapper() {
		
	} 

	public EP_Credit_Exception_Request__c getRecord(Id recordId) {

		return [SELECT id, Name,CreatedDate,EP_CS_Order__r.TotalAmount__c,EP_CS_Order__r.EP_Payment_Term__c,EP_CS_Order__r.EP_Order_Epoch__c,EP_Comment__c,
                	EP_CS_Order__r.EP_Loading_Date__c,EP_CS_Order__r.EP_Expected_Delivery_Date__c,EP_SenderId__c,EP_Sales_Person_Code__c,EP_CS_Order__r.OrderNumber__c,
                	EP_Bill_To_Account_No__c, EP_Company_Code__c,EP_Request_Date__c,EP_Available_Funds__c,EP_Current_Order_Value__c,CurrencyIsoCode,
                	EP_Status__c,EP_Reason__c,EP_Total_Value_of_Open_Orders_in_SFDC__c,EP_Overdue_Invoices__c,EP_Is_Retrospective__c
                FROM EP_Credit_Exception_Request__c 
                WHERE id =:recordId];
	}

	public List<EP_Credit_Exception_Request__c> getRecords(Set<String> creditExceptionNameSet) {

		return [SELECT Id, Name, EP_CS_Order__c,EP_CS_Order__r.OrderNumber__c,  EP_CS_Order__r.EP_Credit_Status__c, 
						EP_Status__c, EP_Reason__c 
				FROM EP_Credit_Exception_Request__c 
				WHERE Name IN : creditExceptionNameSet];
	}
}