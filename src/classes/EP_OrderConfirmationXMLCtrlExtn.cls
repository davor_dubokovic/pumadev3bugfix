/**
  @Author : Accenture
  @Name : EP_OrderConfirmationXMLCtrlExtn
  @CreateDate : 02/15/2017
  @Description : This Controller Extension is used to generate XML message to send Order Confirmation to OPMS
  @Version 1.0
*/
public with sharing class EP_OrderConfirmationXMLCtrlExtn {
    @testvisible private map<id, Account> relatedAccountsMap = new map<id, Account>();
    private csord__Order__c ord;
    private string messageType;
    private string secretCode;
    
    public string messageId{get;set;}
    public string docTitle {get;set;}
    public string docPurpose {get;set;}
    public Boolean isPriceDisplayed{get;set;}
    public csord__Order__c orderDetails{get;set;}
    public String documentType{get;set;}
    public Company__c companyDetails;
    public Account storageLocAcct;
    public Account shipToAcct;
    public Account transporterAcct {get;set;}
    public list<csord__Order_Line_Item__c> orderLineItems;

    public Decimal basePriceSum{get;set;}
    public Decimal taxSum{get;set;}
    public Decimal totalPrice{get;set;}
    public String priceConsolidationBasis{get;set;}
    public String paymentTerm{get;set;}
    public String customerPONumber{get;set;}
    public String priceRequest;
    public String priceResponse;

    public String supplyLocId{get;set;}
    public String hasPriceIndicative{get;set;}
    public Date dlvryDate{get;set;}
    
   /**
    * @Author       Accenture
    * @Name         EP_OrderConfirmationXMLCtrlExtn
    * @Date         02/08/2017
    * @Description  The extension constructor initializes the members variable orderObject by using the getRecord method from the standard controller. This method will be use setup the values for Header in XML message
    * @Param        ApexPages.StandardController
    * @return       
    */
    public EP_OrderConfirmationXMLCtrlExtn (ApexPages.StandardController stdController) {
        secretCode = ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_SECRETCODE);
        Id orderId = stdController.getRecord().Id;
        this.ord = [SELECT Id, csord__Identification__c, EP_Puma_Company_Code__c, EP_Document_Title__c, OrderNumber__c, pricingResponseJson__c, csord__account__r.ep_price_consolidation_basis__c, RepriceRequired__c FROM csord__Order__c WHERE Id = :orderId];
        this.messageType = ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_MESSAGE_TYPE);
        this.messageId =  ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_MESSAGEID);
        docTitle = EP_Common_Constant.BLANK;
        setOrderDetails();
        setRelatedAccountDetails();

        basePriceSum = 0;
        taxSum = 0;
        priceConsolidationBasis = this.ord.csord__account__r.ep_price_consolidation_basis__c;
		hasPriceIndicative = 'No';
    }
    
     /**
    * @Author       Accenture : Ramcharan Patidar
    * @Name         XMLValidatorAccountDetails
    * @Date         01/11/2019
    * @Description  This method is used to validate XML related account Name,ShippingStreet,ShippingState with Special Charactor.
    * @Param :
    * @return       Account
    * @Defect No :  96548 / 97517
    */
    public static void XMLValidatorAccountDetails(Account ordacc) {
        
        if(ordacc != null) {
            ordacc.name = ordacc.name.escapeXML();
            if(ordacc.ShippingStreet != null )
                ordacc.ShippingStreet = ordacc.ShippingStreet.escapeXML();
            if(ordacc.ShippingState != null )
                ordacc.ShippingState = ordacc.ShippingState.escapeXML();
        }
    }
     /**
    * @Author       Accenture : Ramcharan Patidar
    * @Name         XMLValidatorCompanyDetails
    * @Date         30/11/2019
    * @Description  This method is used to validate XML related Company Name,companyStreet,companyState with Special Charactor.
    * @Param :
    * @return       Company__c
    * @Defect No :  97517
    */
    public static void XMLValidatorCompanyDetails(Company__c companyDetails) {
        
        if(companyDetails != null) {
          companyDetails.name = companyDetails.name.escapeXML();
            if(companyDetails.EP_Company_Street__c != null)
                companyDetails.EP_Company_Street__c = companyDetails.EP_Company_Street__c.escapeXML();
            if(companyDetails.EP_Company_State__c != null)
                companyDetails.EP_Company_State__c = companyDetails.EP_Company_State__c.escapeXML();
          
        }
    }
    
    /**
    * @Author       Accenture
    * @Name         getCompanyDetails
    * @Date         02/08/2017
    * @Description  This method is used to Company details for Order's Company
    * @Param        Sobject
    * @return       
    */
    public Company__c getCompanyDetails() {
        EP_GeneralUtility.Log('Public','EP_OrderConfirmationXMLCtrlExtn','getCompanyDetails');
        EP_CompanyMapper mapperObj = new EP_CompanyMapper();
        companyDetails = mapperObj.getCompanyDetailsByCompanyCode(this.ord.EP_Puma_Company_Code__c);
        XMLValidatorCompanyDetails(companyDetails);
        return companyDetails;
    }
    
   /**
    * @Author       Accenture
    * @Name         setOrderDetails
    * @Date         02/08/2017
    * @Description  This method is used set Order details
    * @Param        
    * @return       
    */
    @TestVisible
    private void setOrderDetails() {
        EP_GeneralUtility.Log('Private','EP_OrderConfirmationXMLCtrlExtn','setOrderDetails');
        this.orderDetails = new csord__Order__c();
        EP_OrderMapper mapperObj = new EP_OrderMapper();
        List<csord__Order__c> ordList = new List<csord__Order__c>();
        ordList = mapperObj.getCSRecordsByIds(new Set<Id>{this.ord.Id});
        if(!ordList.isEmpty()){
            this.orderDetails = ordList[0];
            //Validating Special characters of PO Number Field starts - Added as part of Defect:93243
            if(this.orderDetails.EP_Customer_PO_Number__c != null){
            customerPONumber = this.orderDetails.EP_Customer_PO_Number__c.escapeXML();
            }
            //Validating Special characters of PO Number Field Ends - Added as part of Defect:93243
            system.debug('Product Category='+this.orderDetails.EP_Order_Product_Category__c);
            isPriceDisplayed =  this.orderDetails.EP_Sell_To__r.EP_Display_Price__c;
            setDocTitle(); 

            if(this.orderDetails.Stock_Holding_Location__c == null){
                supplyLocId = this.orderDetails.EP_Pickup_Location_ID__c;
            } else{
                supplyLocId = this.orderDetails.EP_Supply_Location_Number__c;
            }

            if (this.orderDetails.EP_Requested_Delivery_Date__c != null) {
                dlvryDate = this.orderDetails.EP_Requested_Delivery_Date__c;
            } else if (this.orderDetails.Delivery_From_Date__c != null && this.orderDetails.Delivery_To_Date__c != null) {
                dlvryDate = this.orderDetails.Delivery_From_Date__c;
            }
    
            if(this.orderDetails.EP_Order_Epoch__c == EP_OrderConstant.Order_Epoch_Retro){
                dlvryDate = null;
            }

        }
    }
    
    /**
    * @Author       Accenture
    * @Name         setRelatedAccountDetails
    * @Date         02/08/2017
    * @Description  This method is used to set all Accounts detail which are associated with Order
    * @Param :
    * @return       
    */
    @TestVisible
     private void setRelatedAccountDetails() {
        EP_GeneralUtility.Log('Private','EP_OrderConfirmationXMLCtrlExtn','setRelatedAccountDetails');
        set<id> accountIdSet = new set<id>();
        relatedAccountsMap = new map<id, Account>();
        
        // MC CS: 18/04/2018 Issue number 85329- adding Pickup_Supply_Location__r.Stock_Holding_Location__c for ex-rack orders
        if(this.orderDetails.Stock_Holding_Location__c == null){
            accountIdSet.add(this.orderDetails.Pickup_Supply_Location__r.Stock_Holding_Location__c);
        } else{
            accountIdSet.add(this.orderDetails.Stock_Holding_Location__r.Stock_Holding_Location__c);
        } 
        accountIdSet.add(this.orderDetails.EP_ShipTo__c);
        accountIdSet.add(this.orderDetails.EP_Transporter__c);
        accountIdSet.add(this.orderDetails.csord__Account__c);
        
        EP_AccountMapper mapperObj = new EP_AccountMapper();
        for(Account accObj : mapperObj.getRecordsByIds(accountIdSet)) {
            relatedAccountsMap.put(accObj.id, accObj);
        }
    }
    
    /**
    * @Author       Accenture
    * @Name         getStorageLocAcct
    * @Date         02/08/2017
    * @Description  This method is used to get Storage Location details from Account
    * @Param :
    * @return        Account
    */
     public Account getStorageLocAcct() {
        EP_GeneralUtility.Log('Public','EP_OrderConfirmationXMLCtrlExtn','getStorageLocAcct');
        Account storageLocAcct = new Account();
        if(this.orderDetails.Stock_Holding_Location__c != null && relatedAccountsMap.containsKey(this.orderDetails.Stock_Holding_Location__r.Stock_Holding_Location__c)) {
            storageLocAcct = relatedAccountsMap.get(this.orderDetails.Stock_Holding_Location__r.Stock_Holding_Location__c);
        // MC CS: 18/04/2018 Issue number 85329- adding Pickup_Supply_Location__r.Stock_Holding_Location__c for ex-rack orders
        } else if (this.orderDetails.Pickup_Supply_Location__c != null && relatedAccountsMap.containsKey(this.orderDetails.Pickup_Supply_Location__r.Stock_Holding_Location__c)){
            storageLocAcct = relatedAccountsMap.get(this.orderDetails.Pickup_Supply_Location__r.Stock_Holding_Location__c);
        }
        XMLValidatorAccountDetails(storageLocAcct);
        return storageLocAcct;
    }

    /**
    * @Author       Accenture
    * @Name         getShipToAcct
    * @Date         02/08/2017
    * @Description  This method is used get ShipTo details from the Order
    * @Param :
    * @return        Account
    */
     public Account getShipToAcct() {
        EP_GeneralUtility.Log('Public','EP_OrderConfirmationXMLCtrlExtn','getShipToAcct');
        Account shipToAcct = new Account();
        if(this.orderDetails.Ep_ShipTo__c == null && relatedAccountsMap.containsKey(this.orderDetails.EP_Pickup_Location__c)){
            shipToAcct = relatedAccountsMap.get(this.orderDetails.EP_ShipTo__c);
        } else {
            if(relatedAccountsMap.containsKey(this.orderDetails.EP_ShipTo__c)) {
            shipToAcct = relatedAccountsMap.get(this.orderDetails.EP_ShipTo__c);
            }
        }
        XMLValidatorAccountDetails(shipToAcct);
        return shipToAcct;
    }
    
    /**
    * @Author       Accenture
    * @Name         getTransporterAcct
    * @Date         02/08/2017
    * @Description  This method is used to get Transporter details for Order
    * @Param :
    * @return        Account
    */
    /*
    public Account getTransporterAcct() {
        EP_GeneralUtility.Log('Public','EP_OrderConfirmationXMLCtrlExtn','getTransporterAcct');
        Account transporterAcct = new Account();
        if(relatedAccountsMap.containsKey(this.orderDetails.EP_Transporter__c)) {
            transporterAcct = relatedAccountsMap.get(this.orderDetails.EP_Transporter__c);
        }
        XMLValidatorAccountDetails(transporterAcct);
        return transporterAcct;
    }*/
     
   /**
    * @Author       Accenture
    * @Name         getOrderLineItems
    * @Date         02/08/2017
    * @Description  This method is used to get Order Items details for Order
    * @Param :
    * @return        list<OrderItem>
    */
    public List<csord__Order_Line_Item__c> getOrderLineItems() {
        EP_GeneralUtility.Log('Public','EP_OrderConfirmationXMLCtrlExtn','getOrderLineItems');
        this.orderLineItems = new List<csord__Order_Line_Item__c>();
        EP_OrderItemMapper ordItemMapper = new EP_OrderItemMapper();
        //Repeat this for every Child node

        String pricingJson = '';
        Boolean isVMI = false;

        String attachName;
        if(this.ord.csord__account__r.ep_price_consolidation_basis__c == EP_Common_Constant.ORDER_SUMMARISED_PRICE){
            attachName = EP_Common_Constant.CS_PRICING_RESPONSE_JSON;
        } else {
            attachName = EP_Common_Constant.ORDER_PRICE_RESPONSE_DETAILED;
        }

        list<Attachment> lineItems = [SELECT Id, Name,body, ParentId, Parent.Type FROM Attachment where ParentId =: this.ord.csord__Identification__c and name =:attachName];
        System.debug('getOrderLineItems lineItems = ' + lineItems);

        System.debug('ParentId: ' + this.ord.csord__Identification__c);

        if (String.isNotBlank(this.ord.pricingResponseJson__c)) {
            isVMI = true;
            pricingJson = this.ord.pricingResponseJson__c;
        } else if (lineItems.size() >0 && String.isNotBlank(lineItems.get(0).body.toString())) {
            pricingJson = lineItems.get(0).body.toString();
        }

        if(this.ord.RepriceRequired__c && !isVMI){
            Map<String, Object> inputMap = new Map<String, Object>();
            inputMap.put('BasketId', this.ord.csord__Identification__c);
            inputMap.put('CompanyCode', this.ord.EP_Puma_Company_Code__c);
            inputMap.put('priceConsolidationBasis', this.ord.csord__account__r.ep_price_consolidation_basis__c);
            Map<String, Object> callResult = TrafiguraGetPricesDetailed.doPriceCallDetailed(inputMap);
            String outcome = (String)callResult.get('outcome');
            
            if(outcome == 'OK'){
                priceRequest = (String)callResult.get('request');
                priceResponse = (String)callResult.get('response');
                pricingJson = (String)callResult.get('response');
                //TrafiguraGetPricesDetailed.saveAttachments(this.ord.csord__Identification__c, priceRequest, priceResponse);
            }
        }

        if(pricingJson != ''){
            system.debug('pricingJson: '+pricingJson);
    
            EP_PricingResponseStub stub = (EP_PricingResponseStub) System.JSON.deserialize(pricingJson , EP_PricingResponseStub.class);
            totalPrice = 0;
            
            for(csord__Order_Line_Item__c orderLineitem : ordItemMapper.getCSOrderById(new set<Id>{this.ord.Id})) {
                for (EP_PricingResponseStub.LineItem li : stub.Payload.any0.pricingResponse.priceDetails.priceLineItems.lineItem) {
                    List<String> quantityList = li.lineItemInfo.quantity.split(',');
                    String receivedQuantity = '';

                    for (Integer i = 0; i < quantityList.size(); i++) {
                        receivedQuantity = receivedQuantity + quantityList[i];
                    }
                        
                    if (li.lineItemInfo.itemId.equals(String.valueOf(orderLineitem.EP_Product_Code__c)) && (orderLineitem.Quantity__c == Decimal.valueOf(receivedQuantity))) {
                    //if (li.lineItemInfo.lineItemId.equals(String.valueOf(orderLineitem.CS_OrderLine_Number__c))) {

                        if(li.lineItemInfo.indicativePriceflag == 'Yes'){
                            hasPriceIndicative = 'Yes'; 
                        }
                        Decimal taxUnitPrice = 0;
                        Decimal baseUnitPrice = 0;
                        List<csord__Order_Line_Item__c> orderLineItemMain = new List<csord__Order_Line_Item__c>();
                        List<csord__Order_Line_Item__c> orderLineItemSubs = new List<csord__Order_Line_Item__c>();

                        Boolean foundFuelPrice = false;

                        for(EP_PricingResponseStub.invoiceComponent invoiceComp : li.lineItemInfo.invoiceDetails.invoiceComponent){
                            csord__Order_Line_Item__c orderLineitemCopy = orderLineitem.clone();

                            orderLineitemCopy.UnitPrice__c = Decimal.valueOf(invoiceComp.amount);  
                            orderLineitemCopy.csord__Line_Description__c = (Decimal)(orderLineitemCopy.UnitPrice__c * orderLineitem.Quantity__c) +'';
                            totalPrice += (Decimal.valueOf(orderLineitemCopy.csord__Line_Description__c)).setscale(2);
                            taxUnitPrice += Decimal.valueOf(invoiceComp.taxAmount);
                            baseUnitPrice += Decimal.valueOf(invoiceComp.amount);
                            
                            if(invoiceComp.type == 'Fuel Price' && !foundFuelPrice){
                                foundFuelPrice = true;

                                orderLineitemCopy.EP_Invoice_Name__c = li.lineItemInfo.itemId;
                                orderLineitemCopy.Description__c = orderLineItem.EP_Product__r.Name;
                                orderLineitemCopy.EP_Tax_Percentage__c = 0.0;
                                orderLineItemMain.add(orderLineitemCopy);
                            } else {
                            
                                orderLineitemCopy.EP_Invoice_Name__c = null;
                                if(invoiceComp.componentDescr == ''){
                                    orderLineitemCopy.Description__c = invoiceComp.name; 
                                } else {
                                    orderLineitemCopy.Description__c = invoiceComp.componentDescr;         
                                }

                                orderLineitemCopy.UnitPrice__c = null;
                                orderLineitemCopy.Quantity__c = null;
                                orderLineitemCopy.EP_Quantity_UOM__c = null;
                                orderLineitemCopy.EP_Tax_Percentage__c = Decimal.valueOf(invoiceComp.taxPercentage);
                                orderLineitemCopy.ListPrice__c = 0.0;
                                orderLineItemSubs.add(orderLineitemCopy);
                            }
                            basePriceSum += (Decimal.valueOf(invoiceComp.amount) * Decimal.valueOf(li.lineItemInfo.quantity)).setScale(2, System.RoundingMode.HALF_UP); 
                            system.debug('basePriceSum='+basePriceSum);
                            
                        }

                        orderLineItemMain.get(0).ListPrice__c = Decimal.valueOf(li.lineItemInfo.unitPrice) - taxUnitPrice;
                        orderLineItems.addAll(orderLineItemMain);
                        orderLineItems.addAll(orderLineItemSubs);

                        taxSum += taxUnitPrice * Decimal.valueOf(li.lineItemInfo.quantity);  
                        system.debug('taxsumLine='+taxsum);
                           
                    }
                }
                
            }
            taxsum = taxsum.setScale(2, System.RoundingMode.HALF_UP);
	    system.debug('taxsumTotal='+taxsum);
        }
        System.debug('getOrderLineItems orderLineItems: '+orderLineItems);
        return orderLineItems;
    }
    
   
    /**
    * @Author       Accenture
    * @Name         getSellToAcct
    * @Date         02/08/2017
    * @Description  This method is used to get SellTo details for Order
    * @Param :
    * @return        Account
    */
    public Account getSellToAcct() {
        EP_GeneralUtility.Log('Public','EP_OrderConfirmationXMLCtrlExtn','getSellToAcct');
        Account sellToAcct = new Account();
        system.debug('pep sell to' + sellToAcct);
        if(relatedAccountsMap.containsKey(this.orderDetails.csord__Account__c)) {
            sellToAcct = relatedAccountsMap.get(this.orderDetails.csord__Account__c);
            system.debug('pep ' + sellToAcct);
        }
		//Defect:96769__Added If else conditions to pick Correct payment Terms from Account in case of Packaged/Bulk/Service Product Types
        if(this.orderDetails.EP_Order_Product_Category__c == 'Packaged' && sellToAcct.EP_Package_Payment_Term__c != null){
            paymentTerm = sellToAcct.EP_Package_Payment_Term__r.Name;
        } else if(sellToAcct.EP_Payment_Term_Lookup__c != null) {
            paymentTerm = sellToAcct.EP_Payment_Term_Lookup__r.Name;
        }
		XMLValidatorAccountDetails(sellToAcct);
        return sellToAcct;
    }

    /**
    * @Author       Accenture
    * @Name         setDocTitle
    * @Date         02/08/2017
    * @Description  This method is used to set Doc Title for ORDER_CONFIRMATION on VF page
    * @Param        
    * @return       NA
    */   
    public void setDocTitle() {
        EP_GeneralUtility.Log('Public','EP_OrderConfirmationXMLCtrlExtn','setDocTitle');
        this.docTitle = ord.EP_Document_Title__c;
        if(this.orderDetails.csord__Status2__c != null && this.orderDetails.csord__Status2__c.equals(EP_Common_Constant.ORDER_DRAFT_STATUS) ){
            this.docTitle = EP_Common_Constant.SALESQUOTES_TITLE;
        }
        documentType = EP_Common_Constant.ORDER_CONFIRMATION_TAG; // 'orderConfirmation'
        docPurpose = EP_Common_Constant.ORDERCONFIRM_DOCPURPOSE; // 'orderConfirmation'
        
        //IF docTitle = 'Quote'
        If(EP_Common_Constant.SALESQUOTES_TITLE.equalsIgnoreCase(this.docTitle)){
            //documentType = EP_Common_Constant.SALESQUOTES_TITLE; // 'Quote'
            docPurpose = EP_Common_Constant.ORDERQUOTE_DOCPURPOSE; //'orderQuote'
        }
        //IF docTitle = 'Pro Forma Invoice'
        If(EP_Common_Constant.PROFORMAINVOICE_DOCTITLE.equalsIgnoreCase(this.docTitle)){
            //documentType = EP_Common_Constant.PROFORMAINVOICE_DOCTITLE; // 'Pro Forma Invoice'
            docPurpose = EP_Common_Constant.PROFORMAINVOICE_DOCPURPOSE; // 'orderProforma'
        }
        //IF docTitle = 'Order Confirmation'
        If(EP_Common_Constant.ORDERCONFIRM_TITLE.equalsIgnoreCase(this.docTitle)){
            //documentType = EP_Common_Constant.ORDERCONFIRM_DOCPURPOSE; // 'orderConfirmation'
            docPurpose = EP_Common_Constant.ORDERCONFIRM_DOCPURPOSE; // 'orderConfirmation'
        }
        //IF docTitle = 'pickupAdvice'
        If(EP_Common_Constant.PICKUPADVICE_TITLE.equalsIgnoreCase(this.docTitle)){
            documentType = EP_Common_Constant.PICKUPADVICE_TITLE; // 'pickupAdvice'
            docPurpose = EP_Common_Constant.PICKUPADVICE_DOCPURPOSE; // 'Pickup Advice'
        }
        //IF docTitle = 'ProformaInvoice'
        If(EP_Common_Constant.PROFORMAINVOICE_TITLE.equalsIgnoreCase(this.docTitle)){
            //documentType = EP_Common_Constant.PROFORMAINVOICE_TITLE; // 'ProformaInvoice'
            docPurpose = EP_Common_Constant.PROFORMAINVOICE_DOCPURPOSE; // 'orderProforma'
        }
    }
    
   /**
    * @Author       Accenture
    * @Name         checkPageAccess
    * @Date         02/08/2017
    * @Description  This method will be use to redirect user at Error Page if they are trying to access this page without passing secret Code.
    * @Param        
    * @return       PageReference
    */
    public PageReference checkPageAccess() {
        //Save Attachments
        //TrafiguraGetPricesDetailed.saveAttachments(this.ord.csord__Identification__c);

        EP_GeneralUtility.Log('Public','EP_OrderConfirmationXMLCtrlExtn','checkPageAccess');
        PageReference pageRef = null;
        if(! EP_OutboundMessageUtil.isAuthorized(this.secretCode)) {
            pageRef =  EP_OutboundMessageUtil.redirectToErrorPage();
        }
        return pageRef;
    }
}