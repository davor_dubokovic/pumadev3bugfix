@isTest
private class EP_TankDipWizardTest{
    
    private static Account vmiShipToAccount,sellToAccount; 
   // private static EP_Tank_Dip__c tankDip; 
    private static EP_TestDataUtility.WrapperCustomerHierarchy customHierarchy=null;
    private static Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.VMI_SHIP_TO);
    static List<Account> accountsToBeUpdated;
    static String recType= Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get('Placeholder').getRecordTypeId();
    
    
    
    /**************************************************************************
    *@Description : This method will validate the dip entry for single tank single 
                    dip entry.
    *@Return      : void                                                      *    
    **************************************************************************/
    
    private static void updateAccountStatus(){
    
        EP_ActionTriggerHandler.isExecuteAfterUpdate = true;
        EP_ActionTriggerHandler.isExecuteBeforeUpdate = true;
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
        //EP_AccountTriggerHandler.isExecuteBeforeUpdate = true;
        accountsToBeUpdated = new List<Account>();
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        //sellToAccount.EP_Reason_Blocked__c = 'Maintenance';
        accountsToBeUpdated.add(sellToAccount);
        vmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        vmiShipToAccount.EP_Tank_Dips_Schedule_Time__c = '01:00';
        vmiShipToAccount.EP_Tank_Dip_Entry_Mode__c = 'Portal Dip Entry';
        accountsToBeUpdated.add(vmiShipToAccount);
        Database.upsert(accountsToBeUpdated);   
    }
    
    private static testMethod void checkSingleTankCheckBox(){
        //CREATE CSC USER       
        Profile cscAgent = [Select id from Profile Where Name = 'EP_CSC' Limit 1];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
         strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,EP_Common_Constant.VMI_SHIP_TO);
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();
        Test.startTest();
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
        EP_BankAccountTriggerHandler.isExecuteBeforeUpdate = true;
        System.runAs(cscUser) {
        // Inserting an account
        sellToAccount = EP_TestDataUtility.createSellToAccount(NULL, NULL);
        insert sellToAccount;
        vmiShipToAccount = EP_TestDataUtility.createShipToAccount(sellToAccount.Id, strRecordTypeId);
        insert vmiShipToAccount;
        
        
        List<EP_Tank__c> tanksToBeInsertedList = new List<EP_Tank__c>();
        EP_Tank__c tankObj= EP_TestDataUtility.createTestEP_Tank(vmiShipToAccount.Id,prod.Id);
        tanksToBeInsertedList.add(tankObj);
        
        insert tanksToBeInsertedList;
        
        
        updateAccountStatus();
        accountsToBeUpdated = new List<Account>();
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BLOCKED;
        sellToAccount.EP_Reason_Blocked__c = 'Maintenance';
        accountsToBeUpdated.add(sellToAccount);
        vmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        vmiShipToAccount.EP_Tank_Dips_Schedule_Time__c = '01:00';
        vmiShipToAccount.EP_Tank_Dip_Entry_Mode__c = 'Portal Dip Entry';
        accountsToBeUpdated.add(vmiShipToAccount);
        Database.upsert(accountsToBeUpdated);
        
        List<EP_Tank_Dip__c> tankDipsToUpdate = new List<EP_Tank_Dip__c>();
        EP_Tank_Dip__c tankDip1 = EP_TestDataUtility.createTestRecordsForTankDip(tankObj.Id,system.now()); 
        tankDip1.RecordTypeId = recType;
        tankDipsToUpdate.add(tankDip1);
        insert tankDipsToUpdate;
        PageReference pageRef = Page.EP_TankDipSubmitPage_R1;
        Test.setCurrentPage(pageRef);//Applying page context here  
        ApexPages.currentPage().getParameters().put('id', vmiShipToAccount.Id);
        //ApexPages.currentPage().getParameters().put('Date', '20181015051719');
        EP_TankDipSubmitPageControllerClass_R1 tankDippageObj=new EP_TankDipSubmitPageControllerClass_R1();
       // tankDippageObj.strSelectedTanksIDs=tankObj1.ID;
        //tankDippageObj.saveStockRecords();
        system.debug('*********'+tankDippageObj.displayedTankDipLines[0].blnAddTankDip);
        System.assertEquals(true,tankDippageObj.displayedTankDipLines.size()==1);
        System.assertEquals(true,tankDippageObj.displayedTankDipLines[0].blnAddTankDip);
        }
    }
    
    
    /**************************************************************************
    *@Description : E2E 186_003_Dip entry is mandate as a CSC user for single 
                    ship to  multiple tank with dip entry missing for more than 
                    one day_via Submit Tank Dips Button

    *@Return      : void                                                      *    
    **************************************************************************/
    private static testMethod void checkMultipleTankCheckBox(){
        //CREATE CSC USER
        Profile cscAgent = [Select id from Profile Where Name = 'EP_CSC' Limit 1];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();
        Test.startTest();
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
        EP_BankAccountTriggerHandler.isExecuteBeforeUpdate = true;
        System.runAs(cscUser) {
        // Inserting an account
        sellToAccount = EP_TestDataUtility.createSellToAccount(NULL, NULL);
        insert sellToAccount;
        vmiShipToAccount = EP_TestDataUtility.createShipToAccount(sellToAccount.Id, strRecordTypeId);
        insert vmiShipToAccount;
        
        
        List<EP_Tank__c> tanksToBeInsertedList = new List<EP_Tank__c>();
        EP_Tank__c tankObj= EP_TestDataUtility.createTestEP_Tank(vmiShipToAccount.Id,prod.Id);
        tankObj.EP_Tank_Code__c = '98765';
        tanksToBeInsertedList.add(tankObj);
        
        EP_Tank__c tankObj1= EP_TestDataUtility.createTestEP_Tank(vmiShipToAccount.Id,prod.Id);
        tankObj1.EP_Tank_Code__c = '9876';
        tanksToBeInsertedList.add(tankObj1);
        
        insert tanksToBeInsertedList;
        
        updateAccountStatus();
        accountsToBeUpdated = new List<Account>();
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BLOCKED;
        sellToAccount.EP_Reason_Blocked__c = 'Maintenance';
        accountsToBeUpdated.add(sellToAccount);
        vmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        vmiShipToAccount.EP_Tank_Dips_Schedule_Time__c = '01:00';
        vmiShipToAccount.EP_Tank_Dip_Entry_Mode__c = 'Portal Dip Entry';
        accountsToBeUpdated.add(vmiShipToAccount);
        Database.upsert(accountsToBeUpdated);
        
        List<EP_Tank_Dip__c> tankDipsToUpdate = new List<EP_Tank_Dip__c>();
        /*EP_Tank_Dip__c tankDip1 = EP_TestDataUtility.createTestRecordsForTankDip(tankObj.Id,system.now()-5); 
        tankDip1.RecordTypeId = recType;*/
        EP_Tank_Dip__c tankDip1 = new EP_Tank_Dip__c(RecordTypeid = recType,EP_Tank__c=tankObj.Id,
                                    EP_Unit_Of_Measure__c='LT',EP_Ambient_Quantity__c=30,
                                    EP_Reading_Date_Time__c=system.now()-5);
        tankDipsToUpdate.add(tankDip1);
        EP_Tank_Dip__c tankDip2 = new EP_Tank_Dip__c(RecordTypeid = recType,EP_Tank__c=tankObj1.Id,
                                    EP_Unit_Of_Measure__c='LT',EP_Ambient_Quantity__c=31,
                                    EP_Reading_Date_Time__c=system.now()-5);                            
        tankDipsToUpdate.add(tankDip2);
        insert tankDipsToUpdate;
        
        PageReference pageRef = Page.EP_SelectTankDipDatePage_R1;
        
        Test.setCurrentPage(pageRef);//Applying page context here
        ApexPages.currentPage().getParameters().put('id', vmiShipToAccount.Id);
           
        EP_SelectTankDipDatePageExtnClass_R1 selectTankDipDatePageExtnClass = new EP_SelectTankDipDatePageExtnClass_R1();
            selectTankDipDatePageExtnClass.retrieveMissingSiteDates();
            selectTankDipDatePageExtnClass.tankDipDates.get(0).dateSelected = true;
            system.debug('--------------------'+selectTankDipDatePageExtnClass);
        pageRef = Page.EP_TankDipSubmitPage_R1;
        Test.setCurrentPage(pageRef);//Applying page context here
        
        ApexPages.currentPage().getParameters().put('id', vmiShipToAccount.Id);
        ApexPages.currentPage().getParameters().put('date', selectTankDipDatePageExtnClass.tankDipDates.get(0).dateText);
         
       //pageRef = Page.EP_TankDipSubmitPage_R1;
        EP_TankDipSubmitPageControllerClass_R1 tankDippageObj=new EP_TankDipSubmitPageControllerClass_R1();
       
        //System.assertEquals('/apex/EP_TankDipSubmitPage_R1?date='+string.valueOf(system.today())+'&id='+vmiShipToAccount.Id,pgRef.getUrl());
        system.debug('*********test********'+tankDippageObj.displayedTankDipLines[0].blnAddTankDip);
        System.assertEquals(true,tankDippageObj.displayedTankDipLines.size()==2);
        //System.assertEquals(true,tankDippageObj.displayedTankDipLines[0].blnAddTankDip);
       // System.assertEquals(true,tankDippageObj.displayedTankDipLines[1].blnAddTankDip);
        }
    }
    
    
    /**************************************************************************
    *@Description : E2E 186_003_Dip entry is mandate as a CSC user for single 
                    ship to  multiple tank with multple dip entry missing for more than 
                    one day_via Submit Tank Dips Button

    *@Return      : void                                                      *    
    **************************************************************************/
    private static testMethod void checkMultilpleTankMultipleDips(){
        //CREATE CSC USER
        Profile cscAgent = [Select id from Profile Where Name = 'EP_CSC' Limit 1];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();
        Test.startTest();
        System.runAs(cscUser) {
        // Inserting an account
        sellToAccount = EP_TestDataUtility.createSellToAccount(NULL, NULL);
        insert sellToAccount;
        
        vmiShipToAccount = EP_TestDataUtility.createShipToAccount(sellToAccount.Id, strRecordTypeId);
        insert vmiShipToAccount;
        
        List<EP_Tank__c> tanksToBeInsertedList = new List<EP_Tank__c>();
        
        EP_Tank__c tankObj= EP_TestDataUtility.createTestEP_Tank(vmiShipToAccount.Id,prod.Id);
        tankObj.EP_Tank_Code__c = '98765';
        tanksToBeInsertedList.add(tankObj);
        
        EP_Tank__c tankObj1= EP_TestDataUtility.createTestEP_Tank(vmiShipToAccount.Id,prod.Id);
        tankObj1.EP_Tank_Code__c = '987';
        tanksToBeInsertedList.add(tankObj1);
        
        insert tanksToBeInsertedList;
        updateAccountStatus();
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update sellToAccount;
        accountsToBeUpdated = new List<Account>();
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BLOCKED;
        sellToAccount.EP_Reason_Blocked__c = 'Maintenance';
        accountsToBeUpdated.add(sellToAccount);
        vmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        vmiShipToAccount.EP_Tank_Dips_Schedule_Time__c = '01:00';
        vmiShipToAccount.EP_Tank_Dip_Entry_Mode__c = 'Portal Dip Entry';
        accountsToBeUpdated.add(vmiShipToAccount);
        Database.upsert(accountsToBeUpdated);
        
        List<EP_Tank_Dip__c> tankDipsToUpdate = new List<EP_Tank_Dip__c>();
        EP_Tank_Dip__c tankDip1 = EP_TestDataUtility.createTestRecordsForTankDip(tankObj.Id,system.now()); 
        tankDip1.RecordTypeId = recType;
        tankDipsToUpdate.add(tankDip1);
        EP_Tank_Dip__c tankDip2 = EP_TestDataUtility.createTestRecordsForTankDip(tankObj.Id,system.now()-1); 
        tankDip2.RecordTypeId = recType;
        tankDipsToUpdate.add(tankDip2);
        EP_Tank_Dip__c tankDip3 = EP_TestDataUtility.createTestRecordsForTankDip(tankObj1.Id,system.now()); 
        tankDip3.RecordTypeId = recType;
        tankDipsToUpdate.add(tankDip3);
        insert tankDipsToUpdate;
        //PageReference pageRef = Page.EP_TankDipSubmitPage_R1;
        PageReference pageRef = Page.EP_SelectTankDipDatePage_R1;
        
        Test.setCurrentPage(pageRef);//Applying page context here
        ApexPages.currentPage().getParameters().put('id', vmiShipToAccount.Id);
        
        EP_SelectTankDipDatePageExtnClass_R1 selectTankDipDatePageExtnClass = new EP_SelectTankDipDatePageExtnClass_R1();
            selectTankDipDatePageExtnClass.retrieveMissingSiteDates();
            selectTankDipDatePageExtnClass.tankDipDates.get(0).dateSelected = true;
            pageRef = Page.EP_TankDipSubmitPage_R1;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id', vmiShipToAccount.Id);
            ApexPages.currentPage().getParameters().put('date', selectTankDipDatePageExtnClass.tankDipDates.get(0).dateText);
           // PageReference pgRef=tankDippageObj.next();
            EP_TankDipSubmitPageControllerClass_R1 tankDippageObj=new EP_TankDipSubmitPageControllerClass_R1();
            system.debug('pageRef============='+pageRef);
                
            system.debug('*********'+tankDippageObj.displayedTankDipLines);
        
        }
    }
    
     /************************************************************************************
     * @author Jyotsna Yadav
     * @date 15/12/2015
     * @description Test Method to test that Dip entry is optional for a CSC user with
                    blocked customer account and shipto and blocked tank_via Submit Tank Dips Link
     ***************************************************************************************/
    static testMethod void testDipEntryWithBblockedCustomerAccount() {
        Profile cscAgentProfile = [SELECT Id FROM Profile WHERE Name='EP_CSC' Limit 1];
        User cscAgentUser = EP_TestDataUtility.createUser(cscAgentProfile.id);
        Product2 prod = EP_TestDataUtility.createTestRecordsForProduct();
        System.runAs(cscAgentUser){
            sellToAccount = EP_TestDataUtility.createSellToAccount(NULL, NULL);
            insert sellToAccount;
            vmiShipToAccount = EP_TestDataUtility.createShipToAccount(sellToAccount.Id, strRecordTypeId);
            insert vmiShipToAccount;
            EP_Tank__c tankObj = EP_TestDataUtility.createTestEP_Tank(vmiShipToAccount.Id,prod.Id);
            insert tankObj;
            updateAccountStatus();
            accountsToBeUpdated = new List<Account>();
            sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BLOCKED;
            sellToAccount.EP_Reason_Blocked__c = 'Maintenance';
            accountsToBeUpdated.add(sellToAccount);
            vmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
            vmiShipToAccount.EP_Tank_Dips_Schedule_Time__c = '01:00';
            vmiShipToAccount.EP_Tank_Dip_Entry_Mode__c = 'Portal Dip Entry';
            accountsToBeUpdated.add(vmiShipToAccount);
            Database.upsert(accountsToBeUpdated);
            EP_Tank_Dip__c tankDip3 = EP_TestDataUtility.createTestRecordsForTankDip(tankObj.Id,system.now()); 
            tankDip3.RecordTypeId = recType;
            insert tankDip3;
            PageReference pageRef = Page.EP_SelectTankDipSitePage_R1;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id', vmiShipToAccount.Id);
            
            EP_SelectTankDipDatePageExtnClass_R1 selectTankDipDatePageExtnClass = new EP_SelectTankDipDatePageExtnClass_R1();
            selectTankDipDatePageExtnClass.retrieveMissingSiteDates();
            selectTankDipDatePageExtnClass.tankDipDates.get(0).dateSelected = true;
            pageRef = Page.EP_TankDipSubmitPage_R1;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id', vmiShipToAccount.Id);
            ApexPages.currentPage().getParameters().put('date', selectTankDipDatePageExtnClass.tankDipDates.get(0).dateText);
            EP_TankDipSubmitPageControllerClass_R1 tankDippageObj = new EP_TankDipSubmitPageControllerClass_R1();
            system.assertEquals(tankDippageObj.displayedTankDipLines.size(), 1);
            //system.assertEquals(tankDippageObj.displayedTankDipLines.get(0).blnAddTankDip, true);
            
        }
    }
    
    
     
}