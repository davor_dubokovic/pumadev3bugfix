/* 
   @Author      Accenture
   @name        EP_RunDomainObject 
   @CreateDate  05/05/2017
   @Description This is the domain class for Run Object
   @Version     1.0
*/
public class EP_RunDomainObject {
    
    private List<EP_Run__c> newRunObjList ;     
    private Map<Id,EP_Run__c> oldRunsMap;
    private EP_RunService service;
    public set<Id> invalidRunIds;
    
    /**
    * @author       Accenture
    * @name         EP_RunDomainObject 
    * @date         02/05/2017
    * @description  Constructor for setting new run and old run Map which will be passed by the trigger
    * @param        List, Map
    * @return       NA
    */  
    public EP_RunDomainObject(List<EP_Run__c> newRunList, Map<Id,EP_Run__c> oldRun){
      this.newRunObjList = newRunList;
      this.oldRunsMap= oldRun;
      this.service = new EP_RunService(this);  
    }
    /**
    * @author       Accenture
    * @name         EP_RunDomainObject
    * @date         02/05/2017
    * @description  Constructor for getting run object  
    * @param        Id
    * @return       NA
    */
    public EP_RunDomainObject(List<EP_Run__c> nRunList){
        EP_RunMapper runMapperObj = new EP_RunMapper();
        this.newRunObjList = runMapperObj.getRunList(nRunList);         
        this.service = new EP_RunService(this);           
    }
    /**
    * @author       Accenture
    * @name         getNewRoutes
    * @date         05/05/2017
    * @description  This method is used to get new run records
    * @param        NA
    * @return       List <EP_Run__c>
    */  
    public List <EP_Run__c> getNewRun(){
        EP_GeneralUtility.Log('Public','EP_RunDomainObject','getNewRun');         
        return newRunObjList ; 
    }                   
    /**
    * @author       Accenture
    * @name         getNewRoutes
    * @date         05/05/2017
    * @description  This method is used to get old run records
    * @param        NA
    * @return       Map <id,EP_Run__c>
    */   
    public Map<id, EP_Run__c> getOldRunMap(){
        EP_GeneralUtility.Log('Public','EP_RunDomainObject','newRunsMap'); 
        system.debug('+++++++++++oldRunsMap'+oldRunsMap);        
        return oldRunsMap; 
        
   }
   
    /**
    * @author       Accenture
    * @name         doActionAfterInsert
    * @date         05/05/2017
    * @description  This method is used to perform some action after insertion of run
    * @param        NA
    * @return       void
    */
   public void doActionBeforeInsert(){
        try{
            EP_GeneralUtility.Log('Public','EP_RunDomainObject','doActionBeforeInsert');        
            service.doBeforeInsertHandle();   
        }catch(Exception ex){
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'doBeforeInsertHandle' , EP_RunDomainObject.class.getName(), ApexPages.Severity.ERROR);
            EP_Common_Util.addCommonErrorToAllRecord((List<Sobject>)newRunObjList, ex.getMessage());
        }   
    }
    
    /**
    * @author       Accenture
    * @name         doActionBeforeDelete
    * @date         05/05/2017
    * @description  This method is used to perform some validation check before deleting a run
    * @param        NA
    * @return       void
    */   
   public void doActionBeforeDelete(){
       try{
            EP_GeneralUtility.Log('Public','EP_RunDomainObject','doActionBeforeUpdate');        
            service.doBeforeDeleteHandle();                             
        }catch(Exception ex){
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'doActionBeforeInsert' , 'EP_RunDomainObject', ApexPages.Severity.ERROR);
            EP_Common_Util.addCommonErrorToAllRecord((List<Sobject>)oldRunsMap.values(), ex.getMessage());
        }
   }
   
   /**
    * @author       Accenture
    * @name         doActionBeforeDelete
    * @date         05/05/2017
    * @description  This method returns true if run not associated to any order 
    * @param        EP_Run__c
    * @return       Boolean
    */   
   public Boolean isRunDeletable(Id runID){
        return !invalidRunIds.contains(runID);
   }
   
   /**
    * @author       Accenture
    * @name         getRouteIds
    * @date         05/05/2017
    * @description  returns the setOf Route Ids from List Of Run recods
    * @param        List<EP_Run__c>
    * @return       Set<Id>
    */    
    
    public Set<Id> getRouteIds(){
        Set<Id> routeIds = new Set<Id>();
        for(EP_Run__c runObj : newRunObjList){
            routeIds.add(runObj.EP_Route__c);    
        }
        return routeIds;
    }
}