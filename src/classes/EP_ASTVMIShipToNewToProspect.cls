/*
*  @Author <Accenture>
*  @Name <EP_ASTVMIShipToNewToProspect>
*  @CreateDate <24/02/2017>
*  @Description <Handles VMI Ship To Account status change from 01-Prospect to 01-Prospect>
*  @Version <1.0>
*/
public class EP_ASTVMIShipToNewToProspect extends EP_AccountStateTransition {

    public EP_ASTVMIShipToNewToProspect() {
        finalState = EP_AccountConstant.PROSPECT;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToNewToProspect','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToNewToProspect','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToNewToProspect','isGuardCondition');
        return true;
    }

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToNewToProspect','doOnExit');

    }
}