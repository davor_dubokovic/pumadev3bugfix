/*
 *  @Author <Accenture>
 *  @Name <EP_AccountConstant>
 *  @CreateDate <21/12/2016>
 *  @Description <Class to hold constants related to Account Object>
 *  @Version <1.0>
 */
public with sharing class EP_AccountConstant {   
    
    public static final Set<String> actionStatus =  new set<String>{EP_Common_Constant.ACT_NEW_STATUS,EP_Common_Constant.ACT_ASSIGNED_STATUS};
    public static final Set<String> setOfShipToRecordTypes = new Set<String>{EP_Common_Constant.VMI_SHIP_TO_DEV_NAME,EP_Common_Constant.NON_VMI_SHIP_TO_DEV_NAME};
    public static final String SHIPTO_VMI_CONSIGNMENT ='ShipToVMIConsignment';
    public static final String SHIPTO_NONVMI_CONSIGNMENT ='ShipToNonVMIConsignment';

    //Account states
    public static final string PROSPECT = '01-Prospect';
    public static final string BASICDATASETUP = '02-Basic Data Setup';
    public static final string ACCOUNTSETUP = '04-Account Set-up';
    public static final string ACTIVE = '05-Active';
    public static final string INACTIVE = '07-Inactive';
    public static final string BLOCKED = '06-Blocked';
    public static final string REJECTED = '08-Rejected';
    public static final string DEACTIVATE = '07-Deactivate';//L4 #45361 Code changes 
    
    //Moved from EP_CreateShipToExtension
    public static final string IDPARAM  = 'id';
    public static final string ACC_EDIT_URL = '/001/e?retURL=';
    public static final string REC_TYPE_PARAM = '&RecordType=';
    public static final string ACCNAME_FIELDID  = '&acc3=';
    public static final string ACCNAME_HIDDEN_FIELDID = '&acc3_lkid=';
    public static final string EXTRA_URL = '&ent=Account';    
    public static final string GET_RECORD_TYPE_MTHD = 'getRecordTypes';
    public static final string SELECT_RECORD_TYPE_MTHD = 'selectRecordType';
    public static final string SHIP_TO = 'ShipTo';
    public static final string SELL_TO = 'SellTo';  
    public static final string BILL_TO = 'BillTo';
    public static final string STORAGE_LOCATION = 'StorageLocation';    
    public static final string STORAGE_LOCATION_RT = 'Storage Location';
    public static final string STORAGE_SHIP_TO_RT = 'Storage Ship-To';
    public static final string VMI_ACCOUNT = 'VMI';    
    public static final string NON_VMI_ACCOUNT = 'NonVMI';
    public static final string CONSIGNMENT = 'Consignment'; 
    public static final string NONCONSIGNMENT = 'Non-Consignment'; 
    public static final string NON_CONSIGNMENT = 'NonConsignment';     
    public static final string TO_CONST = 'To';      
    public static final string NEW_TO_PROSPECT_EVT = 'NewTo01-Prospect';
    public static final string NEW_RECORD = 'NEW_RECORD';
    public static final string SETUP_TO_ACTIVE = '04-AccountSet-upTo05-Active';
    public static final string DUMMY_BASICDATASETUP_TO_ACTIVE = '02-BasicDataSetupTo05-Active';    
	public static final string SFDC_TO_NAV_CUSTOMER_PRICING_ENGINE = 'SFDC_TO_NAV_CUSTOMER_PRICING_ENGINE';
	public static final string SFDC_TO_NAV_SHIPTO_PRICING_ENGINE = 'SFDC_TO_NAV_SHIPTO_PRICING_ENGINE';
	public static final string SFDC_TO_NAV_SHIPTO_EDIT = 'SFDC_TO_NAV_SHIPTO_EDIT';
	public static final string SHIP_TO_EDIT = 'SHIP_TO_EDIT';
	public static final string SFDC_TO_WINDMS_SHIPTO_EDIT = 'SFDC_TO_WINDMS_SHIPTO_EDIT';	
	public static final string CALLSFDCTOLOMOSOFTFUTURE = 'callSfdcToLomoSoftFuture';
    public static final String EDIT_ACCOUNTS_NAV_AND_LOMO_CALLOUT = 'editAccountsNAVAndLomoCallout';
    public static final string CALLSFDCTONAVFUTURE = 'callSfdcToNavFuture';
    public static final string NAV = 'NAV';
    public static final string CUSTOMER_UPDATE = 'CUSTOMER_UPDATE';
    public static final string PRICINGENGINECALLOUT = 'pricingEngineCallout';
    public static final string CALLSFDCTONAVSHIPTOEDIT = 'callSfdcToNavShipToEdit'; 	
    public static final string SFDC_TO_NAV_CUSTOMER_CREATION = 'SFDC_TO_NAV_CUSTOMER_CREATION';
    public static final string SFDC_TO_WINDMS_CUSTOMER_CREATION = 'SFDC_TO_WINDMS_CUSTOMER_CREATION';
    public static final string SFDC_TO_NAV_CUSTOMER_EDIT =  'SFDC_TO_NAV_CUSTOMER_EDIT';
    public static final string SFDC_TO_WINDMS_CUSTOMER_EDIT = 'SFDC_TO_WINDMS_CUSTOMER_EDIT';
    public static final string CUSTOMER_EDIT = 'CUSTOMER_EDIT';    
    public static final string VENDOR = 'Vendor';
    public static final String DELIVERY = 'Delivery';
    public static final string BUSINESSCHANNELS = 'BUSINESSCHANNELS'; 
    public static final string BUSINESSSEGMENTS = 'BUSINESSSEGMENTS';
    public static final string AUD = 'AUD';
    public static final string STORAGE_SHIP_TO = 'StorageShipTo';
    public static final string DUMMY_SELL_TO = 'DummySellTo'; 
    public static final string VMI = 'VMI';
    public static final string NON_VMI = 'NonVMI';
    public static final string STORAGE_LOCATOIN = 'StorageLocation';
    public static final string POSITIVE = 'Positive';
    public static final string NEGATIVE = 'Negative';
    public static final string PREPAYMENT = 'Prepayment';
    public static final string TRANSPORTER_VENDOR_TYPE = 'Transporter';
    public static final string THIRD_PARTY_STOCK_SUPPLIER_VENDOR_TYPE = '3rd Party Stock Supplier';
    public static final set<string> OPEN_ORDER_STATUS_FOR_TRANSPORTER = new Set<String>{'Submitted', 'Accepted', 'Planning', 'Planned', 'Loaded', 'Awaiting Inventory Review', 'Awaiting Credit Review'};
    public static final string NAV_STOCK_LOCATION_ID_FOR_TEST = 'ABCDEXYZ';
    public static final string PROSPECT_TO_BASICDATA_MESSAGE = '01-Prospect status value can only be changed to 02-Basic Data Setup'; 
	//L4_45352_START
    //Create customer payload variables
    public static final String CUSTOMERS = 'customers';
    public static final String CUSTOMER = 'customer';
    public static final String SEQID = 'seqId';
    public static final String IDENTIFIER = 'identifier';
    public static final String CUSTID = 'custId';
    public static final String ENTRPSID = 'entrpsId';
    public static final String CUSTCATGRY = 'custCatgry';
    public static final String NAME = 'name';
    public static final String NAME2 = 'name2';
    public static final String ADDRESS = 'address';
    public static final String CITY = 'city';
    public static final String PHONE = 'phone';
    public static final String MOBILEPHONE = 'mobilePhone';
    public static final String CURRENCYID = 'currencyId';
    public static final String LANGCODE = 'langCode';
    public static final String CNTRYCODE = 'cntryCode';
    public static final String BILLTOCUSTID = 'billToCustId';
    public static final String FAX = 'fax';
    public static final String COMPANYTAXID = 'companyTaxId';
    public static final String POSTCODE = 'postCode';
    public static final String COUNTY = 'county';
    public static final String EMAIL = 'email';
    public static final String WEBSITE = 'website';
    public static final String PMTMTHDID = 'pmtMthdId';
    public static final String RQSTDPYMNTTERM = 'rqstdPymntTerm';
    public static final String RQSTDPCKGDPYMNTTERM = 'rqstdPckgdPymntTerm';
    public static final String BILLBASIS = 'billBasis';
    public static final String BILLCONSOLBASIS = 'billConsolBasis';
    public static final String DLVRYTYPE = 'dlvryType';
    public static final String MANUALINVCINGALLOWED = 'manualInvcingAllowed';
    public static final String SALESPERSONCODE ='salesPersonCode';
    public static final String ISVATEXEMPTED = 'isVatExempted';
    public static final String DEFERINVOICE = 'deferInvoice';
    public static final String DEFERUPPERLMT = 'deferUpperLmt';
    public static final String ALLOWRLSONPYMNTPROOF = 'allowRlsOnPymntProof';
    public static final String ACNTSTMNTFRQNCY = 'acntStmntFrqncy';
    public static final String DAILY = 'daily';
    public static final String WEEKLY = 'weekly';
    public static final String FORTNIGHTLY = 'fortnightly';
    public static final String MONTHLY = 'monthly';
    public static final String STMNTTYPALL = 'stmntTypAll';
    public static final String STMNTTYPOPEN = 'stmntTypOpen';
    public static final String COMBINEDINVOICING = 'combinedInvoicing';
    public static final String BUSINESSSEGMENT = 'businessSegment';
    public static final String BUSINESSCHANNEL = 'businessChannel';
    public static final String EXCISEDUTY = 'exciseDuty';
    public static final String ISPOREQ = 'isPOReq';
    public static final String POREQBY = 'poReqBy';
    public static final String PONR = 'poNr';
    public static final String INVCDUEDTBSDON = 'invcDueDtBsdOn';
    public static final String CUSTACTIVATIONDATE = 'custActivationDate';
    public static final String CUSTSTATUS = 'custStatus';
    public static final String SEARCHNAME = 'searchName';
    public static final String TRADINGNAME = 'tradingName';
    public static final String TELEXNO = 'telexNo';
    public static final String ADDRESS2 = 'address2';
    public static final String LOGOID = 'logoId';
    public static final String ISCAPTIVCUST = 'isCaptivCust';
    public static final String PMTCRDLMT = 'pmtCrdLmt';
    public static final String DISPLAYPRICE = 'displayPrice';
    public static final String REPRCNGONAVGPRDCPRCNG = 'reprcngOnAvgPrdcPrcng';
    public static final String CNSLDTDINVCPRNTOPTNOPTION = 'cnsldtdInvcPrntOptnOption';
    public static final String KYCTYPE = 'kycType';
    public static final String KYCUPDTDBY = 'kycUpdtdBy';
    public static final String KYCUPDATEDON = 'kycUpdatedOn';
    public static final String KYCSTATUS = 'kycStatus';
    public static final String REASONFORKYCRJCTN = 'reasonForKycRjctn';
    public static final String PRFRDBANKACC = 'prfrdBankAcc';
    public static final String VATREGNR = 'vatRegNr';
    public static final String SHIPTOADDRESSES = 'shipToAddresses';
    
    public static final String SHIPTOADDRESS = 'shipToAddress';

    public static final String SHIPTOID = 'shipToId';
    public static final String ENTRPRSID = 'entrprsId';
    public static final String STATUS = 'status';
    public static final String SHIPTOTYPE = 'shipToType';
    public static final String MODIFYBY = 'modifyBy'; 
    public static final String MODIFYON = 'modifyOn';
    public static final String ISVMI = 'isVMI';
    public static final String TANKS = 'tanks';
    public static final String TANK = 'tank';
	public static final String TANKID = 'tankId';
	public static final String UOM = 'uom';
	public static final String SAFEFILLLEVEL = 'safeFillLevel';
	public static final String PRODUCTID = 'productId';
	public static final String CAPACITY = 'capacity';
	public static final String DEADSTOCK = 'deadstock';
	public static final String TANKSTATUS = 'tankStatus';
	public static final String ACTIVEFROM = 'activeFrom'; 
	public static final String ACTIVETO = 'activeTo';
	public static final String AVAILABLESTOCKLASTUPDATED = 'availableStockLastUpdated';
	public static final String TANKDIPENTRYMODE = 'tankDipEntryMode';
	
	public static final String SUPPLYLOCATIONS = 'supplyLocations';
	public static final String SUPPLYLOCATION = 'supplyLocation';
	public static final String STOCKHLDNGLOCID = 'stockHldngLocId';
	public static final String DISTANCE = 'distance';
	public static final String DEFAULTSTR = 'default';
	
    /*
    public static final String CUSTID = 'custId';    
    public static final String IDENTIFIER = 'identifier';
    public static final String NAME = 'name';
    public static final String NAME2 = 'name2';
    public static final String CNTRYCODE = 'cntryCode';
    public static final String ADDRESS = 'address';
    public static final String POSTCODE = 'postCode';
    public static final String CITY = 'city';
    public static final String PHONE = 'phone';
    public static final String MOBILEPHONE = 'mobilePhone';
    public static final String WEBSITE = 'webSite';
    public static final String FAX = 'fax';
    public static final String EMAIL = 'email';
    public static final String COUNTY = 'county';
    
    public static final String BLOCKEDTAG = 'blocked';    
    */
    public static final String BLOCKEDTAG = 'blocked'; 
    public static final String CUSTBANKS = 'custBanks';
    public static final String CUSTBANK ='custBank';

    public static final String BANKACCID = 'bankAccId';

    public static final String BANKBRANCHID = 'bankBranchId';
    public static final String BANKACCOUNTNO = 'bankAccountNo';
    public static final String TRANSITNO = 'transitNo';
    //public static final String CURRENCYID = 'currencyId';
    //public static final String CNTRYCODE = 'cntryCode';
    //public static final String COUNTY = 'county';
    //public static final String WEBSITE = 'website';
    public static final String IBAN = 'iBAN';
    public static final String SWIFTCODE = 'swiftCode';
    public static final String BANKCLEARCODE = 'bankClearCode';
    public static final String BANKCLEARINGSTD = 'bankClearingStd';
    //public static final String ADDRESS2 = 'address2';
    public static final String CONTACTNAME = 'contactName';
    //public static final String TELEXNO = 'telexNo';
    public static final String TELEXANSBCK = 'telexAnsBck';
    public static final String GIROACCNO = 'giroAccNo';
    //public static final String LANGCODE = 'langCode';
    public static final String CORRBANKNAME = 'corrBankName';
    public static final String CORRBANKCNTRYCODE = 'corrBankCntryCode';
    public static final String CORRBANKSWIFTCODE = 'corrBankSwiftCode';
    public static final String CORRBANKCLEARCODE = 'corrBankClearCode';
    public static final String ACCOUNTUSEDFOR = 'accountUsedFor';
    public static final String ROUTENO = 'routeNo';
    public static final String CODE = 'code';
    public static final String TEXTKEY = 'textKey';

    public static final String CLIENTID = 'clientId';
    
    public static final String EXRACK = 'Ex-Rack';
	public static final String TANKCODE = 'tankCode';
    public static final string CASHONDELIVERY = 'Cash on Delivery';
    public static final string STATUS_PAYLOAD = 'StatusPayload';
    public static final String PAYLOAD = 'Payload';
    public static final String ANY0 = 'any0';
    public static final string MSG = 'MSG';
    public static final string COMPLETE = '03-Completed';
    public static final string ACTION_CONSIGNMENT_REVIEW_RT = '03-Completed';
    
    //L4 #45362 Code changes 
    public static final string CONSIGNMENT_LOC_NOT_LINKED = 'Consignment location not linked';
    public static final string BLOCK_SHIPTO = 'Block Ship';
 	
    //E2E fixes
    public static final string BILLMTHD = 'billMthd'; 
    public static final string RQSTDPMTMTHDID = 'rqstdPmtMthdId';
        public static final string REQPMTCRDLMT  ='reqPmtCrdLmt';
    //public static final string PRFRDBANKACC = 'prfrdBankAcc';
    public static final string VERSIONNR ='versionNr';
    public static final string ISACTIVESTATUS = 'isActiveStatus';
    public static final string REQPTMTRM = 'reqPmtTrm';
    public static final string PCKGDRQSTDPYMNTTERM = 'pckgdRqstdPymntTerm';
    public static final string custRefVsbl = 'custRefVsbl';
    public static final string BILLFRQNCY = 'billFrqncy';
    public static final string PRICECONSOLBASIS = 'priceConsolBasis';
    /**Bug-71691**/
    public static final string KYC_Review = 'EP_KYC_Review';
    /**Bug-71691**/
    //SIT-Bug-74051
   public static final string XPOS = 'xPos'; 
    public static final string YPOS = 'yPos'; 
    //SIT-Bug-74051
    /**TFSId-76105**/
    public static final string  REASONFORSTATUSCHNG = 'reasonForStatusChng';
    public static final string TANKALIAS = 'tankAlias';
    /**TFSId-76105**/
}