@isTest
public class EP_ASTSellToProspectToBasicDataSetup_UT
{
    static final string EVENT_NAME = '01-ProspectTo02-BasicDataSetup';
    static final string INVALID_EVENT_NAME = 'NewTo02-Basic Data Setup';
    /*  
       @description: method to intialise data
    */
    @testSetup static void init() {
      List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
      List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }
    static testMethod void isTransitionPossible_positive_test() {
        Account acc =  EP_TestDataUtility.getSellToASProspectDomainObjectPositiveScenario().localAccount;
        EP_Payment_Term__c payTerm = EP_AccountTestDataUtility.getCreditPaymentTerm();
        acc.EP_Requested_Packaged_Payment_Terms__c = payTerm.id;
        acc.EP_RequestedPaymentTerms__c = payTerm.id;
        acc.EP_Recommended_Credit_Limit__c = 5000000;
        update acc;
        EP_AccountDomainObject obj = new EP_AccountDomainObject(acc.Id);
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME );
        EP_ASTSellToBasicDataToBasicData ast = new EP_ASTSellToBasicDataToBasicData();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isTransitionPossible_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASProspectDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME );
        EP_ASTSellToProspectToBasicDataSetup ast = new EP_ASTSellToProspectToBasicDataSetup();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isRegisteredForEvent_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASProspectDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME );
        EP_ASTSellToProspectToBasicDataSetup ast = new EP_ASTSellToProspectToBasicDataSetup();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isRegisteredForEvent_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASProspectDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME );
        EP_ASTSellToProspectToBasicDataSetup ast = new EP_ASTSellToProspectToBasicDataSetup();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    //Recommended Credit Limit condition
    static testMethod void isGuardCondition_Step0_test() {
        //EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectPositiveScenario();
        Account acc =  EP_TestDataUtility.getSellToASProspectDomainObjectPositiveScenario().localAccount;
        EP_Payment_Term__c payTerm = EP_AccountTestDataUtility.getCreditPaymentTerm();
        acc.EP_Requested_Packaged_Payment_Terms__c = payTerm.id;
        acc.EP_RequestedPaymentTerms__c = payTerm.id;
        update acc;
        EP_AccountDomainObject obj = new EP_AccountDomainObject(acc.Id);
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME );
        EP_ASTSellToProspectToBasicDataSetup ast = new EP_ASTSellToProspectToBasicDataSetup();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(false,result); 
    }
    //EXRACK Condition
    static testMethod void isGuardCondition_Step1_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectPositiveScenario();
        Account acc = obj.localAccount;
        EP_Payment_Term__c payTerm = EP_AccountTestDataUtility.getCreditPaymentTerm();
        acc.EP_Delivery_Type__c = EP_AccountConstant.EXRACK;
        acc.EP_Requested_Packaged_Payment_Terms__c = payTerm.id;
        acc.EP_RequestedPaymentTerms__c = payTerm.id;
        acc.EP_Recommended_Credit_Limit__c = 5000000;
        update acc;
        EP_Stock_Holding_Location__C sch = [select id,EP_Is_Pickup_Enabled__c from EP_Stock_Holding_Location__C Where EP_Sell_To__c =: acc.Id Limit 1];
        sch.EP_Is_Pickup_Enabled__c = false;
        update sch;
        EP_AccountDomainObject obj2 = new EP_AccountDomainObject(acc.Id);
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME );
        EP_ASTSellToProspectToBasicDataSetup ast = new EP_ASTSellToProspectToBasicDataSetup();
        ast.setAccountContext(obj2,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    //DELIVERY Condition
    static testMethod void isGuardCondition_Step2_test() {
        EP_AccountDomainObject obj1 = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectPositiveScenario();
        Account acc = EP_TestDataUtility.createSellToAccountWithDirectPaymentSelected();        
        EP_Payment_Term__c payTerm = EP_AccountTestDataUtility.getCreditPaymentTerm();
        acc.EP_Delivery_Type__c = EP_AccountConstant.DELIVERY;
        acc.EP_Requested_Packaged_Payment_Terms__c = payTerm.id;
        acc.EP_RequestedPaymentTerms__c = payTerm.id;
        acc.EP_Recommended_Credit_Limit__c = 5000000;
        update acc;
        EP_AccountDomainObject obj2 = new EP_AccountDomainObject(acc.Id);
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME );
        EP_ASTSellToProspectToBasicDataSetup ast = new EP_ASTSellToProspectToBasicDataSetup();
        ast.setAccountContext(obj2,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    //supply option check 
    static testMethod void isGuardCondition_Step4_test() {
        Account acc =  EP_TestDataUtility.getSellToPositiveScenario();        
        EP_Stock_Holding_Location__C sch = [select id,EP_Is_Pickup_Enabled__c from EP_Stock_Holding_Location__C Where EP_Sell_To__c =: acc.Id Limit 1];
        delete sch;
        EP_AccountDomainObject obj = new EP_AccountDomainObject(acc.Id);
        system.debug('====EP_Requested_Packaged_Payment_Terms__c==='+obj.localAccount.EP_Requested_Packaged_Payment_Terms__r.name);
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME );
        EP_ASTSellToProspectToBasicDataSetup ast = new EP_ASTSellToProspectToBasicDataSetup();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    static testMethod void isGuardCondition_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASProspectDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME );
        EP_ASTSellToProspectToBasicDataSetup ast = new EP_ASTSellToProspectToBasicDataSetup();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(false,result);
    } 
    //L4# 78534 - Test Class Fix - Start 
    //Bank Account check 
    static testMethod void isGuardCondition_BankAccount_test() {
        EP_Payment_Method__c pmt = EP_TestDataUtility.createPaymentMethod(EP_Common_Constant.PM_DIRECT_DEBIT_CODE,EP_Common_Constant.PM_DIRECT_DEBIT_CODE);
        insert pmt;
        
        Account acct = EP_TestDataUtility.getSellToPositiveScenario();
        acct = EP_TestDataUtility.createSellToAccountAndItsSupplyLocation();
        acct.EP_Requested_Payment_Method__c = EP_Common_Constant.DIRECT_DEBIT;
        acct.EP_RequestedPaymentMethod__c = pmt.id;
        update acct;
       
        EP_AccountDomainObject accountDomainObject = new EP_AccountDomainObject(acct.id);
        EP_AccountService localObj = new EP_AccountService(accountDomainObject);
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME );
        EP_ASTSellToProspectToBasicDataSetup ast = new EP_ASTSellToProspectToBasicDataSetup();
        ast.setAccountContext(accountDomainObject,ae);
        
        Test.startTest();
            boolean result = ast.isGuardCondition();
        Test.stopTest();
        
        System.AssertEquals(true,result);
    }
    //L4# 78534 - Test Class Fix - End 
}