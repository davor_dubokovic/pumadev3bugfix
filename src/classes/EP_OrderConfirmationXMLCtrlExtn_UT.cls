@isTest
public class EP_OrderConfirmationXMLCtrlExtn_UT {
    
    static testMethod void getCompanyDetails_test() {
        csord__Order__c ord = [SELECT Id, EP_Puma_Company_Code__c FROM csord__Order__c WHERE Id =:EP_TestDataUtility.getConsumptionOrderPositiveScenario().id];
        PageReference pageRef = Page.EP_OrderConfirmationWithOPMSXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.SOURCE_COMPANY,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE , '1234');
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , ord.Id);
        EP_OrderConfirmationXMLCtrlExtn localObj = new EP_OrderConfirmationXMLCtrlExtn(sc);
        Test.startTest();
        Company__c result = localObj.getCompanyDetails();
        Test.stopTest();
        Company__c company = [select id,EP_Company_Code__c from Company__c where id=: result.Id];
        System.AssertEquals(true,result != NULL && company.EP_Company_Code__c.Equals(ord.EP_Puma_Company_Code__c));
    }
    static testMethod void setOrderDetails_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
        
        ord.csord__Status2__c = EP_Common_Constant.ORDER_DRAFT_STATUS;
        update ord;
        
        PageReference pageRef = Page.EP_OrderConfirmationWithOPMSXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.SOURCE_COMPANY,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE , '1234');
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , ord.Id);
        EP_OrderConfirmationXMLCtrlExtn localObj = new EP_OrderConfirmationXMLCtrlExtn(sc);
        Test.startTest();
        localObj.setOrderDetails();
        Test.stopTest();
        System.AssertEquals(true,localObj.docTitle.equalsIgnoreCase(EP_Common_Constant.SALESQUOTES_TITLE));
    }
    static testMethod void setRelatedAccountDetails_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
        PageReference pageRef = Page.EP_OrderConfirmationWithOPMSXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.SOURCE_COMPANY,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE , '1234');
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , ord.Id);
        EP_OrderConfirmationXMLCtrlExtn localObj = new EP_OrderConfirmationXMLCtrlExtn(sc);
        Test.startTest();
        localObj.setRelatedAccountDetails();
        Test.stopTest();
        System.AssertEquals(true,localObj.relatedAccountsMap.size() > 0); 
    }
    static testMethod void getStorageLocAcct_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
        PageReference pageRef = Page.EP_OrderConfirmationWithOPMSXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.SOURCE_COMPANY,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE , '1234');
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , ord.Id);
        EP_OrderConfirmationXMLCtrlExtn localObj = new EP_OrderConfirmationXMLCtrlExtn(sc);
        //localObj.setRelatedAccountDetails();
        Test.startTest();
        Account result = localObj.getStorageLocAcct();
        Test.stopTest();
        EP_OrderMapper ordMapObj = new EP_OrderMapper();
        list<csord__Order__c> ordList = ordMapObj.getCSRecordsByIds(new set<id> {ord.Id});
        System.AssertEquals(true,ordList.get(0).Stock_Holding_Location__r.Stock_Holding_Location__c.equals(result.Id));
    }
    static testMethod void getShipToAcct_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
        PageReference pageRef = Page.EP_OrderConfirmationWithOPMSXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.SOURCE_COMPANY,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE , '1234');
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , ord.Id);
        EP_OrderConfirmationXMLCtrlExtn localObj = new EP_OrderConfirmationXMLCtrlExtn(sc);
        Test.startTest();
        Account result = localObj.getShipToAcct();
        Test.stopTest();
        EP_OrderMapper ordMapObj = new EP_OrderMapper();
        list<csord__Order__c> ordList = ordMapObj.getCSRecordsByIds(new set<id> {ord.Id});
        System.AssertEquals(true,ordList.get(0).EP_ShipTo__c.equals(result.Id));
    }
    
    /*
    static testMethod void getTransporterAcct_test() {
        
        csord__Order__c ord = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
        
        Account vendor = EP_TestDataUtility.getVendor();
        vendor.recordtypeid = EP_Common_Util.fetchRecordTypeId('Account', 'Vendor');
        update vendor;

        ord.EP_Transporter__c= vendor.Id;
        update ord;
        
        PageReference pageRef = Page.EP_OrderConfirmationWithOPMSXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.SOURCE_COMPANY,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE , '1234');
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , ord.Id);
        EP_OrderConfirmationXMLCtrlExtn localObj = new EP_OrderConfirmationXMLCtrlExtn(sc);
        localObj.setRelatedAccountDetails();
        Test.startTest();
        Account result = localObj.getTransporterAcct();
        Test.stopTest();
        System.AssertEquals(true,ord.EP_Transporter__c.equals(result.Id));
    }*/
    
    @isTest
    public static void getOrderLineItems_test() {
        List<csord__Order__c>  orderList= new List<csord__Order__c>();
        csord__Order__c ord = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
      
        
        
    cscfga__Product_Basket__c basketInsert = new cscfga__Product_Basket__c(
            Name = 'New Basket',
            OwnerId = UserInfo.getUserId()
        );
        insert basketInsert;
 
       
        Id pricebookId = Test.getStandardPricebookId();
        Account acc = [select id,ep_price_consolidation_basis__c,EP_Puma_Company_Code__c from Account limit 1];
        
        csord__Order__c ord1 = new csord__Order__c();
        ord1.Name = 'Sales Order';
        ord1.csord__Account__c = acc.id;
        ord1.Requested_Date__c = date.today();
        ord1.csord__Identification__c = basketInsert.id;
        ord1.PriceBook2Id__c = pricebookId;
        
        ord1.RepriceRequired__c = true;
        ord1.pricingResponseJson__c = '';
        insert ord1; 
        String attachName = 'detailedPricingResponse';
        list<Attachment> lineItems = [SELECT Id, Name,body, ParentId, Parent.Type FROM Attachment where ParentId =: ord1.csord__Identification__c and name =:attachName];
        Map<String, Object> inputMap = new Map<String, Object>();
         inputMap.put('BasketId', basketInsert.id);
         inputMap.put('CompanyCode', acc.EP_Puma_Company_Code__c);
         inputMap.put('priceConsolidationBasis', acc.ep_price_consolidation_basis__c); 
        
        Map<String, Object> callResult = TrafiguraGetPricesDetailed.doPriceCallDetailed(inputMap);

         String outcome = 'OK'; 
        PageReference pageRef = Page.EP_OrderConfirmationWithOPMSXML;
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(ord1);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.SOURCE_COMPANY,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE , '1234');
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , ord1.Id);
        
        EP_OrderConfirmationXMLCtrlExtn localObj = new EP_OrderConfirmationXMLCtrlExtn(sc);

        Test.startTest();
        
        LIST<csord__Order_Line_Item__c> result = localObj.getOrderLineItems();
        
        Test.stopTest();
        
        System.AssertEquals(true, result.isEmpty());
    }
    
     @isTest
    public static void getOrderLineItems1_test() {
        
        csord__Order__c ord = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
           
        cscfga__Product_Basket__c basketInsert = new cscfga__Product_Basket__c(
            Name = 'New Basket',
            OwnerId = UserInfo.getUserId()
        );
        insert basketInsert;
 
       
        Id pricebookId = Test.getStandardPricebookId();
        Account acc = [select id,ep_price_consolidation_basis__c,EP_Puma_Company_Code__c from Account limit 1];
        csord__Order__c ord2 = [SELECT Id,EP_Puma_Company_Code__c,csord__Identification__c FROM csord__Order__c limit 1];
        
        ord2.pricingResponseJson__c = '{ "Payload" : { "any0" : { "pricingResponse" : { "priceDetails" : { "priceDetails" : { "seqId" : "0769ee1cc7303ad6d8-6f2fec360ba48a-d58a", "companyCode" : "EPA", "priceType" : "Fuel Price", "priceRequestSource" : "", "currencyCode" : "USD", "deliveryType" : "Delivery", "customerId" : "00005398", "shipToId" : "00005401", "supplyLocationId" : "9311", "transporterId" : "", "supplierId" : "9311", "priceDate" : "2018-04-27", "onRun" : "", "orderId" : "", "applyOrderQuantity" : "No", "totalOrderQuantity" : "1.00", "priceConsolidationBasis" : "Summarised price", "versionNr" : ""}, "totalAmt" : { "totalAmount" : "3.91", "totalAmountLCY" : "3.91", "totalTaxAmount" : "0.00", "totalTaxAmountLCY" : "0.00", "totalDiscountAmount" : "0.00", "totalDiscountAmountLCY" : "0.00"}, "priceLineItems" : { "lineItem":[ { "lineItemInfo" : { "seqId" : "0769ee1cc7303ad6d8-6f2fec360ba48a-d58a", "orderId" : "", "lineItemId" : "100087", "itemId" : "100087", "quantity" : "1.00", "unitPrice" : "0.97776", "unitPriceLCY" : "0.97776", "invoiceDetails" : { "invoiceComponent":[ { "seqId" : "0769ee1cc7303ad6d8-6f2fec360ba48a-d58a", "type" : "Fuel Price", "name" : "Total Price", "amount" : "0.97776", "amountLCY" : "0.97776", "taxPercentage" : "0.00", "taxAmount" : "0.00000", "taxAmountLCY" : "0.00000", "totalAmount" : "0.97776", "totalAmountLCY" : "0.97776"},{ "seqId" : "0769ee1cc7303ad6d8-6f2fec360ba48a-d58a", "type" : "Fuel Price", "name" : "Total Price", "amount" : "0.97776", "amountLCY" : "0.97776", "taxPercentage" : "0.00", "taxAmount" : "0.00000", "taxAmountLCY" : "0.00000", "totalAmount" : "0.97776", "totalAmountLCY" : "0.97776"} ] }, "acctComponents" : { "acctComponent":[ { "seqId" : "0769ee1cc7303ad6d8-6f2fec360ba48a-d58a", "componentCode" : "EFE", "glAccountNr" : "", "baseAmount" : "0.97776", "baseAmountLCY" : "0.97776", "taxRate" : "0.00", "taxAmount" : "0.00000", "taxAmountLCY" : "0.00000", "totalAmount" : "0.97776", "totalAmountLCY" : "0.97776", "isVAT" : "No"},{ "seqId" : "0769ee1cc7303ad6d8-6f2fec360ba48a-d58a", "componentCode" : "EFE", "glAccountNr" : "", "baseAmount" : "0.97776", "baseAmountLCY" : "0.97776", "taxRate" : "0.00", "taxAmount" : "0.00000", "taxAmountLCY" : "0.00000", "totalAmount" : "0.97776", "totalAmountLCY" : "0.97776", "isVAT" : "No"} ] }}, "error" : { "ErrorCode" : "", "ErrorDescription" : ""}, "error" : { "ErrorCode" : "", "ErrorDescription" : ""}},{ "lineItemInfo" : { "seqId" : "0769ee1cc7303ad6d8-6f2fec360ba48a-d58a", "orderId" : "", "lineItemId" : "70054", "itemId" : "70054", "quantity" : "1.00", "unitPrice" : "0.97776", "unitPriceLCY" : "0.97776", "invoiceDetails" : { "invoiceComponent":[ { "seqId" : "0769ee1cc7303ad6d8-6f2fec360ba48a-d58a", "type" : "Fuel Price", "name" : "Total Price", "amount" : "0.97776", "amountLCY" : "0.97776", "taxPercentage" : "0.00", "taxAmount" : "0.00000", "taxAmountLCY" : "0.00000", "totalAmount" : "0.97776", "totalAmountLCY" : "0.97776"},{ "seqId" : "0769ee1cc7303ad6d8-6f2fec360ba48a-d58a", "type" : "Fuel Price", "name" : "Total Price", "amount" : "0.97776", "amountLCY" : "0.97776", "taxPercentage" : "0.00", "taxAmount" : "0.00000", "taxAmountLCY" : "0.00000", "totalAmount" : "0.97776", "totalAmountLCY" : "0.97776"} ] }, "acctComponents" : { "acctComponent":[ { "seqId" : "0769ee1cc7303ad6d8-6f2fec360ba48a-d58a", "componentCode" : "EFE", "glAccountNr" : "", "baseAmount" : "0.97776", "baseAmountLCY" : "0.97776", "taxRate" : "0.00", "taxAmount" : "0.00000", "taxAmountLCY" : "0.00000", "totalAmount" : "0.97776", "totalAmountLCY" : "0.97776", "isVAT" : "No"},{ "seqId" : "0769ee1cc7303ad6d8-6f2fec360ba48a-d58a", "componentCode" : "EFE", "glAccountNr" : "", "baseAmount" : "0.97776", "baseAmountLCY" : "0.97776", "taxRate" : "0.00", "taxAmount" : "0.00000", "taxAmountLCY" : "0.00000", "totalAmount" : "0.97776", "totalAmountLCY" : "0.97776", "isVAT" : "No"} ] }}, "error" : { "ErrorCode" : "", "ErrorDescription" : ""}, "error" : { "ErrorCode" : "", "ErrorDescription" : ""}} ] }, "Error" : { "ErrorCode" : "", "ErrorDescription" : ""}}}}}}';
        update ord2;        
        system.debug('ord2'+ord2);
        csord__Order_Line_Item__c oli = [SELECT csord__Order__c,CS_OrderLine_Number__c,EP_Product_Code__c,Id FROM csord__Order_Line_Item__c where csord__Order__c =: ord2.id];
        oli.EP_Product_Code__c = '100087';
        oli.CS_OrderLine_Number__c = 100087;
        update oli;
        system.debug('olineItem'+oli);
       
        set<Id> idSet = new set<Id>();
        idSet.add(ord2.id);
        String attachName = 'detailedPricingResponse';
        list<Attachment> lineItems = [SELECT Id, Name,body, ParentId, Parent.Type FROM Attachment where ParentId =: ord2.csord__Identification__c and name =:attachName];
        Map<String, Object> inputMap = new Map<String, Object>();
         inputMap.put('BasketId', basketInsert.id);
         inputMap.put('CompanyCode', acc.EP_Puma_Company_Code__c);
         inputMap.put('priceConsolidationBasis', acc.ep_price_consolidation_basis__c); 
         system.debug('BasketId'+basketInsert.id);
         system.debug('CompanyCode'+acc.EP_Puma_Company_Code__c);
         system.debug('priceConsolidationBasis'+acc.ep_price_consolidation_basis__c);
        Map<String, Object> callResult = TrafiguraGetPricesDetailed.doPriceCallDetailed(inputMap);
        
        system.debug('test1234'+inputMap);
         String outcome = 'OK'; 
        PageReference pageRef = Page.EP_OrderConfirmationWithOPMSXML;
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(ord2);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.SOURCE_COMPANY,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE , '1234');
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , ord2.Id);
        
        EP_OrderConfirmationXMLCtrlExtn localObj = new EP_OrderConfirmationXMLCtrlExtn(sc);

        EP_OrderItemMapper ordItemMapper = new EP_OrderItemMapper();
        ordItemMapper.getCSOrderById(idSet);
        system.debug('orderLineitem'+ordItemMapper.getCSOrderById(idSet));
        EP_PricingResponseStub.Payload stub = new EP_PricingResponseStub.Payload();
        Test.startTest();
        
        LIST<csord__Order_Line_Item__c> result = localObj.getOrderLineItems();
        
        Test.stopTest();
        
        System.AssertEquals(false, result.isEmpty());
    }
    
    static testMethod void getSellToAcct_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
        PageReference pageRef = Page.EP_OrderConfirmationWithOPMSXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.SOURCE_COMPANY,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE , '1234');
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , ord.Id);
        EP_OrderConfirmationXMLCtrlExtn localObj = new EP_OrderConfirmationXMLCtrlExtn(sc);
        Test.startTest();
        Account result = localObj.getSellToAcct();
        Test.stopTest();
        EP_OrderMapper ordMapObj = new EP_OrderMapper();
        list<csord__Order__c> ordList = ordMapObj.getCSRecordsByOrderNumber(new set<String> {ord.orderNumber__c});
        System.AssertEquals(true,ordList.get(0).EP_Sell_To__c.equals(result.Id));
    }
    static testMethod void setDocTitle_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
        ord.csord__Status2__c = EP_Common_Constant.ORDER_DRAFT_STATUS;
        
        update ord;
        
        PageReference pageRef = Page.EP_OrderConfirmationWithOPMSXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.SOURCE_COMPANY,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE , '1234');
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , ord.Id);
        EP_OrderConfirmationXMLCtrlExtn localObj = new EP_OrderConfirmationXMLCtrlExtn(sc);
        Test.startTest();
        localObj.setDocTitle();
        Test.stopTest();
        System.AssertEquals(true,localObj.docTitle.equalsIgnoreCase(EP_Common_Constant.SALESQUOTES_TITLE));
    }
    static testMethod void checkPageAccess_WithoutSecretCode() {
        csord__Order__c ord = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
        PageReference pageRef = Page.EP_OrderConfirmationWithOPMSXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.SOURCE_COMPANY,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , ord.Id);
        EP_OrderConfirmationXMLCtrlExtn localObj = new EP_OrderConfirmationXMLCtrlExtn(sc);
        Test.startTest();
        PageReference result = localObj.checkPageAccess();
        Test.stopTest();
        System.AssertEquals(true,result != NULL);
        System.AssertEquals(true,result.getURL().toUpperCase().contains(EP_Common_Constant.UNAUTHORIZEDACCESSPAGESTR.toUpperCase()));
    }
    static testMethod void checkPageAccess_WithSecretCode() {
        csord__Order__c ord = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
        PageReference pageRef = Page.EP_OrderConfirmationWithOPMSXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(ord);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_MESSAGEID,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.SOURCE_COMPANY,EP_Common_Constant.TEMPSTRINGWITHHYPHEN);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE , '1234');
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , ord.Id);
        EP_OrderConfirmationXMLCtrlExtn localObj = new EP_OrderConfirmationXMLCtrlExtn(sc);
        Test.startTest();
        PageReference result = localObj.checkPageAccess();
        localObj.getTransporterAcct();
        Test.stopTest();
        System.AssertEquals(true,result == NULL);
    }
}