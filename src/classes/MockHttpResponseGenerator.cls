@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock{
    
    global HttpResponse respond(HTTPRequest req)
    {
        HttpResponse httpResponse = new HttpResponse();
        httpResponse.setStatusCode(200);
        httpResponse.setHeader('Content-Type', 'application/json');
        httpResponse.setBody('{"message":"success"}');
        return httpResponse;
    }

}