/*********************************************************************************************
            @Author <>
            @name <TQAppController >
            @CreateDate <>
            @Description <>  
            @Version <1.0>
    *********************************************************************************************/
global with sharing class TQAppController {

    
    public static final String LOGIN_PAGE = 'ACNONE_login';
    public static final String AUTH_USER_PAGE = 'ACNONE_test';
    public static final String USER_TYPE = 'Guest';
    public static final String ERROR_MSG1 = 'Password and verification do not match';
    public static final String ERROR_MSG2 = 'Change is not successfull. ';
    public static final String LOGIN_ERROR = 'Login failed. Please make sure your username and password are correct.';

    public String authUsername {get; set;}
    public String authPassword {get; set;}
    public String loginStatus {get; set;}
    public String adrumAppKey {get {
        List<EP_FE_Configuration__mdt> bootstrapConfigurations = [
            SELECT EP_FE_Configuration_Value__c 
            FROM EP_FE_Configuration__mdt 
            WHERE EP_FE_Configuration_Type__c = :EP_FE_Constants.CONFIG_BOOTSTRAP AND DeveloperName = :EP_FE_Constants.APP_DYNAMICS_KEY limit 1000];
        return (bootstrapConfigurations == null || bootstrapConfigurations.size() > 0)
            ? bootstrapConfigurations[0].EP_FE_Configuration_Value__c
            : '';
    }set;}
    
    global String shouldConfirmTermsAndConditions {
    	get {
    		if (shouldConfirmTermsAndConditions == null){
		        User runningUser = [Select EP_FE_Latest_T_C_accepted_date_time__c, EP_FE_Latest_T_C_accepted_Version__c,
		        				   Contact.Account.EP_Country__c
		            FROM User
		            WHERE Id = :UserInfo.getUserId()];
		        
		        String countryId = null;
		        if (runningUser.Contact != null && runningUser.Contact.Account != null) {
		        	 countryId = runningUser.Contact.Account.EP_Country__c;
		        }
	            
	            EP_FE_Terms_and_Conditions__c latestTermsAndConditions = EP_FE_Utils.getLatestTermsAndCondtions(countryId);
	            if (latestTermsAndConditions != null && latestTermsAndConditions.EP_FE_Version__c != null &&
	               	(runningUser.EP_FE_Latest_T_C_accepted_Version__c == null ||
					 runningUser.EP_FE_Latest_T_C_accepted_Version__c < latestTermsAndConditions.EP_FE_Version__c)) {
	                shouldConfirmTermsAndConditions = 'YES';
	            } else {
	            	shouldConfirmTermsAndConditions = 'NO';
    			}
    		}
    		return shouldConfirmTermsAndConditions;
    	} private set;
    }
    
    global String hasNoActiveSellToAccounts {
    	get {
    		if (hasNoActiveSellToAccounts == null){
    			hasNoActiveSellToAccounts = 'YES';
    			List<Account> activeSellTo = [SELECT Id FROM Account WHERE EP_Status__c = :EP_Common_Constant.STATUS_ACTIVE LIMIT 1];
    			if (activeSellTo.size() > 0) {
    				hasNoActiveSellToAccounts = 'NO';
    			} else {
    				List<Account> shipToWithActiveSellTo = [SELECT Id FROM Account WHERE EP_Is_SellTo_Active__c = true LIMIT 1];
    				if (shipToWithActiveSellTo.size() > 0) {
    					hasNoActiveSellToAccounts = 'NO';
    				}
    			}
    		}
    		return hasNoActiveSellToAccounts;
    	} private set;
    }

    public final String loginPage = LOGIN_PAGE;
    public final String autheticatedUserPage = AUTH_USER_PAGE;
/*********************************************************************************************
            @Author <>
            @name <TQAppController >
            @CreateDate <>
            @Description <>  
            @Version <1.0>
    *********************************************************************************************/
    global PageReference initialAction(){
        if (Apexpages.currentPage().getUrl().contains(autheticatedUserPage) && UserInfo.getUserType() == USER_TYPE){
            return new PageReference(loginPage);
        }

        if (Apexpages.currentPage().getUrl().contains(loginPage) && UserInfo.getUserType() != USER_TYPE){
            return new PageReference(autheticatedUserPage);
        }

        return null;
    }
/*********************************************************************************************
            @Author <>
            @name <TQAppController >
            @CreateDate <>
            @Description <>  
            @Version <1.0>
    *********************************************************************************************/
    global String getNetworkId(){
        return Network.getNetworkId();
    }

    @RemoteAction
/*********************************************************************************************
            @Author <>
            @name <TQAppController >
            @CreateDate <>
            @Description <>  
            @Version <1.0>
    *********************************************************************************************/
    global static Boolean forgotPassword(String credId) {
        List<User> users = [Select
                Username
             From User
             Where
                Username =: credId OR
                Email =: credId limit 10000
        ];
        if(users.isEmpty()){ 
            return false; }
            
        for(User user:users) {
            Site.forgotPassword(user.username);
        }
        return true;
    }

    @RemoteAction
/*********************************************************************************************
            @Author <>
            @name <TQAppController >
            @CreateDate <>
            @Description <>  
            @Version <1.0>
    *********************************************************************************************/    
    global static String changePassword(String password) {
        try {   
            System.setPassword(UserInfo.getUserId(), password);
            return null;
        } catch(System.InvalidParameterValueException e) {
            return e.getMessage().split(': ', 2)[1].capitalize();
        }
    }
    
    
    @RemoteAction
/*********************************************************************************************
            @Author <>
            @name < >
            @CreateDate <>
            @Description <>  
            @Version <1.0>
    *********************************************************************************************/    
    global static String changePasswordWithVerification(String newPassword, String verifyNewPassword, String oldPassword) {
        PageReference changePasswordResult = Site.changePassword( newPassword, verifyNewPassword, oldPassword);
        if (changePasswordResult != null){
            return null;
        } else {
            String errorMessage;
            if (newPassword != verifyNewPassword){
                errorMessage =  ERROR_MSG1;
            } else {
                errorMessage = ERROR_MSG2;
                            
                for (ApexPages.Message errorData : ApexPages.getMessages()){
                    errorMessage += errorData.getDetail() + ' : ' + errorData.getSummary();
                }
            }
            return errorMessage;
        }
    }

    @RemoteAction
/*********************************************************************************************
            @Author <>
            @name < >
            @CreateDate <>
            @Description <>  
            @Version <1.0>
    *********************************************************************************************/   
    global static String login(String credId, String password, String startUrl){
        PageReference pr;
        //The user may log with his username or his email so that we retrieve all users based on the provided credId
        for(User user:[
                Select
                    Username
                From User
                Where
                    Username =: credId OR
                    Email =: credId limit 10000
            ]) {
            pr = Site.login(user.username, password, startUrl);
            
            if(pr!=null){ 
                break;}
        }
        system.debug('result ' + pr);

        if (pr!=null) {
            return pr.getUrl();
        } else {
        	ApexPages.Message[] messages = ApexPages.getMessages();
        	if (messages.size() > 0){
        		String summary = messages.get(0).getSummary();
        		System.debug(summary);
        		return LOGIN_ERROR;
        	} else {
            	return LOGIN_ERROR;
        	}
        }
    }
}