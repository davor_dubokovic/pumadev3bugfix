/*
  @Author          CloudSense
  @Name            CarryOutPricingCall
  @CreateDate      28/02/2018
  @Description     
  @Version         1
 
*/

global class CarryOutPricingCall implements CSPOFA.ExecutionHandler,CSPOFA.Calloutable {
	public List<sObject> process(List<SObject> data) {
		//collect the data for all steps passed in, if needed
		List<CSPOFA__Orchestration_Step__c> stepList = (List<CSPOFA__Orchestration_Step__c>) data;

		List<sObject> result = new List<sObject>();
		
		result = processCS(stepList);
		
		return result;
	}
        List<sObject> result = new List<sObject> ();
		List<Id> orderIdList = new List<Id> ();
    	List<csord__Order__c> orderList = new List<csord__Order__c>();
    	List<csord__Order_Line_Item__c>  lineItemList = new List<csord__Order_Line_Item__c> ();
       Map<Id,Boolean> priceStatusMap = new Map<Id,Boolean>();
    	
    public Boolean performCallouts(List<SObject> data) {
        List<CSPOFA__Orchestration_Step__c> stepList = (List<CSPOFA__Orchestration_Step__c>) data;
        	

		List<CSPOFA__Orchestration_Step__c> extendedList = [SELECT Id, CSPOFA__Orchestration_Process__r.Order__c,
		                                                    CSPOFA__Orchestration_Process__r.Order__r.csord__Account__c,
		                                                    CSPOFA__Status__c, CSPOFA__Completed_Date__c,
		                                                    CSPOFA__Message__c
		                                                    FROM
		                                                    CSPOFA__Orchestration_Step__c
		                                                    WHERE
		                                                    Id IN :stepList];

		for (CSPOFA__Orchestration_Step__c step : extendedList) {
			orderIdList.add(step.CSPOFA__Orchestration_Process__r.Order__c);
		}

		orderList = [SELECT id, EP_SeqId__c, EP_Delivery_Type__c, EP_Order_Epoch__c, VMI_Indicative_PriceFlag__c, EP_Reason_for_Retrospective_Orders__c, EP_ShipTo__c,
		                                   EP_Pickup_Location_ID__c, pricingResponseJson__c,Account_Number__c, EP_Run__c, EP_Supply_Location_Name__c, EP_Payment_Method_Name__c, EP_Transporter__c, CreatedDate, Currency_Code__c,
		                                   EP_Customer_Reference_Number__c,EP_Ship_To_ID__c, EP_BOL_Number__c,EP_Transporter_Code__c,EP_Type2__c, VMI_Price_Request_XML__c, EP_Requested_Delivery_Date__c from csord__Order__c where id in :orderIdList];

		lineItemList = [SELECT id, EP_Product__c, csord__Total_Price__c,ListPrice__c,EP_Account_Id__c, Quantity__c, EP_Company_Code__c, EP_Product_Code__c,EP_WinDMS_StockHldngLocId__c, EP_WinDMS_Line_Item_Reference_Number__c from csord__Order_Line_Item__c where csord__Order__c in :orderIdList];


		for (CSPOFA__Orchestration_Step__c step : extendedList) {
			try {
				for (csord__Order__c currentOrder : orderList) {
					string xmlMessage = '';
					string responseBody = '';
					EP_OrderDataStub order = new EP_OrderDataStub();
					order.deliveryType = currentOrder.EP_Delivery_Type__c;
					order.VMIOrder = 'true';
					order.blanketOrderType = '';
					Date d = currentOrder.EP_Requested_Delivery_Date__c;
				    String months;
				    if(d.month() < 10){
				        months = '0';
				    }
			            else{
			                months = '';
			            }
					order.requestDate = d.year() + '-' +months+d.month() + '-' + d.day();
					order.orderEpoch = currentOrder.EP_Order_Epoch__c;
					order.truckAndTransport = currentOrder.EP_Transporter_Code__c;
					order.rro = currentOrder.EP_Reason_for_Retrospective_Orders__c;
					order.orderType = currentOrder.EP_Delivery_Type__c;
					order.pickupLocation = currentOrder.EP_Pickup_Location_ID__c;
					order.accountNumber = currentOrder.Account_Number__c;
					//order.quantity = '0';
					order.run = '';
					order.shipTo = currentOrder.EP_Ship_To_ID__c;
					//order.transporterId = currentOrder.EP_Transporter__c;
				
					order.paymentTerm = currentOrder.EP_Payment_Method_Name__c;
					order.customPoNumber = currentOrder.EP_Customer_Reference_Number__c;
					order.bolNumber = currentOrder.EP_BOL_Number__c;
					order.lineItems = new List<EP_OrderDataStub.LineItem> ();
					order.currencyCode = currentOrder.Currency_Code__c;

					Decimal quantityTmp = 0;

					for (csord__Order_Line_Item__c orderLineItem : lineItemList) {
						quantityTmp += orderLineItem.Quantity__c;
						order.lineItems.add(new EP_OrderDataStub.LineItem(String.valueOf(orderLineItem.EP_WinDMS_Line_Item_Reference_Number__c), String.valueOf(orderLineItem.Quantity__c), orderLineItem.EP_Company_Code__c, orderLineItem.EP_Product_Code__c));
					}

					order.quantity = String.valueOf(quantityTmp);
	                order.supplierId = lineItemList[0].EP_WinDMS_StockHldngLocId__c;
					order.supplyLocation = lineItemList[0].EP_WinDMS_StockHldngLocId__c;
					
					EP_OutboundMessageService.orderStub = order;

					String messageType = 'SEND_ORDER_PRICING_REQUEST';
					string messageId = '8010l0000008l6qAAA-21082017220223-001780';
					String companyCode = order.lineItems[0].companyCode;
					//1. Get VF page name for given outbound Message Type from custom setting
					EP_CS_OutboundMessageSetting__c msgSetting = EP_CustomSettingsUtil.getOutboundMessageSetting(messageType);
					System.debug('MessageSetting:' + msgSetting);

					System.debug('curr:' + currentOrder.id);
					System.debug('mess:' + messageId);
					System.debug('code:' + companyCode);

					//2. get XML Message
					xmlMessage = EP_OutboundMessageService.generateXMLCS(currentOrder.id, messageId, messageType, msgSetting.Payload_VF_Page_Name__c, companyCode);

					System.debug('XMLMessage:' + xmlMessage);

					//Get End Point
					string endPoint = msgSetting.End_Point__c.replace(EP_Common_Constant.COMPANY_CODE_URL_TOKAN, companyCode);

					//Defect Fix End - 57012
					//3. initate integration Services
					EP_OutboundIntegrationService intService = new EP_OutboundIntegrationService(endPoint, EP_Common_Constant.POST, xmlMessage, msgSetting.EP_Subscription_Key__c);
					//4. Do Call out
					EP_IntegrationServiceResult calloutResult = EP_OutboundMessageService.doCallOutCS(intService, msgSetting);

					//5. Process result
					responseBody = calloutResult.getResponse().getBody();

					system.debug('sendOutboundPricingMessage response body: ' + responseBody);

					if (responseBody.startsWith(EP_Common_Constant.BOM_STR)) {
						responseBody = responseBody.replaceFirst(EP_Common_Constant.BOM_STR, EP_Common_Constant.BLANK);
					}
					orderList[0].pricingResponseJson__c = responseBody;
                    orderList[0].VMI_Price_Request_XML__c = xmlMessage;

					EP_PricingResponseStub stub = (EP_PricingResponseStub) System.JSON.deserialize(responseBody, EP_PricingResponseStub.class);

					Decimal totalAmount = 0;
                    Boolean isVMIIndicativePriceFlag = false;
					for (csord__Order_Line_Item__c oli : lineItemList) {
					System.debug('LineItem:::'+oli);
						for (EP_PricingResponseStub.LineItem li : stub.Payload.any0.pricingResponse.priceDetails.priceLineItems.lineItem) {
						System.debug('pricelineitem'+li);
							if (li.lineItemInfo.itemId.equals(oli.EP_Product_Code__c) && Decimal.valueOf(li.lineItemInfo.quantity.replace(',',''))==oli.Quantity__c) {
								//oli.csord__Total_Price__c = Decimal.valueOf(li.lineItemInfo.invoiceDetails.invoiceComponent[0].amount);
								System.debug('IN:');
								oli.ListPrice__c = Decimal.valueOf(li.lineItemInfo.invoiceDetails.invoiceComponent[0].amount);								
								oli.csord__Total_Price__c = (Decimal.valueOf(li.lineItemInfo.invoiceDetails.invoiceComponent[0].amount) + 
								Decimal.valueOf(li.lineItemInfo.invoiceDetails.invoiceComponent[0].taxAmount)) * oli.Quantity__c ;									
                                totalAmount += oli.csord__Total_Price__c;
                            }
                            if(!isVMIIndicativePriceFlag && li.lineItemInfo.indicativePriceflag == 'Yes'){
                                isVMIIndicativePriceFlag = true;
                            }
                        }
                    }
                    if (!(currentOrder.EP_Type2__c == 'Transfer' || currentOrder.EP_Type2__c == 'Consignment')){
                         currentOrder.TotalAmount__c = totalAmount;
                    } else{
                         currentOrder.TotalAmount__c = 0; 
                    }
                    //if(isVMIIndicativePriceFlag){
                       currentOrder.VMI_Indicative_PriceFlag__c = isVMIIndicativePriceFlag;
                    //}
                    System.Debug('Testing11-->'+currentOrder.TotalAmount__c);
					currentOrder.EP_Price_Retrieval_Successful__c = true;
  					priceStatusMap.put(currentOrder.Id,true);
				}
			} catch(Exception e) {
				EP_loggingService.loghandledException(e, EP_Common_Constant.EPUMA, 'process', 'CarryOutPricingCall', apexPages.severity.ERROR);
			}

			step.CSPOFA__Status__c = 'Complete';
			step.CSPOFA__Completed_Date__c = Date.today();
			step.CSPOFA__Message__c = 'Custom step succeeded';
			result.add(step);
		}
        	
        	return true;
        	
    }

	public List<sObject> processCS(List<CSPOFA__Orchestration_Step__c> stepList) {
	
		for (csord__Order__c currentOrder : orderList) {
		    if(priceStatusMap == null || priceStatusMap.isEmpty()){
                     currentOrder.EP_Price_Retrieval_Successful__c = false;
                    }
            
                else{
                  currentOrder.EP_Price_Retrieval_Successful__c = true;
                 }
		}
		update orderList;
		System.debug(lineItemList);
		update lineItemList;

		return result;
	}
}