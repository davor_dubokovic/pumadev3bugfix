@isTest
private class EP_PortalCancelOrderPageController_Test {
    
    @isTest static void test_PortalCancelOrderPage() {
        Account acc = new Account();
        acc.EP_VendorType__c = '3rd Party Stock Supplier';
        acc.EP_Vendor_Type__c = '3rd Party Stock Supplier';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        acc.Name='testEPAccount';
        acc.EP_Status__c = '06-Blocked';
        insert acc;
        
        csord__Order__c ord = new csord__Order__c(Name='test order',
                                                  csord__Account__c=acc.Id,
                                                  AccountId__c = acc.id,
                                                  csord__Status__c='Order Submitted',
                                                  csord__Identification__c='testorder');
        insert ord;
        
       
        System.Assert(ord.Id != null);
        EP_PortalCancelOrderPageController.cancelOrder(ord.Id);        
        
        ord.EP_CutOff_Check_Required__c = 'Yes';
        ord.Cancellation_Check_Done__c = false;
        update ord;  
        System.AssertEquals(ord.EP_CutOff_Check_Required__c ,'Yes');  
        EP_PortalCancelOrderPageController.cancelOrder(ord.Id);
        
        ord.Cancellation_Check_Done__c = true;      
        update ord;  
        System.AssertEquals(ord.Cancellation_Check_Done__c,true);         
        EP_PortalCancelOrderPageController.cancelOrder(ord.Id);
        
        ord.EP_Delivery_Type__c = 'Delivery';
        ord.csord__Status2__c  = 'Planned';
        update ord;  
        EP_PortalCancelOrderPageController.cancelOrder(ord.Id);
        
        EP_PortalCancelOrderPageController.cancelOrder('00004503450450450');
        EP_PortalCancelOrderPageController.cancelOrder(null);
        
    }    
   
}