/* 
   @Author CloudSense
   @name <EP_RouteAcclocationTriggerHandler>
   @CreateDate <23/11/2017>
   @Description <TThis class is the handler class of EPRouteAllocationTrigger >
   @Version <1.0>
   */

public without sharing class EP_RouteAcclocationTriggerHandler { 
     Public static Boolean isExecuteAfterInsert = false;
     Public static Boolean isExecuteAfterDelete = false;
     
     public static void doAfterInsertDelete(List<EP_Route_Allocation__c>mNewRouteAllocations, boolean isInsert){
      
      EP_GeneralUtility.Log('Public','EP_RouteAcclocationTriggerHandler','doAfterInsertDelete');
      List<Id> shiptoId = new List<Id>();
      List<Account> shipToAccountList = new List<Account>();
         if(!system.isBatch()){
             
             for(EP_Route_Allocation__c RA:mNewRouteAllocations){
                 shiptoId.add(RA.EP_Ship_To__c);
             }
             
             
             system.debug('+++++++++++++++Limits.getLimitQueries() Outer'+Limits.getLimitQueries()); 
             if(shiptoId.size() > 0){
                 
                 shipToAccountList =[ SELECT 
                                            ID,RouteAllocationCount__c
                                       FROM
                                            ACCOUNT
                                        WHERE
                                            ID in: shiptoId
                                    ];
                   system.debug('+++++++++++++++Limits.getLimitQueries() Inner'+Limits.getLimitQueries()); 
                
             }
             system.debug('+++++++++++++++Limits.getLimitQueries() Final'+Limits.getLimitQueries()); 
               
             
             if(shipToAccountList.size() > 0){
                 if(isInsert){ // If Insert Action
                    //isExecuteAfterInsert= true;
                    for(ACCOUNT shipToAccount :shipToAccountList){
                         if(shipToAccount.RouteAllocationCount__c == null){
                            shipToAccount.RouteAllocationCount__c =1; 
                         }else{
                               shipToAccount.RouteAllocationCount__c = shipToAccount.RouteAllocationCount__c+1;
                         }
                       
                    }    
                     
                 }else{ // If delete Action 
                    //isExecuteAfterDelete = true;
                    for(ACCOUNT shipToAccount :shipToAccountList){
                         if(shipToAccount.RouteAllocationCount__c == null){
                            shipToAccount.RouteAllocationCount__c =0; 
                         }else{
                               shipToAccount.RouteAllocationCount__c = shipToAccount.RouteAllocationCount__c -1;
                         }
                       
                    } 
                     
                 }
                 
                 
                 update shipToAccountList;
             }
             
         }
      
    }
    
    
     public static void doAfterDelete(List<EP_Route_Allocation__c>mNewRouteAllocations){
      
      EP_GeneralUtility.Log('Public','EP_RouteAcclocationTriggerHandler','doAfterDelete');
      isExecuteAfterDelete= true;
      
     }

}