@isTest
public class EP_RunTrigger_UT{
    static testMethod void doActionBeforeInsert_UT(){
     List<EP_Run__c> runRecords = EP_TestDataUtility.getRunObjects();
     EP_RunDomainObject runDomainObject = new EP_RunDomainObject (runRecords);
      
     Test.startTest();
        runDomainObject.doActionBeforeInsert();
     Test.stopTest();
     // Assert is not required since this method delegates to another Method.
     system.assertEquals(true,true);                             
    }
    
    static testMethod void doActionBeforeDelete_UT(){
     List<EP_Run__c> runRecords = EP_TestDataUtility.getRunObjects();    
        List<EP_Run__c> oldRunRecord = runRecords.clone();     
        oldRunRecord[0].EP_Run_Start_Date__c = System.today()+2; 
        Order ord = [SELECT Id,Status,EP_Run__c FROM Order LIMIT : EP_Common_Constant.ONE];
        ord.EP_Run__c = oldRunRecord[0].id;
        update ord;
        Map<Id,EP_Run__c> oldRunMap = new Map<Id,EP_Run__c>(oldRunRecord);             
        EP_RunDomainObject runDomainObj = new EP_RunDomainObject (runRecords , oldRunMap );                 
        Test.startTest();
            runDomainObj.doActionBeforeDelete();
        Test.stopTest(); 
        //Assert is not required since this method delegates to another Method.
        system.assertEquals(true,true);                             
    }
}