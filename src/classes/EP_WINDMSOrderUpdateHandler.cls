/* 
   @Author <Accenture>
   @name <EP_WINDMSOrderUpdateHandler>
   @CreateDate <03/07/2017>
   @Description <These are the stub classes to map JSON string for orders Update Rest Service Request from WINDMS> 
   @Version <1.0> 
  */
public class EP_WINDMSOrderUpdateHandler extends EP_InboundHandler {
    private static boolean processingFailed = false;
    @testVisible private static List<EP_AcknowledgementStub.dataSet> ackResponseList = new list <EP_AcknowledgementStub.dataSet> ();
    private static EP_WINDMSOrderUpdateHelper orderUpdateHelper = new EP_WINDMSOrderUpdateHelper();
   /**
    * @Author       Accenture
    * @Name         processRequest
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
   public override string processRequest(string jsonBody) {
        EP_GeneralUtility.Log('public','EP_WINDMSOrderUpdateHandler','processRequest');
        EP_MessageHeader headerCommon = new EP_MessageHeader();
        string failureReason;
        try {
            EP_WINDMSOrderUpdateStub stub = (EP_WINDMSOrderUpdateStub )  System.JSON.deserialize(jsonBody, EP_WINDMSOrderUpdateStub.class);
			csord__Order__c order = [SELECT Id, OrderNumber__c, (select id, csord__Order__c, Planned_Quantity__c FROM csord__Order_Line_Items__r), Sync_with_WINDMS__c,EP_Order_Load_Code__c FROM csord__Order__c WHERE OrderNumber__c = :stub.MSG.Payload.Any0.OrderStatuses.orderStatus[0].Identifier.orderIdSf];
            if(stub.MSG.Payload.Any0.OrderStatuses.orderStatus[0].orderStatusWinDms == EP_Common_Constant.planning || stub.MSG.Payload.Any0.OrderStatuses.orderStatus[0].orderStatusWinDms == EP_Common_Constant.WINDMS_PLANNING){                
                order.Sync_with_WINDMS__c = true;
                if(order.csord__Order_Line_Items__r != null){
                    for(csord__Order_Line_Item__c orderItemObj: order.csord__Order_Line_Items__r) {
                        orderItemObj.Planned_Quantity__c=0;
                    }
                    
                    update order.csord__Order_Line_Items__r;
                } 
            }
			List<csord__Order__c> orderUpdates = new List<csord__Order__c>();
            System.Debug('Testing-->'+stub.MSG.Payload.Any0.OrderStatuses.orderStatus[0].orderStatusWinDms);
            if(stub.MSG.Payload.Any0.OrderStatuses.orderStatus[0].orderStatusWinDms == EP_Common_Constant.loaded  || stub.MSG.Payload.Any0.OrderStatuses.orderStatus[0].orderStatusWinDms == (EP_Common_Constant.loaded).toUpperCase()){
                if (stub.MSG.Payload.Any0.OrderStatuses.orderStatus[0].orderDlvryStartDt != null) {
                    order.EP_Expected_Loading_Date__c = convertStringToDate(stub.MSG.Payload.Any0.OrderStatuses.orderStatus[0].orderDlvryStartDt); 
                }
                orderUpdates.add(order);             
            }
            if(stub.MSG.Payload.Any0.OrderStatuses.orderStatus[0].orderStatusWinDms == EP_Common_Constant.delivered  || stub.MSG.Payload.Any0.OrderStatuses.orderStatus[0].orderStatusWinDms == (EP_Common_Constant.delivered).toUpperCase()){
                if (stub.MSG.Payload.Any0.OrderStatuses.orderStatus[0].orderDlvryStartDt != null) {
                    order.Actual_Delivery_Date__c = convertStringToDate(stub.MSG.Payload.Any0.OrderStatuses.orderStatus[0].orderDlvryStartDt); 
                }
                orderUpdates.add(order);
            }
             if(stub.MSG.Payload.Any0.OrderStatuses.orderStatus[0].orderStatusWinDms == EP_Common_Constant.planning || stub.MSG.Payload.Any0.OrderStatuses.orderStatus[0].orderStatusWinDms == (EP_Common_Constant.planning).toUpperCase()){
                order.EP_Order_Load_Code__c = null; 
                orderUpdates.add(order);             
            }
            if (orderUpdates != null && !orderUpdates.IsEmpty()) {
                update orderUpdates;
            }
            headerCommon = stub.MSG.HeaderCommon;
            string tripId = stub.MSG.Payload.Any0.OrderStatuses.tripId;
            list<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList = stub.MSG.Payload.Any0.OrderStatuses.orderStatus;

            orderUpdateHelper.setOrderAttributes(orderWrapperList, tripId);
            system.debug('MJ:::' + orderWrapperList);
            processOrderForUpdate(orderWrapperList);

        }
        catch (exception exp ) {
            system.debug('**Exception**' + exp);
             system.debug('***getStackTraceString**' + exp.getStackTraceString());
            failureReason = exp.getMessage();
            EP_LoggingService.logServiceException(exp, UserInfo.getOrganizationId(), EP_Common_constant.EPUMA, 'processRequest', 'EP_WINDMSOrderUpdateHandler',  EP_Common_constant.ERROR, UserInfo.getUserId(), EP_Common_constant.TARGET_SF, EP_Common_Constant.BLANK, EP_Common_Constant.BLANK);
            createResponse(null, exp.getTypeName(), exp.getMessage()); 
            
        }
        return EP_AcknowledgementHandler.createAcknowledgement('VMI_ORDER_CREATION', processingFailed, failureReason, headerCommon, ackResponseList);
    }
	
	public Date convertStringToDate(String myDate){     
        String stringDate = myDate.substring(6,10) + '-' + 
                    myDate.substring(3,5) + '-' +
                    myDate.substring(0,2) + ' ' +
                    myDate.substring(11,19);
        Date dt = date.valueOf(stringDate);
        return dt;
    }
    
    /**
    * @Description: This method processes the order and generates a list of orders to update
    * @param: Parsed Request Body message from WINDMS
    * @return: void
    */
    @TestVisible
    private static void processOrderForUpdate (list<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderUpdateHandler','processOrderForUpdate');
        list<csord__Order__c> ordersToUpdateList = new list<csord__Order__c>();
        for(EP_WINDMSOrderUpdateStub.OrderWrapper ordWrp : orderWrapperList){
            if(string.isNotEmpty(ordWrp.errorDescription)) {
                createResponse(ordWrp.seqId,ordWrp.errorCode, ordWrp.errorDescription);
            } else {
                 ordersToUpdateList.add(ordWrp.sfOrder); 
            }
        }
        updateOrders(ordersToUpdateList);
    }
    
    /**
    * @Description: This method performs the dml update for object in context and flags error 
                    in case of failure
    * @param: Lits of VMI Orders and List of Transformed Orders
    * @return: void
    */
    @TestVisible
    private static void updateOrders(list<csord__Order__c> ordersToUpdateList) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderUpdateHandler','updateOrders');
        Database.SaveResult[] ordersUpdateResult = Database.update(ordersToUpdateList, false);
        set<id> successOrderIdSet = new set<id>();
        system.debug('MMJJ ' + ordersUpdateResult);
        for(integer counter = 0 ; counter < ordersUpdateResult.size(); counter++) {        
            if(ordersUpdateResult[counter].isSuccess()){
                createResponse(ordersToUpdateList[counter].EP_SeqId__c, '', '' );
                successOrderIdSet.add(ordersToUpdateList[counter].id);
            }
            else {
                processUpsertErrors(ordersUpdateResult[counter].getErrors(), ordersToUpdateList[counter].EP_SeqId__c);
            }
        }
        //state engine - not needed
        //orderUpdateHelper.doPostActions(successOrderIdSet);
    }
    /**
    * @Author       Accenture
    * @Name         processUpsertErrors
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void processUpsertErrors(list<Database.Error> errorList,string seqId) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderUpdateHandler','processUpsertErrors');
        for(Database.Error err : errorList) {
            createResponse(seqId,String.valueOf(err.getStatusCode()), err.getMessage() ); 
        }
    }
    
    /* @Author       Accenture
    * @Name         createResponse
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void createResponse(string seqId, string errorCode, string errorDescription) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderUpdateHandler','createResponse');
        if(string.isNotBlank(errorDescription)) processingFailed = true;
        ackResponseList.add(EP_AcknowledgementUtil.createDataSet(EP_Common_Constant.ORDER_STRING, seqId, errorCode, errorDescription));
    }
}