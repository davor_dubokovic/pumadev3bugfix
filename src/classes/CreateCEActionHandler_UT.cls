/***
 * Author: Manish Goswami
 * Description: Test Class for CreateCEActionHandler.
 * Version History:
 * v1 - Created on 2018-08-16
 ***/
@isTest
public with sharing class CreateCEActionHandler_UT {
   @testSetup
    
    public static void createTestData() {
     Account acc = new Account();
        acc.EP_VendorType__c = '3rd Party Stock Supplier';
        acc.EP_Vendor_Type__c = '3rd Party Stock Supplier';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        acc.Name='testEPAccount';
        acc.EP_Status__c = '05-Active';
        acc.EP_Overdue_Amount__c=100;
        insert acc;
        
        csord__Order__c ord = new csord__Order__c(Name='test order',
                                                  csord__Account__c=acc.Id,
                                                  AccountId__c = acc.id,
                                                  csord__Status__c='Order Submitted',
                                                  csord__Identification__c='testorder',ep_overdue_amount__c=100);
        insert ord;
        
        CSPOFA__Orchestration_Process_Template__c oProcessTemplate = new CSPOFA__Orchestration_Process_Template__c(Name='test template');
        insert oProcessTemplate;
        
        CSPOFA__Orchestration_Process__c oProcess = new CSPOFA__Orchestration_Process__c(Name='test process', 
                                                                                         CSPOFA__Orchestration_Process_Template__c=oProcessTemplate.Id,
                                                                                         Order__c=ord.Id
                                                                                        );
        insert oProcess;
        
        CSPOFA__Orchestration_Step__c oStep = new CSPOFA__Orchestration_Step__c(Name='test step',
                                                                                CSPOFA__Orchestration_Process__c=oProcess.Id
                                                                               );
        insert oStep;
      }
        public static testmethod void testprocess() {
        list<SObject> steps = [select Id from CSPOFA__Orchestration_Step__c];
    
        test.startTest();
        CreateCEActionHandler returnaction = new CreateCEActionHandler();   
        system.assertEquals(true, steps.size() > 0);     
        returnaction.process(steps);
        test.stopTest();        
        
    }
}