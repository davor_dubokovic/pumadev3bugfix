/**
  * @Author      : Kalpesh Thakur<kalpesh.j.thakur@accenture.com>
  * @name        : EP_Delivery
  * @CreateDate  : 05/02/2017
  * @Description : This class executes logic for Delivery
  * @Version     : <1.0>
  * @reference   : N/A
  */
  public class EP_Delivery extends EP_DeliveryType{

    private static final String exRackInventoryCheck_Method = 'exRackInventoryCheck';
    private static final String deliveryInventoryCheck_Method = 'deliveryInventoryCheck';
    private static final String ClassName = 'EP_Delivery';

    /** Sends Email to Undelivered Customer
      *  @date      05/02/2017
      *  @name      sendEmailForUnDelivered
      *  @param     Order objOrder
      *  @return    NA
      *  @throws    NA
      */ 
      public void sendEmailForUnDelivered(csord__Order__c objOrder) {
        EP_GeneralUtility.Log('Public','EP_Delivery','sendEmailForUnDelivered');

        Set<String> setOrderGrpName = new Set<String>();
        if((EP_Common_Constant.TO_BE_DECIDED).equalsIgnoreCase(objOrder.EP_Action_for_Non_Delivery__c)){

          for(EP_Country_Region_Group_Mapping__mdt oMetadata: EP_PortalOrderUtil.getCountryRegionGroupMapping()) {

            if(objOrder.EP_Region__c.equalsIgnoreCase(oMetadata.EP_Region__c)){
              setOrderGrpName.add(oMetadata.EP_Group_Name__c);
            }
          }
          sendNonDeliveredEmailNotification(setOrderGrpName,objOrder.OrderNumber__c,objOrder.EP_Reason_For_Non_Delivery__c);
        }      
      }

      /** To send email to group
     *  @date      02/02/2017
     *  @name      sendMailToGroup--PrepareNonDeliveryOrderEmail
     *  @param     set<String> setGrpName,String ordNumber, string reasonForNODelivery
     *  @return    NA
     *  @throws    NA
     */
    private void sendNonDeliveredEmailNotification(set<String> setGrpName,String ordNumber, string reasonForNODelivery) {
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','sendNonDeliveredEmailNotification');
        String ordNum = '{!Order.OrderNumber}';
        String ordReasonNonDel = '{!Order.EP_Reason_For_Non_Delivery__c}';
        String strSubject = 'Non-Delivery Notification - Action Required';
        EP_EmailTemplateMapper emailTempMapper = new EP_EmailTemplateMapper();
        EmailTemplate emailTemplate = emailTempMapper.getEmailTemplateAsPerName(EP_Common_Constant.NON_DEL_NOTIFY);
        List<String> lstemailAdrreses = EP_GeneralUtility.getEmailAddresses(setGrpName);
        String plainBody = emailTemplate.Body;
        plainBody = plainBody.replace(ordNum, ordNumber);
        plainBody = plainBody.replace(ordReasonNonDel, reasonForNODelivery);
        EP_GeneralUtility.sendEmail(plainBody,lstemailAdrreses,UserInfo.getUserId(),emailTemplate.Id,strSubject);
    }

    /** This method updates Transport Services fields for delivery
      *  @date      05/02/2017
      *  @name      updateTransportService
      *  @param     Order objOrder
      *  @return    NA
      *  @throws    NA
      */  
      public override void updateTransportService(csord__Order__c objOrder) {
        EP_GeneralUtility.Log('Public','EP_Delivery','updateTransportService');

        super.updateTransportService(objOrder);
      }

     /** This method returns getShiptos for the AccountId
      *  @date      05/02/2017
      *  @name      getShipTos
      *  @param     Id accountId
      *  @return    List<> 
      *  @throws    NA
      */
      public override List<Account> getShipTos(Id accountId) {
        EP_GeneralUtility.Log('Public','EP_Delivery','getShipTos');

        List<Account> lstShipTos = EP_AccountMapper.getAllShipTos(accountId);

        return lstShipTos;
      }


    /** This method is used to get price book entries
      *  @date      05/02/2017
      *  @name      findPriceBookEntries
      *  @param     Id pricebookId,Id storageLocId,Order objOrder
      *  @return    List<PriceBookEntry>
      *  @throws    NA
      */ 
      public override List<PriceBookEntry> findPriceBookEntries(Id pricebookId,csord__Order__c objOrder) {
        EP_GeneralUtility.Log('Public','EP_Delivery','findPriceBookEntries');
        System.debug('***pricebookId******'+pricebookId+'*****ISO*****'+objOrder.CurrencyIsoCode+'********Category***'+objOrder.EP_Order_Product_Category__c);
        return new EP_PriceBookEntryMapper().getRecordsByFilterSet_1(new Set<Id>{pricebookId},new Set<String>{objOrder.CurrencyIsoCode}, new Set<String>{objOrder.EP_Order_Product_Category__c},true);
      }


    /** This method is used to get operational tanks on the selected ship to 
      *  @date      05/02/2017
      *  @name      getOperationalTanks
      *  @param     String strSelectedShipToId
      *  @return    Map<ID, EP_Tank__c> 
      *  @throws    NA
      */ 
      public override Map<ID, EP_Tank__c> getOperationalTanks(String strSelectedShipToId){         
        EP_GeneralUtility.Log('Public','EP_Delivery','getOperationalTanks');
       Map<ID, EP_Tank__c> mapShipToTanks = EP_TankMapper.getOperationalRecordsByShipTo( strSelectedShipToId );
       return mapShipToTanks;
     }  

     /** This method is used to get tanks on the selected ship to
      *  @date      05/02/2017
      *  @name      getTanks
      *  @param     String strSelectedShipToId
      *  @return    Map<ID, EP_Tank__c> 
      *  @throws    NA
      */ 
      public override Map<ID, EP_Tank__c> getTanks(String strSelectedShipToId){  
        EP_GeneralUtility.Log('Public','EP_Delivery','getTanks');      
       Map<ID, EP_Tank__c> mapShipToTanks = EP_TankMapper.getRecordsByShipTo( strSelectedShipToId );
       return mapShipToTanks;
     }  


    /** This method is used to get the Routes based on Ship to and Storage Location
      *  @date      05/02/2017
      *  @name      getAllRoutesOfShipToAndLocation
      *  @param     String shipId, String shlId
      *  @return    List<EP_Route__c>
      *  @throws    NA
      */
      public override List<EP_Route__c> getAllRoutesOfShipToAndLocation( String shipId, String shlId ){
        EP_GeneralUtility.Log('Public','EP_Delivery','getAllRoutesOfShipToAndLocation');
        EP_RouteAllocationMapper routeAllocationMapperObj = new EP_RouteAllocationMapper();
        EP_RouteMapper routeMapperObj = new EP_RouteMapper();
        Set<Id> routesFromRA = new Set<Id>();
        List<EP_Route__c> routeList = new List<EP_Route__c>();

        for(EP_Route_Allocation__c rAlloc : routeAllocationMapperObj.getNotNullRouteAllocShipToRecs(new Set<Id>{shipId})){
          routesFromRA.add(rAlloc.EP_Route__c);
        }
        
        for(EP_Route__c route : routeMapperObj.getRecordsByStorageLocation(new Set<Id>{shlId})){
          if(routesFromRA.contains(route.id)){
            routeList.add(route);
          }
        }
        return routeList;
      }


    /** This method is used set the visbility of Customer PO field on portal order page
      *  @date      05/02/2017
      *  @name      showCustomerPO
      *  @param     Order orderObject
      *  @return    Boolean
      *  @throws    NA
      */ 
      public override Boolean showCustomerPO(csord__Order__c orderObject){
        EP_GeneralUtility.Log('Public','EP_Delivery','showCustomerPO');
        return orderObject.EP_ShipTo__r.EP_Is_Customer_Reference_Visible__c;
      }

    /** This method find if Order Entry is Valid
     *  @date      07/03/2017
     *  @name      isOrderEntryValid
     *  @param     NA
     *  @return    NA
     *  @throws    NA
     */
     public override Boolean isOrderEntryValid(integer numOfTransportAccount){
      EP_GeneralUtility.Log('Public','EP_Delivery','isOrderEntryValid');
      System.debug('numOfTransportAccount:---'+numOfTransportAccount);
      if (numOfTransportAccount < 1) { 
        return false;   
      }
      return true;
    }
  }