/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageLocationActiveToBlocked>
*  @CreateDate <15/3/2017>
*  @Description <Handles Storage Ship To Account status change from 05-Active to 06-Blocked>
*  @Version <1.0>
*/
public class EP_ASTStorageLocationActiveToBlocked extends EP_AccountStateTransition {

    public EP_ASTStorageLocationActiveToBlocked () {
        finalState = EP_AccountConstant.BLOCKED;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationActiveToBlocked','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationActiveToBlocked', 'isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationActiveToBlocked',' isGuardCondition');        
        return true;
    }
}