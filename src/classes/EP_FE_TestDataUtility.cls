/* 
  @Author <Nic Tassini>
  @name <EP_FE_TestDataUtility>
  @CreateDate <21/04/2016>
  @Description <Test data utils will provide all the utilities required to create test records >
  @Version <1.0>
*/
global with sharing class EP_FE_TestDataUtility {  

    public static final String CURRENCY_GBP = 'GBP';
    public static final String PRICEBOOK_NAME = 'Custom Pricebook Test';
    public static final String PRODUCT_DIESEL = 'Diesel';
    public static final String PRODUCT_FAMILY = 'Hydrocarbon Liquid';
    public static final String PUMA_OWNED = 'Puma Owned';
    public static final String PAYMENT_TERM_NAME = 'prepayment';
    public static final String PAYMENT_TERM_CODE = 'PP' ;
    public static final String CUST_NAME = 'Test';
    public static final String BUSINESS_HOUR_NAME = 'Puma Business hours';
    public static final String TIMEZONE_SIDKEY = 'Asia/Singapore';
    public static final String OBJECT_TYPE = 'Account';
    public static final String OPERATION_TYPE = 'UPDATE';
    public static final String LOCATION_TYPE = 'Primary';
    public static final String REQUEST_STATUS = 'Open';
    public static final String RECORD_TYPE = 'Sell To';
    public static final String NEW_VALUE = 'Standard';
    public static final String SOURCE_FIELD_LABEL = 'Billing Basis';
    public static final String ORIGINAL_VALUE = 'Ambient';
    public static final String SOURCE_FIELD_APINAME = 'EP_Billing_Basis__c';
    public static final String TESTNAV = 'testNAV';
    public static final String TRANSPORTER1 = 'Transporter';
    public static final String SKU_UNIQUE_KEY = '12345_';
    public static final String CUSTOMER_INVOICE = 'Customer Invoice';
    public static final String NAV001 = 'NAV001';


/* 
  @Author <>
  @name <createPricebook>
  @CreateDate <>
  @Description <>
  @Version <1.0>
*/
    public static Pricebook2 createPricebook(){
      
        Pricebook2 pricebook = new Pricebook2();
        pricebook.Name = PRICEBOOK_NAME +system.now().millisecond();
        pricebook.IsActive = TRUE;
        pricebook.CurrencyIsoCode = CURRENCY_GBP;
        try{    
            Database.insert (pricebook);
        }catch (Exception ex){
          System.debug('The following exception has occurred: ' + ex.getMessage());
          throw ex;
        }
        return pricebook;
    }
/* 
  @Author <>
  @name <createProduct>
  @CreateDate <>
  @Description <>
  @Version <1.0>
*/    
    public static Product2 createProduct(){

        Product2  productObj = new product2();
        productObj.Name = PRODUCT_DIESEL +system.now().millisecond();
        productObj.CurrencyIsoCode = CURRENCY_GBP;
        productObj.IsActive = TRUE;
        productObj.Family= PRODUCT_FAMILY;
        productObj.Eligible_for_Rebate__c = TRUE;
        try{
            Database.insert (productObj);
        }catch(exception ex){
          System.debug('The following exception has occurred: ' + ex.getMessage());
          throw ex;
        }
        return productObj;

    }
/* 
  @Author <>
  @name <createPricebookEntry>
  @CreateDate <>
  @Description <>
  @Version <1.0>
*/ 
    public static PricebookEntry createPricebookEntry(Id pricebookId){

        Id standardPriceBook = Test.getStandardPricebookId();

        Product2  productObj = new product2();
        productObj.Name = PRODUCT_DIESEL +system.now().millisecond();
        productObj.CurrencyIsoCode = CURRENCY_GBP;
        productObj.IsActive = TRUE;
        productObj.Family= PRODUCT_FAMILY;
        productObj.Eligible_for_Rebate__c = TRUE;
        try{
            Database.insert (productObj);
        }catch(Exception ex){
          System.debug('The following exception has occurred: ' + ex.getMessage());
          throw ex;
        }
        /*PricebookEntry standardPrice = new PricebookEntry();
        standardPrice.Pricebook2Id = standardPriceBook;
        standardPrice.Product2Id = productObj.Id;
        standardPrice.UnitPrice = 10000;
        standardPrice.IsActive = true;
        standardPrice.CurrencyIsoCode = CURRENCY_GBP;
        Database.insert (standardPrice); */

        PricebookEntry priceBookEntry = new PricebookEntry();
        priceBookEntry.Pricebook2Id = pricebookId;
        priceBookEntry.Product2Id = productObj.Id;
        priceBookEntry.UnitPrice = 10000;
        priceBookEntry.IsActive = false;
        priceBookEntry.CurrencyIsoCode = CURRENCY_GBP;
        try{
            Database.insert (priceBookEntry);
        }catch(Exception ex){
          System.debug('The following exception has occurred: ' + ex.getMessage());
          throw ex;
        }
        return priceBookEntry;
    }
/* 
  @Author <>
  @name <getRunAsUser>
  @CreateDate <>
  @Description <>
  @Version <1.0>
*/ 
     public static User getRunAsUser(){
        User sysAdmUser = EP_TestDataUtility.createRunAsUser();
        sysAdmUser.UserName = sysAdmUser.UserName + system.now().millisecond();
        try{
            Database.insert (sysAdmUser);
        }catch(Exception ex){
          System.debug('The following exception has occurred: ' + ex.getMessage());
          throw ex;
        }
        return sysAdmUser;
    }
/* 
  @Author <>
  @name <createDictionaryItemWithLabelType>
  @CreateDate <>
  @Description <>
  @Version <1.0>
*/     
    public static EP_FE_Dictionary_Configurations__mdt createDictionaryItemWithLabelType(){
          EP_FE_Dictionary_Configurations__mdt  customeSettingLabel;
          try{  
            customeSettingLabel= new EP_FE_Dictionary_Configurations__mdt ();
          //string labelkey = customeSettingLabel.EP_FE_Label_Key__c;
          //insert customeSettingLabel;
           }catch(Exception ex){
              System.debug('The following exception has occurred: ' + ex.getMessage());
              throw ex;
           } 
           return customeSettingLabel;
        // }   

        //public static EP_FE_Dictionary_Configurations__mdt createDictionaryItemWithFieldType(){
           
        //     EP_FE_Dictionary_Configurations__mdt  customeSettingField= new EP_FE_Dictionary_Configurations__mdt 
        //     (Name ='Test Custom Object/Field',EP_FE_Type__c='field',EP_FE_FieldAPIName__c  ='EP_Freight_Matrix__c',EP_FE_ObjectAPIName__c='EP_Freight_Price__c');
        //     insert customeSettingField;
            
          //return customeSettingField;
           
        }  
/* 
  @Author <>
  @name <createBusinessHours>
  @CreateDate <>
  @Description <>
  @Version <1.0>
*/ 
    public static BusinessHours createBusinessHours(){

        BusinessHours busHours = new BusinessHours(Name = BUSINESS_HOUR_NAME, IsActive = TRUE, TimeZoneSidKey = TIMEZONE_SIDKEY);
        return busHours;
    }

/* 
  @Author <>
  @name <createSellToDelivery>
  @CreateDate <>
  @Description <>
  @Version <1.0>
*/
    public static Account createTransporter(){

        Id recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(EP_FE_Constants.VENDOR_DEV_NAME).getRecordTypeId();
    /* TFS fix 45559,45560,45567,45568 start EP_NAV_ID__c deprecated, EP_NAV_Vendor_Id__c is replaced by EP_Source_Entity_ID__c and EP_Vendor_Type__c is replaced by EP_VendorType__c*/
        Account transport = new Account(Recordtypeid = recTypeId, Name = TRANSPORTER1+system.now().millisecond(), EP_Source_Entity_ID__c = NAV001+system.now().millisecond(),
                                        EP_VendorType__c = EP_FE_Constants.VENDOR_TYPE_TRANSPORTER, BillingCity = 'Brisbane',BillingCountry ='Australia',EP_Status__c = EP_Common_Constant.STATUS_ACTIVE);
       /* TFS fix 45559,45560,45567,45568 end*/
        try{
            Database.insert (transport);
        }catch(Exception ex){
          System.debug('The following exception has occurred: ' + ex.getMessage());
          throw ex;
        }
        return transport;
    }

/* 
  @Author <>
  @name <createStockLocation>
  @CreateDate <>
  @Description <>
  @Version <1.0>
*/ 
    public static EP_Stock_Holding_Location__c createStockLocation(Id selltoId, Id terminalId){
        
        //Id recTypeId = Schema.SObjectType.EP_Stock_Holding_Location__c.getRecordTypeInfosByName().get(EP_FE_Constants.EXRACK_SUPPLY_LOCATION).getRecordTypeId();
        //EP_Stock_Holding_Location__c  supplyLocation = [Select RecordTypeId from EP_Stock_Holding_Location__c  where RecordType.DeveloperName = :EP_FE_Constants.EXRACK_SUPPLY_LOCATION];

        Account transporter = createTransporter();
        EP_Stock_Holding_Location__c stockList = new EP_Stock_Holding_Location__c(EP_Sell_To__c = selltoId, Stock_Holding_Location__c = terminalId,
                                                     EP_Location_Type__c = LOCATION_TYPE, CurrencyIsoCode = CURRENCY_GBP, EP_Trip_Duration__c=100,
                                                     EP_Transporter__c = transporter.Id);
        try{
            Database.insert (stockList);
        }catch(Exception ex){
            System.debug('The following exception has occurred: ' + ex.getMessage());
            throw ex;
        }
        return stockList;
    }
/* 
  @Author <>
  @name <createChangerequest>
  @CreateDate <>
  @Description <>
  @Version <1.0>
*/
    public static EP_ChangeRequest__c createChangerequest(Id selltoId){
        
        EP_ChangeRequest__c changeRequest = new EP_ChangeRequest__c(EP_Object_ID__c = String.valueOf(selltoId), EP_Object_Type__c = OBJECT_TYPE, EP_Account__c = selltoId,
                                                EP_Request_Status__c = REQUEST_STATUS, EP_Requestor__c = userInfo.getUserId(), EP_Operation_Type__c = OPERATION_TYPE,
                                                EP_Record_Type__c = RECORD_TYPE, EP_Request_Date__c = system.now() );
        try{
            Database.insert (changeRequest);
        }catch(Exception ex){
            System.debug('The following exception has occurred: ' + ex.getMessage());
            throw ex;
        }
        EP_ChangeRequestLine__c chanereqline = new EP_ChangeRequestLine__c(EP_Change_Request__c = changeRequest.id, EP_New_Value__c = NEW_VALUE, EP_Source_Field_Label__c = SOURCE_FIELD_LABEL,
                                          EP_Original_Value__c = ORIGINAL_VALUE, EP_Source_Field_API_Name__c = SOURCE_FIELD_APINAME, EP_Request_Status__c = REQUEST_STATUS);
        try{
            Database.insert (chanereqline);
        }catch(Exception ex){
            System.debug('The following exception has occurred: ' + ex.getMessage());
            throw ex;
        }
        return changeRequest;
        
    }
/* 
  @Author <>
  @name <createInventory>
  @CreateDate <>
  @Description <>
  @Version <1.0>
*/
    public static EP_Inventory__c createInventory(){

        Product2  productObj = new Product2(Name = PRODUCT_DIESEL,CurrencyIsoCode = CURRENCY_GBP,IsActive = TRUE,
                                    Family= PRODUCT_FAMILY,Eligible_for_Rebate__c =TRUE);
        Database.insert (productObj);

        EP_Inventory__c  inventoryObj = new EP_Inventory__c(EP_Product__c = productObj.id,
                                    EP_Stock_Label__c=PUMA_OWNED, EP_SKU_Unique_Key__c= SKU_UNIQUE_KEY +  System.Now().millisecond());
                                                                   
        try{                                                        
            Database.insert (inventoryObj);
        }catch(Exception ex){
            System.debug('The following exception has occurred: ' + ex.getMessage());
            throw ex;
        }
        return inventoryObj;
    } 
/* 
  @Author <>
  @name <createPaymentTerm>
  @CreateDate <>
  @Description <>
  @Version <1.0>
*/
    public static EP_Payment_Term__c createPaymentTerm(){

        EP_Payment_Term__c paymentTerm = new EP_Payment_Term__c(Name = PAYMENT_TERM_NAME, EP_Payment_Term_Code__c = PAYMENT_TERM_CODE);
        try{
            Database.insert (paymentTerm);
        }catch(Exception ex){
          System.debug('The following exception has occurred: ' + ex.getMessage());
          throw ex;
        }
        return paymentTerm;
    }
/* 
  @Author <>
  @name <createSellToDelivery>
  @CreateDate <>
  @Description <>
  @Version <1.0>
*/
    public static Account createSellToDelivery(){

      Account billTo =  EP_TestDataUtility.createBillToAccount(); // returns bill to account
      EP_Freight_Matrix__c freightMatrix =EP_TestDataUtility.createFreightMatrix();// returns freight matrix   

        Pricebook2 pricebook = EP_FE_TestDataUtility.createPricebook();
        //PricebookEntry priceBookEntries = EP_FE_TestDataUtility.createPricebookEntry(pricebook.Id);

        Account sellTo = EP_TestDataUtility.createSellToAccount(billTo.Id,freightMatrix.id);
        Database.insert (sellTo);
        sellTo.EP_Delivery_Type__c=EP_Common_Constant.DELIVERY;
        sellTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        sellTo.CurrencyIsoCode = CURRENCY_GBP;
        sellTo.EP_PriceBook__c = pricebook.Id;
        try{
            Database.update (sellTo);
        }catch(Exception ex){
            System.debug('The following exception has occurred: ' + ex.getMessage());
            throw ex;
        }

        sellTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        try{
            Database.update (sellTo);
        }catch(Exception ex){
          System.debug('The following exception has occurred: ' + ex.getMessage());
          throw ex;
        }
        return sellTo;
    }
/* 
  @Author <>
  @name <createSellToExRack>
  @CreateDate <>
  @Description <>
  @Version <1.0>
*/
    public static Account createSellToExRack(){

      Account billTo1 =  EP_TestDataUtility.createBillToAccount(); // returns bill to account
      EP_Freight_Matrix__c freightMatrix =EP_TestDataUtility.createFreightMatrix();// returns freight matrix 

        Pricebook2 pricebook = EP_FE_TestDataUtility.createPricebook();
        //PricebookEntry priceBookEntries = EP_FE_TestDataUtility.createPricebookEntry(pricebook.Id);

        Account sellTo = EP_TestDataUtility.createSellToAccount(billTo1.Id,freightMatrix.id);
        Database.insert (sellTo);
        sellTo.EP_Delivery_Type__c=EP_Common_Constant.EX_RACK;
        sellTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        sellTo.CurrencyIsoCode = CURRENCY_GBP;
        sellTo.EP_PriceBook__c = pricebook.Id;
        try{
            Database.update (sellTo);
        }catch(Exception ex){
            System.debug('The following exception has occurred: ' + ex.getMessage());
            throw ex;
        }

        sellTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        try{
            Database.update (sellTo);
        }catch(Exception ex){
          System.debug('The following exception has occurred: ' + ex.getMessage());
          throw ex;
        }
        return sellTo;
    }
 /* 
  @Author <>
  @name <createShipTo>
  @CreateDate <>
  @Description <>
  @Version <1.0>
*/
    public static Account createShipTo(){

      Id recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(EP_FE_Constants.SHIPTO_ACCOUNT_RECORD_TYPE_NAME).getRecordTypeId();

        Account sellTo = EP_FE_TestDataUtility.createSellToDelivery();
        
        Pricebook2 pricebook = EP_FE_TestDataUtility.createPricebook();
        //PricebookEntry priceBookEntries = EP_FE_TestDataUtility.createPricebookEntry(pricebook.Id);
        
        Account shipTo = EP_TestDataUtility.createShipToAccount(sellTo.Id, recTypeId);
        try{
            Database.insert (shipTo);
        }catch(Exception e){
            System.debug('The following exception has occurred: ' + e.getMessage());
            throw e;
        }
        shipTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update (shipTo);
        shipTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        shipTo.EP_PriceBook__c = pricebook.Id;
        try{
            Database.update (shipTo);
        }catch(Exception ex){
            System.debug('The following exception has occurred: ' + ex.getMessage());
            throw ex;
        }

        return shipTo;
    }
 /* 
  @Author <>
  @name <createterminal>
  @CreateDate <>
  @Description <>
  @Version <1.0>
*/
    public static Account createterminal(){
        Id recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(EP_FE_Constants.SHIPTO_ACCOUNT_RECORD_TYPE_NAME).getRecordTypeId();

        BusinessHours businessHoursObj = [SELECT Id from BusinessHours WHERE Name = : EP_FE_Constants.DEFAULT_BUSINESS_HOURS_NAME limit 10000];
        
        Account sellTo = EP_FE_TestDataUtility.createSellToExRack();
        Account shipTo = EP_TestDataUtility.createShipToAccount(sellTo.Id, recTypeId);

        EP_Stock_Holding_Location__c stockList = EP_TestDataUtility.createStockLocation(shipTo.id,true);
        stockList.EP_Sell_To__c = sellTo.id;
        Database.insert (stockList);
        Account terminal = EP_TestDataUtility.createStockHoldingLocation();
        try{
            Database.insert (terminal);
        }catch(Exception ex){
            System.debug('The following exception has occurred: ' + ex.getMessage());
            throw ex;
        }
        terminal.EP_Business_Hours__c = businessHoursObj.Id;
        try{
            Database.update (terminal);
        }catch(Exception ex){
            System.debug('The following exception has occurred: ' + ex.getMessage());
            throw ex;
        }
        stockList.Stock_Holding_Location__c = terminal.id;
        Database.update (stockList);

        return terminal;
    }
 /* 
  @Author <>
  @name <createNonVMIOrder>
  @CreateDate <>
  @Description <>
  @Version <1.0>
*/
    public static csord__Order__c createNonVMIOrder()
    {
        Pricebook2 pricebook = EP_FE_TestDataUtility.createPricebook();
        PricebookEntry priceBookEntries = EP_FE_TestDataUtility.createPricebookEntry(pricebook.Id);

        Account sellTo =  EP_TestDataUtility.createSellToAccount(NULL, NULL);
        sellTo.EP_Delivery_Type__c = EP_Common_Constant.DELIVERY;
        sellTo.EP_PriceBook__c  = pricebook.Id;
        
        sellTo.EP_AvailableFunds__c=10000;
        
        Database.insert (sellTo);
        sellTo.EP_Is_Valid_Address__c = true;
        Database.update (sellTo);
        sellTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update (sellTo);
        sellTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;    
        Database.update (sellTo);      
        
        String nonVMIOrderRT =  EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.CSORDER, EP_Common_Constant.NONVMI_ORDER_RECORD_TYPE_NAME);
       
       /* EP_Freight_Matrix__c matrix = EP_TestDataUtility.createFreightMatrix();
        insert matrix;
        
        EP_Freight_Price__c freightPrice = EP_TestDataUtility.createFreight(matrix.id, productPetrolObj.id);*/

        csord__Order__c ord = new csord__Order__c();
        ord = EP_TestDataUtility.createCSOrder(sellTo.Id, nonVMIOrderRT, pricebook.Id);
        try{
            Database.insert (ord);
        }catch(Exception ex){
            System.debug('The following exception has occurred: ' + ex.getMessage());
            throw ex;
        }
        ord.CurrencyIsoCode = CURRENCY_GBP;
        Database.update (ord);
      
        csord__Order_Line_Item__c ot = new csord__Order_Line_Item__c(PricebookEntryId__c = priceBookEntries.Id, UnitPrice__c = 1, Quantity__c = 1, csord__Order__c = ord.id,csord__Identification__c = 'CSIdentification');
        try{
            Database.insert (ot);
        }catch(Exception ex){
            System.debug('The following exception has occurred: ' + ex.getMessage());
            throw ex;
        }
        return ord;
    }
/* 
  @Author <>
  @name <CreateCustomer>
  @CreateDate <>
  @Description <>
  @Version <1.0>
*/    
        public static  EP_Customer_Payment__c  CreateCustomer(){
        //Account bilTo =  EP_TestDataUtility.createBillToAccount(); // returns bill to account
        //insert bilTo ; 
        EP_Customer_Payment__c cust = new EP_Customer_Payment__c();
        cust.Name = CUST_NAME ;
        //cust.EP_Bill_To__c = bilTo.Id;
        cust.EP_Payment_Date__c = system.today();
        cust.EP_Amount_Paid__c = 10;
        cust.CurrencyIsoCode = EP_Common_Constant.GBP;
       // insert cust ;
        return cust ;
       
    
    }
    
 /* 
  @Author <>
  @name <CreateCustomerAccountStatement>
  @CreateDate <>
  @Description <>
  @Version <1.0>
*/ 
    public static void CreateCustomerAccountStatement(Account bilTo){  
    EP_Customer_Account_Statement__c testCustomerAccountStatement = new EP_Customer_Account_Statement__c();
    testCustomerAccountStatement.EP_Bill_To__c = bilTo.id ;
    testCustomerAccountStatement.EP_Statement_Issue_Date__c = system.today(); 
    testCustomerAccountStatement.EP_Statement_NAV_ID__c = TESTNAV;
    testCustomerAccountStatement.CurrencyIsoCode = EP_Common_Constant.GBP;
    //testCustomerAccountStatement.recordtypeid = EP_FE_Constants.RT_CUSTOMER_INVOICE;
    try{
        Database.insert (testCustomerAccountStatement) ;
    }catch(Exception ex){
            System.debug('The following exception has occurred: ' + ex.getMessage());
            throw ex;
        }
    }
/* 
  @Author <>
  @name <CreateCustomerAccountStatementItem>
  @CreateDate <>
  @Description <>
  @Version <1.0>
*/    
    public static void CreateCustomerAccountStatementItem(Account bilTo ){ 
    Id CustomerAccountStatementItemID= Schema.SObjectType.EP_Customer_Account_Statement_Item__c.getRecordTypeInfosByName().get(CUSTOMER_INVOICE).getRecordTypeId(); 
    /*Account bilTo =  EP_TestDataUtility.createBillToAccount(); // returns bill to account
    insert bilTo ; 
    bilTo .EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP ;
    update bilTo ;
    bilTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE ;
    update bilTo ; */
    EP_Invoice__c invoice = new EP_Invoice__c();
    invoice = EP_TestDataUtility.createInvoice(bilTo);
    invoice.EP_Invoice_Due_Date__c =system.today() - 5;
    invoice.EP_Invoice_Issue_Date__c = system.today();
    invoice.EP_Remaining_Balance__c = 100;
    try{
        Database.insert (invoice) ;
    }catch(Exception ex){
            System.debug('The following exception has occurred: ' + ex.getMessage());
            throw ex;
        }
    system.debug('testubgf' +[select EP_Overdue__c from EP_Invoice__c where id=:invoice.id]);
    system.debug('invoice.EP_Overdue__c@@@'+invoice.EP_Overdue__c);
    system.debug('invoice.EP_Invoice_Due_Date__c@@@'+invoice.EP_Invoice_Due_Date__c);
    system.debug('invoice.EP_Remaining_Balance__c@@@'+invoice.EP_Remaining_Balance__c);

    
    EP_Customer_Account_Statement_Item__c testCustomerAccountStatementItem = new EP_Customer_Account_Statement_Item__c ();
    testCustomerAccountStatementItem.recordtypeid = CustomerAccountStatementItemID;
    testCustomerAccountStatementItem.EP_NAV_Statement_Item_ID__c = TESTNAV;
    testCustomerAccountStatementItem.EP_Bill_To__c = bilTo.id ;
    testCustomerAccountStatementItem.EP_Statement_Item_Issue_Date__c = system.today();
    testCustomerAccountStatementItem.EP_Customer_Invoice__c = invoice.id;
    Database.insert (testCustomerAccountStatementItem) ;
    system.debug('###'+testCustomerAccountStatementItem.recordtypeid);
    }
    
    
 }