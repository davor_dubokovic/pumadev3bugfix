@isTest
 public class EP_OrderDomainObject_UT
 {
    Static Final String EMAILRECIPIENT = 'abc@test.com';
    Static Final String TEMPSTR = 'Test String';
    Static Final String GROUPNAME = 'Test Group';
    
    static testMethod void setOrderItems_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        EP_OrderItemMapper ordItemMapObj = new EP_OrderItemMapper();
        LIST<csord__Order_Line_Item__c> listOfOrderItems = ordItemMapObj.getCSOrderLineItemsforOrder(ord.Id) ;
        Test.startTest();
        localObj.setOrderItems(listOfOrderItems);
        Test.stopTest();
        System.AssertEquals(true,!localObj.getOrderItems().isEmpty());
        System.AssertEquals(true,localObj.getOrderItems().size() == 1);
    }
    
   
Public static testMethod void getOrderItems_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        List<csord__Order_Line_Item__c> insertOrderItems = new List<csord__Order_Line_Item__c>();
        cscfga__Product_Basket__c basketInsert = new cscfga__Product_Basket__c(
            Name = 'New Basket',
            OwnerId = UserInfo.getUserId()
        );
        insert basketInsert;
        csord__Order_Line_Item__c  ordLine = new csord__Order_Line_Item__c();
       
        ordLine.EP_SeqId__c = '0480000091';
        ordLine.EP_WinDMS_Line_ItemId__c = '2001002';
        ordLine.EP_Tank_Number__c = 'Diesel0Tnk';
        ordLine.EP_Quantity_UOM__c = 'LT';
        ordLine.EP_WinDMS_StockHldngLocId__c = '43001';
        ordline.Quantity__c = 100;
        ordLine.EP_Prior_Quantity__c = 200;
        ordline.csord__Identification__c = basketInsert.id;
        ordline.csord__Order__c = ord.id;
        insertOrderItems.add(ordLine);
        insert insertOrderItems;
        Test.startTest();
        LIST<csord__Order_Line_Item__c> result = insertOrderItems;
        Test.stopTest();
        System.AssertEquals(true,!result.isEmpty());
    }
    
    
    static testMethod void setWinDMSStatus_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        String value = EP_Common_Constant.planning;
        Test.startTest();
        localObj.setWinDMSStatus(value);
        Test.stopTest();
        System.AssertEquals(true,localObj.getOrder().EP_WinDMS_Status__c.contains(EP_Common_Constant.planning));
        
    }
    static testMethod void setStatus_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        String statusvalue = EP_Common_Constant.planning;
        Test.startTest();
        localObj.setStatus(statusvalue);
        Test.stopTest();
        System.AssertEquals(true,localObj.getStatus().contains(EP_Common_Constant.planning));
    }
    static testMethod void getStatus_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        String result = localObj.getStatus();
        Test.stopTest();
        System.AssertEquals(true,localObj.getStatus().contains(EP_Common_Constant.ORDER_DRAFT_STATUS));
    }
    static testMethod void resetStatus_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        localObj.resetStatus();
        Test.stopTest();
        System.AssertEquals(true,localObj.getStatus().contains(EP_Common_Constant.ORDER_DRAFT_STATUS));
    }
    static testMethod void getOrderTypeClassification_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        String result = localObj.getOrderTypeClassification();
        Test.stopTest();
        System.AssertEquals(true,result != EP_Common_Constant.BLANK);
        EP_AccountDomainObject accountDomainObj = new EP_AccountDomainObject(ord.EP_ShipTo__c);
        String shipToType = accountDomainObj.localAccount.EP_ShipTo_Type__c+'Consignment';
        System.AssertEquals(true,result == shipToType);
    }
    
    static testMethod void getOrderTypeClassificationConsumption_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        String result = localObj.getOrderTypeClassification();
        Test.stopTest();
        System.AssertEquals(true,result.equalsIgnoreCase(EP_Common_Constant.CONSUMPTION));
    }
    
    static testMethod void getType_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        String result = localObj.getType();
        Test.stopTest();
        System.AssertEquals(true,result != EP_Common_Constant.BLANK);
        System.AssertEquals(true,result.equalsIgnoreCase(EP_Common_Constant.SALES));
    }
    static testMethod void getConsumtionType_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        String result = localObj.getType();
        Test.stopTest();
        System.AssertEquals(true,result != EP_Common_Constant.BLANK);
        System.AssertEquals(true,result.equalsIgnoreCase(EP_Common_Constant.CONSUMPTION));
    }
    
   
  Public static testMethod void getTransferType_test() {
        csord__Order__c ord = EP_TestDataUtility.getTransferOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        String result = localObj.getType();
        Test.stopTest();
        System.AssertEquals(true,result != EP_Common_Constant.BLANK);
        //System.AssertEquals(true,result.equalsIgnoreCase(EP_Common_Constant.TRANSFER_ORDER));
   
   }  
    
    static testMethod void getEpochType_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        String result = localObj.getEpochType();
        Test.stopTest();
        System.AssertEquals(true,result != EP_Common_Constant.BLANK);
        System.AssertEquals(true,result.equalsIgnoreCase(EP_Common_Constant.EPOC_CURRENT));
    }
    static testMethod void isRetrospective_PositiveScenariotest() {
        csord__Order__c ord = EP_TestDataUtility.getRetrospectivePositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        Boolean result = localObj.isRetrospective();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isRetrospective_NegativeScenariotest() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderNegativeScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        Boolean result = localObj.isRetrospective();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isPackaged_PositiveScenariotest() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentPackagedOrder();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        Boolean result = localObj.isPackaged();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isPackaged_NegativeScenariotest() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderNegativeScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        Boolean result = localObj.isPackaged();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isBulk_PositiveScenariotest() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        Boolean result = localObj.isBulk();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isBulk_NegativeScenariotest() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentPackagedOrder();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        Boolean result = localObj.isBulk();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void getVMIType_If_test() {
        csord__Order__c ord = EP_TestDataUtility.getExRackOrder();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        String result = localObj.getVMIType();
        Test.stopTest();
        System.AssertEquals(true,result != EP_Common_Constant.BLANK);
        System.AssertEquals(true,result.equalsIgnoreCase(EP_Common_Constant.NONVMI));
    }
    
    static testMethod void getVMIType_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        String result = localObj.getVMIType();
        Test.stopTest();
        System.AssertEquals(true,result != EP_Common_Constant.BLANK);
        EP_AccountDomainObject accountDomainObj = new EP_AccountDomainObject(ord.EP_ShipTo__c);
        String shipToType = accountDomainObj.localAccount.EP_ShipTo_Type__c;
        System.AssertEquals(true,result.equalsIgnoreCase(shipToType));
    }
    
    static testMethod void getProductSoldAsType_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        String result = localObj.getProductSoldAsType();
        Test.stopTest();
        System.AssertEquals(true,result != EP_Common_Constant.BLANK);
        System.AssertEquals(true,result.equalsIgnoreCase(EP_Common_Constant.PRODUCT_BULK));
    }
    static testMethod void getConsignmentType_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        String result = localObj.getConsignmentType();
        Test.stopTest();
        System.AssertEquals(true,result != EP_Common_Constant.BLANK);
        EP_AccountDomainObject accountDomainObj = new EP_AccountDomainObject(ord.EP_ShipTo__c);
        String consignType = accountDomainObj.localAccount.EP_Consignment_Type__c;
        System.AssertEquals(true,result.equalsIgnoreCase(consignType));
        
    }
    static testMethod void getDeliveryType_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        String result = localObj.getDeliveryType();
        Test.stopTest();
        System.AssertEquals(true,result.equalsIgnoreCase(EP_Common_Constant.DELIVERY));
    }
    static testMethod void isOrderSellToInvoiceOverdue_PositiveScenariotest() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        Boolean result = localObj.isOrderSellToInvoiceOverdue();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    
Public static testMethod void isOrderSellToInvoiceOverdue_NegativeScenariotest() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrderWithInvoiceOverdue();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        Boolean result = localObj.isOrderSellToInvoiceOverdue();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    static testMethod void getOrder_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        csord__Order__c result = localObj.getOrder();
        Test.stopTest();
        System.AssertEquals(true,result.Status__c.contains(EP_Common_Constant.ORDER_DRAFT_STATUS));
    }
    static testMethod void setOrder_test() {
        //Order ord = new Order();
        csord__Order__c orderObj = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(orderObj.Id);
        Test.startTest();
        localObj.setOrder(orderObj);
        Test.stopTest();
        System.AssertEquals(true,localObj.getOrder().Status__c.equalsIgnoreCase(EP_Common_Constant.ORDER_DRAFT_STATUS));
    }
    static testMethod void isInventoryLow_PositiveScenariotest() {
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderNegativeScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        Boolean result = localObj.isInventoryLow();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isInventoryLow_PositiveScenario_Else_test() {
        EP_OrderDomainObject localObj = EP_TestDataUtility.getOrderStateAcceptedDomainObject();
        delete [Select Id from EP_Inventory__c];
        Test.startTest();
        Boolean result = localObj.isInventoryLow();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isInventoryLow_NegativeScenariotest() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        Boolean result = localObj.isInventoryLow();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    static testMethod void getInventoryForOrderLines_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Id productId = [Select product2Id from PriceBookEntry where pricebook2Id =: ord.pricebook2Id__c limit 1].product2Id;
        Map<id,EP_Inventory__c> inventoryMap;
        Test.startTest();
        inventoryMap = localObj.getInventoryForOrderLines();
        Test.stopTest();
        System.AssertEquals(true,inventoryMap.size() > 0); 
        System.AssertEquals(true,inventoryMap.get(productId) <> Null);
    }
    
    /*
    static testMethod void sendNonDeliveredEmailNotification_test() {
        User userObj = [Select id,name from User where Id=: UserInfo.getUserId()];
        System.runAs(userObj){
            //EP_TestDataUtility.createGroup();
        }
        Order ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Set<String> setGrpName = new set<String>{GROUPNAME};
        String ordNumber = localObj.getOrder().orderNumber;
        String reasonForNODelivery = TEMPSTR;
        Test.startTest();
        
        localObj.sendNonDeliveredEmailNotification(setGrpName,ordNumber,reasonForNODelivery);
        Test.stopTest();
        Integer emailCount = Limits.getEmailInvocations();
        system.debug('***'+Limits.getEmailInvocations());
        //System.AssertEquals(true,emailCount>0);
    }*/
    static testMethod void setSellTo_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        EP_AccountMapper acctMapper = new EP_AccountMapper();
        list<Account> accList = acctMapper.getRecordsByIds(new set<id> {ord.AccountId__c});
        Account sellTo = accList.get(0);
        Test.startTest();
        localObj.setSellTo(sellTo);
        Test.stopTest();
        system.assert(True,localObj.getOrder().EP_Sell_To__c == sellTo.Id);
    }
    static testMethod void setBillTo_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        EP_AccountMapper acctMapper = new EP_AccountMapper();
        Account billTo = acctMapper.getAccountRecord(ord.AccountId__c);
        Test.startTest();
        localObj.setBillTo(billTo);
        Test.stopTest();
        system.assert(True,localObj.getOrder().EP_Bill_To__c == billTo.Id);
        
    }
    /*
    static testMethod void startInventoryApproval_test() {
        Order ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        localObj.submitForInventoryApproval();
        Test.stopTest();
        system.assert(true,Approval.isLocked(ord.id));
    }*/
    static testMethod void isConsignment_PositiveScenariotest() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        Boolean result = localObj.isConsignment();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isConsignment_NegativeScenariotest() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrderNegativeScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        Boolean result = localObj.isConsignment();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isConsumption_PositiveScenariotest() {
        csord__Order__c ord = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        Boolean result = localObj.isConsumption();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isConsumption_NegativeScenariotest() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrderNegativeScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        Boolean result = localObj.isConsumption();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
        
 public static testMethod void isTransfer_PositiveScenariotest() {
        csord__Order__c ord = EP_TestDataUtility.getTransferOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        localObj.isTransfer();
        Boolean result = localObj.isTransfer();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isTransfer_NegativeScenariotest() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrderNegativeScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        Boolean result = localObj.isTransfer();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void deleteOrderLineItems_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        localObj.deleteOrderLineItems();
        Test.stopTest();
        EP_OrderItemMapper EP_OrderItemMapperobj = new EP_OrderItemMapper();
        List<OrderItem> orderItemsToDelete = EP_OrderItemMapperobj.getOrderLineItemsforOrder(ord.id);
        System.AssertEquals(true,orderItemsToDelete == NULL || orderItemsToDelete.isEmpty());
    }

    static testMethod void setOrderItemWithInventoryAvailability_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        system.debug('localObj.getOrderItems()============' + localObj.getOrderItems());
        Test.startTest();
        localObj.setOrderItemWithInventoryAvailability();
        Test.stopTest();
        System.AssertNotEquals(localObj.getOrderItems()[0].EP_Inventory_Availability__c, null);
    }

    static testMethod void setOrderStockHoldingLocation_Step1_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        Account accObj = EP_TestDataUtility.getSellToASProspectDomainObjectPositiveScenario().getAccount();
        EP_Stock_Holding_Location__c stockHoldingLocationObj = [SELECT id FRom EP_Stock_Holding_Location__c LIMIT 1];
        List<OrderItem> updateOrderItems = new List<OrderItem>();
        List<OrderItem> listOrderItem = [SELECT Id , EP_Stock_Holding_Location__c , EP_Stock_Holding_Location__r.Stock_Holding_Location__c FROM OrderItem];
        for(OrderItem orderItemObj :listOrderItem ){
        	orderItemObj.EP_Stock_Holding_Location__c = stockHoldingLocationObj.id;
        	updateOrderItems.add(orderItemObj);
        } 
        update updateOrderItems;
        
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        localObj.setOrderStockHoldingLocation();
        Test.stopTest();
        System.AssertNotEquals(localObj.getOrder().Stock_Holding_Location__c, null);
    } 
    
    static testMethod void getTypeObject_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        EP_Ordertype result = localObj.getTypeObject();
        Test.stopTest();
        System.AssertNotEquals(null, result);
    }   

    static testMethod void getEpochTypeObject_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        EP_OrderEpoch result = localObj.getEpochTypeObject();
        Test.stopTest();
        System.AssertNotEquals(null, true);
    }   

    static testMethod void getConsignmentTypeObject_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        EP_ConsignmentType result = localObj.getConsignmentTypeObject();
        Test.stopTest();
        System.AssertNotEquals(null, result);
    }   

    static testMethod void getProductSoldAsTypeObject_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        EP_ProductSoldAs result = localObj.getProductSoldAsTypeObject();
        Test.stopTest();
        System.AssertNotEquals(null, result);
    }   
    
    static testMethod void getDeliveryTypeObject_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        EP_DeliveryType result = localObj.getDeliveryTypeObject();
        Test.stopTest();
        System.AssertNotEquals(null, result);
    }   
    
    static testMethod void setOrderTypeField_If_test() {
        EP_OrderDomainObject localObj = EP_TestDataUtility.getOrderStateAcceptedDomainObject();
        Test.startTest();
        localObj.setOrderTypeField();
        Test.stopTest();
        System.AssertEquals(localObj.getOrder().Type__c, EP_Common_Constant.NON_CONSIGNMENT);
    }   
    
    static testMethod void setOrderTypeField_Else_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        localObj.setOrderTypeField();
        Test.stopTest();
        System.AssertNotEquals(localObj.getOrder().Type__c, EP_Common_Constant.NON_CONSIGNMENT);
    }
    
    /*
    static testMethod void updatePaymentTermAndMethod_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        localObj.updatePaymentTermAndMethod();
        Test.stopTest();
        //methode is delegating to another methode , hence added dummy assert
        System.AssertEquals(true, true);
    }
    */
    
    /*
    static testMethod void findRecordType_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        Id result = localObj.findRecordType();
        Test.stopTest();
        System.AssertNotEquals(null, result);
    }
    */
    
    static testMethod void getDeliveryTypes_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        List<String> result = localObj.getDeliveryTypes();
        Test.stopTest();
        System.AssertNotEquals(null, result);
    } 
    
    /*
    static testMethod void getShipToAccounts_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        List<Account> result = localObj.getShipToAccounts();
        Test.stopTest();
        System.AssertNotEquals(null, result);
    }
    */
    
    /*
    static testMethod void isValidLoadingDate_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        Boolean result = localObj.isValidLoadingDate();
        Test.stopTest();
        System.AssertEquals(false, result);
    }
    */
    
    /*
    static testMethod void isValidExpectedDate_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        Boolean result = localObj.isValidExpectedDate();
        Test.stopTest();
        System.AssertEquals(false, result);
    }
    */
    
    /*
    static testMethod void isValidOrderDate_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        Boolean result = localObj.isValidOrderDate();
        Test.stopTest();
        System.AssertEquals(false, result);
    }
    */
    
    /*
    static testMethod void setRetroOrderDetails_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        localObj.setRetroOrderDetails();
        Test.stopTest();
        //methode is delegating another methode , hence adding dummy assert
        System.AssertEquals(true, true);
    }
    */
    
    /*
    static testMethod void showCustomerPOAllowed_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        localObj.showCustomerPOAllowed();
        Test.stopTest();
        //methode is delegating another methode , hence adding dummy assert
        System.AssertEquals(true, true);
    }
    */
    
     
Public static testMethod void isOrderItemQuantityChanged_Positivetest() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
       // OrderItem  OrderItemObj = [SELECT ID , EP_Prior_Quantity__c , Quantity  FROM OrderItem LIMIT 1];
        
        cscfga__Product_Basket__c basketInsert = new cscfga__Product_Basket__c(
            Name = 'New Basket',
            OwnerId = UserInfo.getUserId()
        );
        insert basketInsert;
        csord__Order_Line_Item__c  ordLine = new csord__Order_Line_Item__c();
       
        ordLine.EP_SeqId__c = '0480000091';
        ordLine.EP_WinDMS_Line_ItemId__c = '2001002';
        ordLine.EP_Tank_Number__c = 'Diesel0Tnk';
        ordLine.EP_Quantity_UOM__c = 'LT';
        ordLine.EP_WinDMS_StockHldngLocId__c = '43001';
        ordline.Quantity__c = 100;
        ordLine.EP_Prior_Quantity__c = 200;
        ordline.csord__Identification__c = basketInsert.id;
        ordline.csord__Order__c = ord.id;
        insert ordLine;
        
        ordLine.EP_Prior_Quantity__c = 0 ;
        update ordLine;
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        Boolean result = localObj.isOrderItemQuantityChanged();
        Test.stopTest();
        System.AssertEquals(true, result);
    }
    
    
   
   Public static testMethod void isOrderItemQuantityChanged_Negativetest() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        
        cscfga__Product_Basket__c basketInsert = new cscfga__Product_Basket__c(
            Name = 'New Basket',
            OwnerId = UserInfo.getUserId()
        );
        insert basketInsert;
        csord__Order_Line_Item__c  ordLine = new csord__Order_Line_Item__c();
       
        ordLine.EP_SeqId__c = '0480000091';
        ordLine.EP_WinDMS_Line_ItemId__c = '2001002';
        ordLine.EP_Tank_Number__c = 'Diesel0Tnk';
        ordLine.EP_Quantity_UOM__c = 'LT';
        ordLine.EP_WinDMS_StockHldngLocId__c = '43001';
        ordline.Quantity__c = 100;
        ordLine.EP_Prior_Quantity__c = 200;
        ordline.csord__Identification__c = basketInsert.id;
        ordline.csord__Order__c = ord.id;
        insert ordLine;
        //OrderItem  OrderItemObj = [SELECT ID , EP_Prior_Quantity__c , Quantity  FROM OrderItem LIMIT 1];
        ordLine.EP_Prior_Quantity__c = ordLine.Quantity__c +1;
        
        update ordLine;
        Test.startTest();
        Boolean result = localObj.isOrderItemQuantityChanged();
        Test.stopTest();
        System.AssertEquals(false, result);
    }
    
    
    
   Public static testMethod void revertOrderItemQuantity_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        List<csord__Order_Line_Item__c> insertOrderItems = new List<csord__Order_Line_Item__c>();
        List<csord__Order_Line_Item__c> updateOrderItems = new List<csord__Order_Line_Item__c>();
        cscfga__Product_Basket__c basketInsert = new cscfga__Product_Basket__c(
            Name = 'New Basket',
            OwnerId = UserInfo.getUserId()
        );
        insert basketInsert;
        csord__Order_Line_Item__c  ordLine = new csord__Order_Line_Item__c();
       
        ordLine.EP_SeqId__c = '0480000091';
        ordLine.EP_WinDMS_Line_ItemId__c = '2001002';
        ordLine.EP_Tank_Number__c = 'Diesel0Tnk';
        ordLine.EP_Quantity_UOM__c = 'LT';
        ordLine.EP_WinDMS_StockHldngLocId__c = '43001';
        ordline.Quantity__c = 100;
        ordLine.EP_Prior_Quantity__c = 200;
        ordline.csord__Identification__c = basketInsert.id;
        ordline.csord__Order__c = ord.id;
        insertOrderItems.add(ordLine);
        insert insertOrderItems;
        for(csord__Order_Line_Item__c obj : insertOrderItems){
            obj.EP_Prior_Quantity__c = 100;
            updateOrderItems.add(obj);
        }
      
        update updateOrderItems;
        Test.startTest();
        localObj.revertOrderItemQuantity();
        Test.stopTest();
        System.AssertEquals(updateOrderItems[0].EP_Prior_Quantity__c, updateOrderItems[0].Quantity__c );
    }
    
    /*
    static testMethod void updateTransportService_test() {
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Test.startTest();
        localObj.updateTransportService();
        Test.stopTest();
        //methode is delegating another methode , hence adding dummy assert
        System.AssertEquals(true, true);
    }
    */
    
   
 Public static testMethod void doUpdateTransportFields_Step1_test() {
        EP_StockHoldingLocationMapper stockHoldingLocationMapper = new EP_StockHoldingLocationMapper();
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        EP_Stock_Holding_Location__c stockholdingLocationObj = stockHoldingLocationMapper.getRecordsById(ord.Stock_Holding_Location__c); 
        Test.startTest();
        localObj.doUpdateTransportFields(ord);
        Test.stopTest();
        System.AssertEquals(stockholdingLocationObj.Stock_Holding_Location__r.Name, ord.EP_Supply_Location_Name__c);
    }
    
   
 Public static testMethod void doUpdateTransportFields_Step2_test() {
        EP_StockHoldingLocationMapper stockHoldingLocationMapper = new EP_StockHoldingLocationMapper();
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Account accObj = EP_TestDataUtility.getStorageLocationASActiveDomainObjectPositiveScenario().getAccount();
        accObj.EP_Nav_Stock_Location_Id__c = '123';
        update accObj;
        ord.EP_Supply_Location_Pricing__c = accObj.id;
        update ord;
        Test.startTest();
        localObj.doUpdateTransportFields(ord);
        Test.stopTest();
        System.AssertEquals(accObj.EP_Nav_Stock_Location_Id__c, ord.EP_NAV_Pricing_Stock_Holding_Location_ID__c );
    
}    
    
  
  Public static testMethod void doUpdateTransportFields_Step3_test() {
        EP_StockHoldingLocationMapper stockHoldingLocationMapper = new EP_StockHoldingLocationMapper();
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Account accObj = EP_TestDataUtility.getVendorASActiveDomainObjectPositiveScenario().getAccount();
        // TFS fix 45559,45560,45567,45568 start EP_NAV_ID__c deprecated, EP_NAV_Vendor_Id__c is replaced by EP_Source_Entity_ID__c and EP_Vendor_Type__c is replaced by EP_VendorType__c
        accObj.EP_NAV_Vendor_Id__c = '456';
        accObj.EP_Source_Entity_ID__c = '123';
        update accObj;
        ord.EP_Transporter_Pricing_No__c = accObj.id;
        ord.EP_Transporter_Pricing__c = accObj.id;
        update ord;
        Test.startTest();
        localObj.doUpdateTransportFields(ord);
        Test.stopTest();
        System.AssertEquals(accObj.EP_NAV_Vendor_Id__c, ord.EP_Transporter_Pricing_No__c );
    }
    
    static testMethod void hasStockLocationPricing_test() {
    	EP_StockHoldingLocationMapper stockHoldingLocationMapper = new EP_StockHoldingLocationMapper();
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        Account accObj = EP_TestDataUtility.getStorageLocationASActiveDomainObjectPositiveScenario().getAccount();
        accObj.EP_Nav_Stock_Location_Id__c = '123';
        update accObj;
        ord.EP_Supply_Location_Pricing__c = accObj.id;
        update ord;
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Set<Id> setAccountIds = new Set<Id>();
        setAccountIds.add(ord.AccountId__c);
        setAccountIds.add(ord.EP_Transporter_Pricing__c);
        setAccountIds.add(ord.EP_Supply_Location_Pricing__c);
        setAccountIds.add(ord.EP_Transporter__c);
        Map<Id,Account> mapAccounts = new EP_AccountMapper().getMapOfRecordsByIds(setAccountIds);
        EP_Stock_Holding_Location__c stockholdingLocationObj = stockHoldingLocationMapper.getRecordsById(ord.Stock_Holding_Location__c); 
        Test.startTest();
        Boolean result = localObj.hasStockLocationPricing(mapAccounts,ord);
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
   
static testMethod void hasTransportPricingVendorId_test() {
        EP_StockHoldingLocationMapper stockHoldingLocationMapper = new EP_StockHoldingLocationMapper();
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        Account accObj = EP_TestDataUtility.getVendorASActiveDomainObjectPositiveScenario().getAccount();
        accObj.EP_NAV_Vendor_Id__c = '123';
        update accObj;
        ord.EP_Transporter_Pricing_No__c = accObj.id;
        ord.EP_Transporter_Pricing__c = accObj.id;
        update ord;
        Set<Id> setAccountIds = new Set<Id>();
        setAccountIds.add(ord.AccountId__c);
        setAccountIds.add(ord.EP_Transporter_Pricing__c);
        setAccountIds.add(ord.EP_Supply_Location_Pricing__c);
        setAccountIds.add(ord.EP_Transporter__c);
        Map<Id,Account> mapAccounts = new EP_AccountMapper().getMapOfRecordsByIds(setAccountIds);
        Test.startTest();
        Boolean result = localObj.hasTransportPricingVendorId(mapAccounts,ord);
        Test.stopTest();
        System.AssertEquals(true,result);

}    
    
    
  
 public static testMethod void hasTransportVendorId_test() {
        EP_StockHoldingLocationMapper stockHoldingLocationMapper = new EP_StockHoldingLocationMapper();
        csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
        Set<Id> setAccountIds = new Set<Id>();
        setAccountIds.add(ord.AccountId__c);
        setAccountIds.add(ord.EP_Transporter_Pricing__c);
        setAccountIds.add(ord.EP_Supply_Location_Pricing__c);
        setAccountIds.add(ord.EP_Transporter__c);
        Map<Id,Account> mapAccounts = new EP_AccountMapper().getMapOfRecordsByIds(setAccountIds);
        EP_OrderDomainObject localObj = new EP_OrderDomainObject(ord.Id);
        EP_Stock_Holding_Location__c stockholdingLocationObj = stockHoldingLocationMapper.getRecordsById(ord.Stock_Holding_Location__c); 
        Test.startTest();
        Boolean result = localObj.hasTransportVendorId(mapAccounts,ord);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
}