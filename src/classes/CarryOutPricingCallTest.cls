@isTest
private class CarryOutPricingCallTest {
    class Order_Submission_Mock implements HttpCalloutMock {    

        public Order_Submission_Mock(Boolean pass) {
            this.PassTest = pass;
        }

        public Boolean PassTest {get; private set;}    

        public HTTPResponse respond(HTTPRequest req) {
            HTTPResponse retVal = new HTTPResponse();
            retVal.setStatusCode(this.PassTest ? 202 : 404);
            retVal.setBody(JSON.serialize(new List<String> { 'A', 'B' } ));
            return retVal;
        }

    }

    @isTest static void test_method_one() {
        Test.setMock(HttpCalloutMock.class, new Order_Submission_Mock(true));
        String messageType = 'SEND_ORDER_PRICING_REQUEST';
         
         
         EP_CS_OutboundMessageSetting__c msgSetting = new EP_CS_OutboundMessageSetting__c();
         msgSetting.End_Point__c = 'https://api.eu.coolfurnace.net/WP3SIT/{CID}/ImportFuelPriceBusinessHit:ImportMsgIDPayload';
         msgSetting.name = 'SEND_ORDER_PRICING_REQUEST';
         msgSetting.EP_Subscription_Key__c = '166adcf29d864f1f8f24a2e63c32001c';
         msgSetting.EP_Success_Status_Codes__c = '200;201;202;204';
         msgSetting.Interface_Type__c = 'PER';
         msgSetting.Local__c = 'GBL';
         msgSetting.ObjectName__c = 'pricingDetails';
         msgSetting.Payload_VF_Page_Name__c = 'EP_OrderPricingRequestXML';
         msgSetting.Response_End_Point__c = 'https://cs20.salesforce.com:8443/services/apexrest/v2/AcknowledgementWS2';
         msgSetting.Response_Handler_Name__c = 'EP_PricingResponseHandler';
         msgSetting.Source__c = 'SFD';
         msgSetting.Target_Integration_System__c = 'NAV';
         insert msgSetting;
         system.debug('msgSetting~~~~'+msgSetting);
        


Account acc = new Account();
        acc.EP_VendorType__c = '3rd Party Stock Supplier';
        acc.EP_Vendor_Type__c = '3rd Party Stock Supplier';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        acc.Name='testEPAccount';
        acc.EP_Status__c = '06-Blocked';
        insert acc;
        
        Opportunity opp = new Opportunity();
        opp.name = 'test';
        opp.accountid = acc.id;
        opp.closedate = date.today();
        opp.stagename = 'Prospecting';
        
        //return opp;
        
        //Opportunity opp = TQTestUtils.buildOpportunity();
        
        
        csord__Order_Request__c morc = new csord__Order_Request__c();
        morc.Name = 'Test MORC';
        morc.csord__Module_Name__c = 'Test Module';
        morc.csord__Module_Version__c = 'Test Version';
        morc.csord__Process_Status__c = 'Requested';
        insert morc;

        csord__Order__c moc = new csord__Order__c();
        moc.Name = 'Test MOC';
        moc.csord__Identification__c = 'testOrder0';
        moc.csord__Order_Request__c = morc.Id;
        moc.csord__Status__c = 'Active';
        moc.csord__Status2__c = 'Active';
        moc.EP_Requested_Delivery_Date__c = Date.newInstance(2018, 9, 9);
        moc.csordtelcoa__Opportunity__c = opp.Id;
        moc.pricingResponseJson__c = '{ "Payload" : { "any0" : { "pricingResponse" : { "priceDetails" : { "priceDetails" : { "seqId" : "0769ee1cc7303ad6d8-6f2fec360ba48a-d58a", "companyCode" : "EPA", "priceType" : "Fuel Price", "priceRequestSource" : "", "currencyCode" : "USD", "deliveryType" : "Delivery", "customerId" : "00005398", "shipToId" : "00005401", "supplyLocationId" : "9311", "transporterId" : "", "supplierId" : "9311", "priceDate" : "2018-04-27", "onRun" : "", "orderId" : "", "applyOrderQuantity" : "No", "totalOrderQuantity" : "7,200.00", "priceConsolidationBasis" : "Summarised price", "versionNr" : ""}, "totalAmt" : { "totalAmount" : "3.91", "totalAmountLCY" : "3.91", "totalTaxAmount" : "0.00", "totalTaxAmountLCY" : "0.00", "totalDiscountAmount" : "0.00", "totalDiscountAmountLCY" : "0.00"}, "priceLineItems" : { "lineItem":[ { "lineItemInfo" : { "seqId" : "0769ee1cc7303ad6d8-6f2fec360ba48a-d58a", "orderId" : "", "lineItemId" : "70054", "itemId" : "70054", "quantity" : "3,600.00", "unitPrice" : "0.97776", "unitPriceLCY" : "0.97776", "invoiceDetails" : { "invoiceComponent":[ { "seqId" : "0769ee1cc7303ad6d8-6f2fec360ba48a-d58a", "type" : "Fuel Price", "name" : "Total Price", "amount" : "0.97776", "amountLCY" : "0.97776", "taxPercentage" : "0.00", "taxAmount" : "0.00000", "taxAmountLCY" : "0.00000", "totalAmount" : "0.97776", "totalAmountLCY" : "0.97776"},{ "seqId" : "0769ee1cc7303ad6d8-6f2fec360ba48a-d58a", "type" : "Fuel Price", "name" : "Total Price", "amount" : "0.97776", "amountLCY" : "0.97776", "taxPercentage" : "0.00", "taxAmount" : "0.00000", "taxAmountLCY" : "0.00000", "totalAmount" : "0.97776", "totalAmountLCY" : "0.97776"} ] }, "acctComponents" : { "acctComponent":[ { "seqId" : "0769ee1cc7303ad6d8-6f2fec360ba48a-d58a", "componentCode" : "EFE", "glAccountNr" : "", "baseAmount" : "0.97776", "baseAmountLCY" : "0.97776", "taxRate" : "0.00", "taxAmount" : "0.00000", "taxAmountLCY" : "0.00000", "totalAmount" : "0.97776", "totalAmountLCY" : "0.97776", "isVAT" : "No"},{ "seqId" : "0769ee1cc7303ad6d8-6f2fec360ba48a-d58a", "componentCode" : "EFE", "glAccountNr" : "", "baseAmount" : "0.97776", "baseAmountLCY" : "0.97776", "taxRate" : "0.00", "taxAmount" : "0.00000", "taxAmountLCY" : "0.00000", "totalAmount" : "0.97776", "totalAmountLCY" : "0.97776", "isVAT" : "No"} ] }}, "error" : { "ErrorCode" : "", "ErrorDescription" : ""}, "error" : { "ErrorCode" : "", "ErrorDescription" : ""}},{ "lineItemInfo" : { "seqId" : "0769ee1cc7303ad6d8-6f2fec360ba48a-d58a", "orderId" : "", "lineItemId" : "70054", "itemId" : "70054", "quantity" : "3,600.00", "unitPrice" : "0.97776", "unitPriceLCY" : "0.97776", "invoiceDetails" : { "invoiceComponent":[ { "seqId" : "0769ee1cc7303ad6d8-6f2fec360ba48a-d58a", "type" : "Fuel Price", "name" : "Total Price", "amount" : "0.97776", "amountLCY" : "0.97776", "taxPercentage" : "0.00", "taxAmount" : "0.00000", "taxAmountLCY" : "0.00000", "totalAmount" : "0.97776", "totalAmountLCY" : "0.97776"},{ "seqId" : "0769ee1cc7303ad6d8-6f2fec360ba48a-d58a", "type" : "Fuel Price", "name" : "Total Price", "amount" : "0.97776", "amountLCY" : "0.97776", "taxPercentage" : "0.00", "taxAmount" : "0.00000", "taxAmountLCY" : "0.00000", "totalAmount" : "0.97776", "totalAmountLCY" : "0.97776"} ] }, "acctComponents" : { "acctComponent":[ { "seqId" : "0769ee1cc7303ad6d8-6f2fec360ba48a-d58a", "componentCode" : "EFE", "glAccountNr" : "", "baseAmount" : "0.97776", "baseAmountLCY" : "0.97776", "taxRate" : "0.00", "taxAmount" : "0.00000", "taxAmountLCY" : "0.00000", "totalAmount" : "0.97776", "totalAmountLCY" : "0.97776", "isVAT" : "No"},{ "seqId" : "0769ee1cc7303ad6d8-6f2fec360ba48a-d58a", "componentCode" : "EFE", "glAccountNr" : "", "baseAmount" : "0.97776", "baseAmountLCY" : "0.97776", "taxRate" : "0.00", "taxAmount" : "0.00000", "taxAmountLCY" : "0.00000", "totalAmount" : "0.97776", "totalAmountLCY" : "0.97776", "isVAT" : "No"} ] }}, "error" : { "ErrorCode" : "", "ErrorDescription" : ""}, "error" : { "ErrorCode" : "", "ErrorDescription" : ""}} ] }, "Error" : { "ErrorCode" : "", "ErrorDescription" : ""}}}}}}';
        insert moc;
        
        csord__Subscription__c msc = new csord__Subscription__c();
        msc.Name = 'Test MSC';
        msc.csord__Identification__c = 'testSubscription0';
        msc.csord__Order_Request__c = morc.Id;
        msc.csord__Order__c = moc.Id;
        insert msc;
  
        csord__Order_Line_Item__c oli = new csord__Order_Line_Item__c();
        oli.Quantity__c = 10;
        oli.EP_Company_Code__c = 'CA';
        oli.EP_Product_Code__c = 'CB';
        oli.csord__Order__c = moc.Id;
        oli.csord__Identification__c = 'testOLI0';
         oli.EP_Product_Code__c = '70052';
        insert new List<csord__Order_Line_Item__c> { oli };
        
        CSPOFA__Orchestration_Process_Template__c moptc = new CSPOFA__Orchestration_Process_Template__c();
        moptc.Name = 'Test MOPTC';
        insert moptc;
        
        CSPOFA__Orchestration_Process__c mopc = new CSPOFA__Orchestration_Process__c();
        mopc.Name = 'Test MOPC';
        mopc.CSPOFA__Orchestration_Process_Template__c = moptc.Id;
        mopc.Order__c = moc.Id;
        insert mopc;
        
        CSPOFA__Orchestration_Step__c mosc = new CSPOFA__Orchestration_Step__c();
        mosc.Name = 'Test MOSC';
        mosc.CSPOFA__Orchestration_Process__c = mopc.Id;
        insert mosc;
        
        csord__Order_Line_Item__c oli1 = new csord__Order_Line_Item__c();
        oli1.Quantity__c = 10;
        oli1.EP_Company_Code__c = 'CA';
        oli1.EP_Product_Code__c = 'CB';
        oli1.csord__Order__c = mopc.Order__c;
        oli1.csord__Identification__c = 'testOLI0';
  
        insert new List<csord__Order_Line_Item__c> { oli1 };
       system.debug('oli1List'+oli1);
        Decimal totalAmount = 0;
        Boolean isVMIIndicativePriceFlag = false;
        list<CSPOFA__Orchestration_Step__c> moscList = new list<CSPOFA__Orchestration_Step__c>();
        moscList.add(mosc); 
        String endPoint = EP_Common_Constant.COMPANY_CODE_URL_TOKAN;
     
        test.startTest();
         system.assertEquals(true, moscList.size() > 0);
        CarryOutPricingCall plic = new CarryOutPricingCall();
       
        plic.process(moscList);
        plic.performCallouts(moscList);
        plic.processCS(moscList);
        test.stopTest();
    }
}
