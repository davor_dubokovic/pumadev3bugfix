@isTest
private class EP_OrderSyncWithNAVXML_UT {
    
    private static final string MESSAGE_TYPE = 'SFDC_TO_NAV_ORDER_SYNC'; 
    @testSetup
    public static void init(){
        Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
    	Test.loadData(EP_CS_Communication_Settings__c.sObjectType, 'EP_CS_CommunicationSettingTestData');
    }
     
   static testMethod void createXML_Test() {
       EP_OrderSyncWithNAVXML localObj = new EP_OrderSyncWithNAVXML();
       localObj.OrderObj = EP_TestDataUtility.getTransferOrder();
       localObj.recordid = localObj.OrderObj.id;
       localObj.messageId = EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
       localObj.messageType = MESSAGE_TYPE;
       Test.startTest();
       String result = localObj.createXML();  
       Test.stopTest();
       System.assert(!String.isBlank(result));
   }

  static testMethod void createPayload_Test() {
       EP_OrderSyncWithNAVXML localObj = new EP_OrderSyncWithNAVXML();
       //localObj.orderObj = EP_TestDataUtility.getTransferOrderWithInvoiceLineItem(); //Accenture CCT, 8/28/2018
       
       EP_OrderDomainObject odo = EP_TestDataUtility.getNonVMINonConsignmentOrderPositiveScenario();  //Accenture CCT, 8/28/2018
       localObj.orderObj = [SELECT Id FROM csord__Order__c LIMIT 1];//Accenture CCT, 8/28/2018
       localObj.recordid = localObj.OrderObj.id;
       localObj.orderObj.EP_OnOff_Run__c = EP_OrderConstant.ON_STR; //Accenture CCT, 8/28/2018
       update localObj.orderObj;//Accenture CCT, 8/28/2018
       List<csord__Order_Line_Item__c> oliList = new  List<csord__Order_Line_Item__c>();//Accenture CCT, 8/30/2018
       //Accenture CCT, 8/30/2018
       Integer num = 12345;
       for(csord__Order_Line_Item__c oli : [SELECT Id, EP_WinDMS_Line_Item_Reference_Number__c FROM csord__Order_Line_Item__c]) {
       oli.EP_WinDMS_Line_Item_Reference_Number__c = String.valueOf(num + 1);
       oliList.add(oli);
       }
       update oliList;
       localObj.setOrderLineItems();
       
       
       //localObj.orderLineItemsWrapper[0].orderLineItem = [SELECT Id, EP_WinDMS_Line_Item_Reference_Number__c FROM csord__Order_Line_Item__c LIMIT 1];
       
       localObj.messageId = EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
       localObj.messageType = MESSAGE_TYPE;
       Test.startTest();
       localObj.init();
       localObj.createPayload();  
       Test.stopTest();
	   
       ///no assert needed. Value are only being set
   } 
    static testMethod void createPayloadElse_Test() {
       EP_OrderSyncWithNAVXML localObj = new EP_OrderSyncWithNAVXML();
       //localObj.orderObj = EP_TestDataUtility.getTransferOrderWithInvoiceLineItem(); //Accenture CCT, 8/28/2018
       
       EP_OrderDomainObject odo = EP_TestDataUtility.getNonVMINonConsignmentOrderPositiveScenario();  //Accenture CCT, 8/28/2018
       localObj.orderObj = [SELECT Id FROM csord__Order__c LIMIT 1];//Accenture CCT, 8/28/2018
       localObj.recordid = localObj.OrderObj.id;
       localObj.orderObj.EP_OnOff_Run__c = EP_OrderConstant.ON_STR; //Accenture CCT, 8/28/2018
       localObj.orderObj.VMI_Price_Request_XML__c='<?xml version="1.0" encoding="UTF-8"?><Payload><any0><pricingRequest><requestHeader><seqId>12b79e072fda629832-cc37c85cd7c101-4ca9</seqId><companyCode>EPA</companyCode><priceType>Fuel Price</priceType><priceRequestSource></priceRequestSource><currencyCode>USD</currencyCode><deliveryType>Delivery</deliveryType><customerId>00005404</customerId><shipToId>00005422</shipToId><supplyLocationId>11015</supplyLocationId><transporterId></transporterId><supplierId>11015</supplierId><priceDate>2018-10-10</priceDate><onRun></onRun><orderId></orderId><applyOrderQuantity></applyOrderQuantity><totalOrderQuantity>600.00</totalOrderQuantity><priceConsolidationBasis>Summarised price</priceConsolidationBasis><versionNr>20181010T075129.108</versionNr></requestHeader><requestLines><line><seqId>12b79e072fda629832-cc37c85cd7c101-4ca9</seqId><orderId></orderId><lineItemId>1</lineItemId><itemId>70053</itemId><quantity>600.00</quantity><deliveryType>Delivery</deliveryType><shipToId>00005422</shipToId><supplyLocationId>11015</supplyLocationId><onRun></onRun><priceDate>2018-10-10</priceDate></line></requestLines></pricingRequest></any0></Payload>';
       update localObj.orderObj;//Accenture CCT, 8/28/2018
       List<csord__Order_Line_Item__c> oliList = new  List<csord__Order_Line_Item__c>();//Accenture CCT, 8/30/2018
       //Accenture CCT, 8/30/2018
       Integer num = 12345;
       for(csord__Order_Line_Item__c oli : [SELECT Id, EP_WinDMS_Line_Item_Reference_Number__c FROM csord__Order_Line_Item__c]) {
       oli.EP_WinDMS_Line_Item_Reference_Number__c = String.valueOf(num + 1);
       oliList.add(oli);
       }
       update oliList;
       localObj.setOrderLineItems();
       
       
       //localObj.orderLineItemsWrapper[0].orderLineItem = [SELECT Id, EP_WinDMS_Line_Item_Reference_Number__c FROM csord__Order_Line_Item__c LIMIT 1];
       
       localObj.messageId = EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
       localObj.messageType = MESSAGE_TYPE;
       Test.startTest();
       localObj.init();
       localObj.createPayload();  
       Test.stopTest();
       //no assert needed. Value are only being set
   }   
}