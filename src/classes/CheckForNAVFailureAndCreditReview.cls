global class CheckForNAVFailureAndCreditReview implements CSPOFA.ExecutionHandler {

    public List<sObject> process(List<SObject> data) {

        List<sObject> result = new List<sObject>();
        
        List<CSPOFA__Orchestration_Step__c> stepList = (List<CSPOFA__Orchestration_Step__c>)data;
        List<CSPOFA__Orchestration_Step__c> extendedList = [SELECT Id, CSPOFA__Orchestration_Process__r.Order__c FROM CSPOFA__Orchestration_Step__c WHERE Id in :stepList];
        
        Set<Id> orderIds = new Set<Id>();
        
        for (CSPOFA__Orchestration_Step__c step : extendedList) {

            orderIds.add(step.CSPOFA__Orchestration_Process__r.Order__c);
        }

        List<csord__Order__c> orders = [SELECT Id, ep_integration_status__c, csord__Status2__c, (SELECT Id, Name FROM Credit_Exception_Requests2__r) FROM csord__Order__c 
                                            WHERE Id IN :orderIds AND ep_integration_status__c = 'FAILURE' AND csord__Status2__c = 'Awaiting Credit Review'];
        
        if (!orders.isEmpty()) {
        
            for (csord__Order__c obj :orders) {
                
                if (obj.ep_integration_status__c == 'FAILURE' && obj.csord__Status2__c == 'Awaiting Credit Review' && obj.Credit_Exception_Requests2__r != null &&  obj.Credit_Exception_Requests2__r.size() > 0) {
                    obj.ep_integration_status__c = 'SYNC';
                }
            }
            
            update orders;
        }

        for (CSPOFA__Orchestration_Step__c step : extendedList) {

            step.CSPOFA__Status__c = 'Complete';
            step.CSPOFA__Completed_Date__c = Date.today();
            step.CSPOFA__Message__c = 'Custom step succeeded';
            result.add(step);
        }

        return result;
    }
}