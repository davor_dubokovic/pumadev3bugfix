/*
   @Author          CloudSense
   @Name            InventoryActionCheckHandler
   @CreateDate      13/07/2018
   @Description     This class is responsible for cancelling Inventory Actions
   @Version         1.0
 
*/

global class InventoryActionCheckHandler implements CSPOFA.ExecutionHandler{
    
    /**
    * @Author       CloudSense
    * @Name         execute
    * @Date         13/07/2018
    * @Description  Method to process the step
    * @Param        list<SObject>
    * @return       NA
    */  
    public List<sObject> process(List<SObject> data)
    {
        
        List<sObject> result = new List<sObject>();
        //collect the data for all steps passed in, if needed
        List<CSPOFA__Orchestration_Step__c> stepList= (List<CSPOFA__Orchestration_Step__c>)data;
        Map<Id,CSPOFA__Orchestration_Step__c> stepMap = new Map<Id,CSPOFA__Orchestration_Step__c>();
        List<Id> orderIdList = new List<Id>();
        Map<Id,Id> orderIdCrIdMap = new Map<Id,Id>();
        
        List<CSPOFA__Orchestration_Step__c> extendedList = [Select
                                                                id,CSPOFA__Orchestration_Process__r.Order__c,CSPOFA__Orchestration_Process__r.Order__r.csord__Account__c,CSPOFA__Status__c,CSPOFA__Completed_Date__c,CSPOFA__Message__c
                                                            from 
                                                                CSPOFA__Orchestration_Step__c 
                                                            where 
                                                            id in :stepList];
                                                            
        system.debug('extended list is :' +extendedList);
        for(CSPOFA__Orchestration_Step__c step:extendedList){
            orderIdList.add(step.CSPOFA__Orchestration_Process__r.Order__c);
            system.debug('Order Id is :' +step.CSPOFA__Orchestration_Process__r.Order__c) ;
            //mark step Status, Completed Date, and write optional step Message
          
        }
                                          
        for(CSPOFA__Orchestration_Step__c step:extendedList){
            
            Id crId = orderIdList[0];
            
            try{
                system.debug('before cancelInventoryActions') ;
                cancelInventoryActions(crId);
                system.debug('after cancelInventoryActions') ;
            }Catch(Exception e){
                
             system.debug('exception is :' + e);
             EP_loggingService.loghandledException(e, EP_Common_Constant.EPUMA, 'process', 'InventoryActionCheckHandler',apexPages.severity.ERROR);
            
          }
          
           step.CSPOFA__Status__c ='Complete';
           step.CSPOFA__Completed_Date__c=Date.today();
           step.CSPOFA__Message__c = 'Custom step succeeded';
           result.add(step);
            
        }
        
     return result;        
        
        
    }

    public void cancelInventoryActions(Id orderId) {

    	system.debug('inside cancelInventoryActions') ;

        EP_OrderMapper mapper = new EP_OrderMapper();
        csord__Order__c order = mapper.getCSCERecordsById(orderId);
        Id inventoryReviewActionRecTypeId = Schema.SObjectType.EP_Action__c.getRecordTypeInfosByName().get('Inventory Review').getRecordTypeId();

        system.debug('order.csord__Account__c: '+order.csord__Account__c);
        system.debug('orderId: '+orderId);
        system.debug('inventoryReviewActionRecTypeId: '+inventoryReviewActionRecTypeId);

        List<EP_Action__c> inventoryActions = [
            SELECT id FROM EP_Action__c 
            WHERE EP_Action_Name__c = :EP_OrderConstant.ACTIONNAMEINVENTORYAPPROVAL 
            AND EP_Account__c = :order.csord__Account__c
            AND EP_CSOrder__c = :orderId
            AND RecordTypeId = :inventoryReviewActionRecTypeId];

        system.debug('inventoryActions: '+inventoryActions) ;

        if(inventoryActions.size() > 0){
			for(EP_Action__c action : inventoryActions) {
				action.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS;
			}
            update inventoryActions;
        }
	}  
}