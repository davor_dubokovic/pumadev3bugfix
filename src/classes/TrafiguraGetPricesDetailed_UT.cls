/***
 * Author: Pawan (CloudSense)
 * Description: Test Class for TrafiguraGetPricesDetailed .
 * Version History:
 * v1 - Created on 2018-08-17
 ***/
@isTest
public class TrafiguraGetPricesDetailed_UT {
    
   
    public static testmethod void testTrafiguraGetPricesDetailed() {
       
        Account testBasketAccount = new Account();        
        testBasketAccount.EP_VendorType__c = '3rd Party Stock Supplier';
        testBasketAccount.EP_Vendor_Type__c = '3rd Party Stock Supplier';
        testBasketAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        testBasketAccount.Name='testEPAccount';
        testBasketAccount.EP_Status__c = '06-Blocked';
        insert testBasketAccount;

		System.Assert(testBasketAccount.Id != null);
        cscfga__Product_Basket__c testBasket = new cscfga__Product_Basket__c();
        testBasket.csordtelcoa__Account__c = testBasketAccount.Id;
        INSERT testBasket;
        
        Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
        
        String encodedContentsString = '<?xml version="1.0" encoding="UTF-8"?><Payload><any0><pricingRequest><requestHeader><seqId>3224c1fdaa662f8219-153db83dc4580e-84d1</seqId><companyCode>EPA</companyCode><priceType>Fuel Price</priceType><priceRequestSource></priceRequestSource><currencyCode>USD</currencyCode><deliveryType>Ex-Rack</deliveryType><customerId>00111495</customerId><shipToId></shipToId><supplyLocationId>9122</supplyLocationId><transporterId></transporterId><supplierId></supplierId><priceDate>2018-08-14</priceDate><onRun></onRun><orderId></orderId><applyOrderQuantity></applyOrderQuantity><totalOrderQuantity>30</totalOrderQuantity><priceConsolidationBasis>Summarised price</priceConsolidationBasis><versionNr>20180814T073909.359</versionNr></requestHeader><requestLines><line><seqId>3224c1fdaa662f8219-153db83dc4580e-84d1</seqId><orderId></orderId><lineItemId>-1241696765</lineItemId><itemId>70051</itemId><quantity>30</quantity><deliveryType>Ex-Rack</deliveryType><shipToId></shipToId><supplyLocationId>9122</supplyLocationId><onRun></onRun><priceDate>2018-08-14</priceDate></line></requestLines></pricingRequest></any0></Payload>';
        
		System.Assert(testBasket.Id != null);
		Attachment attachment = new Attachment();
        attachment.Body =  Blob.valueOf(encodedContentsString);
        attachment.Name = 'pricingRequest';
        attachment.ParentId = testBasket.Id; 
        insert attachment;
        
        test.startTest();
              
        Map<String, Object> inputMap = new Map<String, Object>();		
        inputMap.put('BasketId',testBasket.Id);
		inputMap.put('priceConsolidationBasis','34');
		
		System.Assert(testBasket.Id != null);
        TrafiguraGetPricesDetailed.doPriceCallDetailed(inputMap);
        TrafiguraGetPricesDetailed.saveAttachments(testBasket.Id,'req','res');
        test.stopTest();        
        
    }
}