@isTest
public class EP_Consignment_UT
{

static testMethod void findRecordType_test() {
    EP_Consignment localObj = new EP_Consignment();
    EP_VendorManagement vmType = new EP_VendorManagement();
    Test.startTest();
    Id result = localObj.findRecordType(vmType);
    Test.stopTest();
    Id recordTypeId = [Select id from RecordType where developerName = 'EP_Consignment_Orders'].Id;
    System.AssertEquals(recordTypeId,result);
    
}
static testMethod void hasCreditIssue_PositiveScenariotest() {
    EP_Consignment localObj = new EP_Consignment();
    csord__Order__c objOrder = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
    Test.startTest();
    Boolean result = localObj.hasCreditIssue(objOrder);
    Test.stopTest();
   System.AssertEquals(false,result);
}
static testMethod void hasCreditIssue_NegativeScenariotest() {
    EP_Consignment localObj = new EP_Consignment();
    csord__Order__c objOrder = EP_TestDataUtility.getConsignmentOrderNegativeScenario();
    Test.startTest();
    Boolean result = localObj.hasCreditIssue(objOrder);
    Test.stopTest();
    System.AssertEquals(false,result);
}
}