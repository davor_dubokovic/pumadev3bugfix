@isTest
public class EP_NAVOrderNewHelper_UT {
	@testSetup static void init() {
		List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
		List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
		List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType, 'EP_Order_State_Mapping');
		List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType, 'EP_Customer_Support_Settings');
		List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
		EP_INTEGRATION_CUSTOM_SETTING__c integration_Setting = new EP_INTEGRATION_CUSTOM_SETTING__c(Name = 'Request TimeOut', EP_Value__c = '120000');
		insert integration_Setting;
		List<EP_OrderStatusNAVMapping__c> OrderStatusNAVMappingSetting = Test.loadData(EP_OrderStatusNAVMapping__c.sObjectType, 'EP_OrderStatusNAVMapping_TestData');
		List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData');
	}
	static testMethod void createDataSets_test() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = createNAVOrderNewStubWithData();

		Test.startTest();
		localObj.createDataSets(orderWrapper);
		Test.stopTest();

		System.assertEquals(true, localObj.accountCompositKeySet.size() > 0);
		System.assertEquals(true, localObj.orderNumberSet.size() > 0);

	}
	static testMethod void setOrderNumber_test() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = createNAVOrderNewStubWithData();

		EP_OrderMapper ordMapper = new EP_OrderMapper();
		csord__Order__c OrderObj = ordMapper.getCSRecordById(orderWrapper.sfOrder.Id);
		localObj.orderNumberWithOrderMap.put(OrderObj.OrderNumber__c, OrderObj);
		orderWrapper.Identifier.orderId = orderWrapper.sfOrder.OrderNumber__c;
		system.debug('**orderNumberWithOrderMap**' + localObj.orderNumberWithOrderMap);
		Test.startTest();
		localObj.setOrderNumber(orderWrapper);
		Test.stopTest();

		System.assertEquals(true, localObj.orderNumberSet.size() > 0);
	}

	static testMethod void setOrderRefNumber_test() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = EP_TestDataUtility.createNAVOrderWrapper();
		orderWrapper.orderRefNr = '1234';

		Test.startTest();
		localObj.setOrderRefNumber(orderWrapper);
		Test.stopTest();

		System.assertEquals(true, localObj.orderNumberSet.size() > 0);
	}
	static testMethod void setSellToCompositKey_test() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = createNAVOrderNewStubWithData();

		Test.startTest();
		localObj.setSellToCompositKey(orderWrapper);
		Test.stopTest();

		System.assertEquals(true, localObj.accountCompositKeySet.size() > 0);
	}
	static testMethod void setShipToCompositKey_test() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = createNAVOrderNewStubWithData();

		Test.startTest();
		localObj.setShipToCompositKey(orderWrapper);
		Test.stopTest();

		System.assertEquals(true, localObj.accountCompositKeySet.size() > 0);
	}
	static testMethod void createDataMaps_test() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = EP_TestDataUtility.createNAVOrderWrapper();

		Test.startTest();
		localObj.createDataMaps(orderWrapper);
		Test.stopTest();

		System.assert(localObj.compositKeyAccountMap != null);
	}
	static testMethod void setAccountIdWithAccount_test() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		Account acc = EP_TestDataUtility.getSellTo();
		Map<string, Account> compositKeyAccountMap = new Map<string, Account>();
		compositKeyAccountMap.put(acc.EP_Composite_Id__c, acc);

		Test.startTest();
		localObj.setAccountIdWithAccount(compositKeyAccountMap);
		Test.stopTest();

		System.assert(localObj.accIdWithAccountMap != null);
	}
	
	@isTest
    public static void setOrderAttributes_test() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = createNAVOrderNewStubWithData();
		EP_OrderMapper ordMapper = new EP_OrderMapper();
		orderWrapper.sfOrder = ordMapper.getCSRecordById(orderWrapper.sfOrder.Id);
		EP_NAVOrderNewStub.OrderLine ordItem  = orderWrapper.OrderLines.OrderLine.get(0);
		orderWrapper.isCloned = false;
		
		List<csord__Order_Line_Item__c> orderItems = [SELECT Id, priceBookEntryId__c FROM csord__Order_Line_Item__c WHERE orderId__c = : orderWrapper.sfOrder.id];
		PricebookEntry priceBookEntry = [SELECT id FROM PricebookEntry WHERE Id = :orderItems.get(0).priceBookEntryId__c LIMIT 1];
		
		String priceBookEntryKey =  ordItem.itemId + orderWrapper.sfOrder.priceBook2Id__c + orderWrapper.sfOrder.currencyISOCode;
		localObj.pricebookEntryWithProdCode.put(priceBookEntryKey, priceBookEntry);
		orderWrapper.sfOrder = new csord__Order__c();
		orderWrapper.Identifier.orderId = null;
		orderWrapper.orderRefNr = 'orderRefNr';
		Test.startTest();
		localObj.setOrderAttributes(orderWrapper);
		Test.stopTest();

		System.assertEquals(true, localObj.orderNumberWithOrderMap != null);
		System.assertEquals(true, localObj.compositKeyAccountMap != null);
	}
	static testMethod void setOrderJSONAttributes_test() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = createNAVOrderNewStubWithData();
		EP_OrderMapper ordMapper = new EP_OrderMapper();
		csord__Order__c OrderObj = ordMapper.getCSRecordById(orderWrapper.sfOrder.Id);
		localObj.orderNumberWithOrderMap.put(OrderObj.OrderNumber__c, OrderObj);
		orderWrapper.Identifier.orderId = orderWrapper.sfOrder.OrderNumber__c;
		system.debug('**orderNumberWithOrderMap**' + localObj.orderNumberWithOrderMap);
		Test.startTest();
		localObj.createDataMaps(orderWrapper);
		localObj.setOrderJSONAttributes(orderWrapper);
		Test.stopTest();

		System.assert(orderWrapper.seqId == orderWrapper.sfOrder.EP_SeqId__c);
		System.assert(orderWrapper.sfOrder.EP_Order_Epoch__c == orderWrapper.orderEpoch);
		System.assert(orderWrapper.sfOrder.EP_Source_Interface__c == orderWrapper.orderOrigination);
		System.assert(orderWrapper.sfOrder.OrderReferenceNumber__c == orderWrapper.orderRefNr);
	}
	static testMethod void setOrderStatus_test() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = createNAVOrderNewStubWithData();
		orderWrapper.sfOrder.EP_Integration_Status__c = 'Sent';
		orderWrapper.sfOrder.Status__c = EP_Common_Constant.ORDER_STATUS_SUBMITTED;

		Test.startTest();
		update orderWrapper.sfOrder;
		orderWrapper.processingStatus = 'Cancelled';
		localObj.orderStatusMap.put(orderWrapper.processingStatus, 'Cancelled');
		localObj.setOrderStatus(orderWrapper);
		Test.stopTest();

		System.assertNotEquals(orderWrapper.processingStatus, orderWrapper.sfOrder.Status__c);
	}
	static testMethod void setShipTo_test() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = createNAVOrderNewStubWithData();
		EP_OrderMapper ordMapper = new EP_OrderMapper();
		orderWrapper.sfOrder = ordMapper.getCSRecordById(orderWrapper.sfOrder.Id);
		EP_AccountDomainObject accDomain = new EP_AccountDomainObject(orderWrapper.sfOrder.EP_ShipTo__c);
		EP_AccountDomainObject accDomainSellTo = new EP_AccountDomainObject(orderWrapper.sfOrder.AccountId__c);

		localObj.compositKeyAccountMap.put(orderWrapper.shipToCompositKey, accDomain.getAccount());
		localObj.compositKeyAccountMap.put(orderWrapper.sellToCompositKey, accDomainSellTo.getAccount());
		localObj.localOrderWrapper = orderWrapper;
		orderWrapper.sfOrder.EP_ShipTo__c = null;
		Test.startTest();
		localObj.setShipTo(orderWrapper);
		Test.stopTest();

		System.AssertEquals(orderWrapper.sfOrder.EP_ShipTo__c, accDomain.getAccount().Id);
	}
	
	@isTest
    public static void setSellTo_test() {
		Account accountObj = [Select EP_Composite_Id__c from Account where Id = :EP_TestDataUtility.getSellToPositiveScenario().Id];
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = EP_TestDataUtility.createNAVOrderWrapper();
		orderWrapper.sforder = new csord__Order__c();
		orderWrapper.sellToCompositKey = accountObj.EP_Composite_Id__c;
		localObj.compositKeyAccountMap.put(orderWrapper.sellToCompositKey, accountObj);
		orderWrapper.sfOrder.AccountId__c = null;
		Test.startTest();
		localObj.setSellTo(orderWrapper);
		Test.stopTest();

		System.assert(orderWrapper.sfOrder.AccountId__c == null);
	}
	static testMethod void setRecordType_test() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = EP_TestDataUtility.createNAVOrderWrapper();
		orderWrapper.sfOrder = EP_TestDataUtility.getSalesOrder();
		Test.startTest();
		localObj.setRecordType(orderWrapper);
		Test.stopTest();
		System.AssertEquals(true, orderWrapper.sfOrder.RecordTypeId != null);
	}
	static testMethod void getOrderRecordType_test() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = EP_TestDataUtility.createNAVOrderWrapper();
		orderWrapper.SFOrder = new csord__Order__c();
		localObj.setShipTo(orderWrapper);

		Test.startTest();
		Id recordTypeId = localObj.getOrderRecordType(orderWrapper.sfOrder);
		Test.stopTest();

		System.assertEquals(true, recordTypeId != null); //getConsum
	}
	
	@isTest
	public static void getOrderRecordType_VMIShipToTest() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = createNAVOrderNewStubWithData();
		EP_OrderMapper ordMapper = new EP_OrderMapper();
		// Account shipTobj = EP_TestDataUtility.getShipTo();
		
		Account shipTobj = new Account();
        shipTobj.EP_VendorType__c = '3rd Party Stock Supplier';
        shipTobj.EP_Vendor_Type__c = '3rd Party Stock Supplier';
        shipTobj.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        shipTobj.Name='testEPAccount';
        shipTobj.EP_Status__c = '06-Blocked';
        insert shipTobj;

		orderWrapper.sfOrder = ordMapper.getCSRecordById(orderWrapper.sfOrder.Id);
		orderWrapper.sfOrder.EP_ShipTo__c =  shipTobj.Id;
		EP_AccountDomainObject accDomain = new EP_AccountDomainObject(orderWrapper.sfOrder.EP_ShipTo__c);
		EP_AccountDomainObject accDomainSellTo = new EP_AccountDomainObject(orderWrapper.sfOrder.AccountId__c);

		localObj.accIdWithAccountMap.put(orderWrapper.sfOrder.EP_ShipTo__c, accDomain.getAccount());
		localObj.accIdWithAccountMap.put(orderWrapper.sfOrder.AccountId__c, accDomainSellTo.getAccount());
		orderWrapper.sfOrder.EP_Delivery_Type__c = 'EX_RACK';

		Test.startTest();
		Id recordTypeId = localObj.getOrderRecordType(orderWrapper.sfOrder);
		Test.stopTest();

		System.assertEquals(true, recordTypeId != null); //getConsum
	}

    @isTest
    public static void setPriceBook2Id_test() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = createNAVOrderNewStubWithData();

		EP_OrderMapper ordMapper = new EP_OrderMapper();
		orderWrapper.sfOrder = ordMapper.getCSRecordById(orderWrapper.sfOrder.Id);
		EP_AccountDomainObject accDomain = new EP_AccountDomainObject(orderWrapper.sfOrder.EP_ShipTo__c);
		EP_AccountDomainObject accDomainSellTo = new EP_AccountDomainObject(orderWrapper.sfOrder.AccountId__c);

		localObj.accIdWithAccountMap.put(orderWrapper.sfOrder.EP_ShipTo__c, accDomain.getAccount());
		localObj.accIdWithAccountMap.put(orderWrapper.sfOrder.AccountId__c, accDomainSellTo.getAccount());

		Test.startTest();
		localObj.localOrderWrapper = orderWrapper;
		localObj.setPriceBook2Id(orderWrapper);
		Test.stopTest();

		System.assertEquals(true, orderWrapper.sfOrder.PriceBook2Id__c == null);
	}
	static testMethod void setStockHoldingLocation_test() {
		csord__Order__c orderObj = [Select EP_ShipTo__c, EP_Supply_Location_Pricing__r.EP_Nav_Stock_Location_Id__c from csord__Order__c where id = :EP_TestDataUtility.getSalesOrderPositiveScenario().Id];
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = EP_TestDataUtility.createNAVOrderWrapper();
		orderWrapper.sfOrder = new csord__Order__c();
		orderWrapper.sfOrder.EP_ShipTo__c = orderObj.EP_ShipTo__c;
		orderWrapper.pricingStockHldngLocId = orderObj.EP_Supply_Location_Pricing__r.EP_Nav_Stock_Location_Id__c;

		Test.startTest();
		localObj.setStockHoldingLocation(orderWrapper);
		Test.stopTest();

		System.assert(orderWrapper.sfOrder.Stock_Holding_Location__c != null);
	}
	static testMethod void setPaymentMethod_test() {
		csord__Order__c orderObj = EP_TestDataUtility.getSalesOrderPositiveScenario();
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = EP_TestDataUtility.createNAVOrderWrapper();
		orderWrapper.sfOrder = new csord__Order__c();
		orderWrapper.paymentMethod = orderObj.EP_Payment_Method_Code__c;

		Test.startTest();
		localObj.setPaymentMethod(orderWrapper);
		Test.stopTest();

		System.assert(orderWrapper.sfOrder.EP_Payment_Method__c != null);
	}
	static testMethod void setPaymentMethod_NegativeTest() {
		csord__Order__c orderObj = EP_TestDataUtility.getSalesOrderPositiveScenario();
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = EP_TestDataUtility.createNAVOrderWrapper();
		orderWrapper.sfOrder = new csord__Order__c();
		orderWrapper.paymentMethod = null;

		Test.startTest();
		localObj.setPaymentMethod(orderWrapper);
		Test.stopTest();

		System.assertEquals(null, orderWrapper.sfOrder.EP_Payment_Method__c);
	}
	static testMethod void setPaymentTerm_test() {
		csord__Order__c orderObj = EP_TestDataUtility.getSalesOrderPositiveScenario();
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = EP_TestDataUtility.createNAVOrderWrapper();
		orderWrapper.sfOrder = new csord__Order__c();
		orderWrapper.paymentTerm = orderObj.EP_Payment_Term_Value_Code__c;

		Test.startTest();
		localObj.setPaymentTerm(orderWrapper);
		Test.stopTest();

		System.assertEquals(true, orderWrapper.sfOrder.EP_Payment_Term_Value__c != null);
	}
	static testMethod void setPaymentTerm_NegativeTest() {
		csord__Order__c orderObj = EP_TestDataUtility.getSalesOrderPositiveScenario();
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = EP_TestDataUtility.createNAVOrderWrapper();
		orderWrapper.sfOrder = new csord__Order__c();
		orderWrapper.paymentTerm = null;

		Test.startTest();
		localObj.setPaymentTerm(orderWrapper);
		Test.stopTest();

		System.assertEquals(null, orderWrapper.sfOrder.EP_Payment_Term_Value__c);
	}
	static testMethod void cloneOrderFromOrderRefNumber_test() {
		csord__Order__c orderObj = EP_TestDataUtility.getSalesOrderPositiveScenario();
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = EP_TestDataUtility.createNAVOrderWrapper();
		orderWrapper.orderRefNr = orderObj.OrderNumber__c;

		localObj.orderNumberWithOrderMap.put(orderWrapper.orderRefNr, orderObj);

		Test.startTest();
		localObj.cloneOrderFromOrderRefNumber(orderWrapper);
		Test.stopTest();

		System.assertEquals(true, orderWrapper.isCloned == true);
	}
	static testMethod void setPriceBookEntry_test() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = createNAVOrderNewStubWithData();
		EP_OrderMapper ordMapper = new EP_OrderMapper();
		orderWrapper.sfOrder = ordMapper.getCSRecordById(orderWrapper.sfOrder.Id);
		EP_AccountDomainObject accDomain = new EP_AccountDomainObject(orderWrapper.sfOrder.EP_ShipTo__c);
		EP_AccountDomainObject accDomainSellTo = new EP_AccountDomainObject(orderWrapper.sfOrder.AccountId__c);

		localObj.accIdWithAccountMap.put(orderWrapper.sfOrder.EP_ShipTo__c, accDomain.getAccount());
		localObj.accIdWithAccountMap.put(orderWrapper.sfOrder.AccountId__c, accDomainSellTo.getAccount());
		localObj.localOrderWrapper = orderWrapper;

		Test.startTest();
		localObj.setPriceBookEntry(orderWrapper);
		Test.stopTest();

		System.assertEquals(true, localObj.pricebookEntryWithProdCode != null);
		System.assertEquals(true, localObj.pricebookEntryWithName != null);
	}
	
	@isTest
    public static void setOrderItemAttributes_test() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = createNAVOrderNewStubWithData();
		EP_OrderMapper ordMapper = new EP_OrderMapper();
		orderWrapper.sfOrder = ordMapper.getCSRecordById(orderWrapper.sfOrder.Id);
		EP_NAVOrderNewStub.OrderLine ordItem  = orderWrapper.OrderLines.OrderLine.get(0);
		orderWrapper.isCloned = false;
		
		List<csord__Order_Line_Item__c> orderItems = [SELECT Id, priceBookEntryId__c FROM csord__Order_Line_Item__c WHERE orderId__c = : orderWrapper.sfOrder.id];
		PricebookEntry priceBookEntry = [SELECT id FROM PricebookEntry WHERE Id = :orderItems.get(0).priceBookEntryId__c LIMIT 1];
		
		String priceBookEntryKey =  ordItem.itemId + orderWrapper.sfOrder.priceBook2Id__c + orderWrapper.sfOrder.currencyISOCode;
		localObj.pricebookEntryWithProdCode.put(priceBookEntryKey, priceBookEntry);
		EP_AccountDomainObject accDomain = new EP_AccountDomainObject(orderWrapper.sfOrder.EP_ShipTo__c);
		EP_AccountDomainObject accDomainSellTo = new EP_AccountDomainObject(orderWrapper.sfOrder.AccountId__c);

		localObj.accIdWithAccountMap.put(orderWrapper.sfOrder.EP_ShipTo__c, accDomain.getAccount());
		localObj.accIdWithAccountMap.put(orderWrapper.sfOrder.AccountId__c, accDomainSellTo.getAccount());
		localObj.localOrderWrapper = orderWrapper;

		Test.startTest();
		localObj.setOrderItemAttributes(orderWrapper);
		Test.stopTest();

		System.assertEquals(orderWrapper.sfOrder.Id, orderWrapper.OrderLines.orderLine.get(0).orderLineItem.OrderId__c);
	}
	static testMethod void setOrderItemJSONAttributes_test() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.OrderLine ordLine = EP_TestDataUtility.createNAVOrderLinesItem();

		Test.startTest();
		localObj.setOrderItemJSONAttributes(ordLine);
		Test.stopTest();

		System.assertEquals(true, ordLine.orderLineItem.EP_SeqId__c == ordLine.seqId);
		System.assertEquals(true, ordLine.orderLineItem.Quantity__c  == Decimal.valueOf(EP_GeneralUtility.formatNumber(ordLine.qty)));
		System.assertEquals(true, ordLine.orderLineItem.EP_Quantity_UOM__c  == ordLine.uom);
		System.assertEquals(true, ordLine.orderLineItem.EP_Ambient_Loaded_Quantity__c  == Decimal.valueOf(ordLine.loadedAmbientQty));
	}
	static testMethod void getOrderNumber_test() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = EP_TestDataUtility.createNAVOrderWrapper();
		orderWrapper.isCloned = true;
		orderWrapper.SFOrder = new csord__Order__c();

		Test.startTest();
		String orderNumber = localObj.getOrderNumber(orderWrapper);
		Test.stopTest();

		System.assert(true, orderNumber != null);
	}
	static testMethod void setOrderItemId_test() {
		csord__Order__c orderObj = EP_TestDataUtility.getSalesOrderPositiveScenario();
		csord__Order_Line_Item__c orderItemObj = [Select EP_OrderItem_Number__c from csord__Order_Line_Item__c where ID = :orderObj.csord__Order_Line_Items__r[0].Id];
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.OrderLine ordLine = EP_TestDataUtility.createNAVOrderLinesItem();
		ordLine.identifier.lineId = String.valueOf(orderItemObj.EP_OrderItem_Number__c);
		localObj.orderItemMap.put(String.valueOf(orderItemObj.EP_OrderItem_Number__c), orderItemObj);

		Test.startTest();
		localObj.setOrderItemId(ordLine, false);
		Test.stopTest();

		System.assertEquals(true, ordLine.orderLineItem.Id != null);
	}
	
	@isTest
	public static void setPriceBookEntryIdByProdCode_test() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = createNAVOrderNewStubWithData();
		EP_NAVOrderNewStub.OrderLine ordItem = new EP_NAVOrderNewStub.OrderLine();
		ordItem = orderWrapper.OrderLines.OrderLine.get(0);

		List<csord__Order_Line_Item__c> orderItems = [SELECT Id, priceBookEntryId__c FROM csord__Order_Line_Item__c WHERE orderId__c = : orderWrapper.sfOrder.id];

		PricebookEntry priceBookEntry = [SELECT id FROM PricebookEntry WHERE Id = :orderItems.get(0).priceBookEntryId__c LIMIT 1];
		
		String priceBookEntryKey =  ordItem.itemId + orderWrapper.sfOrder.priceBook2Id__c + orderWrapper.sfOrder.currencyISOCode;
		EP_OrderMapper ordMapper = new EP_OrderMapper();
		orderWrapper.sfOrder = ordMapper.getCSRecordById(orderWrapper.sfOrder.Id);

		Test.startTest();
		localObj.localOrderWrapper = orderWrapper;
		localObj.pricebookEntryWithProdCode.put(priceBookEntryKey, priceBookEntry);
		localObj.setPriceBookEntryIdByProdCode(ordItem);
		Test.stopTest();

		System.assertEquals(true, ordItem.orderLineItem.pricebookEntryId__c != null);
	}
	
	@isTest
	public static void setPriceBookEntryIdByProdName_test() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = createNAVOrderNewStubWithData();
		EP_NAVOrderNewStub.InvoiceComponent ordItem = new EP_NAVOrderNewStub.InvoiceComponent();
		for (EP_NAVOrderNewStub.OrderLine ordLine : OrderWrapper.OrderLines.OrderLine) {
			for (EP_NAVOrderNewStub.InvoiceComponent invComp : ordLine.lineComponents.invoiceComponents.invoiceComponent) {
				ordItem = invComp;
			}
		}

		List<csord__Order_Line_Item__c> orderItems = [SELECT Id, priceBookEntryId__c FROM csord__Order_Line_Item__c WHERE orderId__c = : orderWrapper.sfOrder.id];

		PricebookEntry priceBookEntry = [SELECT id FROM PricebookEntry WHERE Id = :orderItems.get(0).priceBookEntryId__c LIMIT 1];

		String priceBookEntryKey =  ordItem.type + orderWrapper.sfOrder.priceBook2Id__c + orderWrapper.sfOrder.currencyISOCode;
		EP_OrderMapper ordMapper = new EP_OrderMapper();
		orderWrapper.sfOrder = ordMapper.getCSRecordById(orderWrapper.sfOrder.Id);

		Test.startTest();
		localObj.localOrderWrapper = orderWrapper;
		localObj.pricebookEntryWithName.put(priceBookEntryKey, priceBookEntry);
		localObj.setPriceBookEntryIdByProdName(ordItem);
		Test.stopTest();

		System.assertEquals(true, ordItem.invoiceItem.pricebookEntryId__c != null);
	}
	static testMethod void setAccountComponentXML_test() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.OrderLine ordLine = EP_TestDataUtility.createNAVOrderLinesItem();

		Test.startTest();
		localObj.setAccountComponentXML(ordLine);
		Test.stopTest();

		System.assertEquals(true, ordLine.orderLineItem.EP_Accounting_Details__c != null);
	}
	
	@isTest
	public static void getPriceBookEntryId_test() {
	    
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = createNAVOrderNewStubWithData();
		
		List<csord__Order_Line_Item__c> orderItems = [SELECT Id, priceBookEntryId__c FROM csord__Order_Line_Item__c WHERE orderId__c = : orderWrapper.sfOrder.id];

		PricebookEntry priceBookEntry = [SELECT id FROM PricebookEntry WHERE Id = :orderItems.get(0).priceBookEntryId__c LIMIT 1];
		
		String priceBookEntryKey =  String.valueOf(priceBookEntry.id) + orderWrapper.sfOrder.priceBook2Id__c + orderWrapper.sfOrder.currencyISOCode;
		localObj.localOrderWrapper = orderWrapper;
		map<string, PricebookEntry > priceBookEntryMap = new  map<string, PricebookEntry >();
		priceBookEntryMap.put(priceBookEntryKey, priceBookEntry);

		Test.startTest();
		
		Id priceBookEntryId = localObj.getPriceBookEntryId(priceBookEntryMap, String.valueOf(priceBookEntry.id));
		
		Test.stopTest();

		System.assertNotEquals(null, priceBookEntryId);
	}
	
	static testMethod void getAccountComponentXML_test() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		List<EP_NAVOrderNewStub.AcctComponent> acctComponentList = new List<EP_NAVOrderNewStub.AcctComponent>();
		acctComponentList.add(EP_TestDataUtility.createAccountingComponent());

		Test.startTest();
		String result = localObj.getAccountComponentXML(acctComponentList);
		Test.stopTest();

		System.assertEquals(true, result != null);
	}
	
	@isTest
    public static void setInvoiceItemsAttributes_test() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = createNAVOrderNewStubWithData();
		EP_NAVOrderNewStub.InvoiceComponent ordItem = new EP_NAVOrderNewStub.InvoiceComponent();
		for (EP_NAVOrderNewStub.OrderLine ordLine : OrderWrapper.OrderLines.OrderLine) {
			for (EP_NAVOrderNewStub.InvoiceComponent invComp : ordLine.lineComponents.invoiceComponents.invoiceComponent) {
				ordItem = invComp;
			}
		}

		List<csord__Order_Line_Item__c> orderItems = [SELECT Id, priceBookEntryId__c FROM csord__Order_Line_Item__c WHERE orderId__c = : orderWrapper.sfOrder.id];
		PricebookEntry priceBookEntry = [SELECT id FROM PricebookEntry WHERE Id = :orderItems.get(0).priceBookEntryId__c LIMIT 1];
		
		String priceBookEntryKey =  ordItem.type + orderWrapper.sfOrder.priceBook2Id__c + orderWrapper.sfOrder.currencyISOCode;
		localObj.pricebookEntryWithName.put(priceBookEntryKey, priceBookEntry);
		EP_OrderMapper ordMapper = new EP_OrderMapper();
		orderWrapper.sfOrder = ordMapper.getCSRecordById(orderWrapper.sfOrder.Id);
		localObj.localOrderWrapper = orderWrapper;

		Test.startTest();
		localObj.createDataMaps(orderWrapper);
		localObj.setInvoiceItemsAttributes(orderWrapper);
		Test.stopTest();

		System.assertEquals(orderItems.get(0).id, ordItem.invoiceItem.EP_Parent_Order_Line_Item__c); // Need help
	}
	static testMethod void setInvoiceItemsAttributes_NegativeTest() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = createNAVOrderNewStubWithData();
		EP_NAVOrderNewStub.InvoiceComponent ordItem = new EP_NAVOrderNewStub.InvoiceComponent();
		for (EP_NAVOrderNewStub.OrderLine ordLine : OrderWrapper.OrderLines.OrderLine) {
			for (EP_NAVOrderNewStub.InvoiceComponent invComp : ordLine.lineComponents.invoiceComponents.invoiceComponent) {
				ordItem = invComp;
			}
		}
		Test.startTest();
		orderWrapper.sfOrder.id = null;
		localObj.setInvoiceItemsAttributes(orderWrapper);
		Test.stopTest();
		System.assertEquals(null, ordItem.invoiceItem.Id); // Need help
	}

    @isTest
	public static void setInvoiceItemJSONAttributes_test() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = createNAVOrderNewStubWithData();
		EP_NAVOrderNewStub.InvoiceComponent ordItem = new EP_NAVOrderNewStub.InvoiceComponent();
		for (EP_NAVOrderNewStub.OrderLine ordLine : OrderWrapper.OrderLines.OrderLine) {
			for (EP_NAVOrderNewStub.InvoiceComponent invComp : ordLine.lineComponents.invoiceComponents.invoiceComponent) {
				ordItem = invComp;
			}
		}
		
		List<csord__Order_Line_Item__c> orderItems = [SELECT Id, priceBookEntryId__c FROM csord__Order_Line_Item__c WHERE orderId__c = : orderWrapper.sfOrder.id];
		PricebookEntry priceBookEntry = [SELECT id FROM PricebookEntry WHERE Id = :orderItems.get(0).priceBookEntryId__c LIMIT 1];
		
		String priceBookEntryKey =  ordItem.type + orderWrapper.sfOrder.priceBook2Id__c + orderWrapper.sfOrder.currencyISOCode;
		localObj.pricebookEntryWithName.put(priceBookEntryKey, priceBookEntry);
		EP_OrderMapper ordMapper = new EP_OrderMapper();
		orderWrapper.sfOrder = ordMapper.getCSRecordById(orderWrapper.sfOrder.Id);
		localObj.localOrderWrapper = orderWrapper;

		Test.startTest();
		localObj.createDataMaps(orderWrapper);
		localObj.setInvoiceItemJSONAttributes(ordItem);
		Test.stopTest();

		System.assertEquals(true, ordItem.invoiceItem.EP_Invoice_Name__c == ordItem.Name);
		System.assertEquals(true, ordItem.invoiceItem.EP_Pricing_Total_Amount__c == Decimal.valueOf(EP_GeneralUtility.formatNumber(ordItem.amount)));
	}

	@isTest
	public static void getInvoiceItemId_test() { // Check this method
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = createNAVOrderNewStubWithData();
		csord__Order__c orderObj = orderWrapper.sforder;
		PriceBookEntry priceBookEntryObj = [SELECT Id , ProductCode, Name FROM PriceBookEntry WHERE PriceBook2Id = : orderObj.csord__Account__r.EP_PriceBook__c LIMIT 1];
		String priceBookEntryName = priceBookEntryObj.name ;

		list<csord__Order_Line_Item__c> orderItems = [SELECT Id,
		                                EP_OrderItem_Number__c, Quantity__c, EP_Pricing_Response_Unit_Price__c,
		                                orderId__c, UnitPrice__c, EP_WinDMS_Line_ItemId__c, csord__Order__r.Status__c, csord__Order__r.EP_Pricing_Status__c, priceBookEntryId__c
		                                FROM csord__Order_Line_Item__c WHERE orderId__c = : orderObj.Id];

		csord__Order_Line_Item__c invoiceItem = new csord__Order_Line_Item__c();
		invoiceItem.EP_Invoice_Name__c = 'INV_Name';
		invoiceItem.EP_Pricing_Total_Amount__c = 300.00;
		invoiceItem.EP_Tax_Percentage__c = 10.00;
		invoiceItem.EP_Tax_Amount__c =  10.00;
		invoiceItem.unitprice__c = 300;
		invoiceItem.quantity__c = orderItems.get(0).quantity__c;
		invoiceItem.orderId__c = orderItems.get(0).OrderId__c;
		invoiceItem.csord__Order__c = orderItems.get(0).OrderId__c;
		invoiceItem.pricebookEntryId__c = orderItems.get(0).pricebookEntryId__c;
		invoiceItem.csord__Identification__c = '4J2OJIO23IJRO32IJF2O3I';
		insert invoiceItem;

		string invoiceItemKey = invoiceItem.EP_Parent_Order_Line_Item__c + priceBookEntryName;
		invoiceItemKey += invoiceItem.EP_Invoice_Name__c;

		localObj.invoiceItemMap.put(invoiceItemKey, invoiceItem);

		Test.startTest();
		Id invoiceItemId = localObj.getInvoiceItemId(invoiceItem, priceBookEntryName);
		Test.stopTest();

		System.assertEquals(true, invoiceItemId != null);
	}
	static testMethod void isNonConsumptionOrder_PositiveScenariotest() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		csord__Order__c OrderObj = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();

		Test.startTest();
		Boolean result = localObj.isNonConsumptionOrder(OrderObj);
		Test.stopTest();

		System.AssertEquals(true, result);
	}
	static testMethod void isNonConsumptionOrder_NegativeScenariotest() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		csord__Order__c OrderObj = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
		Test.startTest();
		Boolean result = localObj.isNonConsumptionOrder(OrderObj);
		Test.stopTest();

		System.AssertEquals(false, result);
	}
	static testMethod void isVMIShipTo_PositiveScenariotest() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		Account accountObj = EP_TestDataUtility.getVMIShipTo();

		Test.startTest();
		Boolean result = localObj.isVMIShipTo(accountObj);
		Test.stopTest();

		System.AssertEquals(true, result);
	}
	static testMethod void isVMIShipTo_NegativeScenariotest() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		Account accountObj = EP_TestDataUtility.getShipTo();

		Test.startTest();
		Boolean result = localObj.isVMIShipTo(accountObj);
		Test.stopTest();

		System.AssertEquals(false, result);
	}
	static testMethod void isNonVMIShipTo_PositiveScenariotest() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		Account accountObj = EP_TestDataUtility.getShipTo();

		Test.startTest();
		Boolean result = localObj.isNonVMIShipTo(accountObj);
		Test.stopTest();

		System.AssertEquals(true, result);
	}
	static testMethod void isNonVMIShipTo_NegativeScenariotest() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		Account accountObj = EP_TestDataUtility.getVMIShipTo();

		Test.startTest();
		Boolean result = localObj.isNonVMIShipTo(accountObj);
		Test.stopTest();

		System.AssertEquals(false, result);
	}
	
	@isTest
	public static void setExistingInvoiceItems_test() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		csord__Order__c orderObj = EP_TestDataUtility.getSalesOrderWithInvoice();
		//string orderNumber = orderObj.orderNumber;
		List<csord__Order_Line_Item__c> orderItems = [SELECT Id,
		                                                    EP_OrderItem_Number__c, Quantity__c, EP_Pricing_Response_Unit_Price__c,
		                                                    orderId__c, UnitPrice__c, EP_WinDMS_Line_ItemId__c, csord__Order__r.Status__c, 
		                                                    csord__Order__r.EP_Pricing_Status__c, priceBookEntryId__c
                                                		FROM csord__Order_Line_Item__c 
                                                		WHERE orderId__c = : orderObj.Id];

		csord__Order_Line_Item__c invoiceItem = new csord__Order_Line_Item__c();
		invoiceItem.EP_Invoice_Name__c = 'INV_Name';
		invoiceItem.EP_Pricing_Total_Amount__c = 300.00;
		invoiceItem.EP_Tax_Percentage__c = 10.00;
		invoiceItem.EP_Tax_Amount__c =  10.00;
		invoiceItem.unitprice__c = 300;
		invoiceItem.quantity__c = orderItems.get(0).Quantity__c;
		invoiceItem.orderId__c = orderItems.get(0).orderId__c;
		invoiceItem.csord__Order__c = orderItems.get(0).OrderId__c;
		invoiceItem.pricebookEntryId__c = orderItems.get(0).pricebookEntryId__c;
		invoiceItem.csord__Identification__c = '4J2OJIO23IJRO32IJF2O3Q';
		insert invoiceItem;

		Test.startTest();
		localObj.setExistingInvoiceItems(orderObj.orderNumber__c);
		Test.stopTest();

		System.assertEquals(true, localObj.invoiceItemMap != null);
	}
	static testMethod void setExistingOrderItems_test() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = createNAVOrderNewStubWithData();

		Test.startTest();
		localObj.setExistingOrderItems(orderWrapper.sfOrder.orderNumber__c);
		Test.stopTest();

		System.assertEquals(true, localObj.orderItemMap != null);
	}
	static testMethod void sendOrderSyncRequest_PositiveTest() {
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = createNAVOrderNewStubWithData();
		orderWrapper.Identifier.orderId = '';
		localObj.localOrderWrapper = orderWrapper;
		Test.startTest();
		localObj.sendOrderSyncRequest(orderWrapper.sfOrder.Id);
		Test.stopTest();

		EP_IntegrationRecord__c integRec = [Select id, Name, EP_Object_ID__c, EP_Target__c from EP_IntegrationRecord__c limit 1];
		system.assertEquals('NAV', integRec.EP_Target__c);
	}
	static testMethod void sendOrderSyncRequest_NegativeTest() {
		EP_NAVOrderNewHelper localObj = new EP_NAVOrderNewHelper();
		EP_NAVOrderNewStub.orderWrapper orderWrapper = createNAVOrderNewStubWithData();
		orderWrapper.Identifier.orderId = '';

		Test.startTest();
		localObj.sendOrderSyncRequest(null);
		Test.stopTest();

		list<EP_IntegrationRecord__c> integRecList = [Select id, Name, EP_Object_ID__c, EP_Target__c from EP_IntegrationRecord__c limit 1];

		system.assertEquals(0, integRecList.size());
	}

	public static EP_NAVOrderNewStub.OrderWrapper createNAVOrderNewStubWithData() {
	
		EP_NAVOrderNewStub stub = EP_TestDataUtility.createNAVOrderNewStub();
		EP_NAVOrderNewStub.OrderWrapper OrderWrapper = stub.MSG.Payload.any0.order;
		OrderWrapper.sfOrder = EP_TestDataUtility.getSalesOrder();
		EP_OrderMapper ordMapper = new EP_OrderMapper();
		
		csord__Order__c OrderObj = ordMapper.getCSRecordById(orderWrapper.sfOrder.Id);
		orderWrapper.Identifier.orderId = orderWrapper.sfOrder.OrderNumber__c;
		
		EP_AccountDomainObject accDomain = new EP_AccountDomainObject(orderWrapper.sfOrder.EP_ShipTo__c);
		EP_AccountDomainObject accDomainSellTo = new EP_AccountDomainObject(orderWrapper.sfOrder.AccountId__c);
		orderWrapper.shipToId = accDomain.getAccount().AccountNumber;
		orderWrapper.sellToId = accDomainSellTo.getAccount().AccountNumber;
		orderWrapper.clientId = orderWrapper.clientId.toUpperCase();
		orderWrapper.sellToCompositKey = orderWrapper.clientId + '-' + orderWrapper.sellToId ;
		orderWrapper.shipToCompositKey = orderWrapper.sellToCompositKey + '-' + orderWrapper.shipToId;
		
		list<csord__Order_Line_Item__c> orderItems = [select Id,
		                                //    priceBookEntry.ProductCode,
		                                EP_OrderItem_Number__c, Quantity__c, EP_Pricing_Response_Unit_Price__c, orderId__c, UnitPrice__c, EP_WinDMS_Line_ItemId__c, csord__Order__r.Status__c, csord__Order__r.EP_Pricing_Status__c, priceBookEntryId__c
		                                // ,PricebookEntry.Product2.Name
		                                from csord__Order_Line_Item__c where orderId__c = : OrderWrapper.sfOrder.id];
		EP_Stock_Holding_Location__c shl = [Select id, EP_Stock_Location_Id__c from EP_Stock_Holding_Location__c where EP_Ship_To__c = : OrderWrapper.sfOrder.EP_ShipTo__c limit 1];

		Product2  productObj = new product2(Name = 'Diesel', CurrencyIsoCode = OrderWrapper.sfOrder.CurrencyIsoCode, isActive = TRUE, family = 'Hydrocarbon Liquid', Eligible_for_Rebate__c = true);
		Database.insert(productObj);
		PricebookEntry customPrice = new PricebookEntry(Pricebook2Id = OrderWrapper.sfOrder.Pricebook2Id__c, Product2Id = productObj.Id,
		        UnitPrice = 12000, IsActive = true, EP_Is_Sell_To_Assigned__c = true);
		insert customPrice;
		
		for (csord__Order_Line_Item__c oli :orderItems) {
			oli.priceBookEntryId__c = customPrice.id;
		}
		
		update orderItems;
		
		for (EP_NAVOrderNewStub.OrderLine ordLine : OrderWrapper.OrderLines.OrderLine) {
			ordLine.orderLineItem = orderItems.get(0);
			//ordLine.itemId = orderItems.get(0).priceBookEntry.ProductCode;
			ordLine.pricingStockHldngLocId = shl.EP_Stock_Location_Id__c;
			for (EP_NAVOrderNewStub.InvoiceComponent invComp : ordLine.lineComponents.invoiceComponents.invoiceComponent) {
				invComp.Name = 'invComp';
				invComp.type = productObj.Name;
			}
		}

		return OrderWrapper;
	}
}
