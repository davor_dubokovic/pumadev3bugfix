/**
 * @author <Manish GOswami>
 * @name <EP_ActionTriggerHandler>
 * @createDate <10/11/2018>
 * @description
 * @version <1.0>
 */
 @isTest
 public class EP_ActionTriggerHandler_UT{
       Static Id accId;
       map<Id,EP_Action__c> mNewActions = new map<Id,EP_Action__c>();
       map<Id,EP_Action__c> mOldActions = new map<Id,EP_Action__c>();
       //@testSetup 
        public static void init() {
        list<user> users = new list<user>();
        profile objProfile = [select id from profile where name= 'EP_BSM/CMA' limit 1];
        user objUser = EP_TestDataUtility.createUser(objProfile.id);
        users.add(objUser);
        user objUser1 =EP_TestDataUtility.createUser(objProfile.id);
        users.add(objUser1);
        
        database.insert(users);
        
        Account accShipTo = EP_TestDataUtility.getShipToPositiveScenario();
        accShipTo.EP_Synced_NAV__c = true;
        accShipTo.EP_Synced_PE__c = true;
        accShipTo.EP_Synced_WINDMS__c = true;
        accShipTo.EP_Status__c = '02-Basic Data Setup';
        update accShipTo; 

        List<account> sellToAcc = [select id,EP_Indcative_Total_Purchase_Value_Yr_USD__c, EP_Synced_NAV__c,EP_Synced_PE__c,EP_Synced_WINDMS__c,EP_Status__c,EP_Puma_Company__c From Account Where Id =: accShipTo.ParentId];
        system.debug('accShipTo%%' +accShipTo);
        System.assert(sellToAcc.size() == 1);
        accId = sellToAcc[0].id;
        if(!sellToAcc.isEmpty()){  
          sellToAcc[0].EP_Indcative_Total_Purchase_Value_Yr_USD__c = '1000';      
          sellToAcc[0].EP_Synced_NAV__c = true;
          sellToAcc[0].EP_Synced_PE__c = true;
          sellToAcc[0].EP_Synced_WINDMS__c = true;
          sellToAcc[0].EP_Status__c = '02-Basic Data Setup';          
        }
    Test.startTest(); 
        update sellToAcc;
          
          //EP_TestDataUtility.createUserCompany(objUser.id,sellToAcc[0].EP_Puma_Company__c);
         // EP_TestDataUtility.createUserCompany(objUser1.id,sellToAcc[0].EP_Puma_Company__c);
          Test.stopTest();
          //List<EP_User_Company__c> userCompanyList = [Select Id FROM EP_User_Company__c];
          //System.assertEquals(2, userCompanyList.size()); 
    }
   
     private static EP_Action__c createAction() {
        Account account = [select Id from account limit 1];
        user objUser = [select Id from user where Profile.Name = 'EP_BSM/CMA' limit 1];
        EP_Action__c action = new EP_Action__c();
        action.RecordTypeId = Schema.SObjectType.EP_Action__c.getRecordTypeInfosByName().get('BSM/GM Review').getRecordTypeId();
        action.EP_Account__c = account.id;
        action.OwnerId = objUser.Id;
        database.insert(action);
       
        map<Id,EP_Action__c> mNewActions = new map<Id,EP_Action__c>();
        map<Id,EP_Action__c> mOldActions = new map<Id,EP_Action__c>();
        mNewActions.put(action.id,action);
        mOldActions.put(action.id,action);
        system.debug('mNewActions'+mNewActions);
        system.debug('mNewActions'+mNewActions);
        test.startTest();  
            EP_ActionTriggerHandler.doBeforeUpdate(mNewActions,mOldActions);
            EP_ActionTriggerHandler.doAfterApdate(mNewActions,mOldActions);
            test.stopTest(); 
        
        return action;
    }
    
     private static testMethod void doBeforeInserttest(){
        //EP_Action__c action  = createAction();
         list<EP_Action__c > action = [select Id from EP_Action__c limit 1];
            test.startTest();
            EP_ActionTriggerHandler returnaction = new EP_ActionTriggerHandler();   
            //system.assertEquals(true, steps.size() > 0);     
            EP_ActionTriggerHandler.doBeforeInsert(action);
            test.stopTest();     
     
     }
     private static testMethod void doAfterInsert(){
        //EP_Action__c action  = createAction();
         list<EP_Action__c > action = [select Id from EP_Action__c limit 1];
            test.startTest();
            EP_ActionTriggerHandler returnaction = new EP_ActionTriggerHandler();   
            //system.assertEquals(true, steps.size() > 0);     
            EP_ActionTriggerHandler.doAfterInsert(action);
            test.stopTest();     
     
     }
     private static testMethod void doAfterInsert1(){
        map<Id,EP_Action__c> mNewActions = new map<Id,EP_Action__c>();
        map<Id,EP_Action__c> mOldActions = new map<Id,EP_Action__c>();
        
        for(EP_Action__c actn : [select id,OwnerId,RecordTypeId,EP_Status__c,EP_Action_Name__c,EP_Account__c From EP_Action__c]){
          mNewActions.put(actn.id,actn);
          test.startTest();
          EP_ActionTriggerHandler.doBeforeUpdate(mNewActions,mNewActions);
          test.stopTest();          
        }
        
     }
 
 
 }