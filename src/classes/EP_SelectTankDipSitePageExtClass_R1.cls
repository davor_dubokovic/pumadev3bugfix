/* 
  @Author <Spiros Markantonatos>
   @name <EP_SelectTankDipSitePageExtensionClass>
   @CreateDate <06/11/2014>
   @Description <This class is used by the users to select the site that they want to submit a tank dip for>
   @Version <1.0>
 
*/
public with sharing class EP_SelectTankDipSitePageExtClass_R1 {
    private static final String EXP_STRING = 'expr0';

    // Properties
    public ApexPages.StandardSetController sscShipTos {get;set;}
    public List<shipToClass> shipTos {get;set;}
    public String strSelectedShipToID {get;set;} // Variable used to select the site that the user was viewing before, 
                                                 // when they click on the back button
    public Integer intNumberOfRecords {get;set;}
        
    private Integer intPageNumber {
        get {
            // Fix for Defect 29809 - Start
            if (intPageNumber == NULL)
            {
                intPageNumber = 1; // Default to 1 if there is no page number
                if (ApexPages.currentPage().getParameters().get(EP_Common_Constant.PAGE_NUMBER) != NULL)
                {
                    if (ApexPages.currentPage().getParameters().get(EP_Common_Constant.PAGE_NUMBER).isNumeric())
                        intPageNumber = Integer.valueOf(ApexPages.currentPage().getParameters().get(EP_Common_Constant.PAGE_NUMBER));
                }
            }
            return intPageNumber;
            // Fix for Defect 29809 - End
        }
        set;
    }
    
    // Const
    private static final Integer SHIP_TO_LIST_PAGE_SIZE = 20;
    private static final Integer SHIP_TO_PAGINATION_LIMIT = 10000;
    private static final string STR_QUERY1 = 'SELECT ID, Name FROM Account';
    private static final string STR_QUERY2 = ' WHERE RecordType.DeveloperName = \'';
    private static final string STR_QUERY3 = ' AND (EP_Status__c = \'Active\' OR EP_Status__c = \'';
    private static final string STR_QUERY4 = '\')';
    private static final string STR_QUERY5 = '\'';
    private static final string STR_QUERY6 = ' AND EP_Number_of_Non_Decommissioned_Tanks__c > 0';
    private static final string STR_QUERY7 = ' ORDER BY Name ASC';
    /*
         Constructor
    */
    public EP_SelectTankDipSitePageExtClass_R1() {
        
        // Initialise values
        strSelectedShipToID = NULL;
        
    }
    
    
    /*
         Fuctions
    */   
    public void populateSiteWrapper() {
        // Fix for Defect 29809 - Start
        // Initialise the ship-to wrapper
        Integer queryRows;
        shipTos = new List<shipToClass>();
        
        // Retrieve placeholder tank dips for the sites of the user and build map
        if (!sscShipTos.getRecords().isEmpty()) {
            Map<String, Integer> missingTankDipsMap = new Map<String, Integer>();
            Map<String, Integer> tankDipForTodayMap = new Map<String, Integer>();
            
            String strTankShipToID;
            String strShipToID;
            
            List<Account> lAccounts = new List<Account>();
            lAccounts.addAll((List<Account>)sscShipTos.getRecords());
            /*for(Account a : (List<Account>)sscShipTos.getRecords())
            {
                lAccounts.add(a);
            }*/
            queryRows = EP_Common_Util.getQueryLimit();
            // Retrieve the site tank IDs
            Map<String, EP_Tank__c> tankMap = new Map<String, EP_Tank__c>([SELECT ID, EP_Ship_To__c, EP_Ship_To__r.Name, EP_Tank_Status__c, 
                                                                            EP_Tank_Missing_Dip_For_Today__c
                                                                                FROM EP_Tank__c 
                                                                                    WHERE EP_Ship_To__c IN :lAccounts LIMIT :queryRows]);
            system.debug('tankMap.isEmpty()++++++'+tankMap);
            if (!tankMap.isEmpty()) {
                DateTime dtNow = EP_PortalLibClass_R1.returnLocalDateTime(System.Now());
                
                // Check if the tanks have tank dips for today
                // Replace OPERATIONAL to Not equals to DECOMISSIONED
                for (EP_Tank__c t : tankMap.values()) {
                    system.debug('EP_Tank_Status__c++++'+t.EP_Tank_Status__c+'+++++'+t.EP_Tank_Missing_Dip_For_Today__c);
                    if (t.EP_Tank_Status__c != EP_Common_Constant.TANK_DECOMISSIONED_STATUS && t.EP_Tank_Missing_Dip_For_Today__c) {
                             tankDipForTodayMap.put(t.EP_Ship_To__c, 1);
                    } // End tank missind dip for today check
                } // End for
                String recType = Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get(EP_Common_Constant.Placeholder).getRecordTypeId();
                system.debug('-----'+recType);
                queryRows = EP_Common_Util.getQueryLimit();
                // Count missing tank dips the last N days
                // A trigger will delete any placeholder tank dips if the tank is flagged as non-operational,
                // therefore this query should not be filtering out any tanks
                
                // Fix for Defect 44600 - Start
                for (AggregateResult td : [SELECT EP_Tank__c, COUNT(ID) FROM EP_Tank_Dip__c 
                                                        WHERE EP_Tank__c IN :tankMap.keySet() AND EP_Tank_Dip_Entered_In_Last_14_Days__c = TRUE 
                                                            AND RecordTypeId =: recType AND EP_Tank_Dip_Date_Time_Ship_To_Time_Zone__c < :dtNow.Date()
                                                            AND EP_Tank__r.EP_Ship_To__c IN :lAccounts
                                                            GROUP BY EP_Tank__c LIMIT :queryRows]) {
                    // Create the first entry in the map if there isn't any
                    strTankShipToID = tankMap.get(String.valueOf(td.get(EP_Common_Constant.TANK_OBJ))).EP_Ship_To__c;
                    
                    if (!missingTankDipsMap.containsKey(strTankShipToID)){
                        missingTankDipsMap.put(strTankShipToID, 0);
                    }
                    missingTankDipsMap.put(strTankShipToID, 
                                    missingTankDipsMap.get(strTankShipToID) + Integer.ValueOf(td.get(EXP_STRING)));
                     
                } // End for
                // Fix for Defect 44600 - End
                system.debug('missingTankDipsMap+++'+missingTankDipsMap);
            } // End tank check
            
            shipToClass shipTo;
            Boolean blnIsFirstLine = TRUE;
            Integer intMissingTankDips;
            try{
                for (Account s : lAccounts) {
                    shipTo = new shipToClass();
                    strShipToID = s.Id;
                    shipTo.siteID = s.Id;
                    shipTo.siteName = s.Name;
                    shipTo.siteSelected = ((strSelectedShipToID == NULL && blnIsFirstLine) || (strShipToID.Left(15) == strSelectedShipToID));
                    
                    shipTo.siteShowOKStatus = (!missingTankDipsMap.containsKey(s.Id) && !tankDipForTodayMap.containsKey(s.ID)); // If there are no placeholder tank dips for the last 7 days, then flag the site as OK
                    intMissingTankDips = 0;
                    
                    // Retrieve how many tank dips are missing (Today + Previous dates)
                    if (missingTankDipsMap.containsKey(s.Id)){
                        intMissingTankDips = missingTankDipsMap.get(s.Id);
                    }
                    system.debug('testting +++++++++'+missingTankDipsMap.containsKey(s.Id));
                    system.debug('testting +++++++++'+intMissingTankDips);
                    system.debug('testting +++++++++'+tankDipForTodayMap.containsKey(s.Id));
                    shipTo.siteShowCautionStatus = (missingTankDipsMap.containsKey(s.Id) && !(intMissingTankDips == 0 && tankDipForTodayMap.containsKey(s.Id))); 
                    shipTo.siteShowWarningStatus = (!shipTo.siteShowOKStatus && !shipTo.siteShowCautionStatus);
                    shipTo.StatusCount = missingTankDipsMap.get(s.Id);
                    
                    // Update the page number of the each site
                    shipTo.pageNumber = sscShipTos.getPageNumber();
                    
                    blnIsFirstLine = FALSE;
                    
                    if (strSelectedShipToID == NULL){
                        strSelectedShipToID = s.Id;
                    }
                    shipTos.add(shipTo);
                    
                } // End for
            } catch (exception exp){        
                string str= (exp.getMessage());         
            }
        } // End account check
        // Fix for Defect 29809 - End
    }
    
    /*
      Site Information method
    */
    public PageReference retrieveSiteInformation() {
        PageReference ref = NULL;
        String strURLShipToIDVar;
        
        // Initialise the ship-to wrapper
        shipTos = new List<shipToClass>();
        
        // Retrieve site ID from URL (if any)
        if (ApexPages.currentPage().getParameters().get(EP_Common_Constant.ID) != NULL) {
            strURLShipToIDVar = ApexPages.currentPage().getParameters().get(EP_Common_Constant.ID);
            
            // Confirm that the ID corresponds to a valid ship-to
            List<Account> selectedShipTos = [SELECT ID FROM Account WHERE ID = :strURLShipToIDVar 
                                                AND RecordType.DeveloperName = :EP_Common_Constant.VMI_SHIP_TO_RECORD_TYPE_DEVELOPER_NAME 
                                                LIMIT :EP_COMMON_CONSTANT.ONE];                
            if (!selectedShipTos.isEmpty()) {
                strSelectedShipToID = strURLShipToIDVar.Left(15);
            }
        }
        
        // Retrieve sites
        // Add 'Blocked' in Account Status 
        // Fix for Defect 29809 - Start
        String strSQL = STR_QUERY1 ;
        String whereClause = STR_QUERY2 ;
        String strEPStatus = STR_QUERY3 ;
        String strSla = STR_QUERY4 ;
        String strSlaWBrc = STR_QUERY5 ;
        String strEpNumbr = STR_QUERY6 ;
        String strOrdBy = STR_QUERY7 ;
        strSQL += whereClause + EP_Common_Constant.VMI_SHIP_TO_RECORD_TYPE_DEVELOPER_NAME + strSlaWBrc ;
        //strSQL += ' AND (EP_Status__c = \'' + EP_Common_Constant.STATUS_ACTIVE + '\' OR EP_Status__c = \'' +
        strSQL += strEPStatus + EP_Common_Constant.STATUS_BLOCKED + strSla;   //'\')';
        strSQL += strEpNumbr;
        strSQL += strOrdBy ;
        strSQL +=  EP_Common_Constant.SPACE_STRING + EP_Common_Constant.limitException + EP_Common_Constant.SPACE_STRING+ SHIP_TO_PAGINATION_LIMIT;   // ' LIMIT ' + SHIP_TO_PAGINATION_LIMIT;        
        sscShipTos = new ApexPages.StandardSetController(Database.getQueryLocator(strSQL));
        sscShipTos.setPageSize(SHIP_TO_LIST_PAGE_SIZE);
        intNumberOfRecords = sscShipTos.getResultSize();     
        sscShipTos.setPageNumber(intPageNumber); // Set the page size for the first time
        
        populateSiteWrapper();
        // Fix for Defect 29809 - End
        
        // If there is only 1 account, then go straight to the date picker
        try{
        if (intNumberOfRecords == 1) {
            ref = Page.EP_SelectTankDipDatePage_R1;
            ref.getParameters().put(EP_Common_Constant.ID, sscShipTos.getRecords()[0].Id);
            ref = new PageReference(EP_Common_Constant.TANKDIP_DATE_PAGE + sscShipTos.getRecords()[0].Id);
        }
        }
        catch (exception exp){
                string str= (exp.getMessage());  
          }
        return ref;
    }
    
    /*
      Cancel Method
    */
    public PageReference cancel() {
        PageReference ref ;
        try{
            // Fix for Defect 29809 - Start
            ref = new PageReference(EP_Common_Constant.HOME_PAGE_URL);
            
            // Fix for Defect 29809 - End
        }
        catch (exception exp){
                string str= (exp.getMessage());  
          }
          return ref;
    }
    
    /*
      Method to redirect to first page
    */
    public PageReference goToFirst() {
        try{
            // Fix for Defect 29809 - Start
            sscShipTos.first();
            populateSiteWrapper();
            // Fix for Defect 29809 - End
        }
        catch (exception exp){
                string str= (exp.getMessage());  
        }
        return NULL;
    }
    
    /*
      Method to redirect to last page
    */
    public PageReference goToLast() {
        try{
            // Fix for Defect 29809 - Start
            sscShipTos.last();
            populateSiteWrapper();
            // Fix for Defect 29809 - End
        }
        catch (exception exp){
                string str= (exp.getMessage());  
          }
        return NULL;
    }
    
    public Boolean hasNext {
        get {
            // Fix for Defect 29809 - Start
            return sscShipTos.getHasNext();
            // Fix for Defect 29809 - End
        }
        set;
    }

    public Boolean hasPrevious {
        get {
            // Fix for Defect 29809 - Start
            return sscShipTos.getHasPrevious();
            // Fix for Defect 29809 - End
        }
        set;
    }
    
    /*
      Method to redirect to previous page
    */
    public void previous() {
        try{
            // Fix for Defect 29809 - Start
            sscShipTos.previous();
            populateSiteWrapper();
            // Fix for Defect 29809 - End
        }
        catch (exception exp){
                string str= (exp.getMessage());  
          }
    }
    
    /*
      Method to redirect to next page
    */
    public void next() {
        // Fix for Defect 29809 - Start
        try{
        sscShipTos.next();
        populateSiteWrapper();
        }
        catch (exception exp){
                string str= (exp.getMessage());  
          }        // Fix for Defect 29809 - End
    } 
    
    /*
         Inner class
    */   
    public without sharing class shipToClass {
        public String siteID {get;set;}
        public String siteName {get;set;}
        public Boolean siteSelected {get;set;}
        public Boolean siteShowOKStatus {get;set;}
        public Boolean siteShowWarningStatus {get;set;}
        public Boolean siteShowCautionStatus {get;set;}
        public Integer statusCount {get;set;}   
        public Integer pageNumber {get;set;} 
    }
}