/* 
@Author      Accenture
@name        EP_CaseService
@CreateDate  07/05/2018
@Description Service layer class for Case Object
@Version     1.0
*/
public with sharing class EP_CaseService{
    EP_CaseDomainObject CaseDomainObject;
    list<case> lstNewCase;
    map<Id,case> CasetriggerOldMap;
    set<Id> accIds = new set<Id>();
    set<Id> BusinessHoursIds = new set<Id>();
    long intBusinessHours;
    decimal closeCaseExp;
    private static final string CASE_CLOSED = 'Closed';
   /**
    * @author       Accenture
    * @name         EP_CaseService
    * @date         EP_CaseService
    * @description  Constructor for setting EP_CaseDomainObject and initilizing Case object instance
    * @param        EP_CaseDomainObject
    * @return       NA
    */  
    public EP_CaseService(EP_CaseDomainObject CaseDomainObject){
        this.CaseDomainObject = CaseDomainObject;
        this.lstNewCase = CaseDomainObject.caseTriggerNew;
        this.CasetriggerOldMap = CaseDomainObject.CaseTriggerOldMap;
    }

     /**
    * @author       Accenture
    * @name         doBeforeUpdateHandle
    * @date         07/05/2018
    * @description  method to handle logic for case on before update event
    * @param        NA
    * @return       NA
    */
    public void doBeforeUpdateHandle(){
        EP_GeneralUtility.Log('Public','EP_CaseService','doBeforeUpdateHandle');
        //To Populate Close Date Expiration on case... 
        populateCloseCaseExpiration();     
    }
    /**
    * @author       Accenture
    * @name         getBusinessHours`
    * @date         08/05/2018
    * @description  This method returns Long BusinessHours
    * @param        map<Id,BusinessHours>
    * @return       long intBusinessHours
    */
    @TestVisible void populateCloseCaseExpiration(){
        map<Id,BusinessHours> mapIdToBusinessHours = getmapIdToBusinessHours(lstNewCase,CasetriggerOldMap);     
        Map<Id,Account> mapIdToAcc = getMapIdToAcc(lstNewCase,CasetriggerOldMap);       
        for(case Caseobj : lstNewCase){
            long intBusinessHours = getBusinessHours(mapIdToBusinessHours,Caseobj.BusinessHoursid);         
            Decimal closeCaseExp = getcloseCaseExp(mapIdToAcc,Caseobj.AccountId);           
            if(intBusinessHours != null && closeCaseExp != null){
                long intervalMillisecondsBus =  (intBusinessHours)*Math.roundToLong(closeCaseExp)*60000;               
                Caseobj.EP_Close_Case_Expiration__c = BusinessHours.add(Caseobj.BusinessHoursid,Caseobj.ClosedDate,intervalMillisecondsBus);                
            }
        }
    }
    /**
    * @author       Accenture
    * @name         getBusinessHours`
    * @date         08/05/2018
    * @description  This method returns Long BusinessHours
    * @param        map<Id,BusinessHours>
    * @return       long intBusinessHours
    */
    @TestVisible private long getBusinessHours(map<Id,BusinessHours> mapIdToBusinessHours,Id BusinessHourId){
        EP_CalculateSLATime CalculateSLATime = new EP_CalculateSLATime();       
        if(mapIdToBusinessHours.containskey(BusinessHourId) && mapIdToBusinessHours.get(BusinessHourId) != null){               
            return CalculateSLATime.CalculateTimeInaDay(mapIdToBusinessHours.get(BusinessHourId));
        }       
        return null;
    }
    
    /**
    * @author       Accenture
    * @name         getcloseCaseExp`
    * @date         08/05/2018
    * @description  This method returns Close Case Expiration field value
    * @param        map<Id,Account>
    * @return       decimal closeCaseExp
    */
    @TestVisible private decimal getcloseCaseExp(Map<Id,Account> mapIdToAcc,Id AccountId){
        if(mapIdToAcc.containskey(AccountId) && mapIdToAcc.get(AccountId) != null && mapIdToAcc.get(AccountId).EP_Puma_Company__r.EP_Close_Case_Expiration__c != null){             
            return mapIdToAcc.get(AccountId).EP_Puma_Company__r.EP_Close_Case_Expiration__c;
        }       
        return null;
    }
    
    /**
    * @author       Accenture
    * @name         getmapIdToBusinessHours`
    * @date         08/05/2018
    * @description  This method returns map Id to BusinessHours
    * @param        list<case> lstNewCase,map<Id,case> CasetriggerOldM
    * @return       map<Id,BusinessHours>
    */
    @TestVisible private map<Id,BusinessHours> getmapIdToBusinessHours(list<case> lstNewCase,map<Id,case> CasetriggerOldMap){
        set<Id> BusinessHoursIds = new set<Id>();
        for(case objCase : lstNewCase){ 
            if(objCase.status != CasetriggerOldMap.get(objCase.id).status && objCase.status.equalsIgnoreCase(CASE_CLOSED) ){
                BusinessHoursIds.add(objCase.BusinessHoursid);
            } 
        }       
        map<Id,BusinessHours> mapIdToBusinessHours = new map<Id,BusinessHours>(EP_BusinessHoursMapper.getRecordsByIds(BusinessHoursIds));
        return mapIdToBusinessHours;
    }
    
    /**
    * @author       Accenture
    * @name         getMapIdToAcc
    * @date         08/05/2018
    * @description  This method returns map Id to Account
    * @param        list<case> lstNewCase,map<Id,case> CasetriggerOldM
    * @return       Map<Id,Account>
    */
     @TestVisible private Map<Id,Account> getMapIdToAcc(list<case> lstNewCase,map<Id,case> CasetriggerOldMap){
        set<Id> accIds = new set<Id>();
        
        for(case objCase : lstNewCase){
            if(objCase.status != CasetriggerOldMap.get(objCase.id).status && objCase.status.equalsIgnoreCase(CASE_CLOSED) ){
                accIds.add(objCase.AccountId);
            }
        }
        
        EP_AccountMapper AccountMapper = new EP_AccountMapper();
        Map<Id,Account> mapIdToAcc = new map<Id,Account>(AccountMapper.getMapOfRecordsByIds(accIds));   
        
        return mapIdToAcc;
    }
}