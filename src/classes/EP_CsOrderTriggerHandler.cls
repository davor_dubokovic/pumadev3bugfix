public with sharing class EP_CsOrderTriggerHandler {

    public static void doAfterUpdate(Map<Id,csord__Order__c> orderMap){
        CSPOFA.Events.emit('update', orderMap.keySet());
    }
    
    /**
    * @Author       CloudSense
    * @Name         doBeforeInsertandUpdate
    * @Date         18/05/2018
    * @Description  This method set value in order Before Insert and Update
    * @Param        List<csord__Order__c> 
    * @return       NA
    */
    public static void doBeforeInsertandUpdate(List<csord__Order__c> orders) {
       
        set<String> users = new set<String>();
        Map<String,User> mapUserIdByUser = new Map<String,User>();
        
        for(csord__Order__c ord : orders) {  
            users.add(ord.OwnerId);
        }
          
        for (User u : [SELECT Id,Email,Phone From User where Id in :users]){
            mapUserIdByUser.put(u.Id,u);  
        }
        
        for (csord__Order__c ord : orders) {
            User userOwner = new User();
            if (ord.OwnerId != null) {
                userOwner = mapUserIdByUser.get(ord.OwnerId);
            }           
            if (userOwner.Email != null) {
                ord.EP_OwnerPhone__c = userOwner.Phone;
            }
            if (userOwner.Email != null) {
                ord.EP_Owner_Email__c = userOwner.Email;
            }            
        }      
    }

    /**
    * @Author       CloudSense
    * @Name         doBeforeUpdateLoadingCode
    * @Date         21/05/2018
    * @Description  This method Updates Loading Code is status changes from PLANNING
    * @Param        List<csord__Order__c> 
    * @return       NA
    */
    public static void doBeforeUpdateLoadingCode(Map<Id,csord__Order__c> newOrders, Map<Id,csord__Order__c> oldOrders) {   
        
        for(csord__Order__c ord : newOrders.values()) {

            if( oldOrders.get(ord.Id).csord__Status2__c == 'PLANNED' && (ord.csord__Status2__c == 'Order Submitted' || ord.csord__Status2__c == 'Draft') &&
                (ord.EP_Delivery_Type__c == 'Ex-Rack' || ord.EP_Delivery_Type__c == 'Ex-Rack with Scheduling')){
                ord.EP_Order_Load_Code__c = null;
            }
        }       
    }
    
     /**
    * @Author       CloudSense
    * @Name         orchestratorUpdate
    * @Date         16/05/2018
    * @Description  This method update Orchestration Process status
    * @Param        Map<Id, csord__Order__c> 
    * @return       NA
    */
    public static void orchestratorUpdate(Map<Id,csord__Order__c> orderMap) {

      EP_GeneralUtility.Log('Public','doBeforeUpdate','doBeforeUpdate');
      set<String> orders = new set<String>();
      List<EP_Action__c> actions = new List<EP_Action__c>();
      actions = [SELECT Id,
                    EP_Status__c,EP_CSOrder__c   
                From 
                    EP_Action__c 
                WHERE EP_CSOrder__c in :orderMap.keySet() 
                      and EP_Status__c = :EP_OrderConstant.ACTIONSTATUSREJECTED
                ];
      System.Debug('actions -->'+actions);
      
      try{         
          if (actions != null && !actions.isEmpty()) {
            for (EP_Action__c act : actions) {              
                orders.add(act.EP_CSOrder__c);
            }
            List<CSPOFA__Orchestration_Process__c> OrchProcess = new List<CSPOFA__Orchestration_Process__c>();
            List<CSPOFA__Orchestration_Step__c> orchSteps = new List<CSPOFA__Orchestration_Step__c>();
            set<String> orchProcessId = new set<String>();
            for (CSPOFA__Orchestration_Process__c oProc : [Select Id,CSPOFA__Status__c From CSPOFA__Orchestration_Process__c where Order__c in :orders]){
                oProc.CSPOFA__Status__c = EP_OrderConstant.ORCHESTRATIONPROCESSCOMPLETESTATUS;
                orchProcessId.add(oProc.Id);
                OrchProcess.add(oProc);
            }   
            System.Debug('OrchProcess -->'+OrchProcess);
            if (OrchProcess != null && !OrchProcess.isEmpty()) {
                update OrchProcess;
            } 
            for (CSPOFA__Orchestration_Step__c oStep: [Select Id,Name,CSPOFA__Status__c From CSPOFA__Orchestration_Step__c where CSPOFA__Orchestration_Process__c in :orchProcessId]) {
                if (oStep.Name == EP_OrderConstant.ORCHESTRATIONSTEPINVENTORYAPPROVALSTATUS) {
                    oStep.CSPOFA__Status__c = null;
                }
                if (oStep.Name == EP_OrderConstant.ORCHESTRATIONSTEPTERMINATEPROCESSSTATUS) {
                    oStep.CSPOFA__Status__c = EP_OrderConstant.ORCHESTRATIONPROCESSCOMPLETESTATUS;
                }
                orchSteps.add(oStep);
            }
            if (orchSteps != null && !orchSteps.isEmpty()) {
                update orchSteps;
            }               
          }       
      }
      catch(Exception excep){
        EP_LoggingService.logHandledException (excep, EP_Common_Constant.EPUMA, 'orchestratorUpdate', 'EP_CsOrderTriggerHandler', ApexPages.Severity.ERROR);
      }
    }
}