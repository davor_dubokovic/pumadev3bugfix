global class TextUtils { 
    private static final Map<String, String> DATA4_CHARACTER = new Map<String, String> {
        '0' => '8', '1' => '9', '2' => 'a', '3' => 'b',
        '4' => '8', '5' => '9', '6' => 'a', '7' => 'b',
        '8' => '8', '9' => '9', 'a' => 'a', 'b' => 'b',
        'c' => '8', 'd' => '9', 'e' => 'a', 'f' => 'b'
    };

    /**
     * Generates uuid-v4.
     */
    global static String generateGuid() {
        Blob b = Crypto.generateAesKey(128);
        String h = EncodingUtil.convertToHex(b).toLowerCase();
        return (
            h.substring(0, 8)
            + '-' + h.substring(8, 12)
            + '-4' + h.substring(13, 16)
            + '-' + DATA4_CHARACTER.get(h.substring(16, 17)) + h.substring(17, 20)
            + '-' + h.substring(20)
        );
    }

    /**
     * Returns epty string if given string is null
     */
    global static String emptyIfNull(String s) {
        return (String)nvl(s, '');
    }
    
  
   /**
   * Simple nvl implementation
   */
  global static Object nvl(Object value, Object defaultValue) {
    return value != null ? value : defaultValue;
  }
}