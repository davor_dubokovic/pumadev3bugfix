@isTest
public class EP_OrderService_UT 
{
    //#60147 User Story Start
     @testSetup static void init() {
         List<EP_CS_Communication_Settings__c> lCummunicationSetting = Test.loadData(EP_CS_Communication_Settings__c.sObjectType, 'EP_CS_CommunicationSettingTestData');
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_Order_State_Mapping__c> lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
        List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
        List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData');
        EP_INTEGRATION_CUSTOM_SETTING__c integration_Setting = new EP_INTEGRATION_CUSTOM_SETTING__c(Name = 'Request TimeOut',EP_Value__c = '120000');
        insert integration_Setting; 
    }
    //#60147 User Story End

    /* Petar
    public static testMethod void doSubmitActions_test() {
        EP_OrderDomainObject ordDomain = EP_TestDataUtility.getOrderPositiveTestScenario();
        EP_OrderService localObj = new EP_OrderService(ordDomain);
        Test.startTest();
        localObj.doSubmitActions();
        Test.stopTest();
        system.assertEquals(true,localObj.orderType instanceof EP_SalesOrder);
    }
    */
    
    /* Petar
    public static testMethod void doUpdatePaymentTermAndMethod_test() {
        EP_OrderDomainObject ordDomain = EP_TestDataUtility.getOrderPositiveTestScenario();     
        EP_OrderService localObj = new EP_OrderService(ordDomain);
        Test.startTest();
        localObj.doUpdatePaymentTermAndMethod();
        Test.stopTest();
        system.assertEquals(true,localObj.orderType instanceof EP_SalesOrder);
    }
    */
    
    /* Petar
    public static testMethod void hasCreditIssue_NegativeScenariotest() {
        EP_OrderDomainObject ordDomain = EP_TestDataUtility.getOrderPositiveTestScenario();
        EP_OrderService localObj = new EP_OrderService(ordDomain);
        Test.startTest();
        Boolean result = localObj.hasCreditIssue();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    */

    static testMethod void setSellToBillTo_test() {
        EP_OrderDomainObject ordDomain = EP_TestDataUtility.getOrderPositiveTestScenario();
        EP_OrderService localObj = new EP_OrderService(ordDomain);
        ordDomain.setSellTo(null);
		ordDomain.setBillTo(null); 
        Test.startTest();
        localObj.setSellToBillTo();
        Test.stopTest();
        // **** IMPLEMENT ASSERT ~@~ *****
        System.AssertEquals(true,ordDomain.getOrder().EP_Sell_To__c != null);
        System.AssertEquals(true,ordDomain.getOrder().EP_Bill_To__c != null);
        // **** TILL HERE ~@~ *****
    }
    
    static testMethod void getApplicablePaymentTerms_test() {
        EP_OrderDomainObject ordDomain = EP_TestDataUtility.getOrderPositiveTestScenario();
        EP_OrderService localObj = new EP_OrderService(ordDomain);
        // **** IMPLEMENT THIS SECTION ~@~ *****
        Account invoicedAccountObject = new EP_AccountMapper().getAccountrecord(ordDomain.getorder().Accountid__c);
        // **** TILL HERE ~@~ *****
        Test.startTest();
        LIST<String> result = localObj.getApplicablePaymentTerms(invoicedAccountObject);
        Test.stopTest();
        // **** IMPLEMENT ASSERT ~@~ *****
        System.AssertEquals(true,result.size()>0);
        // **** TILL HERE ~@~ *****
    }
    
    /* Petar
    public static testMethod void findPriceBookEntries_test() {
        EP_OrderDomainObject ordDomain = EP_TestDataUtility.getOrderPositiveTestScenario();
        EP_OrderService localObj = new EP_OrderService(ordDomain);
        // **** IMPLEMENT THIS SECTION ~@~ *****
        Id pricebookId = ordDomain.getorder().pricebook2Id__c;
        // **** TILL HERE ~@~ *****
        // **** IMPLEMENT THIS SECTION ~@~ *****
        csord__Order__c orderObj = ordDomain.getorder();
        // **** TILL HERE ~@~ *****
        Test.startTest();
        LIST<PriceBookEntry> result = localObj.findPriceBookEntries(pricebookId,orderObj);
        Test.stopTest();
        // **** IMPLEMENT ASSERT ~@~ *****
        System.AssertEquals(true,result.size()>0);
        // **** TILL HERE ~@~ *****
    }
    */
    
    /* Petar 
    public static testMethod void getOperationalTanks_test() {
        EP_OrderDomainObject ordDomain = EP_TestDataUtility.getOrderPositiveTestScenario();
        EP_OrderService localObj = new EP_OrderService(ordDomain);
        Test.startTest();
        Map<Id, EP_Tank__c> result = localObj.getOperationalTanks(ordDomain.getorder().EP_ShipTo__c);
        Test.stopTest();
        // **** IMPLEMENT ASSERT ~@~ *****
        System.AssertEquals(true,result.size()>0);
        // **** TILL HERE ~@~ *****
    }
    */
    
    /* Petar
    public static testMethod void isValidOrderDate_NegativeScenariotest() {
        EP_OrderDomainObject ordDomain = EP_TestDataUtility.getOrderPositiveTestScenario();
        EP_OrderService localObj = new EP_OrderService(ordDomain);
        Test.startTest();
        Boolean result = localObj.isValidOrderDate();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    */
    
    /* Petar
    public static testMethod void setRequestedDateTime_test() {
        EP_OrderDomainObject ordDomain = EP_TestDataUtility.getOrderPositiveTestScenario();
        EP_OrderService localObj = new EP_OrderService(ordDomain);
        // **** IMPLEMENT THIS SECTION ~@~ *****
        csord__Order__c orderObj = ordDomain.getOrder();
        orderObj.EP_Requested_Delivery_Date__c= null;
        orderObj.EP_Requested_Pickup_Time__c= null;
        String selectedDate = '11-04-2017';
        String availableSlots = '12:13:99';
        // **** TILL HERE ~@~ *****
        Test.startTest();
        csord__Order__c result = localObj.setRequestedDateTime(orderObj,selectedDate,availableSlots);
        Test.stopTest();
        // **** IMPLEMENT ASSERT ~@~ *****
        System.AssertEquals(true,result.EP_Requested_Delivery_Date__c == EP_GeneralUtility.convertSelectedDateToDateInstance(selectedDate));
        System.AssertEquals(true,orderObj.EP_Requested_Pickup_Time__c == String.valueOf(EP_GeneralUtility.convertSelectedHourToTime(availableSlots)).subString(0,8));
        // **** TILL HERE ~@~ *****
    }
    */
    
    /* Petar
    public static testMethod void calculatePrice_test() {
        EP_OrderDomainObject ordDomain = EP_TestDataUtility.getOrderPositiveTestScenario();
        EP_OrderService localObj = new EP_OrderService(ordDomain);
        Test.startTest();
        localObj.calculatePrice();
        Test.stopTest();
        // **** IMPLEMENT ASSERT ~@~ *****
        system.assertEquals(true,localObj.orderType instanceof EP_SalesOrder);
        // **** TILL HERE ~@~ *****
    }
    */
   
    /* Petar
    public static testMethod void isOrderEntryValid_PositiveScenariotest() {
        EP_OrderDomainObject ordDomain = EP_TestDataUtility.getOrderPositiveTestScenario();
        EP_OrderService localObj = new EP_OrderService(ordDomain);
        // **** IMPLEMENT THIS SECTION ~@~ *****
        Integer numOfTransportAccount;
        // **** TILL HERE ~@~ *****
        Test.startTest();
        Boolean result = localObj.isOrderEntryValid(numOfTransportAccount);
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    */
    
    static testMethod void setConsumptionOrderDetails_test() {
        EP_OrderDomainObject ordDomain = EP_TestDataUtility.getConsumptionOrderDomainObject();
        EP_OrderService localObj = new EP_OrderService(ordDomain);
        Test.startTest();
        localObj.setConsumptionOrderDetails();
        Test.stopTest();
        // **** IMPLEMENT ASSERT ~@~ *****
        system.assertEquals(true,localObj.orderType instanceof EP_ConsumptionOrder);
        // **** TILL HERE ~@~ *****
    }
    
    /* Petar
	public static testMethod void hasCreditIssue_PositiveScenariotest() {
        csord__Order__c Orderobj = EP_TestDataUtility.getNonConsignmentOrderNegativeScenario();
        system.debug('Orderobj'+Orderobj);
        EP_OrderDomainObject ordDomain = new EP_OrderDomainObject(Orderobj);
        EP_OrderService localObj = new EP_OrderService(ordDomain);
        Test.startTest();
        Boolean result = localObj.hasCreditIssue();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    */
    
    /* Petar
	public static testMethod void isOrderEntryValid_NegativeScenariotest() {
        EP_OrderDomainObject ordDomain = EP_TestDataUtility.getOrderNegativeTestScenario();
        system.debug('ordDomain+++'+ordDomain.getOrder());
        EP_OrderService localObj = new EP_OrderService(ordDomain);
        // **** IMPLEMENT THIS SECTION ~@~ *****
        Integer numOfTransportAccount = 0;
        // **** TILL HERE ~@~ *****
        Test.startTest();
        Boolean result = localObj.isOrderEntryValid(numOfTransportAccount);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    */
    
    /* Petar
	public static testMethod void isValidOrderDate_PositiveScenariotest() {
        EP_OrderDomainObject ordDomain = EP_TestDataUtility.getSalesOrderDomainObjectNegativeScenario();
        EP_OrderService localObj = new EP_OrderService(ordDomain);
        Test.startTest();
        Boolean result = localObj.isValidOrderDate();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    */
    
    /* Petar
    public static testMethod void checkForNoTransporter_test() {
        EP_OrderDomainObject ordDomain = EP_TestDataUtility.getOrderPositiveTestScenario();
        EP_OrderService localObj = new EP_OrderService(ordDomain);
        // **** IMPLEMENT THIS SECTION ~@~ *****
        String transporterName = EP_Common_Constant.NO_TRANSPORTER;
        // **** TILL HERE ~@~ *****
        Test.startTest();
        csord__Order__c result = localObj.checkForNoTransporter(transporterName);
        Test.stopTest();
        // **** IMPLEMENT ASSERT ~@~ *****
        System.AssertEquals(true,result.EP_Delivery_Type__c==EP_Common_Constant.PIPELINE);
        // **** TILL HERE ~@~ *****
    }
    */
    
    /* Petar
    public static testMethod void getAllRoutesOfShipToAndLocation_test() {
        EP_OrderDomainObject ordDomain = EP_TestDataUtility.getOrderPositiveTestScenario();
        EP_OrderService localObj = new EP_OrderService(ordDomain);
        csord__Order__c orderObj = ordDomain.getOrder();
        Id shipToId = orderObj.EP_ShipTo__c;
        Id stockholdinglocationId = orderObj.Stock_Holding_Location__c;
        Test.startTest();
        LIST<EP_Route__c> result = localObj.getAllRoutesOfShipToAndLocation(shipToId,stockholdinglocationId);
        Test.stopTest();
        // **** IMPLEMENT ASSERT ~@~ *****
        System.AssertEquals(true,result != null);
        // **** TILL HERE ~@~ *****
    }
    */

    /* Petar
    public static testMethod void getCreditAmount_test() {
        EP_OrderDomainObject ordDomain = EP_TestDataUtility.getOrderPositiveTestScenario();
        EP_OrderService localObj = new EP_OrderService(ordDomain);
        csord__Order__c orderObj = ordDomain.getOrder();
        Id shipToId = orderObj.EP_ShipTo__c;
        Id stockholdinglocationId = orderObj.Stock_Holding_Location__c;
        Test.startTest();
        Double creditAmount = localObj.getCreditAmount();
        Test.stopTest();
        // **** IMPLEMENT ASSERT ~@~ *****
        System.AssertEquals(0,creditAmount);
        // **** TILL HERE ~@~ *****
    }
    */

    /* Petar
    public static testMethod void getTanks_test() {
        EP_OrderDomainObject ordDomain = EP_TestDataUtility.getOrderPositiveTestScenario();
        EP_OrderService localObj = new EP_OrderService(ordDomain);
        csord__Order__c orderObj = ordDomain.getOrder();
        Id shipToId = orderObj.EP_ShipTo__c;
        Id stockholdinglocationId = orderObj.Stock_Holding_Location__c;
        Test.startTest();
        Map<Id, EP_Tank__c> mapTanks = localObj.getTanks(shipToId);
        Test.stopTest();
        // **** IMPLEMENT ASSERT ~@~ *****
        System.AssertEquals(1,mapTanks.size());
        // **** TILL HERE ~@~ *****
    }
    */

    static testMethod void doSyncStatusWithNav_test() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        EP_OrderDomainObject ordDomain = EP_TestDataUtility.getOrderPositiveTestScenario();
        EP_OrderService localObj = new EP_OrderService(ordDomain);
        csord__Order__c orderObj = ordDomain.getOrder();
        Id shipToId = orderObj.EP_ShipTo__c;
        Id stockholdinglocationId = orderObj.Stock_Holding_Location__c;
        Test.startTest();
         localObj.doSyncStatusWithNav();
        Test.stopTest();
        // **** IMPLEMENT ASSERT ~@~ *****
        List<EP_IntegrationRecord__c> lstIntegrationRecords = [SELECT id, EP_XML_Message__c,EP_Attempt__c,EP_Message_Type__c FROM EP_IntegrationRecord__c];
        System.Assert(lstIntegrationRecords.size() > 0);
        // **** TILL HERE ~@~ *****
    }

    static testMethod void doSyncStatusWithNavEnqeue_test() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        EP_OrderDomainObject ordDomain = EP_TestDataUtility.getOrderPositiveTestScenario();
        EP_OrderService localObj = new EP_OrderService(ordDomain);
        csord__Order__c orderObj = ordDomain.getOrder();
        Id shipToId = orderObj.EP_ShipTo__c;
        Id stockholdinglocationId = orderObj.Stock_Holding_Location__c;
        Test.startTest();
        localObj.enqueueJob = true;
        localObj.doSyncStatusWithNav();
        Test.stopTest();
        // **** IMPLEMENT ASSERT ~@~ *****
        List<EP_IntegrationRecord__c> lstIntegrationRecords = [SELECT id, EP_XML_Message__c,EP_Attempt__c,EP_Message_Type__c FROM EP_IntegrationRecord__c];
        System.Assert(lstIntegrationRecords.size() > 0);
        // **** TILL HERE ~@~ *****
    }

    static testMethod void doSyncStatusWithWindms_test() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        EP_OrderDomainObject ordDomain = EP_TestDataUtility.getOrderPositiveTestScenario();
        EP_OrderService localObj = new EP_OrderService(ordDomain);
        csord__Order__c orderObj = ordDomain.getOrder();
        Id shipToId = orderObj.EP_ShipTo__c;
        Id stockholdinglocationId = orderObj.Stock_Holding_Location__c;
        Test.startTest();
        localObj.doSyncStatusWithWindms();
        Test.stopTest();
        // **** IMPLEMENT ASSERT ~@~ *****
        List<EP_IntegrationRecord__c> lstIntegrationRecords = [SELECT id, EP_XML_Message__c,EP_Attempt__c,EP_Message_Type__c FROM EP_IntegrationRecord__c];
        System.Assert(lstIntegrationRecords.size() > 0);
        // **** TILL HERE ~@~ *****
    }

    /* Petar
    public static testMethod void setOrderStatus_test() {
        EP_OrderDomainObject ordDomain = new EP_OrderDomainObject(EP_TestDataUtility.getNonConsignmentOrderPositiveScenario());
        EP_OrderService localObj = new EP_OrderService(ordDomain);
        csord__Order__c orderObj = ordDomain.getOrder();
        Id shipToId = orderObj.EP_ShipTo__c;
        Id stockholdinglocationId = orderObj.Stock_Holding_Location__c;
        string orderEventStr = EP_OrderConstant.USER_SUBMIT;
        EP_OrderEvent orderEvent = new EP_OrderEvent(orderEventStr);
        Test.startTest();
        Boolean statusChanged = localObj.setOrderStatus(orderEvent);
        localObj.doPostStatusChangeActions();
        Test.stopTest();
        // **** IMPLEMENT ASSERT ~@~ *****
        System.assertEquals(true,statusChanged);
        // **** TILL HERE ~@~ *****
    }
    */

    static testMethod void doSyncCreditCheckWithWINDMS_test() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        EP_OrderDomainObject ordDomain = new EP_OrderDomainObject(EP_TestDataUtility.getNonConsignmentOrderPositiveScenario());
        EP_OrderService localObj = new EP_OrderService(ordDomain);
        csord__Order__c orderObj = ordDomain.getOrder();
        Id shipToId = orderObj.EP_ShipTo__c;
        Id stockholdinglocationId = orderObj.Stock_Holding_Location__c;
        Test.startTest();
        localObj.doSyncCreditCheckWithWINDMS();
        Test.stopTest();
        // **** IMPLEMENT ASSERT ~@~ *****
        List<EP_IntegrationRecord__c> lstIntegrationRecords = [SELECT id, EP_XML_Message__c,EP_Attempt__c,EP_Message_Type__c FROM EP_IntegrationRecord__c];
        System.Assert(lstIntegrationRecords.size() > 0);
        // **** TILL HERE ~@~ *****
    }

    
    static testMethod void doSyncCreditCheckInProgressWithWINDMS_test() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        EP_OrderDomainObject ordDomain = new EP_OrderDomainObject(EP_TestDataUtility.getNonConsignmentOrderPositiveScenario());
        EP_OrderService localObj = new EP_OrderService(ordDomain);
        csord__Order__c orderObj = ordDomain.getOrder();
        Id shipToId = orderObj.EP_ShipTo__c;
        Id stockholdinglocationId = orderObj.Stock_Holding_Location__c;
        Test.startTest();
        localObj.doSyncCreditCheckInProgressWithWINDMS();
        Test.stopTest();
        // **** IMPLEMENT ASSERT ~@~ *****
        List<EP_IntegrationRecord__c> lstIntegrationRecords = [SELECT id, EP_XML_Message__c,EP_Attempt__c,EP_Message_Type__c FROM EP_IntegrationRecord__c];
        System.Assert(lstIntegrationRecords.size() > 0);
        // **** TILL HERE ~@~ *****
    }

    static testMethod void doSyncCreditCheckWithWINDMSEnqueue_test() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        EP_OrderDomainObject ordDomain = new EP_OrderDomainObject(EP_TestDataUtility.getNonConsignmentOrderPositiveScenario());
        EP_OrderService localObj = new EP_OrderService(ordDomain);
        csord__Order__c orderObj = ordDomain.getOrder();
        Id shipToId = orderObj.EP_ShipTo__c;
        Id stockholdinglocationId = orderObj.Stock_Holding_Location__c;
        Test.startTest();
        localObj.enqueueJob = true;
        localObj.doSyncCreditCheckWithWINDMS();
        Test.stopTest();
        // **** IMPLEMENT ASSERT ~@~ *****
        List<EP_IntegrationRecord__c> lstIntegrationRecords = [SELECT id, EP_XML_Message__c,EP_Attempt__c,EP_Message_Type__c FROM EP_IntegrationRecord__c];
        System.Assert(lstIntegrationRecords.size() > 0);
        // **** TILL HERE ~@~ *****
    }
    
    /* Petar
    public static testMethod void submitForInventoryApproval_test() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
         EP_OrderDomainObject ordDomain = new EP_OrderDomainObject(EP_TestDataUtility.getNonConsignmentOrderPositiveScenario());
        EP_OrderService localObj = new EP_OrderService(ordDomain);
        csord__Order__c orderObj = ordDomain.getOrder();
        Id shipToId = orderObj.EP_ShipTo__c;
        Id stockholdinglocationId = orderObj.Stock_Holding_Location__c;
        orderObj.Status__c = EP_OrderConstant.OrderState_Awaiting_Inventory_Review;
        update orderObj;
        Test.startTest();
        localObj.submitForInventoryApproval();
        Test.stopTest();
        // **** IMPLEMENT ASSERT ~@~ *****
        System.assertEquals(true,Approval.isLocked(orderObj.Id));
        // **** TILL HERE ~@~ *****
    }
    */
 

    //#60147 User Story End
}
