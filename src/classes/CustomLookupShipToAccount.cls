global  without sharing class CustomLookupShipToAccount extends cscfga.ALookupSearch {

  public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit) {
        String accountId = searchFields.get('AccountId');
        String pType = searchFields.get('Product Type');
        String accName = searchFields.get('searchValue');
        String userId = UserInfo.getUserId();
        String epoch = searchFields.get('Order Epoch');

        if(accName != null) {
            accName = '%'+ accName +'%';
        }

        List <Account> accList = new List<Account>();
        List <Account> switchAccountList = new List<Account>();
        
        List<Id> addShipTo = new List<Id>();
        
        List<String> nonBulk = new List<String> {'Non-VMI Ship To','VMI Ship To'};
        Map<Id, Profile> ps = new Map<Id, Profile>([SELECT Id, Name FROM Profile WHERE Name IN ('EP_Portal_Basic_Ordering_Access_User', 'EP_Portal_Finance_Access_User', 'EP_Portal_Super_User')]);

        List<User> us = [SELECT Id, Name, ProfileId, Contact.Id, Contact.AccountId FROM User WHERE Id =: userID];
        String userProfileId = us.get(0).ProfileId;
        List<EP_FE_PortalUserAccountAccess__c > cs = [SELECT Id, EP_FE_Shipto_Name__c  FROM EP_FE_PortalUserAccountAccess__c  WHERE EP_FE_Customer_Name__c  =: us.get(0).Contact.Id and EP_FE_Customer_Name__c!=null];
        
        for(EP_FE_PortalUserAccountAccess__c access:cs) {
            addShipTo.add(access.EP_FE_Shipto_Name__c);
        }
        
        system.debug('map'+accountId);
        system.debug('accountId'+searchFields);
        system.debug('pType'+pType);
        if(pType!=null) {

            if(pType.equalsIgnoreCase('Bulk') && epoch.equalsIgnoreCase('Current')) {

                     if(accName != null) { 
                    accList= [SELECT id,name,ShippingPostalCode,ParentId,EP_Puma_Company__c,AccountNumber,RouteAllocationCount__c,ShippingState,ShippingCity,EP_Ship_To_Type__c,EP_Status__c,ShippingStreet,ShippingAddress 
                              FROM Account 
                              WHERE parentid =: accountId and recordtype.name ='Non-VMI Ship To' and EP_Status__c = '05-Active' and name like :accName];
                }
                else  {
                    accList= [SELECT id,name,ShippingPostalCode,ParentId,EP_Puma_Company__c,AccountNumber,RouteAllocationCount__c,ShippingState,ShippingCity,EP_Ship_To_Type__c,EP_Status__c,ShippingStreet,ShippingAddress 
                              FROM Account 
                              WHERE parentid =: accountId and recordtype.name ='Non-VMI Ship To' and EP_Status__c = '05-Active' ];
                }

            }

            else if(pType.equalsIgnoreCase('Bulk') && epoch.equalsIgnoreCase('Retrospective')) {
                if(accName != null) { 
                    accList= [SELECT id,name,ShippingPostalCode,ParentId,EP_Puma_Company__c,AccountNumber,RouteAllocationCount__c,ShippingState,ShippingCity,EP_Ship_To_Type__c,EP_Status__c,ShippingStreet,ShippingAddress 
                              FROM Account 
                              WHERE parentid =: accountId  and EP_Status__c = '05-Active' and name like :accName];
                }
                else  {
                    accList= [SELECT id,name,ShippingPostalCode,ParentId,EP_Puma_Company__c,AccountNumber,RouteAllocationCount__c,ShippingState,ShippingCity,EP_Ship_To_Type__c,EP_Status__c,ShippingStreet,ShippingAddress 
                              FROM Account 
                              WHERE parentid =: accountId  and EP_Status__c = '05-Active' ];
                }
            }

            else {
                if(accName != null) { 
                    accList= [SELECT id,name,RouteAllocationCount__c,ParentId,EP_Puma_Company__c,AccountNumber,ShippingPostalCode,ShippingState,ShippingCity,EP_Ship_To_Type__c,EP_Status__c,ShippingStreet,ShippingAddress 
                              FROM Account 
                              WHERE parentid =: accountId and recordtype.name in:nonBulk and EP_Status__c = '05-Active' and EP_Ship_To_Type__c != 'Consignment' and name like :accName];
                }
                else {
                    accList= [SELECT id,name,RouteAllocationCount__c,ParentId,EP_Puma_Company__c,AccountNumber,ShippingPostalCode,ShippingState,ShippingCity,EP_Ship_To_Type__c,EP_Status__c,ShippingStreet,ShippingAddress
                              FROM Account 
                              WHERE parentid =: accountId and recordtype.name in:nonBulk and EP_Status__c = '05-Active' and EP_Ship_To_Type__c != 'Consignment'];
                }

            }
            if(!addShipTo.isEmpty()) {
                switchAccountList = [SELECT id,name,ShippingPostalCode,ParentId,EP_Puma_Company__c,AccountNumber,RouteAllocationCount__c,ShippingState,ShippingCity,EP_Ship_To_Type__c,EP_Status__c,ShippingStreet,ShippingAddress 
                              FROM Account 
                              WHERE id in :addShipTo and parentId !=: accountId];
            }
            
        }

      if (cs.isEmpty()) {
        return accList;
      }
      
      if(us.get(0).Contact.AccountId == accountId) {
          return accList;
      }
      
      if(switchAccountList.isEmpty()) {
          return accList;
      }
      else
      {
            //accList.addAll(switchAccountList);
            List<Account> finalInsertList = new List<Account>();
            if (ps.containsKey(userProfileId)) {
                for(Account acc: accList) {
                    for (EP_FE_PortalUserAccountAccess__c  pac:cs) {
                        if(acc.Id == pac.EP_FE_Shipto_Name__c){
                            finalInsertList.add(acc);
                        } 
                    }
                }
            }
        if(accList == null && !accList.isEmpty()){
            return null;
        }
          return finalInsertList;
      }
    }  
    global override List<Object> doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionId) {
        String accountId = searchFields.get('AccountId');
        String pType = searchFields.get('Product Type');
        String userId = UserInfo.getUserId();
        system.debug('map'+searchFields);
        system.debug('accountId'+searchFields);
        system.debug('pType'+pType);
        
        Map<Id, Profile> ps = new Map<Id, Profile>([SELECT Id, Name FROM Profile WHERE Name IN ('EP_Portal_Basic_Ordering_Access_User', 'EP_Portal_Finance_Access_User', 'EP_Portal_Super_User')]);

        List<User> us = [SELECT Id, Name, ProfileId, Contact.Id FROM User WHERE Id =: userID];
        String userProfileId = us.get(0).ProfileId;
        List<EP_FE_PortalUserAccountAccess__c > cs = [SELECT Id, Name, EP_FE_Shipto_Name__c  FROM EP_FE_PortalUserAccountAccess__c  WHERE EP_FE_Customer_Name__c  =: us.get(0).Contact.Id];
        
        List <Account> accList = new List<Account>();
        List<String> nonBulk = new List<String> {'Non-VMI Ship To','VMI Ship To'};
        if(pType!=null) {
            if(pType.equalsIgnoreCase('Bulk')) {
                accList= [SELECT id,name,ShippingPostalCode,ParentId,EP_Puma_Company__c,AccountNumber,RouteAllocationCount__c,ShippingState,ShippingCity,EP_Ship_To_Type__c,EP_Status__c 
                          FROM Account 
                          WHERE parentid =: accountId and recordtype.name ='Non-VMI Ship To' and EP_Status__c = '05-Active'];
            }
            else {
                accList= [SELECT id,name,RouteAllocationCount__c,ParentId,EP_Puma_Company__c,AccountNumber,ShippingPostalCode,ShippingState,ShippingCity,EP_Ship_To_Type__c,EP_Status__c 
                          FROM Account 
                          WHERE parentid =: accountId and recordtype.name in:nonBulk and EP_Status__c = '05-Active' and EP_Ship_To_Type__c != 'Consignment'];
            }
        }
      if (cs.isEmpty()) {
        return accList;
      }
        List<Account> finalInsertList = new List<Account>();
        if (ps.containsKey(userProfileId)) {
            for(Account acc: accList) {
                for (EP_FE_PortalUserAccountAccess__c  pac:cs) {
                    if(acc.Id == pac.EP_FE_Shipto_Name__c){
                        finalInsertList.add(acc);
                    } 
                }
            }
        }
      
        if(accList == null && !accList.isEmpty()){
            return null;
        }


          return finalInsertList;
    }
    global override String getRequiredAttributes() {

        return '["AccountId","Product Type", "Order Epoch", "UserId"]';
    }    
}