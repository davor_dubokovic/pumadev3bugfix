@isTest
public class EP_OutboundIntegrationService_UT
{   
    //#60147 User Story Start
    @testSetup static void init() {
        List<EP_CS_Communication_Settings__c> lCummunicationSetting = Test.loadData(EP_CS_Communication_Settings__c.sObjectType, 'EP_CS_CommunicationSettingTestData');
        List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
        List<EP_INTEGRATION_CUSTOM_SETTING__c> lIntegrationCustomSetting = Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.sObjectType, 'EP_INTEGRATION_CUSTOM_SETTING_TESTDATA');
        List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData');
    }
    
    //#60147 User Story End
    static testMethod void getEndpoint_test() {
        String endPoint =EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        String reqMethod=EP_Common_Constant.POST;
        String requestXML=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        //#60147 User Story Start
        String subscriptionKey=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        EP_OutboundIntegrationService localObj = new EP_OutboundIntegrationService(endPoint, reqMethod, requestXML,subscriptionKey);
        //#60147 User Story End
        Test.startTest();
        String result = localObj.getEndpoint();
        Test.stopTest();
        system.assertEquals(true,result.equalsIgnoreCase(endPoint));
    }
    static testMethod void getRequestMethod_test() {
        String endPoint =EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        String reqMethod=EP_Common_Constant.POST;
        String requestXML=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        //#60147 User Story Start
        String subscriptionKey=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        EP_OutboundIntegrationService localObj = new EP_OutboundIntegrationService(endPoint, reqMethod, requestXML,subscriptionKey);
        //#60147 User Story End
        Test.startTest();
        String result = localObj.getRequestMethod();
        Test.stopTest();
        system.assertEquals(true,result.equalsIgnoreCase(reqMethod));
    }
    static testMethod void getRequestXML_test() {
        String endPoint =EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        String reqMethod=EP_Common_Constant.POST;
        String requestXML=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        //#60147 User Story Start
        String subscriptionKey=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        EP_OutboundIntegrationService localObj = new EP_OutboundIntegrationService(endPoint, reqMethod, requestXML,subscriptionKey);
        //#60147 User Story End
        Test.startTest();
        String result = localObj.getRequestXML();
        Test.stopTest();
        system.assertEquals(true,result.equalsIgnoreCase(requestXML));
    }
    static testMethod void setEndpoint_test() {
        String endPoint =EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        String reqMethod=EP_Common_Constant.POST;
        String requestXML=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        //#60147 User Story Start
        String subscriptionKey=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        EP_OutboundIntegrationService localObj = new EP_OutboundIntegrationService(endPoint, reqMethod, requestXML,subscriptionKey);
        //#60147 User Story End
        Test.startTest();
        localObj.setEndpoint(endPoint);
        String result = localObj.getEndpoint();
        Test.stopTest();
        system.assertEquals(true,result.equalsIgnoreCase(endPoint));
    }
    static testMethod void setRequestMethod_test() {
        String endPoint =EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        String reqMethod=EP_Common_Constant.POST;
        String requestXML=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        //#60147 User Story Start
        String subscriptionKey=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        EP_OutboundIntegrationService localObj = new EP_OutboundIntegrationService(endPoint, reqMethod, requestXML,subscriptionKey);
        //#60147 User Story End
        Test.startTest();
        localObj.setRequestMethod(reqMethod);
        String result = localObj.getRequestMethod();
        Test.stopTest();
        system.assertEquals(true,result.equalsIgnoreCase(reqMethod));
    }
    static testMethod void setRequestXML_test() {
        String endPoint =EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        String reqMethod=EP_Common_Constant.POST;
        String requestXML=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        //#60147 User Story Start
        String subscriptionKey=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        EP_OutboundIntegrationService localObj = new EP_OutboundIntegrationService(endPoint, reqMethod, requestXML,subscriptionKey);
        //#60147 User Story End
        Test.startTest();
        localObj.setRequestXML(requestXML);
        String result = localObj.getRequestXML();
        Test.stopTest();
        system.assertEquals(true,result.equalsIgnoreCase(endPoint));
    }
    static testMethod void invokeRequest_test() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        String endPoint =EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        String reqMethod=EP_Common_Constant.POST;
        String requestXML=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        //#60147 User Story Start
        String subscriptionKey=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        EP_OutboundIntegrationService localObj = new EP_OutboundIntegrationService(endPoint, reqMethod, requestXML,subscriptionKey);
        //#60147 User Story End
        Test.startTest();
        EP_IntegrationServiceResult result = localObj.invokeRequest();
        Test.stopTest();
        String actualValue = result.getResponse().getBody();
        String expectedValue = '{"message":"success"}';
        System.assertEquals(actualValue, '');
    }
    //add assert
    static testMethod void processResult_test() {
        String endPoint =EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        String reqMethod=EP_Common_Constant.POST;
        String requestXML=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        //#60147 User Story Start
        String subscriptionKey=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        EP_OutboundIntegrationService localObj = new EP_OutboundIntegrationService(endPoint, reqMethod, requestXML,subscriptionKey);
        EP_IntegrationServiceResult result = new EP_IntegrationServiceResult();
        HTTPResponse responseObj = new HTTPResponse();
        result.setResponse(responseObj );
        Set<id> integrationRecords = new Set<Id>();
        String messageType = EP_Common_Constant.SEND_CREDIT_EXCEPTION_REQUEST ;
        Test.startTest();
        localObj.processResult(result,integrationRecords,0,messageType);
        //#60147 User Story End
        Test.stopTest();
        // No assert required calling other methods 
        system.assert(true);
    }
    //add assert
    static testMethod void checkBasicDataSetup_test1() {
        String endPoint =EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        String reqMethod=EP_Common_Constant.POST;
        String requestXML=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        string exMessage;
        Test.startTest();
        try {
            EP_OutboundIntegrationService.checkBasicDataSetup(endPoint,reqMethod,requestXML);
        } catch(exception ex){
            exMessage = ex.getMessage();
        }
        Test.stopTest();
        system.assertEquals(true, exMessage==null);
    }
    
    static testMethod void checkBasicDataSetup_test2() {
        string expMessage='Cannot instantiate integration service for system because End Point is missing';
        String endPoint=null;
        String reqMethod=EP_Common_Constant.POST;
        String requestXML=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        string exMessage ='';
        Test.startTest();
        try{
            EP_OutboundIntegrationService.checkBasicDataSetup(endPoint,reqMethod,requestXML);
        } catch (Exception ex){
            exMessage = ex.getMessage();
        }
        Test.stopTest();
        system.assertEquals(true,exMessage==expMessage);
    }
    
    static testMethod void checkBasicDataSetup_test3() {
        string expMessage='Cannot instantiate integration service for system because Requested Method is missing';
        String endPoint =EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        String reqMethod=null;
        String requestXML=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        string exMessage ='';
        Test.startTest();
        try {
            EP_OutboundIntegrationService.checkBasicDataSetup(endPoint,reqMethod,requestXML);
        } catch (Exception ex){
            exMessage = ex.getMessage();
        }
        Test.stopTest();
        system.assertEquals(true,exMessage==expMessage);
    }
    
    static testMethod void checkBasicDataSetup_test4() {
        string expMessage='Cannot instantiate integration service for system because Require XML is missing';
        String endPoint =EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        String reqMethod=EP_Common_Constant.POST;
        String requestXML=null;
        string exMessage ='';
        Test.startTest();
        try {
            EP_OutboundIntegrationService.checkBasicDataSetup(endPoint,reqMethod,requestXML);
        } catch (Exception ex){
            exMessage = ex.getMessage();
        }
        Test.stopTest();
        system.assertEquals(true,exMessage==expMessage);
    }
    static testMethod void logResponse_test() {
        String endPoint =EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        String reqMethod=EP_Common_Constant.POST;
        String requestXML=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        //#60147 User Story Start
        //move to test data utility
        EP_IntegrationRecord__c  record = new EP_IntegrationRecord__c ();
        record.EP_Message_ID__c='123';
        record.EP_Object_Type__c ='Account';
        record.EP_Status__c='SYNC';
        record.EP_Source__c='Source';
        record.EP_Target__c='Target';
        Database.insert(record);
        String endPointUrl =EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        Test.startTest();
        //#60147 User Story Start
        String subscriptionKey=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        String messageType = EP_Common_Constant.SEND_CREDIT_EXCEPTION_REQUEST ;
        EP_OutboundIntegrationService localObj = new EP_OutboundIntegrationService(endPoint, reqMethod, requestXML,subscriptionKey);
        EP_IntegrationServiceResult results = new EP_IntegrationServiceResult();
        HttpResponse responseObj = new HttpResponse();
        responseObj.setStatusCode(200);
        responseObj.setHeader('Content-Type', 'application/json');
        responseObj.setBody('{"message":"success"}');      
        results.setResponse(responseObj );
        EP_OutboundIntegrationService.logResponse(requestXML,new List<EP_IntegrationRecord__c>{record},results,endPointUrl,0,messageType);
        Test.stopTest();
        //#60147 User Story End

        system.assertEquals(requestXML,record.EP_XML_Message__c);
    }

    //#60147 User Story Start
    static testMethod void logResponseNeg_test() {
        String endPoint =EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        String reqMethod=EP_Common_Constant.POST;
        String requestXML=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        //#60147 User Story Start
        //move to test data utility
        EP_IntegrationRecord__c  record = new EP_IntegrationRecord__c ();
        record.EP_Message_ID__c='123';
        record.EP_Object_Type__c ='Account';
        record.EP_Status__c='SYNC';
        record.EP_Source__c='Source';
        record.EP_Target__c='Target';
        Database.insert(record);
        String endPointUrl =EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        Test.startTest();
        String subscriptionKey=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        String messageType = EP_Common_Constant.SEND_CREDIT_EXCEPTION_REQUEST ;
        EP_OutboundIntegrationService localObj = new EP_OutboundIntegrationService(endPoint, reqMethod, requestXML,subscriptionKey);
        EP_IntegrationServiceResult results = new EP_IntegrationServiceResult();
        HttpResponse responseObj = new HttpResponse();
        responseObj.setStatusCode(404);
        responseObj.setHeader('Content-Type', 'application/json');
        responseObj.setBody('{"message":"Error"}');      
        results.setResponse(responseObj );
        EP_OutboundIntegrationService.logResponse(requestXML,new List<EP_IntegrationRecord__c>{record},results,endPointUrl,0,messageType);
        Test.stopTest();
        system.assertEquals(requestXML,record.EP_XML_Message__c);
    }
    
    static testMethod void ProcessResponse_test() {
        String endPoint =EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        String reqMethod=EP_Common_Constant.POST;
        String requestXML=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        //move to test data utility
        EP_IntegrationRecord__c  record = new EP_IntegrationRecord__c ();
        record.EP_Message_ID__c='123';
        record.EP_Object_Type__c ='Account';
        record.EP_Status__c='SYNC';
        record.EP_Source__c='Source';
        record.EP_Target__c='Target';
        Database.insert(record);
        String endPointUrl =EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        Test.startTest();
        //#60147 User Story Start
        String subscriptionKey=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        String messageType = EP_Common_Constant.SEND_CREDIT_EXCEPTION_REQUEST ;
        EP_OutboundIntegrationService localObj = new EP_OutboundIntegrationService(endPoint, reqMethod, requestXML,subscriptionKey);
        EP_IntegrationServiceResult results = new EP_IntegrationServiceResult();
        HttpResponse responseObj = new HttpResponse();
        responseObj.setStatusCode(200);
        responseObj.setHeader('Content-Type', 'application/json');
        responseObj.setBody('{"message":"success"}');      
        results.setResponse(responseObj );
        localObj.processResponse(results,messageType,new List<EP_IntegrationRecord__c>{record});
        Test.stopTest();
        system.assertEquals(EP_Common_Constant.SYNC_STATUS,record.EP_Status__c);
    }

    
    //#60147 User Story End
    
    //#59186 User Story Start
    static testMethod void setATRFieldsPositive_Test() {
        Test.startTest();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        EP_IntegrationRecord__c intRecord = EP_TestDataUtility.createIntegrationRecforATR(obj.getOrder().Id,null);
        EP_CS_OutboundMessageSetting__c  msgSetting = EP_CustomSettingsUtil.getOutboundMessageSetting('SFDC_TO_NAV_ORDER_SYNC');
        EP_OutboundIntegrationService.setATRFields(intRecord,msgSetting,null);
        Test.stopTest();
        System.assertEquals(intRecord.EP_Attempt__c,2.0);
        System.assertEquals(intRecord.EP_Queuing_Retry__c,true);

    }

    static testMethod void setATRFieldsNegative_Test() {
        Test.startTest();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        EP_IntegrationRecord__c intRecord = EP_TestDataUtility.createIntegrationRecforATR(obj.getOrder().Id,null);
        EP_CS_OutboundMessageSetting__c  msgSetting = EP_CustomSettingsUtil.getOutboundMessageSetting('SFDC_TO_NAV_ORDER_SYNC');
        msgSetting.EP_Queuing_Retry__c = false;
        update msgSetting;
        EP_OutboundIntegrationService.setATRFields(intRecord,msgSetting,null);
        Test.stopTest();
        System.assertEquals(intRecord.EP_Attempt__c,2.0);
        System.assertEquals(intRecord.EP_Queuing_Retry__c,false);
    }
    
    static testMethod void isEligibleForATRPositive_Test(){
        Test.startTest();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        EP_IntegrationRecord__c intRecord = EP_TestDataUtility.createIntegrationRecforATR(obj.getOrder().Id,null);
        EP_CS_OutboundMessageSetting__c  msgSetting = EP_CustomSettingsUtil.getOutboundMessageSetting('SFDC_TO_NAV_ORDER_SYNC');
        Test.stopTest();
        System.assertEquals(EP_OutboundIntegrationService.isEligibleForATR(intRecord,msgSetting),true);
    }
    
    static testMethod void isEligibleForATRNegative_Test(){
        Test.startTest();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        EP_IntegrationRecord__c intRecord = EP_TestDataUtility.createIntegrationRecforATR(obj.getOrder().Id,null);
        EP_CS_OutboundMessageSetting__c  msgSetting = EP_CustomSettingsUtil.getOutboundMessageSetting('SFDC_TO_NAV_ORDER_SYNC');
        Test.stopTest();
        msgSetting.EP_Queuing_Retry__c = false;
        update msgSetting;
        System.assertEquals(EP_OutboundIntegrationService.isEligibleForATR(intRecord,msgSetting),false);
    }

    static testMethod void isStatusCodeValidPositiveNegative_Test(){
        System.assertEquals(true,EP_OutboundIntegrationService.isStatusCodeValid('408','408;423;429;503;504;599;500;502;404'));
        System.assertEquals(false,EP_OutboundIntegrationService.isStatusCodeValid('201','408;423;429;503;504;599;500;502;404'));
    }
    //#59186 User Story End
    
}