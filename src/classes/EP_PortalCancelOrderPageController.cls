public with sharing class EP_PortalCancelOrderPageController {

    @RemoteAction
    public static String cancelOrder(String orderId){

        try{
            
            String result = '';

            if(orderId != '' && orderId != null){

                List<csord__Order__c> orderList = [SELECT Id, EP_Order_Locked__c, Cancellation_Check_Done__c, EP_CutOff_Check_Required__c, csord__Status2__c, 
                                                        EP_Order_Epoch__c, EP_Order_Product_Category__c, Stock_Holding_Location__c, Pickup_Supply_Location__c, 
                                                        EP_Requested_Delivery_Date__c, Delivery_From_Date__c, Delivery_To_Date__c, csord__Account__c, csord__Account__r.EP_Puma_Company__c,
                                                        csord__Account__r.EP_Advance_Order_Entry_Days__c, csord__Account__r.EP_Puma_Company__r.EP_Advance_Order_Entry_Days__c,
                                                        EP_Delivery_Type__c
                                                    FROM csord__Order__c 
                                                    WHERE Id = :orderId];

                if(orderList != null && orderList.size() > 0){
                    
                    csord__Order__c orderToCancel = orderList[0];

                    Boolean isCutOff = false;

                    if(orderToCancel.EP_Order_Locked__c){
                        
                        result = Label.EP_Order_Cancel_Msg;
                        
                    } else if(orderToCancel.Cancellation_Check_Done__c){
                        
                        result = Label.EP_Order_Already_Checked;
                        
                    } else if(orderToCancel.EP_CutOff_Check_Required__c == 'Yes'){
                        
                        Datetime localDateTime = Datetime.now();
                        TimeZone tz = UserInfo.getTimeZone();
                        
                        Integer localTimeZoneOffset = tz.getOffset(localDateTime);

                        result = Label.EP_Cut_off_Matrix_Failed;
                        
                        isCutOff = CutOffMatrixAPI.processCutOffMatrixCheckUsingOrder(orderToCancel, localDateTime, 0, true);

                    } else {
                        
                        isCutOff = true;
                    }

                    if(isCutOff){
                        orderToCancel.Cancellation_Check_Done__c = true;
                        update orderToCancel;
                        result = Label.EP_Cut_off_Matrix_Passed;
                    } 
                    
                } else {
                    result = string.format(Label.EP_Order_Id_not_found, new List<String>{orderId});
                }
                
            } else {
                result = Label.EP_Order_Id_cannot_be_null;
            }

            return result;
            
        } catch (Exception e) {
            
            EP_LoggingService.logServiceException(e, UserInfo.getOrganizationId(), EP_Common_constant.EPUMA, 'cancelOrder', 'EP_PortalCancelOrderPageController', EP_Common_constant.ERROR, UserInfo.getUserId(), EP_Common_constant.TARGET_SF,  EP_Common_Constant.BLANK, EP_Common_Constant.BLANK);
            return 'Process error.';
        }
    }
}