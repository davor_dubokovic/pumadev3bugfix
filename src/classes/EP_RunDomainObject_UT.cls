@isTest
public class EP_RunDomainObject_UT{
   static testMethod void getNewRun_test()  {        
     List<EP_Run__c> runRecords = EP_TestDataUtility.getRunObjects();         
     EP_RunDomainObject localObj = new EP_RunDomainObject (runRecords);
     Test.startTest();
        List <EP_Run__c> result = localObj.getNewRun();
     Test.stopTest();
     System.AssertEquals(true,result.size() > 0); 
   } 
   
   static testMethod void getOldRunMap_test() {
        List<EP_Run__c> runRecords = EP_TestDataUtility.getRunObjects();         
        List<EP_Run__c> oldRunRecord = runRecords.clone();     
        oldRunRecord[0].EP_Run_Start_Date__c = System.today()+2;                
        EP_RunDomainObject localObj = new EP_RunDomainObject (runRecords ,new  Map<Id,EP_Run__c>(oldRunRecord ));
        Test.startTest();
           Map<id, EP_Run__c> result = localObj.getOldRunMap();
        Test.stopTest();
        System.assertEquals(true, result.values().size()>0);
         
    }
    static testMethod void doActionBeforeInsert_test_Positive() {
        List<EP_Run__c> runRecords = EP_TestDataUtility.getRunObjects();         
        EP_RunDomainObject localObj = new EP_RunDomainObject (runRecords);
        Test.startTest();
        localObj.doActionBeforeInsert();
        Test.stopTest();
        //No Assert Required, as this method delegates other methods.  
        System.assert(true); 
    }
    static testMethod void doActionBeforeInsert_ExceptionTest() {             
        //List<EP_Run__c> runRecords = EP_TestDataUtility.getRunObjects();         
        List<EP_Run__c> runRecords =  new List<EP_Run__c> ();         
        EP_RunDomainObject localObj = new EP_RunDomainObject(runRecords);
        Test.startTest();
        localObj.doActionBeforeInsert();
        Test.stopTest();
        //No Assert Required, as this method delegates other methods.  
        System.assert(true); 
    }
     static testMethod void doActionBeforeDelete_Test() {
        List<EP_Run__c> runRecords = EP_TestDataUtility.getRunObjects();        
        List<EP_Run__c> oldRunRecord = runRecords.clone();     
        oldRunRecord[0].EP_Run_Start_Date__c = System.today()+2; 
        update oldRunRecord;               
        EP_RunDomainObject localObj = new EP_RunDomainObject (runRecords ,new  Map<Id,EP_Run__c>(oldRunRecord ));          
        Test.startTest();
        localObj.doActionBeforeDelete();
        Test.stopTest();        
        //No Assert Required, as this method delegates other methods.  
        System.assert(true);     
    } 
    static testMethod void doActionBeforeDelete_ExceptionTest() {
        List<EP_Run__c> runRecords = EP_TestDataUtility.getRunObjects();
        //Test class Fix Start                                                          
        EP_RunDomainObject localObj = new EP_RunDomainObject (null, new Map<Id,EP_Run__c>());
		//Test class Fix End           
        Test.startTest();
        localObj.doActionBeforeDelete();
        Test.stopTest();
        //No Assert Required, as this method delegates other methods.
        System.assert(true);    
    } 
    /* static testMethod void isRunDeletable_test() {
        List<EP_Run__c> runRecords = EP_TestDataUtility.getRunObjects();        
        List<EP_Run__c> oldRunRecord = runRecords.clone();     
        oldRunRecord[0].EP_Run_Start_Date__c = System.today()+2; 
        update oldRunRecord;               
        EP_RunDomainObject localObj = new EP_RunDomainObject (runRecords ,new  Map<Id,EP_Run__c>(oldRunRecord ));           
        List <EP_Run__c> runList =  localObj.getOldRunMap().values();                     
        Test.startTest();
        Boolean result = localObj.isRunDeletable(runList[0].id);
        Test.stopTest();       
        System.assertEquals(true,result);    
    }  */       
}