/*
 *  @Author <Accenture>
 *  @Name <EP_Vendor>
 *  @CreateDate <16/02/2017>
 *  @Description  <This class executes logic for Vendor account>
 *  @Version <1.0>
 */
 public class EP_Vendor extends EP_AccountType{
 	
 	public static EP_OrderMapper orderMapper = new EP_OrderMapper();
 	public static EP_OrderItemMapper orderItemMapper = new EP_OrderItemMapper();
 	public static EP_CompanyMapper companyMapper = new EP_CompanyMapper();
 	public static EP_AccountMapper accountMapper = new EP_AccountMapper();
 	
	/**
    *  @description:Method to check if vendor is 3rd party vendor or not
    *  @date      :03/06/2017
    *  @name      :isTransportOrThirdPartyVender
    *  @param     :Account
    *  @return    :boolean
    *  @throws    :NA
    */
    @TestVisible
    private Boolean isTransportOrThirdPartyVender(Account account) {
    	EP_GeneralUtility.Log('Private','EP_Vendor','isTransportOrThirdPartyVender');
    	Boolean isThirdPartyVender = false; 
        Set<String> venderTypes = new Set<String>{EP_AccountConstant.TRANSPORTER_VENDOR_TYPE, EP_AccountConstant.THIRD_PARTY_STOCK_SUPPLIER_VENDOR_TYPE};
        /* TFS fix 45559,45560,45567,45568 start EP_NAV_ID__c deprecated, EP_NAV_Vendor_Id__c is replaced by EP_Source_Entity_ID__c and EP_Vendor_Type__c is replaced by EP_VendorType__c*/
        if(String.isNotBlank( account.EP_VendorType__c ) && venderTypes.contains(account.EP_VendorType__c)) {
            isThirdPartyVender = true;
        }
        /* TFS fix 45559,45560,45567,45568 end*/
        return isThirdPartyVender;
    }   
    
    
	/**
    *  @description: Method to set unique vendor Id
    *  @date      :03/06/2017
    *  @name      :setUniqueVendorId
    *  @param     :Account
    *  @return    :boolean
    *  @throws    :NA
    */
    @TestVisible
    private void setUniqueVendorId(Account vendor){
    	EP_GeneralUtility.Log('Private','EP_Vendor','setUniqueVendorId');
        /* TFS fix 45559 start EP_NAV_Vendor_Id__c is replaced by EP_Source_Entity_ID__c*/ 
        if(vendor.EP_Puma_Company__c != null && String.isNotBlank(vendor.EP_Source_Entity_ID__c)){
            //Company__c company = [Select id, Name, EP_Company_Code__c From Company__c Where Id=: vendor.EP_Puma_Company__c];
            List<Company__c> company = companyMapper.getRecordsByIds(new set<Id>{vendor.EP_Puma_Company__c});
            if(company != null){
                vendor.EP_Vendor_Unique_Id__c = vendor.EP_Source_Entity_ID__c + EP_Common_Constant.STRING_HYPHEN + company[0].EP_Company_Code__c;
            }
            /* TFS fix 45559 end*/ 
        }
    }
    
	 /**  @Description:This method handles trigger's before insert event
      *  @date      :08/03/2017
      *  @name      :doBeforeInsertHandle
      *  @param     :Account
      *  @return    :NA
      *  @throws    :NA
      */	
      public override void doBeforeInsertHandle(Account account){
      	EP_GeneralUtility.Log('Public','EP_Vendor','doBeforeInsertHandle');
      	setUniqueVendorId(account);
      }
      
      
	/**  @Description:This method handles trigger's before update event
      *  @date      :08/03/2017
      *  @name      :doBeforeUpdateHandle
      *  @param     :Account
      *  @return    :NA
      *  @throws    :NA
      */	
      public override void doBeforeUpdateHandle(Account newAccount, Account oldAccount){
      	EP_GeneralUtility.Log('Public','EP_Vendor','doBeforeUpdateHandle');
      	setUniqueVendorId(newAccount);
      }		
}