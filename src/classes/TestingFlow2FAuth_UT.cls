@isTest
private class TestingFlow2FAuth_UT {

    @isTest
	private static void getAccountNames() {
        
        List<String> otps = new List<String>();
        otps.add('Test 1');

        test.startTest();
        
        List<Boolean> retval = TestingFlow2FAuth.getAccountNames(otps);
        
        test.stopTest();
        
        System.assertNotEquals(null, retval, 'Invali data');
        System.assertNotEquals(0, retval.size(), 'Invali data');
	}
	
    @isTest
	private static void getAccountNamesNegative() {
        
        List<String> otps = new List<String>();
        otps.add('Test 1');
        otps.add('Test 2');
        otps.add('Test 3');
        
        test.startTest();
        
        List<Boolean> retval = TestingFlow2FAuth.getAccountNames(otps);
        
        test.stopTest();
        
        System.assertNotEquals(null, retval, 'Invali data');
        System.assertNotEquals(0, retval.size(), 'Invali data');
	}
}