/*
 * Single point of refference for cutoff matrix processing
*/
global class CutOffMatrixAPI {

    /*
     * Enumerate posible validation messages
    */
    global enum CutOffReason { MATRIX_NOT_DEFINED, DATE_IS_OUTSIDE_ALLOWABLE_DELIVERY, VALID, LOCATION_IS_NOT_AVAILABLE }
    
    /*
     * Validatein Helper return value class. Use this to provide complex return results to the client
    */
    global class ValidationHelper {
        
        public DateTime checkedDate { get; set; }
        public CutOffReason reason { get; set; }
        
        public ValidationHelper(DateTime checkedDate, CutOffReason reason) {
            
            this.checkedDate = checkedDate;
            this.reason = reason;
        }
    }

	@RemoteAction
	global static List<ValidationHelper> checkCutOffMatrix(Id basketId, DateTime localDateTime, Integer localGMTOffset, List<DateTime> deliveryDates, Id siteLocation, Id pumaCompany, String deliveryType, Boolean cancelation) {
        System.debug('basketId: ' + basketId);
        System.debug('localDateTime: ' + localDateTime);
        System.debug('localGMTOffset: ' + localGMTOffset);
        System.debug('deliveryDates: ' + deliveryDates);
        System.debug('siteLocation: ' + siteLocation);
        System.debug('pumaCompany: ' + pumaCompany);
        System.debug('deliveryType: ' + deliveryType);

        cscfga__Product_Basket__c basket = (cscfga__Product_Basket__c)csse.SObjectUtils.loadSObject(basketId);

        // Check type and override
        if (siteLocation.getsobjecttype() == Schema.EP_Stock_Holding_Location__c.getSObjectType()) {
            EP_Stock_Holding_Location__c supplyLocation = [SELECT Id, Stock_Holding_Location__c FROM EP_Stock_Holding_Location__c WHERE Id = :siteLocation LIMIT 1];
            siteLocation = supplyLocation.Stock_Holding_Location__c;
        }
        
        Account acc = [SELECT Id, Name, EP_Puma_Company__c,
                            EP_Advance_Order_Entry_Days__c,
                            EP_Puma_Company__r.EP_Advance_Order_Entry_Days__c
                        FROM Account 
                        WHERE Id = :basket.csordtelcoa__Account__c];
        
        if (siteLocation == null) {
            return new List<ValidationHelper> { new ValidationHelper(localDateTime, CutOffReason.LOCATION_IS_NOT_AVAILABLE) };
        }

        return validateCutOffDates(acc, localDateTime, siteLocation, deliveryDates, deliveryType, localGMTOffset, cancelation);
	}

    webservice static Boolean processCutOffMatrixCheck(Id orderId, DateTime localDateTime, Integer localGMTOffset, Boolean cancelation) {
        
        csord__Order__c order = [SELECT Id, EP_Order_Locked__c, Cancellation_Check_Done__c, EP_CutOff_Check_Required__c, csord__Status2__c, 
                                        EP_Order_Epoch__c, EP_Order_Product_Category__c, Stock_Holding_Location__c, Pickup_Supply_Location__c, 
                                        EP_Requested_Delivery_Date__c, Delivery_From_Date__c, Delivery_To_Date__c, csord__Account__c, csord__Account__r.EP_Puma_Company__c,
                                        csord__Account__r.EP_Advance_Order_Entry_Days__c, csord__Account__r.EP_Puma_Company__r.EP_Advance_Order_Entry_Days__c,
                                        EP_Delivery_Type__c
                                    FROM csord__Order__c WHERE Id = :orderId LIMIT 1];
        
	    return processCutOffMatrixCheckUsingOrder(order, localDateTime, localGMTOffset, cancelation);
    }
    
    webservice static String processCutOffMatrixCheckTyped(Id orderId, DateTime localDateTime, Integer localGMTOffset, Boolean cancelation) {
        
        csord__Order__c order = [SELECT Id, EP_Order_Locked__c, Cancellation_Check_Done__c, EP_CutOff_Check_Required__c, csord__Status2__c, 
                                        EP_Order_Epoch__c, EP_Order_Product_Category__c, Stock_Holding_Location__c, Pickup_Supply_Location__c, 
                                        EP_Requested_Delivery_Date__c, Delivery_From_Date__c, Delivery_To_Date__c, csord__Account__c, csord__Account__r.EP_Puma_Company__c,
                                        csord__Account__r.EP_Advance_Order_Entry_Days__c, csord__Account__r.EP_Puma_Company__r.EP_Advance_Order_Entry_Days__c,
                                        EP_Delivery_Type__c
                                    FROM csord__Order__c WHERE Id = :orderId LIMIT 1];

        List<ValidationHelper> results = processCutOffMatrix(order, localDateTime, localGMTOffset, cancelation);

	    return JSON.serialize(results);
    }
    
    webservice static Boolean processCutOffMatrixCheckUsingOrder(csord__Order__c order, DateTime localDateTime, Integer localGMTOffset, Boolean cancelation) {
        
        Boolean result = true;

        List<ValidationHelper> reasons = processCutOffMatrix(order, localDateTime, localGMTOffset, cancelation);
		    
	    for (ValidationHelper helper :reasons) {
	        if (helper.reason !== CutOffReason.VALID) {
	            result = false;
	        }
	    }
	    
	    return result;
    }
    
    /*
     * Process Cutoff matrix, prepare data, suply location, delivery dates and validate result
    */
	private static List<ValidationHelper> processCutOffMatrix(csord__Order__c order, DateTime localDateTime, Integer localGMTOffset, Boolean cancelation) {
	    
	    List<ValidationHelper> reasons = new List<ValidationHelper>();
	    
	    // CutOff Matrix is not relevant for packaged goods or retrospective orders
        if (order.EP_Order_Epoch__c == 'Retrospective' || order.EP_Order_Product_Category__c == 'Packaged') {
            reasons.add(new ValidationHelper(localDateTime, CutOffReason.VALID));
            return reasons;
        }
    
		Id siteLocation = null;

        // Set Site Location, Lookup(Supply Option)
		if (order.EP_Delivery_Type__c.equals(System.Label.Delivery_Type)) {
		    
			siteLocation = String.valueOf(order.Stock_Holding_Location__c);
			
		} else if (order.EP_Delivery_Type__c.equals(System.Label.Ex_Rack_Type) || order.EP_Delivery_Type__c == 'Ex-Rack with Scheduling') {
		    
			siteLocation = String.valueOf(order.Pickup_Supply_Location__c);
			
		} else {
            reasons.add(new ValidationHelper(localDateTime, CutOffReason.LOCATION_IS_NOT_AVAILABLE));
            return reasons;
		}
		
		EP_Stock_Holding_Location__c supplyLocation = [SELECT Id, Stock_Holding_Location__c FROM EP_Stock_Holding_Location__c WHERE Id = :siteLocation];

		List<DateTime> checkDeliveryDates = new List<DateTime>();

        // Construct delivery dates array
		if (order.EP_Requested_Delivery_Date__c != null) {
		    
			checkDeliveryDates.add(order.EP_Requested_Delivery_Date__c);
			
		} else if (order.Delivery_From_Date__c != null && order.Delivery_To_Date__c != null) {
		    
			checkDeliveryDates.add(order.Delivery_From_Date__c);
			checkDeliveryDates.add(order.Delivery_To_Date__c);
			
		} else {
		    
			checkDeliveryDates.add(order.Delivery_From_Date__c);
			checkDeliveryDates.add(order.Delivery_To_Date__c);
		}

        if (checkDeliveryDates.size() == 1) {
            
            DateTime deliveryDateTime = DateTime.newInstanceGmt(order.EP_Requested_Delivery_Date__c, Time.newInstance(00, 00, 00, 00));
            
		    List<ValidationHelper> reasonSingle = validateCutOffDates(order.csord__Account__r, localDateTime, supplyLocation.Stock_Holding_Location__c, deliveryDateTime, order.EP_Delivery_Type__c, localGMTOffset, cancelation);
            reasons.addAll(reasonSingle);
		    
        } else {
            
		    List<ValidationHelper> reasonMultiple = validateCutOffDates(order.csord__Account__r, localDateTime, supplyLocation.Stock_Holding_Location__c, checkDeliveryDates, order.EP_Delivery_Type__c, localGMTOffset, cancelation);
            reasons.addAll(reasonMultiple);
        }

        if (reasons.isEmpty()) {
            reasons.add(new ValidationHelper(localDateTime, CutOffReason.VALID));
        }

		return reasons;
	}
	
	/*
	 * Validate cutoff dates where delivery date is only one element
	*/
	private static List<ValidationHelper> validateCutOffDates(Account account, DateTime submitDateTime, Id suplyLocation, DateTime deliveryDateTime, String deliveryType, Integer timeOffset, Boolean cancelation) {

        List<ValidationHelper> results = new List<ValidationHelper>();

        Time t = deliveryDateTime.timeGmt();
        if (cancelation && t == Time.newInstance(00, 00, 00, 00)) {
            
            System.debug('Important: ' + deliveryDateTime);
            deliveryDateTime = DateTime.newInstanceGmt(deliveryDateTime.dateGmt(), submitDateTime.timeGmt());
            System.debug('Important: ' + deliveryDateTime);
        }

        // Formula for offset is: my time - my offset + matrix offset
	    submitDateTime = submitDateTime.addMinutes(timeOffset * -1);
	    System.debug('Output: off: ' + deliveryDateTime + ', ' + timeOffset);
	    // Formula for offset is: my time - my offset + matrix offset
	    deliveryDateTime = deliveryDateTime.addMinutes(timeOffset * -1);
	    System.debug('Output: off: ' + deliveryDateTime + ', ' + timeOffset);

	    DeliveryDay__c orderDay = fetchLocalDay(account, submitDateTime, suplyLocation, deliveryType);
	    // DeliveryDay__c deliveryDay = fetchDeliveryDay(account, deliveryDateTime, suplyLocation, deliveryType);
	    
	    if (orderDay == null) {
	        results.add(new ValidationHelper(submitDateTime, CutOffReason.MATRIX_NOT_DEFINED));
	        return results;
        }
        
        // Get Matrix depth
        CS_ORDER_SETTINGS__c csOrderSetting = CS_ORDER_SETTINGS__c.getInstance();
        Integer matrixDepth = Integer.valueOf(csOrderSetting.CutoffMatrixDefaultDepth__c);
        
        // Get Matrix Depth
        if (account.EP_Advance_Order_Entry_Days__c != null) {
            matrixDepth = matrixDepth + Integer.valueOf(account.EP_Advance_Order_Entry_Days__c);
        } else if (account.EP_Puma_Company__r.EP_Advance_Order_Entry_Days__c != null) {
            matrixDepth = matrixDepth + Integer.valueOf(account.EP_Puma_Company__r.EP_Advance_Order_Entry_Days__c);
        }
        
        Date submitDate = submitDateTime.date();
        Date deliveryDate = deliveryDateTime.date();
        
        System.debug('Greater then: submitDate: ' + submitDate + ', deliveryDate: ' + deliveryDate + ', diff: ' + submitDate.daysBetween(deliveryDate) + ', matrixDepth: ' + matrixDepth);

        // Delivery date can not be greater then certain amound of days
        if (submitDate.daysBetween(deliveryDate) > matrixDepth) {
            System.debug('Greater then: submitDate: ' + submitDateTime + ', orderDay: ' + orderDay);
            results.add(new ValidationHelper(deliveryDate, CutOffReason.DATE_IS_OUTSIDE_ALLOWABLE_DELIVERY));
            return results;
        }

        // Submit date is checked only if it is a case of cancelation        
        if (!cancelation) {
            
            if (!validateDayTime(submitDateTime, orderDay)) {
                results.add(new ValidationHelper(submitDateTime, CutOffReason.DATE_IS_OUTSIDE_ALLOWABLE_DELIVERY));
                return results;
            }
        }
        
        if (!validateDayTime(deliveryDateTime, orderDay)) {
            results.add(new ValidationHelper(deliveryDateTime, CutOffReason.DATE_IS_OUTSIDE_ALLOWABLE_DELIVERY));
            return results;
        }

        results.add(new ValidationHelper(null, CutOffReason.VALID));
        
	    return results;
	}
	
	/*
	 * Validate cutoff dates where delivery dates are list of dates (Australia implementation)
	*/
	private static List<ValidationHelper> validateCutOffDates(Account account, DateTime submitDateTime, Id suplyLocation, List<DateTime> deliveryDateTimes, String deliveryType, Integer timeOffset, Boolean cancelation) {

        List<ValidationHelper> reasons = new List<ValidationHelper>();

        // Formula for offset is: my time - my offset + matrix offset
	    submitDateTime = submitDateTime.addMinutes(timeOffset * -1);
	    
	    // Formula for offset is: my time - my offset + matrix offset
	    for (DateTime deliveryDateTime :deliveryDateTimes) {
	        
            Time t = deliveryDateTime.timeGmt();
            if (cancelation && t == Time.newInstance(00, 00, 00, 00)) {
                
                System.debug('Important: ' + deliveryDateTime);
                deliveryDateTime = DateTime.newInstanceGmt(deliveryDateTime.dateGmt(), submitDateTime.timeGmt());
                System.debug('Important: ' + deliveryDateTime);
            }

	        deliveryDateTime = deliveryDateTime.addMinutes(timeOffset * -1);
	    }

	    DeliveryDay__c orderDay = fetchLocalDay(account, submitDateTime, suplyLocation, deliveryType);
	    // List<DeliveryDay__c> deliveryDays = fetchDeliveryDays(account, deliveryDateTimes, suplyLocation, deliveryType);
	    
	    if (orderDay == null) {// || deliveryDays == null || deliveryDays.isEmpty()) {
	        
	        reasons.add(new ValidationHelper(submitDateTime, CutOffReason.MATRIX_NOT_DEFINED));
	        return reasons;
        }
        
        // Get Matrix depth
        CS_ORDER_SETTINGS__c csOrderSetting = CS_ORDER_SETTINGS__c.getInstance();
        Integer matrixDepth = Integer.valueOf(csOrderSetting.CutoffMatrixDefaultDepth__c);
        
        // Get Matrix Depth
        if (account.EP_Advance_Order_Entry_Days__c != null) {
            matrixDepth = matrixDepth + Integer.valueOf(account.EP_Advance_Order_Entry_Days__c);
        } else if (account.EP_Puma_Company__r.EP_Advance_Order_Entry_Days__c != null) {
            matrixDepth = matrixDepth + Integer.valueOf(account.EP_Puma_Company__r.EP_Advance_Order_Entry_Days__c);
        }

        Date submitDate = submitDateTime.date();

        // Submit date is checked only if it is a case of cancelation        
        if (cancelation) {
            
            if (!validateDayTime(submitDateTime, orderDay)) {
                System.debug('Greater then: submitDate: ' + submitDateTime + ', orderDay: ' + orderDay);
                reasons.add(new ValidationHelper(submitDateTime, CutOffReason.DATE_IS_OUTSIDE_ALLOWABLE_DELIVERY));
            }
        }

        for (DateTime deliveryDateTime :deliveryDateTimes) {
            
            Date deliveryDate = deliveryDateTime.date();
            System.debug('Greater then: submitDate: ' + submitDate + ', deliveryDate: ' + deliveryDate + ', diff: ' + submitDate.daysBetween(deliveryDate) + ', matrixDepth: ' + matrixDepth);    
            // Delivery date can not be greater then certain amound of days
            if (submitDate.daysBetween(deliveryDate) > matrixDepth) { 
                reasons.add(new ValidationHelper(deliveryDate, CutOffReason.DATE_IS_OUTSIDE_ALLOWABLE_DELIVERY));
            }

            if (!validateDayTime(deliveryDateTime, orderDay)) {
                reasons.add(new ValidationHelper(deliveryDateTime, CutOffReason.DATE_IS_OUTSIDE_ALLOWABLE_DELIVERY));
            }
        }

        if (reasons.isEmpty()) {
            reasons.add(new ValidationHelper(submitDateTime, CutOffReason.VALID));
        }

	    return reasons;
	}

    /*
     * Fetch delivery date using Company, Suply Location and Delivery Type
    */
	private static DeliveryDay__c fetchLocalDay(Account account, DateTime submitDateTime, Id suplyLocation, String deliveryType) {
	    
	    System.debug('EP_Puma_Company__c: ' + account.EP_Puma_Company__c);
	    System.debug('Delivery_Type__c: ' + deliveryType);
	    System.debug('Site_Location__c: ' + suplyLocation);
        
	    List<DeliveryDay__c> deliveryDays = [SELECT Id, Name, CurrencyIsoCode,  Cut_Off_Matrix__c, 
                                                Delivery_Type__c, Friday__c, Monday__c, OrderDayNum__c, Order_Day_Name__c, Saturday__c, 
                                                Site_Location__c, Sunday__c, Thursday__c, Tuesday__c, Wednesday__c, 
                                                Cut_Off_Matrix__r.Site_Location__c, Cut_Off_Matrix__r.Delivery_Type__c,
                                                Cut_Off_Matrix__r.Time_Zone__c, Cut_Off_Matrix__r.Override_matrix__c
                                            FROM DeliveryDay__c 
                                            WHERE Cut_Off_Matrix__r.Puma_Company__c = :account.EP_Puma_Company__c AND Cut_Off_Matrix__r.Delivery_Type__c = :deliveryType AND 
                                                    Cut_Off_Matrix__r.Site_Location__c = :suplyLocation];
        
        // If exist return the first one
        if (!deliveryDays.isEmpty()) {
            System.debug('Override matrix: Delivery days...');
            DeliveryDay__c deliveryDay = deliveryDays[0];
        
            System.debug('Order_Day_Name__c: ' + submitDateTime.formatGmt('EEEE'));
            
            // Add matrix overflow 
            addMatrixOffset(submitDateTime, deliveryDay);
            
            // Get Submit day
    	    String submitDayName = submitDateTime.formatGmt('EEEE');
    	    Date submitDayValue = submitDateTime.dateGMT();
    	    
    	    System.debug('Order_Day_Name__c: ' + submitDayName);

            // 
    	    for (DeliveryDay__c dd :deliveryDays) {
    	        
    	        if (dd.Order_Day_Name__c == submitDayName) {
    	            deliveryDay = dd;
    	            break;
    	        }
    	    }
            
            // Check for overload matrix entriex
            if (deliveryDay.Cut_Off_Matrix__r.Override_matrix__c != null) {
                System.debug('Override matrix: deliveryDay: ' + deliveryDay);
                
                System.debug('Override matrix: deliveryDay: ' + deliveryDay.Cut_Off_Matrix__c);
                System.debug('Override matrix: deliveryDay: ' + deliveryDay.Cut_Off_Matrix__r.Override_matrix__c);
                System.debug('submitDayValue: ' + submitDayValue + ', submitDayName: ' + submitDayName);
                
                
                // Get matrix
                List<Overload_matrix_entry__c> overloadTimes = [SELECT Time__c, Date__c, Order_Date__c FROM Overload_matrix_entry__c 
                                                                    WHERE Overload_matrix__c = :deliveryDay.Cut_Off_Matrix__r.Override_matrix__c AND Order_Date__c = :submitDayValue];
                System.debug('Override matrix: overloadTimes: ' + overloadTimes);
                // Update entries in the delivery day with overloaded values
                for (Overload_matrix_entry__c overload :overloadTimes) {
                    System.debug('Override matrix: matrix entry: ' + overload);
                    Date d = overload.Date__c;
                    Time t = overload.Time__c;
                    System.debug('Override matrix: d: ' + d);
                    System.debug('Override matrix: t: ' + t);
                    System.debug('Override matrix: overloadTimes: ' + d + ' , ' + t);
                    DateTime dt = DateTime.newInstanceGmt(d, t == null ? Time.newInstance(00, 00, 00, 00) : t);
                    System.debug(dt + ' -> ' + dt.formatGmt('EEEE'));
                    System.debug('Override matrix: update delivery day value before: ' + deliveryDay);
                    deliveryDay.put(dt.formatGmt('EEEE') + '__c', t);
                    System.debug('Override matrix: update delivery day value before: ' + deliveryDay);
                }
            }
            
            return deliveryDay;
        }
        
        return null;
	}

    /*
     * Validate DateTime against CutOffMatrix DeliveryDay__c type
    */
	@TestVisible
	private static boolean validateDayTime(DateTime dateInfo, DeliveryDay__c dayInfo) {
	    
	    Time matrixOffset = null;
	    
	    String offsetMatrix = dayInfo.Cut_Off_Matrix__r.Time_Zone__c;
	    System.debug('Time_Zone__c: ' + offsetMatrix);
	    if (!String.isBlank(offsetMatrix)) {
	        
            offsetMatrix = offsetMatrix.replace('GMT ', '');
            offsetMatrix = offsetMatrix.replace(' ', '');
            
            String[] times = offsetMatrix.split(':');
            
            Integer hours = Integer.valueOf(times[0]);
            Integer minutes = Integer.valueOf(times[1]);
            
            Boolean isNegative = hours < 0 ? true: false;

            dateInfo = dateInfo.addHours(hours);
            dateInfo = dateInfo.addMinutes(isNegative ? minutes * -1 : minutes);

            String dayName = dateInfo.formatGmt('EEEE');

            Time matrixTime = (Time)dayInfo.get(dayName + '__c');
            System.debug('Validate day output: ' + dayName + ', matrixTime: ' + matrixTime + ', dateInfo: ' + dateInfo);
            if (matrixTime == null) {
                System.debug('ValidateDayTime: matrix time: false');
                return false;
            }
            
            System.debug('Validate matrixTime > dayInfo: ' + matrixTime + ', dayInfoGMT: ' + dateInfo.timeGMT());
            if (matrixTime >= dateInfo.timeGMT()) {
                System.debug('ValidateDayTime: return: true');
                return true;
            }
	    }
	    
	    System.debug('ValidateDayTime: return: false');
	    return false;
	}
	/*
	@TestVisible
	private static boolean validateDayTime(DateTime submitDay, DateTime dateInfo, DeliveryDay__c dayInfo) {
	    
	    // If the dates are the same calculate with matrix time offset
	    if (submitDay.dateGMT() == dateInfo.dateGMT()) {
	        
	        // Take submit DateTime because it has time component inside
            return validateDayTime(submitDay, dayInfo);
	    }
	    
        String dayName = dateInfo.formatGmt('EEEE');

        Time matrixTime = (Time)dayInfo.get(dayName + '__c');
        System.debug('Validate day output: not the same day: ' + dayName + ', matrixTime: ' + matrixTime + ', dateInfo: ' + dateInfo);
        if (matrixTime == null) {
            System.debug('ValidateDayTime: not the same day: matrix time: false');
            return false;
        }
        
        System.debug('Validate matrixTime > dayInfo: ' + matrixTime + ', dayInfoGMT: ' + dateInfo.timeGMT());
        if (matrixTime >= dateInfo.timeGMT()) {
            System.debug('ValidateDayTime: not the same day: return: true');
            return true;
        }
        
        return false;
	}
	*/
	
	/*
	 * Add matrix GMT offset to the inputed DateTime
	*/
	private static DateTime addMatrixOffset(DateTime dateInfo, DeliveryDay__c dayInfo) {

	    Time matrixOffset = null;
	    
	    String offsetMatrix = dayInfo.Cut_Off_Matrix__r.Time_Zone__c;
	    System.debug('Time_Zone__c: ' + offsetMatrix);
	    if (!String.isBlank(offsetMatrix)) {
	        
            offsetMatrix = offsetMatrix.replace('GMT ', '');
            offsetMatrix = offsetMatrix.replace(' ', '');
            
            String[] times = offsetMatrix.split(':');
            
            Integer hours = Integer.valueOf(times[0]);
            Integer minutes = Integer.valueOf(times[1]);
            
            Boolean isNegative = hours < 0 ? true: false;

            dateInfo = dateInfo.addHours(hours);
            dateInfo = dateInfo.addMinutes(isNegative ? minutes * -1 : minutes);

            return dateInfo;
	    }
	    
	    return dateInfo;
	}
}