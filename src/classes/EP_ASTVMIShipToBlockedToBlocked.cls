/*
*  @Author <Accenture>
*  @Name <EP_ASTVMIShipToBlockedToBlocked>
*  @CreateDate <24/02/2017>
*  @Description <Handles VMI Ship To Account status change from 06-Blocked to 06-Blocked>
*  @Version <1.0>
*/
public class EP_ASTVMIShipToBlockedToBlocked extends EP_AccountStateTransition {

    public EP_ASTVMIShipToBlockedToBlocked() {
        finalState = EP_AccountConstant.BLOCKED;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToBlockedToBlocked','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToBlockedToBlocked','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToBlockedToBlocked','isGuardCondition');
        return true;
    }

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToBlockedToBlocked','doOnExit');

    }
}