@isTest
private class EP_RunMapper_UT{
   
   static List<Account> accList;
   static List<EP_Route__c> ercList;
   @TestSetup
   static void createData() {
        EP_OrderDomainObject ordDomObj = EP_TestDataUtility.getOrderStatePlannedDomainObject();
        EP_AccountMapper accMapper = new EP_AccountMapper();
        csord__Order__c ord = ordDomObj.getOrder();
        Account acc = accMapper.getAccountRecord(ord.EP_ShipTo__r.Id); 
        EP_Route__c erc = EP_TestDataUtility.getRouteObject2(acc);
   }
   
   static void prepareData() {
        accList = [SELECT Id, EP_Puma_Company__r.Id, EP_Puma_Company__r.Name  from Account];
        ercList = [SELECT Id, EP_Route_Name__c  FROM EP_Route__c];
   }
   
   static testMethod void getActiveRunsByRoute_test() {
         prepareData();
        //Order orderObj = EP_TestDataUtility.getSalesOrderPositiveScenario();
        //Id routeId = [SELECT Id FROM EP_Route__c LIMIT 1].id;
        EP_RunMapper localObj = new EP_RunMapper();
        List<EP_Run__c> runRecords = EP_TestDataUtility.getRunObjects2(ercList[0]);    
        Id routeId = runRecords[0].EP_Route__c; 
        Test.startTest();
            list<EP_Run__c> result = localObj.getActiveRunsByRoute(routeId);
        Test.stopTest();
        System.assertEquals(true,result.size()>0);
    }
     static testMethod void getRunList_UT() {
         prepareData();
        //Order orderObj = EP_TestDataUtility.getSalesOrderPositiveScenario();
        //Id routeId = [SELECT Id FROM EP_Route__c LIMIT 1].id;
        EP_RunMapper localObj = new EP_RunMapper();
        List<EP_Run__c> runRecords = EP_TestDataUtility.getRunObjects2(ercList[0]);    
        Id routeId = runRecords[0].EP_Route__c; 
        Test.startTest();
            list<EP_Run__c> result = localObj.getRunList(runRecords);
        Test.stopTest();
        System.assertEquals(true,result.size()>0);
    }
     static testMethod void getRunsByRoute_UT() {
         prepareData();
        //Order orderObj = EP_TestDataUtility.getSalesOrderPositiveScenario();
        //Id routeId = [SELECT Id FROM EP_Route__c LIMIT 1].id;
        EP_RunMapper localObj = new EP_RunMapper();
        List<EP_Run__c> runRecords = EP_TestDataUtility.getRunObjects2(ercList[0]);    
        Id routeId = runRecords[0].EP_Route__c; 
        Test.startTest();
            list<EP_Run__c> result = localObj.getRunsByRoute(routeId);
        Test.stopTest();
        System.assertEquals(true,result.size()>0);
    }
    static testMethod void getRunsByRunId_UT() {
         prepareData();
        //Order orderObj = EP_TestDataUtility.getSalesOrderPositiveScenario();
        //Id routeId = [SELECT Id FROM EP_Route__c LIMIT 1].id;
        EP_RunMapper localObj = new EP_RunMapper();
        List<EP_Run__c> runRecords = EP_TestDataUtility.getRunObjects2(ercList[0]);    
        Id routeId = runRecords[0].EP_Route__c; 
        set<Id> runIdSet = new set<Id>();
        runIdSet.add(routeId); 
        Test.startTest();
            List<EP_Run__c> result = localObj.getRunsByRunId(runIdSet);
        Test.stopTest();
    } 
    static testMethod void getAssociatedOrdersWithRun_UT() {
         prepareData();
        //Order orderObj = EP_TestDataUtility.getSalesOrderPositiveScenario();
        //Id routeId = [SELECT Id FROM EP_Route__c LIMIT 1].id;
        EP_RunMapper localObj = new EP_RunMapper();
        List<EP_Run__c> runRecords = EP_TestDataUtility.getRunObjects2(ercList[0]);    
        Id routeId = runRecords[0].EP_Route__c; 
        set<Id> runIdSet = new set<Id>();
        runIdSet.add(routeId); 
        Test.startTest();
            Set<id> result = localObj.getAssociatedOrdersWithRun(runRecords);
        Test.stopTest();
    } 
    
}
