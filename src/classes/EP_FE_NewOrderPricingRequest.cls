/* 
   @Author <Nicola Tassini>
   @name <EP_FE_NewOrderPricingRequest>
   @CreateDate <23/04/2016>
   @Description <Request for the Pricing API>  
   @Version <1.0>
*/
global with sharing class EP_FE_NewOrderPricingRequest {
    
    // Error list
    global static final Integer ERROR_MISSING_SELLTO = -1;
    global static final Integer ERROR_PARSING_SELLTO = -2;
    global static final Integer ERROR_SELLTOID_NOT_FOUND = -3;
    global static final Integer ERROR_SELLTO_NOT_FOUND = -4;
    global static final Integer ERROR_MISSING_SHIPTO = -5;
    global static final Integer ERROR_MISSING_TERMINAL = -6;
    global static final Integer ERROR_MISSING_REQUEST = -7;
    global static final Integer ERROR_MISSING_ORDERDATE = -8;
    global static final Integer ERROR_MISSING_ORDERTIME = -9;
    global static final Integer ERROR_MISSING_SHIPTOANDTERMINAL= -10;
    global static final Integer ERROR_MISSING_ORDERITEMS = -11;
    global static final Integer ERROR_ORDERITEMS_UNDEFINED = -12;
    global static final Integer ERROR_SHIPTO_NOT_FOUND = -13;
    global static final Integer ERROR_SHIPTO_AND_TERMINAL_NOT_FOUND = -14;
    global static final Integer ERROR_PRODUCTS_DUPLICATES = -15;
    global static final Integer ERROR_PRODUCTS_NOT_ALL_ON_SFDC = -16;
    global static final Integer ERROR_TERMINAL_NOT_FOUND = -17;
    global static final Integer ERROR_MISSING_DELIVERYTYPE = -18;
    global static final Integer ERROR_MISSING_TRUCKPLATE = -19;

    global final static String DESCRIPTION1 = 'Missing SellTo';
    global final static String DESCRIPTION2 = 'SelltoId/SellTo not found';
    global final static String DESCRIPTION3 = 'Missing ShipTo/ShipTo not found';
    global final static String DESCRIPTION4 = 'Missing Terminal/Terminal not found';
    global final static String DESCRIPTION5 = 'Missing Request';
    global final static String DESCRIPTION6 = 'Missing OrderDate';
    global final static String DESCRIPTION7 = 'Missing OrderTime';
    global final static String DESCRIPTION8 = 'Missing ShipTo and Terminal/ShipTo and terminal not found';
    global final static String DESCRIPTION9 = 'Missing OrderItems';
    global final static String DESCRIPTION10 = 'Orderitems Undefined';
    global final static String DESCRIPTION11 = 'ShipTo not found';
    global final static String DESCRIPTION12 = 'Duplicate Products';
    global final static String DESCRIPTION13 = 'Products not on SFDC';
    global final static String DESCRIPTION14 = 'Missing Delivery Type';
    global final static String DESCRIPTION15 = 'Missing Truckplate';
    global final static String DESCRIPTION16 = 'OrderTime can not be null if Slotting in Enabled for Storage Location';
   
    // end
        
    public Id sellToId {get; set;} 

    public Date orderDate {get; set;} 
    public String orderTime {get; set;} 

    public String deliveryType {get; set;}

    public Id shipToId {get; set;}      // If Delivery 
    public Id terminalId {get; set;}    // If ShipTo
    
    public Id draftOrderId {get; set;}
    public Boolean trackPriceForOrder {get; set;}

    public String action {get; set;}
    
    public List<EP_FE_OrderItem> orderItems;

/*********************************************************************************************
     @Author <>
     @name <EP_FE_OrderItem >
     @CreateDate <>
     @Description <  >  
     @Version <1.0>
    *********************************************************************************************/
    global with sharing class EP_FE_OrderItem {
        public Id productId {get; set;}
        public Decimal amount {get; set;}
        public Id tankId {get; set;}
 /*********************************************************************************************
     @Author <>
     @name <EP_FE_OrderItem>
     @CreateDate <>
     @Description <  >  
     @Version <1.0>
    *********************************************************************************************/       
        public EP_FE_OrderItem(Id productId, Decimal amount, Id tankId) {
            this.productId = productId;
            this.amount = amount;
            this.tankId = tankId;
        }       
    } 

    public String truckPlate {get; set;}
    public String customerReferenceNumber {get; set;}
    public String comments {get; set;}

 /*********************************************************************************************
     @Author <>
     @name <EP_FE_NewOrderPricingRequest>
     @CreateDate <>
     @Description <  >  
     @Version <1.0>
    *********************************************************************************************/ 
    global EP_FE_NewOrderPricingRequest() {
        this.orderItems = new List<EP_FE_OrderItem>();
    }
}