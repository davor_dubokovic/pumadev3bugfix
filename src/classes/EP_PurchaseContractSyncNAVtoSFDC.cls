/**
 * @author <Jai Singh>
 * @name <EP_PurchaseContractSyncNAVtoSFDC>
 * @createDate <20/04/2016>
 * @description <This is apex RESTful WebService for Contract Sync from NAV to SFDC>
 * @version <1.0>
 */
@RestResource(urlMapping='/v1/PurchaseContractSync/*')
global without sharing class EP_PurchaseContractSyncNAVtoSFDC{
    
    /**
     * @author <Jai Singh>
     * @description <This is the method that handles HttpPost request for Purchase Contract sync>
     * @name <purchaseContractSync>
     * @date <20/04/2016>
     * @param none
     * @return void
     */
    @HttpPost
    global static void purchaseContractSync(){
        RestRequest request = RestContext.request;
        String requestBody = request.requestBody.toString();
        string resultString = EP_PurchaseContractSyncHandler.createUpdatePurchaseContract(requestBody);
        RestContext.response.responseBody = Blob.valueOf(resultString);
        RestContext.response.addHeader(EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON);
    }
    
}