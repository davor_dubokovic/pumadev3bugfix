/* 
      @Author <Accenture>
       @name <EP_CaseDomainObject_UT>
       @CreateDate <08/05/2018>
       @Description  <This is apex test class for EP_CaseDomainObject> 
       @Version <1.0>
    */
    @isTest
    public class EP_CaseDomainObject_UT{
    
        /**
        * @author Accenture
        * @date 08/05/2018
        * @description create test data for all required object in test methods
        */
        @TestSetup
        static void initData(){
            //create Company
           Company__c tempCompany = EP_TestDataUtility.createCompany('12345');
           tempCompany.EP_Close_Case_Expiration__c = 7;
           insert tempCompany;
           
            //Create Sell To Account
            Account tempSellToAcc = EP_TestDataUtility.createSellToAccount(null,null);
            tempSellToAcc.EP_Puma_Company__c = tempCompany.id;
            tempSellToAcc.EP_Status__c= EP_Common_Constant.STATUS_ACTIVE;
            tempSellToAcc.CurrencyIsoCode = 'AUD';
            insert tempSellToAcc;
            
            //create case record..
            Id CaseRecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Customer Support Case').getRecordTypeId();
            Case tempCase = EP_TestDataUtility.createCase(CaseRecordTypeId);
            tempcase.AccountId = tempSellToAcc.id;
            tempcase.status = 'Open';
            insert tempcase;
        }
        
        /**
        * @author Accenture
        * @date 08/05/2018
        * @description Test to EP_CaseDomainObject constructor.
        */
        static testMethod void EP_CaseDomainObject_Test(){
            map<Id,case> CaseTriggerOldMap = new map<Id,case>();
            list<Case> tempCase = [Select Id,Status from case];
            
            for(case objCase : tempCase){
                objCase.Status = 'Closed';
                CaseTriggerOldMap.put(objCase.id,objCase);
            }
            
            Test.startTest();
                EP_CaseDomainObject CaseDomainObject = new EP_CaseDomainObject(tempCase,CaseTriggerOldMap);
            Test.stopTest();
            //No Asserts requred , As this methdods delegates to other methods
            
        }
        
        /**
        * @author Accenture
        * @date 08/05/2018
        * @description Test to EP_CaseDomainObject constructor.
        */
        static testMethod void doActionBeforeUpdate_Test(){
            map<Id,case> CaseTriggerOldMap = new map<Id,case>();
            list<Case> tempCase = [Select Id,Status from case];
            
            for(case objCase : tempCase){
                objCase.Status = 'Closed';
                CaseTriggerOldMap.put(objCase.id,objCase);
            }
            
            Test.startTest();
                EP_CaseDomainObject CaseDomainObject = new EP_CaseDomainObject(tempCase,CaseTriggerOldMap);
                CaseDomainObject.doActionBeforeUpdate();
            Test.stopTest();
            //No Asserts requred , As this methdods delegates to other methods
            
        }
    }