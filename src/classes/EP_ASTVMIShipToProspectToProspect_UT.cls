@isTest
public class EP_ASTVMIShipToProspectToProspect_UT
{
static final string EVENT_NAME = '01-ProspectTo01-Prospect';
static final string INVALID_EVENT_NAME = '08-ProspectTo08-Rejected';
static testMethod void isTransitionPossible_positive_test() {
    EP_AccountDomainObject obj = EP_TestDataUtility.getAccountPositiveTestScenario();
    EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
    EP_ASTVMIShipToProspectToProspect ast = new EP_ASTVMIShipToProspectToProspect();
    ast.setAccountContext(obj,ae);
    Test.startTest();
    boolean result = ast.isTransitionPossible();
    Test.stopTest();
    System.AssertEquals(false,result);
}
static testMethod void isTransitionPossible_negative_test() {
    EP_AccountDomainObject obj = EP_TestDataUtility.getAccountNegativeTestScenario();
    EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME);
    EP_ASTVMIShipToProspectToProspect ast = new EP_ASTVMIShipToProspectToProspect();
    ast.setAccountContext(obj,ae);
    Test.startTest();
    boolean result = ast.isTransitionPossible();
    Test.stopTest();
    System.AssertEquals(false,result);
}


static testMethod void isRegisteredForEvent_positive_test() {
    EP_AccountDomainObject obj = EP_TestDataUtility.getAccountPositiveTestScenario();
    EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
    EP_ASTVMIShipToProspectToProspect ast = new EP_ASTVMIShipToProspectToProspect();
    ast.setAccountContext(obj,ae);
    Test.startTest();
    boolean result = ast.isRegisteredForEvent();
    Test.stopTest();
    System.AssertEquals(false,result);
}
static testMethod void isRegisteredForEvent_negative_test() {
    EP_AccountDomainObject obj = EP_TestDataUtility.getAccountNegativeTestScenario();
    EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME);
    EP_ASTVMIShipToProspectToProspect ast = new EP_ASTVMIShipToProspectToProspect();
    ast.setAccountContext(obj,ae);
    Test.startTest();
    boolean result = ast.isRegisteredForEvent();
    Test.stopTest();
    System.AssertEquals(false,result);
}


static testMethod void isGuardCondition_positive_test() {
    EP_AccountDomainObject obj = EP_TestDataUtility.getAccountPositiveTestScenario();
    EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
    EP_ASTVMIShipToProspectToProspect ast = new EP_ASTVMIShipToProspectToProspect();
    ast.setAccountContext(obj,ae);
    Test.startTest();
    boolean result = ast.isGuardCondition();
    Test.stopTest();
    System.AssertEquals(true,result);
}
static testMethod void isGuardCondition_negative_test() {
    EP_AccountDomainObject obj = EP_TestDataUtility.getAccountNegativeTestScenario();
    EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME);
    EP_ASTVMIShipToProspectToProspect ast = new EP_ASTVMIShipToProspectToProspect();
    ast.setAccountContext(obj,ae);
    Test.startTest();
    boolean result = ast.isGuardCondition();
    Test.stopTest();
    System.AssertEquals(true,result);
}

  static testMethod void isGuardCondition_negative_route_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getAccountPositiveTestScenario();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
        EP_ASTVMIShipToProspectToProspect ast = new EP_ASTVMIShipToProspectToProspect();
        ast.setAccountContext(obj,ae);
        
        Account account = obj.localAccount;
        Map<Id,EP_Route__c> routeRec = EP_TestDataUtility.getRouteObjects(1);
        EP_Route__c route = routeRec.values()[0];
        route.EP_Company__c = account.EP_Puma_Company__c;
        update route;
        account.EP_Default_Route__c = route.id;
        delete [Select Id FROM EP_Action__c];
        delete [Select Id FROM EP_Route_Allocation__c];
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
}