/**
 *  @Author <Ashok Arora, Jai Singh (Accenture)>
 *  @Name <EP_AcknowledgementWS2>
 *  @CreateDate <05/05/2016>
 *  @Description <This class is rest resource which accepts POST request, it initiates parsing request body>
 *  @Version <1.1>
 */
@RestResource(urlMapping='/v2/AcknowledgementWS2/*')
global without sharing class EP_AcknowledgementWS2 {
    
    /**
     * @author <Ashok Arora, Jai Singh (Accenture)>
     * @name <sendAcknowledgement2>
     * @date <05/05/2016>
     * @description <This method is used to handle Post Requests>
     * @version <1.0>
     * @param 
     * @return void
     */
    @HttpPost
    global static void sendAcknowledgement2() {
        RestRequest request = RestContext.request;
        RestContext.response.addHeader(EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON);
        String sRequestBody = request.requestBody.toString();
        if(String.isNotBlank(sRequestBody)){
            //EP_AcknowledgementHandlerV2.parseAcknowledgement(sRequestBody);
            //EP_AcknowledgementHandler.processAcknowledgement(sRequestBody);
            EP_IntegrationService service = new EP_IntegrationService();
	        string response =service.handleRequest(EP_Common_Constant.COMMON_ACKNOWLEDGEMENT,sRequestBody);
	        RestContext.response.responseBody = blob.valueOf(response);
        }    
    }    
}