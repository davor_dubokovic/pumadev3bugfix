@isTest
public class EP_NAVOrderUpdateHandler_UT
{
    @testSetup static void init() {
        List<EP_CS_InboundMessageSetting__c> lInboundCustomSetting = Test.loadData(EP_CS_InboundMessageSetting__c.sObjectType, 'EP_CS_InboundMessageSettingTestData');
    }
    static testMethod void processRequest_test() {
        EP_NAVOrderUpdateHandler localObj = new EP_NAVOrderUpdateHandler();
        String requestBody = JSON.serialize(EP_TestDataUtility.createNAVOrderUpdateStub());
        
        Test.startTest();
            String result = localObj.processRequest(requestBody);
        Test.stopTest();
        
        System.assertEquals(true, result!=null);
    }
    
    static testMethod void processRequest_NegativeTest() {
        EP_NAVOrderUpdateHandler localObj = new EP_NAVOrderUpdateHandler();
        String requestBody = '{"MSG"}';
        
        Test.startTest();
            String result = localObj.processRequest(requestBody);
        Test.stopTest();
        System.assertEquals(true, EP_NAVOrderUpdateHandler.ackResponseList.size() > 0);
    }
    
    static testMethod void updateOrders_test() {
        EP_NAVOrderUpdateHandler localObj = new EP_NAVOrderUpdateHandler();   
        LIST<EP_NAVOrderUpdateStub.orderWrapper> orderWrapperList = new List<EP_NAVOrderUpdateStub.orderWrapper>{EP_TestDataUtility.createNAVUpdateOrderWrapper()};
        orderWrapperList[0].sfOrder = EP_TestDataUtility.getCurrentOrder();
        // assign dummy value to check update operation
        orderWrapperList[0].sfOrder.Status__c = EP_Common_Constant.delivered;
        
        Test.startTest();
            EP_NAVOrderUpdateHandler.updateOrders(orderWrapperList);
        Test.stopTest();
        
        csord__Order__c orderObj = [SELECT Id, Status__c FROM csord__Order__c WHERE Id =: orderWrapperList[0].sfOrder.id ];
        System.AssertEquals(EP_Common_Constant.delivered,orderObj.Status__c);
    }
    static testMethod void doUpdate_test() {
        csord__Order__c sfOrder = EP_TestDataUtility.getcurrentvmiorder();
        sfOrder.Status__c = EP_Common_Constant.delivered;
        LIST<csord__Order__c> ordersToUpdateList = new List<csord__Order__c>{sfOrder};
        
        Test.startTest();
            EP_NAVOrderUpdateHandler.doUpdate(ordersToUpdateList);
        Test.stopTest();
        
        csord__Order__c orderObj = [SELECT Id, Status__c FROM csord__Order__c WHERE Id =: ordersToUpdateList[0].id ];
        System.AssertEquals(EP_Common_Constant.delivered, orderObj.Status__c);
    }
    static testMethod void doUpdate_ErrorTest() {
        csord__Order__c sfOrder = EP_TestDataUtility.getcurrentvmiorder();
        sfOrder.Status__c = null;
        LIST<csord__Order__c> ordersToUpdateList = New List<csord__Order__c>{sfOrder};
        
        Test.startTest();
            EP_NAVOrderUpdateHandler.doUpdate(ordersToUpdateList);
        Test.stopTest();
        
        csord__Order__c orderObj = [SELECT Id , Status__c FROM csord__Order__c WHERE Id =: ordersToUpdateList[0].id ];
        System.AssertEquals(null, orderObj.Status__c);
        
    }
    static testMethod void processUpsertErrors_test() {
        LIST<EP_NAVOrderUpdateStub.orderWrapper> orderWrapperList = new  LIST<EP_NAVOrderUpdateStub.orderWrapper>{EP_TestDataUtility.createNAVUpdateOrderWrapper()};
        List<Database.Error> errorList = new List<Database.Error>();
        Account[] accts = new List<Account>{new Account(Name=''),new Account()};
        Database.SaveResult[] srList = Database.insert(accts, false);
        if (srList != null && !srList.IsEmpty() && srList[0].getErrors() != null) {
          errorList.add(srList[0].getErrors()[0]);
        }  
        String seqId =  orderWrapperList[0].seqId;
        
        Test.startTest();
            EP_NAVOrderUpdateHandler.processUpsertErrors(errorList,seqId);
        Test.stopTest();
        System.assertEquals(true, EP_NAVOrderUpdateHandler.ackResponseList.size() > 0);
       
    }
    static testMethod void createResponse_test() {
        EP_NAVOrderUpdateHandler localObj = new EP_NAVOrderUpdateHandler();
        LIST<EP_NAVOrderUpdateStub.orderWrapper> orderWrapperList = EP_TestDataUtility.getOrderWrapper();
        EP_NAVOrderUpdateStub.OrderWrapper ordWrp = orderWrapperList[0];
        String seqId = ordWrp.seqId;
        String errorCode = ordWrp.errorCode;
        String errorDescription = ordWrp.errorDescription;
        
        Test.startTest();
            EP_NAVOrderUpdateHandler.createResponse(seqId,errorCode,errorDescription);
        Test.stopTest();
        
        System.AssertEquals(true,seqId!=null);
        System.AssertEquals(true,errorCode!=null);
        System.AssertEquals(true,errorDescription!=null);
        
    }
}