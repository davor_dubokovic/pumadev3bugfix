public with sharing class TrafiguraGetPricesDetailed {

	public static Map<String, Object> doPriceCallDetailed(Map<String, Object> inputMap) {
		Map<String, Object> returnMap = new Map<String, Object>();
		String basketId = (String) inputMap.get('BasketId');
		String newPCB = (String) inputMap.get('priceConsolidationBasis');
		string xmlMessage = '';
		String messageType = 'SEND_ORDER_PRICING_REQUEST';
		string responseBody = '';
		string endPoint = '';
		String outcome = '';

		try {
			String companyCode = (String) inputMap.get('CompanyCode');
			
			EP_CS_OutboundMessageSetting__c msgSetting = EP_CustomSettingsUtil.getOutboundMessageSetting(messageType);
			
			//Retrieves he existing regular price call
			xmlMessage = [select body from Attachment where Name = 'pricingRequest' and ParentId = :basketId][0].body.toString();

			//Updates the previous price call message with the new parameters
			Integer startSeq = xmlMessage.indexOf('<seqId>')+7;
			Integer endSeq = xmlMessage.indexOf('</seqId>');
			String seqId = xmlMessage.substring(startSeq, endSeq);
			xmlMessage = xmlMessage.replace(seqId, '');

			Integer startPCB = xmlMessage.indexOf('<priceConsolidationBasis>')+25;
			Integer endPCB = xmlMessage.indexOf('</priceConsolidationBasis>');
			String oldPCB = xmlMessage.substring(startPCB, endPCB);
			xmlMessage = xmlMessage.replace(oldPCB, newPCB);

			endPoint = msgSetting.End_Point__c.replace(EP_Common_Constant.COMPANY_CODE_URL_TOKAN, companyCode);

			EP_OutboundIntegrationService intService = new EP_OutboundIntegrationService(endPoint, EP_Common_Constant.POST, xmlMessage, msgSetting.EP_Subscription_Key__c);
			EP_IntegrationServiceResult result = EP_OutboundMessageService.doCallOutCS(intService, msgSetting);

			responseBody = result.getResponse().getBody();
			returnMap.put('status', result.getResponse().getStatus());
			outcome = result.getResponse().getStatus();

			if(responseBody.startsWith(EP_Common_Constant.BOM_STR)) {
			   responseBody = responseBody.replaceFirst(EP_Common_Constant.BOM_STR, EP_Common_Constant.BLANK);
			}

			returnMap.put('request', xmlMessage);
			returnMap.put('response', responseBody);
			returnMap.put('outcome', outcome);
		} 
		catch(Exception e) {
			returnMap.put('error', e.getStackTraceString());	
			returnMap.put('errormsg', e.getMessage());
			returnMap.put('outcome', e.getMessage());
		}

		return returnMap;
	}
	
	@Future(callout=true)
    public static void saveAttachments(Id basketId, String request, String response) {
    	System.debug('TrafiguraGetPricesDetailed basketId: '+ basketId);

		List<Attachment> pricingResponseAtt = [
			select id
			from Attachment
			where Name = 'pricingResponseDetailed'
			and ParentId = :basketId
		];
		if (pricingResponseAtt.isEmpty()) {

			/*List<csord__Order__c> ordList = [
				SELECT EP_Puma_Company_Code__c, csord__Identification__c, csord__account__r.ep_price_consolidation_basis__c 
				FROM csord__Order__c 
				WHERE csord__Identification__c = :basketId];

			if(ordList.size() > 0){
				Map<String, Object> inputMap = new Map<String, Object>();

                inputMap.put('BasketId', ordList[0].csord__Identification__c);
                inputMap.put('CompanyCode', ordList[0].EP_Puma_Company_Code__c);
                inputMap.put('priceConsolidationBasis', ordList[0].csord__account__r.ep_price_consolidation_basis__c);

                Map<String, Object> callResult = doPriceCallDetailed(inputMap);
                String outcome = (String)callResult.get('outcome');

                if(outcome == 'OK'){
                    String priceRequest = (String)callResult.get('request');
                    String priceResponse = (String)callResult.get('response');*/

                    List<Attachment> attList = new List<Attachment> ();			
					Attachment attResponse = new Attachment(
						ParentId = basketId,
						Name = 'pricingResponseDetailed',
						Body = Blob.valueOf(response)
					);
					attList.add(attResponse);

					Attachment attRequest = new Attachment(
						ParentId = basketId,
						Name = 'pricingRequestDetailed',
						Body = Blob.valueOf(request)
					);
					attList.add(attRequest);
					System.debug('TrafiguraGetPricesDetailed attRequest: '+ attRequest);
					insert attList;	
                /*}
			}*/
		}
    }	
}