/* 
   @Author........<Amit Singh>
   @name..........<EP_ProductTriggerHelper>
   @CreateDate....<Aug 06,2016>
   @Description...<This class handles requests from EP_ProductTriggerHandler>  
   @Version.......<1.0>
*/
public without sharing class EP_ProductTriggerHelper {
    private static final string CLASSNAME = 'EP_ProductTriggerHelper';
    private static final string CREATE_STANDARD_PRCE = 'createStandardPrice';
    
    /**
    * @Description: This method creates a PRICE BOOK ENTRY record with standard pricebook 
    *               for each product
    *               This method is getting called from EP_ProductTriggerHandler class on 
    *               product insert event. 
    * @Params : newProduct List<Product2> - List of Product records(Trigger.new)
    * @ReturnType : NONE 
    **/
    public static void createStandardPrice(List<Product2> newProduct){
        
        Integer nRows;
        try{
            
            Id standardPriceBookId = Test.isRunningTest() ?
                                        Test.getStandardPricebookId() :
                                        [SELECT Id From Pricebook2 WHERE IsStandard = true limit 1 for update].Id;
            
            List<PricebookEntry> pbeToInsert = new List<PricebookEntry>();
            nRows = EP_Common_Util.getQueryLimit();
            /*GET ACTIVE CURRENCIES IN THE SYSTEM*/
            list<CurrencyType>lCurrencyTypes = [Select id
                                                      ,IsoCode
                                                From CurrencyType
                                                Where isActive = true
                                                limit :nRows
                                               ];
            for(Product2 p : newProduct){
                pbeToInsert.addAll(createPricebookEntry(p, standardPriceBookId,lCurrencyTypes));
            }
            
            if(!pbeToInsert.isEmpty()){
                database.insert(pbeToInsert);
            }
        }Catch(Exception e){
             EP_LoggingService.logHandledException(e, EP_Common_Constant.EPUMA, CREATE_STANDARD_PRCE, CLASSNAME, ApexPages.Severity.ERROR );
        }
    } 
    
    /**
    * @Description: This method craetes new Instance of pricebook Entry object
    * @Params : Product2 prodcut - product Instance 
    *           Id priceBookId - Standard PriceBook Id
    *           list<CurrencyType>lCurrencyTypes - list of currency types           
    * @ReturnType : NONE 
    **/
    private static list<PricebookEntry> createPricebookEntry(Product2 product
                                                             ,Id priceBookId
                                                             ,list<CurrencyType>lCurrencyTypes){
        list<PriceBookEntry> lPriceBookEntries = new list<PriceBookEntry>();
        for(CurrencyType currencyType :  lCurrencyTypes){
                lPriceBookEntries.add(new PriceBookEntry(
                    Product2Id = product.id,
                    Pricebook2ID = priceBookId,
                    UnitPrice = 0,
                    IsActive = true,
                    EP_Attached_to_Standard_Product_List__c =  true,
                    CurrencyISOCode = currencyType.isocode)
                );
        }
        
        return lPriceBookEntries;
    }
    
}