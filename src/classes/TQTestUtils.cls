/* 
     *  TQTestUtils 
     */
public with sharing class TQTestUtils {    

    public final static String COMMUNITY_NAME = 'testCommunity';
    public static final String ACCOUNT_RECORD_TYPE_CUSTOMER = 'Customer';
    public static final String ACCOUNT_RECORD_TYPE_BUSINESS = 'Business Unit';
    public final static String CLIENT_APP_ID = 'guestApp';
    public static final String CUSTOMER_PROFILE_NAME = 'Customer-Guest';

    public static final String BILLING_STREET = 'Am Tierpark 16';
    public static final String BILLING_CITY = 'Cologne';
    public static final String BILLING_POSTALCODE ='54321';
    public static final String BILLING_STATE = 'Nordrhein-Westfalen';
    public static final String BILLING_COUNTRY = 'Germany';
    public static final String ACCOUNT_OBJ = 'Account';
    public static final String ACCOUNT_NAME = 'Test Company';
    public static final String OPP_NAME = 'Test Opp';
    public static final String OPP_STAGENAME = 'Prospecting';
    public static final String ATTACHEMENT_NAME = 'test';
    public static final String ATTACHEMENT_BODY = 'testBody'; 
    public static final String EMAIL_ENCODING_KEY = 'UTF-8';
    public static final String LANG_LOCAL_KEY = 'en_US';
    public static final String LOCALE_SIDKEY = 'en_GB';
    public static final String TIMEZONE_SIDKEY = 'GMT';
    public static final String ID = 'Id';



    /* 
     *  Create Account
     */
    public static Account createAccount(String name) {
        
        Account account = new Account(Name = name,BillingStreet = BILLING_STREET ,BillingCity = BILLING_CITY,
        BillingPostalCode = BILLING_POSTALCODE ,BillingState = BILLING_STATE ,BillingCountry = BILLING_COUNTRY);

        return account;
    }

    /* 
     *  Create Business Account, without parent
     */
    public static Account createBusinessAccount(String name, String email) {
        return createBusinessAccount(name, email, null);
    }

    /* 
     *  Create Business Account
     */
    public static Account createBusinessAccount(String name, String email, Account parent) {
        Map<String, String> accountRecordTypeMap = getRecordTypeMap(ACCOUNT_OBJ);
        Account account = new Account(
            Name = name
        );
        if(parent != null) {
            account.ParentId = parent.Id;
        }
        
        return account;
    }

    /* 
     *  Create List of Accounts
     */
    public static List<Account> createAccounts(Integer i) {
        List<Account> res = new List<Account>();
        for(Integer j = 0; j < i; j++){
            res.add(createAccount('Account'+j));
        }
        return res;
    }

    /* 
     *  Create ticket
     */
    public static Case createTicket(Contact contact) {
        Case ticket = new Case();
        if(contact != null) {
            ticket.ContactId = contact.Id;
        }
        
        return ticket;
    }
  
    /*
     * Create Opportunity
     */
    public static Opportunity buildOpportunity() {
        
        Account account = new Account(Name = ACCOUNT_NAME);
        Database.insert (account);
        
        Opportunity opp = new Opportunity();
        opp.name = OPP_NAME;
        opp.accountid = account.id;
        opp.closedate = date.today();
        opp.stagename = OPP_STAGENAME;
        
        return opp;
    }
    
    /*
     *  Create attachment
     */
    public static Attachment createAttachment(Id parentId) {
        Attachment attachment = new Attachment();
        attachment.ParentId = parentId;
        attachment.Name = ATTACHEMENT_NAME;
        attachment.Body = Blob.valueOf(ATTACHEMENT_BODY);
        
        return attachment;
    }
    
    /*
     *  Create contact
     */
    public static Contact createContact(String firstName, String lastName, String email) {
        return createContact(firstName, lastName, email, null);
    }

    /*
     *  Create contact
     */
    public static Contact createContact(String firstName, String lastName, String email, Id accountId) {
        Contact contact = new Contact();
        contact.firstName = firstName;
        contact.lastName = lastName;
        contact.Email = email;
        
        if(accountId != null) {
            contact.AccountId = accountId;
        }
        
        return contact;
    }

    /*
     *  Create User
     */
    public static User createUser(String personAccountId, String firstName, String lastName, String username, String email) {
       List<Contact> personContacts = [SELECT Id FROM Contact WHERE AccountId = :personAccountId limit 20000];
        if (personContacts == null || personContacts.size() == 0){
            return null;
        } else {
            String personContactId = personContacts.get(0).Id;
            String profileName = CUSTOMER_PROFILE_NAME;

            List<Profile> profiles = [SELECT Id FROM Profile WHERE Name = :profileName limit 2000];
            Profile customerProfile = profiles != null && profiles.size() > 0 ? profiles.get(0) : null; 

            return (customerProfile != null) 
                ? new User(
                    firstName = firstName,
                    lastName = lastName,
                    username = username,
                    email = email,
                    alias = userName.substring(0, 7),
                    emailencodingkey = EMAIL_ENCODING_KEY,
                    languagelocalekey = LANG_LOCAL_KEY, 
                    localesidkey = LOCALE_SIDKEY, 
                    timezonesidkey = TIMEZONE_SIDKEY,
                    ProfileId = customerProfile.Id,
                    ContactId = personContactId
                )
                : null;
        }
    }

    /*
     *  Get the Community ID
     */
    public static Id getCommunityID() {
        sObject currentNetwork;
        Id networkId = null;
        try {
            currentNetwork = Database.query('SELECT Id FROM Network WHERE Name = \'' + COMMUNITY_NAME + '\'');
        } catch(Exception ex){
            boolean exc = true;
        } finally {
            networkId = (currentNetwork != null) ? (Id)(currentNetwork.get('Id')) : null;
        }
        return networkId;
    }
    
    /*
     * Create a list of TQUploadRequestItem for tetsing BatchDml option
     */
    public static List<TQUploadRequestItem> createTQUploadRequestItemList(Integer i){
        List<TQUploadRequestItem> res = new List<TQUploadRequestItem>();
        
        for(Integer j = 0; j < i; j++){
            res.add(new TQUploadRequestItem(
                'Contact', 'Local_Id_'+j,
                '{"Id" : "Local_Id_'+j+'", "Name" : "Name'+j+'", "LastName": "Surname'+j+'", "AccountId" : "Local_Id"}'
            ));
        }
        
        return res;
    }
    
    /*
     * Create a list of TQUploadRequestItem from list of sObjects, should be list of inserted objects for delete or update
     * params = '"IsDeleted" : true'
     */
    public static List<TQUploadRequestItem> createTQUploadRequestItemList(List<SObject> objList, String objectType, String params){
        List<TQUploadRequestItem> res = new List<TQUploadRequestItem>();
        
        for(SObject o : objList){
            res.add(new TQUploadRequestItem(
                objectType, String.valueOf(o.get(ID)),
                '{"Id" : "' + o.get(ID) + '", '+ params +'}'
            ));
        }
        
        return res;
    }

    
    /*
     * get the record type of an object
     */
    public static Map<String, String> getRecordTypeMap(String objectApiName) {
        Map<String,String> accountRecordTypesMap = new Map<String,String>{};
        
        for(RecordType rt : [Select Name, Id From RecordType where sObjectType=:objectApiName and isActive=true limit 20000]) {
            accountRecordTypesMap.put(rt.Name, rt.Id);
        }
        return accountRecordTypesMap;
    }
}