/**
 * @author <Arpit Sethi>
 * @name <EP_PortalOrderHelper>
 * @createDate <08/01/2016>
 * @description <This class is a helper class for Front end work> 
 * @version <1.0>
 */
public with sharing class EP_PortalOrderHelper {
    
    public static Boolean checkSellToHasBillTo = true;
    public static Boolean checkBillTo = false;
    private static final String LOCAL_SUPPORT_NUMBER = 'returnLocalSupportNumbers';
    private static final String LOCAL_SUPPORT_ADDRESS = 'returnLocalSupportAddress';
    /**
     * @author Pooja Dhiman
     * @date 08/01/2016
     * @description This method validates Invoices which are overdue.
     */
    public static Boolean validateOverdueInvoice(String currentRecordId) {
        Boolean isPendingInvoices = false;
        Account sellToOrBillToAccount;
        
        Account acc = [SELECT EP_Bill_To_Account__c, RecordTypeId FROM Account 
                          WHERE Id=:currentRecordId LIMIT :EP_Common_Constant.ONE];
        try{
        if(acc != NULL) {
            Id sellToRecordType = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.SELL_TO);
            Id billToRecordType = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.BILL_TO);
            
            if(acc.recordTypeId == sellToRecordType) {
                if(acc.EP_Bill_To_Account__c == NULL) {
                    checkSellToHasBillTo = false;
                    sellToOrBillToAccount = acc;
                } else {
                    checkSellToHasBillTo = true;
                    sellToOrBillToAccount = new Account(Id=acc.EP_Bill_To_Account__c);
                }
            }
            else if(acc.recordTypeId == billToRecordType) {
                checkBillTo = true;
                sellToOrBillToAccount = acc;
            }
            else{}
        }
        
        if(sellToOrBillToAccount != NULL) {
            List<EP_Invoice__c> invoices = [SELECT EP_Bill_To__c FROM EP_Invoice__c 
                                        WHERE EP_Bill_To__c=:sellToOrBillToAccount.Id AND EP_Overdue__c=TRUE LIMIT :EP_Common_Constant.ONE];
            if(invoices != NULL && !invoices.isEmpty()) {
                isPendingInvoices = true;
            }
        }
        }catch(Exception ex){
            EP_LoggingService.logHandledException (ex, EP_Common_Constant.EPUMA, EP_Common_Constant.VALIDATEOVERDUEINVOICEMETHOD, EP_Common_Constant.CLASSNAME, ApexPages.Severity.ERROR);
            
        }
        return isPendingInvoices;
    }
    
    /**
     * @author: Pooja Dhiman
     * @date 02/02/2016
     * @description: This method returns Customer Support Number based on Running user company code.
     */
    public static List<supportNumberClass> returnLocalSupportNumbers() {
        List<supportNumberClass> localSupportNumbers = new List<supportNumberClass>();
        
        User currentUser = [SELECT ContactId, Contact.Account.EP_Puma_Company_Code__c 
                                FROM User WHERE Id = :UserInfo.getUserId() LIMIT :EP_Common_Constant.ONE];
        try{
            if (currentUser.ContactId != NULL) 
            {   
                Map<String, EP_Customer_Support_Settings__c> localSupportNumbersMap = EP_Customer_Support_Settings__c.getAll();
                supportNumberClass supportNoClass = null;
                for(EP_Customer_Support_Settings__c instance : localSupportNumbersMap.values()) 
                {
                    if(instance.EP_Company_Code__c == currentUser.Contact.Account.EP_Puma_Company_Code__c) 
                    {   
                        supportNoClass = new supportNumberClass(instance.Name, instance.EP_Phone__c);
                        localSupportNumbers.add(supportNoClass);
                    } // End company code check
                } // End for
            } // End portal user check
        }
        catch (exception exp){
            EP_LoggingService.logHandledException (exp, EP_Common_Constant.EPUMA,LOCAL_SUPPORT_NUMBER, EP_Common_Constant.CLASSNAME, ApexPages.Severity.ERROR);  
        }
        return localSupportNumbers;
    }
    
    /**
     * @author: Spiros Markantonatos
     * @date 12/07/2016
     * @description: This method returns Customer Support Address based on Running user company code.
     */
    public static supportAddressClass returnLocalSupportAddress() {
        supportAddressClass localSupportAddress = new supportAddressClass();
        
        User currentUser = [SELECT ContactId, Contact.Account.EP_Puma_Company__r.EP_Company_Street__c,
                                 Contact.Account.EP_Puma_Company__r.EP_Company_City__c,
                                 Contact.Account.EP_Puma_Company__r.EP_Company_State__c,
                                 Contact.Account.EP_Puma_Company__r.EP_Company_Post_Code__c,
                                 Contact.Account.EP_Puma_Company__r.EP_Company_Country__r.Name
                                    FROM User WHERE Id = :UserInfo.getUserId() LIMIT :EP_Common_Constant.ONE];
        try{
            if (currentUser.ContactId != NULL) 
            {
                localSupportAddress.supportStreetName = currentUser.Contact.Account.EP_Puma_Company__r.EP_Company_Street__c;
                localSupportAddress.supportCityName = currentUser.Contact.Account.EP_Puma_Company__r.EP_Company_City__c;
                localSupportAddress.supportStateName = currentUser.Contact.Account.EP_Puma_Company__r.EP_Company_State__c;
                localSupportAddress.supportPostCodeName = currentUser.Contact.Account.EP_Puma_Company__r.EP_Company_Post_Code__c;
                localSupportAddress.supportCountryName = currentUser.Contact.Account.EP_Puma_Company__r.EP_Company_Country__r.Name;
            } // End portal user check
        }
        
        catch (exception exp){
            EP_LoggingService.logHandledException (exp, EP_Common_Constant.EPUMA,LOCAL_SUPPORT_ADDRESS, EP_Common_Constant.CLASSNAME, ApexPages.Severity.ERROR);    
        }
        return localSupportAddress;
    }
    
    /**
     * Inner Class
    */
    public without sharing class supportAddressClass {
        public String supportStreetName {get;set;}
        public String supportCityName {get;set;}
        public String supportStateName {get;set;}
        public String supportPostCodeName {get;set;}
        public String supportCountryName {get;set;}
    }
    
    /**
     * Inner Class
    */
    public without sharing class supportNumberClass {
        public String supportNumberName {get;set;}
        public String supportNumber {get;set;}
        /*
         Inner Class Constructor
        */
        public supportNumberClass(String strName, String strPhoneNumber) {
            this.supportNumberName = strName;
            this.supportNumber = strPhoneNumber;
        }
    }
}