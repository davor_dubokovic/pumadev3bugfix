/* 
   @Author          Accenture
   @name            EP_StockLocationCreateUpdateHelper
   @CreateDate      15/12/2017
   @Description     Transformation class used to populate lookups and other calculated fields in the wrapper instance
   @Version         1.0
*/
public class EP_StockLocationCreateUpdateHelper {
	
	/**
    * @author           Accenture
    * @name             setStockHoldingAttributes
    * @date             15/12/2017
    * @description      Will process the stock Location inbound request and sends back the response
    * @param            string
    * @return           string
    */
	public static list<Account> setStockLocationAttributes(list<EP_StockLocationStub.InventoryLoc> lstStockLoc){
		EP_GeneralUtility.Log('public','EP_StockLocationCreateUpdateHelper','setStockLocationAttributes'); 
		list<Account> lstAccount = new list<Account>();
		Account objStockLocation;
		map<string,Company__c> mapCompanyCodeCompany = getCompanyMap(lstStockLoc);
		for(EP_StockLocationStub.InventoryLoc objStockLoc :lstStockLoc){ 
			objStockLocation = new Account();
			objStockLocation.recordtypeid 						= getRecordTypeId();
			objStockLocation.EP_NavSeqId__c 					= objStockLoc.seqId;
			objStockLocation.EP_Location_Type__c 	    		= objStockLoc.locType;
			objStockLocation.Name 								= objStockLoc.name;
			objStockLocation.EP_Account_Name_2__c 				= objStockLoc.name2;
			objStockLocation.ShippingStreet 					= objStockLoc.address;
			objStockLocation.ShippingCity 						= objStockLoc.city;
			objStockLocation.ShippingPostalCode					= objStockLoc.postCode;
			objStockLocation.ShippingState 						= objStockLoc.county;
			objStockLocation.ShippingCountry 					= objStockLoc.countryCode;
			objStockLocation.Phone 								= objStockLoc.phone;
			objStockLocation.Fax 								= objStockLoc.fax;
			objStockLocation.EP_Email__c 						= objStockLoc.email;
			objStockLocation.Is_3rd_Party_Supply_Location__c	= objStockLoc.isThirdParty;
			objStockLocation.EP_Ship_To_UTC_Timezone__c	 		= objStockLoc.timeZone;
			objStockLocation.EP_Version_Number__c				= objStockLoc.versionNr;
			objStockLocation.EP_Nav_Stock_Location_Id__c		= objStockLoc.identifier.locationId;
			objStockLocation.EP_Puma_Company__c					= getCompany(mapCompanyCodeCompany,objStockLoc.identifier.companyCode);
			objStockLocation.EP_Status__c						= getStatus(objStockLoc.isBlocked); 
			objStockLocation.EP_Enterprise_ID__c				= objStockLoc.identifier.entrprsId;
			objStockLocation.EP_Storage_Location_External_ID__c	= generateExternalIdForStockLocation(objStockLoc);
			lstAccount.add(objStockLocation);
		}
		return lstAccount;
	}
	
	/**
    * @author           Accenture
    * @name             getRecordTypeId
    * @date             15/12/2017
    * @description      This method is used to get Record type id for Storage Location record type of Account
    * @param            string
    * @return           string
    */
    public static string getRecordTypeId(){ 
        EP_GeneralUtility.Log('Public','EP_StockLocationCreateUpdateHelper','getRecordTypeId');
        return Schema.SObjectType.Account.getRecordTypeInfosByName().get(EP_Common_Constant.STORAGELOCATION).getRecordTypeId();
    }
	   
    /**
    * @author           Accenture
    * @name             getCompanyMap
    * @date             15/12/2017
    * @description      This method is used to get company record based on company code.
    * @param            string
    * @return           string
    */
    public static map<string,Company__c>  getCompanyMap(list<EP_StockLocationStub.InventoryLoc> lstStockLoc){
        EP_GeneralUtility.Log('Public','EP_StockLocationCreateUpdateHelper','getCompanyMap');
        set<string>serCompanyCode = new set<string>();
        for(EP_StockLocationStub.InventoryLoc objStockLoc :lstStockLoc){ 
        	serCompanyCode.add(objStockLoc.identifier.companyCode);
        }
        EP_CompanyMapper objCompanyMapper = new  EP_CompanyMapper();
        map<string,Company__c> mapCompanyCodeCompany  =objCompanyMapper.getCompanyDetailsByCompanyCodes(serCompanyCode);
        return mapCompanyCodeCompany;
    }
    
    /**
    * @author           Accenture
    * @name             getCompany
    * @date             15/12/2017
    * @description      This method is used to get company record based on company code.
    * @param            string
    * @return           string
    */ 
    public static string getCompany( map<string,Company__c> mapCompanyCodeCompany,string companyCode){
        EP_GeneralUtility.Log('Public','EP_StockLocationCreateUpdateHelper','getCompany');
        return mapCompanyCodeCompany.containskey(companyCode)?mapCompanyCodeCompany.get(companyCode).id:EP_Common_Constant.BLANK;
    }
    /**
    * @author           Accenture
    * @name             getStatus
    * @date             15/12/2017
    * @description      This method is used to set the value of status based on Blocked tag
    * @param            string
    * @return           string
    */
    public static string getStatus(string strStatus){
        EP_GeneralUtility.Log('Public','EP_StockLocationCreateUpdateHelper','getStatus');
        String statusValue =EP_Common_Constant.BLANK;
        if(!string.isblank(strStatus)){
        	statusValue =strStatus.equalsignorecase(EP_Common_Constant.YES) ?EP_Common_Constant.STATUS_DEACTIVETE:EP_Common_Constant.STATUS_ACTIVE;
        }
        return statusValue; 
    }
    
    /**
    * @author           Accenture
    * @name             generateExternalIdForStockLocation
    * @date             15/12/2017
    * @description      This method is used to generate externalid for StorageLocation
    * @param            string,sting
    * @return           string
    */
    public static string generateExternalIdForStockLocation(EP_StockLocationStub.InventoryLoc objStockLoc){
        EP_GeneralUtility.Log('Public','EP_StockLocationCreateUpdateHelper','generateExternalIdForStockLocation');
        return (objStockLoc.identifier.locationId + EP_Common_Constant.UNDERSCORE_STRING +objStockLoc.identifier.companyCode);        
    }
}