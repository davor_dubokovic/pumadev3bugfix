@isTest
public class EP_NonVMIShipToASBasicDataSetup_UT
{
    static final string EVENT_NAME = '02-BasicDataSetupTo02-BasicDataSetup';
    static final string INVALID_EVENT_NAME = '08-RejectedTo02-BasicDataSetup';
    /*  
    @description: method to intialise data
    */
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
        List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
    }
    static testMethod void setAccountDomainObject_test() {
        EP_NonVMIShipToASBasicDataSetup localObj = new EP_NonVMIShipToASBasicDataSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.setAccountDomainObject(obj);
        Test.stopTest();
        system.assertEquals(obj.getAccount().Id,localObj.account.getAccount().Id);
    }
    static testMethod void doOnEntry_test() {
        //Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
        //Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        EP_NonVMIShipToASBasicDataSetup localObj = new EP_NonVMIShipToASBasicDataSetup ();
        Account newAccount = EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectPositiveScenario().getAccount();
        Account oldAccount = newAccount.clone();
        oldAccount.EP_Status__c = EP_AccountConstant.BASICDATASETUP;
        EP_AccountDomainObject obj = new EP_AccountDomainObject(newAccount, oldAccount);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        System.AssertEquals(EP_AccountConstant.BASICDATASETUP,oldAccount.EP_Status__c);         
    }
    //L4_Start
    static testMethod void doOnEntry1_test() {
        EP_NonVMIShipToASBasicDataSetup localObj = new EP_NonVMIShipToASBasicDataSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectPositiveScenario();
        account acc = obj.getAccount();
        acc.EP_Synced_NAV__c = true;
        acc.EP_Synced_PE__c = true;
        acc.EP_Synced_WINDMS__c = true;
        acc.EP_Status__c = '02-Basic Data Setup';
        update acc;
                
        EP_AccountService service = new EP_AccountService(obj);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        //Method has no implementation, hence adding dummy assert
        system.assert(true);
    }
    
    static testMethod void doOnEntry2_test() {
        EP_NonVMIShipToASBasicDataSetup localObj = new EP_NonVMIShipToASBasicDataSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectPositiveScenario();
        account acc = obj.getAccount();
        /*account parent = [select id from account where id = :acc.id];
        parent.EP_Synced_NAV__c = true;
        parent.EP_Synced_PE__c = true;
        parent.EP_Synced_WINDMS__c = true;
        parent.EP_Status__c = '02-Basic Data Setup';
        update parent;*/
           
        acc.EP_Synced_NAV__c = true;
        acc.EP_Synced_PE__c = true;
        acc.EP_Synced_WINDMS__c = true;
        update acc; 
        
        acc.EP_Synced_WINDMS__c = false;
        update acc;  
           
        EP_AccountService service = new EP_AccountService(obj);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        //Method has no implementation, hence adding dummy assert
        system.assert(true);
    }
    //L4_End
    
    //Method has no implementation, hence adding dummy assert   
    static testMethod void doOnExit_test() {
        EP_NonVMIShipToASBasicDataSetup localObj = new EP_NonVMIShipToASBasicDataSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest(); 
        System.assertEquals(true, true); 
    }
    static testMethod void doTransition_PositiveScenariotest() {
        EP_NonVMIShipToASBasicDataSetup localObj = new EP_NonVMIShipToASBasicDataSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void doTransition_NegativeScenariotest() {
        EP_NonVMIShipToASBasicDataSetup localObj = new EP_NonVMIShipToASBasicDataSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectNegativeScenario();
        EP_AccountEvent oe = new EP_AccountEvent(INVALID_EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        string message;
            Test.startTest();
        try{
            Boolean result = localObj.doTransition();
        }catch(Exception e){
            message = e.getMessage();
        }
        Test.stopTest();
        System.assert(string.isNotBlank(message));
    }
    static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
        EP_NonVMIShipToASBasicDataSetup localObj = new EP_NonVMIShipToASBasicDataSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.isInboundTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    /** 
        This isInboundTransitionPossible() method is returning only true for all cases.
        So only positive scenario applicable. 
    **/
}