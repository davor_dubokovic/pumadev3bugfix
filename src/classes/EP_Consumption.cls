/**
  * @Author      : Kalpesh Thakur<kalpesh.j.thakur@accenture.com>
  * @name        : EP_ExRack
  * @CreateDate  : 05/02/2017
  * @Description : This class executes logic for Delivery
  * @Version     : <1.0>
  * @reference   : N/A
  */
  public with sharing class EP_Consumption extends EP_DeliveryType  {
    
    private static final String exRackInventoryCheck_Method = 'exRackInventoryCheck';
    private static final String deliveryInventoryCheck_Method = 'deliveryInventoryCheck';
    private static final String ClassName = 'EP_Consumption';
    
    
    public override void updateTransportService(csord__Order__c objOrder) {
      EP_GeneralUtility.Log('Public','EP_Consumption','updateTransportService');
      
      super.updateTransportService(objOrder);
    }


   

    /** This method is used to get price book entries
      *  @date      05/02/2017
      *  @name      findPriceBookEntries
      *  @param     Id pricebookId,Id storageLocId,Order objOrder
      *  @return    List<PriceBookEntry>
      *  @throws    NA
      */ 
      public override List<PriceBookEntry> findPriceBookEntries(Id pricebookId,csord__Order__c objOrder) {
        EP_GeneralUtility.Log('Public','EP_Consumption','findPriceBookEntries');
        
        return new EP_PriceBookEntryMapper().getRecordsByFilterSet_1(new Set<Id>{pricebookId},new Set<String>{objOrder.CurrencyIsoCode}, new Set<String>{objOrder.EP_Order_Product_Category__c},true);
      }
      
      
      
    /** This method is used set the visbility of Customer PO field on portal order page
      *  @date      05/02/2017
      *  @name      showCustomerPO
      *  @param     csord__Order__c orderObject
      *  @return    Boolean
      *  @throws    NA
      */ 
      public override Boolean showCustomerPO(csord__Order__c orderObject){
        EP_GeneralUtility.Log('Public','EP_Consumption','showCustomerPO');
        return orderObject.csord__Account__r.EP_Is_Customer_Reference_Visible__c;
      }
      


    /** This method returns getShiptos for the AccountId
      *  @date      05/02/2017
      *  @name      getShipTos
      *  @param     Id accountId
      *  @return    List<> 
      *  @throws    NA
      */
      public override List<Account> getShipTos(Id accountId) {
        EP_GeneralUtility.Log('Public','EP_Consumption','getShipTos');

        List<Account> lstShipTos = EP_AccountMapper.getAllShipTos(accountId);

        return lstShipTos;
      }


    /** This method is used to get operational tanks on the selected ship to
      *  @date      05/02/2017
      *  @name      getOperationalTanks
      *  @param     String strSelectedShipToId
      *  @return    Map<ID, EP_Tank__c> 
      *  @throws    NA
      */ 
      public override Map<ID, EP_Tank__c> getOperationalTanks(String strSelectedShipToId){         
       Map<ID, EP_Tank__c> mapShipToTanks = EP_TankMapper.getOperationalRecordsByShipTo( strSelectedShipToId );
       return mapShipToTanks;
     } 

     /** This method is used to get tanks on the selected ship to
      *  @date      05/02/2017
      *  @name      getTanks
      *  @param     String strSelectedShipToId
      *  @return    Map<ID, EP_Tank__c> 
      *  @throws    NA
      */ 
      public override Map<ID, EP_Tank__c> getTanks(String strSelectedShipToId){         
       Map<ID, EP_Tank__c> mapShipToTanks = EP_TankMapper.getRecordsByShipTo( strSelectedShipToId );
       return mapShipToTanks;
     }        
   }