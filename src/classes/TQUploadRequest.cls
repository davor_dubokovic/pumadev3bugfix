/*********************************************************************************************
            @Author <Soumya Raj>
            @name <TQUploadRequest >
            @CreateDate <>
            @Description < >  
            @Version <1.0>
    *********************************************************************************************/
global with sharing class TQUploadRequest {
    public List<String> recordList {get; set;}
    
/*********************************************************************************************
            @Author <Soumya Raj>
            @name <TQUploadRequest >
            @CreateDate <>
            @Description < >  
            @Version <1.0>
    *********************************************************************************************/
    public TQUploadRequest(){
        recordList = new List<String>();
    }
    
    /*global class TQUploadRequestItem {
        public String objectApiName {get; set;}
        public Map<String, String> record {get; set;}
        
        public Map<String, String> getRecord(){
            return record;
        }
    
        public String getObjectApiName(){
            return objectApiName;
        }   
    }*/
}