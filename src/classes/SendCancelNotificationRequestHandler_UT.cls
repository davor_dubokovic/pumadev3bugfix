/***
 * Author: Pawan Srivastava
 * Description: Test Class for SendCancelNotificationRequestHandler.
 * Version History:
 * v1 - Created on 2018-08-17
 ***/
@isTest
public class SendCancelNotificationRequestHandler_UT {
    
    @testSetup
    public static void createTestData() {
        Account acc = new Account();
        acc.EP_VendorType__c = '3rd Party Stock Supplier';
        acc.EP_Vendor_Type__c = '3rd Party Stock Supplier';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        acc.Name='testEPAccount';
        acc.EP_Status__c = '06-Blocked';
        insert acc;
        
        System.Assert(acc.Id != null);
        csord__Order__c ord = new csord__Order__c(Name='test order',
                                                  csord__Account__c=acc.Id,
                                                  AccountId__c = acc.id,
                                                  csord__Status__c='Order Submitted',
                                                  csord__Identification__c='testorder');
        insert ord;
        System.Assert(ord.Id != null);
         
        CSPOFA__Orchestration_Process_Template__c oProcessTemplate = new CSPOFA__Orchestration_Process_Template__c(Name='test template');
        insert oProcessTemplate;
        
        System.Assert(oProcessTemplate.Id != null);
        CSPOFA__Orchestration_Process__c oProcess = new CSPOFA__Orchestration_Process__c(Name='test process', 
                                                                                         CSPOFA__Orchestration_Process_Template__c=oProcessTemplate.Id,
                                                                                         Order__c=ord.Id
                                                                                        );
        insert oProcess;

        System.Assert(oProcess.Id != null);
        CSPOFA__Orchestration_Step__c oStep = new CSPOFA__Orchestration_Step__c(Name='test step',
                                                                                CSPOFA__Orchestration_Process__c=oProcess.Id
                                                                               );
        insert oStep;
    }
    
    public static testmethod void testSendCancelNotificationRequestHandler() {
        
        list<SObject> steps = [select Id from CSPOFA__Orchestration_Step__c];        
        
        test.startTest();        
        System.assert(steps.size() > 0, 'Invalid data'); 
        SendCancelNotificationRequestHandler sendCancelNotification = new SendCancelNotificationRequestHandler();  
        sendCancelNotification.process(steps);		
        test.stopTest();        
        
    }
}