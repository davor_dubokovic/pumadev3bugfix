/* 
   @Author <Accenture>
   @name <EP_PricingResponseWS>
   @CreateDate <03/03/2017>
   @Description <This is apex RESTful WebService for Pricing Sycn from NAV> 
   @Version <1.0>
*/
@RestResource(urlMapping ='/v1/PricingResponse/*' )
global with sharing class EP_PricingResponseWS {
    /*
        Method For PRICINGSync
    */ 
    @HttpPost
    global static void processResuest(){
        EP_GeneralUtility.Log('global','EP_PricingResponseWS','processRequest');
        RestRequest request = RestContext.request; 
        RestContext.response.addHeader(EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON);
        String requestBody = request.requestBody.toString();
        EP_IntegrationService service = new EP_IntegrationService();
        string response =service.handleRequest(EP_Common_Constant.NAV_PRICING_RESPONSE,requestBody); 
        RestContext.response.responseBody = Blob.valueOf(response);
    }
}