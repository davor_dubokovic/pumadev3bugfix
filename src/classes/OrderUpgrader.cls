global class OrderUpgrader implements Database.Batchable<sObject> {
    
    global String productDefinitionName = 'Puma Energy Order';
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('SELECT Id, csordtelcoa__Product_Configuration__c FROM csord__Order__c WHERE csordtelcoa__Product_Configuration__c != NULL AND csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__r.Name != :productDefinitionName');
    }
    
    global void execute(Database.BatchableContext bc, List<csord__Order__c> scope) {
        
        List<Id> productConfigurationIds = new List<Id>();
        
        for (csord__Order__c obj : scope) {
            productConfigurationIds.add(obj.csordtelcoa__Product_Configuration__c);
        }
        
        cfgug1.ProductConfigurationUpgrader.upgradeConfigurations(productConfigurationIds);
    }    
    
    global void finish(Database.BatchableContext bc) {

        AsyncApexJob job = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id = :bc.getJobId()];
        
        // Email the Batch Job's submitter that the Job is finished.  
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
        String[] toAddresses = new String[] { job.CreatedBy.Email };
        
        mail.setToAddresses(toAddresses);  
        mail.setSubject('Order Refresh Successfully completed');  
        mail.setPlainTextBody('Congratulations, Orders have been refreshed with latest product definitions. Result: ' + JSON.serialize(job));
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }    
}