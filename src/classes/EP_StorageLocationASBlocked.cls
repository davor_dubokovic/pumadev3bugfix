/*
 *  @Author <Accenture>
 *  @Name <EP_StorageLocationASBlocked >
 *  @CreateDate <6/3/2017>
 *  @Description < Account State for  06-Blocked Status>
 *  @Version <1.0>
 */
 public with sharing class EP_StorageLocationASBlocked extends EP_AccountState{
     
    public EP_StorageLocationASBlocked() {
        
    }

    public override void setAccountDomainObject(EP_AccountDomainObject currentAccount){
        EP_GeneralUtility.Log('Public','EP_StorageLocationASBlocked','setAccountDomainObject');
        super.setAccountDomainObject(currentAccount);
    }

    public override void doOnEntry(){
        EP_GeneralUtility.Log('Public','EP_StorageLocationASBlocked','doOnEntry');
        
    }  

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_StorageLocationASBlocked','doOnExit');
        
    }
    
    public override boolean doTransition(){
        EP_GeneralUtility.Log('Public','EP_StorageLocationASBlocked','doTransition');
        return super.doTransition();
    }

    public override boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_StorageLocationASBlocked','isInboundTransitionPossible');
        //Override this with all possible guard conditions that permits the state transition possible
        //For e.g.) Criteria based state "coming from", "Event" that's triggering this change 
        return super.isInboundTransitionPossible();

    } 
}