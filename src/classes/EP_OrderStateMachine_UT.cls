@isTest
public class EP_OrderStateMachine_UT{
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }
    static testMethod void getOrderState_Test(){
        EP_OrderStateMachine localObj =  new EP_OrderStateMachine();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        EP_OrderStateMachine.order = obj;
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.OrderState_Draft);
        EP_OrderStateMachine.orderEvent = oe;
        Test.startTest();
        EP_OrderState orderState = localObj.getOrderState();
        Test.stopTest();
        System.assertEquals(true,orderState != null);
    }
    static testMethod void getOrderStateWithEventParam_Test(){
        EP_OrderStateMachine localObj =  new EP_OrderStateMachine();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        EP_OrderStateMachine.order = obj;        
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.OrderState_Draft);
        Test.startTest();
        EP_OrderState orderState = localObj.getOrderState(oe);    
        Test.stopTest();
        System.assertEquals(true,orderState != null);
        System.assertEquals(oe ,EP_OrderStateMachine.orderEvent);
    }
    static testMethod void getOrderStateWithDomainAndEventParam_Test(){
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();     
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.OrderState_Draft);
        Test.startTest();
        EP_OrderState orderState = EP_OrderStateMachine.getOrderState(obj,oe);    
        Test.stopTest();
        System.assertEquals(true,orderState != null);
        System.assertEquals(oe ,EP_OrderStateMachine.orderEvent);
        System.assertEquals(obj ,EP_OrderStateMachine.order);
    }
    static testMethod void getOrderStateInstance_Test(){
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        EP_OrderStateMachine.order = obj;        
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.OrderState_Draft);
        Test.startTest();
        EP_OrderState orderState = EP_OrderStateMachine.getOrderStateInstance(obj,oe);    
        Test.stopTest();
        System.assertEquals(true,orderState != null);
    }
    static testMethod void getOrderStateInstance_CustomMappingException_Test(){
        List<EP_Order_State_Mapping__c> orderStateMappingCS = [select Classnames__c from EP_Order_State_Mapping__c];
        Database.delete(orderStateMappingCS);
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        EP_OrderStateMachine.order = obj;        
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.OrderState_Draft);
        Test.startTest();
        String message;
        try{
        EP_OrderState orderState = EP_OrderStateMachine.getOrderStateInstance(obj,oe); 
        }  
        catch(Exception exp){
            message = exp.getMessage();
        } 
        Test.stopTest();
        System.assert(message != null);
    }
    /*static testMethod void getOrderStateInstance_ClassName_Test(){
        EP_Order_State_Mapping__c orderStateMappingCS = [select Classnames__c from EP_Order_State_Mapping__c where
        Order_Type__c = 'NonVMINonConsignment' and Order_Status__c = 'Draft'];
        orderStateMappingCS.Classnames__c = 'Incorrect';
        Database.update(orderStateMappingCS);
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        EP_OrderStateMachine.order = obj;        
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.OrderState_Draft);
        Test.startTest();
        String message;
        try{
        EP_OrderState orderState = EP_OrderStateMachine.getOrderStateInstance(obj,oe);  
        } 
        catch(Exception exp){
            message = exp.getMessage();
        } 
        Test.stopTest();        
        System.assert(message != null);
    }*/
    static testMethod void setOrderDomainObject_Test(){
        EP_OrderStateMachine localObj =  new EP_OrderStateMachine();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        Test.startTest();
        localObj.setOrderDomainObject(obj);    
        Test.stopTest();
        System.assertEquals(obj ,EP_OrderStateMachine.order);
    }
}