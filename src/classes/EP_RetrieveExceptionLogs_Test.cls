/* 
   @Author Accenture
   @name <Test_EP_RetrieveExceptionLogs>
   @CreateDate <20/11/2015>
   @Description <this is the test class for the REST API>
   @Version <1.0>
 
*/
@isTest
private class EP_RetrieveExceptionLogs_Test {
  /**************************************************************************
    *@Description : This  method is used to test the postive scenario of restAPI call to retrieve exception logs.                                 *
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
 **************************************************************************/  
   
  static testMethod void testDoGetPositive() {
    
    //List<sObject> ls = Test.loadData(EP_Exception_Log__c.sObjectType,'Exception_Log_Test_Data');
    EP_Exception_Log__c newRec = new EP_Exception_Log__c();
    DateTime dt = System.today().addDays(-3);
    String dts=dt.format('MM-dd-yyyy');
    newRec.EP_OrgID__c = 'OrgId';
    newRec.EP_Severity__c = 'ERROR';
    newRec.EP_Is_Exported__c= false;
    insert newRec; 
    EP_Exception_Log__c newRec1 = new EP_Exception_Log__c();
    newRec1.EP_OrgID__c = 'OrgId';
    newRec1.EP_Severity__c = 'FATAL';
    newRec1.EP_Is_Exported__c= false;
    insert newRec1; 
     
    Test.StartTest(); 
    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();
    req.addParameter('dateException',dts);//Adding parameter date fo the request i.e date will be TODAY-2DAYS
   
    // pass the req and resp objects to the method     
    req.requestURI = '/services/apexrest/v1/ExceptionLogs/';  
    req.httpMethod = 'GET';
    RestContext.request = req;
    System.debug(' RestContext.request   '+ RestContext.request);
    RestContext.response= res;
    
    System.debug('req.requestURI   '+req.requestURI);
    EP_RetrieveExceptionLogs.retrieveExceptionLogs();
    Test.StopTest();
    List<EP_Exception_Log__c> epLogList = [Select EP_IS_Exported__c,Name,Id,createdDate from EP_Exception_Log__c ];
    System.debug('epLogList    '+epLogList  );
    System.assertEquals(true, epLogList[0].EP_Is_Exported__c); 
    System.assertEquals(true, epLogList[1].EP_Is_Exported__c);
    

  }
 /**************************************************************************
    *@Description : This  method is used to test the negative scenario of restAPI call passing date field as null(i.e not adding date parameter to the request).                                 *
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
 **************************************************************************/ 
    static testMethod void testDoGetNegative() {
    
    //List<sObject> ls = Test.loadData(EP_Exception_Log__c.sObjectType,'Exception_Log_Test_Data');
    EP_Exception_Log__c newRec = new EP_Exception_Log__c();
    DateTime dt = System.today().addDays(-3);
    String dts=dt.format('MM-dd-yyyy');
    newRec.EP_OrgID__c = 'OrgId';
    newRec.EP_Severity__c = 'ERROR';
    newRec.EP_Is_Exported__c= false;
    insert newRec; 
    EP_Exception_Log__c newRec1 = new EP_Exception_Log__c();
    newRec1.EP_OrgID__c = 'OrgId';
    newRec1.EP_Severity__c = 'FATAL';
    newRec1.EP_Is_Exported__c= false;
    insert newRec1; 
     
    Test.StartTest(); 
    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();
   
   
    // pass the req and resp objects to the method     
    req.requestURI = '/services/apexrest/v1/ExceptionLogs/';  
    req.httpMethod = 'GET';
    RestContext.request = req;
    System.debug(' RestContext.request   '+ RestContext.request);
    RestContext.response= res;
    
    System.debug('req.requestURI   '+req.requestURI);
    EP_RetrieveExceptionLogs.retrieveExceptionLogs();
    Test.StopTest();
    List<EP_Exception_Log__c> epLogList = [Select EP_IS_Exported__c,Name,Id,createdDate from EP_Exception_Log__c ];
    System.debug('epLogList    '+epLogList  );
    System.assertEquals(false, epLogList[0].EP_Is_Exported__c); 
    System.assertEquals(false, epLogList[1].EP_Is_Exported__c);
    

  }

}