/* 
@Author      Accenture
@name        EP_BusinessHourMapper
@CreateDate  04/1/2018
@Description This class contains all SOQLs related to BusinessHours Object 
@Version     1.0
*/
public class EP_BusinessHoursMapper{ 

    /** This method returns Business Hour Records
    *  @name            getRecordsByIds
    *  @param           id
    *  @return          BusinessHours
    *  @throws          NA
    */
    public static BusinessHours getRecordsByIds(id RecordId) {
        BusinessHours objBusinessHours ;
        if(string.isNotBlank(RecordId)){
            list<BusinessHours> lstBusinessHours = [select id,MondayStartTime, MondayEndTime from BusinessHours where id=:RecordId];
            if(!lstBusinessHours.isEmpty()){
                objBusinessHours = lstBusinessHours[0];
            }
        }
        return objBusinessHours;
    }
    /*CR #86413 -  code changes Start*/ 
    /** This method returns Business Hour Records
    *  @name            getRecordsByIds
    *  @param           id
    *  @return          BusinessHours
    *  @throws          NA
    */
    public static map<Id,BusinessHours> getRecordsByIds(set<Id> BusinessHoursIds) {       
        map<Id,BusinessHours> mapIdToBusinessHours = new map<Id,BusinessHours>([select id,MondayStartTime, MondayEndTime from BusinessHours where id IN: BusinessHoursIds]);
        return mapIdToBusinessHours;
    }
    /*CR #86413 -  code changes END*/
}