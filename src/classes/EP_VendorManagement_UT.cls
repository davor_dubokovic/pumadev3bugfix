@isTest
public class EP_VendorManagement_UT{  

    static testMethod void doUpdatePaymentTermAndMethod_test() {
        EP_VendorManagement localObj = new EP_VendorManagement();
        csord__Order__c orderObj = EP_TestDataUtility.getConsignmentPackagedOrder();
        Test.startTest();
        localObj.doUpdatePaymentTermAndMethod(orderObj);
        Test.stopTest();
        System.AssertEquals(orderObj.EP_Email__c ,null);
        System.AssertEquals(orderObj.BillingCountry__c,null);
    }
    
    static testMethod void setPaymntTermValForPckgOdr_test() {
        EP_VendorManagement localObj = new EP_VendorManagement();
        csord__Order__c orderObj = EP_TestDataUtility.getConsignmentPackagedOrder();
        String paymentTermId = [SELECT Id FROM EP_Payment_Term__c LIMIT 1].id;
        Test.startTest();
        localObj.setPaymntTermValForPckgOdr(paymentTermId,orderObj);
        Test.stopTest();
        System.AssertEquals(paymentTermId,orderObj.EP_Payment_Term_Value__c);
    }

    static testMethod void isPackagedProdCategoryAndPayTermISNOTNULL_PositiveScenariotest() {
        EP_VendorManagement localObj = new EP_VendorManagement();
        csord__Order__c orderObj = EP_TestDataUtility.getConsignmentPackagedOrder();
        String paymentTermId = [SELECT Id FROM EP_Payment_Term__c LIMIT 1].id;
        Test.startTest();
        Boolean result = localObj.isPackagedProdCategoryAndPayTermISNOTNULL(paymentTermId,orderObj);
        Test.stopTest();
        System.AssertEquals(true,result);
    }

    static testMethod void isPackagedProdCategoryAndPayTermISNOTNULL_NegativeScenariotest() {
        EP_VendorManagement localObj = new EP_VendorManagement();
        csord__Order__c orderObj = EP_TestDataUtility.getConsignmentPackagedOrder();
        String paymentTermId = null;
        Test.startTest();
        Boolean result = localObj.isPackagedProdCategoryAndPayTermISNOTNULL(paymentTermId,orderObj);
        Test.stopTest();
        System.AssertEquals(false,result);
    }

    static testMethod void setLocalNumberonOrder_test() {
    	EP_TestDataUtility.createCustomerSupportSetting();
        EP_VendorManagement localObj = new EP_VendorManagement();
        List<EP_Customer_Support_Settings__c> listCustomerSupportSettting = [SELECT EP_Company_Code__c , Name ,EP_Country__c,EP_Phone__c FROM EP_Customer_Support_Settings__c];
        csord__Order__c orderObj = EP_TestDataUtility.getConsignmentPackagedOrder();
        Account accountObj = [SELECT Id, EP_Puma_Company_Code__c FROM Account WHERE id=: orderObj.AccountId__c];
        Test.startTest();
        localObj.setLocalNumberonOrder(listCustomerSupportSettting,accountObj,orderObj);
        Test.stopTest();
        System.AssertNotEquals(null,orderObj.EP_Customer_Local_Number__c);
    }

    static testMethod void findRecordType_test() {
        EP_VendorManagement localObj = new EP_VendorManagement();
        Test.startTest();
        Id result = localObj.findRecordType();
        Test.stopTest();
        System.AssertEquals(null,result);
    }

}