/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageLocationActiveToActive>
*  @CreateDate <15/3/2017>
*  @Description <Handles Storage Ship To Account status change from 05-Active to 05-Active>
*  @Version <1.0>
*/
public class EP_ASTStorageLocationActiveToActive extends EP_AccountStateTransition {

    public EP_ASTStorageLocationActiveToActive () {
        finalState = EP_AccountConstant.ACTIVE;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationActiveToActive','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationActiveToActive', 'isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationActiveToActive',' isGuardCondition');        
        return true;
    }
}