/*
   @Author          Accenture
   @Name            EP_AccountStatementItemMapper
   @CreateDate      03/03/2018
   @Description     This class contains all SOQLs related to Customer Account Statement Item
   @Version         1.0
   @Reference       NA
*/
/***L4-45308 start****/
public class EP_AccountStatementItemMapper {
    /****************************************************************
    * @author       Accenture                                       
    * @Description  This method is used to get value of EP_Customer_Account_Statement_Item__c object record Id based on EP_Customer_Account_Statement_Item_Key__c field values 
    * @Name         setStatemrntItemId 
    * @param        EP_Customer_Account_Statement_Item__c                                   
    * @return       NA                                            
    ****************************************************************/
    public static void setStatemrntItemId(EP_Customer_Account_Statement_Item__c oCASItemRecord){
        EP_GeneralUtility.Log('Private','EP_AccountStatementItemMapper','setStatemrntItemId');
        list<EP_Customer_Account_Statement_Item__c> lstStatementItem = [select id from EP_Customer_Account_Statement_Item__c where EP_Customer_Account_Statement_Item_Key__c =:oCASItemRecord.EP_Customer_Account_Statement_Item_Key__c];       
        if(lstStatementItem.size()>0){
        	oCASItemRecord.id = lstStatementItem[0].id;
        }
    }    
    /**
     * @date <03/08/2018>
     * @description <This method is used to get value of EP_Customer_Account_Statement_Item__c object record Id based on EP_Customer_Account_Statement_Item_Key__c field values>
     * @param List<EP_Customer_Payment__c> listOfCustomerPayment
     * @return map<string,Id>
     */
     public static map<string,Id> getStatemrntItemId(List<EP_Customer_Payment__c> listOfCustomerPayment){
     	EP_GeneralUtility.Log('public','EP_AccountStatementItemMapper','getStatemrntItemId');
     	map<string,Id> mapStatementItemKeyToId = new map<string,Id>();
     	set<String> setStatementItemKey = new set<string>();
     	
     	setStatementItemKey = EP_CustomerAccountStatementUtility.getSetStatementItemKey(listOfCustomerPayment);
		for(EP_Customer_Account_Statement_Item__c objStatementItem : [select id,EP_Customer_Account_Statement_Item_Key__c from EP_Customer_Account_Statement_Item__c where EP_Customer_Payment__c IN :  listOfCustomerPayment and EP_Customer_Account_Statement_Item_Key__c IN : setStatementItemKey]){
			if(!mapStatementItemKeyToId.containskey(objStatementItem.EP_Customer_Account_Statement_Item_Key__c)){
				mapStatementItemKeyToId.put(objStatementItem.EP_Customer_Account_Statement_Item_Key__c,objStatementItem.id);
			}
		}
        return mapStatementItemKeyToId;  
     }
     /**
     * @date <03/08/2018>
     * @description <This method is used to get value of EP_Customer_Account_Statement_Item__c object record Id based on EP_Customer_Account_Statement_Item_Key__c field values>
     * @param List<EP_Customer_Payment__c> listOfCustomerPayment
     * @return map<string,Id>
     */
     public static map<string,Id> getStatemrntItemId(List<EP_Customer_Other_Adjustment__c> customerAdjustmentsToUpdate){
     	EP_GeneralUtility.Log('public','EP_AccountStatementItemMapper','getStatemrntItemId');
     	map<string,Id> mapStatementItemKeyToId = new map<string,Id>();
     	set<String> setStatementItemKey = new set<string>();
     	
     	setStatementItemKey = EP_CustomerAccountStatementUtility.getSetStatementItemKey(customerAdjustmentsToUpdate);
     	
		for(EP_Customer_Account_Statement_Item__c objStatementItem : [select id,EP_Customer_Account_Statement_Item_Key__c from EP_Customer_Account_Statement_Item__c where EP_Customer_Other_Adjustment__c IN :  customerAdjustmentsToUpdate and EP_Customer_Account_Statement_Item_Key__c IN : setStatementItemKey]){
			if(!mapStatementItemKeyToId.containskey(objStatementItem.EP_Customer_Account_Statement_Item_Key__c)){
				mapStatementItemKeyToId.put(objStatementItem.EP_Customer_Account_Statement_Item_Key__c,objStatementItem.id);
			}
		}
        return mapStatementItemKeyToId;  
     }     
    /***L4-45308 code changes End****/
}