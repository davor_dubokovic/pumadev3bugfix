/* 
   @Author 			Accenture
   @name 			EP_InboundHandler
   @CreateDate 		03/10/2017
   @Description		Inbound handler for all inbound interfaces
   @Version 		1.0
*/
public virtual class EP_InboundHandler {
	
    public virtual string processRequest(string jsonRequest){
        EP_GeneralUtility.Log('Public','EP_InboundHandler','processRequest');
        return '';
    }
    
    public virtual string processRequest(){
        EP_GeneralUtility.Log('Public','EP_InboundHandler','processRequest');
        return '';
    }
    //Changes for #60147
    public virtual string processRequest(string jsonRequest, string recordNumber){
        EP_GeneralUtility.Log('Public','EP_InboundHandler','processRequest');
        return '';
    }
}