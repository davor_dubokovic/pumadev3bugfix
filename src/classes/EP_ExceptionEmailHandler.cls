/* 
@Author <Accenture>
@name <EP_ExceptionEmailHandler >
@CreateDate <11/22/2015>
@Description <This class will handle the incoming email which salesforce sends when exception occurs>
@Version <1.0>

*/
global without sharing class EP_ExceptionEmailHandler implements Messaging.InboundEmailHandler{

	private static final String URL = '/00D';
	private static final String HANDLE_INBOUND_EMAIL ='handleInboundEmail';
	private static final String EP_EXCEPTION_EMAIL_HANDLER ='EP_ExceptionEmailHandler';
	private static final String USERORG_NAME ='user/organization: ';

	/**
	* @author <Accenture>
	* @date <11/22/2015>
	* @name <handleInboundEmail>
	* @description <This method will handle the incoming email and result in generating of the Exception log object>
	* @param Messaging.InboundEmail, Messaging.InboundEnvelope
	* @return Messaging.InboundEmailResult
	*/
	global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
		String userOrg = USERORG_NAME;
		Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
		Savepoint sp = Database.setSavepoint();
		try{
			EP_Exception_Log__c exceptionLog = new EP_Exception_Log__c();
			string emailBody = email.plainTextBody.toLowerCase().trim();
			
			//First, determine the error type
			string emailErrorType = EP_Common_constant.BLANK;       
			if (emailBody.contains(EP_Common_Constant.CUSTOM_VALIDATION))  {
				emailErrorType = emailErrorType + EP_Common_Constant.VALIDATION_ERROR;
			}
			if (emailBody.contains(EP_Common_Constant.APEX_TRIGGER_SCRIPT_EXCEPTION)) {
				emailErrorType = emailErrorType + EP_Common_Constant.APEX_TRIGGER;
			}
			if (emailBody.contains(EP_Common_Constant.BATCH)){
				emailErrorType = emailErrorType + EP_Common_Constant.BATCH_APEX    ;
			}
			if (emailBody.contains(EP_Common_Constant.VISUALFORCE)) {
				emailErrorType = emailErrorType +EP_Common_Constant.CHAR_C;
			}
			if (emailBody.contains(EP_Common_Constant.APEX_SCRIPT_EXCEPTION)){
				emailErrorType = emailErrorType + EP_Common_Constant.APEX_CLASS;
			}
			else{
				if (String.isBlank(emailErrorType)){  //Finally, if we haven't found anything, dump it in 'other'
					emailErrorType =EP_Common_Constant.OTHER;
				}
			}
			exceptionLog.EP_Exception_Type__c= emailErrorType;    
			if(email.plainTextBody.contains(EP_Common_Constant.CLASS_DOT)){
				exceptionLog.EP_Class_Name__c= email.plainTextBody.substringBetween(EP_Common_Constant.CLASS_DOT,EP_Common_Constant.DOT);
			}
			if(email.plainTextBody.contains(EP_Common_Constant.LINE)){
				exceptionLog.EP_Method_Name__c= email.plainTextBody.substringBetween(exceptionLog.EP_Class_Name__c+EP_Common_Constant.DOT,EP_Common_Constant.LINE);
			}
			exceptionLog.EP_Exception_Details__c=email.plainTextBody;
			exceptionLog.EP_Application_Name__c=EP_LoggingService.returnCurrentApp();
			if(email.plainTextBody.contains(userOrg)){
				User user=[Select id from USER where id=:email.plainTextBody.substringBetween(userOrg,URL) LIMIT : EP_Common_Constant.ONE];
				exceptionLog.EP_Running_User__c= user.ID;
			}
			
			//insert exception log 
			EP_LoggingService.logUnhandledException(exceptionLog,EP_Common_Constant.ERROR_SYNC_SENT_STATUS );
			
			result.success = true;
		}catch (Exception ex){
			EP_LoggingService.logHandledException (ex,EP_Common_Constant.EPUMA, HANDLE_INBOUND_EMAIL, EP_EXCEPTION_EMAIL_HANDLER, ApexPages.Severity.ERROR);
			result.success = false;
			result.Message = Label.EP_Incoming_Email_Error_Message;
			Database.rollBack(sp);
			//TODO: Add error handling scenario
		}
		return result;
	}
}