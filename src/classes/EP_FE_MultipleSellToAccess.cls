/*********************************************************************************************
     @Author :<Prayank Sahu>
     @name : <C1>
     @CreateDate : <14/02/2018>
     @Description : <This class to get Multiple SellTo Account On Portal >  
     @VFPage Usage : C1
     @Version : <1.0>
    *********************************************************************************************/
public Class EP_FE_MultipleSellToAccess{
       
    public EP_FE_PortalUserAccountAccess__c PtlAccount{get; set;}
    public String StrSellToId {get;set;}
    public String StrUsrConId{get;set;}
    public List<wrapShipToAcc> lstAccWithCheckbox{get;set;}
    public Set<Id> sShipToIds {get;set;}
    public boolean bShipToSection {get;set;}
    public boolean bAcknowledgeSection{get;set;}
    public boolean showSelectAllbutton{get;set;}
    public boolean showUnselectAllbutton{get;set;}
    public boolean disableCustName {get;set;}
    
     public EP_FE_MultipleSellToAccess(ApexPages.StandardController controller) {
    
        lstAccWithCheckbox = new List<wrapShipToAcc>();  
        sShipToIds = new Set<Id>(); 
        PtlAccount = new EP_FE_PortalUserAccountAccess__c(); 
        bShipToSection = False; 
        bAcknowledgeSection = False;
        showUnselectAllbutton = False;
        showSelectAllbutton = True;
        disableCustName = False;
        StrUsrConId =  apexpages.currentpage().getparameters().get('ContactId');
        PtlAccount.EP_FE_Customer_Name__c = StrUsrConId;
    }
           
    public PageReference allShipToWithCheckbox(){
        
      
        
        bShipToSection = True;
        bAcknowledgeSection = False;
        //StrUsrConId = PtlAccount.EP_FE_Customer_Name__c;
        StrSellToId = PtlAccount.EP_FE_Sellto_Name__c;
                
        for(Account accInstance: [Select Id,Name,ParentId From Account Where RecordType.DeveloperName IN(:EP_Common_Constant.NON_VMI_SHIP_TO_DEV_NAME, :EP_Common_Constant.VMI_SHIP_TO_DEV_NAME)
                                  AND (ParentId =: PtlAccount.EP_FE_Sellto_Name__c) ]){
          wrapShipToAcc w = new wrapShipToAcc();            
          w.accObj =accInstance;
          lstAccWithCheckbox.add(w);            
      }                        
        return null;                
    }
    
    public void SelectedShipTo(){   
        
        bShipToSection = False;
        bAcknowledgeSection = True;                 
        for(wrapShipToAcc w:lstAccWithCheckbox){                        
            if(w.Checkbox == true ){                                
                sShipToIds.add(w.accObj.Id);              
            }                                                
        }                      
        InsertSelectedRecords(sShipToIds);  
         
          lstAccWithCheckbox.Clear();      
    }
    
    public Class wrapShipToAcc{
        
        public Account accObj{get;set;}
        public boolean Checkbox{get;set;}
        
        public wrapShipToAcc(){                                                
        }                
    }
    
    public void InsertSelectedRecords(Set<Id> sShipToIds){
        
        List<EP_FE_PortalUserAccountAccess__c> lstPUAAtoInsert = new List<EP_FE_PortalUserAccountAccess__c>();
        List<EP_FE_PortalUserAccountAccess__c> lstFinalPUAA = new List<EP_FE_PortalUserAccountAccess__c>();
        
        for(Id IdInstance :sShipToIds){
            
            EP_FE_PortalUserAccountAccess__c ObjInstance = new EP_FE_PortalUserAccountAccess__c();
            ObjInstance.EP_FE_Customer_Name__c = StrUsrConId ;
            ObjInstance.EP_FE_Sellto_Name__c = StrSellToId ;
            ObjInstance.EP_FE_Shipto_Name__c = IdInstance;
            
            lstPUAAtoInsert.add(ObjInstance);    
        }    
        
          List<EP_FE_PortalUserAccountAccess__c> lstPUAA = [Select EP_FE_Customer_Name__c,EP_FE_Sellto_Name__c,EP_FE_Shipto_Name__c From EP_FE_PortalUserAccountAccess__c
                                                            Where EP_FE_Shipto_Name__c IN:sShipToIds ];
                                   
        Map<Id,String> mShiToIdVsStrCustNSellTo = new Map<Id,String>();
                                                            
        for(EP_FE_PortalUserAccountAccess__c p:lstPUAA ){
        
        String sCustNSellTo = p.EP_FE_Customer_Name__c+'-'+p.EP_FE_Sellto_Name__c;
        mShiToIdVsStrCustNSellTo.put(p.EP_FE_Shipto_Name__c,sCustNSellTo);
        
        }
        
        
        for(EP_FE_PortalUserAccountAccess__c p:lstPUAAtoInsert){
        
           String sCustNSellTo = p.EP_FE_Customer_Name__c+'-'+p.EP_FE_Sellto_Name__c;
           System.debug('======sCustNSellTo===>'+sCustNSellTo);
           System.debug('======mShiToIdVsStrCustNSellTo.get(p.EP_FE_Shipto_Name__c)===>'+mShiToIdVsStrCustNSellTo.get(p.EP_FE_Shipto_Name__c));
           
           if(mShiToIdVsStrCustNSellTo.get(p.EP_FE_Shipto_Name__c) != sCustNSellTo ){
           
            lstFinalPUAA.add(p);
           }
        }
        
        //Insert lstPUAAtoInsert;  
        Insert lstFinalPUAA;  
    }
    
    public void selectAll(){
    
    for(wrapShipToAcc  a:lstAccWithCheckbox){
    
      a.checkbox= true;
    }    
    showUnselectAllbutton = True;
    showSelectAllbutton = False;
    
    }
    
    public void UnselectAll(){
    
     for(wrapShipToAcc  a:lstAccWithCheckbox){    
      a.checkbox= False;
    }        
    showUnselectAllbutton = False;
    showSelectAllbutton = True;
    
    }            
}