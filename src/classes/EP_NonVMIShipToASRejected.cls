/*
 *  @Author <Accenture>
 *  @Name <EP_NonVMIShipToASRejected>
 *  @CreateDate <>
 *  @Description <VMI Ship To Account State for 08-Rejected Status>
 *  @Version <1.0>
 */
 public with sharing class EP_NonVMIShipToASRejected extends EP_AccountState{
    /***NOvasuite fix constructor removed**/ 
/*
*  @Author <Accenture>
*  @Name setAccountDomainObject
*/
    public override void setAccountDomainObject(EP_AccountDomainObject currentAccount){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASRejected','setAccountDomainObject');
        super.setAccountDomainObject(currentAccount);
    }

    //L4_45352_Start
/*
*  @Author <Accenture>
*  @Name doOnEntry
*/
    public override void doOnEntry(){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASAccountSetup','doOnEntry');
        EP_AccountService service = new EP_AccountService(this.account);
        if(service.isSellToSynced()  && !EP_IntegrationUtil.ISERRORSYNC){//WP2-Pricing Engine Callout changes     
            if(!this.account.localaccount.EP_Synced_PE__c){
                service.doActionSyncCustomerToPricingEngine();
            }            
            else if(!this.account.localaccount.EP_Synced_NAV__c){//WP2-Pricing Engine Callout changes
                system.debug('-Calling--NAV--');
                service.doActionSendCreateRequestToNav();
            }
            else if(this.account.localaccount.EP_Synced_NAV__c && !this.account.localaccount.EP_Synced_WinDMS__C){
                system.debug('-Calling--WinDMS--');
                service.doActionSendCreateRequestToWinDMS();
            }
        } 
    }  
    //L4_45352_END
/*
*  @Author <Accenture>
*  @Name doOnExit
*/
    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASRejected','doOnExit');
        
    }
/*
*  @Author <Accenture>
*  @Name doTransition
*/    
    public override boolean doTransition(){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASRejected','doTransition');
        return super.doTransition();
    }
/*
*  @Author <Accenture>
*  @Name isInboundTransitionPossible
*/ 
    public override boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASRejected','isInboundTransitionPossible');
        //Override this with all possible guard conditions that permits the state transition possible
        //For e.g.) Criteria based state "coming from", "Event" that's triggering this change 
        return super.isInboundTransitionPossible();

    }

}