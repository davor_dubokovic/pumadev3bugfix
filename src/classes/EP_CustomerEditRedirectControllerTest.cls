@isTest
private class EP_CustomerEditRedirectControllerTest {
    public static testMethod void testMethod1(){
    	PageReference pref = null;
    	//Run as Sales User
        Profile p = [SELECT Id FROM Profile WHERE Profile.Name Like 'System Administrator%' LIMIT 1]; 
        User u = EP_TestDataUtility.createUser(p.Id);            
        Database.SaveResult saveUser = Database.insert(u);            
        System.runAs(u){
        	//L4# 78534 - Test Class Fix - Start 
        	//RecordType AccRt = [select id,Name from RecordType where SobjectType='Account' and Name='VMI Ship To' Limit 1];
        	// Insert Account
            //Account acc = new Account();
            account sellToAccount = EP_TestDataUtility.getSellTo();
            Account acc = EP_TestDataUtility.createShipToAccount(sellToAccount.Id, null);
            //L4# 78534 - Test Class Fix - End 
            acc.name='Test Accenture';
            acc.BillingCity = 'test City';
            acc.BillingStreet  = 'test street';
            acc.BillingState = 'test state';
            acc.BillingPostalCode = '201301';
            acc.BillingCountry = 'test';         
            acc.ShippingCity = 'ShipCity';
	        acc.ShippingStreet = 'ShipStreet';
	        acc.ShippingState = 'ShipState';
	        acc.EP_Eligible_for_Rebate__c =True;
	        acc.ShippingCountry = 'ShipCountry';
	        acc.ShippingPostalCode = '123465';        
            acc.EP_CRM_Geo1__c = 'Africa';
            acc.EP_CRM_Geo2__c = 'Ghana';
            //acc.RecordTypeId = AccRt.Id;
            Database.SaveResult saveAcc = Database.insert(acc);
            
            RecordType rt = [select id,Name from RecordType where SobjectType='EP_Tank__c' Limit 1];
            
            Product2  testProduct = new product2(Name = 'Tax Product Name', CurrencyIsoCode = 'GBP', isActive = TRUE, Family = 'Hydrocarbon Liquid', Eligible_for_Rebate__c = TRUE);
                           
	        Database.Insert(testProduct);
	        Id pricebookId = Test.getStandardPricebookId();
	        PricebookEntry standardPrice = new PricebookEntry(
	                                            Pricebook2Id = pricebookId, Product2Id = testProduct.Id,
	                                                UnitPrice = 10000, IsActive = TRUE);
	        //Database.Insert(standardPrice);//L4# 78534 - Test Class Fix - Start 
            
            EP_Tank__c tankObj = new EP_Tank__c ();
            tankObj.EP_Ship_To__c = acc.Id;
            tankObj.EP_Product__c = testProduct.Id;
            tankObj.EP_Safe_Fill_Level__c=2500;
            tankObj.CurrencyIsoCode='AUD';
            tankObj.EP_Tank_Dip_Entry_Mode__c = 'Automatic Dip Entry';
        	tankObj.RecordTypeId= rt.Id; 
        	tankObj.EP_Tank_Status__c = 'New'; 
	    	insert tankObj;
	    	
	    	System.assertNotEquals(tankObj, null);
	    	System.assertNotEquals(tankObj.Id, null);
	    	
	    	PageReference pageRef = Page.EP_RedirectAccountEdit;    	
	        Test.setCurrentPage(pageRef);
	        ApexPages.currentPage().getParameters().put('Id', tankObj.Id);
    		EP_CustomerEditRedirectController controller = new EP_CustomerEditRedirectController(null);
    		pref = controller.redirectTank();
    		
    		pageRef = Page.EP_RedirectAccountEdit;    	
	        Test.setCurrentPage(pageRef);
	        ApexPages.currentPage().getParameters().put('Id', acc.Id);
    		controller = new EP_CustomerEditRedirectController(null);
    		pref = controller.redirectAccount();
        }
    }
}