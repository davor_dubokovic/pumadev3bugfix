/* 
   @Author <Accenture>
   @name <EP_OrderSyncMsgFromNAV>
   @CreateDate <03/08/2017>
   @Description <These are the stub classes to map JSON string for orders sync Rest Service Request from NAV> 
   @Version <1.0>
   */
/* 
    Main stub Class for JSON parsing for orders sync from NAV  
*/
public with sharing class EP_NAVOrderNewStub {
    /* Class for MSG */
    public MSG MSG;
    
    /* stub Class for MSG */
    public class MSG {
        public HeaderCommon HeaderCommon;
        public Payload Payload;
        public String StatusPayload;
    }
    
    /* stub Class for HeaderCommon */
    public class HeaderCommon extends EP_MessageHeader { }
    
    /* stub Class for Payload */
    public class Payload {
        public Any0 any0;
    }
    
    /* stub Class for Any0 */
    public class Any0 {
        public OrderWrapper order;
    }
    
    /* stub Class for Order */
    public class OrderWrapper {
        //Added New variable
        public csord__Order__c sfOrder;
        public boolean isCloned;
        public String shipToCompositKey;
        public String sellToCompositKey;
        //END
        public String seqId;
        public Identifier identifier;
        public String sellToId; //sellToId
        public String shipToId; //shipToId
        public String reqDlvryDt ;
        public String expctdDlvryDt;
        public String orderStartDt ;
        public String loadingDt;
        public String dlvryDt;
        public String deliveryType;
        public String transportManagmnt;
        public String processingStatus; //Query on EP_OrderStatusNAVMapping__c PLANNED
        public String orderType;    //MINon-Consignment
        public String totalOrderQty;    //totalOrderQty
        public String paymentTerm;  //Query on paymentTerm
        public String paymentMethod;    //Query on EP_Payment_Method__c paymentMethod
        public String paymentType;  //PrePayment
        public String currencyId;
        public String pricingDate;  //pricingDate
        public String priceValidityPeriod;  //2days
        public String pricingStockHldngLocId;   //Query pricingStockHldngLocId
        public String logisticStockHldngLocId;  //logisticStockHldngLocId
        public String comment;
        public String exceptionNr ;
        public String pricingTransporterCode ;
        public String logisticTransporterCode;
        public String tripId;
        public String orderEpoch;
        public String orderOrigination;
        public String orderRefNr;
        public String clientId; //clientId
        public OrderLines orderLines;
    }
    
    /* stub Class for OrderLines */
    public class OrderLines {
        public OrderLine[] OrderLine;
    }
    
    /* stub Class for OrderLine */
    public class OrderLine {
        public csord__Order_Line_Item__c orderLineItem;
        public String seqId;
        public OrderLineIdentifier identifier;
        public String itemId;   //itemId
        public String qty;
        public String pricingStockHldngLocId;   //pricingStockHldngLocId
        public String logisticStockHldngLocId;  //logisticStockHldngLocId
        public String uom;
        public String unitPrice;    //unitPrice
        public String loadedAmbientQty;
        public String loadedStandardQty;
        public String deliveredAmbientQty;
        public String deliveredStandardQty;
        public Bols bols;
        public LineComponents lineComponents;
    }
    
    /* stub Class for LineComponents */
    public class LineComponents {
        public InvoiceComponents invoiceComponents;
        public AcctComponents acctComponents;
    }
    
    /* stub Class for InvoiceComponent */
    public class InvoiceComponent {
        public csord__Order_Line_Item__c invoiceItem;
        public String type;
        public String name;
        public String amount;
        public String taxPercentage;
        public String taxAmount;
    }
    
    /* stub Class for InvoiceComponents */
    public class InvoiceComponents {
        public List<InvoiceComponent> invoiceComponent;
    }
    
    /*
        Stub Class for AcctComponents tag
    */
    public class AcctComponents {
        public AcctComponent[] acctComponent;
    }
        
    /*
        Stub Class for AcctComponent tag
    */
    public class AcctComponent {
        public String componentCode;    //componentCode
        public String glAccountNr;  //glAccountNr
        public String baseAmount;   //0.0
        public String taxRate;  //0.0
        public String taxAmount;    //0.0
        public String totalAmount;  //0.0
        public String isVAT;    //YES
    }
    
    /*
        Stub Class for Bols tag
    */
    public class Bols {
        public List<Bol> bol;
    }
        
    /*
        Stub Class for Bol tag
    */
    public class Bol {
        public String bolid;
        public String qty;
        public String contractid;
        public String supplierid;
    }
        
    /*
        Stub Class for Identifier tag
    */
    public class Identifier {
        public String orderId;
    }
        
    /*
        Stub Class for OrderLineIdentifier tag
    */
    public class OrderLineIdentifier {
        public String lineId;
    }
 }