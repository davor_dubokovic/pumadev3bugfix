/*
*  @Author <Accenture>
*  @Name <EP_AccountShareUtility>
*  @CreateDate <09/03/2017>
*  @Description <Utility Class is used to create manual sharing records to allow Sell-To portal users to access Ship-To accounts>
*  @Version <1.0>
*/
public class EP_AccountShareUtility{	
	
	private static final String ACCOUNT_EDIT_ACCESS_LEVEL = 'Edit';
	private static final String ACCOUNT_READ_ACCESS_LEVEL = 'Read';
	private static final String OPP_ACCESS_LEVEL = 'None';
	private static final String CASE_EDIT_ACCESS_LEVEL = 'Edit';
	private static final String CASE_NO_ACCESS_LEVEL = 'None';
	private static final String CONTACT_ACCESS_LEVEL = 'Read';
	private static final String MANUAL_ROW_CAUSE = 'Manual';
	
	 @Testvisible static Map<Id, Account> mapAccountsForProcessing = new Map<Id, Account>();
	 @Testvisible static List<UserRole> lAccountUserRoles = new list<UserRole>();
	 @Testvisible static Map<String, String> mapGroupPortalUserRoleRecords = new Map<String, String>();
	 @Testvisible static List<ShareAccessWrapper> accountsharingWrapper =  new List<ShareAccessWrapper>();


    /**
    *  @name managePortalRoleSharingRulesOnAccountCreation
    *  @description <This method will generate the sharing record for the account/portal role combination>
    *  @param Account
    *  @return none
    */ 
    @Testvisible
    private static void managePortalRoleSharingRulesOnAccountCreation(Account account){
    	EP_GeneralUtility.Log('Private','EP_AccountShareUtility','managePortalRoleSharingRulesOnAccountCreation');
    	mapAccountsForProcessing = getAccountsForProcessing(account);
    	lAccountUserRoles = getUserRole(mapAccountsForProcessing.KeySet());
    	mapGroupPortalUserRoleRecords = getGroupPortalUserRoleRecords(lAccountUserRoles);  
    	if(!mapAccountsForProcessing.isEmpty()){
    		processPortalRoleSharingRules(); 
    	}
    }


   /**
	*  @description <Method to get a map of Account records which will be processed for sharing>
    *  @name getAccountsForProcessing  
    *  @param Account
    *  @return Map<Id, Account>
    */
    @Testvisible
    private static Map<Id, Account> getAccountsForProcessing(Account account){
    	EP_GeneralUtility.Log('Private','EP_AccountShareUtility','getAccountsForProcessing');
    	Map<Id, Account> accountforprocessingMap = new Map<Id, Account>();
    	accountforprocessingMap.put(account.Id, account);
    	if (account.ParentId != null){
    		EP_AccountDomainObject objAccountDomain = new EP_AccountDomainObject(account.ParentId);
    		Account parentAccount = objAccountDomain.localAccount; //[SELECT Id, ParentId FROM Account WHERE ID =: account.ParentId];
    		accountforprocessingMap.put(account.ParentId, parentAccount);
    	}
    	return accountforprocessingMap;		
    }


   /**
	*  @description <Method to get map of user role id and its realetd group Id>
    *  @name getGroupPortalUserRoleRecords  
    *  @param List<UserRole>
    *  @return Map<string, string>
    */
    @Testvisible
    private static Map<String, String> getGroupPortalUserRoleRecords(List<UserRole> accountUserRoles){
    	EP_GeneralUtility.Log('Private','EP_AccountShareUtility','getGroupPortalUserRoleRecords');
    	EP_GroupMapper groupMaperobj =  new EP_GroupMapper();
    	Map<String, String> groupPortalUserRoleRecordsMap = new Map<String, String>();		
		//Build a map linking portal user roles to groups
		for (Group grp : groupMaperobj.getGroupByUserRoles(accountUserRoles)){
			groupPortalUserRoleRecordsMap.put(grp.RelatedId, grp.Id); // User Role ID / Group ID
		}
		return groupPortalUserRoleRecordsMap; 
	}


	/**
	*  @description <Method to get portal roles for the accounts and their parents if any >
    *  @name getUserRole  
    *  @param Set<Id> 
    *  @return List<UserRole>
    */
    @Testvisible
    private static List<UserRole> getUserRole(Set<Id> setAccountIDs){
    	EP_GeneralUtility.Log('Private','EP_AccountShareUtility','getUserRole');
    	EP_UserMapper userMapperObj = new EP_UserMapper();
    	List<UserRole> accountUserRolesList = userMapperObj.getPortalAccountUserRole(setAccountIDs);//[SELECT Id, PortalAccountId FROM UserRole  WHERE PortalAccountId IN :setAccountIDs ];
    	return accountUserRolesList;
    }


	/**
	* @description <This method will process the different cases of sharing rules, depending of this is triggered at child/parent level>
    * @name <processPortalRoleSharingRules>
	* @param NA
    * @return NA
    */
    @Testvisible
    private static void processPortalRoleSharingRules() {
    	EP_GeneralUtility.Log('Private','EP_AccountShareUtility','processPortalRoleSharingRules');
    	for (Account account : mapAccountsForProcessing.values()){
			// 1. This covers the actual account sharing and read-only sharing of the parent account to the child one
			accountsharingWrapper.addAll(setSharingAccess(account.Id));
			// 2. This covers the sharing of the account children to the users of the parent account
			if (account.ParentId != NULL){
				accountsharingWrapper.addAll(setSharingAccess(account.ParentId));
			}	
		}              
	}


	/**
	* @description <This method is used to set Account and Case record access>
    * @name <ShareAccessWrapper>
	* @param NA
    * @return NA
    */
    @Testvisible
    private static List<ShareAccessWrapper> setSharingAccess(Id accountId){
    	EP_GeneralUtility.Log('Private','EP_AccountShareUtility','setSharingAccess');
    	List<ShareAccessWrapper> sharingWrapperList = new List<ShareAccessWrapper>();
    	for (UserRole usrRole : lAccountUserRoles){
    		if (mapGroupPortalUserRoleRecords.containsKey(usrRole.Id)){
    			if (accountId == usrRole.PortalAccountId){
    				ShareAccessWrapper sharingWrapper = new ShareAccessWrapper();
    				sharingWrapper.accountId = accountId;
    				sharingWrapper.PortalRoleGroupId = mapGroupPortalUserRoleRecords.get(usrRole.Id);
    				sharingWrapper.accountEditAccess = TRUE;
    				sharingWrapper.caseEditAccess = TRUE;	
    				sharingWrapperList.add(sharingWrapper);
    			}				
    		}
    	}
    	return sharingWrapperList;
    }


	/**
	 * @description <This method will perform the actual insert @future>
     * @name <insertManualSharingRecords>
     * @param List<ShareAccessWrapper>
     * @return none
     */
     @future
     public static void insertManualSharingRecords(Id accountId) {
     	EP_GeneralUtility.Log('Public','EP_AccountShareUtility','insertManualSharingRecords');
     	EP_AccountDomainObject objDomain = new EP_AccountDomainObject(accountId);
     	Account account = objDomain.localAccount;
     	managePortalRoleSharingRulesOnAccountCreation(account);     	
     	if(!accountsharingWrapper.isEmpty()){
     		List<AccountShare> lAccountShareRecordsForInsert = new List<AccountShare>();
     		for (ShareAccessWrapper accountshare : accountsharingWrapper){
     			lAccountShareRecordsForInsert.add(getAccountPortalRoleManualSharingRecord(accountshare));
     		}                
     		if (!lAccountShareRecordsForInsert.isEmpty()){
     			Database.insert(lAccountShareRecordsForInsert);
     		}
     	}
     }


	/**
	 * @description <This method will generate the sharing record for the account/portal role combination>
     * @methodname <getAccountPortalRoleManualSharingRecord>
     * @param ShareAccessWrapper
     * @return AccountShare
     */  
     @Testvisible
     private static AccountShare getAccountPortalRoleManualSharingRecord(ShareAccessWrapper accountshare) {
     	EP_GeneralUtility.Log('Private','EP_AccountShareUtility','getAccountPortalRoleManualSharingRecord');
     	AccountShare newAccountShareRecord =  new AccountShare(
     		AccountId = accountshare.accountId,
     		UserOrGroupId = accountshare.portalRoleGroupId,
     		AccountAccessLevel = getAccountAccessType(accountshare),
     		OpportunityAccessLevel = OPP_ACCESS_LEVEL,
     		CaseAccessLevel = getCaseAccessType(accountshare),
     		ContactAccessLevel = CONTACT_ACCESS_LEVEL,
     		RowCause = MANUAL_ROW_CAUSE
     		);
     	return newAccountShareRecord;
     }


	/**
	 * @description <This method is used to account access type for sharing>
     * @methodname <getAccountAccessType>
     * @param ShareAccessWrapper
     * @return string
     */
     @Testvisible
     private static string getAccountAccessType(ShareAccessWrapper accountshare){
     	EP_GeneralUtility.Log('Private','EP_AccountShareUtility','getAccountAccessType');
     	String strAccountAccess = ACCOUNT_READ_ACCESS_LEVEL;
     	if(accountshare.accountEditAccess){
     		strAccountAccess = ACCOUNT_EDIT_ACCESS_LEVEL;
     	}
     	return strAccountAccess;
     }


	/**
	 * @description <This method is used to case access type for sharing>
     * @methodname <getCaseAccessType>
     * @param ShareAccessWrapper
     * @return string
     */
     @Testvisible
     private static string getCaseAccessType(ShareAccessWrapper accountshare){
     	EP_GeneralUtility.Log('Private','EP_AccountShareUtility','getCaseAccessType');
     	String strCaseAccess = CASE_NO_ACCESS_LEVEL;
     	if(accountshare.caseEditAccess){
     		strCaseAccess = CASE_EDIT_ACCESS_LEVEL;
     	} 
     	return strCaseAccess;
     }


	/**
	 * @description <Wrapper class to hold account and portal group association and its edit access>
     * @methodname <ShareAccessWrapper>
     * @param NA
     * @return NA
     */
    @Testvisible
     class ShareAccessWrapper{
     	@Testvisible string accountId;
     	@Testvisible string PortalRoleGroupId;
     	@Testvisible boolean accountEditAccess;
     	@Testvisible boolean caseEditAccess;		
     }

 }