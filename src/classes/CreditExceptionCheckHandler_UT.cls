/***
 * Author: Pawan (CloudSense)
 * Description: Test Class for CreditExceptionCheckHandler .
 * Version History:
 * v1 - Created on 2018-08-17
 ***/
@isTest
public class CreditExceptionCheckHandler_UT {
    
    @testSetup
    public static void createTestData() {
        Account acc = new Account();
        acc.EP_VendorType__c = '3rd Party Stock Supplier';
        acc.EP_Vendor_Type__c = '3rd Party Stock Supplier';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        acc.Name='testEPAccount';
        acc.EP_Status__c = '06-Blocked';
        insert acc;
		System.Assert(acc.Id != null);
        
        csord__Order__c ord = getCSOrder();
        
        CSPOFA__Orchestration_Process_Template__c oProcessTemplate = new CSPOFA__Orchestration_Process_Template__c(Name='test template');
        insert oProcessTemplate;
		System.Assert(oProcessTemplate.Id != null);
        
        CSPOFA__Orchestration_Process__c oProcess = new CSPOFA__Orchestration_Process__c(Name='test process', 
                                                                                         CSPOFA__Orchestration_Process_Template__c=oProcessTemplate.Id,
                                                                                         Order__c=ord.Id
                                                                                        );
        insert oProcess;
		System.Assert(oProcess.Id != null);

        CSPOFA__Orchestration_Step__c oStep = new CSPOFA__Orchestration_Step__c(Name='test step',
                                                                                CSPOFA__Orchestration_Process__c=oProcess.Id
                                                                               );
        insert oStep;
        
        EP_Credit_Exception_Request__c creditException = new EP_Credit_Exception_Request__c();
        creditException.EP_Available_Funds__c=20000;
        creditException.EP_Bill_To__c = ord.EP_Sell_To__c;
        creditException.EP_Current_Order_Value__c =12;
        creditException.EP_CS_Order__c=ord.Id;
        creditException.EP_Overdue_Invoices__c=false;
        creditException.EP_Reason__c='Reason';
        creditException.EP_Request_Date__c=Date.today();
        insert creditException;
		System.Assert(creditException.Id != null);
    }
    
    public static testmethod void testCreditExceptionCheckHandler() {
        list<SObject> steps = [select Id from CSPOFA__Orchestration_Step__c];
        system.debug(steps);
        
        test.startTest();
        CreditExceptionCheckHandler  creditExceptionCheckHandler = new CreditExceptionCheckHandler ();   
        System.assert(steps.size() > 0, 'Invalid data');     
        creditExceptionCheckHandler.process(steps);       
        test.stopTest();        
        
    }
    
    public static csord__Order__c getCSOrder() {
 
        Account shipTo = EP_TestDataUtility.getshipTo();
        csord__Order__c csOrder = new csord__Order__c(Name='Test Order',csord__Identification__c='XXXXX',csord__Delivered_Date__c=Date.today(),EP_Payment_Term__c='PrePayment',
            EP_Order_Epoch__c='Current',EP_Loading_Date__c = Date.today(),
            EP_Sell_To__c= shipTo.ParentId ,EP_ShipTo__c = shipTo.Id,EP_Delivery_Type__c='Ex-Rack',
            EP_Expected_Delivery_Date__c = Date.today(),Status__c='Draft');
 
 
        insert csOrder;
        System.Assert(csOrder != null);
        return csOrder;
 
    }
}