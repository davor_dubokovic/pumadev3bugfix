/**
@Author <Spiros Markantonatos>
@name <EP_IntegrationErrorOnSidebarController>
@CreateDate <31/08/2016>
@Description <This is controller class is used to retrieve the integration errors displayed on the Salesforce sidebar>
@Version <1.0>
*/
public with sharing class EP_IntegrationErrorOnSidebarController {
	private static final String MIDDLEWARE_TRANSPORT_ERROR_TYPE = 'Middleware Transport Error';
	private static final String TARGET_TRANSPORT_ERROR_TYPE = 'Target Transport Error';
	private static final String FUNCTIONAL_TRANSPORT_ERROR_TYPE = 'Functional Error';
	private static final String MY_ERROR_LIST_VIEW_LABEL = '1. My Failed Integration Records';
	private static final String INTEGRATION_RECORD_OBJECT_API_NAME = 'EP_IntegrationRecord__c';
	private static final Integer MIDDLEWARE_ERROR_LIMIT = 1;
	private static final Integer FUNCTIONAL_ERROR_LIMIT = 10;
	private static final Integer TARGET_ERROR_LIMIT = 5000;
	private static final String S_COMPARISON = 'S';
	private static final String SPACE_WITH_BLANK = ' ';
	private static final String querystring = 'SELECT Name FROM EP_IntegrationRecord__c LIMIT 1';
	private static final String queryStringForIntegrationRecord = 'SELECT COUNT() FROM EP_IntegrationRecord__c WHERE ';
	private static final String queryStringForIntegrationRecordWhereClause1 = ' EP_Integration_Error_Type__c != :MIDDLEWARE_TRANSPORT_ERROR_TYPE AND EP_Integration_Failed__c = TRUE AND EP_IsLatest__c = TRUE AND EP_Is_Error_Resolved__c = FALSE'; 
	private static final String queryStringForIntegrationRecordWhereClause2 = ' AND EP_Is_My_Integration_Record__c = TRUE'; 
	private static final String URL_VALUE = '?fcf=';

	private static final String CLASS_NAME = 'EP_IntegrationErrorOnSidebarController';
	private static final String LOAD_WARNING_METHOD = 'loadIntegrationFrameworkErrors';
	private static final String DISMISS_ERROR_METHOD = 'dismissIntegrationError';

	// Properties
	public List<IntegrationErrorClass> listFunctionalIntegrationErrors {get;set;}
	public List<IntegrationErrorClass> listTransportIntegrationErrors {get;set;}

	public String selectedIntegrationRecordString {get;set;}

	public Integer totalNumberOfIntegrationErrors {get;set;}
	public Integer numberOfDisplayedIntegrationErrors {get;set;}

	public Boolean middlewareIntegrationErrorBoolean {
		get {
			// Retrieve the middleware integration framework errors (if any)
			if (middlewareIntegrationErrorBoolean == NULL){
				middlewareIntegrationErrorBoolean = FALSE;
			}

			return middlewareIntegrationErrorBoolean;
		}
		set;
	}
	
	/**
	* @author <Spiros Markantonatos>
	* @date <31/08/2016>
	* @name <EP_IntegrationErrorOnSidebarController>
	* @description <Constructor>
	* @param none
	* @return none
	*/
	public EP_IntegrationErrorOnSidebarController() {
		EP_DebugLogger.printDebug('++'+middlewareIntegrationErrorBoolean);
	}
	
	/**
	* @author <Spiros Markantonatos>
	* @date <31/08/2016>
	* @name <openMyErrorListView>
	* @description <This property will navigate the user to the "My Errors" integration record list view>
	* @param none
	* @return PageReference
	*/
	public PageReference openMyErrorListView {
		get {
			PageReference ref = NULL;
			String prefixString = NULL;

			// Get the object prefix
			Map<String, Schema.SObjectType> mapGlobalDescribes = Schema.getGlobalDescribe();
			for(String sObj : mapGlobalDescribes.keySet()){
				Schema.DescribeSObjectResult r =  mapGlobalDescribes.get(sObj).getDescribe();
				if (r.getName() == INTEGRATION_RECORD_OBJECT_API_NAME){
					prefixString = r.getKeyPrefix();
					break;
				}
			}

			ApexPages.StandardSetController integrationRecordPage = new ApexPages.StandardSetController(
			Database.getQueryLocator(querystring));
			List<SelectOption> listViewOptions = integrationRecordPage.getListViewOptions();
			for(SelectOption so : listViewOptions){
				if(so.getLabel() == MY_ERROR_LIST_VIEW_LABEL){
					ref = new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + EP_Common_Constant.SLASH + prefixString + URL_VALUE + so.getValue().left(15));
				}
			}
			return ref;
		}
	}
	
	/**
	* @author <Spiros Markantonatos>
	* @date <31/08/2016>
	* @name <loadIntegrationFrameworkErrors>
	* @description <This method will populate the internal variables used to display the integration errors on the sidebar page component>
	* @param none
	* @return null
	*/
	public PageReference loadIntegrationFrameworkErrors() {

		// Reset the wrapper classes
		listFunctionalIntegrationErrors = new List<IntegrationErrorClass>();
		listTransportIntegrationErrors = new List<IntegrationErrorClass>();

		// Reset error counter variables
		totalNumberOfIntegrationErrors = 0;
		numberOfDisplayedIntegrationErrors = 0;

		// 1. Retrieve the middleware connectivity integration framework errors (if any)
		// Check the latest integration record that the user has created
		middlewareIntegrationErrorBoolean = FALSE;
		List<EP_IntegrationRecord__c> listIntegrationRecordsUpdated = [SELECT Id, Name, EP_Target__c, EP_Is_Notification_Dismissed__c, EP_Object_Type__c, EP_Object_ID__c, EP_Error_Description__c, EP_Object_Record_Name__c
		FROM EP_IntegrationRecord__c
		WHERE EP_Integration_Error_Type__c = :MIDDLEWARE_TRANSPORT_ERROR_TYPE
		AND EP_Integration_Failed__c = TRUE
		AND EP_IsLatest__c = TRUE AND EP_Is_Error_Resolved__c = FALSE 
		AND EP_Is_Notification_Dismissed__c = FALSE
		AND EP_Is_My_Integration_Record__c = TRUE
		ORDER BY CreatedDate DESC
		LIMIT :MIDDLEWARE_ERROR_LIMIT];

		for (EP_IntegrationRecord__c ir : listIntegrationRecordsUpdated){
			middlewareIntegrationErrorBoolean = TRUE;
			ir.EP_Is_Notification_Dismissed__c = TRUE;
		}

		if (!listIntegrationRecordsUpdated.isEmpty()){
			try {
				Database.update(listIntegrationRecordsUpdated);
			}
			catch(Exception ex){
				EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, CLASS_NAME, 
				LOAD_WARNING_METHOD, ApexPages.Severity.ERROR);
			}
		}

		// 2. Retrieve the target solution connectivity integration framework errors (if any)
		Map<String, String> mapTargetApplication = new Map<String, String>();
		String strConnectionErrorMessage = Label.EP_Unable_To_Connect_Error + SPACE_WITH_BLANK;
		IntegrationErrorClass intErrorClass = null;
		for (EP_IntegrationRecord__c ir : [SELECT Id, Name, EP_Target__c, EP_Object_Type__c, EP_Object_ID__c, EP_Error_Description__c, EP_Object_Record_Name__c
		FROM EP_IntegrationRecord__c
		WHERE EP_Integration_Error_Type__c = :TARGET_TRANSPORT_ERROR_TYPE
		AND EP_Integration_Failed__c = TRUE
		AND EP_IsLatest__c = TRUE AND EP_Is_Error_Resolved__c = FALSE 
		AND EP_Is_Notification_Dismissed__c = FALSE
		AND EP_Is_My_Integration_Record__c = TRUE
		AND CreatedDate >= LAST_N_DAYS:14
		ORDER BY CreatedDate DESC
		LIMIT :TARGET_ERROR_LIMIT]){
			if (!mapTargetApplication.containsKey(ir.EP_Target__c)){
				mapTargetApplication.put(ir.EP_Target__c, ir.EP_Target__c);
				intErrorClass = new IntegrationErrorClass(ir.ID, ir.EP_Object_Record_Name__c, 
				generateObjectName(ir.EP_Object_Type__c), 
				strConnectionErrorMessage + ir.EP_Target__c,
				generateRecordURL(ir.ID),
				ir.EP_Target__c,
				generateRecordURL(ir.EP_Object_ID__c));
				listTransportIntegrationErrors.add(intErrorClass);
			}
		}

		// 3. Retrieve the functional integration framework errors (if any)
		for (EP_IntegrationRecord__c ir : [SELECT Id, Name, EP_Target__c, EP_Object_Type__c, EP_Object_ID__c, EP_Error_Description__c, EP_Object_Record_Name__c
		FROM EP_IntegrationRecord__c
		WHERE EP_Integration_Error_Type__c = :FUNCTIONAL_TRANSPORT_ERROR_TYPE
		AND EP_Integration_Failed__c = TRUE
		AND EP_IsLatest__c = TRUE AND EP_Is_Error_Resolved__c = FALSE 
		AND EP_Is_Notification_Dismissed__c = FALSE
		AND EP_Is_My_Integration_Record__c = TRUE
		ORDER BY CreatedDate DESC
		LIMIT :FUNCTIONAL_ERROR_LIMIT]){
			intErrorClass = new IntegrationErrorClass(ir.ID, ir.EP_Object_Record_Name__c, 
			generateObjectName(ir.EP_Object_Type__c), 
			ir.EP_Error_Description__c,
			generateRecordURL(ir.ID),
			ir.EP_Target__c,
			generateRecordURL(ir.EP_Object_ID__c));
			listFunctionalIntegrationErrors.add(intErrorClass);
		}

		String strFunctionalErrorCountQuery = queryStringForIntegrationRecord;
		strFunctionalErrorCountQuery += queryStringForIntegrationRecordWhereClause1;
		strFunctionalErrorCountQuery += queryStringForIntegrationRecordWhereClause2;

		numberOfDisplayedIntegrationErrors = listFunctionalIntegrationErrors.size() + listTransportIntegrationErrors.size();
		totalNumberOfIntegrationErrors = Database.countQuery(strFunctionalErrorCountQuery);

		return NULL;
	}

	/**
	* @author <Spiros Markantonatos>
	* @date <31/08/2016>
	* @name <dismissIntegrationError>
	* @description <This method will dismiss the integration error from appearing on the "My Errors" sidebar component>
	* @param none
	* @return null
	*/
	public PageReference dismissIntegrationError() {

		if (selectedIntegrationRecordString != NULL){
			EP_IntegrationRecord__c integrationRecordToUpdate = new EP_IntegrationRecord__c(ID = selectedIntegrationRecordString,
			EP_Is_Notification_Dismissed__c = TRUE);

			try {
				Database.update(integrationRecordToUpdate);
			} catch(Exception ex){
				EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, CLASS_NAME, 
				DISMISS_ERROR_METHOD, ApexPages.Severity.ERROR);
			}
		}

		return NULL;
	}
	
	/**
	* @author <Spiros Markantonatos>
	* @date <31/08/2016>
	* @name <generateObjectName>
	* @description <This method will generate ObjectName>
	* @param String
	* @return String
	*/
	private String generateObjectName(String objectNameString) {
		if (objectNameString != NULL){
			if(S_COMPARISON.equals(objectNameString.right(1).toUpperCase())){
				objectNameString = objectNameString.left(objectNameString.length() - 1);
			}
		}

		return objectNameString;
	}
	
	/**
	* @author <Spiros Markantonatos>
	* @date <31/08/2016>
	* @name <generateRecordURL>
	* @description <This method will generate RecordURL>
	* @param String
	* @return PageReference
	*/
	private PageReference generateRecordURL(String strIntegrationRecordID) {
		return new PageReference(EP_Common_Constant.SLASH + strIntegrationRecordID);
	}


	/**
	@Author <Spiros Markantonatos>
	@name <IntegrationErrorClass>
	@CreateDate <31/08/2016>
	@Description <Inner Class>
	@Version <1.0>
	*/
	public with sharing class IntegrationErrorClass {
		public String strRecordID {get;set;}
		public String strRecordName {get;set;}  
		public String objectNameString {get;set;}
		public String strErrorDescription {get;set;}
		public PageReference prRecordLink {get;set;}
		public String strTargetApplicationName {get;set;}
		public PageReference prSourceRecordLink {get;set;}

		/**
		* @author <Spiros Markantonatos>
		* @date <31/08/2016>
		* @name <IntegrationErrorClass>
		* @description <Inner Class Constructor>
		* @param none
		* @return PageReference
		*/
		public IntegrationErrorClass(String strRecordID, String strRecordName, 
		String objectNameString, String strErrorDescription, 
		PageReference prRecordLink, String strTargetApplicationName,
		PageReference prSourceRecordLink) {
			this.strRecordID = strRecordID;
			this.strRecordName = strRecordName;
			this.objectNameString = objectNameString;
			this.strErrorDescription = strErrorDescription;
			this.prRecordLink = prRecordLink;
			this.strTargetApplicationName = strTargetApplicationName;
			this.prSourceRecordLink = prSourceRecordLink;
		}
	}
}