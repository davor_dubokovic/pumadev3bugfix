/**
  * @Author      : Kalpesh Thakur<kalpesh.j.thakur@accenture.com>
  * @name        : EP_VendorManagement
  * @CreateDate  : 31/01/2017
  * @Description : This class is Parent for VMI and NonVMI Strategies and contains Common Code Of VMI and NonVMI
  * @Version     : <1.0>
  * @reference   : N/A
  */

  public virtual class EP_VendorManagement{
    
    /** This method updates the payment terms and methods, this method will we be overriden by the child 
    *  @date      01/02/2017
    *  @name      updatePaymentTermAndMethod
    *  @param     Order objOrd
    *  @return    NA
    *  @throws    NA
    */
    public virtual void doUpdatePaymentTermAndMethod(csord__Order__c orderObj) {
      EP_GeneralUtility.Log('Public','EP_VendorManagement','doUpdatePaymentTermAndMethod');
    }

   /** This method sets payment term for packaged order 
    *  @date      01/02/2017
    *  @name      setPaymntTermValForPckgOdr
    *  @param     String PckgdPaymntTerm, Order ord
    *  @return    Order
    *  @throws    NA
    */
    public void setPaymntTermValForPckgOdr(String PckgdPaymntTerm, csord__Order__c orderObj) {
      EP_GeneralUtility.Log('Public','EP_VendorManagement','setPaymntTermValForPckgOdr');

      if(isPackagedProdCategoryAndPayTermISNOTNULL(PckgdPaymntTerm,orderObj)){
        
        orderObj.EP_Payment_Term_Value__c = PckgdPaymntTerm;
      }
    }


    /** This method checks for Packaged Order and Product Category and Payment is not null. 
    *  @date      16/02/2017
    *  @name      isPackagedProdCategoryAndPayTermISNOTNULL
    *  @param     String PckgdPaymntTerm, Order ord
    *  @return    Boolean
    *  @throws    NA
    */
    public Boolean isPackagedProdCategoryAndPayTermISNOTNULL(String PckgdPaymntTerm, csord__Order__c orderObj){
      EP_GeneralUtility.Log('Public','EP_VendorManagement','isPackagedProdCategoryAndPayTermISNOTNULL');

      return (orderObj.EP_Order_Product_Category__c != NULL && PckgdPaymntTerm != NULL 
        && orderObj.EP_Order_Product_Category__c.equalsIgnoreCase(EP_Common_Constant.PRODUCT_PACKAGED));
    }
    

    /** This method sets payment term for packaged order 
     *  @date      01/02/2017
     *  @name      setLocalNumberonOrder
     *  @param     List<EP_Customer_Support_Settings__c> localSupportNumbers,Map<Id,Account> mapAccount
     *  @return    NA
     *  @throws    NA
     */
     public void setLocalNumberonOrder(List<EP_Customer_Support_Settings__c> localSupportNumbers,Account objAccount , csord__Order__c orderObj){
      EP_GeneralUtility.Log('Public','EP_VendorManagement','setLocalNumberonOrder');

      for(EP_Customer_Support_Settings__c instance : localSupportNumbers) {
       
        if(instance.EP_Company_Code__c == objAccount.EP_Puma_Company_Code__c) {

          orderObj.EP_Customer_Local_Number__c = instance.EP_Phone__c;
          break;
        }
      } 
    }

    /**  to fetch the record type of order
      *  @date      05/02/2017
      *  @name      findRecordType
      *  @param     NA
      *  @return    Id 
      *  @throws    NA
      */
      public virtual Id findRecordType() {
        EP_GeneralUtility.Log('Public','EP_VendorManagement','findRecordType');

        return null;
      }
    }