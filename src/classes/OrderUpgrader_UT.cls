@isTest
private class OrderUpgrader_UT {
    
    @testSetup 
    private static void setup() {
        
        Account acc = new Account();
        acc.EP_VendorType__c = '3rd Party Stock Supplier';
        acc.EP_Vendor_Type__c = '3rd Party Stock Supplier';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        acc.Name = 'Test';
        acc.EP_Status__c = '05-Active';
        acc.cscfga__Active__c = 'Yes';
        
        insert acc;
        
        cscfga__Product_Definition__c productDefinition = new cscfga__Product_Definition__c();
        productDefinition.Name = 'Puma Energy Order [Archived]';
        productDefinition.cscfga__Description__c = 'Puma Energy Order';
        
        insert productDefinition;
        
        List<cscfga__attribute_definition__c> attributeDefinitions = new List<cscfga__attribute_definition__c>();
        
        cscfga__attribute_definition__c ad = new cscfga__attribute_definition__c();
        
        ad.Name = 'Test';
        ad.cscfga__Data_Type__c = 'String';
        ad.cscfga__Type__c = 'Text Display';
        ad.cscfga__Product_Definition__c = productDefinition.Id;
        
        attributeDefinitions.add(ad);
         
        cscfga__Product_Configuration__c testProductConfiguration = new cscfga__Product_Configuration__c();
        testProductConfiguration.Name = 'Puma Energy Order';
        testProductConfiguration.cscfga__Product_Definition__c = productDefinition.Id;
        
        insert testProductConfiguration;
        
        csord__Order__c ord = new csord__Order__c(Name='Test Order',
                                                  csord__Account__c = acc.Id,
                                                  AccountId__c = acc.Id,
                                                  EP_Sell_To__c = acc.Id,
                                                  csord__Status__c = 'Awaiting Credit Review',
                                                  csord__Status2__c = 'Awaiting Credit Review',
                                                  ep_integration_status__c = 'FAILURE',
                                                  csord__Identification__c = '9SADGA83JFOW24234R24',
                                                  csordtelcoa__Product_Configuration__c = testProductConfiguration.Id);
                                                  
        insert ord;
                                                  
        cscfga__Product_Definition__c productDefinitionNew = new cscfga__Product_Definition__c();
        productDefinitionNew.Name = 'Puma Energy Order';
        productDefinitionNew.cscfga__Description__c = 'Puma Energy Order';
        
        insert productDefinitionNew;
        
        cscfga__attribute_definition__c adNew = new cscfga__attribute_definition__c();
        
        adNew.Name = 'Test';
        adNew.cscfga__Data_Type__c = 'String';
        adNew.cscfga__Type__c = 'Text Display';
        adNew.cscfga__Product_Definition__c = productDefinitionNew.Id;
        
        attributeDefinitions.add(adNew);
        
        insert attributeDefinitions;
        
        cscfga__Product_Definition_Version__c pdv = new cscfga__Product_Definition_Version__c();
        
        pdv.cscfga__Original_Definition__c = productDefinition.Id;
        pdv.cscfga__Current_Definition__c = productDefinitionNew.Id;
        pdv.cscfga__Replacement_Definition__c = productDefinitionNew.Id;
        pdv.cscfga__Replaced_On__c = Date.today();
        
        insert pdv;
    }
    
    @isTest
    private static void orderUpdaterTest() {
        
        Test.startTest();
        
        OrderUpgrader upgrade = new OrderUpgrader(); 
        Id batchId = Database.executeBatch(upgrade, 50);
        
        Test.stopTest();
        
        System.assertEquals(1, [SELECT count() FROM csord__Order__c WHERE csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__r.Name = 'Puma Energy Order'], 'Invalid data');
    }
}