public class TestingFlow2FAuth {

	@InvocableMethod(label='Check User OTP' description='Check if the OTP specified by the user is valid')
	public static List<Boolean> getAccountNames(List<String> otps) {
		// Prepare the output 
		List<Boolean> outputs = new List<Boolean>();

		// Check if we have just one code
		if(otps != null && otps.size() == 1) {
			String code = otps.get(0);

			// Clean input
			if(code != null) {
				code = code.replaceAll(',', '');
			}
			System.debug('2FA Code is ' + code);

			// Check the OTP Code for the running user
			Boolean authValidation = false;
			try {
				authValidation = Auth.SessionManagement.validateTotpTokenForUser(otps.get(0), 'User is logging in...');	
				System.debug('2FA Code check result is ' + authValidation);
			} catch(Exception e) {
				System.debug('2FA Code check result exception ' + e);
			}
			
			outputs.add(authValidation);
		} else {
			outputs.add(false);
		}

		return outputs;
	}

	/*

	Map<String,String> session = Auth.SessionManagement.getCurrentSession();
    String sessionLevel = session.get('SessionSecurityLevel');

	Auth.SessionManagement.setSessionLevel(Auth.SessionLevel.HIGH_ASSURANCE);
    */

}