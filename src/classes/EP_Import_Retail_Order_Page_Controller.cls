public with sharing class EP_Import_Retail_Order_Page_Controller{

    public EP_Import_Retail_Order_Context ctx {get;set;}
    public EP_Import_Retail_Order_Helper xmlHelper{get;set;}
    public EP_File__c fileRecord;

    public EP_Import_Retail_Order_Page_Controller(ApexPages.StandardController controller)
    {
         fileRecord = (EP_File__c) controller.getRecord();
         ctx = new EP_Import_Retail_Order_Context (fileRecord);
           
    }
    
     public PageReference parseCSVFile() {
      system.debug('**fileRecord**' + ctx.ctxfileRecord);
      system.debug('**ctx **' + ctx.blbXMLFile.toString() );
      xmlHelper = new EP_Import_Retail_Order_Helper(ctx.blbXMLFile.toString());
     // xmlHelper.processXMLData();
      /*try {
        if(csvHelper.hasValidFile()) {
          csvHelper.processCSVData();
          csvHelper.processImportRequest();
          csvHelper.initDataValition();
        }
      } catch (exception exp){
        ctx.CSVParseSuccess = false;
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,system.Label.EP_CSV_IMPORT_ERROR_MSG + EP_Common_Constant.COLON_WITH_SPACE + exp.getMessage()));
        system.debug('**Exception** ' + exp.getMessage()+' ***' + exp.getStackTraceString());
      }*/
       return null;
    } 
}