@isTest
public class TrafiguraEditConfigurationController_UT {
    
    @isTest
    static void openEditOrderPage_test(){
        
        Account newAccount = Ep_TestDataUtility.getSellTo();
        csord__Order__c newOrder = EP_TestDataUtility.createCSOrder(newAccount.Id, null, null);
        insert newOrder;
        
        PageReference pageRef = Page.EditOrder;
        pageRef.getParameters().put('id', newOrder.Id);
        Test.setCurrentPage(pageRef);

        Test.startTest();
        TrafiguraEditConfigurationController controller = new TrafiguraEditConfigurationController();
        PageReference pr = controller.openEditOrderPage();
        Test.stopTest();
        
        System.assertEquals('System.PageReference[/apex/customconfiguration?configId=null&linkedId='+String.valueOf(newOrder.csord__Identification__c)+']', 
                            String.valueOf(pr), 'Wrong string value returned.');
    }
    
    @isTest
    static void openEditOrderLinePageFromStandard_test(){
        
        Pricebook2 pricebook = new Pricebook2(Name = 'Standard Price Book', isActive = TRUE);
        insert pricebook;
        System.Assert(pricebook.Id != null);
        
        Product2 prod = new Product2(Name = 'Product');
        insert prod;
        System.Assert(prod.Id != null);
        
        PricebookEntry entry = new PricebookEntry();
        entry.UnitPrice = 20;
        entry.Pricebook2Id = pricebook.Id;
        entry.Product2Id = prod.Id;
        insert entry;
        System.Assert(entry.Id != null);
        
        Account acc = new Account();
        acc.EP_VendorType__c = '3rd Party Stock Supplier';
        acc.EP_Vendor_Type__c = '3rd Party Stock Supplier';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        acc.Name='testEPAccount';
        acc.EP_Status__c = '06-Blocked';
        insert acc;
        
        cscfga__Product_Basket__c testBasket = new cscfga__Product_Basket__c();
        testBasket.csordtelcoa__Account__c = acc.Id;
        INSERT testBasket;
        
        cscfga__Product_Configuration__c testProductConfiguration = new cscfga__Product_Configuration__c();
        testProductConfiguration.Name = 'Puma Energy Order Line';
        testProductConfiguration.cscfga__Product_Basket__c = testBasket.Id;
        INSERT testProductConfiguration;
        
        System.Assert(acc.Id != null);
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ord.Cancellation_Check_Done__c = false;
        ord.EP_Delivery_Type__c  = 'Delivery';
        ord.csord__Status2__c    = 'PLANNED';
        ord.csord__Identification__c = testBasket.Id;
        update ord;
        System.AssertEquals(ord.EP_Order_Locked__c, false);
        
        System.Assert(ord.Id != null);        
        csord__Order_Line_Item__c oli = new csord__Order_Line_Item__c();
        oli.Quantity__c = 10;
        oli.EP_Company_Code__c = 'CA';
        oli.EP_Product_Code__c = 'CB';
        oli.csord__Order__c = ord.Id;
        oli.csord__Identification__c = 'testOLI0';
        insert oli;
        
        PageReference pageRef = Page.EditOrder;
        pageRef.getParameters().put('id', oli.Id);
        pageRef.getParameters().put('retUrl', oli.Id);
        Test.setCurrentPage(pageRef);

        Test.startTest();
        TrafiguraEditConfigurationController controller = new TrafiguraEditConfigurationController();
        PageReference pr = controller.openEditOrderLinePageFromStandard();
        ord.Cancellation_Check_Done__c = true;       
        update ord;    
            
        PageReference pageRefLocked = Page.EditOrder;
        pageRefLocked.getParameters().put('id', oli.Id);
        pageRefLocked.getParameters().put('retUrl', oli.Id);
        Test.setCurrentPage(pageRefLocked);    
        PageReference pageRe = controller.openEditOrderLinePageFromStandard();  

        PageReference pageReException = Page.EditOrder;
        pageReException.getParameters().put('id', oli.Id);
        pageReException.getParameters().put('retUrl', null);
        Test.setCurrentPage(pageReException);    
        PageReference pageRefEx = controller.openEditOrderLinePageFromStandard();       
        System.AssertEquals(ord.Cancellation_Check_Done__c, true); 
                
        Test.stopTest();
        
    }
    
    @isTest
    static void openEditOrderPageFromStandard_test(){
        
        Pricebook2 pricebook = new Pricebook2(Name = 'Standard Price Book', isActive = TRUE);
        insert pricebook;
        System.Assert(pricebook.Id != null);
        
        Product2 prod = new Product2(Name = 'Product');
        insert prod;
        System.Assert(prod.Id != null);
        
        PricebookEntry entry = new PricebookEntry();
        entry.UnitPrice = 20;
        entry.Pricebook2Id = pricebook.Id;
        entry.Product2Id = prod.Id;
        insert entry;
        System.Assert(entry.Id != null);
        
        Account acc = new Account();
        acc.EP_VendorType__c = '3rd Party Stock Supplier';
        acc.EP_Vendor_Type__c = '3rd Party Stock Supplier';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        acc.Name='testEPAccount';
        acc.EP_Status__c = '06-Blocked';
        insert acc;
        
        cscfga__Product_Basket__c testBasket = new cscfga__Product_Basket__c();
        testBasket.csordtelcoa__Account__c = acc.Id;
        INSERT testBasket;
        
        cscfga__Product_Configuration__c testProductConfiguration = new cscfga__Product_Configuration__c();
        testProductConfiguration.Name = 'Puma Energy Order Line';
        testProductConfiguration.cscfga__Product_Basket__c = testBasket.Id;
        INSERT testProductConfiguration;
        
        System.Assert(acc.Id != null);
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ord.Cancellation_Check_Done__c = false;
        ord.EP_Delivery_Type__c  = 'Delivery';
        ord.csord__Status2__c    = 'PLANNED';
        ord.csord__Identification__c = testBasket.Id;
        update ord;
        System.AssertEquals(ord.EP_Order_Locked__c, false);
        
        System.Assert(ord.Id != null);        
        csord__Order_Line_Item__c oli = new csord__Order_Line_Item__c();
        oli.Quantity__c = 10;
        oli.EP_Company_Code__c = 'CA';
        oli.EP_Product_Code__c = 'CB';
        oli.csord__Order__c = ord.Id;
        oli.csord__Identification__c = 'testOLI0';
        insert oli;
        
        PageReference pageRef = Page.EditOrder;
        pageRef.getParameters().put('id', ord.Id);
        pageRef.getParameters().put('retUrl', ord.Id);
        Test.setCurrentPage(pageRef);

        Test.startTest();
        TrafiguraEditConfigurationController controller = new TrafiguraEditConfigurationController();
        PageReference pr = controller.openEditOrderPageFromStandard();
        ord.Cancellation_Check_Done__c = true;       
        update ord;     
            
        PageReference pageRefLocked = Page.EditOrder;
        pageRefLocked.getParameters().put('id', ord.Id);
        pageRefLocked.getParameters().put('retUrl', ord.Id);
        Test.setCurrentPage(pageRefLocked);    
        PageReference pageRe = controller.openEditOrderPageFromStandard();  

        PageReference pageReException = Page.EditOrder;
        pageReException.getParameters().put('id', ord.Id);
        pageReException.getParameters().put('retUrl', null);
        Test.setCurrentPage(pageReException);    
        PageReference pageRefEx = controller.openEditOrderPageFromStandard();       
        System.AssertEquals(ord.Cancellation_Check_Done__c, true); 
                
        Test.stopTest();
        
    }
    
    @isTest
    static void openEditOrderPage_cancelCheckDoneTrue_test(){
        
        Account newAccount = Ep_TestDataUtility.getSellTo();
        csord__Order__c newOrder = EP_TestDataUtility.createCSOrder(newAccount.Id, null, null);
        newOrder.Cancellation_Check_Done__c = true;
        insert newOrder;
        
        PageReference pageRef = Page.EditOrder;
        pageRef.getParameters().put('id', newOrder.Id);
        Test.setCurrentPage(pageRef);

        Test.startTest();
        TrafiguraEditConfigurationController controller = new TrafiguraEditConfigurationController();
        PageReference pr = controller.openEditOrderPage();
        Test.stopTest();
        
        System.assertEquals('System.PageReference[/apex/customconfiguration?configId=null&linkedId='+String.valueOf(newOrder.csord__Identification__c)+']', 
                            String.valueOf(pr), 'Wrong string value returned.');
    }
    
    @isTest
    static void openEditOrderPageWithId_test(){
        
        Account newAccount = Ep_TestDataUtility.getSellTo();
        csord__Order__c newOrder = EP_TestDataUtility.createCSOrder(newAccount.Id, null, null);
        insert newOrder;
        
        Test.startTest();
        try {
          String prExceptionTest = TrafiguraEditConfigurationController.openEditOrderPageWithId(null);
        }
        catch (Exception ex){
            
        }
        String propenEditOrderPageWithId2_Test = TrafiguraEditConfigurationController.openEditOrderPageWithId2(newOrder.Id);
        String pr = TrafiguraEditConfigurationController.openEditOrderPageWithId(newOrder.Id);
        PageReference pageRe = Page.EditOrder;
        pageRe.getParameters().put('id', newOrder.Id);
        pageRe.getParameters().put('retUrl', null);
        Test.setCurrentPage(pageRe);    
        String prRefNull = TrafiguraEditConfigurationController.openEditOrderPageWithId(newOrder.Id);       
        Test.stopTest();
        System.assertEquals('/apex/customconfiguration?configId=null&linkedId='+String.valueOf(newOrder.csord__Identification__c), pr, 'Wrong string value returned.');
    }
    
}