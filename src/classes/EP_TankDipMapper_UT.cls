/**
@Author Accenture
@name <EP_TankMapper_UT>
@CreateDate 01/02/2017
@Description This class will be use to test/cover unit test cases for EP_TankMapper class
@Version <1.0>
@reference NA
*/
@isTest 
private class EP_TankDipMapper_UT {   
   
    static EP_Tank_Dip__c createTankDip(Boolean insertRec){          
        EP_AccountDataWrapper adw = new EP_AccountDataWrapper();
        adw.accountType = EP_AccountConstant.SHIP_TO;
        adw.vendorType = EP_AccountConstant.VMI;
        adw.status = EP_Common_Constant.STATUS_PROSPECT;
        adw.scenarioType = EP_Common_Constant.POSITIVE;
        adw.isTankRequired = true;
        Account acct = EP_AccountTestDataUtility.getAccount(adw);                 
        EP_Tank__c  tank = EP_testDataUtility.createTestEP_Tank(acct);  
        EP_Tank_Dip__c tankDip = EP_testDataUtility.createTestRecordsForTankDip(tank.id);
        tankDip.RecordTypeId = EP_TestDataUtility.RECORDTYPEID_PLACEHOLDER;
        //tankDip.EP_Ambient_Quantity__c = 5000;
        tankDip.EP_Ambient_Quantity__c = 50;         
        tankDip.EP_Tank_Dip_Exported__c = FALSE ;
        //tankDip.EP_Reading_Date_Time__c= System.now().addDays(-1);
        if(insertRec) {
            insert tankDip;
         }  
         EP_Tank_Dip__c TankDipTest = [SELECT EP_Reading_Date_Time__c , EP_Tank__r.EP_Ship_To__r.EP_Ship_To_Current_Date_Time__c from EP_Tank_Dip__c];
         return tankDip;       
    }   
    static testMethod void getPlaceHolderTankDipRecords_test() {
        EP_TankDipMapper localObj = new EP_TankDipMapper(); 
        set<Id> idSet = new Set<id>{createTankDip(true).EP_Tank__c};                  
        Id recordTypeId = EP_TestDataUtility.RECORDTYPEID_PLACEHOLDER; 
        Test.startTest();
        list<EP_Tank_Dip__c> result = localObj.getPlaceHolderTankDipRecords(idSet,recordTypeId); 
        Test.stopTest();
        System.AssertEquals(1,result.size());
    }     
    static testMethod void getTankDipsByIds_PositiveTest() {
        EP_TankDipMapper localObj = new EP_TankDipMapper();
        set<String> idSet = new Set<String>{createTankDip(true).id};
        Test.startTest();
        list<EP_Tank_Dip__c> result = localObj.getTankDipsByIds(idSet);
        Test.stopTest();
        System.AssertEquals(1,result.size());
    }
    static testMethod void getTankDipsByIds_NegativeTest() {
        EP_TankDipMapper localObj = new EP_TankDipMapper();
        set<String> idSet = new Set<String>();
        Test.startTest();
        list<EP_Tank_Dip__c> result = localObj.getTankDipsByIds(idSet);
        Test.stopTest();
        System.AssertEquals(0,result.size());
    }
    static testMethod void getTankDipsToExport_PositiveTest() {
        EP_TankDipMapper localObj = new EP_TankDipMapper(); 
        EP_Tank_Dip__c TankDip = createTankDip(true);
        TankDip.RecordTypeId = Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get(EP_Common_Constant.Actual).getRecordTypeId();
        tankDip.EP_Reading_Date_Time__c= System.now().addDays(-1);
        update TankDip;
        EP_Tank_Dip__c TankDipTest = [SELECT EP_Reading_Date_Time__c , EP_Tank__r.EP_Ship_To__r.EP_Ship_To_Current_Date_Time__c from EP_Tank_Dip__c];
        Test.startTest();
        list<EP_Tank_Dip__c> result = localObj.getTankDipsToExport(); 
        Test.stopTest();
        System.AssertEquals(result.size(),1);
    }     
    static testMethod void getTankDipsToExport_NegativeTest() {
        EP_TankDipMapper localObj = new EP_TankDipMapper();
        Test.startTest();
        list<EP_Tank_Dip__c> result = localObj.getTankDipsToExport();
        Test.stopTest();
        System.AssertEquals(result.size(), 0);
    }
}