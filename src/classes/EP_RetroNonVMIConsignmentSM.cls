/*   
     @Author Aravindhan Ramalingam
     @name <EP_RetroNonVMIConsignmentSM.cls>     
     @Description <Retro VMI Non Consignment Statemachine >   
     @Version <1.1> 
     */

     public class EP_RetroNonVMIConsignmentSM extends EP_OrderStateMachine {
     	
     	public EP_RetroNonVMIConsignmentSM(){

     	}
     	
     	public override EP_OrderState getOrderState(EP_OrderEvent currentEvent){
     		EP_GeneralUtility.Log('Public','EP_RetroNonVMIConsignmentSM','getOrderState');
     		return super.getOrderState(currentEvent);
     	}
     }