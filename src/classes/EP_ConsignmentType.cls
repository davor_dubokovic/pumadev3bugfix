/**
* @Author      : Kalpesh Thakur<kalpesh.j.thakur@accenture.com>
* @name        : EP_ConsignmentType
* @CreateDate  : 05/02/2017
* @Description : This class is Parent for Consignment  and Non Consignment Strategies and contains Common Code Of Consignment and Non Consignment
* @Version     : <1.0>
* @reference   : N/A
*/
public virtual class EP_ConsignmentType {

    public EP_ConsignmentType () {
    }


    /** This method is used for credit check
     *  @date      05/02/2017
     *  @name      hasCreditIssue
     *  @param     Order objOrder
     *  @return    boolean 
     *  @throws    NA
     */  
     public virtual boolean hasCreditIssue(csord__Order__c orderObj) {
        EP_GeneralUtility.Log('Public','EP_ConsignmentType','hasCreditIssue');
        return false;
     }


    /**  to fetch the record type of order
     *  @date      05/02/2017
     *  @name      findRecordType
     *  @param     EP_VendorManagement vmType
     *  @return    Id 
     *  @throws    NA
     */
     public virtual Id findRecordType(EP_VendorManagement vmType) {
        EP_GeneralUtility.Log('Public','EP_ConsignmentType','findRecordType');
        return null;
     }

    /**  Calculate Price by sending request to the Pricing engine.
     *  @date      05/02/2017
     *  @name      calculatePrice
     *  @param     Order orderObj
     *  @return    NA
     *  @throws    NA
     */
     public virtual void calculatePrice(csord__Order__c orderObj) {
        EP_GeneralUtility.Log('Public','EP_ConsignmentType','calculatePrice');
     }
     
     //Defect 57278 Start
    /** 		
        * @Author     	Accenture
        * @Date       	04/28/2017
        * @Name       	calculatePrice
        * @Description	This method gets amount of open orders
        * @Param      	Order 
        * @Return     	Double 
        * @Throws     	NA
    */
    public virtual Double getCreditAmount(csord__Order__c orderObj) {
        EP_GeneralUtility.Log('Public','EP_ConsignmentType','getCreditAmount');
        return 0;
    } 
    //Defect 57278 Start

    }