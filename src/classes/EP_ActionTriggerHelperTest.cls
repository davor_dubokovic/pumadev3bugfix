/**
 * @author <Sandeep Kumar>
 * @name <EP_ActionTriggerHelperTest>
 * @createDate <06/11/2016>
 * @description
 * @version <1.0>
 */
@isTest
public class EP_ActionTriggerHelperTest {
    Static Id accId;
     //Code changes for #45436
    /*********************************************************************************************
    *@Description : This method valids restrictActionOwnerChange method of EP_ActionTriggerHelper class
    *@Params      :                    
    *@Return      : Void                                                                             
    *********************************************************************************************/
    @testSetup static void init() {
        list<user> users = new list<user>();
        profile objProfile = [select id from profile where name= 'EP_BSM/CMA' limit 1];
        user objUser = EP_TestDataUtility.createUser(objProfile.id);
        users.add(objUser);
        user objUser1 =EP_TestDataUtility.createUser(objProfile.id);
        users.add(objUser1);
        
        database.insert(users);
        
        Account accShipTo = EP_TestDataUtility.getShipToPositiveScenario();
        accShipTo.EP_Synced_NAV__c = true;
        accShipTo.EP_Synced_PE__c = true;
        accShipTo.EP_Synced_WINDMS__c = true;
        accShipTo.EP_Status__c = '02-Basic Data Setup';
        update accShipTo; 

        List<account> sellToAcc = [select id,EP_Indcative_Total_Purchase_Value_Yr_USD__c, EP_Synced_NAV__c,EP_Synced_PE__c,EP_Synced_WINDMS__c,EP_Status__c,EP_Puma_Company__c From Account Where Id =: accShipTo.ParentId];
        system.debug('accShipTo%%' +accShipTo);
        System.assert(sellToAcc.size() == 1);
        accId = sellToAcc[0].id;
        if(!sellToAcc.isEmpty()){  
          sellToAcc[0].EP_Indcative_Total_Purchase_Value_Yr_USD__c = '1000';      
          sellToAcc[0].EP_Synced_NAV__c = true;
          sellToAcc[0].EP_Synced_PE__c = true;
          sellToAcc[0].EP_Synced_WINDMS__c = true;
          sellToAcc[0].EP_Status__c = '02-Basic Data Setup';          
        }
        Test.startTest(); 
        update sellToAcc;
         
        EP_TestDataUtility.createUserCompany(objUser.id,sellToAcc[0].EP_Puma_Company__c);
        EP_TestDataUtility.createUserCompany(objUser1.id,sellToAcc[0].EP_Puma_Company__c);
          Test.stopTest();
          List<EP_User_Company__c> userCompanyList = [Select Id FROM EP_User_Company__c];
          System.assertEquals(2, userCompanyList.size());
        // EP_TestDataUtility.InsertCheck=true;
        //   Test.stopTest(); 
    }
    
    
    private static EP_Action__c createAction() {
        Account account = [select Id from account limit 1];
        user objUser = [select Id from user where Profile.Name = 'EP_BSM/CMA' limit 1];
        
        cscfga__Product_Basket__c basketInsert = new cscfga__Product_Basket__c(
            Name = 'New Basket',
            OwnerId = UserInfo.getUserId()
        );
        insert basketInsert;
 
       
        Id pricebookId = Test.getStandardPricebookId();
        Account acc = [select id,ep_price_consolidation_basis__c from Account limit 1];
        csord__Order__c ord1 = new csord__Order__c();
        ord1.Name = 'Sales Order';
        ord1.csord__Account__c = account.id;
        ord1.Requested_Date__c = date.today();
        ord1.csord__Identification__c = basketInsert.id;
        ord1.PriceBook2Id__c = pricebookId;
        ord1.csord__Status2__c = 'Awaiting Tolerance Approval';
        ord1.RepriceRequired__c = true;
        ord1.pricingResponseJson__c = '';
        insert ord1;
        
        EP_Action__c action = new EP_Action__c();
        action.RecordTypeId = Schema.SObjectType.EP_Action__c.getRecordTypeInfosByName().get('BSM/GM Review').getRecordTypeId();
        action.EP_Account__c = account.id;
        action.EP_Record_Type_Name__c = 'BSM/GM Review';
        action.EP_Status__c = '03-Approved';
        action.EP_Action_Name__c = 'Tolerance Approval';
        action.OwnerId = objUser.Id;
        action.EP_CSOrder__c = ord1.id;
        //action.EP_Reason__c = 'Test';
        database.insert( action);
        
        return action;
    }
    /*private static testMethod void testRestrictActionOwnerChange_POSITIVE(){ 
        EP_Action__c action  = createAction();
        user objUser1 = [select Id from user where Id !=: action.OwnerId and Profile.Name = 'EP_Business Support Manager' limit 1];
        EP_ActionTriggerHandler.isExecuteBeforeUpdate = false;    
        Test.startTest();
            action.OwnerId = objUser1.Id;
            //database.update(action);
        Test.stopTest(); 
        EP_Action__c action1 = [select OwnerId from  EP_Action__c where Id=:action.Id limit 1];
        System.AssertEquals(action1.OwnerId,objUser1.Id);
    }*/
    //Prateek Kumar, Accenture, Commenting out this method because it is failing - SOQL 101
    /*private static testMethod void testRestrictActionOwnerChange_Negative(){ 
        list<user> users = new list<user>();
        EP_Action__c action  = createAction();
        profile objProfile1 = [select id from profile where name= 'EP_CSC' limit 1];
        user objUser1 =EP_TestDataUtility.createUser(objProfile1.id);
        users.add(objUser1);
        database.insert(users);   
        EP_ActionTriggerHandler.isExecuteBeforeUpdate = false;    
        try {
            Test.startTest();
                action.OwnerId = objUser1.Id;
                database.update(action);
            Test.stopTest(); 
        } catch (Exception exp) {
            system.assertEquals(true, exp.getMessage().contains(Label.EP_Insufficient_Permissions));
        }
    }*/
    
    private static testMethod void updateAccountStatus_UT(){ 
        Account acc = [select Id from Account limit 1];
        
        Test.startTest();   
             EP_ActionTriggerHelper.updateAccountStatus(new set<id>{acc.id}); 
        Test.stopTest();
        
        Account acc1 = [select Id,EP_Status__c from Account where id =:acc.Id limit 1];
        system.assertEquals(acc1.EP_Status__c,EP_Common_Constant.STATUS_SET_UP);
    }
    
    /*private  static testMethod void assignQueueChangeActions_UT() {
        Id chngRqstTypeAction = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACT_OBJ,EP_Common_Constant.ACT_CR_RT);
        EP_Action__c action  = createAction();
        action.RecordTypeId = chngRqstTypeAction;
        action.EP_Action_Name__c = 'BSM/GM Review';
        action.EP_Region__c = 'Australian Capital Territory';
        action.EP_Country__c = 'AU';
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

       system.runAs(thisUser){
            Test.startTest();   
                 EP_ActionTriggerHelper.assignQueueChangeActions(new list<EP_Action__c>{action}); 
            Test.stopTest();
       }
       system.assertNotEquals(null,action.EP_System_Queue__c);
    }*/

    public static testMethod void assignQueueAndRecordType_test(){
        Account acc = [select Id from Account limit 1];
      String REVIEWQUEUE = 'EP_Review_Queue';
      string QUEUE_STR = 'Queue';
      Test.startTest();
        List<EP_Action__c> actionList =  new List<EP_Action__c>();
        EP_Action__c action1 = new EP_Action__c();
        action1.EP_Account__c = acc.id;
        action1.EP_Record_Type_Name__c = 'BSM/GM Review';
        action1.EP_Action_Name__c = 'BSM/GM Review';
        action1.EP_Status__c = EP_Common_Constant.ACT_NEW_STATUS;
        actionList.add(action1);
        //   Test.startTest();
        database.insert(actionList);
        //  Test.stopTest();
      EP_Action__c action = [select id,OwnerId,RecordTypeId From EP_Action__c Limit 1];
      //Assert Review queue is been assigend to the action record
      list<Group> lstqueue = [select Id From Group Where DeveloperName =: REVIEWQUEUE And Type =: QUEUE_STR Limit 1];      
        Test.stopTest();
      system.assertEquals(lstqueue[0].id,action.OwnerId);
    }

    

    public static testMethod void isBSMGMRequired_test(){
      String REVIEWQUEUE = 'EP_Review_Queue';
      string QUEUE_STR = 'Queue';
      List<EP_Action__c> actionUpdateList = new List<EP_Action__c>();
      User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
      insert new EP_CS_Validation_Rule_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(), EP_Skip_Action_Rules__c=true);
      Test.startTest();
        for(EP_Action__c actn : [select id,OwnerId,RecordTypeId,EP_Status__c,EP_Action_Name__c,EP_Account__c From EP_Action__c]){
          actn.OwnerId = thisUser.id;
          actn.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS;
          actionUpdateList.add(actn);
          system.debug('$$REC'+actn.RecordTypeId +'$$NAme '+actn.EP_Action_Name__c + '$$Acc ' + actn.EP_Account__c);          
        }
        database.update(actionUpdateList);
      Test.stopTest();
    }
     public static testMethod void DeactivateAllPriceBookEntries_test(){
      EP_ProductListTriggerHandler.isExecuteBeforeUpdate = true;
       //Prateek Kumar, Accenture, 7/25/2018
        Id sellToRecordId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sell To').getRecordTypeId();//Prateek Kumar, Accenture, 7/25/2018
        Account acc = [select Id,EP_Puma_Company__c from Account WHERE RecordTypeId= :sellToRecordId LIMIT 1];//Prateek Kumar, Accenture, 7/25/2018
        List<EP_Action__c> actionList1 =  new List<EP_Action__c>();
        
        //   EP_AccountTestDataUtility.accountName='testAcc2';
        // Account sellToAccount = EP_TestDataUtility.getSellToASProspectDomainObject().localAccount;
        Pricebook2 testCustomPricebook = new Pricebook2(Name='myPB123', isActive = TRUE, EP_Company__c = acc.EP_Puma_Company__c);//Prateek Kumar, Accenture, 7/25/2018
        Database.insert(testCustomPricebook);//Prateek Kumar, Accenture, 7/25/2018
        EP_Product_Option__c productlist = EP_TestDataUtility.createProductOption(acc.Id, testCustomPricebook.Id);//Prateek Kumar, Accenture, 7/25/2018
        //EP_Product_Option__c productlist = [select id,Price_List__c from EP_Product_Option__c where  sell_To__c =: acc.id];
      List<EP_Action__c> actionList =  new List<EP_Action__c>();
        
        EP_Action__c action1 = new EP_Action__c();
        action1.EP_Account__c = acc.id;
        action1.EP_Record_Type_Name__c = 'BSM/GM Review';
        action1.EP_Action_Name__c = 'BSM/GM Review';
        action1.EP_Status__c = EP_Common_Constant.ACT_NEW_STATUS;
        actionList.add(action1);
        
      EP_Action__c action = new EP_Action__c();
        action.EP_Account__c = acc.id;
      action.EP_Record_Type_Name__c = 'Price Book Review';
      action.EP_Action_Name__c = 'Price Book Review';
      action.EP_Status__c = EP_Common_Constant.ACT_NEW_STATUS;
      actionList.add(action);
      for(EP_Action__c objaction : actionList){
        objaction.EP_Product_List__c = productlist.Price_List__c;
      }
      Test.startTest();
        database.insert(actionList);
      List<PricebookEntry> pbeList = [select id,IsActive From PricebookEntry where Pricebook2Id =: productlist.Price_List__c];
        Test.stopTest();
        
      //Assert Review queue is been assigend to the action record
      for(PricebookEntry pbe: pbeList) {
        system.assertEquals(false,pbe.IsActive);
      }     
    }
    
    public static testMethod void ActivateAllPriceBookEntries_test(){
        
        //Prateek Kumar, Accenture, 7/25/2018
        Id sellToRecordId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sell To').getRecordTypeId();//Prateek Kumar, Accenture, 7/25/2018
        Account acc = [select Id,EP_Puma_Company__c from Account WHERE RecordTypeId= :sellToRecordId LIMIT 1];//Prateek Kumar, Accenture, 7/25/2018
        
      EP_ProductListTriggerHandler.isExecuteBeforeUpdate = true;
      EP_ActionTriggerHandler.isExecuteAfterUpdate = true;
      EP_ActionTriggerHandler.isExecuteBeforeUpdate = true;
      EP_ActionTriggerHandler.isExecuteAfterInsert = true;
      EP_ActionTriggerHandler.isExecuteBeforeInsert = true;
        Pricebook2 testCustomPricebook = new Pricebook2(Name='myPB123', isActive = TRUE, EP_Company__c = acc.EP_Puma_Company__c);//Prateek Kumar, Accenture, 7/25/2018
        Database.insert(testCustomPricebook);//Prateek Kumar, Accenture, 7/25/2018
        EP_Product_Option__c productlist = EP_TestDataUtility.createProductOption(acc.Id, testCustomPricebook.Id);//Prateek Kumar, Accenture, 7/25/2018
      List<EP_Action__c> actionList =  new List<EP_Action__c>();
        EP_Action__c action1 = new EP_Action__c();
        action1.EP_Account__c = acc.id;
        action1.EP_Record_Type_Name__c = 'BSM/GM Review';
        action1.EP_Action_Name__c = 'BSM/GM Review';
        action1.EP_Status__c = EP_Common_Constant.ACT_NEW_STATUS;
        action1.EP_Product_List__c = productlist.Price_List__c;
        actionList.add(action1);
        
      EP_Action__c action = new EP_Action__c();
        action.EP_Account__c = acc.id;
      action.EP_Record_Type_Name__c = 'Price Book Review';
      action.EP_Action_Name__c = 'Price Book Review';
      action.EP_Status__c = EP_Common_Constant.ACT_NEW_STATUS;
      action.EP_Product_List__c = productlist.Price_List__c;
      actionList.add(action);
      Test.startTest();
        database.insert(actionList);
        //User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        insert new EP_CS_Validation_Rule_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(), EP_Skip_Action_Rules__c=true);       
        
          EP_Action__c pricebookAction = [select id,EP_Status__c,EP_Action_Name__c,EP_Product_List__c from EP_Action__c where EP_Action_Name__c = 'Price Book Review' Limit 1];
          system.debug('pricebookAction##'+pricebookAction);
          //pricebookAction.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS;
        pricebookAction.OwnerId = UserInfo.getUserId();
          database.update(pricebookAction);

          pricebookAction.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS;
          database.update(pricebookAction);
          EP_ActionTriggerHelper.ActivateAllPriceBookEntries(new set<Id>{productlist.Price_List__c});
      List<PricebookEntry> pbeList = [select id,IsActive From PricebookEntry where Pricebook2Id =: productlist.Price_List__c];
        Test.stopTest();
        
      //Assert Review queue is been assigend to the action record
      for(PricebookEntry pbe: pbeList) {
            system.assertEquals(true,pbe.IsActive);//Prateek Kumar, Accenture, 7/25/2018
      }     
    } 
    private static List<EP_Action__c> createActionRecord(string actionName){
      Account sellToAccount = EP_TestDataUtility.getSellToASProspectDomainObject().localAccount;
      List<EP_Action__c> actionList =  new List<EP_Action__c>();
      EP_Action__c action = new EP_Action__c();
      action.EP_Account__c = sellToAccount.id;
      action.EP_Record_Type_Name__c = actionName;
      action.EP_Action_Name__c = actionName;
      action.EP_Status__c = EP_Common_Constant.ACT_NEW_STATUS;
      actionList.add(action);
      return actionList;
    }
    
     private static testMethod void updateOrderRODatesApproval(){
        map<Id,EP_Action__c> mNewActions = new map<Id,EP_Action__c>();
        map<Id,EP_Action__c> mOldActions = new map<Id,EP_Action__c>();
        EP_Action__c actn  =  createAction();
        EP_Action__c actn1  =  createAction();
        actn1.EP_Status__c = '03-Rejected';
        
        //for(EP_Action__c actn : [select id,OwnerId,RecordTypeId,EP_Status__c,EP_Action_Name__c,EP_Account__c From EP_Action__c]){
          mNewActions.put(actn.id,actn);
          mOldActions.put(actn1.id,actn1);
          system.debug('OldMap'+mOldActions +'NewMap'+mNewActions);
         test.startTest();
      
        EP_ActionTriggerHelper.checkAndUpdateToleranceApproval(mNewActions,mNewActions);
        EP_ActionTriggerHelper.updateOrderRODatesApproval(actn.EP_CSOrder__c,'03-Rejected');
           
         test.stopTest();          
       
        
     }
 
}