/*
    @Author          Accenture
    @Name            EP_Third_Party_Order_Service
    @CreateDate     
    @Description     Service class for Third Party Order Record Type on RO Import Staging Object
    @Version         1.0
    @Reference       NA
*/
public with sharing class EP_Third_Party_Order_S extends EP_ROImportStagingService  {
	@testVisible private map<String,list<EP_RO_Import_Staging__c>> StagingOrders = new map<String,list<EP_RO_Import_Staging__c>>();
	
	public override void masterDataVerification(){
    	for(Id fileId : stagingRecordsMap.keySet()){
    		StagingOrders = new map<String,list<EP_RO_Import_Staging__c>>();
    		for(EP_RO_Import_Staging__c stagingRec : stagingRecordsMap.get(fileId)) {
	    		/*super.basicDataVerification(stagingRec);
	    		verifyTransporter(stagingRec);
				verifySupplier(stagingRec);
				verifyContract(stagingRec);
	    		orderDataVerification(stagingRec);
	    		createStagingOrderMap(stagingRec);*/
	    	}
	    	//Consolidation checkes
	    	//verifyNumberofOrderLines();
			//verifyOrderLinesSupplyLocation();
    	}
    }

}