/*
   @Author          CloudSense
   @Name            GetAndDeleteInventoryReview
   @CreateDate      21/03/2018
   @Description     
   @Version         1
 
*/

global class GetAndDeleteInventoryReview implements CSPOFA.ExecutionHandler {

    public List<sObject> process(List<SObject> data) {
        //collect the data for all steps passed in, if needed
        List<CSPOFA__Orchestration_Step__c> stepList = (List<CSPOFA__Orchestration_Step__c>)data;
        List<sObject> result = new List<sObject>();
        result = processCS(stepList);
        return result;
    }

    public List<sObject> processCS(List<CSPOFA__Orchestration_Step__c> stepList) {
        List<sObject> result = new List<sObject>();
        List<Id> orderIdList = new List<Id>();
        
        List<CSPOFA__Orchestration_Step__c> extendedList = [SELECT Id, CSPOFA__Orchestration_Process__r.Order__c,
                                                                    CSPOFA__Orchestration_Process__r.Order__r.csord__Account__c,
                                                                    CSPOFA__Status__c, CSPOFA__Completed_Date__c,
                                                                    CSPOFA__Message__c
                                                            FROM CSPOFA__Orchestration_Step__c 
                                                            WHERE Id IN :stepList];
        for(CSPOFA__Orchestration_Step__c step : extendedList) {
            orderIdList.add(step.CSPOFA__Orchestration_Process__r.Order__c);
        }
        
        List<csord__Order__c> orderList = [SELECT Id, 
                                                (SELECT Id,  EP_Status__c 
                                                FROM Actions__r
                                                WHERE RecordType.Name = 'EP_Inventory_Review')
                                            FROM csord__Order__c 
                                            WHERE Id IN :orderIdList];
        
        for(CSPOFA__Orchestration_Step__c step : extendedList) {

            try {
                List<EP_Action__c> actionListToDelete = new List<EP_Action__c>();
                
                for(csord__Order__c order : orderList){
                    for(EP_Action__c action : order.Actions__r){
                        if(action.EP_Status__c == '01-New'){
                            actionListToDelete.add(action);
                        }
                    }
                }
                
                DELETE actionListToDelete;
            }
            catch(Exception e){
                EP_loggingService.loghandledException(e, EP_Common_Constant.EPUMA, 'process', 'GetAndDeleteInventoryReview', apexPages.severity.ERROR);
            }
            
            step.CSPOFA__Status__c = 'Complete';
            step.CSPOFA__Completed_Date__c = Date.today();
            step.CSPOFA__Message__c = 'Custom step succeeded';
            result.add(step);
        }
        return result;
    }
}