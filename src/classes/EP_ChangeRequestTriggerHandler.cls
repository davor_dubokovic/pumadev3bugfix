/* 
  @Author <Ashok Arora>
   @name <EP_ChangeRequestTriggerHandler>
   @CreateDate <22/06/2016>
   @Description <THIS CLASS HANDLES REQUEST FROM CHANGE REQUEST TRIGGER>
   @Version <1.0>
 
*/
public with sharing class EP_ChangeRequestTriggerHandler {
    public static Boolean isExecuteAfterUpdate = false;
    
    /**
     * @author <Ashok Arora>
     * @name <doExecuteBeforeInsert>
     * @date <22/06/2016>
     * @description <THIS METHOD HANDLES BEFORE INSERT REQUESTS FROM CHANGE REQUEST TRIGGER>
     * @version <1.0>
     * @param list<EP_ChangeRequest__c>
     * @return void
     */
    public static void doExecuteBeforeInsert(list<EP_ChangeRequest__c> listOfNewRequest){
        EP_ChangeRequestTriggerHelper.mapDeliveryCountryAndRegion(listOfNewRequest);
    }
    
    /**
     * @author <Ashok Arora>
     * @name <doExecuteAfterUpdate>
     * @date <22/06/2016>
     * @description <THIS METHOD HANDLES AFTER UPDATE REQUESTS FROM CHANGE REQUEST TRIGGER>
     * @version <1.0>
     * @param list<EP_ChangeRequest__c>,list<EP_ChangeRequest__c>,map<Id,EP_ChangeRequest__c>,map<Id,EP_ChangeRequest__c>
     * @return void
     */
    public static void doExecuteAfterUpdate(list<EP_ChangeRequest__c> listOfOldRequest
                                            ,list<EP_ChangeRequest__c> listOfNewRequest
                                            ,map<Id,EP_ChangeRequest__c> mapOfOldRequest
                                            ,map<Id,EP_ChangeRequest__c> mapOfNewRequest){
        isExecuteAfterUpdate = true;
        EP_ChangeRequestTriggerHelper.commitCompletedRequest(mapOfOldRequest   
                                                        ,mapOfNewRequest);
    }
}