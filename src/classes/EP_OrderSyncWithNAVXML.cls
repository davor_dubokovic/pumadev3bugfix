/*
    @Author          Accenture
    @Name            EP_OrderSyncWithNAVXML
    @CreateDate      04/18/2017
    @Description     This class  is used to generate outbound XML's of Order to Sync with NAV
    @Version         1.0
    @Reference       NA
*/
public class EP_OrderSyncWithNAVXML extends EP_GenerateOrderRequestXML{
    
    /**
    * @author           Accenture
    * @name             createXML
    * @date             04/18/2017
    * @description      This method is used to create XML for Order to Sync with NAV
    * @param            NA
    * @return           NA
    */
    public override string createXML(){
        EP_GeneralUtility.Log('Public','EP_OrderCreditCheckXML','createXML');
        return super.createXML();
    }
    
    /**
    * @author           Accenture
    * @name             createPayload
    * @date             04/18/2017
    * @description      This method is used to create Payload for Order to Sync with NAV
    * @param            NA
    * @return           NA
    */      
    public override void createPayload(){
        EP_GeneralUtility.Log('Public','EP_OrderCreditCheckXML','createPayload');
        
        DOM.Document tempDoc = new DOM.Document();
        Dom.XMLNode OrderNode = tempDoc.createRootElement(EP_OrderConstant.Order,null, null);
        Datetime dt = Datetime.now();
        String versionNr = dt.format('yyyyMMdd\'T\'hhmmss') + '.' + dt.millisecondGmt();
        String onRun =OrderObj.EP_OnOff_Run__c;
        String onOffRun;

        Boolean isVMI = false;

        system.debug('onRun is :' +onRun);
        if(onRun!= null && onRun== EP_OrderConstant.ON_STR){
            onOffRun= EP_OrderConstant.TRUE_STR;
        }else if(onRun== EP_OrderConstant.OFF_STR){
            onOffRun= EP_OrderConstant.FALSE_TAG;
        }else{
            onOffRun= EP_Common_Constant.BLANK;
        }
        // Defect -80324 Ex-Rack changes
        String locationId;
        if(OrderObj.EP_Delivery_Type__c == Label.Delivery_Type || OrderObj.EP_Delivery_Type__c == Label.Consumption_Type){
            locationId = getValueforNode(OrderObj.EP_Supply_Location_Number__c);
        }else{
            locationId = getValueforNode(OrderObj.EP_Pickup_Location_ID__c);
      
        }

        String attachName;
        if(OrderObj.Invoice_Consolidation_Basis__c == EP_Common_Constant.ORDER_SUMMARISED_PRICE){
            attachName = EP_Common_Constant.CS_PRICING_RESPONSE_JSON;
        } else {
            attachName = EP_Common_Constant.ORDER_PRICE_RESPONSE_DETAILED;
        }

        list<Attachment> lineItems =  [SELECT Id, Name,body, ParentId, Parent.Type FROM Attachment where ParentId =: OrderObj.csord__Identification__c and Name =:attachName];
        system.debug('pep ' + lineItems);
        String pricingJson;
        String pricingStockHldngLocId = getValueforNode(OrderObj.EP_Supply_Location_Number__c);
        String paymentMethod = getValueforNode(OrderObj.EP_Payment_Method_Code__c);
        String paymentType = getValueforNode(OrderObj.EP_Payment_Method_Name__c);
        // VMI order
        //if (String.isBlank(OrderObj.pricingResponseJson__c) == false) {
        if (String.isBlank(orderLineItemsWrapper[0].orderLineItem.EP_WinDMS_Line_Item_Reference_Number__c) == false) {

            isVMI = true;

            //pricingJson = OrderObj.pricingResponseJson__c;
            List<Account> accList = [SELECT id, EP_Payment_Method__r.EP_Payment_Method_Code__c, 
                                            EP_Payment_Term_Lookup__r.EP_Payment_Term_Code__c from Account where id = :OrderObj.csord__Account__c];
            paymentMethod = accList[0].EP_Payment_Method__r.EP_Payment_Method_Code__c;
            if (accList[0].EP_Payment_Term_Lookup__r.EP_Payment_Term_Code__c.equals(EP_Common_Constant.COD) ||
                accList[0].EP_Payment_Term_Lookup__r.EP_Payment_Term_Code__c.equals(EP_Common_Constant.CHKOD)) {
                paymentType = EP_Common_Constant.COD;
            } else if(accList[0].EP_Payment_Term_Lookup__r.EP_Payment_Term_Code__c.equals(EP_Common_Constant.PREPAYMENT_CODE)) {
                paymentType = EP_Common_Constant.Pre_Payment;
            } else {
                paymentType = EP_Common_Constant.Credit;
            }

            locationId = getValueforNode(orderLineItemsWrapper[0].orderLineItem.EP_WinDMS_StockHldngLocId__c);
            pricingStockHldngLocId = getValueforNode(orderLineItemsWrapper[0].orderLineItem.EP_WinDMS_StockHldngLocId__c);//getValueforNode(OrderObj.EP_WinDMS_StockHldngLocId__c);

            String VMIPriceRequest = OrderObj.VMI_Price_Request_XML__c;

            // If VMIPriceRequest is null it's possibly a consignment order, otherwise:
            if(String.isBlank(VMIPriceRequest) == false){

                //Updates the previous price call message with the new parameters
                Integer startSeq = VMIPriceRequest.indexOf('<seqId>')+7;
                Integer endSeq = VMIPriceRequest.indexOf('</seqId>');
                String seqId = VMIPriceRequest.substring(startSeq, endSeq);
                VMIPriceRequest = VMIPriceRequest.replace('<seqId>'+seqId+'</seqId>', '<seqId>'+EP_OrderPricingRequestXML.generateSeqGuid()+'</seqId>');

                Integer startPCB = VMIPriceRequest.indexOf('<priceConsolidationBasis>')+25;
                Integer endPCB = VMIPriceRequest.indexOf('</priceConsolidationBasis>');
                String oldPCB = VMIPriceRequest.substring(startPCB, endPCB);
                VMIPriceRequest = VMIPriceRequest.replace(oldPCB, OrderObj.Invoice_Consolidation_Basis__c);
                
                String messageType = 'SEND_ORDER_PRICING_REQUEST';
                EP_CS_OutboundMessageSetting__c msgSetting = EP_CustomSettingsUtil.getOutboundMessageSetting(messageType);

                string endPoint = msgSetting.End_Point__c.replace(EP_Common_Constant.COMPANY_CODE_URL_TOKAN, OrderObj.EP_Puma_Company_Code__c);

                EP_OutboundIntegrationService intService = new EP_OutboundIntegrationService(endPoint, EP_Common_Constant.POST, VMIPriceRequest, msgSetting.EP_Subscription_Key__c);
                EP_IntegrationServiceResult calloutResult = EP_OutboundMessageService.doCallOutCS(intService, msgSetting);
                pricingJson = calloutResult.getResponse().getBody();
            }

        } else if (lineItems != null && lineItems.size() >0 && String.isBlank(lineItems.get(0).body.toString()) == false) {
            //standard order
            pricingJson = lineItems.get(0).body.toString();
        }

        system.debug('pricingJson: '+pricingJson);

        //Construct Seq ID
        String seqid = EP_IntegrationUtil.reCreateSeqId(messageId, OrderObj.Id);
        DateTime ceatedDate = OrderObj.CreatedDate;
        Date orderStartDate = date.newinstance(ceatedDate.year(), ceatedDate.month(), ceatedDate.day());
        Dom.XMLNode seqIdNode = OrderNode.addChildElement(EP_OrderConstant.seqId,null,null).addTextNode(getValueforNode(seqid)); //Value for seqId
        
        Dom.XMLNode IdentifierNode = OrderNode.addChildElement(EP_OrderConstant.identifier_windmsxml,null,null);
        Dom.XMLNode orderIdNode = IdentifierNode.addChildElement(EP_OrderConstant.orderId,null,null).addTextNode(getValueforNode(OrderObj.OrderNumber__c)); //Value for orderId
        Dom.XMLNode entrprsIdNode = IdentifierNode.addChildElement(EP_OrderConstant.entrprsId,null,null).addTextNode(getValueforNode(OrderObj.Enterprise_Id__c)); //Value for enterpriseId

        OrderNode.addChildElement(EP_OrderConstant.sellToId,null,null).addTextNode(getValueforNode(OrderObj.EP_Sell_To_Id__c)); //Value for sellToId
        OrderNode.addChildElement(EP_OrderConstant.shipToId,null,null).addTextNode(getShipToOfOrder()); //Value for shipToId
        if (OrderObj.EP_Requested_Delivery_Date__c == null){
            OrderNode.addChildElement(EP_OrderConstant.reqDlvryDt,null,null).addTextNode(getValueforNode(EP_DateTimeUtility.formatDateAsString(OrderObj.Delivery_From_Date__c)));        
        } else {
            OrderNode.addChildElement(EP_OrderConstant.reqDlvryDt,null,null).addTextNode(getValueforNode(EP_DateTimeUtility.formatDateAsString(OrderObj.EP_Requested_Delivery_Date__c)));
        }
        OrderNode.addChildElement(EP_OrderConstant.expctdDlvryDt,null,null).addTextNode(EP_DateTimeUtility.formatDateAsString(OrderObj.EP_Expected_Delivery_Date__c)); // should be Null
        OrderNode.addChildElement(EP_OrderConstant.orderStartDt,null,null).addTextNode(EP_DateTimeUtility.formatDateAsString(orderStartDate));// Delivery from date
        OrderNode.addChildElement(EP_OrderConstant.loadingDt,null,null).addTextNode(EP_DateTimeUtility.formatDateAsString(OrderObj.EP_Expected_Loading_Date__c));
        OrderNode.addChildElement(EP_OrderConstant.dlvryDt,null,null).addTextNode(getValueforNode(EP_DateTimeUtility.formatDateAsString(OrderObj.Actual_Delivery_Date__c)));
        OrderNode.addChildElement(EP_OrderConstant.deliveryType,null,null).addTextNode(getValueforNode(OrderObj.EP_Delivery_Type__c)); //Value for deliveryType
        OrderNode.addChildElement(EP_OrderConstant.prodcutSoldAs,null,null).addTextNode(getValueforNode(OrderObj.EP_Order_Product_Category__c));
        if(OrderObj.Cancellation_Check_Done__c == true || OrderObj.Is_Indicative_on_Modify__c == 'Yes'){
            OrderNode.addChildElement(EP_OrderConstant.docStatus,null,null).addTextNode(Label.DocStatus_Cancel);
        } else {
            OrderNode.addChildElement(EP_OrderConstant.docStatus,null,null).addTextNode('');
        }
        OrderNode.addChildElement(EP_OrderConstant.orderType,null,null).addTextNode(getValueforNode(OrderObj.EP_Type2__c));
        
        OrderNode.addChildElement(EP_OrderConstant.totalOrderQty,null,null).addTextNode(getValueforNode(OrderObj.EP_Total_Order_Quantity__c));
        
        OrderNode.addChildElement(EP_OrderConstant.paymentMethod,null,null).addTextNode(getValueforNode(paymentMethod)); // pickup AP20
        OrderNode.addChildElement(EP_OrderConstant.paymentType,null,null).addTextNode(getValueforNode(paymentType));  // payment term
        OrderNode.addChildElement(EP_OrderConstant.currencyId,null,null).addTextNode(getValueforNode(OrderObj.Currency_Code__c));
        OrderNode.addChildElement(EP_OrderConstant.pricingDate,null,null).addTextNode(getValueforNode(EP_DateTimeUtility.formatDateAsString(OrderObj.EP_Pricing_Date__c))); //Value for pricingDate
        OrderNode.addChildElement(EP_OrderConstant.priceValidityPeriod,null,null).addTextNode(getValueforNode(OrderObj.Description__c)); // Null
        OrderNode.addChildElement(EP_OrderConstant.pricingStockHldngLocId,null,null).addTextNode(pricingStockHldngLocId); //getValueforNode(OrderObj.EP_Supply_Location_Number__c));
        OrderNode.addChildElement(EP_OrderConstant.logisticStockHldngLocId,null,null).addTextNode(locationId); //blank
        OrderNode.addChildElement(EP_OrderConstant.comment,null,null).addTextNode(getValueforNode(OrderObj.EP_Order_Comments__c)); 
        system.debug('Order id is :' +OrderObj.ID);
        OrderNode.addChildElement(EP_OrderConstant.exceptionNr,null,null).addTextNode(this.getCreditExceptionId(OrderObj.ID)); 
        OrderNode.addChildElement(EP_OrderConstant.pricingTransporterCode,null,null).addTextNode(getValueforNode(OrderObj.EP_Transportation_Code__c)); 
        OrderNode.addChildElement(EP_OrderConstant.logisticTransporterCode,null,null).addTextNode(getValueforNode(OrderObj.EP_Transportation_Code__c)); 
        OrderNode.addChildElement(EP_OrderConstant.orderEpoch,null,null).addTextNode(getValueforNode(OrderObj.EP_Order_Epoch__c));
        OrderNode.addChildElement(EP_OrderConstant.orderOrigination,null,null).addTextNode(EP_OrderConstant.CS_TAG);
        OrderNode.addChildElement(EP_OrderConstant.orderRefNr,null,null).addTextNode(getValueforNode(OrderObj.OrderReferenceNumber__c));
        OrderNode.addChildElement(EP_OrderConstant.customerIntRefNr,null,null).addTextNode(getValueforNode(OrderObj.EP_Customer_PO_Number__c));
        OrderNode.addChildElement(EP_OrderConstant.onRun,null,null).addTextNode(onOffRun);
        OrderNode.addChildElement(EP_OrderConstant.runId,null,null).addTextNode(getValueforNode(OrderObj.Run_Number__c));
        OrderNode.addChildElement(EP_OrderConstant.versionNr,null,null).addTextNode(getValueforNode(versionNr));
      
        Dom.XMLNode OrderLinesNode = OrderNode.addChildElement(EP_OrderConstant.OrderLines,null,null);
                system.debug('pep ' + orderLineItemsWrapper);
        //Repeat this for every Child node
        
        for(orderLineItemWrapper orderLineWrp: orderLineItemsWrapper){
                system.debug('pep ' + orderLineWrp);
            Dom.XMLNode OrderLineNode = OrderLinesNode.addChildElement(EP_OrderConstant.OrderLine,null,null);
            
            OrderLineNode.addChildElement(EP_OrderConstant.seqId,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineSeqId));
            Dom.XMLNode IdentifierNode2 = OrderLineNode.addChildElement(EP_OrderConstant.identifier_windmsxml,null,null);
            // VMI order
            if (String.isBlank(orderLineWrp.orderLineItem.EP_WinDMS_Line_Item_Reference_Number__c) == false) {
                IdentifierNode2.addChildElement(EP_OrderConstant.lineId,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.EP_WinDMS_Line_Item_Reference_Number__c)); //Value for lineId
            } else {
                //IdentifierNode2.addChildElement(EP_OrderConstant.lineId,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.Order_Line_Number__c)); //Value for lineId

IdentifierNode2.addChildElement(EP_OrderConstant.lineId,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.CS_OrderLine_Number__c));
            }
            
            OrderLineNode.addChildElement(EP_OrderConstant.itemId,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.EP_Product_Code__c)); //Value for itemId
            OrderLineNode.addChildElement(EP_OrderConstant.qty,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.Quantity__c)); //Value for qty
            OrderLineNode.addChildElement(EP_OrderConstant.pricingStockHldngLocId,null,null).addTextNode(pricingStockHldngLocId); //getValueforNode(OrderObj.EP_Supply_Location_Number__c)); //Value for pricingStockHldngLocId
            OrderLineNode.addChildElement(EP_OrderConstant.logisticStockHldngLocId,null,null).addTextNode(locationId); //Value for logisticStockHldngLocId
            OrderLineNode.addChildElement(EP_OrderConstant.uOm,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.EP_Quantity_UOM__c)); //Value for uOm
            OrderLineNode.addChildElement(EP_OrderConstant.unitPrice,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.EP_Pricing_Response_Unit_Price__c)); //Value for unitPrice
            OrderLineNode.addChildElement(EP_OrderConstant.loadedAmbientQty,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.EP_Ambient_Loaded_Quantity__c));
            OrderLineNode.addChildElement(EP_OrderConstant.loadedStandardQty,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.EP_Standard_Loaded_Quantity__c));
            OrderLineNode.addChildElement(EP_OrderConstant.deliveredAmbientQty,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.EP_Ambient_Delivered_Quantity__c));
            OrderLineNode.addChildElement(EP_OrderConstant.deliveredStandardQty,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.EP_Standard_Delivered_Quantity__c));
            
            Dom.XMLNode bolsNode = OrderLineNode.addChildElement(EP_OrderConstant.bols,null,null);
            Dom.XMLNode bolNode = bolsNode.addChildElement(EP_OrderConstant.bol,null,null);
            //bolNode.addChildElement(EP_OrderConstant.bolId,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.EP_BOL_Number__c));//As per defect 88696
            bolNode.addChildElement(EP_OrderConstant.bolId,null,null).addTextNode(getValueforNode(OrderObj.EP_BOL_Number__c));
            bolNode.addChildElement(EP_OrderConstant.qty,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.Quantity__c)); //Value for qty
            bolNode.addChildElement(EP_OrderConstant.contractId,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.EP_Contract_Nav_Id__c)); //Value for qty
            bolNode.addChildElement(EP_OrderConstant.supplierId,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.EP_Supplier_Nav_Vendor_Id__c));
            // end of bol node
            
            Dom.XMLNode lineComponentsNode = OrderLineNode.addChildElement(EP_OrderConstant.lineComponents,null,null);
            Dom.XMLNode invoiceComponentsNode = lineComponentsNode.addChildElement(EP_OrderConstant.invoiceComponents,null,null);
            Dom.XMLNode acctComponentsNode = lineComponentsNode.addChildElement(EP_OrderConstant.acctComponents,null,null);
            //if(lineItems.get(0).pricingResponseJson__c != null){
            
            //if(lineItems != null && lineItems.size() >0 && lineItems.get(0).body.toString() != null ) {
            if (String.isBlank(pricingJson) == false) {
           //System.debug('Attachement  Body:'+ lineItems[0].body.toString());
            //EP_PricingResponseStub stub = (EP_PricingResponseStub) System.JSON.deserialize(lineItems.get(0).body.toString(), EP_PricingResponseStub.class);

            if(pricingJson.startsWith(EP_Common_Constant.BOM_STR)) {
               pricingJson = pricingJson.replaceFirst(EP_Common_Constant.BOM_STR, EP_Common_Constant.BLANK);
            }
            
            System.debug('Attachement  Body:'+ pricingJson);
            EP_PricingResponseStub stub = (EP_PricingResponseStub) System.JSON.deserialize(pricingJson, EP_PricingResponseStub.class);
            
            for (EP_PricingResponseStub.LineItem li : stub.Payload.Any0.PricingResponse.PriceDetails.PriceLineItems.lineItem) {
                List<String> quantityList = li.lineItemInfo.quantity.split(',');
                String receivedQuantity = '';

                for (Integer i = 0; i < quantityList.size(); i++) {
                    receivedQuantity = receivedQuantity + quantityList[i];
                }
                    
                //if (li.lineItemInfo.itemId.equals(String.valueOf(orderLineWrp.orderLineItem.EP_Product_Code__c)) && (orderLineWrp.orderLineItem.Quantity__c == Decimal.valueOf(receivedQuantity))) {
                /* Start
                   fixed by Developer Ramcharan 
                   Defect No. 95843
                   Date 25/10/2018 
                */
                // if ((!isVMI && li.lineItemInfo.lineItemId.equals(String.valueOf(orderLineWrp.orderLineItem.CS_OrderLine_Number__c)))
                        //    || (isVMI && li.lineItemInfo.lineItemId.equals(String.valueOf(orderLineWrp.orderLineItem.EP_WinDMS_Line_Item_Reference_Number__c)))) {
           
                    if ((!isVMI && li.lineItemInfo.itemId.equals(String.valueOf(orderLineWrp.orderLineItem.EP_Product_Code__c)) && (orderLineWrp.orderLineItem.Quantity__c == Decimal.valueOf(receivedQuantity)))
                    || (isVMI && li.lineItemInfo.lineItemId.equals(String.valueOf(orderLineWrp.orderLineItem.EP_WinDMS_Line_Item_Reference_Number__c)))) {
               /*End related to 95843 */ 
                    for (Integer i = 0;  i < li.lineItemInfo.invoiceDetails.invoiceComponent.size(); i++) {
                        Dom.XMLNode invoiceComponentNode = invoiceComponentsNode.addChildElement(EP_OrderConstant.invoiceComponent,null,null);
                        invoiceComponentNode.addChildElement(EP_OrderConstant.seqId,null,null).addTextNode(getValueforNode(li.lineItemInfo.invoiceDetails.invoiceComponent[i].seqId));
                        invoiceComponentNode.addChildElement(EP_OrderConstant.TYPE,null,null).addTextNode(getValueforNode(li.lineItemInfo.invoiceDetails.invoiceComponent[i].type));
                        invoiceComponentNode.addChildElement(EP_OrderConstant.name,null,null).addTextNode(getValueforNode(li.lineItemInfo.invoiceDetails.invoiceComponent[i].name));
 invoiceComponentNode.addChildElement(EP_OrderConstant.componentDescr,null,null).addTextNode(getValueforNode(li.lineItemInfo.invoiceDetails.invoiceComponent[i].componentDescr));
                        invoiceComponentNode.addChildElement(EP_OrderConstant.amount,null,null).addTextNode(getValueforNode(li.lineItemInfo.invoiceDetails.invoiceComponent[i].amount));
                        invoiceComponentNode.addChildElement(EP_OrderConstant.amountLCY,null,null).addTextNode(getValueforNode(li.lineItemInfo.invoiceDetails.invoiceComponent[i].amountLCY)); //Value for amount
                        invoiceComponentNode.addChildElement(EP_OrderConstant.taxPercentage,null,null).addTextNode(getValueforNode(li.lineItemInfo.invoiceDetails.invoiceComponent[i].taxPercentage)); //Value for taxPercentage
                        invoiceComponentNode.addChildElement(EP_OrderConstant.taxAmount,null,null).addTextNode(getValueforNode(li.lineItemInfo.invoiceDetails.invoiceComponent[i].taxAmount));
                        invoiceComponentNode.addChildElement(EP_OrderConstant.taxAmountLCY,null,null).addTextNode(getValueforNode(li.lineItemInfo.invoiceDetails.invoiceComponent[i].taxAmountLCY));
                        invoiceComponentNode.addChildElement(EP_OrderConstant.totalAmount,null,null).addTextNode(getValueforNode(li.lineItemInfo.invoiceDetails.invoiceComponent[i].totalAmount));
                        invoiceComponentNode.addChildElement(EP_OrderConstant.totalAmountLCY,null,null).addTextNode(getValueforNode(li.lineItemInfo.invoiceDetails.invoiceComponent[i].totalAmountLCY));
                    }
                    
                    //End of Invoice Components node
                   
                    for (Integer i = 0;  i < li.lineItemInfo.acctComponents.acctComponent.size(); i++) {
                        Dom.XMLNode acctComponentNode = acctComponentsNode.addChildElement(EP_OrderConstant.acctComponent,null,null);
                        acctComponentNode.addChildElement(EP_OrderConstant.seqId,null,null).addTextNode(getValueforNode(li.lineItemInfo.acctComponents.acctComponent[i].seqId));
                        acctComponentNode.addChildElement(EP_OrderConstant.componentCode,null,null).addTextNode(getValueforNode(li.lineItemInfo.acctComponents.acctComponent[i].componentCode));
                        acctComponentNode.addChildElement(EP_OrderConstant.glAccountNr,null,null).addTextNode(getValueforNode(li.lineItemInfo.acctComponents.acctComponent[i].glAccountNr));
                        acctComponentNode.addChildElement(EP_OrderConstant.baseAmount,null,null).addTextNode(getValueforNode(li.lineItemInfo.acctComponents.acctComponent[i].baseAmount));
                        acctComponentNode.addChildElement(EP_OrderConstant.baseAmountLCY,null,null).addTextNode(getValueforNode(li.lineItemInfo.acctComponents.acctComponent[i].baseAmountLCY));
                        acctComponentNode.addChildElement(EP_OrderConstant.taxRate,null,null).addTextNode(getValueforNode(li.lineItemInfo.acctComponents.acctComponent[i].taxRate));
                        acctComponentNode.addChildElement(EP_OrderConstant.taxAmount,null,null).addTextNode(getValueforNode(li.lineItemInfo.acctComponents.acctComponent[i].taxAmount));
                        acctComponentNode.addChildElement(EP_OrderConstant.taxAmountLCY,null,null).addTextNode(getValueforNode(li.lineItemInfo.acctComponents.acctComponent[i].taxAmountLCY));
                        acctComponentNode.addChildElement(EP_OrderConstant.totalAmount,null,null).addTextNode(getValueforNode(li.lineItemInfo.acctComponents.acctComponent[i].totalAmount));
                        acctComponentNode.addChildElement(EP_OrderConstant.totalAmountLCY,null,null).addTextNode(getValueforNode(li.lineItemInfo.acctComponents.acctComponent[i].totalAmountLCY));
                        acctComponentNode.addChildElement(EP_OrderConstant.isVAT,null,null).addTextNode(getValueforNode(li.lineItemInfo.acctComponents.acctComponent[i].isVAT));
                    }
                }
            }
            // fix for defect -80505
            } else{ 
            
                Dom.XMLNode invoiceComponentNode = invoiceComponentsNode.addChildElement(EP_OrderConstant.invoiceComponent,null,null);
                invoiceComponentNode.addChildElement(EP_OrderConstant.seqId,null,null);
                invoiceComponentNode.addChildElement(EP_OrderConstant.TYPE,null,null);
                invoiceComponentNode.addChildElement(EP_OrderConstant.name,null,null);
         invoiceComponentNode.addChildElement(EP_OrderConstant.componentDescr,null,null);
                invoiceComponentNode.addChildElement(EP_OrderConstant.amount,null,null);
                invoiceComponentNode.addChildElement(EP_OrderConstant.amountLCY,null,null);
                invoiceComponentNode.addChildElement(EP_OrderConstant.taxPercentage,null,null);
                invoiceComponentNode.addChildElement(EP_OrderConstant.taxAmount,null,null);
                invoiceComponentNode.addChildElement(EP_OrderConstant.taxAmountLCY,null,null);
                invoiceComponentNode.addChildElement(EP_OrderConstant.totalAmount,null,null);
                invoiceComponentNode.addChildElement(EP_OrderConstant.totalAmountLCY,null,null);
                
                Dom.XMLNode acctComponentNode = acctComponentsNode.addChildElement(EP_OrderConstant.acctComponent,null,null);
                acctComponentNode.addChildElement(EP_OrderConstant.seqId,null,null);
                acctComponentNode.addChildElement(EP_OrderConstant.componentCode,null,null);
                acctComponentNode.addChildElement(EP_OrderConstant.glAccountNr,null,null);
                acctComponentNode.addChildElement(EP_OrderConstant.baseAmount,null,null);
                acctComponentNode.addChildElement(EP_OrderConstant.baseAmountLCY,null,null);
                acctComponentNode.addChildElement(EP_OrderConstant.taxRate,null,null);
                acctComponentNode.addChildElement(EP_OrderConstant.taxAmount,null,null);
                acctComponentNode.addChildElement(EP_OrderConstant.taxAmountLCY,null,null);
                acctComponentNode.addChildElement(EP_OrderConstant.totalAmount,null,null);
                acctComponentNode.addChildElement(EP_OrderConstant.totalAmountLCY,null,null);
                acctComponentNode.addChildElement(EP_OrderConstant.isVAT,null,null);
            
            
            
            
            }

            
            //end of orderline node
        }
        //end of Order node
        OrderNode.addChildElement(EP_OrderConstant.clientId,null,null).addTextNode(getValueforNode(OrderObj.EP_Puma_Company_Code__c));        
        Dom.XMLNode PayloadNode = MSGNode.addChildElement(EP_OrderConstant.Payload,null,null);
          Dom.XmlNode AnyNode = PayloadNode.addChildElement(EP_OrderConstant.any0,null, null);
        // Encoding payload by calling encode XML method in superclass
        system.debug('pep xml' + tempDoc.toXmlString());
        AnyNode.addTextNode(encodeXML(tempDoc));
    }
    
 /**
    * @author           CloudSense
    * @name             getCreditExceptionId
    * @date             02/08/2018
    * @description      This method is used to get the credit exception id
    * @param            NA
    * @return           NA
    */  
    @TestVisible
    private String getCreditExceptionId(Id OrderId){
        EP_GeneralUtility.Log('Public','EP_OrderCreditCheckXML','getCreditExceptionId');
        //Defect Start #57275 -  NAV is Accepting only "PrePayment" text for PaymentType xml tag. Now set blank for other EP_Payment_Term__c values
        String CRNumber = EP_Common_Constant.BLANK;
        List<EP_Credit_Exception_Request__c> crList =[SELECT
                                                                ID,EP_CS_Order__c,EP_Status__c,Name
                                                      FROM
                                                              EP_Credit_Exception_Request__c
                                                      WHERE EP_CS_Order__c=:OrderId and EP_Status__c= :EP_Common_Constant.CRLITEM_STATUS_APPROVED
                                                      ];
        
        if(crList != null && crList.size() > 0){
           CRNumber = crList.get(0).Name;
        }
     return CRNumber;
      }
  
}