/*   
     @Author Aravindhan Ramalingam
     @name <EP_TransferSM.cls>     
     @Description <Transfer order Statemachine >   
     @Version <1.1> 
     */

     public class EP_TransferSM extends EP_OrderStateMachine {
     	
     	public EP_TransferSM(){

     	}
     	
     	public override EP_OrderState getOrderState(EP_OrderEvent currentEvent){
     		EP_GeneralUtility.Log('Public','EP_TransferSM','getOrderState');
     		return super.getOrderState(currentEvent);
     	}
     }