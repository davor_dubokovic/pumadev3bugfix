@isTest
public class EP_CsOrderTriggerHandler_UT {

    @isTest
    static void orderTriggerHandler_test(){
    
        Pricebook2 pricebook = new Pricebook2(Name = 'Standard Price Book', isActive = TRUE);
        INSERT pricebook;
        Product2 prod = new Product2(Name = 'Product');
        INSERT prod;
		System.Assert(prod.Id != null);
        
        PricebookEntry entry = new PricebookEntry();
        entry.UnitPrice = 20;
        entry.Pricebook2Id = pricebook.Id;
        entry.Product2Id = prod.Id;
        INSERT entry;
		System.Assert(entry.Id != null);
        
        csord__Order__c newOrder = EP_TestDataUtility.getSalesOrder();
        newOrder.EP_Error_Product_Codes__c = 'Test';       
        update newOrder; 
		System.Assert(newOrder.Id != null);
        
        LIST<csord__Order_Line_Item__c> oiList = [SELECT Id, OrderId__c, PricebookEntryId__c FROM csord__Order_Line_Item__c WHERE OrderId__c =:newOrder.Id];
        csord__Order_Line_Item__c lineItem = oiList[0];
        lineItem.PricebookEntryId__c = String.valueOf(entry.Id);
        lineItem.EP_Product__c = prod.Id;
        UPDATE lineItem;
        
        CSPOFA__Orchestration_Process_Template__c moptc = new CSPOFA__Orchestration_Process_Template__c();
        moptc.Name = 'Test MOPTC';
        insert moptc;
		System.Assert(moptc.Id != null);        
        
        CSPOFA__Orchestration_Process__c mopc = new CSPOFA__Orchestration_Process__c();
        mopc.Name = 'Test MOPC';
        mopc.CSPOFA__Orchestration_Process_Template__c = moptc.Id;
        mopc.Order__c = newOrder.Id;
        insert mopc;
		System.Assert(mopc.Id != null);
        
        CSPOFA__Orchestration_Step__c mosc = new CSPOFA__Orchestration_Step__c();
        mosc.Name = EP_OrderConstant.ORCHESTRATIONSTEPINVENTORYAPPROVALSTATUS;
        mosc.CSPOFA__Orchestration_Process__c = mopc.Id;
        insert mosc;    
        System.Assert(mosc.Id != null);		
       
        EP_Action__c action = new EP_Action__c();      
        action.EP_Record_Type_Name__c = 'Price Book Review';
        action.EP_Action_Name__c = 'Price Book Review';
        action.EP_Status__c = EP_Common_Constant.ACT_NEW_STATUS;       
        action.EP_CSOrder__c = newOrder.Id;
        action.EP_Status__c = EP_OrderConstant.ACTIONSTATUSREJECTED;  
        action.EP_Reason_For_Status__c= 'TBD';         
        insert action;
		System.Assert(action.Id != null);	
       
        Map<Id,csord__Order__c> orderMap = new Map<Id,csord__Order__c>{newOrder.Id => newOrder};
        
        Test.startTest();
        EP_CsOrderTriggerHandler.doAfterUpdate(orderMap);
        EP_CsOrderTriggerHandler.orchestratorUpdate(orderMap);        
        
        mosc.Name = EP_OrderConstant.ORCHESTRATIONPROCESSCOMPLETESTATUS;
        update mosc;
        EP_CsOrderTriggerHandler.orchestratorUpdate(orderMap);
        
        newOrder.csord__Status2__c = 'PLANNED';  
        update newOrder; 
        System.AssertEquals(newOrder.csord__Status2__c, 'PLANNED');
        Map<Id,csord__Order__c> oldOrderMap = new Map<Id,csord__Order__c>{newOrder.Id => newOrder};
        
        newOrder.csord__Status2__c = 'Order Submitted';   
        newOrder.EP_Delivery_Type__c = 'Ex-Rack';   
        update newOrder; 
        System.AssertEquals(newOrder.csord__Status2__c, 'Order Submitted');
        Map<Id,csord__Order__c> newOrderMap = new Map<Id,csord__Order__c>{newOrder.Id => newOrder};
        EP_CsOrderTriggerHandler.doBeforeUpdateLoadingCode(newOrderMap , oldOrderMap);        
         
        Test.stopTest();
    }
}