/*
 *  @Author <Accenture>
 *  @Name <EP_SellToASActive>
 *  @CreateDate <20/02/2017>
 *  @Description <State Class for Sell To Active Status>
 *  @NovaSuite Fix -- comments added
 *  @Version <1.0>
 */
 public class EP_SellToASActive extends EP_AccountState{
    
    /***NOvasuite fix constructor removed**/
    /*
    *  @Author <Accenture>
    *  @Name setAccountDomainObject
    *  @param EP_AccountDomainObject 
    */
    public override void setAccountDomainObject(EP_AccountDomainObject currentAccount){
        EP_GeneralUtility.Log('Public','EP_SellToASActive','setAccountDomainObject');
        super.setAccountDomainObject(currentAccount);
    }
     /*
    *  @Author <Accenture>
    *  @Name doOnEntry
    */
    public override void doOnEntry(){
		EP_GeneralUtility.Log('Public','EP_SellToASActive','doOnEntry');
		EP_AccountService service = new EP_AccountService(this.account);
		if(this.account.isStatuschanged()){
			//Code changes for L4 #45362 Start
			/*if(this.account.localAccount.EP_Is_Dummy__c){
				service.doActionSendCreateRequestToNavAndLomo();
			}else*/
			if(this.account.isUnblocked()) {
				system.debug('**In Unblock State**');
				service.doActionSendCreateRequestToWinDMS();
			} else if(EP_COMMON_CONSTANT.DELIVERY.equalsignorecase(this.account.localAccount.EP_Delivery_Type__c) && !service.isShipToActive()){
				system.debug('**In resetStatus**');
				account.resetStatus();
			}
			//Code changes for L4 #45362 End
		} 
        // Changes made for CUSTOMER MODIFICATION L4 Start
        //Pricing Callout Commented as it will be part of WP2
        else if(!EP_IntegrationUtil.ISERRORSYNC) { 
            if (this.account.isUpdatedFromNAV()){
                service.doActionSyncCustomerToPricingEngine();
                service.doActionSendCustomerEditRequestToWINDMS();
            }
            else {
                service.doActionSendRequest();
            }
        }
        // Changes made for CUSTOMER MODIFICATION L4 End
    }  
     /*
    *  @Author <Accenture>
    *  @Name doOnExit
    */
    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_SellToASActive','doOnExit');
        
    }
     /*
    *  @Author <Accenture>
    *  @Name doTransition
    *  @return boolean
    */
    public override boolean doTransition(){
        EP_GeneralUtility.Log('Public','EP_SellToASActive','doTransition');
        return super.doTransition();
    }
     /*
    *  @Author <Accenture>
    *  @Name isInboundTransitionPossible
    *  @return boolean
    */
    public override boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_SellToASActive','isInboundTransitionPossible');
        return super.isInboundTransitionPossible();
    }
}