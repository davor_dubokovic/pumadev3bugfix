/*
    @Author          Accenture
    @Name            EP_OrderImportContext
    @CreateDate      
    @Description     This is a controller extension for RO Order Import VF page
    @Version         1.0
    @Reference       NA
*/
public with sharing class EP_OrderImportContext {
    public String strCSVFileContents {get;set;}
    public String strCSVFileName {get;set;}
    public String strCSVColumnSquence {get;set;}
    public Transient Blob blbCSVFile {get;set;}
    public Boolean CSVParseSuccess{get;set;}
    public string strErrorMessage{get;set;}
    public EP_File__c ctxfileRecord;
    
    /* 
        Constructor
    */
    public EP_OrderImportContext(EP_File__c fileRec) {
    	EP_GeneralUtility.Log('public','EP_OrderImportContext','EP_OrderImportContext');
    	ctxfileRecord = fileRec;
        init();
    }

    /* 
        method to init the variables
    */
    @testVisible
    private void init() {
    	EP_GeneralUtility.Log('private','EP_OrderImportContext','init');
        strErrorMessage = '';
        CSVParseSuccess = false;
    }
}