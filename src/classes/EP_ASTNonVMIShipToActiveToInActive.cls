/*
*  @Author <Accenture>
*  @Name <EP_ASTNonVMIShipToActiveToInActive>
*  @CreateDate <28/02/2017>
*  @Description <Handles VMI Ship To Account status change from 05-Active to 07-Inactive>
*  @Version <1.0>
*/
public class EP_ASTNonVMIShipToActiveToInActive extends EP_AccountStateTransition {

    public EP_ASTNonVMIShipToActiveToInActive () {
        finalState = EP_AccountConstant.INACTIVE;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToActiveToInActive','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToActiveToInActive','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToActiveToInActive','isGuardCondition');
        return true;
    }

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToActiveToInActive','doOnExit');

    }
}