/*
    This is the Bean class for the leadConvertPage and MergeContactPage
*/
public with sharing class EP_CRM_LeadConvertBean {
    // This is the lead that is to be converted
    public Lead leadToConvert {get; set;}
    
    // Prefix for the label of existing accounts
    private final String EXISTING = 'Attach to Existing: ';

    // Checkbox on the page indicating if there will be an email sent to the owner 
    public boolean sendOwnerEmail { get; set; }

    // This will hold the Opportunity for the Opportunity name on the comonent 
    public Opportunity opportunityObj { get; set; }

    // Checkbox on the page indicating if an Opportunity should be created
    public Boolean doNotCreateOppty { get; set; }
    
    // Checkbox on the page indicating if an overwriting lead source
    public Boolean overwriteLeadSource { get; set; }

    // the list of accounts in the SELECT list
    public List < SelectOption > accounts { get; set; }
    
    // the selected account in the SELECT list of accounts
    public String selectedAccountId { get; set; }
    
    public String getSelectedAccountName() {
        if(String.isNotBlank(selectedAccountId)) {
            Account accountObj = [SELECT Id, Name FROM Account WHERE Id = :selectedAccountId limit 1];
            if(accountObj != null) {
                return accountObj.Name;
            }
        }
        return '';
    }    
    
    // the selected account in the SELECT list of accounts
    public String selectedContactId { get; set; }
    
    // This will hold the owner of Lead
    public Contact contactObj {
        get {
            if (contactObj == null) {
                contactObj = new Contact(OwnerId = leadToConvert.ownerId);
            }
            return contactObj;
        }
        set;
    }
    
    public EP_CRM_LeadConvertBean() {
    }
    
    public EP_CRM_LeadConvertBean(Id leadId) {
        // Query to get existing Lead details
        leadToConvert = [SELECT Id, Status, OwnerId, Name, FirstName, LastName, Company FROM Lead WHERE Id = :leadId]; 
        
        // create a new Opportunity which will hold the Opportuniy name set by the user
        opportunityObj = new Opportunity();

        // set the selected Account to NONE by default
        selectedAccountId = 'NONE';
        
        selectedContactId = 'NONE';
        
        populateAccounts();
    }
    
    // set up the Lead Status pick list
    public List < SelectOption > LeadStatusOption {
        get {
            if (LeadStatusOption == null) {
                LeadStatusOption = new List < SelectOption > ();

                //get the lead statuses
                LeadStatus[] ls = [SELECT MasterLabel FROM LeadStatus WHERE IsConverted = true AND MasterLabel like '%Converted%' order by SortOrder];

                // if there is more than 1 lead status option, add a NONE option  
                if (ls.size() > 1) {
                    LeadStatusOption.add(new SelectOption('NONE', '--None--'));
                }

                // add the rest of the lead status options
                for (LeadStatus convertStatus: ls) {
                    LeadStatusOption.add(new SelectOption(convertStatus.MasterLabel, convertStatus.MasterLabel));
                }
            }
            return LeadStatusOption;
        }
        set;
    }
    
    // when the selected account in the SELECT list of accounts changes this method is called 
    public PageReference accountChanged() {
        // if either the NONE option or the Create New Account option is selected, the Opportuniy Name is set to the lead's company
        if (selectedAccountId == 'NEW' || selectedAccountId == 'NONE') {
            opportunityObj.Name = leadToConvert.Company + '-';
        } else {
            // otherwise find the account's Id and Name that was selected and set the Opportuity name to that Account
            Account[] a = [
                SELECT Id, Name
                FROM Account WHERE Id =: selectedAccountId
            ];

            if (a.size() > 0) {
                opportunityObj.Name = a[0].Name + '-';
            }
        }
        return null;
    }
    
    // This gets called when an existing accout gets looked up via the lookup magnifying glass
    public PageReference accountLookedUp() {
        system.debug('!!! Account looked up --> ' + contactObj.AccountId);

        //find the Id and Nmae of the Account that was looked up        
        Account[] a = [
            SELECT Id, Name
            FROM Account WHERE Id =: contactObj.AccountId
        ];

        if (a.size() > 0) {

            // add the locked up account to the slect list
            accounts.add(new SelectOption(a[0].Id, EXISTING + a[0].Name));

            // set the selected account to the one that was just looked up by default
            selectedAccountId = a[0].Id;

            // set the Opportunity name to the account's name that was looked up
            opportunityObj.Name = a[0].Name + '-';

            system.debug('accounts --> ' + accounts);
        }
        return null;
    }
    
    // Find an Account using SOSL based on the given company name
    private Account[] findCompany(String companyName) {
        //perform the SOSL query
        List < List < SObject >> searchList = [
            FIND: companyName
            IN NAME FIELDS
            RETURNING
            Account(
                Id,
                Name
            )
        ];

        List < Account > accountsFound = new List < Account > ();
        for (List < sobject > sObjs: searchList) {
            for (sObject s: sObjs) {
                //add the account that was found to the list of found accounts
                accountsFound.add((Account) s);
            }
        }
        // return the list of found accounts
        return accountsFound;
    }
    
    // Populate the list of Accounts in the dropdown
    private void populateAccounts() {
        if (leadToConvert != null) {
            String company = leadToConvert.Company;

            // find any accounts that match the SOSL query in the findCompany() method  
            Account[] accountsFound = findCompany(company + '*');

            accounts = new List < selectOption > ();

            if (accountsFound != null && accountsFound.size() > 0) {

                // if there is at least 1 account found add a NONE option and a Create New Account option
                accounts.add(new SelectOption('NONE', '--None--'));

                accounts.add(new SelectOption('NEW', 'Create New Account: ' + company));

                // for each account found, add an option to attach to the existing account
                for (Account a: accountsFound) {
                    accounts.add(new SelectOption(a.Id, EXISTING + a.Name));
                }
            } else {
                // if no accounts matched then simply add a Create New Account option
                accounts.add(new SelectOption('NEW', 'Create New Account: ' + company));
                system.debug('no account matches on company ' + company);
            }
            //the default opportunity name will be the lead's company
            opportunityObj.Name = company + '-';
        } else system.debug('leadToConvert = null');
    }
    
    // Populate the list of Contacts in the dropdown
    public List < SelectOption > getContacts() {
        List < SelectOption > contacts = new List < selectOption > ();
            
        if (String.isNotBlank(selectedAccountId) && selectedAccountId != 'NONE') {
        
            // find related contacts that match the SOQL query
            Contact[] contactsFound = [SELECT Id, Name FROM Contact WHERE AccountId = :selectedAccountId AND lastName = :leadToConvert.LastName];            
            
            if (contactsFound != null && contactsFound.size() > 0) {
                // if there is at least 1 contact found add a NONE option and a Create New Account option
                contacts.add(new SelectOption('NONE', '--None--'));

                contacts.add(new SelectOption('NEW', 'Create New Contact: ' + leadToConvert.Name));

                // for each contact found, add an option to attach to the existing contact
                for (Contact c: contactsFound) {
                    contacts.add(new SelectOption(c.Id, EXISTING + c.Name));
                }
            } else {
                // if no contacts matched then simply add a Create New Contact option
                contacts.add(new SelectOption('NEW', 'Create New Contact: ' + leadToConvert.Name));
                system.debug('no contact matches on last name ' + leadToConvert.lastName);
            }            
        } else system.debug('Selected Account is Blank');
        
        return contacts;
    }
}