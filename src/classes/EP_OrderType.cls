/**
 * @Author      : Kalpesh Thakur<kalpesh.j.thakur@accenture.com>
 * @name        : EP_OrderType
 * @CreateDate  : 31/01/2017
 * @Description : This is Parent for all the Order Type Classes 
 * @Version     : <1.0>
 * @reference   : N/A
 */

 public virtual class EP_OrderType {
    public EP_VendorManagement vmiType;
    public EP_ConsignmentType consignmentType;
    public EP_OrderEpoch epochType;
    public EP_ProductSoldAs productSoldAsType;
    public EP_DeliveryType deliveryType;
    @TestVisible protected EP_OrderDomainObject orderDomainObj;
    @TestVisible protected csord__Order__c orderObj;

    /** Do submit Actions
     *  @date      05/02/2017
     *  @name      doSubmitActions
     *  @param     NA
     *  @return    NA
     *  @throws    NA
     */
     public virtual void doSubmitActions() {
        EP_GeneralUtility.Log('Public','EP_OrderType','doSubmitActions');
    }

    public virtual void updateTransportService() {
        EP_GeneralUtility.Log('Public','EP_OrderType','updateTransportService');
        deliverytype.updateTransportService(orderObj);
    }

    /** Set Order Domain Variables 
     *  @date      05/02/2017
     *  @name      setOrderDomain
     *  @param     NA
     *  @return    NA
     *  @throws    NA
     */

     public virtual void setOrderDomain(EP_OrderDomainObject porderDomainObj) {
        EP_GeneralUtility.Log('Public','EP_OrderType','setOrderDomain');
        this.orderDomainObj = porderDomainObj;
        this.orderObj       = orderDomainObj.getOrder();
        //loadStrategies(orderDomainObj);  
    }

    /** updates Payment Term Fields
     *  @date      05/02/2017
     *  @name      doUpdatePaymentTermAndMethod
     *  @param     NA
     *  @return    NA
     *  @throws    NA
     */
    public virtual void doUpdatePaymentTermAndMethod() {
        EP_GeneralUtility.Log('Public','EP_OrderType','doUpdatePaymentTermAndMethod');
    } 


    /** checks Order Credit 
     *  @date      05/02/2017
     *  @name      hasCreditIssue
     *  @param     NA
     *  @return    NA
     *  @throws    NA
     */
     public virtual boolean hasCreditIssue() {
        EP_GeneralUtility.Log('Public','EP_OrderType','hasCreditIssue');
        return true;
    }

    /** return ShipTos 
    *  @date      16/02/2017
    *  @name      getShipTos
    *  @param     accountId
    *  @return    NA
    *  @throws    NA
    */
    public virtual List<Account> getShipTos(Id accountId) {
        EP_GeneralUtility.Log('Public','EP_OrderType','getShipTos');
        return null;
    }

    /** return Operational Tanks 
     *  @date      22/02/2017
     *  @name      getOperationalTanks
     *  @param     accountId
     *  @return    NA
     *  @throws    NA
     */
     public virtual Map<Id, EP_Tank__c> getOperationalTanks(Id accountId) {
        Map<Id, EP_Tank__c> mapOperationalTanks = new Map<Id, EP_Tank__c>();
        //filter based on deliverytype
        mapOperationalTanks = deliverytype.getOperationalTanks(accountId);
        System.Debug(accountId+'****'+deliverytype+'****mapOperationalTanks:-'+mapOperationalTanks);
        mapOperationalTanks = productSoldAsType.getTanksForBulkProduct(mapOperationalTanks);
        System.Debug(productSoldAsType+'**2**mapOperationalTanks:-'+mapOperationalTanks);
        return mapOperationalTanks;
    }

    /** return Tanks 
     *  @date      22/02/2017
     *  @name      getOperationalTanks
     *  @param     accountId
     *  @return    NA
     *  @throws    NA
     */
     public virtual Map<Id, EP_Tank__c> getTanks(Id accountId) {
        Map<Id, EP_Tank__c> mapTanks = new Map<Id, EP_Tank__c>();
        //filter based on deliverytype
        mapTanks = deliverytype.getTanks(accountId);
        System.Debug(accountId+'****'+deliverytype+'****mapTanks:-'+mapTanks);
        mapTanks = productSoldAsType.getTanksForBulkProduct(mapTanks);
        System.Debug(productSoldAsType+'**2**mapTanks:-'+mapTanks);
        return mapTanks;
    }

    /** check if Loading Date is Valid or not
     *  @date      22/02/2017
     *  @name      isValidLoadingDate
     *  @param     orderObj
     *  @return    NA
     *  @throws    NA
     */
     public virtual Boolean isValidLoadingDate(csord__Order__c orderObj) {
        EP_GeneralUtility.Log('Public','EP_OrderType','isValidLoadingDate');
        return false;
    }

    /** check if Expected Date is Valid or not
     *  @date      22/02/2017
     *  @name      isValidExpectedDate
     *  @param     orderObj
     *  @return    NA
     *  @throws    NA
     */
     public virtual Boolean isValidExpectedDate(csord__Order__c orderObj) {
        EP_GeneralUtility.Log('Public','EP_OrderType','isValidExpectedDate');
        return false;
    }

    /** check if Expected Date is Valid or not
     *  @date      22/02/2017
     *  @name      isValidExpectedDate
     *  @param     orderObj
     *  @return    NA
     *  @throws    NA
     */
     @TestVisible
     protected void loadStrategies(EP_OrderDomainObject orderDomainObject){
        EP_GeneralUtility.Log('Public','EP_OrderType','loadStrategies');
        System.debug('**DeveloperName**'+orderDomainObject.getOrder().EP_ShipTo__r.RecordType.DeveloperName);
        System.debug('**DeveloperName**'+orderDomainObject.getOrder().EP_ShipTo__c);
        if(orderDomainObject.getVMIType() != EP_Common_Constant.BLANK){        
            vmiType     =  orderDomainObject.getVMITypeObject();
        } 
        consignmentType = orderDomainObject.getConsignmentTypeObject();   
        epochType = orderDomainObject.getEpochTypeObject();
        productSoldAsType = orderDomainObject.getProductSoldAsTypeObject();
        deliveryType = orderDomainObject.getDeliveryTypeObject();
    } 

    /** returns routes of ship to and location
     *  @date      22/02/2017
     *  @name      getAllRoutesOfShipToAndLocation
     *  @param     Id shipToId,Id stockholdinglocationId
     *  @return    List<EP_Route__c>
     *  @throws    NA
     */
     public virtual List<EP_Route__c> getAllRoutesOfShipToAndLocation(Id shipToId,Id stockholdinglocationId) {
        EP_GeneralUtility.Log('Public','EP_OrderType','getAllRoutesOfShipToAndLocation');
        return new List<EP_Route__c>(); 
    }

    /** returns routes of ship to and location
     *  @date      22/02/2017
     *  @name      showCustomerPO
     *  @param     NA
     *  @return    Boolean
     *  @throws    NA
     */
     public virtual Boolean showCustomerPO(){
        EP_GeneralUtility.Log('Public','EP_OrderType','showCustomerPO');
        return deliveryType.showCustomerPO(this.orderObj);
    }

    /** returns record recordype for the Order
    *  @date      22/02/2017
    *  @name      findRecordType
    *  @param     NA
    *  @return    Id
    *  @throws    NA
    */
    public virtual Id findRecordType() {
        EP_GeneralUtility.Log('Public','EP_OrderType','findRecordType');
        return null;
    }

    /** This method returns the list of the payment terms applicable for the order.
     *  @date      24/02/2017
     *  @name      getApplicablePaymentTerms
     *  @param     NA
     *  @return    NA
     *  @throws    NA
     */
     public virtual List<String> getApplicablePaymentTerms(Account accountObject){
        EP_GeneralUtility.Log('Public','EP_OrderType','getApplicablePaymentTerms');
        String field = orderDomainObj.getProductSoldAsType().equalsIgnoreCase(EP_Common_Constant.BULK_ORDER_PRODUCT_CAT) 
        ? 'EP_Payment_Term_Lookup__r' : 'EP_Package_Payment_Term__r';
        return getApplicablePaymentTerms(accountObject, field);
    }

    /** This method returns the applicable type of deliveries for the order.
    *  @date      26/02/2017
    *  @name      getApplicableDeliveryTypes
    *  @param     NA
    *  @return    NA
    *  @throws    NA
    */  
    public virtual List<String> getApplicableDeliveryTypes(){
        EP_GeneralUtility.Log('Public','EP_OrderType','getApplicableDeliveryTypes');
        return null;
    }

    /** This method returns the list of the payment terms applicable for the type of product order created for.
    *  @date      24/02/2017
    *  @name      getApplicablePaymentTerms
    *  @param     NA
    *  @return    NA
    *  @throws    NA
    */
    
    private List<String> getApplicablePaymentTerms(Account accountObject, String field){
        EP_GeneralUtility.Log('Private','EP_OrderType','getApplicablePaymentTerms');
        List<String> listOfPaymentTerms = new List<String>();
        //System.Debug('Check the term:' + accountObject.getPopulatedFieldsAsMap().containskey('EP_Payment_Term_Lookup__c'));
        EP_Payment_Term__c paymentTerm = (EP_Payment_Term__c)accountObject.getPopulatedFieldsAsMap().get(field);
        if(paymentTerm != null){
            string fieldValue = paymentTerm.Name;
            if(fieldValue.equalsIgnoreCase(EP_Common_Constant.CHEQUE_ON_DELIVERY)) {
                listOfPaymentTerms.add(EP_Common_Constant.CHEQUE_ON_DELIVERY);
            }
            if(!fieldValue.equalsIgnoreCase(EP_Common_Constant.PREPAYMENT) && !fieldValue.equalsIgnoreCase(EP_Common_Constant.CHEQUE_ON_DELIVERY)){
                listOfPaymentTerms.add(EP_Common_Constant.CREDIT_PAYMENT_TERM);
            }
            listOfPaymentTerms.add(EP_Common_Constant.PREPAYMENT);
        }
        return listOfPaymentTerms;
    }

    /** This method returns Pricbook Entries for the Order
    *  @date      24/02/2017
    *  @name      getApplicablePaymentTerms
    *  @param     NA
    *  @return    NA
    *  @throws    NA
    */
    public virtual List<PriceBookEntry> findPriceBookEntries(Id pricebookId,csord__Order__c objOrder) {
        EP_GeneralUtility.Log('Public','EP_OrderType','findPriceBookEntries');
        return null;
    } 

    /** checks if Order Date is Valid or not
    *  @date      25/02/2017
    *  @name      isValidOrderDate
    *  @param     NA
    *  @return    NA
    *  @throws    NA
    */
    public virtual Boolean isValidOrderDate(csord__Order__c orderObj) {
        EP_GeneralUtility.Log('Public','EP_OrderType','isValidOrderDate');
        return false;
    }

    /** checks if Order Date is Valid or not
    *  @date      25/02/2017
    *  @name      isValidOrderDate
    *  @param     NA
    *  @return    NA
    *  @throws    NA
    */
    public virtual csord__Order__c setRequestedDateTime(csord__Order__c orderObj,String selectedDate,String availableSlots) {
        EP_GeneralUtility.Log('Public','EP_OrderType','setRequestedDateTime');
        return orderObj;
    }

    /** calculates Price for the Order
    *  @date      28/02/2017
    *  @name      calculatePrice
    *  @param     NA
    *  @return    NA
    *  @throws    NA
    */
    public virtual void calculatePrice() {
        EP_GeneralUtility.Log('Public','EP_OrderType','calculatePrice');
    }

    /** return delivery Types for the Order
    *  @date      28/02/2017
    *  @name      getDeliveryTypes
    *  @param     NA
    *  @return    NA
    *  @throws    NA
    */
    public virtual List<String> getDeliveryTypes() {
        EP_GeneralUtility.Log('Public','EP_OrderType','getDeliveryTypes');
        return new List<String>();
    }

    /** This method is used to populate "Pipeline" in delivery type when default trannsporter is "No Transporter"
    *  @date      02/03/2017
    *  @name      checkForNoTransporter
    *  @param     String transporterName 
    *  @return    Order
    *  @throws    NA
    */ 
    public virtual csord__Order__c checkForNoTransporter(String transporterName) {
        EP_GeneralUtility.Log('Public','EP_OrderType','checkForNoTransporter');
        return orderObj;
    }


     /** This method sets retrospective orders
     *  @date      03/03/2017
     *  @name      setRetroOrderDetails
     *  @param     NA
     *  @return    NA
     *  @throws    NA
     */
     public virtual void setRetroOrderDetails(){
        EP_GeneralUtility.Log('Public','EP_OrderType','setRetroOrderDetails');
        epochType.setRetroOrderDetails(orderObj);
    }

    /** This method find if Order Entry is Valid
     *  @date      07/03/2017
     *  @name      isOrderEntryValid
     *  @param     NA
     *  @return    NA
     *  @throws    NA
     */
     public virtual Boolean isOrderEntryValid(integer numOfTransportAccount){
        EP_GeneralUtility.Log('Public','EP_OrderType','isOrderEntryValid');
        return true;
    }

    /** This method sets Consumption values on fields
     *  @date      07/03/2017
     *  @name      setConsumptionOrderDetails
     *  @param     NA
     *  @return    NA
     *  @throws    NA
     */
     public virtual void setConsumptionOrderDetails() {
        EP_GeneralUtility.Log('Public','EP_OrderType','setConsumptionOrderDetails');

    }
    
    //Defect 57278 Start
    /**
    * @author       Accenture
    * @name         getCreditAmount
    * @date         04/28/2017
    * @description  This method gets amount of open orders
    * @param        NA
    * @return       Double
    */
    public virtual Double getCreditAmount() {
        EP_GeneralUtility.Log('Public','EP_OrderType','getShipTos');
        return consignmentType.getCreditAmount(orderObj);
    }
    //Defect 57278 End
}