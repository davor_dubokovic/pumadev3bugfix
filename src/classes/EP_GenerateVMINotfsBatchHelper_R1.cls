/**
* @author <Accenture>
* @name <EP_GenerateVMINotfsBatchHelper_R1>
* @CreateDate <13/11/2015>
* @description <this is helper class for batch class EP_GenerateVMINotificationsBatchClass_R1 
                This class is sending notification for missing tank dips to customer and also creating blank 
                tank dips.> 
* @version <1.0>
*/
public without sharing class EP_GenerateVMINotfsBatchHelper_R1 {
    private static final String STATUS_BLOCKED = '06-Blocked' ;//ACCOUNT STATUS VALUE
    private static final String TANK_OPERATIONAL_STATUS = Label.EP_Operation_Tank_Status_Label;
    private static final string DIP_REMINDERS = 'DIP REMINDERS';
    private static final String PLACEHOLDER = 'Placeholder';//RECORD TYPE NAME
    private static final String ZERO_STRING = '0';
    private static final String TIME_SEPARATOR_SIGN = ':';
    private static final String DOUBLE_QUOTES_STRING = '"';
    private static final String HYPHEN = '-';
    private static final String CLASS_NAME = 'EP_GenerateVMINotfsBatchHelper_R1';
    private static final String RET_TIME = 'returnTimeFromString'; 
    private static final String EPUMA = 'ePuma';
    private static final String METHOD_CHECK_SHIP_ACCT = 'checkShipToAccountsForCompliance';
    private static final String queryString = 'SELECT ID,CreatedDate,EP_Ship_To__c,EP_Last_Dip_Ship_To_Date_Time__c, EP_Ship_To__r.Name, EP_Last_Place_Dip_Reading_Date_Time__c, EP_Capacity__c, EP_Deadstock__c, '
    + 'EP_Last_Dip_Reading_Date__c,EP_Tank_Status__c,  EP_UnitOfMeasure__c  ,EP_Ship_To__r.EP_Country_Code__c, '
    + 'EP_Ship_To__r.EP_Ship_To_UTC_Timezone__c,EP_Last_Placeholder_Ship_To_Date_Time__c,EP_Missing_Placeholder_Dip_For_Today__c,EP_Ship_To__r.parentId, '
    + 'EP_Ship_To__r.EP_Ship_To_Current_Date_Time__c, EP_Ship_To__r.EP_Tank_Dips_Schedule_Time__c,EP_Ship_To__r.EP_Tank_Dips_Submission_Time__c,EP_Ship_To__r.EP_Reminder_Notification_Interval_in_hrs__c '
    + 'FROM EP_Tank__c '
    + 'WHERE EP_Tank_Status__c != \'' + Label.EP_Decomissioned_Tank_Status_Label + '\' '
    + 'AND EP_Tank_Missing_Dip_For_Today__c = TRUE '
    + 'AND EP_Tank_Dip_Entry_Mode__c = \'' + Label.EP_Portal_Dip_Entry_Mode_Value + '\' '
    + 'AND EP_Ship_To__r.EP_Tank_Dips_Schedule_Time__c != NULL '
    + 'AND EP_Ship_To__r.RecordType.DeveloperName = \'' + Label.EP_VMI_Ship_To_Record_Type_Dev_Name + '\' '
    + 'AND (EP_Ship_To__r.EP_Status__c = \'' + Label.EP_Active_Ship_To_Status_Label + '\' '
    + 'OR EP_Ship_To__r.EP_Status__c = \'' + STATUS_BLOCKED + '\')'
    + 'AND EP_Ship_To__r.EP_Is_Ship_To_Open_Today__c = TRUE ';
    
    private static final String NAME = 'EP_GenerateVMINotfsBatchHelper_R1 ';
    private static final String RETREIVEVMISHIPTOACCOUNTSFORCOMPL = 'retrieveVMIShipToAccountsForCompliance';
    
    // #### Note ####
    // We do not use static variables from the constant class to avoid dependencies that 
    // will prevent us from deploying code when the scheduled job is running
    /**
    * @author <Accenture>
    * @date <13/11/2015>
    * @name <retrieveVMIShipToAccountsForCompliance>
    * @description <Retrieve all the tanks that are missing dips where the following criteria apply:
                    A. The Ship-Tos they belong to:
                    1. Are VMI
                    2. Are Active
                    3. Their Tank Dip Entry mode is set to "Portal"
                    4. Are open today
                    B. The tanks are:
                    1. Operational
                    2. Last Tank Dip Date < Ship-To Current Date/Time>
    * @param none
    * @return Database.QueryLocator
    */
    public static Database.QueryLocator retrieveVMIShipToAccountsForCompliance() {
        Database.QueryLocator ql;
        System.debug('+++++++'+queryString);
        // All tanks returned by this query MUST be missing dips for today
        try{
            ql = Database.getQueryLocator(queryString);
            
        }catch(Exception handledException){
            system.debug('Error = '+handledException.getMessage()+' at line number '+handledException.getLineNumber());
            EP_LoggingService.logHandledException(handledException, EPUMA,
            RETREIVEVMISHIPTOACCOUNTSFORCOMPL, 
            NAME, ApexPages.Severity.ERROR);
        }
        return ql;
    }
    
    /**
    * @author <Accenture>
    * @date <13/11/2015>
    * @name <simulatedDateTime>
    * @description <>
    * @param none
    * @return EP_Simulated__c
    */
    public static EP_Simulated__c simulatedDateTime {
        get {
            if (simulatedDateTime == NULL) {
                simulatedDateTime = EP_Simulated__c.getInstance();
            }
            return simulatedDateTime;
        }
        set;
    }

    /**
    * @author <Accenture>
    * @date <13/11/2015>
    * @name <returnShipToDateTime>
    * @description <Return current date/time>
    * @param DateTime
    * @return DateTime
    */
    @TestVisible private static DateTime returnShipToDateTime(DateTime currentShipToDateTime) {
        
        if (simulatedDateTime.EP_Simulated_Date_Time__c != NULL) {
            currentShipToDateTime = simulatedDateTime.EP_Simulated_Date_Time__c;
        }
        
        return currentShipToDateTime;
    }

//45463 Starts  
    /**
    * @author <Accenture>
    * @date <13/11/2015>
    * @name <checkShipToAccountsForCompliance>
    * @description <Check ship to Account for Compliance>
    * @param List<EP_Tank__c>
    * @return void
    */
    public static void checkShipToAccountsForCompliance(List<EP_Tank__c> tanks) {
        // Ship-Tos to be updated
        Map<ID, Account> mapShipTos = new Map<ID, Account>(); 
        map<Id,Id> mapAccParentId = new map<Id,Id>();
        // Tank-Dips to be inserted
        List<EP_Tank_Dip__c> listTankDipsForInsert = new List<EP_Tank_Dip__c>();
        EP_Tank_Dip__c tankDipForInsert;
        
        DateTime scheduledTankDipDateTime = NULL;
        //DateTime tankDipDeadlineDateTime = NULL;
        //DateTime notificationLimitDateTime = NULL;
        DateTime shipToCurrentDateTime = NULL;
        DateTime lastTankDipDateTime = NULL;
        Date currentSiteDate = NULL;
        Date lastPlaceholderTankDipDate = NULL;
        Time tankDipTime = NULL;
        Boolean proceedToNextBoolean = FALSE;
        
        // Scheduler deadline settings
        //Map<String, EP_VMI_Notification_Configuration__c> mapSchedulerSettings = EP_VMI_Notification_Configuration__c.getAll();
        //String schedulerDeadlineString;
        try{
            for (EP_Tank__c t : tanks) {
                // Initialise internal variables for each Ship-To run
                proceedToNextBoolean = TRUE;
                system.debug('t.EP_Ship_To__r.EP_Ship_To_Current_Date_Time__c = '+t.EP_Ship_To__r.EP_Ship_To_Current_Date_Time__c);
                if (t.EP_Ship_To__r.EP_Ship_To_Current_Date_Time__c != NULL) {
                    shipToCurrentDateTime = returnShipToDateTime(t.EP_Ship_To__r.EP_Ship_To_Current_Date_Time__c);
                    // 1. Check if placeholder the scheduled job needs to create new placeholder tank dips
                    currentSiteDate = returnLocalDate(shipToCurrentDateTime);
                    lastTankDipDateTime = returnLocalDate(t.EP_Last_Dip_Ship_To_Date_Time__c);
                    lastPlaceholderTankDipDate = returnLocalDate(t.EP_Last_Placeholder_Ship_To_Date_Time__c);
                    DateTime projectedReadingDateTime = null;
                    if (null != t.EP_Last_Dip_Reading_Date__c){
                        projectedReadingDateTime = t.EP_Last_Dip_Reading_Date__c.addHours(24);
                    }else{
                        projectedReadingDateTime = t.CreatedDate.addHours(24);
                    }
                    // Only create a new placeholder tank dip if the latest tank dip is from the previous day
                    // or if the site does not have a tank dip already
                    if (lastPlaceholderTankDipDate == NULL || lastPlaceholderTankDipDate < currentSiteDate) {
                        if(lastTankDipDateTime == NULL || Date.valueOf(lastTankDipDateTime) < Date.valueOf(shipToCurrentDateTime)) {
                            if (t.EP_Missing_Placeholder_Dip_For_Today__c) {
                                // Add new placeholder tank dip into the system
                                tankDipForInsert = generatePlaceholderTankDip(t, System.Now());
                                listTankDipsForInsert.add(tankDipForInsert);
                            } // End missing placeholder dip formula field check
                        } // End last dip date check
                    } // End last placeholder dip date check
                    
                    // Then progress with the notifications
                    // The algorithm is progressing with the notifications in reverse order
                    // to ensure that the user is always receiving the latest notification only once
                    if (TANK_OPERATIONAL_STATUS.equals(t.EP_Tank_Status__c)) {
                        // Constuct the scheduled tank dip date/time of the Ship-To if there is any set against the Ship-To
                        tankDipTime = returnTimeFromString(String.ValueOf(t.EP_Ship_To__r.EP_Tank_Dips_Submission_Time__c));
                        
                        scheduledTankDipDateTime = DateTime.newInstance(shipToCurrentDateTime.Year(), 
                        shipToCurrentDateTime.Month(), 
                        shipToCurrentDateTime.Day(), 
                        tankDipTime.Hour(),
                        tankDipTime.Minute(),
                        0);
                        
                        // 2. Check if the system should send out notification #Email
                           if (projectedReadingDateTime.addHours(-Integer.valueOf(t.EP_Ship_To__r.EP_Reminder_Notification_Interval_in_hrs__c)) > t.EP_Ship_To__r.EP_Ship_To_Current_Date_Time__c
                                || projectedReadingDateTime.addHours(24) < t.EP_Ship_To__r.EP_Ship_To_Current_Date_Time__c){
                            
                            continue;
                            system.debug('Enter Continue');
                        }
                            // Check if the scheduled time is less or equal to the current site time(Ship-To)
                            // Notification no 2 has to be sent out only if the time difference between last dip and scheduled is > 24hrs and current time and scheduled is < 1                      
                            if (t.EP_Ship_To__r.EP_Ship_To_Current_Date_Time__c.hour() >= projectedReadingDateTime.addHours(-Integer.valueOf(t.EP_Ship_To__r.EP_Reminder_Notification_Interval_in_hrs__c)).hour()
                                && (scheduledTankDipDateTime.addHours(-Integer.valueOf(t.EP_Ship_To__r.EP_Reminder_Notification_Interval_in_hrs__c)).hour() == t.EP_Ship_To__r.EP_Ship_To_Current_Date_Time__c.hour()) 
                                ) {
                                // Check if the Account existing in the map
                                if (!mapShipTos.containsKey(t.EP_Ship_To__c)){
                                    mapShipTos.put(t.EP_Ship_To__c,createAccount(t));
                                }
                                // Update the Account field
                                mapShipTos.get(t.EP_Ship_To__c).EP_Send_VMI_Compliance_Notification_1__c = TRUE;
                                mapAccParentId.put(t.EP_Ship_To__c,t.EP_Ship_To__r.parentId);
                            } // End time check
                            // Notification no 3 has to be sent out only if the time difference between last dip and scheduled is > 24hrs and current time = scheduled time 
                            if ((projectedReadingDateTime.hour() <= t.EP_Ship_To__r.EP_Ship_To_Current_Date_Time__c.hour())
                                    && t.EP_Ship_To__r.EP_Ship_To_Current_Date_Time__c.hour() == scheduledTankDipDateTime.hour()) {
                                // Check if the Account existing in the map
                                if (!mapShipTos.containsKey(t.EP_Ship_To__c)){
                                    mapShipTos.put(t.EP_Ship_To__c, createAccount(t));
                                }  
                                // Update the Account field
                                mapShipTos.get(t.EP_Ship_To__c).EP_Send_VMI_Compliance_Notification_2__c = TRUE;
                                mapAccParentId.put(t.EP_Ship_To__c,t.EP_Ship_To__r.parentId);
                            } // End Ship-To scheduled time check
                            if (t.EP_Ship_To__r.EP_Ship_To_Current_Date_Time__c.hour() >= projectedReadingDateTime.addHours(+Integer.valueOf(t.EP_Ship_To__r.EP_Reminder_Notification_Interval_in_hrs__c)).hour()
                                    && (scheduledTankDipDateTime.addHours(Integer.valueOf(t.EP_Ship_To__r.EP_Reminder_Notification_Interval_in_hrs__c)).hour() == t.EP_Ship_To__r.EP_Ship_To_Current_Date_Time__c.hour()) 
                                    ) {
                                    // Check if the Account existing in the map
                                    if (!mapShipTos.containsKey(t.EP_Ship_To__c))
                                    mapShipTos.put(t.EP_Ship_To__c, createAccount(t));
                                    
                                    // Update the Account field
                                    mapShipTos.get(t.EP_Ship_To__c).EP_Send_VMI_Compliance_Notification_3__c = TRUE;
                                    mapAccParentId.put(t.EP_Ship_To__c,t.EP_Ship_To__r.parentId);
                                    //proceedToNextBoolean = FALSE;
                                } // End time check
                        } // End proceed next check
                    } // End proceed next check                                                                   
                 // End Ship-To current date/time check 
            }//End for
            system.debug('mapShipTos =  '+mapShipTos);
            EP_CheckRecursive.isBatchRunning = true;
            // Insert placeholder tank dips in the system (if there are any)
            if (!listTankDipsForInsert.isEmpty()) {
                Database.insert(listTankDipsForInsert);
            }
            isDipReminderToBeSent(mapShipTos,mapAccParentId);
            // Update Ship-To records with the new compliance status (if there are any in need for an update)
            system.debug('mapShipTos = '+mapShipTos);
            if (!mapShipTos.isEmpty()) {
                Database.update(mapShipTos.values());
            } // End Ship-To list for update check
        }
        catch(Exception e){
            EP_loggingService.loghandledException(e,EPUMA, METHOD_CHECK_SHIP_ACCT ,CLASS_NAME,apexPages.severity.ERROR);
        }
    }
    
    /**
    * @author <Accenture>
    * @date <13/11/2015>
    * @name <isDipReminderToBeSent>
    * @description <check if notification is enabled for this type>
    * @param EP_Tank__c
    * @return Account
    */
    @TestVisible private static void isDipReminderToBeSent(map<Id,Account> mapShipTos, map<Id,Id> mapAccParentId){
        map<Id, list<EP_Notification_Account_Settings__c>> notificationSettingMap = 
                    EP_Account_Notification_SettingsMapper.getNotificationSettingByAcc(mapAccParentId.values());
        system.debug('notificationSettingMap = '+notificationSettingMap);
        for(Account acc : mapShipTos.values()){
            if(!getAccNotify(notificationSettingMap.get(mapAccParentId.get(acc.id)),DIP_REMINDERS).EP_Enabled__c)
                mapShipTos.remove(acc.id);
        }
    }

    /* @author <Accenture>
    * @date <10/1/2018>
    * @name <getAccNotifyId>
    * @description <return Account Notification Id>
    * @param List<EP_Notification_Account_Settings__c>, String
    * @return Id
    */
    @TestVisible private static EP_Notification_Account_Settings__c getAccNotify(List<EP_Notification_Account_Settings__c> accNotifyList,String notificationName){
        EP_Notification_Account_Settings__c accNotify = new EP_Notification_Account_Settings__c();
        for(EP_Notification_Account_Settings__c obj : accNotifyList){
            if(obj.EP_Notification_Code__c == notificationName){
                accNotify = obj;
                break;
            }
        }
        return accNotify;
    }
//45463 Ends

    /**
    * @author <Accenture>
    * @date <13/11/2015>
    * @name <createAccount>
    * @description <create account with tank ship to Id>
    * @param EP_Tank__c
    * @return Account
    */
    @TestVisible private static account createAccount(EP_Tank__c tank){ 
        return new Account(id=tank.EP_Ship_To__c);
    }     
    
    /**
    * @author <Accenture>
    * @date <13/11/2015>
    * @name <generatePlaceholderTankDip>
    * @description <Generate Placeholder TankDip>
    * @param EP_Tank__c, DateTime
    * @return EP_Tank_Dip__c
    */
    @TestVisible private static EP_Tank_Dip__c generatePlaceholderTankDip(EP_Tank__c tank, DateTime siteCurrentDateTime) {
        
        String recordTypeString = Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get(PLACEHOLDER).getRecordTypeId();
        EP_Tank_Dip__c placeholderTankDip = new EP_Tank_Dip__c();

        placeholderTankDip.RecordTypeId = recordTypeString;
        placeholderTankDip.EP_Ambient_Quantity__c = 0.00;
        /* #L4_45352_Start*/
        placeholderTankDip.EP_Unit_Of_Measure__c = tank.EP_UnitOfMeasure__c;
        /* #L4_45352_END*/
        placeholderTankDip.EP_Tank__c = tank.Id;
        placeholderTankDip.EP_Tank_Dip_Entered_In_Last_14_Days__c = true;
        placeholderTankDip.EP_Reading_Date_Time__c = siteCurrentDateTime;
        placeholderTankDip.EP_Tank_Status__c = tank.EP_Tank_Status__c;

        return placeholderTankDip; 
    }
    
    /**
    * @author <Accenture>
    * @date <13/11/2015>
    * @name <returnTimeFromString>
    * @description <return time from String>
    * @param String
    * @return Time
    */
    public static Time returnTimeFromString(String timeString) {
        Time t = NULL;
        List<String> timeComponents = new List<String>();
        try{
            // Initialise time component array
            timeComponents.add(ZERO_STRING);
            timeComponents.add(ZERO_STRING);   
            
            if(timeString != NULL) {
                timeComponents = timeString.split(TIME_SEPARATOR_SIGN);
            }
            
            t = Time.newInstance(Integer.valueOf(timeComponents[0]), Integer.valueOf(timeComponents[1]), 0, 0);
        }
        catch(Exception e){
            EP_loggingService.loghandledException(e,EPUMA, RET_TIME ,CLASS_NAME,apexPages.severity.ERROR);
        }
        return t;
    }
    
    /**
    * @author <Accenture>
    * @date <13/11/2015>
    * @name <returnLocalDate>
    * @description <Return Local Date>
    * @param DateTime
    * @return Date
    */
    private static Date returnLocalDate(DateTime dt) {
        Date selectedDate;
        
        if (dt != NULL) {
            selectedDate = (Date)JSON.deserialize(DOUBLE_QUOTES_STRING + dt.Year() + HYPHEN + dt.Month() + HYPHEN + dt.Day() + DOUBLE_QUOTES_STRING, Date.class);
        } // End dt check
        
        return selectedDate;
    }
}