/**
* @Author      : Kalpesh Thakur<kalpesh.j.thakur@accenture.com>
* @name        : EP_VMIStrategy
* @CreateDate  : 31/01/2017
* @Description : This class executes logic for VMI Orders
* @Version     : <1.0>
* @reference   : N/A
*/

public class EP_VMIStrategy extends EP_VendorManagement {

    EP_AccountMapper accountMapper = new EP_AccountMapper();

    public EP_VMIStrategy() {
    }

    /** This method is used to update Payment Fields on ORder for VMI Orders
    *  @date      01/02/2017
    *  @name      updatePaymentTermAndMethod
    *  @param     List<Order>
    *  @return    NA
    *  @throws    NA
    */
    public override void doUpdatePaymentTermAndMethod(csord__Order__c orderObj) {
        EP_GeneralUtility.Log('Public','EP_VMIStrategy','doUpdatePaymentTermAndMethod');
        Map<String, EP_Customer_Support_Settings__c> localSupportNumbers = EP_Customer_Support_Settings__c.getAll();
        // Query Account Records
        Account objAccount = accountMapper.getAccountRecordById(orderObj.AccountId__c);
        // Check If Bill Account Exist
        if(objAccount != null
            && objAccount.EP_Bill_To_Account__c != null ) {
            // Set Payment field from Bill To
            orderObj.EP_Email__c = objAccount.EP_Email__c;
            setPaymentFields(orderObj,objAccount.EP_Bill_To_Account__r, localSupportNumbers);
        }
        else if(objAccount != null) {
            // Set Payment field from Sell To
            orderObj.EP_Email__c = objAccount.EP_Email__c;
            setPaymentFields(orderObj,objAccount, localSupportNumbers);
        }
    }


    /** This method is used to update Payment Fields for Bill-To and Sell-TO Accounts
    *  @date     01/02/2017
    *  @name     setPaymentFields
    *  @param    Order orderObj,Account objAcc,Map<String,EP_Payment_Term__c> mapPayTerm,
    String strPaymentTerm,Map<String, EP_Customer_Support_Settings__c> localSupportNumbers
    *  @return   NA
    *  @throws   NA
    */
    @testVisible
    private void setPaymentFields(csord__Order__c orderObj,Account objAcc,Map<String, EP_Customer_Support_Settings__c> localSupportNumbers) {
        EP_GeneralUtility.Log('Public','EP_VMIStrategy','setPaymentFields');
        orderObj.BillingCountry__c = objAcc.BillingCountry;
        orderObj.BillingCity__c = objAcc.BillingCity;
        orderObj.EP_Payment_Method__c = objAcc.EP_Payment_Method__c;
        orderObj.EP_Payment_Term_Value__c = objAcc.EP_Payment_Term_Lookup__c;
        orderObj.EP_Price_Consolidation_Basis__c = objAcc.EP_Price_Consolidation_Basis__c;
        orderObj.EP_Billing_Basis__c = objAcc.EP_Billing_Basis__c;
        // set Local Number
        super.setLocalNumberonOrder(localSupportNumbers.values(),objAcc,orderObj);
        //sets payment term for packaged order 
        super.setPaymntTermValForPckgOdr(objAcc.EP_Package_Payment_Term__c,orderObj);
    }


    /** This method is used to set delivery type as "Pipeline"
    *  @date      05/02/2017
    *  @name      updateDeliveryType
    *  @param     List<Order>
    *  @return    NA
    *  @throws    NA
    */    
    public void doUpdateDeliveryType(csord__Order__c orderObj){
        EP_GeneralUtility.Log('Public','EP_VMIStrategy','doUpdateDeliveryType');
        if (orderObj.EP_Transporter__c != null ) {
            if (orderObj.EP_Transporter__r.Name.equalsIgnoreCase(EP_Common_Constant.NO_TRANSPORTER)) {
                orderObj.EP_Delivery_Type__c = EP_Common_Constant.PIPELINE;
            }              
        }
    }

    /** This method is used to set VMI Suggestion
    *  @date      05/02/2017
    *  @name      updateVMISuggestion
    *  @param     List<Order>
    *  @return    NA
    *  @throws    NA
    */ 
    public void doUpdateVMISuggestion(csord__Order__c orderObj) {
        EP_GeneralUtility.Log('Public','EP_VMIStrategy','doUpdateVMISuggestion');
        Account objAccount = accountMapper.getAccountByAcountId(orderObj.EP_ShipTo__c);
        if(objAccount != NULL 
            && objAccount.EP_VMI_Suggestion__c == True) {
            orderObj.EP_Is_VMI_Suggestion_Order__c = True; 
        }
    }

    /**  to fetch the record type of order
    *  @date      05/02/2017
    *  @name      findRecordType
    *  @param     NA
    *  @return    Id 
    *  @throws    NA
    */
    public override Id findRecordType() {
        EP_GeneralUtility.Log('Public','EP_VMIStrategy','findRecordType');
        return EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ORDER, EP_Common_Constant.VMI_ORDER_RECORD_TYPE_NAME);
    }
}