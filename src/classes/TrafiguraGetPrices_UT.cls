@isTest
public class TrafiguraGetPrices_UT {

    @isTest
    static void execute_test(){
        
        Pricebook2 pricebook = new Pricebook2(Name = 'Standard Price Book', isActive = TRUE);
        insert pricebook;
        System.Assert(pricebook.Id != null);
        
        Product2 prod = new Product2(Name = 'Product');
        insert prod;
        System.Assert(prod.Id != null);
        
        PricebookEntry entry = new PricebookEntry();
        entry.UnitPrice = 20;
        entry.Pricebook2Id = pricebook.Id;
        entry.Product2Id = prod.Id;
        INSERT entry;
        System.Assert(entry.Id != null);
        
        Account acc = new Account();
        acc.EP_VendorType__c = '3rd Party Stock Supplier';
        acc.EP_Vendor_Type__c = '3rd Party Stock Supplier';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        acc.Name='testEPAccount';
        acc.EP_Status__c = '06-Blocked';
        insert acc;
        System.Assert(acc.Id != null);
        
        cscfga__Product_Basket__c testBasket = new cscfga__Product_Basket__c();
        testBasket.csordtelcoa__Account__c = acc.Id;
        INSERT testBasket;
        
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ord.EP_Error_Product_Codes__c = 'Test';
        ord.csord__Identification__c = testBasket.Id;
        update ord;
        System.Assert(ord.Id != null);
        
        EP_CS_OutboundMessageSetting__c customSettingData = new EP_CS_OutboundMessageSetting__c();
        customSettingData.Name = EP_AccountConstant.SFDC_TO_NAV_SHIPTO_EDIT;
        customSettingData.Target_Integration_System__c = 'NAV';
        customSettingData.End_Point__c = 'https://api.eu.coolfurnace.net/wp3-dev/IntegrationService/api/sfdc2nav/ordersync';
        customSettingData.Payload_VF_Page_Name__c = 'Test';
        insert customSettingData;
        
        String jsonOrder = '{"lineItems":[{"companyCode":"666"}]}';
        
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('BasketId', ord.csord__Identification__c);
        inputMap.put('OrderJSON', jsonOrder);
        inputMap.put('AccId', acc.Id);
        
        Test.startTest();
        try {
            TrafiguraGetPrices.test();
        }
        catch (Exception ex) {
        }    
        TrafiguraGetPrices trafigura = new TrafiguraGetPrices();
        trafigura.execute(inputMap);
        Test.stopTest();
    }    
   
}