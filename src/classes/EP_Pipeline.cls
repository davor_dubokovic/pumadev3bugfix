/**
    @Author         Accenture
    @Name           EP_Pipeline 
    @Createdate     07/03/2017 
    @Description    
    @Version        1.0
    @Reference      NA
*/
public with sharing class EP_Pipeline extends EP_DeliveryType {
    
    /**     
        * @Author       Accenture
        * @Name         EP_Pipeline 
        * @Description  Constructor
    */
    public EP_Pipeline() {  
    }

    /**         
        * @Author       Accenture
        * @Date         13/02/2017
        * @Name         updateTransportService
        * @Description  This method sets Use Managed Trasnport Services as NA
        * @Param        csord__Order__c 
        * @Return       NA 
        * @Throws       NA
    */
    public override void updateTransportService(csord__Order__c objOrder) {
        EP_GeneralUtility.Log('Public','EP_Pipeline','updateTransportService');
        objOrder.EP_Use_Managed_Transport_Services__c = EP_Common_Constant.STR_NA;
    }

    /**         
        * @Author       Accenture
        * @Date         07/03/2017
        * @Name         isOrderEntryValid
        * @Description  This method checks if Order Eentry is valid
        * @Param        integer 
        * @Return       Boolean 
        * @Throws       NA
    */
    public override Boolean isOrderEntryValid(integer numOfTransportAccount){
        EP_GeneralUtility.Log('Public','EP_Pipeline','isOrderEntryValid');
        if (numOfTransportAccount < 1) { 
            return false;   
        }
        return true;
    }
    
    public override List<PriceBookEntry> findPriceBookEntries(Id pricebookId,csord__Order__c objOrder) {
        EP_GeneralUtility.Log('Public','EP_Delivery','findPriceBookEntries');
        System.debug('***pricebookId******'+pricebookId+'*****ISO*****'+objOrder.CurrencyIsoCode+'********Category***'+objOrder.EP_Order_Product_Category__c);
        return new EP_PriceBookEntryMapper().getRecordsByFilterSet_1(new Set<Id>{pricebookId},new Set<String>{objOrder.CurrencyIsoCode}, new Set<String>{objOrder.EP_Order_Product_Category__c},true);
    }


    // Requirement 59441 - Start
    // Though it is added for Modern ux requirement, as this is one of the delivery type,
    // it is good to have shiptos in pipeline as well
    /**         
        * @Author       Accenture
        * @Date         05/02/2017
        * @Name         getShipTos
        * @Description  This method returns getShiptos for the AccountId
        * @Param        Id 
        * @Return       List<Account> 
        * @Throws       NA
    */
    public override List<Account> getShipTos(Id accountId) {
        EP_GeneralUtility.Log('Public','EP_Pipeline','getShipTos');
        List<Account> lstShipTos = EP_AccountMapper.getAllShipTos(accountId);
        return lstShipTos;
    }

    // Requirement 59441 - End
}