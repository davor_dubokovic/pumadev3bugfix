/* 
   @Author <Accenture>
   @name <EP_WINDMSOrderNewHandler>
   @CreateDate <02/27/2017>
   @Description <These are the stub classes to map JSON string for orders creation Rest Service Request from WINDMS> 
   @Version <1.0>
*/
/* Main stub Class for JSON parsing for VMI order Creation from WINDMS  */
public class EP_WINDMSOrderNewHandler extends EP_InboundHandler {
    @TestVisible private static List<EP_AcknowledgementStub.dataSet> ackResponseList = new list <EP_AcknowledgementStub.dataSet> ();
    @TestVisible private static boolean processingFailed = false;
    @TestVisible private static EP_WINDMSOrderNewHelper VMIOrderHelper =  new EP_WINDMSOrderNewHelper();
    @TestVisible private map<id,Order> orderMap = new map<id,Order>();
    /**
    * @Author       Accenture
    * @Name         doProcess
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    public override string processRequest(string jsonBody) {
        EP_GeneralUtility.Log('public','EP_WINDMSOrderNewHandler','doProcess');
        EP_MessageHeader headerCommon = new EP_MessageHeader();
        string failureReason;
     try {

            EP_WINDMSOrderNewStub stub = (EP_WINDMSOrderNewStub )  System.JSON.deserialize(jsonBody, EP_WINDMSOrderNewStub.class);
            headerCommon = stub.MSG.HeaderCommon;
                        system.debug('stub ' + stub);
            list<EP_WINDMSOrderNewStub.orderWrapper> orderWrapperList = stub.MSG.Payload.Any0.Orders.Order;
            
            VMIOrderHelper.setCompanyCode(stub.MSG.HeaderCommon.SourceCompany);
            
	     VMIOrderHelper.setOrderAttributesNonVMI(orderWrapperList);
            List<Decimal> lineNumberList = new List<Decimal>();
            Map<Decimal,decimal> linenumberQuanityMap = new Map<Decimal,decimal>();
            Set<String> stockHldngLocSet = new Set<String>();
            //VMIOrderHelper.setRecordTypeId(orderWrapperList[0]);
            //VMI ORDER update part
            if(orderWrapperList[0].isExistingOrder)
            {
               // createOrders(orderWrapperList);     
                if(orderWrapperList[0].sfOrder.RecordTypeId !=  EP_Common_Util.fetchRecordTypeId('csord__Order__c',EP_Common_Constant.VMI_ORDER_RECORD_TYPE_NAME)){

                    system.debug('NON VMI');
                    // fetch the line items
                    
                    system.debug('orderlines actual 1 : '+orderWrapperList[0].OrderLines.OrderLine );
                    
            
                    for(EP_WINDMSOrderNewStub.OrderLine Line :orderWrapperList[0].OrderLines.OrderLine){
                        system.debug('line is :' + Line);
                        if (Line.Identifier.lineNr != null) {
                            lineNumberList.add(Decimal.Valueof(Line.Identifier.lineNr));
                            linenumberQuanityMap.put(Decimal.Valueof(Line.Identifier.lineNr),Decimal.valueOf(Line.qty));
                        }
                        stockHldngLocSet.add(Line.stockHldngLocId);
                    } 
                    system.debug('linenumberQuanityMap' +linenumberQuanityMap );
                    List<csord__Order_Line_Item__c> oliList = [SELECT id, Planned_Quantity__c, Order_Line_Number__c FROM csord__Order_Line_Item__c 
                        WHERE EP_NonVMIVMI_OrderLIneNumber__c in :lineNumberList
                        AND csord__Order__c = : orderWrapperList[0].sfOrder.id];
                   
                    if(oliList.size() > 0) {
                        for(csord__Order_Line_Item__c oli :oliList){
                            if (oli.Order_Line_Number__c != null){
                            oli.Planned_Quantity__c= linenumberQuanityMap.get(Decimal.valueof(oli.Order_Line_Number__c));
                          }  
                        }
                       
                        update oliList;
                       
                    }

                    EP_StockHoldingLocationMapper stockHoldMapper = new EP_StockHoldingLocationMapper();
                    map<String,EP_Stock_Holding_Location__c> storageStockLocationMap = stockHoldMapper.getStockHoldingLocations(stockHldngLocSet, stub.MSG.HeaderCommon.SourceCompany);
                    orderWrapperList[0].sfOrder.Supply_Location_Logistics__c = storageStockLocationMap.get(orderWrapperList[0].OrderLines.OrderLine[0].stockHldngLocId).Stock_Holding_Location__c;

                    orderWrapperList[0].sfOrder.EP_Order_Credit_Status__c = EP_Common_Constant.WINDMS_Credit_Okay;
                    update  orderWrapperList[0].sfOrder;
                    EP_OrderDomainObject orderObjc = new EP_OrderDomainObject(orderWrapperList[0].sfOrder); 
                    EP_OrderService service = new EP_OrderService(orderObjc);
                    service.doSyncCreditCheckWithWINDMS();

                } else {
                    system.debug('VMI');
                    VMIOrderHelper.setOrderAttributes(orderWrapperList);
                    List<Id> sellToShipToIdList = new List<Id>();
                    for(EP_WINDMSOrderNewStub.orderWrapper currentOrder:orderWrapperList) {
                        System.Debug('Test 2-->'+currentOrder.sfOrder);
                        if(currentOrder.sfOrder.EP_Sell_To__c!=null)
                            sellToShipToIdList.add(currentOrder.sfOrder.EP_Sell_To__c);
                        if(currentOrder.sfOrder.EP_ShipTo__c!=null)
                            sellToShipToIdList.add(currentOrder.sfOrder.EP_ShipTo__c);
                    }

                    List<Account> sellToShipToList = [SELECT Id,EP_Status__c from account where id in :sellToShipToIdList 
                        and (EP_Status__c= :EP_Common_Constant.STATUS_BLOCKED OR EP_Status__c=  :EP_Common_Constant.STATUS_INACTIVE)];

                    if(!sellToShipToList.isEmpty()) {
                        for(Account acc : sellToShipToList) {
                        //CB with response neeeded TODO
                            for(EP_WINDMSOrderNewStub.orderWrapper currentOrder:orderWrapperList) {
                                if(currentOrder.sfOrder.EP_Sell_To__c==acc.id) {
                                    currentOrder.sfOrder.EP_Order_Credit_Status__c = EP_Common_Constant.WINDMS_CREDIT_BLOCK;
                                    currentOrder.sfOrder.Credit_Check_Reason_Code_XML__c = 'Sell-to Blocked';
                                    EP_OrderDomainObject orderObjc = new EP_OrderDomainObject(currentOrder.sfOrder);                                        
                                    EP_OrderService service = new EP_OrderService(orderObjc);
                                    service.doSyncCreditCheckWithWINDMS();
                                }
                                if(currentOrder.sfOrder.EP_ShipTo__c ==acc.id) {
                                    currentOrder.sfOrder.EP_Order_Credit_Status__c = EP_Common_Constant.WINDMS_CREDIT_BLOCK;
                                    currentOrder.sfOrder.Credit_Check_Reason_Code_XML__c = 'Ship-to Blocked';
                                    EP_OrderDomainObject orderObjc = new EP_OrderDomainObject(currentOrder.sfOrder);                                        
                                    EP_OrderService service = new EP_OrderService(orderObjc);
                                    service.doSyncCreditCheckWithWINDMS();
                                }
                            }
                        }
                    }           
                else {
                    createOrders(orderWrapperList);
                    VMIOrderHelper.setOrderItemAttributes(orderWrapperList);            
                    createOrdersItems(orderWrapperList);       
                    for(EP_WINDMSOrderNewStub.orderWrapper currentOrder:orderWrapperList) {
                        startOrchestrationProcess(currentOrder.sfOrder,'VMI Quantity Update');                  
                    }
                }
            }
        }             
            else {
                //added by CS 05/03/2018
                //sellto(shipto) inactive or blocked check before order is created
		 VMIOrderHelper.setOrderAttributes(orderWrapperList);
                //VMIOrderHelper.setOrderAttributes(orderWrapperList);
                List<Id> sellToShipToIdList = new List<Id>();
                for(EP_WINDMSOrderNewStub.orderWrapper currentOrder:orderWrapperList) {
                    System.Debug('Test 2-->'+currentOrder.sfOrder);
                    if(currentOrder.sfOrder.EP_Sell_To__c!=null)
                    sellToShipToIdList.add(currentOrder.sfOrder.EP_Sell_To__c);

                    if(currentOrder.sfOrder.EP_ShipTo__c!=null)
                    sellToShipToIdList.add(currentOrder.sfOrder.EP_ShipTo__c);
                }

                List<Account> sellToShipToList = [SELECT Id,EP_Status__c from account where id in :sellToShipToIdList 
                and (EP_Status__c= :EP_Common_Constant.STATUS_BLOCKED OR EP_Status__c=  :EP_Common_Constant.STATUS_INACTIVE)];

                if(!sellToShipToList.isEmpty()) {
                    for(Account acc : sellToShipToList) {
                    //CB with response neeeded TODO
                        for(EP_WINDMSOrderNewStub.orderWrapper currentOrder:orderWrapperList) {
                            if(currentOrder.sfOrder.EP_Sell_To__c==acc.id) {
                                currentOrder.sfOrder.EP_Order_Credit_Status__c = EP_Common_Constant.WINDMS_CREDIT_BLOCK;
                                currentOrder.sfOrder.Credit_Check_Reason_Code_XML__c = 'Sell-to Blocked';
                                EP_OrderDomainObject orderObjc = new EP_OrderDomainObject(currentOrder.sfOrder);                                        
                                EP_OrderService service = new EP_OrderService(orderObjc);
                                service.doSyncCreditCheckWithWINDMS();
                            }
                            if(currentOrder.sfOrder.EP_ShipTo__c ==acc.id) {
                                currentOrder.sfOrder.EP_Order_Credit_Status__c = EP_Common_Constant.WINDMS_CREDIT_BLOCK;
                                currentOrder.sfOrder.Credit_Check_Reason_Code_XML__c = 'Ship-to Blocked';
                                EP_OrderDomainObject orderObjc = new EP_OrderDomainObject(currentOrder.sfOrder);                                        
                                EP_OrderService service = new EP_OrderService(orderObjc);
                                service.doSyncCreditCheckWithWINDMS();
                            }
                        
                            }
                        }
                    }           
                else {
                        createOrders(orderWrapperList);           
                        VMIOrderHelper.setOrderItemAttributes(orderWrapperList);            
                        createOrdersItems(orderWrapperList);            
                //added by CS 05/03/2018
                //Orchestration process for VMI order creation starting point

                for(EP_WINDMSOrderNewStub.orderWrapper currentOrder:orderWrapperList) {
                                    system.debug('MJ:::::  '+currentOrder.sfOrder);
                    startOrchestrationProcess(currentOrder.sfOrder,'VMI Order Creation');               
                }
            }
            }
        } catch (exception exp ) {
            failureReason = exp.getMessage();
            EP_LoggingService.logServiceException(exp, UserInfo.getOrganizationId(), EP_Common_constant.EPUMA, 'processRequest', 'EP_WINDMSOrderNewHandler',  EP_Common_constant.ERROR, UserInfo.getUserId(), EP_Common_constant.TARGET_SF, EP_Common_Constant.BLANK, EP_Common_Constant.BLANK);
            
            createResponse(null, exp.getTypeName(), exp.getMessage()); 
        }
        return EP_AcknowledgementHandler.createAcknowledgement('VMI_ORDER_CREATION', processingFailed, failureReason, headerCommon, ackResponseList);
    }
    
    @TestVisible
    private void startOrchestrationProcess(csord__Order__c currentOrder, string processName) {

        List<CSPOFA__Orchestration_Process_Template__c> orchTemplates = [SELECT Id, Name 
                                                                         FROM CSPOFA__Orchestration_Process_Template__c
                                                                         WHERE Name = :processName];
        
        List<CSPOFA__Orchestration_Process__c> orchProcessesToCreate = new List<CSPOFA__Orchestration_Process__c>();
        
         // create process for every order
        orchProcessesToCreate.add(new CSPOFA__Orchestration_Process__c(Order__c = currentOrder.Id));
        
        // populate correctly Orch Processes which are going to be inserted
        for(CSPOFA__Orchestration_Process__c orchp : orchProcessesToCreate) {
            orchp.CSPOFA__Orchestration_Process_Template__c = orchTemplates[0].Id;
            orchp.Name='VMI Order Orchestration';
            orchp.CSPOFA__Priority__c = '2 - Normal';
        }
        
        system.debug('MJ::2 ' + orchProcessesToCreate);
        if(orchProcessesToCreate.size() > 0) {
            insert orchProcessesToCreate;
        }
    }
    
    /**
    * @Author       Accenture
    * @Name         createOrders
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void createOrders(list<EP_WINDMSOrderNewStub.orderWrapper> orderWrapperList) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderNewHandler','createOrders');
        list<csord__Order__c> orderList = new list<csord__Order__c>();
        for(EP_WINDMSOrderNewStub.orderWrapper orderWrapper : orderWrapperList) {
            if(string.isNotEmpty(orderWrapper.errorDescription)) {
                createResponse(orderWrapper.seqId,orderWrapper.errorCode, orderWrapper.errorDescription);
            } else {
                orderList.add(orderWrapper.sfOrder);
            }
        }
        doOrderUpsert(orderList);
    }
    
    /**
    * @Author       Accenture
    * @Name         doOrderUpsert
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void doOrderUpsert(list<csord__Order__c> orderList) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderNewHandler','doOrderUpsert');
        if(orderList.isEmpty()){
            return;
        }
        upsert orderList;
        list<Database.UpsertResult> ordersUpsertResult = DataBase.Upsert(orderList,false);
        
        for(integer counter = 0 ; counter < ordersUpsertResult.size(); counter++) {        
            if(ordersUpsertResult[counter].isSuccess()){
                createResponse(orderList[counter].EP_SeqId__c, '', '' ); 
            }
            else {
                processUpsertErrors(ordersUpsertResult[counter].getErrors(), orderList[counter].EP_SeqId__c);
            }
        }
    }
    
    /**
    * @Author       Accenture
    * @Name         processUpsertErrors
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void processUpsertErrors(list<Database.Error> errorList,string seqId) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderNewHandler','processUpsertErrors');
        for(Database.Error err : errorList) {
            createResponse(seqId,String.valueOf(err.getStatusCode()), err.getMessage() ); 
        }
    }
    /**
    * @Author       Accenture
    * @Name         createOrdersItems
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void createOrdersItems(list<EP_WINDMSOrderNewStub.orderWrapper> orderWrapperList) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderNewHandler','createOrdersItems');
        list<csord__Order_Line_Item__c> orderItemsToBeProcess  = new list<csord__Order_Line_Item__c>();
        system.debug('MJ:::: ' + orderWrapperList);
        for(EP_WINDMSOrderNewStub.orderWrapper orderWrapper : orderWrapperList) {
            for(EP_WINDMSOrderNewStub.OrderLine ordLine : orderWrapper.OrderLines.orderLine){
                if(string.isNotEmpty(ordLine.errorDescription)) {
                    system.debug('**processOrderItemsError is' + ordLine.orderLineItem.EP_SeqId__c);
                    processOrderItemsError(orderWrapper,ordLine.orderLineItem.EP_SeqId__c, ordLine.errorDescription);
                } else {
                    orderItemsToBeProcess.add(ordLine.orderLineItem);
                }
            }
        }

         // if it is order update action
        if (orderWrapperList[0] != null && String.isEmpty(orderWrapperList[0].orderIdSF) == false) {
            List<csord__Order__c> orders = [SELECT id from csord__Order__c where OrderNumber__c = :orderWrapperList[0].orderIdSF];
        
            if (orders != null && orders.size() > 0) {
                List<csord__Order_Line_Item__c> deleteItems = [SELECT id from csord__Order_Line_Item__c where csord__Order__c = :orders[0].id];
                if (deleteItems != null && deleteItems.size() > 0) {
                    delete deleteItems;
                }
            }
        }
        doOrderItemUpsert(orderItemsToBeProcess);
    }
    
    /**
    * @Author       Accenture
    * @Name         processOrderItemsError
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void processOrderItemsError(EP_WINDMSOrderNewStub.orderWrapper orderWrapper, string seqId, string errorDescription) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderNewHandler','processOrderItemsError');
        list<csord__Order__c> orderToBeDelete = new list<csord__Order__c>();
        createResponse(seqId , null, errorDescription);
        if(!orderWrapper.isExistingOrder) {
            orderToBeDelete.add(orderWrapper.sfOrder);
        }
        deleteOrder(orderToBeDelete);
    }
    
    /**
    * @Author       Accenture
    * @Name         doOrderItemUpsert
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void doOrderItemUpsert(list<csord__Order_Line_Item__c> orderItemsToBeProcess) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderNewHandler','doOrderItemUpsert');
        set<id> orderIdSet = new set<id>();
        system.debug('orderItemsToBeProcess is' + orderItemsToBeProcess);
        if(orderItemsToBeProcess.isEmpty()){
            return;
        }
        list<Database.UpsertResult> ordersItemsUpsertResult = Database.upsert(orderItemsToBeProcess,false);
        for(integer counter = 0 ; counter < ordersItemsUpsertResult.size(); counter++){           
            if(!ordersItemsUpsertResult[counter].isSuccess()){
                processUpsertErrors(ordersItemsUpsertResult[counter].getErrors(), orderItemsToBeProcess[counter].EP_SeqId__c);
            }else{
                orderIdSet.add(orderItemsToBeProcess[counter].orderId__c);
                system.debug('***orderItemsToBeProcess' + orderItemsToBeProcess[counter]);
            }
        }
        system.debug('orderIdSet is' + orderIdSet);
        List<csord__Order__c> orderWithStatusUpdate = VMIOrderHelper.validateAndSetOrderStatus(orderIdSet);
        updateOrders(orderWithStatusUpdate);

        VMIOrderHelper.doPostStatusUpdateActions(orderIdSet);
    }
    
    /**
    * @Author       Accenture
    * @Name         deleteOrder
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void deleteOrder(list<csord__Order__c> ordersObj) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderNewHandler','deleteOrder');
        if(!ordersObj.isEmpty()) {
            DataBase.Delete(ordersObj,false);
        }
    }
    
    /**
    * @Author       Accenture
    * @Name         createResponse
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void createResponse(string seqId, string errorCode, string errorDescription) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderNewHandler','createResponse');
        if(string.isNotBlank(errorDescription)) processingFailed = true;
        ackResponseList.add(EP_AcknowledgementUtil.createDataSet(EP_Common_Constant.ORDER_STRING, seqId, errorCode, errorDescription));
    }
    
    /**
    * @Author       Accenture
    * @Name         updateOrders
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void updateOrders(list<csord__Order__c> orderList) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderNewHandler','updateOrders');
        try {
            if(!orderList.isEmpty()) {
                database.update(orderList);
            }
        }catch(exception ex){
            EP_LoggingService.logServiceException(ex, UserInfo.getOrganizationId(), EP_Common_constant.EPUMA, 'UPDATE_ORDERS_STATUS', 'VMIORDER_CREATION_HANDLER', EP_Common_constant.ERROR, UserInfo.getUserId(), EP_Common_constant.TARGET_SF,EP_Common_Constant.BLANK, EP_Common_Constant.BLANK);
        }
    }
}