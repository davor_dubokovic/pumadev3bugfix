@isTest
public class EP_StorageLocation_UT{
    //L4 45526 Start
    static testMethod void doBeforeDeleteHandle_test() {
        Account acct = EP_TestDataUtility.createStockHoldingLocation();
        acct.EP_Status__c =EP_Common_Constant.STATUS_ACTIVE ;
        insert acct;
        EP_StorageLocation objStocLoc = new EP_StorageLocation();
        objStocLoc.doBeforeDeleteHandle(acct);
        // Putting the dummy assets as method not returning any value.
        system.assert(true);
    }
    //L4 45526 End
}