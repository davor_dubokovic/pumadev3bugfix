/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class EP_ROImportStagingFactory_UT {
    private static final string THIRDPARTYRTNAME =  'EP_Third_Party_Order';
    static testMethod void ProcessStagingRecordsTest() {
        //Create a File record
        EP_File__c tempFile =  new EP_File__c(Name = 'TestFile',EP_CheckSum_Key__c = '12swerdERT345sdErrr');
        database.insert(tempFile);
        Test.startTest();        
        EP_ROImportStagingFactory.ProcessStagingRecords(tempFile.Id,THIRDPARTYRTNAME);
        Test.stopTest();
        EP_File__c result = [select Id, EP_In_Process__c from EP_File__c Where Id =: tempFile.Id];
        system.assertEquals(true, result.EP_In_Process__c);        
    }
}