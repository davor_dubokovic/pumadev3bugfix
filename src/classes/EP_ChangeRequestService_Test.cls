@isTest
private class EP_ChangeRequestService_Test{

    static testMethod void addFieldValueInSObject_test() {       
        Map<String, Schema.Sobjecttype> mapOfGlobalSObjectType = Schema.getGlobalDescribe();
        sObject sObjRec = mapOfGlobalSObjectType.get('Account').newSObject();
        Map<String, Schema.SObjectField> abc =mapOfGlobalSObjectType.get('Account').getDescribe().fields.getMap();
        test.starttest();
        EP_ChangeRequestService.addFieldValueInSObject(sObjRec,'EP_Company_Is_Tax_Exempt__c','True',abc );
        test.stoptest();
        System.assertEquals(sObjRec.get('EP_Company_Is_Tax_Exempt__c'), true);
    }
    static testMethod void addFieldValueInSObject1_test() {       
        Map<String, Schema.Sobjecttype> mapOfGlobalSObjectType = Schema.getGlobalDescribe();
        sObject sObjRec = mapOfGlobalSObjectType.get('Account').newSObject();
        Map<String, Schema.SObjectField> abc =mapOfGlobalSObjectType.get('Account').getDescribe().fields.getMap();
        test.starttest();
        EP_ChangeRequestService.addFieldValueInSObject(sObjRec,'EP_AvailableFunds__c','11',abc );
        test.stoptest();
        System.assertEquals(sObjRec.get('EP_AvailableFunds__c'), 11);
    }
    static testMethod void addFieldValueInSObject2_test() {       
        Map<String, Schema.Sobjecttype> mapOfGlobalSObjectType = Schema.getGlobalDescribe();
        sObject sObjRec = mapOfGlobalSObjectType.get('Account').newSObject();
        Map<String, Schema.SObjectField> abc =mapOfGlobalSObjectType.get('Account').getDescribe().fields.getMap();
        test.starttest();
        EP_ChangeRequestService.addFieldValueInSObject(sObjRec,'EP_Available_Funds__c','12.22',abc );
        test.stoptest();
        System.assertEquals(sObjRec.get('EP_Available_Funds__c'), 12.22);
    }
    static testMethod void addFieldValueInSObject3_test() {       
        Map<String, Schema.Sobjecttype> mapOfGlobalSObjectType = Schema.getGlobalDescribe();
        sObject sObjRec = mapOfGlobalSObjectType.get('Account').newSObject();
        Map<String, Schema.SObjectField> abc =mapOfGlobalSObjectType.get('Account').getDescribe().fields.getMap();
        test.starttest();
        EP_ChangeRequestService.addFieldValueInSObject(sObjRec,'EP_Customer_Activation_Date__c','12/12/2000',abc );
        test.stoptest();
        System.assertEquals(sObjRec.get('EP_Customer_Activation_Date__c'), date.parse('12/12/2000'));
    }
    
    static testMethod void addFieldValueInSObject4_test() {       
        Map<String, Schema.Sobjecttype> mapOfGlobalSObjectType = Schema.getGlobalDescribe();
        sObject sObjRec = mapOfGlobalSObjectType.get('EP_Tank__c').newSObject();
        Map<String, Schema.SObjectField> abc =mapOfGlobalSObjectType.get('EP_Tank__c').getDescribe().fields.getMap();
        test.starttest();
        EP_ChangeRequestService.addFieldValueInSObject(sObjRec,'EP_Last_Dip_Ship_To_Date_Time__c','10/12/2011 11:46 AM',abc );
        test.stoptest();
        System.assertEquals(sObjRec.get('EP_Last_Dip_Ship_To_Date_Time__c'), datetime.parse('10/12/2011 11:46 AM'));
    }
    
    static testMethod void addFieldValueInSObject5_test() {       
        Map<String, Schema.Sobjecttype> mapOfGlobalSObjectType = Schema.getGlobalDescribe();
        sObject sObjRec = mapOfGlobalSObjectType.get('Account').newSObject();
        Map<String, Schema.SObjectField> abc =mapOfGlobalSObjectType.get('Account').getDescribe().fields.getMap();
        test.starttest();
        EP_ChangeRequestService.addFieldValueInSObject(sObjRec,'EP_AllowRelease_on_Proof_of_Payment__c','test',abc );
        test.stoptest();
        System.assertEquals(sObjRec.get('EP_AllowRelease_on_Proof_of_Payment__c'), 'test');
    }
}