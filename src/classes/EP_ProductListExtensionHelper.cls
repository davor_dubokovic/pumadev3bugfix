/***L4-45352 start****/
/***************************************************************
*  @Author <Accenture>                                         *
*  @Name <EP_ProductListExtensionHelper>                       *
*  @CreateDate <3/11/2017>                                     *
*  @Description <Helper class of EP_ProductListExtension for   *
*  implementation>
*  @Version <1.0>                                              *
****************************************************************/
public with sharing class EP_ProductListExtensionHelper{
    private EP_ProductListExtensionContext ctx;

/****************************************************************
* @author       Accenture                                       *
* @name         EP_ProductListExtensionHelper                   *
* @description  constructor of class                            *
* @param        EP_ProductListExtensionContext                  *
* @return       NA                                              *
****************************************************************/
    public EP_ProductListExtensionHelper(EP_ProductListExtensionContext ctx) {
        this.ctx = ctx;
    }

/****************************************************************
* @author       Accenture                                       *
* @name         DeleteProducts                                  *
* @description  This method is used to delete PriceBookEntry,   *
* Only if the  validation fails                                 *
* @param        Row number (Integer)                            *
* @return       NA                                              *
****************************************************************/
    Public Void DeleteProducts(Integer RowNum){
        PricebookEntry varPriceBookEntry; 
        string PriceBookEntryIdForDelete = ctx.ListProd[RowNum].id;
        string PriceBookIdToCheck = ctx.ListProd[RowNum].PriceBook2.id;
        string ProductToCheck = ctx.ListProd[RowNum].Product2.id;        
        if(PriceBookEntryIdForDelete!=null){
            ctx.ShowErrorForTank = ValidateDeletion(PriceBookIdToCheck,ProductToCheck);
            if(ctx.ShowErrorForTank == false){
                varPriceBookEntry = new PricebookEntry(id = PriceBookEntryIdForDelete);
                database.delete(varPriceBookEntry);
                ctx.ListProd.remove(RowNum);
            }
        } 
    }
/****************************************************************
* @author       Accenture                                       *
* @name         ValidateDeletion                                *
* @description  This method is used to show error if validation passes*
* @param        pricebookId and ProductId                       *
* @return       Boolean                                         *
****************************************************************/
    public static Boolean ValidateDeletion(Id PriceBookIdToCheck,Id ProductIdToCheck){
       Boolean CheckTank = false; 
       if(isProductLinkToTank(PriceBookIdToCheck,ProductIdToCheck)){
           CheckTank = true;
           Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, Label.EP_Error_on_tank_associated_to_product));
       }
       if(isProductLinkToOders(PriceBookIdToCheck,ProductIdToCheck)){
           CheckTank = true;
           Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, Label.EP_Error_if_Orders_associated_to_product));
       }
       return CheckTank; 
    }   
    
/****************************************************************
* @author       Accenture                                       *
* @name         AddProducts                                     *
* @description  If User tries to remove the Product from        *  
* ProductList associated to the Account and the Product is      *
* already associated to operational Tanks.                      *
* This method returns true to show error on deletion of product *
* @param        NA                                              *
* @return       NA                                              *
****************************************************************/
     Private static boolean isProductLinkToTank(Id PriceBookIdToCheck,Id ProductIdToCheck){
        set<Id> AccountIdSet = new set<id>();
        EP_TankMapper tankmapper = new EP_TankMapper();
        List<Account> AccountShipToList = new List<Account>();
        
           for(EP_Product_Option__c pricebookOption : EP_ProductOptionMapper.getProductOptionLinkedToPriceBook(PriceBookIdToCheck)){
               AccountIdSet.add(pricebookOption.Sell_To__c);
           }
           for(Account acc : EP_AccountMapper.getAllShipToFromSellTo(AccountIdSet)){
               if(acc.ChildAccounts.size()>0){
                   AccountShipToList.addall(acc.ChildAccounts);
               }
           }
           if(AccountShipToList.size()>0){
               for(EP_Tank__c TankVar : tankmapper.getRecordsByShipToIdsList(AccountShipToList)){
                   if(TankVar.EP_Product__c == ProductIdToCheck){
                       return true;
                   }          
               }
           }
         return false;  
    }
    /****************************************************************
* @author       Accenture                                       *
* @name         AddProducts                                     *
* @description  Below code is require clarification from CS 
* User tries to remove the Product List                         *
* associated to the Account and the Product List is already     *
* associated to Inflight Orders(Pre delivered Order stages)     *
* Order Inflight Orders(Pre delivered Order)                    *
* This method returns true to show error on prodcut deletion    *
* @param        NA                                              *
* @return       NA                                              *
***************************************************************/
    Public static boolean isProductLinkToOders(Id PriceBookIdToCheck,Id ProductIdToCheck){
        set<Id> AccountIdSet = new set<id>();
        List<EP_Product_Option__c> ListPriceBookOption = new list<EP_Product_Option__c>();
        ListPriceBookOption = [select id,Price_List__c,Sell_To__c from EP_Product_Option__c where Price_List__c = : PriceBookIdToCheck];
           for(EP_Product_Option__c pricebookOption : ListPriceBookOption){
               AccountIdSet.add(pricebookOption.Sell_To__c);
           }
           return false;  
    } 
/****************************************************************
* @author       Accenture                                       *
* @name         CreatePriceBookReview                           *
* @description  This method will create PriceBook review if method
*  IsPriceBookReviewRequired returns true on validation         *
* @param        NA                                              *
* @return       NA                                              *
****************************************************************/ 
     Public Void CreatePriceBookReview(){
     Boolean createAction = false;
     try{
          createAction = IsPriceBookReviewRequired(ctx.PriceBookId);
          if(createAction){
               EP_Action__c PriceBookAction = new EP_Action__c();
               PriceBookAction.EP_Product_List__c = ctx.PriceBookId;
               PriceBookAction.EP_Record_Type_Name__c = EP_Common_Constant.ACT_PRICEBOOK_RT;
               PriceBookAction.EP_Action_Name__c = EP_Common_Constant.ACT_PRICEBOOK_RT;
               PriceBookAction.EP_Status__c = EP_Common_Constant.ACT_NEW_STATUS;
               database.insert(PriceBookAction);
           }
       }
       catch(exception ex){
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, ex.getMessage() + EP_Common_Constant.ATLINE+ex.getLineNumber())); 
        }
    }
/****************************************************************
* @author       Accenture                                       *
* @name         IsPriceBookReviewRequired                       *
* @description  This method will check the conditions for
*               price book review creation                      *
*  This method returns true if all associated price 
*  book entries are inactive and new action is not associated 
*  with price book and account associated with pricebook is at 
*  active or basic data set up state and if these conditons are  
*  satisfied a new price book review is created in method CreatePriceBookReview 
* @param        NA                                              *
* @return       NA                                              *
****************************************************************/
    private Boolean IsPriceBookReviewRequired(Id PriceBookId){
        if(PriceBookId!=null){
               Integer CountAction = 0;
               Integer CountproductOption = 0;
               Boolean InactivePriceBookEntry = false;
               for(PriceBookEntry VarPBE : ctx.ListProd){
                   if(VarPBE.isActive == false){
                      InactivePriceBookEntry = true;
                   }
               }
               CountAction = EP_ActionMapper.getActionsAssociatedToPriceBook(PriceBookId).size();
               CountproductOption = EP_ProductOptionMapper.getProductOptionLinkedToPriceBook(PriceBookId).size();
               system.debug('>>>CountproductOption'+CountproductOption);
                system.debug('>>>CountAction'+CountAction);
                system.debug('>>>InactivePriceBookEntry'+InactivePriceBookEntry);
               if(CountAction == 0 && CountproductOption > 0 && InactivePriceBookEntry)
               {
                   return true;
               }
        }
        return false;
    }
    
}
/***L4-45352 end****/