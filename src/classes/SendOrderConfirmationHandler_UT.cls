/***
 * Author: LVirdi (CloudSense)
 * Description: Test Class for SendOrderConfirmationHandler.
 * Version History:
 * v1 - Created on 2018-04-10
 ***/
@isTest
public class SendOrderConfirmationHandler_UT {
    
    @testSetup
    public static void createTestData() {
        Pricebook2 pricebook = new Pricebook2(Name = 'Standard Price Book', isActive = TRUE);
        insert pricebook;
        System.Assert(pricebook.Id != null);
        
        Product2 prod = new Product2(Name = 'Product');
        insert prod;
        System.Assert(prod.Id != null);
        
        PricebookEntry entry = new PricebookEntry();
        entry.UnitPrice = 20;
        entry.Pricebook2Id = pricebook.Id;
        entry.Product2Id = prod.Id;
        insert entry;
        System.Assert(entry.Id != null);
        
        Account acc = new Account();
        acc.EP_VendorType__c = '3rd Party Stock Supplier';
        acc.EP_Vendor_Type__c = '3rd Party Stock Supplier';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        acc.Name='testEPAccount';
        acc.EP_Status__c = '06-Blocked';
        insert acc;
        
        System.Assert(acc.Id != null);
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        ord.EP_Error_Product_Codes__c = 'Test';
        update ord;
        System.Assert(ord.Id != null);
        
        LIST<csord__Order_Line_Item__c> oiList = [SELECT Id, OrderId__c, PricebookEntryId__c FROM csord__Order_Line_Item__c WHERE OrderId__c =:ord.Id];
        csord__Order_Line_Item__c lineItem = oiList[0];
        lineItem.PricebookEntryId__c = String.valueOf(entry.Id);
        lineItem.EP_Product__c = prod.Id;
        UPDATE lineItem;
         
        CSPOFA__Orchestration_Process_Template__c oProcessTemplate = new CSPOFA__Orchestration_Process_Template__c(Name='test template');
        insert oProcessTemplate;
        
        System.Assert(oProcessTemplate.Id != null);
        CSPOFA__Orchestration_Process__c oProcess = new CSPOFA__Orchestration_Process__c(Name='test process', 
                                                                                         CSPOFA__Orchestration_Process_Template__c=oProcessTemplate.Id,
                                                                                         Order__c=ord.Id
                                                                                        );
        insert oProcess;

        System.Assert(oProcess.Id != null);
        CSPOFA__Orchestration_Step__c oStep = new CSPOFA__Orchestration_Step__c(Name='test step',
                                                                                CSPOFA__Orchestration_Process__c=oProcess.Id
                                                                               );
        insert oStep;
    }
    
    public static testmethod void testSendOrderConfirmationHandler() {
        list<SObject> steps = [select Id from CSPOFA__Orchestration_Step__c];        
        
        test.startTest();        
        System.assert(steps.size() > 0, 'Invalid data'); 
        SendOrderConfirmationHandler  sendOrderConf = new SendOrderConfirmationHandler ();  
        sendOrderConf.process(steps);      
        test.stopTest();           
        
    }
}