@isTest
private class SOQLBuilder_UT {

	static void testDataInsert() {
		List<cscfga__User_Session__c> recordsToInsert = new cscfga__User_Session__c[] {
			new cscfga__User_Session__c(
				name = 'Test account 1',
				cscfga__user_agent__c = 'Street 1'
			),
			new cscfga__User_Session__c(
				name = 'Test account 2',
				cscfga__user_agent__c = 'Street 2'
			),
			new cscfga__User_Session__c(
				name = 'Random account 1',
				cscfga__session_id__c = 'Aggriculture',
				cscfga__ip_address__c = '10234',
				cscfga__pc_return_url__c = 'Yes'
			),
			new cscfga__User_Session__c(
				name = 'Random account 2',
				cscfga__session_id__c = 'Banking',
				cscfga__ip_address__c = '102345',
				cscfga__pc_return_url__c = 'No'
			),
			new cscfga__User_Session__c(
				name = 'Random account 3',
				cscfga__session_id__c = 'Apparel',
				cscfga__ip_address__c = '1023456',
				cscfga__pc_return_url__c = 'Yes'
			),
			new cscfga__User_Session__c(
				name = 'Supa account 1337',
				cscfga__session_id__c = 'Apparel',
				cscfga__ip_address__c = '1337',
				cscfga__pc_return_url__c = 'Yes',
				cscfga__user_agent__c = 'Radnicka cesta 80'
			)
		};

		insert recordsToInsert;
	}

	@IsTest
	static void testSimpleAccountSelect() {

		testDataInsert();
		Test.startTest();

		SOQLBuilder bldr = new SOQLBuilder(cscfga__User_Session__c.sObjectType);
		bldr.selectColumn('Id');
		bldr.selectColumn('Name');
		bldr.selectColumns(new String[] { 'cscfga__user_agent__c', 'IsDeleted' });
		String query = bldr.getSOQL();

		Test.stopTest();

		List<cscfga__User_Session__c> recsByBuilder = Database.query(query);
		List<cscfga__User_Session__c> wantedSOQLRecords = [select id, name, cscfga__user_agent__c, isDeleted from cscfga__User_Session__c];

		System.assertEquals(
			wantedSOQLRecords.size(),
			recsByBuilder.size(),
			'Simple account select returned different values with SOQLBuilder query from exprected Query'
		);

		// Now we check order and values
		for (Integer iter = 0; iter < wantedSOQLRecords.size(); iter++) {
			cscfga__User_Session__c recFromBldr = recsByBuilder.get(iter);
			cscfga__User_Session__c recFromSOQL = wantedSOQLRecords.get(iter);

			System.assertEquals(
				recFromSOQL.id,
				recFromBldr.id,
				'Order in SOQLBuilder and integrated SOQL doesn\'t match. Objects are not the same'
			);
			checkValues(recFromSOQL, recFromBldr, new String[] {'name', 'cscfga__user_agent__c', 'isDeleted'});

			try {
				System.debug(recFromBldr.cscfga__last_accessed_date__c);
				System.assert(false, 'Last_Accessed_Date__c field shouldn\'ve been fetched using SOQLBuilder class');
			} catch (Exception e) {
				System.assert(true);
			}
		}
	}

	@IsTest
	static void testConstructors() {

		Test.startTest();

		SOQLBuilder bldr = new SOQLBuilder(Account.sObjectType);
		System.assertEquals(
			Account.sObjectType,
			bldr.objectType,
			'Object type not set correctly'
		);

		// empty object type should be false
		try {
			bldr = new SOQLBuilder(null);
			System.assert(false, 'Empty object type is acceptable by SOQLBuilder. It should not be accepted.');
		} catch (Exception e) {
			System.assert(true);
		}

		try {
			Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();
			for (String key : globalDescribe.keySet()) {
				// We have to find a nonqueryable sObject first
				Schema.SObjectType objType = globalDescribe.get(key);
				if (objType.getDescribe().isQueryable()) {
					continue;
				}
				bldr = new SOQLBuilder(objType);
				System.assert(false, 'Non queryable sObject types should not be accepted.');
			}
		} catch (Exception e) {
			System.assert(True);
		}

		Test.stopTest();
	}

	@IsTest
	static void testAddRemoveSelectColumns() {

		Test.startTest();

		SOQLBuilder bldr = new SOQLBuilder(Account.sObjectType);
		bldr.selectColumn('Id');
		bldr.selectColumn('Name');
		bldr.selectColumns(new String[] {'BillingStreet', 'IsDeleted'});

		// null in select columns is acceptable argument. Nothing should happen then
		bldr.selectColumns(null);

		System.assertEquals(4, bldr.selectColumns.size(), 'Not all columns were added to select clause columns');
		System.assert(bldr.selectColumns.contains('IsDeleted'.toLowerCase()), '"IsDeleted" column was not added to select clause');
		System.assert(bldr.selectColumns.contains('BillingStreet'.toLowerCase()), '"BillingStreet" column was not added to  select clause');
		System.assert(bldr.selectColumns.contains('Name'.toLowerCase()), '"Name" column was not added to select clause');
		System.assert(bldr.selectColumns.contains('Id'.toLowerCase()), '"Id" column was not removed added to  select clause');

		// check single non existing addition
		try {
			bldr.selectColumn('TestNonExistingColumn______cc');
			System.assert(false, 'Added non existing Account column to select clause');
		} catch (Exception e) {
			System.assert(true);
		}

		// check multiple non existing or at least one
		try {
			bldr.selectColumns(new String[] {'TestNonExistingColumn______cc', 'AnotherNonExisting__cc', 'Active__c'});
			System.assert(false, 'Added non existing Account column to select clause');
		} catch (Exception e) {
			System.assert(true);
		}

		// Remove existing column
		bldr.removeSelectColumn('isdeleted');
		System.assert(!bldr.selectColumns.contains('isdeleted'), '"IsDeleted" column was not removed from select clause');
		System.assertEquals(3, bldr.selectColumns.size(), 'Column was not removed from select clause');

		// Falsify remove column
		bldr.removeSelectColumn('TestColumn');
		System.assertEquals(3, bldr.selectColumns.size(), 'Non existing selectColumn was removed from select clause');

		// Remove multiple columns
		bldr.removeSelectColumns(new String[] {'Name', 'BillingStreet'});
		System.assertEquals(1, bldr.selectColumns.size(), 'Not all requested columns were removed from select clause');
		System.assert(!bldr.selectColumns.contains('Name'), '"Name" column was not removed from select clause');
		System.assert(!bldr.selectColumns.contains('BillingStreet'), '"BillingStreet" column was not removed from select clause');

		bldr.clearSelectClause();
		System.assertEquals(0, bldr.selectColumns.size(), 'Select clause columns not cleared after clear.');

		Test.stopTest();
	}

	@IsTest
	static void testOrderByCreation() {

		SOQLBuilder bldr = new SOQLBuilder(Account.sObjectType);

		bldr.addSortExpression('name');
		bldr.addSortExpression('industry', SOQLBuilder.SortOrder.SORT_ASC);
		bldr.addSortExpression('BillingCity', SOQLBuilder.SortOrder.SORT_DESC);
		bldr.addSortExpression('accountNumber', SOQLBuilder.SortOrder.SORT_DESC, SOQLBuilder.NullSortOrder.NULLS_FIRST);
		bldr.addSortExpression('AnnualRevenue', SOQLBuilder.SortOrder.SORT_ASC, SOQLBuilder.NullSortOrder.NULLS_LAST);
		bldr.addSortExpression('BillingCity', SOQLBuilder.SortOrder.SORT_DEFAULT, SOQLBuilder.NullSortOrder.NULLS_LAST);

		Test.startTest();

		String[] orderByTokens = bldr.buildOrderByClause().split(',');

		bldr.clearOrderByColumns();
		System.assert(bldr.buildOrderByClause() == null, 'Build order by clause should be null for no order by column defined.');

		Test.stopTest();

		System.assert(orderByTokens[0].trim().toLowerCase() == 'name', 'Default order by sort entity defined wrong');
		System.assert(orderByTokens[1].trim().toLowerCase() == 'industry asc', 'Default asc sort defined wrong');
		System.assert(orderByTokens[2].trim().toLowerCase() == 'billingcity desc', 'Default desc sort defined wrong');
		System.assert(orderByTokens[3].trim().toLowerCase() == 'accountnumber desc nulls first', 'DESC and NULLS_FIRST combination defined wrong soql statement');
		System.assert(orderByTokens[4].trim().toLowerCase() == 'annualrevenue asc nulls last', 'DESC and NULLS_FIRST combination defined wrong soql statement');
		System.assert(orderByTokens[5].trim().toLowerCase() == 'billingcity nulls last', 'DEFAULT and NULLS_FIRST combination defined wrong soql statement');
	}

	@IsTest
	static void testLimitClause() {

		SOQLBuilder bldr = new SOQLBuilder(Account.sObjectType);

		Test.startTest();

		// you cannot define a limit less than 0
		try {
			bldr.fetchLimit = -1;
			System.assert(false, 'Limit cannot be set to less than zero.');
		} catch (Exception e) {
			System.assert(true);
		}

		bldr.fetchLimit = 100;
		System.assertEquals('limit 100', bldr.createLimitStatement().trim(), 'Limit statement not created well');

		// null fetch limit should not generate any limit clause
		bldr.fetchLimit = null;
		System.assert(String.isEmpty(bldr.createLimitStatement()), 'Limit statement should be empty if no fetch limit defined.');

		Test.stopTest();
	}

	@IsTest
	static void testOffsetClause() {

		SOQLBuilder bldr = new SOQLBuilder(Account.sObjectType);

		Test.startTest();

		// you cannot define offset less than 0
		try {
			bldr.offset = -1;
			System.assert(false, 'Offset cannot be set to less than zero.');
		} catch (Exception e) {
			System.assert(true);
		}

		bldr.offset = 2;
		System.assertEquals('offset 2', bldr.createOffsetStatement().trim(), 'Offset statement not created well');

		// null fetch offset should not generate any offset clause
		bldr.offset = null;
		System.assert(String.isEmpty(bldr.createOffsetStatement()), 'Offset statement should be empty if no offset defined.');

		Test.stopTest();
	}

	@IsTest
	static void testAddRemoveWhereClause() {

		SOQLBuilder bldr = new SOQLBuilder(Account.sObjectType);

		Test.startTest();

		// Where clause with identifier
		SOQLBuilder.WhereClauseExpression simpleWhereClauseExpression = bldr.addWhereClauseExpression('ID1', '1 = 1');
		System.assertEquals(
			'ID1',
			simpleWhereClauseExpression.identifier,
			'After where clause expression is created, its entity doesn\'t have right identifier'
		);
		System.assertEquals(
			'1 = 1',
			simpleWhereClauseExpression.expression,
			'After where clause expression is created, its entity doesn\'t have right where clause SOQL');

		System.assertEquals(1,
			bldr.whereClauseExpressions.size(),
			'Where clause expression not added to where clause entity collection'
		);
		System.assertEquals(
			0,
			bldr.whereClausesWithoutIdentifier.size(),
			'Where clause expression with identifier should not be added to where clause without identifier entity collection'
		);

		// Where clause without identifier
		SOQLBuilder.WhereClauseExpression simpleNullIdWhereClauseExpression = bldr.addWhereClauseExpression(null, '1 = 1');
		System.assert(
			simpleNullIdWhereClauseExpression.identifier == null ,
			'After where clause expression is created, its entity doesn\'t have right identifier'
		);
		System.assertEquals(
			1,
			bldr.whereClauseExpressions.size(),
			'Where clause expression without id shouldn\'ve been added to entity with identifiers collection.'
		);
		System.assertEquals(
			1,
			bldr.whereClausesWithoutIdentifier.size(),
			'Where clause expression without identifier should be added to where clause without identifier entity collection'
		);

		// Default add where clause with AND
		SOQLBuilder.WhereClauseExpression defNullIdWhereClauseExpression = bldr.addWhereClauseExpression('1 = 1');
		System.assert(
			defNullIdWhereClauseExpression.identifier == null ,
			'After where clause expression is created, its entity doesn\'t have right identifier'
		);
		System.assertEquals(
			'1 = 1',
			defNullIdWhereClauseExpression.expression,
			'After where clause expression is createt, its entity doesn\'t have right where clause SOQL'
		);
		System.assertEquals(
			1,
			bldr.whereClauseExpressions.size(),
			'Where clause expression without id shouldn\'ve been added to entity with identifiers collection.'
		);
		System.assertEquals(
			2,
			bldr.whereClausesWithoutIdentifier.size(),
			'Where clause expression without identifier should be added to where clause without identifier entity collection'
		);

		// Remove without identifier
		bldr.removeWhereClauseExpression(defNullIdWhereClauseExpression);
		System.assertEquals(
			1,
			bldr.whereClauseExpressions.size(),
			'Where clause expression without identifier removed from identifiers collection.'
		);
		System.assertEquals(
			1,
			bldr.whereClausesWithoutIdentifier.size(),
			'Where clause expression without identifier should\'ve been moved from where clause without identifier entity collection'
		);

		// Falsify remove object
		bldr.removeWhereClauseExpression(new SOQLBuilder.WhereClauseExpression('ID1', '1 = 2'));
		System.assertEquals(
			1,
			bldr.whereClauseExpressions.size(),
			'Falsified where clause removed some other object from entity with identifier collection.'
		);
		System.assertEquals(
			1,
			bldr.whereClausesWithoutIdentifier.size(),
			'Falsified where clause removed some other object from entity without identifier collection.'
		);

		// Remove null
		bldr.removeWhereClauseExpression(null);
		System.assertEquals(
			1,
			bldr.whereClauseExpressions.size(),
			'Null where clause removed some other object from entity with identifier collection.'
		);
		System.assertEquals(
			1,
			bldr.whereClausesWithoutIdentifier.size(),
			'Null where clause removed some other object from entity without identifier collection.'
		);

		// Remove where clause with id
		bldr.removeWhereClauseExpression(simpleWhereClauseExpression);
		System.assertEquals(
			0,
			bldr.whereClauseExpressions.size(),
			'Where clause expression with identifier was not removed from identifiers collection.'
		);
		System.assertEquals(
			1,
			bldr.whereClausesWithoutIdentifier.size(),
			'Where clause expression with identifier was not removed from entities without identifiers collection.'
		);

		Test.stopTest();
	}

	@IsTest
	static void testCreateWhereClause() {

		SOQLBuilder bldr = new SOQLBuilder(Account.sObjectType);

		Test.startTest();

		SOQLBuilder.WhereClauseExpression id1 = bldr.addWhereClauseExpression('ID1', '1 = 1');
		SOQLBuilder.WhereClauseExpression id2 = bldr.addWhereClauseExpression('ID2', 'name = null');
		SOQLBuilder.WhereClauseExpression id3 = bldr.addWhereClauseExpression('ID3', 'idustry = null');
		SOQLBuilder.WhereClauseExpression nonId = bldr.addWhereClauseExpression('active__c = \'Yes\'');

		bldr.whereClauseLogic = 'ID3 and ( ID1 or ID2 )';
		System.assertEquals(
			'((idustry = null) and ( (1 = 1) or (name = null) )) and (active__c = \'yes\')',
			bldr.buildWhereClause().toLowerCase()
		);

		// Without entities without identifier
		bldr.removeWhereClauseExpression(nonId);
		System.assertEquals(
			'(idustry = null) and ( (1 = 1) or (name = null) )',
			bldr.buildWhereClause().toLowerCase()
		);

		// Without where clause logic, all should be appended with and
		bldr.whereClauseLogic = null;
		System.assertEquals(
			'(1 = 1) and (name = null) and (idustry = null)',
			bldr.buildWhereClause().toLowerCase()
		);

		// Spaces should be ignored in where clause logic
		bldr.whereClauseLogic = '   ';
		System.assertEquals(
			'(1 = 1) and (name = null) and (idustry = null)',
			bldr.buildWhereClause().toLowerCase()
		);

		// builder logic changing
		bldr.whereClauseLogic = 'ID3 and ID1 or ID2 or 1 = 2';
		bldr.addWhereClauseExpression(nonId);
		System.assertEquals(
			'((idustry = null) and (1 = 1) or (name = null) or 1 = 2) and (active__c = \'yes\')',
			bldr.buildWhereClause().toLowerCase()
		);

		bldr.whereClauseLogic = 'ID3 or ID1 or ID2 or 1 = 2 - 1';
		System.assertEquals(
			'((idustry = null) or (1 = 1) or (name = null) or 1 = 2 - 1) and (active__c = \'yes\')',
			bldr.buildWhereClause().toLowerCase()
		);

		bldr.clearWhereClause();
		System.assert(
			bldr.buildWhereClause() == null,
			'Without select clause entities, buildWhereClause method should return null'
		);

		Test.stopTest();
	}

	private static void checkValues(SObject referenceObj, SObject objToCheck, List<String> fieldsToCheck) {
		if (referenceObj.getSObjectType() != objToCheck.getSObjectType()) {
			System.assert(false, 'Fetched objects not the same type');
		}

		for (String field : fieldsToCheck) {
			System.assertEquals(
				referenceObj.get(field),
				objToCheck.get(field),
				'Different values for field "' + field +
					'" between apex SOQL fetch and SOQLBuilder query fetch.'
			);
		}
	}
}