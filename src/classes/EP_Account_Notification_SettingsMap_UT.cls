/****************************************************************
* @author       Accenture                                       *
* @name         EP_Account_Notification_SettingsMap_UT          *
* @Created Date 2/1/2018                                        *
* @description                                                  *
****************************************************************/
@isTest
private class EP_Account_Notification_SettingsMap_UT {
    static Account acc;
    static EP_Notification_Account_Settings__c notifyAcc;

    @testSetup
    private static void testData(){
        Folder foldr = EP_AccountNotificationTestDataUtility.getFolder(system.label.EP_Folder_Name);
        EP_AccountNotificationTestDataUtility.insertEmailTemplate(foldr.id);
    }

    private static void data(){
        EmailTemplate templateObj = [select id from EmailTemplate where developerName = 'UserTemplate'];
        acc = EP_TestDataUtility.getSellTo();
        Account shipTo = EP_TestDataUtility.getshipTo();
        Contact con = EP_TestDataUtility.createTestRecordsForContact(acc);
        
        csord__Order__c csOrder = new csord__Order__c(Name='Test Order',csord__Identification__c='XXXXX',csord__Delivered_Date__c=Date.today(),EP_Payment_Term__c='PrePayment',
            EP_Order_Epoch__c='Current',EP_Loading_Date__c = Date.today(),
            EP_Sell_To__c= acc.Id,EP_ShipTo__c = shipTo.Id,EP_Delivery_Type__c='Ex-Rack',
            EP_Expected_Delivery_Date__c = Date.today(),Status__c='Draft');

        insert csOrder;
        system.debug('csOrder+++++++++csOrder'+csOrder);
        EP_Notification_Type__c notifyType = EP_AccountNotificationTestDataUtility.createNotificationType(templateObj.id);
        notifyAcc = EP_AccountNotificationTestDataUtility.createAccountNotification(acc.id,templateObj.id,'mohitg12@yahoo.com',con.id,notifyType.id);
        system.debug('notifyAcc+++++++++notifyAcc'+notifyAcc);
    }

    @isTest static void queryAccountNotificationSettings_Positive() {
        data();
        Test.startTest();
        List<EP_Notification_Account_Settings__c> lsNotifyAcc = EP_Account_Notification_SettingsMapper.queryAccountNotificationSettings(acc.id);
        Test.stopTest();
        system.assert(!lsNotifyAcc.isEmpty());
    }
    
    @isTest static void queryAccountNotificationSettings_Negative() {
        acc = EP_TestDataUtility.getSellTo();
        Test.startTest();
        List<EP_Notification_Account_Settings__c> lsNotifyAcc = EP_Account_Notification_SettingsMapper.queryAccountNotificationSettings(acc.id);
        Test.stopTest();
        system.assert(lsNotifyAcc.isEmpty());
    }
    
    @isTest static void getNameFromId_Positive() {
        data();
        set<Id> templateContactIdSet = new set<Id>{notifyAcc.EP_Notification_Template_Contact__c};
        Test.startTest();
        map<String,String> mapIDName = new map<String,String>();
        mapIDName = EP_Account_Notification_SettingsMapper.getNameFromId(templateContactIdSet,system.label.EP_EmailTemplate,mapIDName);
        Test.stopTest();
        system.assert(!mapIDName.isEmpty());
    }

    @isTest static void getNameFromId_Negative() {
        data();
        set<Id> templateContactIdSet = new set<Id>{notifyAcc.EP_Notification_Template_Contact__c};
        Test.startTest();
        map<String,String> mapIDName = new map<String,String>();
        mapIDName = EP_Account_Notification_SettingsMapper.getNameFromId(templateContactIdSet,system.label.EP_Contact,mapIDName);
        Test.stopTest();
        system.assert(mapIDName.isEmpty());
    }

    @isTest static void getFolderId_Positive() {
        Test.startTest();
        Id foldrId = EP_Account_Notification_SettingsMapper.getFolderId(system.label.EP_Folder_Name);
        Test.stopTest();
        system.assert(foldrId!=null);
    }

    @isTest static void getNotificationSettingByAcc_Positive(){
        data();
        Test.startTest();
        map<Id,List<EP_Notification_Account_Settings__c>> mapObj = EP_Account_Notification_SettingsMapper.getNotificationSettingByAcc(new List<Id>{acc.id});
        Test.stopTest();
        system.assert(!mapObj.isEmpty());
    }

    @isTest static void getNotificationSettingByAcc_Negative(){
        Test.startTest();
        map<Id,List<EP_Notification_Account_Settings__c>> mapObj = EP_Account_Notification_SettingsMapper.getNotificationSettingByAcc(new List<Id>());
        Test.stopTest();
        system.assert(mapObj.isEmpty());
    }

    @isTest static void getNotificationIdByCode_Positive(){
        data();
        set<string> orderIdSet =new set<string>();
        csord__Order__c csOrder = [select id from csord__Order__c limit 1];      
        orderIdSet.add(csOrder.id);
        
        Test.startTest();
        List<EP_Notification_Account_Settings__c> lsNotifyAcc = EP_Account_Notification_SettingsMapper.getNotificationIdByCode('121',acc.Id);
        map<string, list<EP_Notification_Account_Settings__c>> mapNotificationSet = EP_Account_Notification_SettingsMapper.getNotificationSettingsForSellToByOrderNumber(orderIdSet);
        Test.stopTest();
        system.assert(!lsNotifyAcc.isEmpty());
    }

    @isTest static void getNotificationIdByCode_Negative(){
        Id idVal;
        
        Test.startTest();
        EP_Account_Notification_SettingsMapper localObj = new EP_Account_Notification_SettingsMapper();
        List<EP_Notification_Account_Settings__c> lsNotifyAcc = EP_Account_Notification_SettingsMapper.getNotificationIdByCode('121',idVal);       
        Test.stopTest();
        system.assert(lsNotifyAcc.isEmpty());
    }
}