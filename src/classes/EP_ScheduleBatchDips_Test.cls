///* 
//  @Author <Abhay Arora>
//   @name <EP_ScheduleBatchGenarateBlankTankDips>
//   @CreateDate <16/10/2015>
//   @Description <this is the test class for scheduler used for batch scheduling>
//   @Version <1.0>
// 
//*/
//@IsTest 
public with sharing class EP_ScheduleBatchDips_Test{
//
//    private static List<Account> accList;
//    private static Account currentAccount; 
//    private static List<EP_Tank_Dip__c> tankDipList; 
//    private static EP_Tank__c TankInstance; 
// /**************************************************************************
//    *@Description : This method is used to create Data.                                  *
//    *@Params      : none                                                      *
//    *@Return      : void                                                      *    
//    **************************************************************************/    
//    private static void init(){
//     user u =EP_TestDataUtility.createTestRecordsForUser();   
//     accList= EP_TestDataUtility.createTestAccount(new List<Account>{new Account()});
//
//     TankInstance= EP_TestDataUtility.createTestEP_Tank(accList.get(0));
//     Account acc =accList.get(0);
//     acc.EP_Tank_Dip_Entry_Mode__c=label.EP_Portal_Dip_Entry_Mode_Value ;
//     acc.EP_Tank_Dips_Schedule_Time__c=String.valueof(system.now().addMinutes(30).hour())+':'+String.valueof(system.now().addMinutes(30).minute());
//     acc.EP_Status__c =Label.EP_Active_Ship_To_Status_Label;
//     update acc;
//     currentAccount=acc;
//     tankDipList= EP_TestDataUtility.createTestEP_Tank_Dip (new List<EP_Tank_Dip__c>{new EP_Tank_Dip__c(EP_Tank__c=TankInstance.id)});
//
//     }
//    private static testMethod void testSchedule(){ 
//        init();   
//        test.StartTest();
//        String CRON_EXP = '0 0 * * * ?';
//        EP_ScheduleVMINotificationsBatchClass sch = new EP_ScheduleVMINotificationsBatchClass ();
//        system.schedule('Schedule Dips Generation Hourly test', CRON_EXP, sch);
//        test.stopTest();
//     }
//     
//      private static testMethod void testScheduleNot5ification2(){ 
//        init();   
//        currentAccount.EP_Tank_Dips_Schedule_Time__c='12:00';
//        currentAccount.EP_Ship_To_UTC_Offset__c=03.00;
//        update currentAccount;
//        test.StartTest();
//        String CRON_EXP = '0 0 * * * ?';
//        EP_ScheduleVMINotificationsBatchClass sch = new EP_ScheduleVMINotificationsBatchClass ();
//        system.schedule('Schedule Dips Generation Hourly test2', CRON_EXP, sch);
//        test.stopTest();
//     }
//
//    
}