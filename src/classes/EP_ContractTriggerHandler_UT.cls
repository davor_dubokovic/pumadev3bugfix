@IsTest
public class EP_ContractTriggerHandler_UT {

    @testSetup
    private static void testSetup() {
        
        Id purchaseContractRecordTypeId = EP_Common_Util.getRecordTypeIdForGivenSObjectAndName(EP_Common_Constant.EP_CONTRACT_OBJECT_API , EP_Common_Constant.EP_PURCHASE_CONTRACT_RT);

        EP_Payment_Term__c paytermObj = new EP_Payment_Term__c();
        paytermObj.Name = 'Test payment method';
        paytermObj.EP_Payment_Term_Code__c = '001ABC';
        insert paytermObj;
        
        Account sellToAccountObj = EP_TestDataUtility.getSellToPositiveScenario();
        Account vendorAccountObj = EP_TestDataUtility.getVendorPositiveScenario();
        vendorAccountObj.EP_VendorType__c = EP_Common_Constant.THIRD_PARTY_STCK_SUPPLIER;
        update vendorAccountObj;
        
        Account storageLoc = EP_TestDataUtility.getStorageLocationPositiveScenario();
        Company__c company = [SELECT Id  FROM Company__c WHERE Id =: sellToAccountObj.EP_Puma_Company__c];
        Contract contractRec = EP_TestDataUtility.createContract(storageLoc.Id,company.id,paytermObj.Id,vendorAccountObj);

        contractRec.RecordTypeId = purchaseContractRecordTypeId;
        contractRec.Status = EP_Common_Constant.ORDER_DRAFT_STATUS;
        insert contractRec;
        
        contractRec.Status = EP_Common_Constant.EP_CONTRACT_STATUS_ONHOLD;
        update contractRec;
        
        Contract contractRec2 = EP_TestDataUtility.createContract(storageLoc.Id,company.id,paytermObj.Id,vendorAccountObj);

        contractRec2.RecordTypeId = purchaseContractRecordTypeId;
        contractRec2.Status = EP_Common_Constant.ORDER_DRAFT_STATUS;
        
        insert contractRec2;
    }

    @IsTest
    public static void doAfterUpdateTest() {
        
        Map<Id, Contract> mapOldContractIdContract = new Map<Id, Contract>([SELECT Id, Name, RecordTypeId, Status FROM Contract WHERE Status = :EP_Common_Constant.ORDER_DRAFT_STATUS]);
        Map<Id, Contract> mapNewContractIdContract = new Map<Id, Contract>([SELECT Id, Name, RecordTypeId, Status FROM Contract WHERE Status = :EP_Common_Constant.EP_CONTRACT_STATUS_ONHOLD]);
        
        Test.startTest();
        
        EP_ContractTriggerHandler.doAfterUpdate(mapOldContractIdContract, mapNewContractIdContract);
        
        Test.stopTest();
        
        List<Contract> contracts = [SELECT Id, Name, Status FROM Contract];
        
        System.assertEquals(2, contracts.size(), 'Invalid data');
    }
    
    @IsTest
    public static void doAfterUpdateNegativeTest() {
        
        Map<Id, Contract> mapOldContractIdContract = new Map<Id, Contract>();
        Map<Id, Contract> mapNewContractIdContract = new Map<Id, Contract>();
        
        Test.startTest();
        
        EP_ContractTriggerHandler.doAfterUpdate(null, null);
        
        Test.stopTest();
        
        List<EP_Exception_Log__c> exceptions = [SELECT Id FROM EP_Exception_Log__c];
        
        System.assert(exceptions.size() > 0, 'Invalid data');
    }
}