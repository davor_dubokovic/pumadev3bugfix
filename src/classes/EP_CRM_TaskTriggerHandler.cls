/* ================================================
 * @Class Name : EP_CRM_TaskTriggerHandler 
 * @author : Kamendra Singh
 * @Purpose: This Class is EP_CRM_TaskTrigger trigger class and  is used to update Last Activity date on opportunity.
 * @created date: 08/07/2016
 ================================================*/
public with sharing class EP_CRM_TaskTriggerHandler {
    private static set<id>oppIds = new set<id>();
/* ================================================
 * @Method Name : BeforeInsertUpdateTask 
 * @author : Kamendra Singh
 * @Purpose: This method is used to update Last Activity date on opportunity on before insert and update Activity.
 * @created date: 08/07/2016
 ================================================*/
    public static void beforeInsertUpdateTask(List<Task> TriggerNew){
        try{
            for(Task objTask : TriggerNew){
            if(objTask.whatid != null){
                string oppId = objTask.whatid;
                    if(oppId.startsWith(Label.EP_CRM_Oppty_Prefix)){
                        oppIds.add(objTask.whatid);
                    }                 
                }
            }
            list<Opportunity> lstopp; 
            if(!oppIds.isEmpty()){
                lstopp= [select id,EP_CRM_Last_Activity_Date__c from Opportunity where id IN : oppIds limit 50000];
            }
            if(lstopp != null && !lstopp.isEmpty()){
                for(Opportunity objOpp : lstopp){
                    objOpp.EP_CRM_Last_Activity_Date__c  = system.now();
                }
                Database.SaveResult[] SR = Database.update(lstopp);
            }
        }
        catch(exception ex){ex.getmessage();}
    }
/* ================================================
 * @Method Name : BeforeDeleteEvent 
 * @author : Kamendra Singh
 * @Purpose: This method is used to update Last Activity date on opportunity on before delete Activity.
 * @created date: 08/07/2016
 ================================================*/
    public static void beforeDeleteTask(List<Task> TriggerOld){
        try{
            list<Opportunity> updateOpplst = new list<Opportunity>();
            for(Task objTask : TriggerOld){
                if(objTask.whatid != null){
                string oppId = objTask.whatid;
                    if(oppId.startsWith(Label.EP_CRM_Oppty_Prefix)){
                         oppIds.add(objTask.whatid);
                    }
                }
            }
            if(!oppIds.isEmpty()){
                for(Opportunity objOpp : [select id,EP_CRM_Last_Activity_Date__c,(select id,LastModifiedDate from tasks where Id NOT IN: Trigger.old order by LastModifiedDate DESC limit 50000),(select id,LastModifiedDate from Events order by LastModifiedDate DESC limit 50000) from Opportunity where Id IN : oppIds limit 50000]){
                    List<Task> lstTask = objOpp.Tasks;
                    if(lstTask.size()>=1){                  
                        objOpp.EP_CRM_Last_Activity_Date__c = lstTask[0].LastModifiedDate;
                        updateOpplst.add(objOpp);
                    }
                    if(objOpp.Tasks.size() == 0 && objOpp.Events.size() == 0){
                        objOpp.EP_CRM_Last_Activity_Date__c = null;
                        updateOpplst.add(objOpp);
                    }
                }
            }
            if(updateOpplst != null && !updateOpplst.IsEmpty()){
                Database.SaveResult[] updateOpp = Database.update(updateOpplst);
            }
        }
        catch(exception ex){
            ex.getmessage();
        }
    } 
    //Defect #45313 - Start
    /* ================================================
    * @Method Name : modifyTaskPermissionCheck 
    * @author : Seema Bisht
    * @Purpose: Defect #45313 - Admin/CSC/SM/TM user will only be able to edit/delete the Credit Limit Task
    * @created date: 08/06/2017
    ================================================*/
    public static void modifyCreditLimitTaskPermissionCheck(List<Task> taskList){
        Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        for(Task taskIns : taskList){
            if(isSubjecthasCreditLimitCheck(taskIns.Subject) && isProfileHasTaskModificationCheck(profileName)){
                            taskIns.addError(system.Label.EP_Task_Modification_Error);
            }        
        }
    }
    public static boolean isSubjecthasCreditLimitCheck(String subject){
       return ((!String.isBlank(subject)) && (subject.contains(EP_Common_Constant.CREDIT_LIMIT_APPROVED_SUBJECT)|| 
                subject.contains(EP_Common_Constant.CREDIT_LIMIT_REJECTED_SUBJECT))); 
    }
    public static boolean isProfileHasTaskModificationCheck(String profileName){
        return (!(profileName.equalsIgnoreCase(EP_Common_Constant.CSC_AGENT_PROFILE) || profileName.equalsIgnoreCase(EP_Common_Constant.CSC_MANAGER_PROFILE) ||
                        profileName.equalsIgnoreCase(EP_Common_Constant.SM_AGENT_PROFILE) || ProfileName.equalsIgnoreCase(EP_Common_Constant.TM_AGENT_PROFILE) || 
                        profileName.equalsIgnoreCase(EP_Common_Constant.ADMIN_PROFILE)));
    }  
    //Defect #45313 - End*/
}