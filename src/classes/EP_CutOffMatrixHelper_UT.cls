@isTest
private class EP_CutOffMatrixHelper_UT {
    
    @isTest static void getDayName_test() {
        EP_CutOffMatrixHelper cmh = new EP_CutOffMatrixHelper();

        System.AssertEquals('Monday',cmh.getDayName(1));
        System.AssertEquals('Tuesday',cmh.getDayName(2));
        System.AssertEquals('Wednesday',cmh.getDayName(3));
        System.AssertEquals('Thursday',cmh.getDayName(4));
        System.AssertEquals('Friday',cmh.getDayName(5));
        System.AssertEquals('Saturday',cmh.getDayName(6));
        System.AssertEquals('Sunday',cmh.getDayName(7));
        System.AssertEquals('Monday',cmh.getDayName(8));
    }
    
    @isTest static void getTodayNum_test() {
        EP_CutOffMatrixHelper cmh = new EP_CutOffMatrixHelper();

        System.AssertEquals(1,cmh.getTodayNum('Monday'));
        System.AssertEquals(2,cmh.getTodayNum('Tuesday'));
        System.AssertEquals(3,cmh.getTodayNum('Wednesday'));
        System.AssertEquals(4,cmh.getTodayNum('Thursday'));
        System.AssertEquals(5,cmh.getTodayNum('Friday'));
        System.AssertEquals(6,cmh.getTodayNum('Saturday'));
        System.AssertEquals(7,cmh.getTodayNum('Sunday'));
        System.AssertEquals(0,cmh.getTodayNum('MMMonday'));
    }

    @isTest static void getCutoffTime_test() {
        EP_CutOffMatrixHelper cmh = new EP_CutOffMatrixHelper();

        DeliveryDay__c dDay = new DeliveryDay__c(Monday__c = Time.newInstance(1, 0, 0, 0),
                                                 Tuesday__c = Time.newInstance(2, 0, 0, 0),
                                                 Wednesday__c = Time.newInstance(3, 0, 0, 0),
                                                 Thursday__c = Time.newInstance(4, 0, 0, 0),
                                                 Friday__c = Time.newInstance(5, 0, 0, 0),
                                                 Saturday__c = Time.newInstance(6, 0, 0, 0),
                                                 Sunday__c = Time.newInstance(7, 0, 0, 0));

        System.AssertEquals(1,cmh.getCutoffTime('Monday', dDay).hour());
        System.AssertEquals(2,cmh.getCutoffTime('Tuesday', dDay).hour());
        System.AssertEquals(3,cmh.getCutoffTime('Wednesday', dDay).hour());
        System.AssertEquals(4,cmh.getCutoffTime('Thursday', dDay).hour());
        System.AssertEquals(5,cmh.getCutoffTime('Friday', dDay).hour());
        System.AssertEquals(6,cmh.getCutoffTime('Saturday', dDay).hour());
        System.AssertEquals(7,cmh.getCutoffTime('Sunday', dDay).hour());
        System.AssertEquals(1,cmh.getCutoffTime('SSSunday', dDay).hour());
    }


    @isTest static void getDeliveryDaysByTerminalAndDeliveryType_test() {
        EP_CutOffMatrixHelper cmh = new EP_CutOffMatrixHelper();

        cmh.cutoffMatrix = new CutOffMatrix__c();
        cmh.deliveryDayList = new List<DeliveryDay__c>();

        for (Integer i = 1; i < 8; i++) {
            cmh.deliveryDayList.add(new DeliveryDay__c(Order_Day_Name__c = cmh.getDayName(i), OrderDayNum__c = i));
        }

        List<DeliveryDay__c> ddList = cmh.getDeliveryDaysByTerminalAndDeliveryType();

        Integer j = 0;
        for (DeliveryDay__c dd : ddList) {
            DeliveryDay__c dd_res = cmh.deliveryDayList.get(j);
            System.AssertEquals(dd_res.Order_Day_Name__c,dd.Order_Day_Name__c);
            System.AssertEquals(dd_res.OrderDayNum__c,dd.OrderDayNum__c);
            j++;
        }
    }


    @isTest static void createDeliveryDaysList_test() {
        EP_CutOffMatrixHelper cmh = new EP_CutOffMatrixHelper();

        cmh.cutoffMatrix = new CutOffMatrix__c();
        cmh.deliveryDayList = new List<DeliveryDay__c>();

        for (Integer i = 1; i < 8; i++) {
            cmh.deliveryDayList.add(new DeliveryDay__c(Order_Day_Name__c = cmh.getDayName(i), OrderDayNum__c = i));
        }

        List<DeliveryDay__c> ddList = cmh.createDeliveryDaysList();

        Integer j = 0;
        for (DeliveryDay__c dd : ddList) {
            DeliveryDay__c dd_res = cmh.deliveryDayList.get(j);
            System.AssertEquals(dd_res.Order_Day_Name__c,dd.Order_Day_Name__c);
            System.AssertEquals(dd_res.OrderDayNum__c,dd.OrderDayNum__c);
            j++;
        }
        cmh.deliveryDays  = ddList;
        cmh.fullDeliveryDayList  = ddList;
    }

    @isTest static void getDayOfTheWeek_test() {
        EP_CutOffMatrixHelper cmh = new EP_CutOffMatrixHelper();

        DateTime d = DateTime.newInstance(2018, 5, 20);

        //MM/DD/YYY
        System.AssertEquals(d.format('EEEE'),cmh.getDayOfTheWeek('05/20/2018'));
    }

    @isTest static void checkCutOffTimeOk_test() {
        EP_CutOffMatrixHelper cmh = new EP_CutOffMatrixHelper();

        cmh.cutoffMatrix = new CutOffMatrix__c(Time_Zone__c = 'GMT');

        Time cutOffT = Time.newInstance(20, 0, 0, 0);
        String localTime = '19:00:00';
        String localDate = '02/28/2018';
        String localGmtOffset = '0';
        System.AssertEquals(true, cmh.checkCutOffTimeOk(cutOffT, localTime, localDate, localGmtOffset));

        localGmtOffset = '-120';
        System.AssertEquals(false, cmh.checkCutOffTimeOk(cutOffT, localTime, localDate, localGmtOffset));
    }
    
    @isTest static void EP_CutOffMatrixHelper_test() {
        
        CS_ORDER_SETTINGS__c  csOrderSetting = new CS_ORDER_SETTINGS__c();
        csOrderSetting.CutoffMatrixDefaultDepth__c = 5;
        insert csOrderSetting;
        
        Company__c testCompany = new Company__c();
        testCompany.Name = 'testCompany';
        testCompany.EP_Combined_Invoicing__c = 'Delegate to Customer';
        testCompany.EP_Company_Id__c = 1234;
        testCompany.EP_Company_Code__c = 'Test code';
        testCompany.EP_Window_Start_Hours__c = 8;
        testCompany.KYC_limit_in_KUSD__c = '0';
        insert testCompany;
        System.Assert(testCompany.Id != null);
    
        Account acc = new Account();
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Storage Location').getRecordTypeId();
        acc.Name='testEPAccount';
        acc.EP_Status__c = '06-Blocked';
        acc.EP_Reason_Blocked__c = 'Maintenance';
        acc.EP_Puma_Company__c = testCompany.Id;
        acc.EP_Advance_Order_Entry_Days__c = 5;
        insert acc;
        System.Assert(acc.Id != null);
        
        CutOffMatrix__c cutofMatrix = new CutOffMatrix__c();
        cutofMatrix.Site_Location__c = acc.Id;
        cutofMatrix.Puma_Company__c = testCompany.Id;
        cutofMatrix.Delivery_Type__c = 'Delivery';
        cutofMatrix.Shift__c = 'AM';
        cutofMatrix.Time_Zone__c = 'GMT + 5:30';
        insert cutofMatrix;
        System.Assert(cutofMatrix.Id != null);
        
        
        testCompany.Cut_Off_Matrix__c = cutofMatrix.Id;
        update testCompany;
        
        Id recordTypeVendor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();

        Account testTransporterAccount = new Account();
        testTransporterAccount.Name = 'testTransporterAccount';
        testTransporterAccount.RecordTypeId = recordTypeVendor;
        testTransporterAccount.EP_Status__c = '05-Active';
        testTransporterAccount.EP_VendorType__c = 'Transporter';
        INSERT testTransporterAccount;

        EP_Stock_Holding_Location__c testHoldingLocation = new EP_Stock_Holding_Location__c();
        testHoldingLocation.EP_Transporter__c = testTransporterAccount.Id;
        INSERT testHoldingLocation;
        System.Assert(testHoldingLocation.Id != null);
        
        Test.startTest();
        EP_CutOffMatrixHelper matrixHelperwithAcount = new EP_CutOffMatrixHelper(acc.Id);
        EP_CutOffMatrixHelper matrixHelperwithAcountandCompany = new EP_CutOffMatrixHelper(acc.Id,testCompany.Id, testHoldingLocation.Id,'Delivery');
        matrixHelperwithAcount.initializeCutoffMatrixHelper(cutofMatrix.Id);        
        matrixHelperwithAcountandCompany.getCutOffTimeByAccountId(acc.Id,'02/28/2004','23:45:26','02/28/2004','GMT + 5:30');
        
        Overload_matrix__c overloadMatrix = new Overload_matrix__c();
        overloadMatrix.Name = 'Test';
        insert overloadMatrix;
        
        Overload_matrix_entry__c overloadMatrixEntry = new Overload_matrix_entry__c();
        overloadMatrixEntry.Overload_matrix__c = overloadMatrix.Id;
        overloadMatrixEntry.Order_Date__c = System.Today();
        overloadMatrixEntry.Date__c = System.Today();
        insert overloadMatrixEntry;
        
        List<CutOffMatrix__c> cutofMatics = new  List<CutOffMatrix__c>{cutofMatrix};
        matrixHelperwithAcount.cutoffMatrices = cutofMatics;
        
        System.Assert(overloadMatrix.Id != null);
        matrixHelperwithAcount.getOverrideMatrixEntries(overloadMatrix.Id);     
        matrixHelperwithAcount.getOverloadCutOffTime(acc.Id,'02/28/2004','02/28/2004'); 
        matrixHelperwithAcount.initializeCutoffMatrixHelper(null);
        Test.stopTest();
        
    }
    
}