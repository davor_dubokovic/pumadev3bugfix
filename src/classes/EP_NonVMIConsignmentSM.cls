/*   
     @Author Aravindhan Ramalingam
     @name <EP_NonVMIConsignmentSM.cls>     
     @Description <Non VMI Consignment Statemachine >   
     @Version <1.1> 
     */

     public class EP_NonVMIConsignmentSM extends EP_OrderStateMachine {
     	
     	public EP_NonVMIConsignmentSM(){

     	}
     	
     	public override EP_OrderState getOrderState(EP_OrderEvent currentEvent){
     		EP_GeneralUtility.Log('Public','EP_NonVMIConsignmentSM','getOrderState');
     		return super.getOrderState(currentEvent);
     	}
     }