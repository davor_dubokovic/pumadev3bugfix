/* 
* @Author <Jai Singh>
* @name <EP_ProductSyncNAVtoSFDC>
* @CreateDate <20/04/2016>
* @Description <This is apex RESTful WebService for Product Sync from NAV to SFDC> 
* @Version <1.0>
*/
@RestResource(urlMapping='/v1/ProductSync/*')
global without sharing class EP_ProductSyncNAVtoSFDC{
	
	/**
	* @author <Jai Singh>
	* @description <This is the method that handles HttpPost request for product sync>
	* @name <productSync>
	* @date <20/04/2016>
	* @param none
	* @return void
	*/
	@HttpPost
	global static void productSync(){
		RestRequest request = RestContext.request;
		String requestBody = request.requestBody.toString();
		String resultString = EP_ProductSyncHandler.createUpdateProduct(requestBody); 
		RestContext.response.responseBody = Blob.valueOf(resultString); 
		RestContext.response.addHeader(EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON);
	}
}