/* 
   @Author      Accenture
   @name        EP_AccountDomainObject
   @CreateDate  02/05/2017
   @Description This is the domain class for Account Object
   @NovaSuite Fix -- comments added
   @Version     1.1
*/
 public class EP_AccountDomainObject{

    public Account localAccount;
    public Account oldAccount;  
    //Bulkification Implementation -- Start
    public EP_AccountMapper accountMapper = new EP_AccountMapper();
    public List<EP_State_Transitions__c> listStateTrans = new List<EP_State_Transitions__c>();
    public EP_ActionMapper actionMapper = new EP_ActionMapper();
    public EP_StockHoldingLocationMapper stockHoldLocMapper = new EP_StockHoldingLocationMapper();
    //Bulkification Implementation -- End
    public Boolean isSellToBlocked;
    public Boolean isBillToBlocked;
    EP_AccountService service; 
    public string toStatus;
 
   /**
    * @author       Accenture
    * @name         EP_AccountDomainObject
    * @date         02/05/2017
    * @description  Constructor for setting new account and old account objects which will be passed by the trigger
    * @param        Account,Account
    * @return       NA
    */  
    public EP_AccountDomainObject(Account newAccount, Account oldAccount){
        this.localAccount = newAccount;
        this.oldAccount = oldAccount;
        this.service = new EP_AccountService(this);
        this.toStatus = newAccount.EP_Status__c;   
    }

   /**
    * @author       Accenture
    * @name         EP_AccountDomainObject
    * @date         02/05/2017
    * @description  Constructor for getting account object when an Id is passes to it
    * @param        Id
    * @return       NA
    */
    public EP_AccountDomainObject(Id AccountID){
        Account acc = accountMapper.getAccountRecord(AccountID);
        localAccount = acc;
        this.service = new EP_AccountService(this);
        this.toStatus = acc.EP_Status__c;       
    }


   /**
    * @author       Accenture
    * @name         EP_AccountDomainObject
    * @date         02/05/2017
    * @description  Constructor for setting account object 
    * @param        Account
    * @return       NA
    */
    public EP_AccountDomainObject(Account account){
        localAccount = account;
        this.toStatus = localAccount.EP_Status__c;
        this.service = new EP_AccountService(this);       
    }
    


    /**
    * @author       Accenture
    * @name         getStatus
    * @date         02/05/2017
    * @description  This method is used to get Account status 
    * @param        NA
    * @return       string
    */
    public String getStatus(){
        EP_GeneralUtility.Log('Public','EP_AccountDomainObject','getStatus');
        return localAccount.EP_Status__c;
    }
    

    /**
    * @author       Accenture
    * @name         setStatus
    * @date         02/05/2017
    * @description  This method is used to set Account status 
    * @param        string
    * @return       NA
    */
    public void setStatus(String accountStatus){
        EP_GeneralUtility.Log('Public','EP_AccountDomainObject','setStatus');
        localAccount.EP_Status__c = accountStatus;
    }
    

    /**
    * @author       Accenture
    * @name         getOldRecord
    * @date         02/05/2017
    * @description  This method is used to get old Account record object 
    * @param        NA
    * @return       Account
    */
    public Account getOldRecord() {
        EP_GeneralUtility.Log('Public','EP_AccountDomainObject','getOldRecord');
        return oldAccount;
    }
    

    /**
    * @author       Accenture
    * @name         getAccount
    * @date         02/05/2017
    * @description  This method is used to get current account object
    * @param        NA
    * @return       Account
    */
    public Account getAccount(){
        EP_GeneralUtility.Log('Public','EP_AccountDomainObject','getAccount');
        return localaccount; 
    } 
    
 
    /**
    * @author       Accenture
    * @name         getEventName
    * @date         02/05/2017
    * @description  This method is used to get the event name that will be used in Account State machine to get respective transition class
    * @param        NA
    * @return       string
    */
    @testVisible
    private string getEventName(){
        EP_GeneralUtility.Log('Private','EP_AccountDomainObject','getEventName');
        string event;
        //L4_45352_START
        //if(localAccount.EP_Integration_Status__c != oldAccount.EP_Integration_Status__c && localAccount.EP_SENT_TO_NAV_WINDMS__c && EP_AccountConstant.ACCOUNTSETUP.equalsIgnoreCase(localAccount.EP_Status__c)){
        if(isAccountSetupToActiveEvent()){
        //L4_45352_END
            event = EP_AccountConstant.SETUP_TO_ACTIVE;// '04-AccountSet-upTo05-Active';            
        }/*BUG-FIX-80805-START
         else if(isAccountBasicSetUpToActiveEvent()){
            event = EP_AccountConstant.DUMMY_BASICDATASETUP_TO_ACTIVE;

        }BUG-FIX-80805-END*/
        //L4_45352_Start
        else if(isAccountIsBasicSetUpEvent() && isCustomerActivationEvent()){
            event = EP_AccountConstant.DUMMY_BASICDATASETUP_TO_ACTIVE;
        }
        //L4_45352_End
        else{
            event = getActualEvent(); 
        }
        return event; 
    } 
    
    //L4_45352_Start
    /**
    * @author       Accenture
    * @name         isAccountSetupToActiveEvent
    * @date         11/21/2017
    * @description  This method is used to check account active state event
    * @param        NA
    * @return       boolean
    */
     private boolean isAccountSetupToActiveEvent(){
         return (localAccount.EP_Synced_With_All__c && EP_AccountConstant.ACCOUNTSETUP.equalsIgnoreCase(localAccount.EP_Status__c));
     }
    /* @author      Accenture
    * @name         isAccountBasicSetUpToActiveEvent
    * @date         11/21/2017
    * @description  This method is used to check account active state event from basic data set up
    * @param        NA
    * @return       boolean
    */
     /*private boolean isAccountBasicSetUpToActiveEvent(){
         return (EP_AccountConstant.BASICDATASETUP.equalsIgnoreCase(oldAccount.EP_Status__c)&& localAccount.EP_Is_Dummy__c && EP_AccountConstant.ACCOUNTSETUP.equalsIgnoreCase(toStatus));
     }*/
     /* @author      Accenture
    * @name         isAccountIsBasicSetUpEvent
    * @date         11/21/2017
    * @description  This method is basic data set up
    * @param        NA
    * @return       boolean
    */
     private boolean isAccountIsBasicSetUpEvent(){
         return (EP_AccountConstant.BASICDATASETUP.equalsIgnoreCase(localaccount.EP_Status__c));
     }
    //L4_45352_End
    
    /**
    * @author       Accenture
    * @name         getActualEvent
    * @date         03/30/2017
    * @description  This method is used to get the actual event name that will be used in Account State machine to get respective transition class
    * @param        NA
    * @return       string
    */
    @testVisible
    private string getActualEvent(){
        string eventName;
        string newstatus = localAccount.EP_Status__c.replace(EP_Common_Constant.SPACE_STRING, EP_Common_Constant.BLANK);
        string oldstatus = (oldAccount.EP_Status__c!=null)?oldAccount.EP_Status__c.replace(EP_Common_Constant.SPACE_STRING, EP_Common_Constant.BLANK):null;
        eventName = oldstatus + EP_AccountConstant.TO_CONST + newstatus; 
        return eventName;
    }

    //L4_45352_Start
    /**
    * @author       Accenture
    * @name         isCustomerActivationEvent
    * @date         03/30/2017
    * @description  This method to check if customer activation event is possible
    * @param        NA
    * @return       string
    */
    @testVisible
    private boolean isCustomerActivationEvent(){
       return (localAccount.EP_BSM_GM_Review_Completed__c && localAccount.EP_Synced_PE__c && localaccount.EP_Synced_NAV__c && localaccount.EP_Synced_WinDMS__c && EP_Common_Constant.SYNC_STATUS.equalsIgnoreCase(localAccount.EP_Integration_Status__c));
    }   
    //L4_45352_End
    
    /**
    * @author       Accenture
    * @name         getAccountState
    * @date         02/20/2017
    * @description  This method is used to get current account state for the state machine
    * @param        String
    * @return       EP_AccountState
    */ 
    @testVisible
    private EP_AccountState getAccountState(string eventname){
        EP_GeneralUtility.Log('Private','EP_AccountDomainObject','getAccountState');
        EP_AccountEvent accountEvent = new EP_AccountEvent(eventname);
        EP_AccountState ast = EP_AccountTypeStateMachineFactory.getAccountStateMachine(this).getAccountState(accountEvent);
        return ast;
    }


    /**
    * @author       Accenture
    * @name         doActionBeforeInsert
    * @date         02/20/2017
    * @description  This method is to be called thorugh trigger to execute all before insert event logic/actions
    * @param        NA
    * @return       NA
    */ 
    public void doActionBeforeInsert(){
        try{
            EP_GeneralUtility.Log('Public','EP_AccountDomainObject','doActionBeforeInsert');        
            service.doBeforeInsertHandle();
            string eventname = EP_AccountConstant.NEW_RECORD;
            EP_AccountEvent accountEvent = new EP_AccountEvent(eventname);
            doActionInvokeAccountStateMachine(accountEvent);
            if(accountEvent.isError){
                localAccount.addError(accountEvent.getEventMessage(), false);
            }
        }catch(Exception ex){
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'doActionBeforeInsert' , 'EP_AccountDomainObject', ApexPages.Severity.ERROR);
            localAccount.addError(ex.getMessage()+ex.getLineNumber());
        }   
    }
    
    
    /**
    * @author       Accenture
    * @name         doActionBeforeUpdate
    * @date         02/20/2017
    * @description  This method is to be called thorugh trigger to execute all before update event logic/actions
    * @param        NA
    * @return       NA
    */
    public void doActionBeforeUpdate(){
        try{
            EP_GeneralUtility.Log('Public','EP_AccountDomainObject','doActionBeforeUpdate');
            service.doBeforeUpdateHandle();
            string eventname = getEventName();
            EP_AccountEvent accountEvent = new EP_AccountEvent(eventname);
            localAccount.EP_Status__c = oldAccount.EP_Status__c;
            doActionInvokeAccountStateMachine(accountEvent);    
            if(accountEvent.isError){
                localAccount.addError(accountEvent.getEventMessage(), false);
            }
        }catch(Exception ex){
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'doActionBeforeUpdate' , 'EP_AccountDomainObject', ApexPages.Severity.ERROR);
            localAccount.addError(ex.getMessage());
        }          
    }
    
    
    /**
    * @author       Accenture
    * @name         doActionInvokeAccountStateMachine
    * @date         02/20/2017
    * @description  Method to invoke Account State Machine depending upon the type of event passed to it.
    * @param        EP_AccountEvent
    * @return       NA
    */
    @testVisible
    private void doActionInvokeAccountStateMachine(EP_AccountEvent accountEvent){
        EP_GeneralUtility.Log('Private','EP_AccountDomainObject','doActionInvokeAccountStateMachine');
        boolean transition = EP_AccountTypeStateMachineFactory.getAccountStateMachine(this).getAccountState(accountEvent).doTransition();
    }
    

    /**
    * @author       Accenture
    * @name         doActionInvokeAccountStateMachine
    * @date         02/20/2017
    * @description  Method to check if Integration status on Account has changed to SYNC
    * @param        NA
    * @return       Boolean
    */  
    public boolean isIntegrationStatusChangedToSync(){
        EP_GeneralUtility.Log('Public','EP_AccountDomainObject','isIntegrationStatusChangedToSync');
        return ((EP_Common_Constant.ACKNWLDGD.equalsIgnoreCase(oldAccount.EP_Integration_Status__c)||EP_Common_Constant.SENT_STATUS.equalsIgnoreCase(oldAccount.EP_Integration_Status__c))&& EP_Common_Constant.SYNC_STATUS.equalsIgnoreCase(localAccount.EP_Integration_Status__c));
    }
    

    /**
    * @author       Accenture
    * @name         doActionAfterInsert
    * @date         02/20/2017
    * @description  This method is to be called thorugh trigger to execute all after insert event logic/actions
    * @param        NA
    * @return       NA
    */      
    public void doActionAfterInsert(){
        EP_GeneralUtility.Log('Public','EP_AccountDomainObject','doActionAfterInsert');
        service.doAfterInsertHandle();
    }
    //# L4- 45526 Start    
    /**
    * @author       Accenture
    * @name         doActionBeforeDelete
    * @date         12/20/2017
    * @description  This method is to be called thorugh trigger to execute all before delete event logic/actions
    * @param        NA
    * @return       NA
    */      
    public void doActionBeforeDelete(){
        EP_GeneralUtility.Log('Public','EP_AccountDomainObject','doActionBeforeDelete');
        service.doBeforeDeleteHandle();
    }
    //# L4- 45526 End  
    
    /**
    * @author       Accenture
    * @name         doActionAfterUpdate
    * @date         02/20/2017
    * @description  This method is to be called thorugh trigger to execute all after update event logic/actions
    * @param        NA
    * @return       string
    */ 
    public void doActionAfterUpdate(){
        EP_GeneralUtility.Log('Public','EP_AccountDomainObject','doActionAfterUpdate');
        //if(localAccount.EP_Status__c != oldAccount.EP_Status__c){            
            string eventname = getEventName();
            EP_AccountState ast = getAccountState(eventname);
            ast.doOnEntry();
       // }
        service.doAfterUpdateHandle();       
    }
    

    /**
    * @author       Accenture
    * @name         getPriceBook
    * @date         02/20/2017
    * @description  Method to get the Product List Id attached to customer
    * @param        NA
    * @return       Id
    */   
    public Id getPriceBook(){
        EP_GeneralUtility.Log('Public','EP_AccountDomainObject','getPriceBook');
        return (localAccount.EP_Has_Bill_To__c == FALSE?localAccount.EP_PriceBook__c:localAccount.EP_Bill_To_Account__r.EP_PriceBook__c);
    } 
    
    
    /**
    * @author       Accenture
    * @name         getBillTo
    * @date         02/20/2017
    * @description  Method to get bill To account associated to a customer account
    * @param        NA
    * @return       Account
    */ 
    public Account getBillTo() {
        EP_GeneralUtility.Log('Public','EP_AccountDomainObject','getBillTo');
        return service.getBillTo();
    }    


    /**
    * @author       Accenture
    * @name         setOwnerAlias 
    * @date         02/20/2017
    * @description  Method to set owner alias value in EP_Cstmr_Owner_Alias__c field on customer. Note: Do not call this method from within the loop
    * @param        NA
    * @return       NA
    */  
    public void setOwnerAlias(){
        EP_GeneralUtility.Log('Public','EP_AccountDomainObject','setOwnerAlias');
        EP_UserMapper userMapperObj = new EP_UserMapper();
        List<User> userList = userMapperObj.getRecordsByIds(new Set<Id>{localAccount.OwnerId});
        if(!userList.isEmpty()){
            localAccount.EP_Cstmr_Owner_Alias__c = userList[0].Alias;
        }
    }
    

    /**
    * @author       Accenture
    * @name         isValidCustomer
    * @date         02/20/2017
    * @description  This method returns true if the current Account is valid customer
    * @param        NA
    * @return       Boolean
    */   
    public Boolean isValidCustomer(){
        EP_GeneralUtility.Log('Public','EP_AccountDomainObject','isValidCustomer');
        Boolean isCustomerValid = false;
        isBillToBlocked = true;
        isSellToBlocked = true;
        if(this.localaccount.EP_Account_Type__c.equalsIgnoreCase(EP_AccountConstant.SELL_TO)){
            isCustomerValid = this.localAccount.EP_IsActive__c;
            isSellToBlocked = !isCustomerValid;
            if(isCustomerValid && this.getBillTo() <> null){
                isCustomerValid = this.getBillTo().EP_Status__c.contains(EP_Common_Constant.STATUS_ACTIVE);
                isBillToBlocked = !isCustomerValid;
            }
        }
        
        return isCustomerValid;
    }
    
    
    /**
    * @author       Accenture
    * @name         isBillToAccountChanged
    * @date         02/20/2017
    * @description  Method to check if Bill To on customer is changed
    * @param        NA
    * @return       Boolean
    */ 
    public boolean isBillToAccountChanged(){
        EP_GeneralUtility.Log('Public','EP_AccountDomainObject','isBillToAccountChanged');
        return (localAccount.EP_Bill_To_Account__c != oldAccount.EP_Bill_To_Account__c);
    }
        
    
    /**
    * @author       Accenture
    * @name         getSupplyLocationPricingList
    * @date         02/20/2017
    * @description  Method to get supply location pricing list for current account
    * @param        NA
    * @return       List<Account>
    */
    public List<Account> getSupplyLocationPricingList(){
        EP_GeneralUtility.Log('Public','EP_AccountDomainObject','getSupplyLocationPricingList');
        return accountMapper.getsupplyLocationPricingList(localAccount.EP_Puma_Company_Code__c);  
    }   
    
    /**
    * @author       Accenture
    * @name         getTransporterPricingList
    * @date         02/20/2017
    * @description  Method to get transporter pricing list for current account
    * @param        NA
    * @return       List<Account>
    */
    public List<Account> getTransporterPricingList(){
        EP_GeneralUtility.Log('Public','EP_AccountDomainObject','getTransporterPricingList');
        return accountMapper.getTransporterPricingList(localAccount.EP_Puma_Company_Code__c);  
    }
    
    /**
    * @author       Accenture
    * @name         isStatusChanged
    * @date         02/20/2017
    * @description  Method to check if account status has changed
    * @param        NA
    * @return       boolean
    */
    public boolean isStatusChanged(){
        EP_GeneralUtility.Log('Public','EP_AccountDomainObject','isStatusChanged');
        return (this.oldAccount != null && this.localAccount.EP_Status__c != this.oldAccount.EP_Status__c);
    }
    
    
    /**
    * @author       Accenture
    * @name         resetStatus
    * @date         02/20/2017
    * @description  Method to reset account status back to its old status
    * @param        NA
    * @return       NA
    */
    public void resetStatus(){
        Account accountObj = accountMapper.getAccountRecord(localAccount.id);
        accountObj.EP_Status__c = oldAccount.EP_Status__c;
        update accountObj;
    }

    //L4_45352_START
    /**
    * @author       Accenture
    * @name         isIntegrationStatusChanged
    * @date         02/20/2017
    * @description  Method to check if Integration status on Account has changed
    * @param        NA
    * @return       Boolean
    */  
    public boolean isIntegrationStatusChanged(){
        EP_GeneralUtility.Log('Public','EP_AccountDomainObject','isIntegrationStatusChanged');
            return (localAccount.EP_Integration_Status__c != oldAccount.EP_Integration_Status__c);
        }
     //L4_45352_END
    //Code changes for L4 #45362 Start
    /*
    *  @Author <Accenture>
    *  @Name isUnblocked
    */
    public boolean isUnblocked() {
        EP_GeneralUtility.Log('Public','EP_AccountDomainObject','isUnblocked');
        return (this.oldAccount != null 
                    && EP_AccountConstant.ACTIVE.equalsignorecase(this.localAccount.EP_Status__c)
                    && EP_AccountConstant.BLOCKED.equalsignorecase(this.oldAccount.EP_Status__c));
    }
    //Code changes for L4 #45362 Start
    /*
    *  @Author <Accenture>
    *  @Name isBlocked
    */
    public boolean isBlocked() {
        EP_GeneralUtility.Log('Public','EP_AccountDomainObject','isBlocked');
        return (this.oldAccount != null 
                    && EP_AccountConstant.BLOCKED.equalsignorecase(this.localAccount.EP_Status__c)
                    && EP_AccountConstant.ACTIVE.equalsignorecase(this.oldAccount.EP_Status__c));
    }
    //Code changes for L4 #45362 End

    // Changes made for CUSTOMER MODIFICATION L4 Start
    /*
    *  @Author <Accenture>
    *  @Name isUpdatedFromNAV
    */
    public boolean isUpdatedFromNAV(){
        EP_GeneralUtility.Log('Public','EP_AccountDomainObject','isUpdatedFromNAV');

        return (this.oldAccount != null && this.localAccount.EP_Source_SEQ_ID__c != this.oldAccount.EP_Source_SEQ_ID__c);
    }
    /*
    *  @Author <Accenture>
    *  @Name isChangeIntegrationStatus
    */
    public boolean isChangeIntegrationStatus(){
        EP_GeneralUtility.Log('Public','EP_AccountDomainObject','isChangeIntegrationStatus');

        return (this.oldAccount != null && this.localAccount.EP_Integration_Status__c != this.oldAccount.EP_Integration_Status__c);
    }
    // Changes made for CUSTOMER MODIFICATION L4 End
    
    public Decimal getAvailableFunds(){
        EP_GeneralUtility.Log('Public','EP_AccountDomainObject','getAvailableFunds');
        return 0.0;
    }
}