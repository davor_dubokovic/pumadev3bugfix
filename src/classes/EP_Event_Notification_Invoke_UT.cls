/****************************************************************
* @author       Accenture                                       *
* @name         EP_Event_Notification_Invoke_UT                 *
* @Created Date 2/1/2018                                        *
* @description                                                  *
****************************************************************/
@isTest
public with sharing class EP_Event_Notification_Invoke_UT {
	static Attachment att;
	static Account acc;
	static Contact con;

	@testSetup
	static void setupData(){
		EP_AccountNotificationTestDataUtility.insertEmailTemplate(EP_AccountNotificationTestDataUtility.getFolder(system.label.EP_Folder_Name).id);
	}

	static void data(){
		acc = EP_TestDataUtility.getSellTo();
		con = EP_TestDataUtility.createTestRecordsForContact(acc);
		att = EP_AccountNotificationTestDataUtility.createAttachment(acc.id);

	}


	@isTest static void invokeNotificationToUser_Positive(){
		data();
		EmailTemplate templateObj = [select id from EmailTemplate where developerName = 'UserTemplate'];
		EP_Event_Notification_Invoke eventInvokeObj = new EP_Event_Notification_Invoke();
		List<EP_Event_Notification_Invoke.subscriptionInvokation> subscriptionInvokationList = new List<EP_Event_Notification_Invoke.subscriptionInvokation>();
		EP_Event_Notification_Invoke.subscriptionInvokation subscriptionInvokationObj = new EP_Event_Notification_Invoke.subscriptionInvokation();
		subscriptionInvokationObj.attachmentId = att.id;
		subscriptionInvokationObj.contacttemplateid = templateObj.id;
		subscriptionInvokationObj.contactId = con.id;
		subscriptionInvokationObj.whatId = con.id;
		subscriptionInvokationObj.userId = UserInfo.getUserId();
		subscriptionInvokationObj.additionalUser = 'mohitg12@yahoo.com';
		subscriptionInvokationObj.usertemplateid = templateObj.id;
		subscriptionInvokationList.add(subscriptionInvokationObj);
		Test.startTest();
		EP_Event_Notification_Invoke.invokeNotificationToUser(subscriptionInvokationList);
		Test.stopTest();
		system.assert(!subscriptionInvokationList.isEmpty());
	}

	@isTest static void processAdditionalUsers_Positive(){
		String additionalUsers = 'mohitg12@yahoo.com;h.seth@yahoo.com';
		Test.startTest();
		List<String> usersList = EP_Event_Notification_Invoke.processAdditionalUsers(additionalUsers);
		Test.stopTest();
		system.assert(!usersList.isEmpty());
	}

	@isTest static void sendEmailToAdditionalUsers_Positive(){
		data();
		List<String> additionalUsers = new List<String>{'mohitg12@yahoo.com'};
		EmailTemplate templateObj = [select id,Subject,HtmlValue,Body,TemplateType from EmailTemplate where developerName = 'UserTemplate'];
		List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
		EP_Event_Notification_Invoke.subscriptionInvokation subscriptionInvokationObj = new EP_Event_Notification_Invoke.subscriptionInvokation();
		subscriptionInvokationObj.attachmentId = att.id;
		subscriptionInvokationObj.contacttemplateid = templateObj.id;
		subscriptionInvokationObj.contactId = con.id;
		subscriptionInvokationObj.whatId = con.id;
		Test.startTest();
		EP_Event_Notification_Invoke.sendEmailToAdditionalUsers(additionalUsers,templateObj,emailList,subscriptionInvokationObj);
		Test.stopTest();
		system.assert(!emailList.isEmpty());
	}

	@isTest static void sendEmailToContacts_Positive(){
		data();
		EmailTemplate templateObj = [select id,Subject,HtmlValue,Body,TemplateType from EmailTemplate where developerName = 'UserTemplate'];
		List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
		EP_Event_Notification_Invoke.subscriptionInvokation subscriptionInvokationObj = new EP_Event_Notification_Invoke.subscriptionInvokation();
		subscriptionInvokationObj.attachmentId = att.id;
		subscriptionInvokationObj.contacttemplateid = templateObj.id;
		subscriptionInvokationObj.contactId = con.id;
		subscriptionInvokationObj.whatId = con.id;
		Test.startTest();
		EP_Event_Notification_Invoke.sendEmailToContacts(subscriptionInvokationObj,emailList); 
		Test.stopTest();
		system.assert(!emailList.isEmpty());
	}

	@isTest static void sendEmailToUsers_Positive(){
		data();
		EmailTemplate templateObj = [select id,Subject,HtmlValue,Body,TemplateType from EmailTemplate where developerName = 'UserTemplate'];
		List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
		EP_Event_Notification_Invoke.subscriptionInvokation subscriptionInvokationObj = new EP_Event_Notification_Invoke.subscriptionInvokation();
		subscriptionInvokationObj.attachmentId = att.id;
		subscriptionInvokationObj.userId = UserInfo.getUserId();
		subscriptionInvokationObj.usertemplateid = templateObj.id;
		Test.startTest();
		EP_Event_Notification_Invoke.sendEmailToUsers(subscriptionInvokationObj,templateObj,emailList);
		Test.stopTest();
		system.assert(!emailList.isEmpty());
	}

}