@isTest
private class ProductLineInventoryCheckTest {

    @isTest static void testProcessCS() {
        
        Pricebook2 pricebook = new Pricebook2(Name = 'Standard Price Book', isActive = TRUE);
        INSERT pricebook;
        Product2 prod = new Product2(Name = 'Product');
        INSERT prod;
        
        PricebookEntry entry = new PricebookEntry();
        entry.UnitPrice = 20;
        entry.Pricebook2Id = pricebook.Id;
        entry.Product2Id = prod.Id;
        INSERT entry;
        
        csord__Order__c newOrder = EP_TestDataUtility.getSalesOrder();
        newOrder.EP_Error_Product_Codes__c = 'Test';       
        update newOrder; 
        LIST<csord__Order_Line_Item__c> oiList = [SELECT Id, OrderId__c, PricebookEntryId__c FROM csord__Order_Line_Item__c WHERE OrderId__c =:newOrder.Id];
        csord__Order_Line_Item__c lineItem = oiList[0];
        lineItem.PricebookEntryId__c = String.valueOf(entry.Id);
        lineItem.EP_Product__c = prod.Id;
        UPDATE lineItem;
        
        EP_Inventory__c inventory = EP_TestDataUtility.createInventory(null, String.valueOf(prod.Id));
        inventory.EP_Storage_Location__c = newOrder.EP_Stock_Holding_Location__c;
        inventory.EP_Inventory_Availability__c = EP_Common_Constant.LOW_INV;
        inventory.EP_Product__c = prod.Id;
        INSERT inventory;
        
        CSPOFA__Orchestration_Process_Template__c moptc = new CSPOFA__Orchestration_Process_Template__c();
        moptc.Name = 'Test MOPTC';
        insert moptc;
        
        CSPOFA__Orchestration_Process__c mopc = new CSPOFA__Orchestration_Process__c();
        mopc.Name = 'Test MOPC';
        mopc.CSPOFA__Orchestration_Process_Template__c = moptc.Id;
        mopc.Order__c = newOrder.Id;
        insert mopc;
        
        CSPOFA__Orchestration_Step__c mosc = new CSPOFA__Orchestration_Step__c();
        mosc.Name = 'Test MOSC';
        mosc.CSPOFA__Orchestration_Process__c = mopc.Id;
        insert mosc;
        
        list<CSPOFA__Orchestration_Step__c> moscList = new list<CSPOFA__Orchestration_Step__c>();
        moscList.add(mosc);  
        
         
        //Run test
        test.startTest();
        
        ProductLineInventoryCheck plic = new ProductLineInventoryCheck();
		System.assert(moscList.size() > 0, 'Invalid data');  
        plic.process(moscList);
       
        newOrder.EP_AnyProductLineInventory__c = null;
        update newOrder;
        inventory.EP_Inventory_Availability__c = 'Good';        
        update inventory;
		System.assertEquals(inventory.EP_Inventory_Availability__c, 'Good'); 
        plic.process(moscList);
        test.stopTest();
    }
    
}