@isTest
public class EP_CreditExceptionUpdateHelper_UT {
    
    static String REASON_OF_FAILURE = 'Invalid Amount';
    static String EXCEPTION_NUM = '500';
    static string jsonBody =  '{"MSG":{"HeaderCommon":{"InterfaceType":"SFDC_NAV","SourceCompany":"AAF","SourceResponseAddress":"https://cs31.salesforce.com:8443/services/apexrest/v2/AcknowledgementWS2","SourceUpdateStatusAddress":"https://cs31.salesforce.com:8443/services/apexrest/v2/AcknowledgementWS2","ContinueOnError":"true","ComprehensiveLogging":"true","TransportStatus":"New","UpdateSourceOnReceive":"false","UpdateSourceOnDelivery":"false","UpdateSourceAfterProcessing":"false","UpdateDestinationOnDelivery":"true","CallDestinationForProcessing":"true","ObjectType":"Table","ObjectName":"CreditException","CommunicationType":"Async","MsgID":"788d09f3-eba2-45ff-bbd1-cb29c44a7f57","SourceGroupCompany":"null","DestinationGroupCompany":"null","DestinationCompany":"null","CorrelationID":"null","DestinationAddress":"","DestinationUpdateStatusAddress":"","MiddlewareUrlForPush":"null","EmailNotification":"null","ErrorCode":"null","ErrorDescription":"null","ProcessStatus":"null","ProcessingErrorDescription":"ProcessingErrorDescription"},"Payload":{"any0":{"creditExceptionStatus":{"status":[{"seqId":"seqId-000362","identifier":{"exceptionNo":"CER-000026","billTo":"00006683","clientId":"PA22"},"approvalStatus":"Approved","reasonForRejection":"XXX","approverId":"Tester.Te","modifiedDt":"2017-12-21T12:45:00","modifiedBy":"Tester.Test","versionNr":"20171219T234609.000"}]}}}}}';

    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_Order_State_Mapping__c> lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
        List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
        List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData');
        EP_INTEGRATION_CUSTOM_SETTING__c integration_Setting = new EP_INTEGRATION_CUSTOM_SETTING__c(Name = 'Request TimeOut',EP_Value__c = '120000');
        insert integration_Setting; 
    }
    public static list<EP_CreditExceptionUpdateStub.CreditExceptionStatusType> getCreditExceptionList() {
        EP_CreditExceptionUpdateStub stub = (EP_CreditExceptionUpdateStub )  System.JSON.deserialize(jsonBody, EP_CreditExceptionUpdateStub.class);
        EP_Credit_Exception_Request__c creditExp = EP_TestDataUtility.createCreditExceptionRequest();
        list<EP_CreditExceptionUpdateStub.CreditExceptionStatusType> creditExceptionStubList = new list<EP_CreditExceptionUpdateStub.CreditExceptionStatusType>();
        for(EP_CreditExceptionUpdateStub.CreditExceptionStatusType stubs : stub.MSG.Payload.any0.creditExceptionStatus.status) {
            stubs.identifier.exceptionNo = creditExp.Name;
            stubs.creditExcpObj = creditExp;
            creditExceptionStubList.add(stubs);
        }
        System.assertEquals(true, creditExceptionStubList != null);
        return creditExceptionStubList;
    }
    static testMethod void createDataSets_test() {
        EP_CreditExceptionUpdateHelper localObj = new EP_CreditExceptionUpdateHelper();
        
        Test.startTest();
            localObj.createDataSets(EP_CreditExceptionUpdateHelper_UT.getCreditExceptionList());
        Test.stopTest();
        
        System.assertEquals(true, localObj.creditExceptionNameSet.size() > 0 );
    }
    static testMethod void createDataMaps_test() {
        EP_CreditExceptionUpdateHelper localObj = new EP_CreditExceptionUpdateHelper();
        
        Test.startTest();
            localObj.createDataMaps(EP_CreditExceptionUpdateHelper_UT.getCreditExceptionList());
        Test.stopTest();
        
        System.assertEquals(true, localObj.creditExceptionNameSet.size() > 0 );
    }
    //Delegates to other methods. Adding dummy assert
    static testMethod void setCreditExceptionAttributes_test() {
        EP_CreditExceptionUpdateHelper localObj = new EP_CreditExceptionUpdateHelper();
        list<EP_CreditExceptionUpdateStub.CreditExceptionStatusType> creditExceptionStubList = EP_CreditExceptionUpdateHelper_UT.getCreditExceptionList();
        
        Test.startTest();
            localObj.setCreditExceptionAttributes(EP_CreditExceptionUpdateHelper_UT.getCreditExceptionList());
        Test.stopTest();
        System.assertEquals(true,true);
    }
    static testMethod void processValidRecords_test() {
        EP_CreditExceptionUpdateHelper localObj = new EP_CreditExceptionUpdateHelper();
        list<EP_CreditExceptionUpdateStub.CreditExceptionStatusType> creditExceptionStubList = EP_CreditExceptionUpdateHelper_UT.getCreditExceptionList();
        creditExceptionStubList.get(0).identifier.exceptionNo = creditExceptionStubList.get(0).creditExcpObj.Name;
        localObj.creditExceptionNameMap.put(creditExceptionStubList.get(0).creditExcpObj.Name, creditExceptionStubList.get(0).creditExcpObj);
        
        Test.startTest();
            localObj.processValidRecords(creditExceptionStubList.get(0));
        Test.stopTest();
        
        System.assertNotEquals(null, creditExceptionStubList.get(0).creditExcpObj.Id);
    }
    
    static testMethod void isRequestApproved_PositiveScenariotest() {
        EP_CreditExceptionUpdateHelper localObj = new EP_CreditExceptionUpdateHelper();
        String status = EP_Common_Constant.CREDIT_EXC_STATUS_APPROVED;
        
        Test.startTest();
            Boolean result = localObj.isRequestApproved(status);
        Test.stopTest();
        
        System.AssertEquals(true,result);
    }
    static testMethod void isRequestApproved_NegativeScenariotest() {
        EP_CreditExceptionUpdateHelper localObj = new EP_CreditExceptionUpdateHelper();
        String status = EP_Common_Constant.CREDIT_EXC_STATUS_REJECTED;
        
        Test.startTest();
            Boolean result = localObj.isRequestApproved(status);
        Test.stopTest();
        
        System.AssertEquals(false,result);
    }
    static testMethod void setRejectedStatus_test() {
      EP_CreditExceptionUpdateHelper localObj = new EP_CreditExceptionUpdateHelper();
        list<EP_CreditExceptionUpdateStub.CreditExceptionStatusType> creditExceptionStubList = EP_CreditExceptionUpdateHelper_UT.getCreditExceptionList();
        creditExceptionStubList.get(0).creditExcpObj.EP_Status__c = EP_Common_Constant.CREDIT_EXC_STATUS_REJECTED;
        creditExceptionStubList.get(0).creditExcpObj.EP_Reason__c = null;//L4# 78534 - Test Class Fix - Start
        Test.startTest();
            localObj.setRejectedStatus(creditExceptionStubList.get(0));
        Test.stopTest();
        System.assertEquals(false, creditExceptionStubList.get(0).isProcess);
    }

    static testMethod void setRejectedStatus_NegativeTest() {
        EP_CreditExceptionUpdateHelper localObj = new EP_CreditExceptionUpdateHelper();
        list<EP_CreditExceptionUpdateStub.CreditExceptionStatusType> creditExceptionStubList = EP_CreditExceptionUpdateHelper_UT.getCreditExceptionList();
        creditExceptionStubList.get(0).creditExcpObj.EP_Status__c = EP_Common_Constant.CREDIT_EXC_STATUS_REJECTED;
        creditExceptionStubList.get(0).creditExcpObj.EP_Reason__c = null;
        
        Test.startTest();
            localObj.setRejectedStatus(creditExceptionStubList.get(0));
        Test.stopTest();
        
        System.assertEquals(true, creditExceptionStubList.get(0).failureReason != null);
    }
    static testMethod void setfailureReason_test() {
        EP_CreditExceptionUpdateHelper localObj = new EP_CreditExceptionUpdateHelper();
        list<EP_CreditExceptionUpdateStub.CreditExceptionStatusType> creditExceptionStubList = EP_CreditExceptionUpdateHelper_UT.getCreditExceptionList();
        
        Test.startTest();
            localObj.setfailureReason(creditExceptionStubList.get(0),REASON_OF_FAILURE);
        Test.stopTest();
        
        System.assertEquals(true, creditExceptionStubList.get(0).isProcess == false );
        System.assertEquals(true, creditExceptionStubList.get(0).failureReason == REASON_OF_FAILURE);
    }

    static testMethod void isRecordsExists_PositiveScenariotest() {
        EP_CreditExceptionUpdateHelper localObj = new EP_CreditExceptionUpdateHelper();
        list<EP_CreditExceptionUpdateStub.CreditExceptionStatusType> creditExceptionStubList = EP_CreditExceptionUpdateHelper_UT.getCreditExceptionList();
        localObj.creditExceptionNameMap.put(creditExceptionStubList.get(0).creditExcpObj.Name, creditExceptionStubList.get(0).creditExcpObj);
        
        Test.startTest();
            Boolean result = localObj.isRecordsExists(creditExceptionStubList.get(0));
        Test.stopTest();
        
        System.assertEquals(true,result);
    }
    static testMethod void isRecordsExists_NegativeScenariotest() {
        EP_CreditExceptionUpdateHelper localObj = new EP_CreditExceptionUpdateHelper();
        list<EP_CreditExceptionUpdateStub.CreditExceptionStatusType> creditExceptionStubList = EP_CreditExceptionUpdateHelper_UT.getCreditExceptionList();
        localObj.creditExceptionNameMap.put('XXXX', creditExceptionStubList.get(0).creditExcpObj);
        
        Test.startTest();
            Boolean result = localObj.isRecordsExists(creditExceptionStubList.get(0));
        Test.stopTest();
        
        System.AssertEquals(false,result);
    }
    static testMethod void isValidApprovalStatus_PositiveScenariotest() {
        EP_CreditExceptionUpdateHelper localObj = new EP_CreditExceptionUpdateHelper();
        list<EP_CreditExceptionUpdateStub.CreditExceptionStatusType> creditExceptionStubList = EP_CreditExceptionUpdateHelper_UT.getCreditExceptionList();
        creditExceptionStubList.get(0).approvalStatus = EP_Common_Constant.CREDIT_EXC_STATUS_AUTO_APPROVED;
        
        Test.startTest();
            Boolean result = localObj.isValidApprovalStatus(creditExceptionStubList.get(0));
        Test.stopTest();
        
        System.AssertEquals(true,result);
    }
    static testMethod void isValidApprovalStatus_NegativeScenariotest() {
        EP_CreditExceptionUpdateHelper localObj = new EP_CreditExceptionUpdateHelper();
        list<EP_CreditExceptionUpdateStub.CreditExceptionStatusType> creditExceptionStubList = EP_CreditExceptionUpdateHelper_UT.getCreditExceptionList();
        creditExceptionStubList.get(0).approvalStatus = '';
        
        Test.startTest();
            Boolean result = localObj.isValidApprovalStatus(creditExceptionStubList.get(0));
        Test.stopTest();
        
        System.AssertEquals(false,result);
    }
    static testMethod void isValidOrder_PositiveScenariotest() {
        EP_CreditExceptionUpdateHelper localObj = new EP_CreditExceptionUpdateHelper();     
        list<EP_CreditExceptionUpdateStub.CreditExceptionStatusType> creditExceptionStubList = EP_CreditExceptionUpdateHelper_UT.getCreditExceptionList();
        localObj.creditExceptionNameMap.put(creditExceptionStubList.get(0).identifier.exceptionNo,creditExceptionStubList.get(0).creditExcpObj);
        
        Test.startTest();
            Boolean result = localObj.isValidOrder(creditExceptionStubList.get(0));
        Test.stopTest();
        
        System.AssertEquals(true,result);
    }
    static testMethod void isValidOrder_NegativeScenariotest() {
        EP_CreditExceptionUpdateHelper localObj = new EP_CreditExceptionUpdateHelper();     
        list<EP_CreditExceptionUpdateStub.CreditExceptionStatusType> creditExceptionStubList = EP_CreditExceptionUpdateHelper_UT.getCreditExceptionList();
        creditExceptionStubList.get(0).creditExcpObj.EP_CS_Order__c = null;
        localObj.creditExceptionNameMap.put(creditExceptionStubList.get(0).identifier.exceptionNo, creditExceptionStubList.get(0).creditExcpObj);  
        
        Test.startTest();
            Boolean result = localObj.isValidOrder(creditExceptionStubList.get(0));
        Test.stopTest();
        
        System.AssertEquals(false,result);
    }
    static testMethod void setCreditExceptions_test() {
        EP_CreditExceptionUpdateHelper localObj = new EP_CreditExceptionUpdateHelper();     
        list<EP_CreditExceptionUpdateStub.CreditExceptionStatusType> creditExceptionStubList = EP_CreditExceptionUpdateHelper_UT.getCreditExceptionList();
        Set<string> creditExceptionNameSet = new Set<string>{creditExceptionStubList.get(0).creditExcpObj.name};
        
        Test.startTest();
            localObj.setCreditExceptions(creditExceptionNameSet);
        Test.stopTest();
        
        System.assertEquals(true, localObj.creditExceptionNameMap != null);
    }


    
    //Delegates to other methods. Adding dummy assert
    static testMethod void  setCreditExceptionAttributes_isRecordsNotExists() {
        EP_CreditExceptionUpdateHelper localObj = new EP_CreditExceptionUpdateHelper();
        list<EP_CreditExceptionUpdateStub.CreditExceptionStatusType> creditExceptionStubList = EP_CreditExceptionUpdateHelper_UT.getCreditExceptionList();
        creditExceptionStubList.get(0).identifier.exceptionNo = 'EXP-123';
        
        Test.startTest();
            localObj.setCreditExceptionAttributes(creditExceptionStubList);
        Test.stopTest();
        System.assertEquals(true,true);
    }
    //Delegates to other methods. Adding dummy assert
    static testMethod void  setCreditExceptionAttributes_isValidApprovalStatus() {
        EP_CreditExceptionUpdateHelper localObj = new EP_CreditExceptionUpdateHelper();
        list<EP_CreditExceptionUpdateStub.CreditExceptionStatusType> creditExceptionStubList = EP_CreditExceptionUpdateHelper_UT.getCreditExceptionList();
        creditExceptionStubList.get(0).approvalStatus = '';
        Test.startTest();
            localObj.setCreditExceptionAttributes(creditExceptionStubList);
        Test.stopTest();
        System.assertEquals(true,true);
    }
    //Delegates to other methods. Adding dummy assert
    static testMethod void  setCreditExceptionAttributes_isValidOrder() {
        EP_CreditExceptionUpdateHelper localObj = new EP_CreditExceptionUpdateHelper();
        list<EP_CreditExceptionUpdateStub.CreditExceptionStatusType> creditExceptionStubList = EP_CreditExceptionUpdateHelper_UT.getCreditExceptionList();
        
        creditExceptionStubList.get(0).creditExcpObj.EP_CS_Order__c = null;
        
        update creditExceptionStubList.get(0).creditExcpObj;
        
        localObj.creditExceptionNameMap.put(creditExceptionStubList.get(0).identifier.exceptionNo, creditExceptionStubList.get(0).creditExcpObj);  
        Test.startTest();
            localObj.setCreditExceptionAttributes(creditExceptionStubList);
        Test.stopTest();
        System.assertEquals(true,true);
    }
}