public class EP_ASTVendorNewToActive extends EP_AccountStateTransition {
    public EP_ASTVendorNewToActive() {
        finalState = EP_AccountConstant.ACTIVE;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTVendorNewToActive','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTVendorNewToActive','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
		EP_GeneralUtility.Log('Public','EP_ASTVendorNewToActive','isGuardCondition');
       	return true;
    }
}