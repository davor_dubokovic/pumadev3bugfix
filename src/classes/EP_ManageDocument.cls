/**
 *  @Author <Kamal Garg>
 *  @Name <EP_ManageDocument>
 *  @CreateDate <31/05/2016>
 *  @Description <This is the apex class used to generate PDF>
 *  @Version <1.0>
 */
public with sharing class EP_ManageDocument {
    
    private static final string CLASS_NAME = 'EP_ManageDocument';
    private static final string METHOD_NAME = 'manageDocuments';
    private static final string METHOD_NAME1 = 'sendAcknowledgement';
    //map containg 'HeaderCommon' information
    private static Map<String, String> headerMap = new Map<String, String>();
    //map containing 'Attachment' record 'Name' field value as key and result of getMessage() Api of 'Exception' class
    public static Map<String, String> errDesMap = new Map<String, String>();
    //map containing 'Attachment' record 'Name' field value as key and result of getStatusCode() Api of 'Exception' class
    public static Map<String, String> errCodeMap = new Map<String, String>();
    //map containing 'Attachment' record 'Name' field value as key and 'Id' field value of 'Attachment' record
    public static Map<String, String> successRecIdMap = new Map<String, String>();
    
    
    /**
     * @author <Kamal Garg>
     * @name <manageDocuments>
     * @date <31/05/2016>
     * @description <This method is used to generate JSON Response based on JSON input>
     * @version <1.0>
     * @param String
     * @return String
     */
    public static String manageDocuments(String jsonInput) {
        String jsonResponse;
        //Code changes for Requirement - #59462(Reliable Messaging) and #59463(Message Idempotency)
        //1: Get Header Details from the JSON String
        Map<String, String> mapOfHeaderCommonDetails = EP_IntegrationService.getRequestHeaderDetails(jsonInput);
        
        //2: Check for Idempotency - Check the message Id in integration Record if already exists then return old Response
        if(EP_IntegrationService.isIdempotent(mapOfHeaderCommonDetails.get(EP_Common_Constant.MSG_ID))) {
            return EP_IntegrationService.processResponse;
        }
        //3: Log Request - Log the JSON String and other details from Header into the integration record for inbound Request 
        String subJson = jsonInput;  
        if (subJson.length() > 131070) {

            subJson = subJson.substring(0,131070);    
        }
         
        EP_IntegrationRecord__c integrationRecord = EP_IntegrationService.logInboundRequest('OPMS_PDF',mapOfHeaderCommonDetails,subJson);
		//Code changes End for Requirement - #59462(Reliable Messaging) and #59463(Message Idempotency)
        try
        {
            System.debug('[EP_ManageDocument:manageDocuments] jsonInput : '+jsonInput);
            Map<String, Object> jsonInputMap = (Map<String, Object>)JSON.deserializeUntyped(jsonInput);
            Map<String, Object> msgMap = (Map<String, Object>)jsonInputMap.get(EP_Common_Constant.MSG);
            Map <String, Object> headerCommonMap = (Map<String, Object>)msgMap.get(EP_Common_Constant.HEADER_COMMON);
            for(String key : headerCommonMap.keyset()) {
                headerMap.put(key, String.valueof(headerCommonMap.get(key)));
            }
            Map <String, Object> headerOPMSMap = (Map<String, Object>)msgMap.get(EP_Common_Constant.HEADER_OPMS_TAG);
            Map <String, Object> generateDocumentDetailMap = (Map<String, Object>)headerOPMSMap.get(EP_Common_Constant.GENERATE_DOCUMENT_DETAIL_TAG);
            String documentType = String.valueOf(generateDocumentDetailMap.get(EP_Common_Constant.DOCUMENT_TYPE_TAG));
            //Map<String, Object> payloadMap = (Map<String, Object>)msgMap.get(EP_Common_Constant.PAYLOAD);
            Map<String, Object> payloadMap = (Map<String, Object>)msgMap.get(EP_Common_Constant.PAYLOAD_TAG);
            // system.debug(' --- MAP PayLoad --- ' + payloadMap);
            Map<String, Object> anyMap = (Map<String, Object>)payloadMap.get(EP_Common_Constant.ANYZERO_NODE);// Defect Fix- 56975
            Map<String, Object> documentsMap = (Map<String, Object>)anyMap.get(EP_Common_Constant.DOCUMENTS_TAG);
            
            EP_ManageDocument.Documents documents = new EP_ManageDocument.Documents();
            documents.document = new List<EP_ManageDocument.Document>();
            if(documentsMap.get(EP_Common_Constant.DOCUMENT_TAG) instanceof List<Object>) {
                List<Object> docList = (List<Object>)documentsMap.get(EP_Common_Constant.DOCUMENT_TAG);
                for(Object obj : docList) {
                    Map<String, Object> docInfoMap = (Map<String, Object>)obj;
                    documents.document.add(EP_DocumentUtil.fillDocumentsWrapper(docInfoMap));
                }
            } else {
                Map<String, Object> docInfoMap = (Map<String, Object>)documentsMap.get(EP_Common_Constant.DOCUMENT_TAG);
                documents.document.add(EP_DocumentUtil.fillDocumentsWrapper(docInfoMap));
            }
            if(Schema.SobjectType.EP_Invoice__c.getLabel().equals(EP_DocumentUtil.getObjectNameFromDocumentType(documentType))) {
                EP_ManageInvoiceDocument.generateAttachmentForInvoice(documents);
            }
            else if(Schema.SobjectType.EP_Customer_Credit_Memo__c.getLabel().equals(EP_DocumentUtil.getObjectNameFromDocumentType(documentType))) {
                EP_ManageCreditMemoDocument.generateAttachmentForCreditMemo(documents);
            }
            else if(Schema.SobjectType.EP_Customer_Account_Statement__c.getLabel().equals(EP_DocumentUtil.getObjectNameFromDocumentType(documentType))) {
                EP_ManageAccountStatementDocument.generateAttachmentForAccountStatement(documents);
            }
            else if(Schema.SobjectType.EP_Delivery_Docket__c.getLabel().equals(EP_DocumentUtil.getObjectNameFromDocumentType(documentType))){
                EP_ManageCustomerDeliveryDocketDocument.generateAttachmentForDeliveryDocket(documents);
            }
            else if(Schema.SobjectType.Account.getLabel().equals(EP_DocumentUtil.getObjectNameFromDocumentType(documentType))){
                EP_ManageDirectDebitAdviceDocument.generateAttachmentForDirectDebitAdvice(documents);
            }
            else if(Schema.SobjectType.EP_BOL_Document__c.getLabel().equals(EP_DocumentUtil.getObjectNameFromDocumentType(documentType))){
                EP_ManageBOLDocument.generateAttachmentForBOL(documents);
            }
            else if(Schema.SobjectType.Order.getLabel().equals(EP_DocumentUtil.getObjectNameFromDocumentType(documentType))){
                EP_ManageOrderConfirmationDocument.generateAttachmentForOrderConfirmation(documents);
            }
            /*CR 70287 START*/
            else if(EP_Common_Constant.LOADPLAN_TAG.equals(documentType)){
                EP_ManageLoadPlanDocument.generateAttachmentForOrderLoadPlan(documents);
            }
            /*CR 70287 END*/
            jsonResponse = sendAcknowledgement(NULL);
        } 
        catch(Exception ex) 
        {
            System.debug('[EP_ManageDocument:manageDocuments] ex.getLineNumber() : '+ex.getLineNumber());
            System.debug('[EP_ManageDocument:manageDocuments] ex.getMessage() : '+ex.getMessage());
            System.debug('[EP_ManageDocument:manageDocuments] ex.getStackTraceString() : '+ex.getStackTraceString());
            EP_LoggingService.logServiceException(ex, UserInfo.getOrganizationId(), EP_Common_constant.EPUMA, METHOD_NAME, CLASS_NAME, EP_Common_constant.ERROR, UserInfo.getUserId(), EP_Common_constant.TARGET_SF, EP_Common_Constant.BLANK, headerMap.get(EP_Common_Constant.MSG_ID));
            jsonResponse = sendAcknowledgement(ex);
        }
        
        System.debug('[EP_ManageDocument:manageDocuments] jsonResponse : '+jsonResponse);

        //Code changes for Requirement - #59462(Reliable Messaging) and #59463(Message Idempotency)
        //4: Log the Proccessing Response 
        EP_IntegrationService.logProcessResponse(integrationRecord, jsonResponse);
        //Code changes End for Requirement - #59462(Reliable Messaging) and #59463(Message Idempotency)
        
        return jsonResponse;
    }
    
    /**
     * @author <Kamal Garg>
     * @name <sendAcknowledgement>
     * @date <31/05/2016>
     * @description <This method is used to generate acknowledgment to be sent in JSON response>
     * @version <1.0>
     * @param 
     * @return String
     */
    private static String sendAcknowledgement(Exception ex) {
        String jsonResponse = EP_Common_Constant.BLANK;
        try
        {
            List<EP_AcknowledmentGenerator.cls_dataset> datasetList = new List<EP_AcknowledmentGenerator.cls_dataset>();
            
            EP_Message_Id_Generator__c msgIdGenObj = new EP_Message_Id_Generator__c();
	        Database.insert(msgIdGenObj, true);
	        String strMsgGeneratorName = [SELECT Name FROM EP_Message_Id_Generator__c WHERE Id=:msgIdGenObj.Id Limit 1].Name;
	        String sMsgID = EP_IntegrationUtil.getMessageId(EP_Common_Constant.STRSOURCE, EP_Common_Constant.STRLOCATION,
	                                                        EP_Common_Constant.ATTACHMENT, System.NOW(), strMsgGeneratorName);
	        
	        headerMap.put(EP_Common_Constant.CORR_ID, headerMap.get(EP_Common_Constant.MSG_ID));
	        headerMap.put(EP_Common_Constant.MSG_ID, sMsgID);
	        
	        if(ex != NULL) {
	            headerMap.put(EP_Common_Constant.PROCESS_STATUS, EP_Common_Constant.FAILURE);
	            headerMap.put(EP_Common_Constant.ERROR_DESCRIPTION, ex.getMessage());
	        } else if(!errDesMap.isEmpty()) {
	            headerMap.put(EP_Common_Constant.PROCESS_STATUS, EP_Common_Constant.FAILURE);
	        } else if(!successRecIdMap.isEmpty()) {
	            headerMap.put(EP_Common_Constant.PROCESS_STATUS, EP_Common_Constant.SUCCESS);
	        }
	        if(!successRecIdMap.isEmpty()) {
	            EP_AcknowledmentGenerator.cls_dataset dataset;
	            for(String key : successRecIdMap.keySet()) {
	                dataset = new EP_AcknowledmentGenerator.cls_dataset();
	                dataset.name = EP_Common_Constant.DOCUMENT_TAG;
	                dataset.seqId = key;
	                dataset.errorCode = EP_Common_Constant.BLANK;
	                dataset.errorDescription = EP_Common_Constant.BLANK;
	                datasetList.add(dataset);
	            }
	        }
	        if(!errDesMap.isEmpty()) {
	            EP_AcknowledmentGenerator.cls_dataset dataset;
	            for(String key : errDesMap.keySet()) {
	                dataset = new EP_AcknowledmentGenerator.cls_dataset();
	                dataset.name = EP_Common_Constant.DOCUMENT_TAG;
	                dataset.seqId = key;
	                dataset.errorCode = errCodeMap.get(key);
	                dataset.errorDescription = errDesMap.get(key);
	                datasetList.add(dataset);
	            }
	        }
	        
	        jsonResponse = EP_AcknowledmentGenerator.createacknowledgement(headerMap, datasetList, EP_Common_Constant.BLANK);
        }
        catch(Exception ackException) {
            System.debug('[EP_ManageDocument:sendAcknowledgement] ackException.getLineNumber() : '+ackException.getLineNumber());
            System.debug('[EP_ManageDocument:sendAcknowledgement] ackException.getMessage() : '+ackException.getMessage());
            System.debug('[EP_ManageDocument:sendAcknowledgement] ackException.getStackTraceString() : '+ackException.getStackTraceString());
            EP_LoggingService.logServiceException(ackException, UserInfo.getOrganizationId(), EP_Common_constant.EPUMA, METHOD_NAME1, CLASS_NAME, EP_Common_constant.ERROR, UserInfo.getUserId(), EP_Common_constant.TARGET_SF, EP_Common_Constant.BLANK, headerMap.get(EP_Common_Constant.MSG_ID));
        }
        
        return jsonResponse;
     }
    
    /**
     *  @Author <Kamal Garg>
     *  @Name <Documents>
     *  @CreateDate <31/05/2016>
     *  @Description <This is the wrapper class containing List of 'Document'>
     *  @Version <1.0>
     */
    public with sharing class Documents {
        public Document[] document;
    }
    /**
     *  @Author <Kamal Garg>
     *  @Name <Document>
     *  @CreateDate <31/05/2016>
     *  @Description <This is the wrapper class containing 'Attachment' record information>
     *  @Version <1.0>
     */
    public with sharing class Document {
        public DocumentMetaData documentMetaData;
        public String documentRefId;
        public String documentURL;
        public String documentFileName;
        public String documentEmbedded;
        public String documentUUId;
    }
    /**
     *  @Author <Kamal Garg>
     *  @Name <DocumentMetaData>
     *  @CreateDate <31/05/2016>
     *  @Description <This is the wrapper class containing 'Attachment' record metadta information>
     *  @Version <1.0>
     */
    public with sharing class DocumentMetaData {
        public String documentTemplateName;
        public String collationKey;
        public MetaDatas metaDatas;
    }
    /**
     *  @Author <Kamal Garg>
     *  @Name <MetaDatas>
     *  @CreateDate <31/05/2016>
     *  @Description <This is the wrapper class containing 'Attachment' record parent record information>
     *  @Version <1.0>
     */
    public with sharing class MetaDatas {
        public MetaData[] metaData;
    }
    /**
     *  @Author <Kamal Garg>
     *  @Name <MetaData>
     *  @CreateDate <31/05/2016>
     *  @Description <This is the wrapper class containing 'Attachment' record parent record information in form of name-value pair>
     *  @Version <1.0>
     */
    public with sharing class MetaData {
        public String name;
        public String value;
    }
    /**
     *  @Author <Kamal Garg>
     *  @Name <CustomerInvoiceWrapper>
     *  @CreateDate <18/06/2016>
     *  @Description <This is the wrapper class containing 'Customer Invoice' record information>
     *  @Version <1.0>
     */
    public with sharing class CustomerInvoiceWrapper {
        public String invoiceNr;
        public String SeqId;
        public String BillTo;
        public String ShipTo;
        public String InvoiceDate;
        public String InvoiceDueDate;
        public String issuedFromId;
        //L4 #45304 Changes Start
        public String entryNr;
        //L4 #45304 Changes End
        
    }
    /**
     *  @Author <Pooja Dhiman>
     *  @Name <CustomerCreditMemoWrapper>
     *  @CreateDate <03/07/2016>
     *  @Description <This is the wrapper class containing 'Customer Credit Memo' record information>
     *  @Version <1.0>
     */
    public with sharing class CustomerCreditMemoWrapper {
        public String docNr;
        public String SeqId;
        public String Currencie;
        public String CreditMemoIssueDate;
        public String issuedToId;
        public String issuedToName;
        public String issuedFromId;
        //L4 #45304 Changes Start
        public String entryNr;
        //L4 #45304 Changes End
    }
    /**
     *  @Author <Kamal Garg>
     *  @Name <CustomerDeliveryDocketWrapper>
     *  @CreateDate <22/07/2016>
     *  @Description <This is the wrapper class containing 'Customer Delivery Docket' record information>
     *  @Version <1.0>
     */
    public with sharing class CustomerDeliveryDocketWrapper {
        public String issuedToId;
        public String shipTo;
        public String deliveryDocNr;
        public String deliveryDocDate;
        public String tripId;
        public String orderId;
        public String issuedFromId;
    }
    
}