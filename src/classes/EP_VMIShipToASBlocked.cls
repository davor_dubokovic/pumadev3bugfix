/*
 *  @Author <Accenture>
 *  @Name <EP_VMIShipToASBlocked>
 *  @CreateDate <>
 *  @Description <VMI Ship To Account State for 06-Blocked Status>
 *  @NovaSuite Fix -- comments added
 *  @Version <1.0>
 */
 public with sharing class EP_VMIShipToASBlocked extends EP_AccountState{
    /***NOvasuite fix constructor removed**/
    /**
    * @author <Accenture>
    * @Name setAccountDomainObject
    */ 
    public override void setAccountDomainObject(EP_AccountDomainObject currentAccount){
        EP_GeneralUtility.Log('Public','EP_VMIShipToASBlocked','setAccountDomainObject');
        super.setAccountDomainObject(currentAccount);
    }
    /**
    * @author <Accenture>
    * @Name doOnEntr
    */ 
    public override void doOnEntry(){
        EP_GeneralUtility.Log('Public','EP_VMIShipToASBlocked','doOnEntry');
        EP_AccountService accService = new EP_AccountService(this.account);
        //code Changes for #45362 Start
         accService.doActionSendUpdateRequestToNavAndLomo();
        //code Changes for #45362 End
        
    }  
    /**
    * @author <Accenture>
    * @Name constructor of class EP_VMIShipToASBlocked
    */ 
    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_VMIShipToASBlocked','doOnExit');
        
    }
    /**
    * @author <Accenture>
    * @Name doTransition
    */ 
    public override boolean doTransition(){
        EP_GeneralUtility.Log('Public','EP_VMIShipToASBlocked','doTransition');
        return super.doTransition();
    }
    /**
    * @author <Accenture>
    * @Name isInboundTransitionPossible
    */ 
    public override boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_VMIShipToASBlocked','isInboundTransitionPossible');
        //Override this with all possible guard conditions that permits the state transition possible
        //For e.g.) Criteria based state "coming from", "Event" that's triggering this change 
        return super.isInboundTransitionPossible();

    }

}