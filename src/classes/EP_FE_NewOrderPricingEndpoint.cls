@RestResource(urlMapping = '/FE/V1/order/pricing/*')
/* 
   @Author <Pryank Sahu>
   @name <EP_FE_NewOrderPricingEndpoint>
   @CreateDate <23/04/2016>
   @Description <Calculate the price of a new order>  
   @Version <1.0>
*/
global with sharing class EP_FE_NewOrderPricingEndpoint {

    private static Integer attempts = 1;//Prayank
                         
    @HttpPost
    global static EP_FE_NewOrderPricingResponse calculateOrderPrice(EP_FE_NewOrderPricingRequest request) {
        EP_FE_NewOrderPricingResponse response;
            
        response = submitOrderInformation(request);
        
        return response;
    }

/* 
   @Author <Prayank>
   @name <submitOrderInformation>
   @CreateDate <23/04/2016>
   @Description <>  
   @Version <1.0>
*/    

    private static EP_FE_NewOrderPricingResponse submitOrderInformation(EP_FE_NewOrderPricingRequest request){
    //String sBasketId = 'basketIdDummy';
    Id BaskatId = request.sellToId;//Id.ValueOf(sBasketId);
    
    String sConfigurationId =  'ConfigurationIdDummy';
    Id ConfigurationId =  request.sellToId;//Id.ValueOf(sConfigurationId);
    
    //String TransportIdDummy =  'TransportIdDummy';
    //Id TransfortId = Id.ValueOf(TransportIdDummy);
    
                                   
        Account AccSellToObj = [Select Id,AccountNumber,EP_Payment_Term_Lookup__c From Account where Id =: request.sellToId];
        Account AccShipToObj = [Select Id From Account where Id =: request.shipToId];
                       
                            //System.debug('========request==========='+request);
                            EP_OrderDataStub OrdStub = new EP_OrderDataStub();
                            
                            List<EP_OrderDataStub.LineItem> lorderDataStub = new List<EP_OrderDataStub.LineItem>();
                            
                            for(EP_FE_NewOrderPricingRequest.EP_FE_OrderItem o:request.orderItems){
                                
                                //[amount=6, productId=01t1F000001g6uPQAQ, tankId=null],
                                
                                EP_OrderDataStub.LineItem  li = new EP_OrderDataStub.LineItem();
                                 
                                 li.lineItemId = '';
                                 li.quantity = String.valueOf(o.Amount);
                                 li.companyCode = '';
                                 li.productCode = '30005';                                                        
                            
                            lorderDataStub.add(li);
                            
                            }                            
                            
                            OrdStub.lineItems = lorderDataStub ;
                            OrdStub.deliveryType = request.deliveryType;
                            OrdStub.VMIOrder = '';
                            OrdStub.blanketOrderType = '' ; 
                            OrdStub.requestDate= String.ValueOf(request.orderDate);
                            OrdStub.orderEpoch ='Current'; 
                            OrdStub.truckAndTransport ='' ; 
                            OrdStub.rro ='';
                            OrdStub.truckId = '' ;
                            OrdStub.orderType = 'Bulk' ;
                            OrdStub.pickupLocation = '0011F00000645s5' ;
                            OrdStub.shipTo =  request.shipToId;
                            OrdStub.accountNumber = AccSellToObj.AccountNumber  ;
                            OrdStub.quantity = '12' ;
                            OrdStub.run = '' ;
                            OrdStub.supplyLocation = 'SHL-37026' ;
                            OrdStub.preferedDeliverySlot = '' ; 
                            OrdStub.captiveORderReferenceNumber = '' ;
                            OrdStub.paymentTerm =  AccSellToObj.EP_Payment_Term_Lookup__c;
                            OrdStub.supplierId = '' ; 
                            OrdStub.overridePaymenTerm = '' ;
                            OrdStub.contractId = '' ;
                            OrdStub.customPoNumber = '' ;
                            OrdStub.bolNumber = '' ;
                            OrdStub.basketId = BaskatId ;
                            OrdStub.configurationId = ConfigurationId;
                            OrdStub.currencyCode = '' ;
                            OrdStub.transporterId = '' ;
                            
                            
                            
        string xmlMessage = '';                    
        String messageId = '8010l0000008l6qAAA-21082017220223-001780'; 
        String messageType = 'SEND_ORDER_PRICING_REQUEST';  
        string responseBody = '';
        String companyCode = 'AAF'; //'AAF' 'EPA'
        String recordId = request.sellToId;
        CS_ORDER_SETTINGS__c csOrderSetting = CS_ORDER_SETTINGS__c.getInstance();           
        
        
        //1. Get VF page name for given outbound Message Type from custom setting
            EP_CS_OutboundMessageSetting__c msgSetting = EP_CustomSettingsUtil.getOutboundMessageSetting(messageType);
            
            //2. get XML Message                                                                     EP_OrderPricingRequestXML
            //xmlMessage = EP_FE_NewOrderPricingEndpoint.generateXML(recordId, messageId, messageType, msgSetting.Payload_VF_Page_Name__c, companyCode);
            
             xmlMessage = '<?xml version="1.0" encoding="UTF-8"?> '+
                            '<p:MSG xmlns:p="http://TRAF.EPUMA.BizTalk.BTCloudToNav.PushAsync.Schemas.message_RequestResponse" xmlns:tns="http://www.trafigura.com/Header/" xmlns:tns1="http://www.example.org/PriceRequest" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://TRAF.EPUMA.BizTalk.BTCloudToNav.PushAsync.Schemas.message_RequestResponse ../XSD Files/Message_PriceRequestMessage.xsd "> '+
                               ' <HeaderCommon> '+
                                    '<MsgID>SFD-GBL-CPE-06052018-23:58:05-003922</MsgID> '+
                                    '<InterfaceType>SFDC_NAV</InterfaceType> '+
                                    '<SourceGroupCompany></SourceGroupCompany> '+
                                    '<DestinationGroupCompany></DestinationGroupCompany> '+
                                    '<SourceCompany>EPA</SourceCompany> '+
                                    '<DestinationCompany></DestinationCompany> '+
                                    '<CorrelationID></CorrelationID> '+
                                    '<DestinationAddress></DestinationAddress> '+
                                    '<SourceResponseAddress>https://cs58.salesforce.com:8443/services/apexrest/v2/AcknowledgementWS2</SourceResponseAddress> '+
                                    '<SourceUpdateStatusAddress>https://cs58.salesforce.com:8443/services/apexrest/v2/AcknowledgementWS2</SourceUpdateStatusAddress> '+
                                    '<DestinationUpdateStatusAddress></DestinationUpdateStatusAddress> '+
                                    '<MiddlewareUrlForPush></MiddlewareUrlForPush> '+
                                    '<EmailNotification></EmailNotification> '+
                                    '<ErrorCode></ErrorCode> '+
                                    '<ErrorDescription></ErrorDescription> '+
                                    '<ProcessingErrorDescription></ProcessingErrorDescription> '+
                                    '<ContinueOnError>true</ContinueOnError> '+
                                    '<ComprehensiveLogging>true</ComprehensiveLogging> '+
                                    '<TransportStatus>New</TransportStatus> '+
                                    '<ProcessStatus></ProcessStatus> '+
                                    '<UpdateSourceOnReceive>false</UpdateSourceOnReceive> '+
                                    '<UpdateSourceOnDelivery>false</UpdateSourceOnDelivery> '+
                                    '<UpdateSourceAfterProcessing>false</UpdateSourceAfterProcessing> '+
                                    '<UpdateDestinationOnDelivery>true</UpdateDestinationOnDelivery> '+
                                    '<CallDestinationForProcessing>true</CallDestinationForProcessing> '+
                                    '<ObjectType>Table</ObjectType> '+
                                    '<ObjectName>Pricing_Customer</ObjectName> '+
                                    '<CommunicationType>Async</CommunicationType> '+
                                '</HeaderCommon> '+
                                ' <payload> '+
                                ' <pricingRequest> '+
                                 ' <requestHeader> '+
                                   ' <seqId>c59cbcf9-d5e2-481c-9d2c-7204dfa6ebbf</seqId> '+
                                   ' <companyCode>AAF</companyCode> '+
                                   ' <priceType>Fuel Price</priceType>  '+
                                   ' <priceRequestSource>SFDC</priceRequestSource> '+
                                   ' <currencyCode>AUD</currencyCode> '+
                                   ' <deliveryType>Delivery</deliveryType> '+
                                   ' <customerId>00001887</customerId> '+
                                   ' <shipToId>00002281</shipToId> '+
                                   ' <supplyLocationId>SHL-2586</supplyLocationId> '+
                                   ' <transporterId></transporterId> '+
                                   ' <supplierId></supplierId> '+
                                   ' <priceDate>2001-12-35</priceDate> '+
                                   ' <onRun>true</onRun> '+
                                   ' <orderId></orderId> '+
                                   ' <applyOrderQuantity>true</applyOrderQuantity> '+
                                   ' <totalOrderQuantity>6.0</totalOrderQuantity> '+
                                   ' <priceConsolidationBasis>Summarised Price</priceConsolidationBasis> '+
                                   ' <versionNr></versionNr> '+
                                 ' </requestHeader> '+
                                 ' <requestLines> '+
                                 '   <line> '+
                                     ' <seqId>c59cbcf9-d5e2-481c-9d2c-7204dfa6ebbf</seqId> '+
                                     ' <entityId></entityId> '+
                                     ' <orderId>12345</orderId> '+
                                     ' <lineItemId>1</lineItemId> '+
                                     ' <itemId>30005</itemId> '+
                                     ' <quantity>6.0</quantity> '+
                                     ' <deliveryType>Delivery</deliveryType> '+
                                     ' <shipToId>00002281</shipToId> '+
                                     ' <supplyLocationId>SHL-2586</supplyLocationId> '+
                                     ' <onRun>false</onRun> '+
                                     ' <priceDate>2001-12-35</priceDate> '+
                                   ' </line> '+
                                  ' </requestLines> '+
                                ' </pricingRequest> '+
                              ' </payload> '+
                               ' <StatusPayload>StatusPayload</StatusPayload> '+
                               '</p:MSG> ';
                               
             System.debug('=========xmlMessage=========='+ xmlMessage);                     
            
            //Get End Point
            //https://api.eu.coolfurnace.net/WP3DEV/{CID}/ImportFuelPriceBusinessHit:ImportMsgIDPayload
            string endPoint = msgSetting.End_Point__c.replace(EP_Common_Constant.COMPANY_CODE_URL_TOKAN, companyCode);
            
            System.debug('=========endPoint=========='+ endPoint); 
            
            //Defect Fix End - 57012
            //3. initate integration Services                                                                                            bf4c14cdc3aa4218876496fb6be0e930
            EP_OutboundIntegrationService intService = new EP_OutboundIntegrationService(endPoint, EP_Common_Constant.POST, xmlMessage, msgSetting.EP_Subscription_Key__c);
            
            System.debug('=========intService=========='+ intService); 
            
            //4. Do Call out
            EP_IntegrationServiceResult result = doCallOut(intService, msgSetting);
            
             System.debug('=========result=========='+ result); 

            //5. Process result
            responseBody = result.getResponse().getBody();

            system.debug('sendOutboundPricingMessage response body: ' + responseBody);        
                            
                            
                            
        return null;
    }
    
    private static string generateXML(string recordId, string messageId, string messageType, string className, string companyCode) {
        EP_GeneralUtility.Log('Private','EP_OutboundMessageServicePlanB','generateXML');
        Type classType = Type.forName(className);
        EP_GenerateRequestXML genXML = (EP_GenerateRequestXML) classType.newInstance();
        genXML.recordId = recordId;
        genXML.messageId = messageId;
        genXML.messageType = messageType;
        genXML.companyCode = companyCode;
        genXML.order = EP_OutboundMessageServicePlanB.orderStub;
        return genXML.CreateXML();  
    }
    
    private static EP_IntegrationServiceResult doCallOut( EP_OutboundIntegrationService intService , EP_CS_OutboundMessageSetting__c msgSetting) {
        EP_IntegrationServiceResult result = new EP_IntegrationServiceResult();
        if(isCommunicationDisabled(msgSetting)) {
            HttpResponse httpResponse = result.createErrorResponse(EP_Common_Constant.HTTP_ERROR_CODE_503,EP_Common_Constant.COMMUNICATION_DISABLE_MSG_JSON);
            result.setResponse(httpResponse); 
        } else {
            result = performImmediateCallouts(intService,msgSetting);
        }
        return result;
    }
    
    public static boolean isCommunicationDisabled(EP_CS_OutboundMessageSetting__c msgSetting){
        EP_CS_Communication_Settings__c communicationSettings = EP_CS_Communication_Settings__c.getValues(EP_Common_Constant.DISABLE_OUTBOUND_COMMUNICATIONS);
        return (msgSetting.Disable_Communications__c || communicationSettings.Disable__c);
    }
    
    private static Boolean isOrderPriceReady(Order orderObject){
        return true;
    }
    private static EP_IntegrationServiceResult performImmediateCallouts(EP_OutboundIntegrationService intService , EP_CS_OutboundMessageSetting__c msgSetting){
        EP_IntegrationServiceResult result = new EP_IntegrationServiceResult();
        Integer numberOfRetries = Integer.valueOf(msgSetting.EP_Immediate_Retries__c == null || !msgSetting.EP_Perform_Immediate_Retry__c ? 1 : msgSetting.EP_Immediate_Retries__c);
        
        for(integer iCount =0;iCount<numberOfRetries;iCount++){
            attempts = iCount + 1;
            result = intService.invokeRequest();
            string statusCode = string.valueOf(result.getResponse().getStatusCode());
            if(EP_OutboundIntegrationService.isStatusCodeValid(statusCode,msgSetting.EP_Success_Status_Codes__c) ) {
                break;
            }
        }
        return result;
    }
    

}