/*********************************************************************************************
            @Author <>
            @name <TQUploadService >
            @CreateDate <>
            @Description <>  
            @Version <1.0>
    *********************************************************************************************/ 
public without sharing class TQUploadService{
    public static final String LOCALIDPREFFIX = 'Local_';
    public static final String DELETE_OPERATION = 'DELETE';
    public static final String INSERT_OPERATION = 'INSERT';
    public static final String UPDATE_OPERATION = 'UPDATE';
    private static final String DEFAULTAPPID = 'TquilaONE';
    
    public final String ClientAppId;

    public static final String REC_ID = 'Id';
    public static final String ISDELETED = 'isDeleted';
    public static final String SELECT_QUERY = 'SELECT ';
    public static final String FROM_QUERY = ' FROM ';

/*********************************************************************************************
            @Author <>
            @name <TQUploadService >
            @CreateDate <>
            @Description <>  
            @Version <1.0>
    *********************************************************************************************/ 
        public TQUploadService(){
        this(DEFAULTAPPID);
    } 
 /*********************************************************************************************
            @Author <>
            @name < TQUploadService>
            @CreateDate <>
            @Description <>  
            @Version <1.0>
    *********************************************************************************************/    
    public TQUploadService(String clientAppId){
        this.ClientAppId = clientAppId;
    } 
    
    //Make List of strings unique removing all the repetitive elements
    @TestVisible
/*********************************************************************************************
            @Author <>
            @name <TQUploadService >
            @CreateDate <>
            @Description <>  
            @Version <1.0>
    *********************************************************************************************/     
    private List<String> getUniqueArray(List<String> stringList){
        List<String> resultList = new List<String>();
        Set<String> stringSet = new Set<String>();
        
        for (String val : stringList){
            //If element was not added to resulting array before
            if (!stringSet.contains(val)) {
                resultList.add(val);
                stringSet.add(val);
            }
        }
        
        return resultList;
    }
    
/*********************************************************************************************
            @Author <>
            @name <isLocalId>
            @CreateDate <>
            @Description <Recognize if sObject Id is local>  
            @Version <1.0>
    *********************************************************************************************/    
    public Boolean isLocalId(String id){
        return id.startsWith(LOCALIDPREFFIX);
    }
 
/*********************************************************************************************
            @Author <>
            @name <getOperationType>
            @CreateDate <>
            @Description <Recognize which DML operation should be done with object: insert, update or delete>  
            @Version <1.0>
    *********************************************************************************************/    
    public String getOperationType(sObject record){
        String recordId = (String)record.get(REC_ID);
        Schema.Describesobjectresult objDescribe = record.getSObjectType().getDescribe();
        
        if (objDescribe.isDeletable() && record.get(ISDELETED) != null && record.get(ISDELETED) == true){        
            return DELETE_OPERATION;
        }else if (isLocalId(recordId)) {
            return INSERT_OPERATION;
        }else { 
            return UPDATE_OPERATION;}
    }
    
/*********************************************************************************************
            @Author <>
            @name <reassignLocalReferences>
            @CreateDate <>
            @Description <Go through the all reference fields of sObject to replace all local Ids with all ids inserted before>  
            @Version <1.0>
    *********************************************************************************************/        
    public Boolean reassignLocalReferences(sObject recordObject, Map<String, String> assignmentMap){
    
    System.debug('====recordObject==123=='+recordObject);
        Boolean assignmentComplete = true;
        String sObjectId = (String)recordObject.get(REC_ID);
        recordObject.put(REC_ID, null);
        Schema.DescribeSObjectResult objectDescribe = recordObject.getSObjectType().getDescribe();
        Map<String, Schema.SObjectField> fieldDescribeMap = objectDescribe.fields.getMap();
        Type objectType = Type.forName(objectDescribe.getName());

        for (String fieldName : fieldDescribeMap.keySet()) {
        
        System.debug('====fieldName===='+fieldName);
            Schema.DescribeFieldResult fieldDescribe = fieldDescribeMap.get(fieldName).getDescribe();
            
            //Check if field is Lookup or Parent-Child field to replace it
            if (fieldName != REC_ID && fieldDescribe.getType() == Schema.Displaytype.Reference){
                Object fieldValue = recordObject.get(fieldName);
                if (fieldValue != null &&  fieldValue instanceof String) {
                    String stringValue = String.valueOf(fieldValue);
                    if (isLocalId(stringValue)){
                        if (assignmentMap.containsKey(stringValue)) {
                            // Replace local Id of Lookup with the actual one
                            String assignedId = assignmentMap.get(stringValue);
                            recordObject.put(fieldName, assignedId);
                        }else {
                            // sObject still does not contains assigned fields so should be processed later
                            assignmentComplete = false;}
                    }
                }
            }
        }
        //recordObject.put('AccountId', '0011F000006EteDQAS');
        recordObject.put(REC_ID, sObjectId);
        System.debug('======recordObject==>>'+recordObject);
        
        return assignmentComplete;
    }
    
  /*********************************************************************************************
            @Author <>
            @name < TQUploadService>
            @CreateDate <>
            @Description <>  
            @Version <1.0>
    *********************************************************************************************/       
    private Map<String, Schema.DescribeFieldResult>getFieldDescriptionMap(String objectApiName){
        Map<String, Schema.DescribeFieldResult> resultMap = new Map<String, Schema.DescribeFieldResult>();
        Schema.SObjectType sObjType = TQTrackingUtils.getGlobalDescribe().get(objectApiName);
        if (sObjType != null){
            Map<String, Schema.SObjectField> fieldMap = sObjType.getDescribe().fields.getMap();
            for (String fieldKey : fieldMap.keySet()) {
                Schema.DescribeFieldResult fieldDescribe = fieldMap.get(fieldKey).getDescribe();
                resultMap.put(fieldDescribe.Name, fieldDescribe);
            }
        }
        return resultMap;        
    }
/*********************************************************************************************
            @Author <>
            @name <deserializeSObject>
            @CreateDate <>
            @Description <Convert JSON string in request item to the sObject>  
            @Version <1.0>
    *********************************************************************************************/   
    public sObject deserializeSObject(TQUploadRequestItem requestItem, Map<String, Object> additionalData){
        Map<String, Object> dataMap = (Map<String, Object>)JSON.deserializeUntyped(requestItem.record);
        
        system.debug('==dataMap=='+ dataMap);
        
        Map<String, Blob> blobDataMap = new Map<String, Blob>();
        
        Map<String, Schema.DescribeFieldResult> fieldDescriptionMap = getFieldDescriptionMap(requestItem.objectApiName);
        system.debug('==fieldDescriptionMap=='+ fieldDescriptionMap);
        if (fieldDescriptionMap != null){
            for (String key : dataMap.keySet()){
                Schema.DescribeFieldResult fieldDescribe = fieldDescriptionMap.get(key);
                system.debug('==fieldDescribe=='+fieldDescribe);
                Object fieldValue = dataMap.get(key);
                system.debug('==fieldValue=='+ fieldValue);
                if (fieldDescribe != null && fieldDescribe.getType() == Schema.Displaytype.base64 &&
                    fieldValue != null && fieldValue instanceof String){
                    Blob DataBlob = EncodingUtil.base64Decode((String) fieldValue);
                    blobDataMap.put(key, DataBlob);
                        system.debug('==blobDataMap=='+ blobDataMap);
                        system.debug('==dataMap==org=='+ dataMap);
                    dataMap.remove(key);
                         system.debug('==blobDataMap===latest='+ dataMap);
                }
                
                if (fieldDescribe == null) {
                     system.debug('==dataMap==org=111='+ dataMap);
                    dataMap.remove(key);
                     system.debug('==blobDataMap===latest=22222'+ dataMap);
                }
            }
        }
        
        String cleanSObjectString = JSON.serialize(dataMap);
        system.debug('==cleanSObjectString=='+ cleanSObjectString);
        Type objectType = Type.forName(requestItem.objectApiName);
        sObject resultObject = (sObject)JSON.deserializeStrict(cleanSObjectString, objectType);
         system.debug('==resultObject=='+ resultObject);
        if (blobDataMap.size() > 0){
            Object objectId = resultObject.get(REC_ID);
            resultObject.put(REC_ID, null);
            
            for (String key : blobDataMap.keySet()){
                resultObject.put(key, blobDataMap.get(key));}
                
            if (objectId != null){
                resultObject.put(REC_ID, objectId);}
        }
        
        return resultObject;
    }
/*********************************************************************************************
            @Author <>
            @name < >
            @CreateDate <>
            @Description <>  
            @Version <1.0>
*********************************************************************************************/    
    public Map<String, String> getUnresolvedRelatedFields(sObject recordObject){    
        Map<String, String> unresolvedFields = new Map<String, String>();
            
        Schema.DescribeSObjectResult objectDescribe = recordObject.getSObjectType().getDescribe();
        Map<String, Schema.SObjectField> fieldDescribeMap = objectDescribe.fields.getMap();
        Type objectType = Type.forName(objectDescribe.getName());

        for (String fieldName : fieldDescribeMap.keySet()) {
            Schema.DescribeFieldResult fieldDescribe = fieldDescribeMap.get(fieldName).getDescribe();
            
            //Check if field is Lookup or Parent-Child field to replace it
            if (fieldName != REC_ID && fieldDescribe.getType() == Schema.Displaytype.Reference){
                Object fieldValue = recordObject.get(fieldName);
                if (fieldValue != null &&  fieldValue instanceof String) {
                    String stringValue = String.valueOf(fieldValue);
                    if (isLocalId(stringValue)){
                        unresolvedFields.put(fieldName, fieldDescribe.getLabel());
                    }
                }
            }
        }       
        
        return unresolvedFields;
    }
/*********************************************************************************************
            @Author <>
            @name < getAllFieldSet >
            @CreateDate <>
            @Description <>  
            @Version <1.0>
*********************************************************************************************/     
    public List<String> getAllFieldSet(String objectApiName){
        List<String> fullFieldSet = new List<String>();
        Schema.SObjectType sObjType = TQTrackingUtils.getGlobalDescribe().get(objectApiName);
        if(sObjType != null) {
            Map<String, Schema.SObjectField> fieldMap = sObjType.getDescribe().fields.getMap();
            for (String fieldKey : fieldMap.keySet()) {
                Schema.DescribeFieldResult fieldDescribe = fieldMap.get(fieldKey).getDescribe();
                if(fieldDescribe != null && fieldDescribe.isAccessible()) {
                    fullFieldSet.add(fieldDescribe.getName());
                }
            }
        }
        return fullFieldSet;
    }
    
/*********************************************************************************************
            @Author <>
            @name <deserializeSObject>
            @CreateDate <>
            @Description <Fetch new sObject data after DML operation was finished successfully>  
            @Version <1.0>
    *********************************************************************************************/     
    public sObject getResultSObject(String objectApiName, String objectId){
        List<String> fieldList = getAllFieldSet(objectApiName);
        if (fieldList == null || fieldList.size() == 0){
            fieldList = new List<String>{REC_ID};
        }
        
        //Prepare list of select fields in query
        String fieldsString = '';
        for (String fieldName : fieldList){
            if (fieldsString != ''){
                fieldsString += ', ';}
            fieldsString += fieldName;
        }
        
        String query = SELECT_QUERY + fieldsString + FROM_QUERY + objectApiName + ' WHERE Id = \'' + objectId + '\'';
            
        List<sObject> resultObjects = Database.query(query);
        sObject resultObject = (resultObjects.size() > 0) ? resultObjects.get(0) : null;
        
        if (resultObject != null) {
            for (String fieldName : fieldList){
                try {
                    if (resultObject.get(fieldName) == null){
                        resultObject.put(fieldName, null);
                    }
                }
                catch (Exception e){
                    boolean exec = true;
                }
            }
        }
        return resultObject;
    }
}