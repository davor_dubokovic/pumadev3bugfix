/*
   @Author          CloudSense
   @Name            CreateCreditExceptionHandler
   @CreateDate      02/07/2017
   @Description     This class is responsible for initiating the Credit Exception Callout in case of Credit check failure on Order
   @Version         1.1
 
*/

global class CreateCreditExceptionHandler implements CSPOFA.ExecutionHandler{
    
    /**
    * @Author       CloudSense
    * @Name         execute
    * @Date         02/09/2017
    * @Description  Method to process the step and and call Credit Exception Outbound service
    * @Param        list<SObject>
    * @return       NA
    */  
    public List<sObject> process(List<SObject> data)
    {
        system.debug('mategr orch 1');
        List<sObject> result = new List<sObject>();
        List<EP_Credit_Exception_Request__c> crList = new List<EP_Credit_Exception_Request__c>();
        //collect the data for all steps passed in, if needed
        List<CSPOFA__Orchestration_Step__c> stepList= (List<CSPOFA__Orchestration_Step__c>)data;
        Map<Id,CSPOFA__Orchestration_Step__c> stepMap = new Map<Id,CSPOFA__Orchestration_Step__c>();
        List<Id> orderIdList = new List<Id>();
        Map<Id,Id> orderIdCrIdMap = new Map<Id,Id>();
        Map<Id,EP_Credit_Exception_Request__c> creditExceptionMap = new Map<Id,EP_Credit_Exception_Request__c>();
        system.debug('mategr orch 2');
        system.debug('StepList is :' +stepList);
        
        List<CSPOFA__Orchestration_Step__c> extendedList = [Select
                                                                id,CSPOFA__Orchestration_Process__r.Order__c,CSPOFA__Orchestration_Process__r.Order__r.csord__Account__c,CSPOFA__Status__c,CSPOFA__Completed_Date__c,CSPOFA__Message__c, CSPOFA__Orchestration_Process__r.Order__r.csord__Account__r.EP_AvailableFunds__c,
                                                                CSPOFA__Orchestration_Process__r.Order__r.TotalAmount__c, CSPOFA__Orchestration_Process__r.Order__r.csord__Account__r.CurrencyIsoCode, CSPOFA__Orchestration_Process__r.Order__r.CreatedBy.FederationIdentifier,
                                                                CSPOFA__Orchestration_Process__r.Order__r.csord__Account__r.EP_Overdue_Amount__c, CSPOFA__Orchestration_Process__r.Order__r.csord__Account__r.EP_Balance__c,
                                                                CSPOFA__Orchestration_Process__r.Order__r.EP_Order_Comments__c,CSPOFA__Orchestration_Process__r.Order__r.EP_Is_Credit_Exception_Cancelled_Sync__c,CSPOFA__Orchestration_Process__r.Order__r.Cancellation_Check_Done__c
                                                            from 
                                                                CSPOFA__Orchestration_Step__c 
                                                            where 
                                                            id in :stepList];
                                                            
         if (!extendedList.isEmpty()) {
             for(CSPOFA__Orchestration_Step__c step:extendedList){
                 orderIdList.add(step.CSPOFA__Orchestration_Process__r.Order__c);
                 }   
             }
        
         if(orderIdList != null){
           crList =[SELECT 
                        id,EP_Bill_To__c,EP_Current_Order_Value__c,EP_CS_Order__c,EP_Status__c
                    FROM
                        EP_Credit_Exception_Request__c
                    WHERE
                        EP_CS_Order__c in : orderIdList and (EP_Status__c = :system.label.CE_Pending_Approval OR EP_Status__c = :EP_Common_Constant.BLANK)
                                                    
                  ];
        }
        system.debug('CR list is :' +crList);
        if(crList != null && crList.size() >0){
            for(EP_Credit_Exception_Request__c ce :crList){
                orderIdCrIdMap.put(ce.EP_CS_Order__c,ce.id);
                creditExceptionMap.put(ce.id,ce);
            }
        }
        
         
                                                            
        system.debug('extended list is :' +extendedList);
        Id crId;
        for(CSPOFA__Orchestration_Step__c step:extendedList){
            system.debug('CSPOFA__Orchestration_Process__r.Order__c is :' +step.CSPOFA__Orchestration_Process__r.Order__c);
            system.debug('mategr orch 3 EP_Bill_To__c ' + step.CSPOFA__Orchestration_Process__r.Order__r.csord__Account__c);
            Id OrderId = step.CSPOFA__Orchestration_Process__r.Order__c;
            if(crList != null && crList.size()== 0){
                EP_Credit_Exception_Request__c creditException = new EP_Credit_Exception_Request__c();
                creditException.EP_Available_Funds__c= step.CSPOFA__Orchestration_Process__r.Order__r.csord__Account__r.EP_AvailableFunds__c;
                creditException.EP_Bill_To__c = step.CSPOFA__Orchestration_Process__r.Order__r.csord__Account__c;
                creditException.EP_Current_Order_Value__c = step.CSPOFA__Orchestration_Process__r.Order__r.TotalAmount__c;
                creditException.EP_CS_Order__c =step.CSPOFA__Orchestration_Process__r.Order__c;
                creditexception.CurrencyIsoCode = step.CSPOFA__Orchestration_Process__r.Order__r.csord__Account__r.CurrencyIsoCode;
                creditexception.EP_SenderID__c = step.CSPOFA__Orchestration_Process__r.Order__r.CreatedBy.FederationIdentifier;
                if(step.CSPOFA__Orchestration_Process__r.Order__r.csord__Account__r.EP_Overdue_Amount__c > 0){
                  creditException.EP_Overdue_Invoices__c = true;
                }else{
                  creditException.EP_Overdue_Invoices__c=false;
                }
                
                creditException.EP_Status__c=EP_Common_Constant.BLANK; 
                creditException.EP_Request_Date__c = Date.today();
                creditException.EP_Total_Value_of_Open_Orders_in_SFDC__c = step.CSPOFA__Orchestration_Process__r.Order__r.csord__Account__r.EP_Balance__c;
                creditexception.EP_Comment__c = step.CSPOFA__Orchestration_Process__r.Order__r.EP_Order_Comments__c;
                crList.add(creditException);
                stepMap.put(step.id,step);
                orderIdList.add(step.CSPOFA__Orchestration_Process__r.Order__c);
                system.debug('Order Id is :' +step.CSPOFA__Orchestration_Process__r.Order__c) ;
                
            }
            //mark step Status, Completed Date, and write optional step Message
            step.CSPOFA__Status__c ='Complete';
            step.CSPOFA__Completed_Date__c=Date.today();
            step.CSPOFA__Message__c = 'Custom step succeeded';
            result.add(step);
          
            system.debug(' final result is :' +result);
          
      }
      try{
            system.debug('mategr orch 5');
            if(!crList.isEmpty()){
                system.debug('mategr orch 6');
                database.insert(crList);
            }
        }Catch(Exception exp){
            system.debug('mategr orch 7');
         EP_loggingService.loghandledException(exp, EP_Common_Constant.EPUMA, 'process', 'CreateCreditExceptionHandler',apexPages.severity.ERROR);
        }
      return result; 
   }
}