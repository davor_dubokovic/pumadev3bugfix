/**
 * @name <EP_UserTriggerHandler_R1>
 * @description <This class handles requests from User trigger> 
 * @version <1.0>
 */
public with sharing class EP_UserTriggerHandler_R1 {
    //switches to control execution of trigger
    public static Boolean isExecuteAfterUpdate = TRUE; 
    public static Boolean isExecuteBeforeUpdate = TRUE; 
    public static Boolean isExecuteAfterInsert = TRUE; 
    public static Boolean isExecuteBeforeInsert = TRUE; 
    
    /* 
     *Call on before insert
     */
    public static void doBeforeInsert(List<User> lNewUsers) {
        EP_UserTriggerHelper_R1.calculateUserTimeZoneUTCOffset(lNewUsers);
    }
    
    /* 
     *Call on before update
     */
    public static void doBeforeUpdate(List<User> lOldUsers) {
        EP_UserTriggerHelper_R1.calculateUserTimeZoneUTCOffset(lOldUsers);
    }
    
     /* 
     *Call on after insert
     */
    public static void doAfterInsert(List<User> lNewUsers) {
        // Portal User Sharing Rule Change - Start
        EP_AccountSharingHandler.managePortalUserAccountSharingRulesOnUserCreation(lNewUsers);
        // Portal User Sharing Rule Change - End
    }
    
    /* 
     *Call on after update
     */
    public static void doAfterUpdate(List<User> lOldUsers) {
        // Portal User Sharing Rule Change - Start
        EP_AccountSharingHandler.managePortalUserAccountSharingRulesOnUserCreation(lOldUsers);
        // Portal User Sharing Rule Change - End
    }
    
}