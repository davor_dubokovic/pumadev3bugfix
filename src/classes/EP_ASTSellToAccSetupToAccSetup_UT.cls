@isTest
public class EP_ASTSellToAccSetupToAccSetup_UT
{
    static final String VALID_EVENT = '04-AccountSet-upTo04-AccountSet-up';
    static final String INVALID_EVENT = '03-ProspectTo05-AccountSet-up';

    /*  
       @description: method to intialise data
    */
    @testSetup static void init() {
      List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
      List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }
    static testMethod void isTransitionPossible_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASAccountSetupDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(VALID_EVENT);
        EP_ASTSellToAccountSetupToAccountSetup ast = new EP_ASTSellToAccountSetupToAccountSetup();
        delete [SELECT Id From EP_Action__c];
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isTransitionPossible_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASAccountSetupDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT);
        EP_ASTSellToAccountSetupToAccountSetup ast = new EP_ASTSellToAccountSetupToAccountSetup();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isRegisteredForEvent_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASAccountSetupDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(VALID_EVENT);
        EP_ASTSellToAccountSetupToAccountSetup ast = new EP_ASTSellToAccountSetupToAccountSetup();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isRegisteredForEvent_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASAccountSetupDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT);
        EP_ASTSellToAccountSetupToAccountSetup ast = new EP_ASTSellToAccountSetupToAccountSetup();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(false,result);
    }

    static testMethod void isGuardCondition_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASAccountSetupDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(VALID_EVENT);
        EP_ASTSellToAccountSetupToAccountSetup ast = new EP_ASTSellToAccountSetupToAccountSetup();
        delete [SELECT Id From EP_Action__c];
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    //No longer required as guardian is always returning true
    /*static testMethod void isGuardCondition_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASAccountSetupDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT);
        EP_ASTSellToAccountSetupToAccountSetup ast = new EP_ASTSellToAccountSetupToAccountSetup();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(false,result);
    }*/
}