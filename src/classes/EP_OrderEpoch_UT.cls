/**
  * @Author      : Accenture
  * @name        : EP_OrderEpoch_UT
  * @CreateDate  : 3/20/2017
  * @Description : Test class for EP_OrderEpoch
  * @Version     : <1.0>
  * @reference   : N/A
*/
@isTest
private class EP_OrderEpoch_UT {
    /**
      * @name        : getShipTos_PassingListShipToAccount
      * @CreateDate  : 3/20/2017
      * @Description : This method check return shipTos , passing all ship to accounts
    */
    static testMethod void getShipTos_Positivetest() {
        EP_OrderEpoch localObj = new EP_OrderEpoch();
        EP_ProductSoldAs productSoldAsType = new EP_ProductSoldAs();
        List<Account> listShiptosAccounts = new List<Account>{EP_TestDataUtility.getShipTo()};
        
        Test.startTest();
        List<Account> result = localObj.getShipTos(listShiptosAccounts,productSoldAsType);
        Test.stopTest();
        
        System.assert(result.size() > 0);       
    }
    
    static testMethod void isValidLoadingDate_postivetest() {
        EP_OrderEpoch localObj = new EP_OrderEpoch();
        csord__Order__c ord = EP_TestDataUtility.getOrderEpochPositiveScenario();
        Test.startTest();
        Boolean result = localObj.isValidLoadingDate(ord);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isValidLoadingDate_negativetest() {
        EP_OrderEpoch localObj = new EP_OrderEpoch();
        csord__Order__c ord = EP_TestDataUtility.getOrderEpochNegativeScenario();
        Test.startTest();
        Boolean result = localObj.isValidLoadingDate(ord);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isValidExpectedDate_postivetest() {
        EP_OrderEpoch localObj = new EP_OrderEpoch();
        csord__Order__c ord = EP_TestDataUtility.getOrderEpochPositiveScenario();
        Test.startTest();
        Boolean result = localObj.isValidExpectedDate(ord);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isValidExpectedDate_negativetest() {
        EP_OrderEpoch localObj = new EP_OrderEpoch();
        csord__Order__c ord = EP_TestDataUtility.getOrderEpochNegativeScenario();
        Test.startTest();
        Boolean result = localObj.isValidExpectedDate(ord);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    static testMethod void isValidOrderDate() {
        EP_OrderEpoch localObj = new EP_OrderEpoch();
        csord__Order__c ord = EP_TestDataUtility.getOrderEpochPositiveScenario();
        Test.startTest();
        Boolean result = localObj.isValidOrderDate(ord);
        localObj.setRetroOrderDetails(ord);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    static testMethod void setRequestedDateTime() {
        EP_OrderEpoch localObj = new EP_OrderEpoch();
        csord__Order__c ord = EP_TestDataUtility.getOrderEpochPositiveScenario();
        Test.startTest();
        csord__Order__c objOrder = localObj.setRequestedDateTime(ord, '10/10/2017','Slot');
        EP_VendorManagement vendorManagment = new EP_VendorManagement();
        EP_ConsignmentType consigmentType = new EP_ConsignmentType();
        localObj.findRecordType(vendorManagment,consigmentType);
        Test.stopTest();
        System.Assert(objOrder != null);
    }
    
    
    
}