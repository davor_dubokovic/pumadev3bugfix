@isTest
public class EP_OrderMapper_UT {
    
    public static testMethod void getOpenOrderOfTransporters_test() {
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
        insert fm;
        Account newAccount = Ep_TestDataUtility.getSellTo();
        
        Id recordTypeId = EP_Common_Util.fetchRecordTypeId('Order', 'VMI Orders');
        
        csord__Order__c newOrder = EP_TestDataUtility.createCSOrder(newAccount.Id, recordTypeId, null);
        insert newOrder;
        
        Set<id> idSet = new Set<id>{newOrder.EP_Transporter__c};
        Set<string> stringSet = new Set<String>{EP_OrderConstant.OrderState_Draft};
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            List<Order> result = localObj.getOpenOrderOfTransporters(idSet,stringSet);
        Test.stopTest();
        
        System.Assert(true,result.size()==1);
    }
    
     public static testMethod void getRecordsByIds_test() {
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
        insert fm;
        Account newAccount = Ep_TestDataUtility.getSellTo();
        
        Id recordTypeId = EP_Common_Util.fetchRecordTypeId('Order', 'VMI Orders');
        
        csord__Order__c newOrder = EP_TestDataUtility.createCSOrder(newAccount.Id, recordTypeId, null);
        insert newOrder;
        
        Set<id> idSet = new Set<id>{newOrder.id};
        //Set<string> stringSet = new Set<String>{EP_OrderConstant.orderId};
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            List<Order> result = localObj.getRecordsByIds(idSet);
        Test.stopTest();
        
        System.Assert(true,result.size()==1);
    }
    
    public static testMethod void getRecordsByOrderNumber_test() {
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
        insert fm;
        Account newAccount = Ep_TestDataUtility.getSellTo();
        
        Id recordTypeId = EP_Common_Util.fetchRecordTypeId('Order', 'VMI Orders');
        
        csord__Order__c newOrder = EP_TestDataUtility.createCSOrder(newAccount.Id, recordTypeId, null);
        insert newOrder;
        
        //Set<id> idSet = new Set<id>{newOrder.EP_Transporter__c};
        Set<string> stringSet = new Set<String>{EP_OrderConstant.ordNum};
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            List<Order> result = localObj.getRecordsByOrderNumber(stringSet);
        Test.stopTest();
        
        System.Assert(true,result.size()==1);
    }
    
   
    
    public static testMethod void getOrderListWithStandardOrderItemsDetails_test() {
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
        insert fm;
        Account newAccount = Ep_TestDataUtility.getSellTo();
        
        Id recordTypeId = EP_Common_Util.fetchRecordTypeId('Order', 'VMI Orders');
        
        csord__Order__c newOrder = EP_TestDataUtility.createCSOrder(newAccount.Id, recordTypeId, null);
        insert newOrder;
        Set<Id> mOrder = new Set<id>{newOrder.id};
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            LIST<Order> result = localObj.getOrderListWithStandardOrderItemsDetails(mOrder);
        Test.stopTest();
        
        //System.Assert(result.size() > 0);
    }
    public static testMethod void getOrderPricing_test() {
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
        insert fm;
        Account newAccount = Ep_TestDataUtility.getSellTo();
        
        Id recordTypeId = EP_Common_Util.fetchRecordTypeId('Order', 'VMI Orders');
        
        csord__Order__c newOrder = EP_TestDataUtility.createCSOrder(newAccount.Id, recordTypeId, null);
        insert newOrder;
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            Order result = localObj.getOrderPricing(newOrder.Id);
        Test.stopTest();
        
        System.Assert(result <> null);
    }
    
    public static testMethod void getRecordById_test() {
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
        insert fm;
        Account newAccount = Ep_TestDataUtility.getSellTo();
        
        Id recordTypeId = EP_Common_Util.fetchRecordTypeId('Order', 'VMI Orders');
        
        csord__Order__c newOrder = EP_TestDataUtility.createCSOrder(newAccount.Id, recordTypeId, null);
        insert newOrder;
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            Order result = localObj.getRecordById(newOrder.Id);
        Test.stopTest();
        
        System.Assert(result <> null);
    }
    
    public static testMethod void getBulkOrdersForAccount_test() {
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
        insert fm;
        Account newAccount = Ep_TestDataUtility.getSellTo();
        //system.debug('newAccount~~'+newAccount);
        //
        
        Id recordTypeId = EP_Common_Util.fetchRecordTypeId('Order', 'VMI Orders');
        
        csord__Order__c newOrder = EP_TestDataUtility.createCSOrder(newAccount.Id, recordTypeId, null);
        newOrder.EP_Order_Product_Category__c = EP_Common_Constant.PRODUCT_BULK;
        insert newOrder;
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            List<Order> result = localObj.getBulkOrdersForAccount(newAccount.Id);
        Test.stopTest();
        
        //System.Assert(result.size() > 0);
    }
    public static testMethod void getOrderByAccountAndProductCategory_test() {
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
        insert fm;
        Account newAccount = Ep_TestDataUtility.getSellTo();
        
        
        Id recordTypeId = EP_Common_Util.fetchRecordTypeId('Order', 'VMI Orders');
        
        csord__Order__c newOrder = EP_TestDataUtility.createCSOrder(newAccount.Id, recordTypeId, null);
        newOrder.EP_Order_Product_Category__c = EP_Common_Constant.PRODUCT_BULK;
        insert newOrder;
        /*
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            List<csord__Order__c> result = localObj.getOrderByAccountAndProductCategory(newAccount.Id, newOrder.EP_Order_Product_Category__c, new Set<String>{newOrder.Status});
        Test.stopTest();
        
        System.Assert(result.size() == 0);*/
    }
    Public static testMethod void getCsOrderSumAmountbyBillTo_test() {
        EP_OrderMapper localObj = new EP_OrderMapper();
        csord__Order__c OrderObj = EP_TestDataUtility.getSalesOrderPositiveScenario();
        Test.startTest();
        List<AggregateResult> result = localObj.getCSOrderSumAmountbyBillTo(OrderObj);
        Test.stopTest();
        System.Assert(result.size() == 0);
    }
    Public static testMethod void getCsOrderWithStandardItems_test() {
        EP_OrderMapper localObj = new EP_OrderMapper();
        Id orderId = EP_TestDataUtility.getSalesOrderPositiveScenario().id;
        csord__Order__c orderObj = [SELECT Id,OrderNumber__c FROM csord__Order__c WHERE ID =: orderId];
        Test.startTest();
        map<string, csord__Order__c> result = localObj.getCSOrderWithStandardItems(new set<string>{orderObj.OrderNumber__c});
        Test.stopTest();
        System.Assert(result.size() == 1);
    }
    
    public static testMethod void getCSRecordsByIds_test() {
        Id orderId = EP_TestDataUtility.getSalesOrderPositiveScenario().id;
        csord__Order__c orderObj = [SELECT Id FROM csord__Order__c WHERE ID =: orderId];
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            List<csord__Order__c> result = localObj.getCSRecordsByIds(new Set<Id>{orderObj.Id});
        Test.stopTest();
        
        System.Assert(result.size() == 1);
    }
    
    public static testMethod void getCsRecordsByOrderNumber_test() {
        Id orderId = EP_TestDataUtility.getSalesOrderPositiveScenario().id;
        csord__Order__c orderObj = [SELECT Id,OrderNumber__c FROM csord__Order__c WHERE ID =: orderId];
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            List<csord__Order__c> result = localObj.getCsRecordsByOrderNumber(new Set<String>{orderObj.OrderNumber__c});
        Test.stopTest();
        
        System.Assert(result.size() == 1);
    }
    
    public static testMethod void getCSOrderPricing_test() {
        Id orderId = EP_TestDataUtility.getSalesOrderPositiveScenario().id;
        csord__Order__c orderObj = [SELECT Id FROM csord__Order__c WHERE ID =: orderId];
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            csord__Order__c result = localObj.getCSOrderPricing(orderObj.Id);
        Test.stopTest();
        
        System.Assert(result != null);
    }
    
    public static testMethod void getOrderWithStandardItems_test() {
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
        insert fm;
        Account newAccount = Ep_TestDataUtility.getSellTo();
        
        
        Id recordTypeId = EP_Common_Util.fetchRecordTypeId('Order', 'VMI Orders');
        
        csord__Order__c newOrder = EP_TestDataUtility.createCSOrder(newAccount.Id, recordTypeId, null);
        insert newOrder;
        csord__Order__c order = [SELECT Id,OrderNumber__c FROM csord__Order__c WHERE Id =: newOrder.Id];
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            Map<string, Order> result = localObj.getOrderWithStandardItems(new Set<String>{order.OrderNumber__c});
        Test.stopTest();
        
        //System.Assert(result.size() == 1);
    }
    
    public static testMethod void getOrderMapByOrderNumber_test() {
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
        insert fm;
        Account newAccount = Ep_TestDataUtility.getSellTo();
        
        
        Id recordTypeId = EP_Common_Util.fetchRecordTypeId('Order', 'VMI Orders');
        
        csord__Order__c newOrder = EP_TestDataUtility.createCSOrder(newAccount.Id, recordTypeId, null);
        insert newOrder;
        /*csord__Order__c order = [SELECT Id, OrderNumber FROM csord__Order__c WHERE Id =: newOrder.Id];
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            Map<string, Order> result = localObj.getOrderMapByOrderNumber(new Set<String>{order.OrderNumber});
        Test.stopTest();*/
        
        //System.Assert(result.size() == 1);
    }
    
    public static testMethod void getCsOrderMapByOrderNumber_test() {
        Id orderId = EP_TestDataUtility.getSalesOrderPositiveScenario().id;
        csord__Order__c orderObj = [SELECT Id, OrderNumber__c FROM csord__Order__c WHERE ID =: orderId];
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            Map<String, csord__Order__c> result = localObj.getCsOrderMapByOrderNumber(new Set<String>{orderObj.OrderNumber__c});
        Test.stopTest();
        
        System.Assert(result.size() == 1);
    }
    
    public static testMethod void getOrderPricingwithorderitems_test() {
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
        insert fm;
        Account newAccount = Ep_TestDataUtility.getSellTo();
        
        
        Id recordTypeId = EP_Common_Util.fetchRecordTypeId('Order', 'VMI Orders');
        
        csord__Order__c newOrder = EP_TestDataUtility.createCSOrder(newAccount.Id, recordTypeId, null);
        insert newOrder;
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            Order result = localObj.getOrderPricingwithorderitems(newOrder.Id);
        Test.stopTest();
        
        System.Assert(result != null);
    }
    /* static testMethod void getCsOrderByAccountAndProductCategory_test() {
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
        insert fm;
        //Account newAccount = Ep_TestDataUtility.getSellTo();
        Account newAccount = Ep_TestDataUtility.getSellTo();
        
        
        Id recordTypeId = EP_Common_Util.fetchRecordTypeId('Order', 'VMI Orders');
        
        csord__Order__c orderObj = EP_TestDataUtility.getSalesOrderPositiveScenario();
        
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            List<csord__Order__c> result = localObj.getCsOrderByAccountAndProductCategory(newAccount.Id, orderObj.EP_Order_Product_Category__c, new Set<String>{orderObj.Status__c});
        Test.stopTest();
        
        System.Assert(result.size() == 0);
    }*/
    
    public static testMethod void getAcceptedOrderByOrderNumber_test() {
        Id orderId = EP_TestDataUtility.getSalesOrderPositiveScenario().id;
        csord__Order__c orderObj = [SELECT Id, OrderNumber__c FROM csord__Order__c WHERE ID =: orderId];
        orderObj.csord__Status2__c = 'Accepted';
        update orderObj;
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            Map<String, csord__Order__c> result = localObj.getAcceptedOrderByOrderNumber(new Set<String>{orderObj.OrderNumber__c});
        Test.stopTest();
        
        System.Assert(result.size() == 1);
    }
    
    public static testMethod void getOrderSumAmountbyBillTo_test() {
        EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
        insert fm;
        Account newAccount = Ep_TestDataUtility.getSellTo();
        //
        
        Id recordTypeId = EP_Common_Util.fetchRecordTypeId('Order', 'VMI Orders');
        
        csord__Order__c newOrder = EP_TestDataUtility.createCSOrder(newAccount.Id, recordTypeId, null);
        insert newOrder;
        /*
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
            List<AggregateResult> result = localObj.getOrderSumAmountbyBillTo(newOrder);
        Test.stopTest();
        
        System.Assert(result.size() == 0);*/
    }
    Public static testMethod void getDeliveryDockets_test() {
        EP_OrderMapper localObj = new EP_OrderMapper();
        Id orderId = EP_TestDataUtility.getSalesOrderPositiveScenario().id;
        csord__Order__c orderObj = [SELECT Id,EP_Delivery_Reference_Number__c FROM csord__Order__c WHERE ID =: orderId];
        
        Set<string> idSet = new Set<string>();
        idset.add(String.valueOf(orderObj.EP_Delivery_Reference_Number__c));
        Test.startTest();
        localObj.getDeliveryDockets(idSet);
        Test.stopTest();
    }
	 public static testMethod void getCSCERecordsById_test() {
        Id orderId = EP_TestDataUtility.getSalesOrderPositiveScenario().id;
        csord__Order__c orderObj = [SELECT Id, OrderNumber__c FROM csord__Order__c WHERE ID =: orderId];
        Id ordid=orderObj.id;
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
           localObj.getCSCERecordsById(ordid);
        Test.stopTest();
    }
	public static testMethod void getOrdersAsscociatedWithTrip_test() {
        Id orderId = EP_TestDataUtility.getSalesOrderPositiveScenario().id;
        csord__Order__c orderObj = [SELECT Id,EP_Order_Load_Code__c  FROM csord__Order__c WHERE ID =: orderId];
        List<String> idlist = new List<String>();
        idlist.add(String.valueOf(orderObj.EP_Order_Load_Code__c ));
        Test.startTest();
            EP_OrderMapper localObj = new EP_OrderMapper();
           EP_OrderMapper.getOrdersAsscociatedWithTrip(idlist);
        Test.stopTest();
    }
    
    public static testMethod void getCsOrderListWithStandardOrderItemsDetails_test() {
         Pricebook2 pricebook = new Pricebook2(Name = 'Standard Price Book', isActive = TRUE);
        INSERT pricebook;
        Product2 prod = new Product2(Name = 'Product');
        INSERT prod;
        System.Assert(prod.Id != null);
        
        PricebookEntry entry = new PricebookEntry();
        entry.UnitPrice = 20;
        entry.Pricebook2Id = pricebook.Id;
        entry.Product2Id = prod.Id;
        INSERT entry;
        System.Assert(entry.Id != null);
        
        Account acc = new Account();
        acc.EP_VendorType__c = '3rd Party Stock Supplier';
        acc.EP_Vendor_Type__c = '3rd Party Stock Supplier';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        acc.Name='testEPAccount';
        acc.EP_Status__c = '06-Blocked';
        insert acc;
        
        csord__Order__c orderObj= EP_TestDataUtility.getSalesOrder();
        orderObj.EP_Error_Product_Codes__c = 'Test';
        update orderObj;
        System.Assert(orderObj.Id != null);
        
        LIST<csord__Order_Line_Item__c> oiList = [SELECT Id, OrderId__c, PricebookEntryId__c FROM csord__Order_Line_Item__c WHERE OrderId__c =:orderObj.Id];
        csord__Order_Line_Item__c lineItem = oiList[0];
        lineItem.PricebookEntryId__c = String.valueOf(entry.Id);
        lineItem.EP_Product__c = prod.Id;
        UPDATE lineItem;
        
        Test.startTest();
        EP_OrderMapper localObj = new EP_OrderMapper();
        List<csord__Order__c> result = localObj.getCsOrderListWithStandardOrderItemsDetails(new Set<Id>{orderObj.Id});
        Test.stopTest();
        
        System.Assert(result.size() > 0, 'Invalid data');
    }
    
}
