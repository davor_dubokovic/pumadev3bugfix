@isTest
public class CutOffMatrixAPI_UT {

    @testSetup
    public static void testSetup() {
        
        CS_ORDER_SETTINGS__c  csOrderSetting = new CS_ORDER_SETTINGS__c();
        csOrderSetting.CutoffMatrixDefaultDepth__c = 5;
        insert csOrderSetting;

        Company__c testCompany = new Company__c();
        testCompany.Name = 'Test Company';
        testCompany.EP_Combined_Invoicing__c = 'Delegate to Customer';
        testCompany.EP_Company_Id__c = 1234;
        testCompany.EP_Company_Code__c = 'Test code';
        testCompany.EP_Window_Start_Hours__c = 8;
        testCompany.KYC_limit_in_KUSD__c = '0';
        testCompany.EP_Advance_Order_Entry_Days__c = 0;
        
        insert testCompany;
        
        Account testAccount = new Account();
        testAccount.Name = 'Test Account';
        testAccount.EP_Puma_Company__c = testCompany.Id;
        testAccount.EP_Balance_LCY__c =100;
        testAccount.EP_CRM_Geo1__c ='NA';
        testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        testAccount.EP_Advance_Order_Entry_Days__c = 0;
        
        insert testAccount;
        
        cscfga__Product_Basket__c testBasket = new cscfga__Product_Basket__c();
        
        testBasket.csordtelcoa__Account__c = testAccount.Id;
        
        insert testBasket;

        Account storageLocation = new Account();
        storageLocation.Name = 'Test Account Storage Location';
        storageLocation.EP_Puma_Company__c = testCompany.Id;
        storageLocation.EP_Balance_LCY__c =100;
        storageLocation.EP_CRM_Geo1__c ='NA';
        storageLocation.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Storage Location').getRecordTypeId();
        
        insert storageLocation;
        
        Id recordTypeVendor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        
        Account testTransporterAccount = new Account();
        testTransporterAccount.Name = 'testTransporterAccount';
        storageLocation.EP_Puma_Company__c = testCompany.Id;
        testTransporterAccount.RecordTypeId = recordTypeVendor;
        testTransporterAccount.EP_Status__c = '05-Active';
        testTransporterAccount.EP_VendorType__c = 'Transporter';
        
        insert testTransporterAccount;
        
        EP_Stock_Holding_Location__c testHoldingLocation = new EP_Stock_Holding_Location__c();
        testHoldingLocation.EP_Transporter__c = testTransporterAccount.Id;
        testHoldingLocation.Stock_Holding_Location__c = storageLocation.Id;

        insert testHoldingLocation;
        
        CutOffMatrix__c cutOffMatrix = new CutOffMatrix__c();
        cutOffMatrix.Delivery_Type__c = 'Delivery';
        cutOffMatrix.Puma_Company__c = testCompany.Id;
        cutOffMatrix.Shift__c = 'AM';
        cutOffMatrix.Time_Zone__c = 'GMT + 5:00';
        cutOffMatrix.Site_Location__c = storageLocation.Id;

        insert cutOffMatrix;

        setupDeliveryDays(cutOffMatrix, storageLocation);
    }
    
    private static void setupDeliveryDays(CutOffMatrix__c matrix, Account location) {
        
        List<DeliveryDay__c> deliveryDays = new List<DeliveryDay__c>();
        
        List<String> days = new List<String> { 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday' };
        
        Integer firstDay = 1;
        
        for (String day :days) {
            
            DeliveryDay__c dd = new DeliveryDay__c();
        
            dd.Cut_Off_Matrix__c = matrix.Id;
            dd.Delivery_Type__c = 'Delivery';
            dd.Site_Location__c = location.Id;
            dd.Order_Day_Name__c = day;
            dd.OrderDayNum__c = firstDay;
            
            dd.Monday__c = Time.newInstance(16, 00, 00, 00);
            dd.Tuesday__c= Time.newInstance(16, 00, 00, 00);
            dd.Wednesday__c = Time.newInstance(16, 00, 00, 00);
            dd.Thursday__c = Time.newInstance(16, 00, 00, 00);
            dd.Friday__c = Time.newInstance(16, 00, 00, 00);
            dd.Saturday__c = Time.newInstance(16, 00, 00, 00);
            dd.Sunday__c = Time.newInstance(16, 00, 00, 00);
        
            deliveryDays.add(dd);
            
            firstDay++;
        }
    
        insert deliveryDays;
        
        Overload_matrix__c overloadMatrix = new Overload_matrix__c();
        overloadMatrix.Name = 'Overload Thursday';
        insert overloadMatrix;
        
        Overload_matrix_entry__c overloadMatrixEntry = new Overload_matrix_entry__c();
        overloadMatrixEntry.Overload_matrix__c = overloadMatrix.Id;
        overloadMatrixEntry.Order_Date__c = Date.newInstance(2018, 10, 25);
        overloadMatrixEntry.Date__c = Date.newInstance(2018, 10, 27);
        overloadMatrixEntry.Time__c = Time.newInstance(12, 00, 00, 00);
        insert overloadMatrixEntry;
        
        matrix.Override_matrix__c = overloadMatrix.Id;
        update matrix;
    }

    private static csord__Order__c refreshOrder(Id orderId) {
        
        // Reload Order in order to fetch related records
        return [SELECT Id, EP_Order_Locked__c, Cancellation_Check_Done__c, EP_CutOff_Check_Required__c, csord__Status2__c, 
                            EP_Order_Epoch__c, EP_Order_Product_Category__c, Stock_Holding_Location__c, Pickup_Supply_Location__c, 
                            EP_Requested_Delivery_Date__c, Delivery_From_Date__c, Delivery_To_Date__c, csord__Account__c, csord__Account__r.EP_Puma_Company__c,
                            csord__Account__r.EP_Advance_Order_Entry_Days__c, csord__Account__r.EP_Puma_Company__r.EP_Advance_Order_Entry_Days__c,
                            EP_Delivery_Type__c
                        FROM csord__Order__c WHERE Id = :orderId LIMIT 1];
    }

    @isTest
    private static void processCutOffMatrixCheckUsingOrder() {
        
        Company__c testCompany = [SELECT Id, Name FROM Company__c WHERE Name = 'Test Company' LIMIT 1];

        Account testAccount = [SELECT Id, Name FROM Account WHERE Name = 'Test Account' LIMIT 1];
        
        cscfga__Product_Basket__c basket = [SELECT Id, Name FROM cscfga__Product_Basket__c LIMIT 1];
        Account storageLocation = [SELECT Id FROM Account WHERE Name = 'Test Account Storage Location' LIMIT 1];
        EP_Stock_Holding_Location__c siteLocation = [SELECT Id, Name, Stock_Holding_Location__c FROM EP_Stock_Holding_Location__c LIMIT 1];

        Test.startTest();
        
        csord__Order__c testOrder = new csord__Order__c();
        testOrder.csord__Identification__c = 'SGH849S4G9AS4ASVA4S9VA';
        testOrder.EP_Delivery_Type__c = 'Delivery';
        testOrder.Delivery_From_Date__c = Date.newInstance(2018, 10, 25);
        testOrder.Delivery_To_Date__c = Date.newInstance(2018, 10, 25);
        testOrder.csord__Account__c = testAccount.Id;
        testOrder.EP_Stock_Holding_Location__c = storageLocation.Id;
        testOrder.Stock_Holding_Location__c = siteLocation.Id;
        testOrder.EP_Order_Product_Category__c = 'Service';
		testOrder.EP_Order_Epoch__c = 'Current';
		testOrder.EP_Requested_Delivery_Date__c = Date.newInstance(2018, 10, 30);
        
        insert testOrder;
        
        // Reload Order in order to fetch related records
        testOrder = refreshOrder(testOrder.Id);
        
        cscfga__Product_Basket__c testBasket = [SELECT Id FROM cscfga__Product_Basket__c LIMIT 1];
        
        // 10/25/2018 = Thursday
        Boolean retval = CutOffMatrixAPI.processCutOffMatrixCheck(testOrder.Id, DateTime.newInstanceGmt(2018, 10, 25, 10, 00, 00), 0, false);
        Boolean retvalOrder = CutOffMatrixAPI.processCutOffMatrixCheckUsingOrder(testOrder, DateTime.newInstanceGmt(2018, 10, 25, 10, 00, 00), 0, false);
        List<CutOffMatrixAPI.ValidationHelper> retvalCheckMatrix = CutOffMatrixAPI.checkCutOffMatrix(testBasket.Id, DateTime.newInstanceGmt(2018, 10, 25, 10, 00, 00), 0, new List<Date> { Date.newInstance(2018, 10, 30) }, storageLocation.Id, testCompany.Id, 'Delivery', false);
        
        List<CutOffMatrixAPI.ValidationHelper> retvalCheckMatrixTraf = TrafiguraConfigurationControllerNew.checkCutOffMatrixNew(testBasket.Id, DateTime.newInstanceGmt(2018, 10, 25, 10, 00, 00), 0, new List<Date> { Date.newInstance(2018, 10, 30) }, storageLocation.Id, testCompany.Id, 'Delivery', false);

        Test.stopTest();
        
        System.assertEquals(true, retval, 'Invalid data');
        System.assertEquals(true, retvalOrder, 'Invalid data');
        System.assertEquals(CutOffMatrixAPI.CutOffReason.VALID , retvalCheckMatrix[0].reason, 'Invalid data');
        System.assertEquals(CutOffMatrixAPI.CutOffReason.VALID , retvalCheckMatrixTraf[0].reason, 'Invalid data');
    }
    
    @isTest
    private static void processCutOffMatrixCheckUsingOrder_from_to_dates() {
        
        Company__c testCompany = [SELECT Id, Name FROM Company__c WHERE Name = 'Test Company' LIMIT 1];

        Account testAccount = [SELECT Id, Name FROM Account WHERE Name = 'Test Account' LIMIT 1];
        
        cscfga__Product_Basket__c basket = [SELECT Id, Name FROM cscfga__Product_Basket__c LIMIT 1];
        Account storageLocation = [SELECT Id FROM Account WHERE Name = 'Test Account Storage Location' LIMIT 1];
        EP_Stock_Holding_Location__c siteLocation = [SELECT Id, Name FROM EP_Stock_Holding_Location__c LIMIT 1];

        Test.startTest();
        
        csord__Order__c testOrder = new csord__Order__c();
        testOrder.csord__Identification__c = 'SGH849S4G9AS4ASVA4S9VA';
        testOrder.EP_Delivery_Type__c = 'Delivery';
        testOrder.Delivery_From_Date__c = Date.newInstance(2018, 10, 30);
        testOrder.Delivery_To_Date__c = Date.newInstance(2018, 10, 30);
        testOrder.csord__Account__c = testAccount.Id;
        testOrder.EP_Stock_Holding_Location__c = storageLocation.Id;
        testOrder.Stock_Holding_Location__c = siteLocation.Id;
        testOrder.EP_Order_Product_Category__c = 'Service';
		testOrder.EP_Order_Epoch__c = 'Current';
		testOrder.EP_Requested_Delivery_Date__c = null;
        
        insert testOrder;
        
        // Reload Order in order to fetch related records
        testOrder = refreshOrder(testOrder.Id);
        
        cscfga__Product_Basket__c testBasket = [SELECT Id FROM cscfga__Product_Basket__c LIMIT 1];
        
        // 10/25/2018 = Thursday
        Boolean retval = CutOffMatrixAPI.processCutOffMatrixCheckUsingOrder(testOrder, DateTime.newInstanceGmt(2018, 10, 25, 10, 00, 00), 0, false);
        List<CutOffMatrixAPI.ValidationHelper> retvalCheckMatrix = CutOffMatrixAPI.checkCutOffMatrix(testBasket.Id, DateTime.newInstanceGmt(2018, 10, 25, 10, 00, 00), 0, new List<Date> { Date.newInstance(2018, 10, 30), Date.newInstance(2018, 10, 30) }, storageLocation.Id, testCompany.Id, 'Delivery', false);

        Test.stopTest();
        
        System.assertEquals(true, retval, 'Invalid data');
        System.assertEquals(CutOffMatrixAPI.CutOffReason.VALID , retvalCheckMatrix[0].reason, 'Invalid data');
    }

    @isTest
    private static void processCutOffMatrixCheckUsingOrder_with_override() {
        
        Company__c testCompany = [SELECT Id, Name FROM Company__c WHERE Name = 'Test Company' LIMIT 1];

        Account testAccount = [SELECT Id, Name FROM Account WHERE Name = 'Test Account' LIMIT 1];
        
        cscfga__Product_Basket__c basket = [SELECT Id, Name FROM cscfga__Product_Basket__c LIMIT 1];
        Account storageLocation = [SELECT Id FROM Account WHERE Name = 'Test Account Storage Location' LIMIT 1];
        EP_Stock_Holding_Location__c siteLocation = [SELECT Id, Name FROM EP_Stock_Holding_Location__c LIMIT 1];

        Test.startTest();
        
        csord__Order__c testOrder = new csord__Order__c();
        testOrder.csord__Identification__c = 'SGH849S4G9AS4ASVA4S9VA';
        testOrder.EP_Delivery_Type__c = 'Delivery';
        testOrder.Delivery_From_Date__c = Date.newInstance(2018, 10, 25);
        testOrder.Delivery_To_Date__c = Date.newInstance(2018, 10, 25);
        testOrder.csord__Account__c = testAccount.Id;
        testOrder.EP_Stock_Holding_Location__c = storageLocation.Id;
        testOrder.Stock_Holding_Location__c = siteLocation.Id;
        testOrder.EP_Order_Product_Category__c = 'Service';
		testOrder.EP_Order_Epoch__c = 'Current';
		testOrder.EP_Requested_Delivery_Date__c = Date.newInstance(2018, 10, 27);
        
        insert testOrder;
        
        // Reload Order in order to fetch related records
        testOrder = refreshOrder(testOrder.Id);
        
        cscfga__Product_Basket__c testBasket = [SELECT Id FROM cscfga__Product_Basket__c LIMIT 1];
        
        // 10/25/2018 = Thursday
        Boolean retval = CutOffMatrixAPI.processCutOffMatrixCheckUsingOrder(testOrder, DateTime.newInstanceGmt(2018, 10, 25, 10, 00, 00), 0, false);
        List<CutOffMatrixAPI.ValidationHelper> retvalCheckMatrix = CutOffMatrixAPI.checkCutOffMatrix(testBasket.Id, DateTime.newInstanceGmt(2018, 10, 25, 10, 00, 00), 0, new List<Date> { Date.newInstance(2018, 10, 27) }, storageLocation.Id, testCompany.Id, 'Delivery', false);

        Test.stopTest();
        
        System.assertEquals(true, retval, 'Invalid data');
        System.assertEquals(CutOffMatrixAPI.CutOffReason.VALID , retvalCheckMatrix[0].reason, 'Invalid data');
    }

    @isTest
    private static void processCutOffMatrixCheckUsingOrder_retrospective() {
        
        Company__c testCompany = [SELECT Id, Name FROM Company__c WHERE Name = 'Test Company' LIMIT 1];

        Account testAccount = [SELECT Id, Name FROM Account WHERE Name = 'Test Account' LIMIT 1];
        
        cscfga__Product_Basket__c basket = [SELECT Id, Name FROM cscfga__Product_Basket__c LIMIT 1];
        Account storageLocation = [SELECT Id FROM Account WHERE Name = 'Test Account Storage Location' LIMIT 1];
        EP_Stock_Holding_Location__c siteLocation = [SELECT Id, Name FROM EP_Stock_Holding_Location__c LIMIT 1];

        Test.startTest();
        
        csord__Order__c testOrder = new csord__Order__c();
        testOrder.csord__Identification__c = 'SGH849S4G9AS4ASVA4S9VA';
        testOrder.EP_Delivery_Type__c = 'Delivery';
        testOrder.Delivery_From_Date__c = Date.newInstance(2018, 10, 25);
        testOrder.Delivery_To_Date__c = Date.newInstance(2018, 10, 25);
        testOrder.csord__Account__c = testAccount.Id;
        testOrder.EP_Stock_Holding_Location__c = storageLocation.Id;
        testOrder.Stock_Holding_Location__c = siteLocation.Id;
        testOrder.EP_Order_Product_Category__c = 'Service';
		testOrder.EP_Order_Epoch__c = 'Retrospective';
        
        insert testOrder;
        
        testOrder = refreshOrder(testOrder.Id);
        
        cscfga__Product_Basket__c testBasket = [SELECT Id FROM cscfga__Product_Basket__c LIMIT 1];
        
        // 10/25/2018 = Thursday
        Boolean retval = CutOffMatrixAPI.processCutOffMatrixCheckUsingOrder(testOrder, DateTime.newInstanceGmt(2018, 10, 25, 23, 00, 00), 0, false);
        List<CutOffMatrixAPI.ValidationHelper> retvalCheckMatrix = CutOffMatrixAPI.checkCutOffMatrix(testBasket.Id, DateTime.newInstanceGmt(2018, 10, 25, 23, 00, 00), 0, new List<Date> { Date.newInstance(2018, 10, 25) }, storageLocation.Id, testCompany.Id, 'Delivery', false);

        Test.stopTest();

        System.assertEquals(true, retval, 'Invalid data');
        System.assertEquals(CutOffMatrixAPI.CutOffReason.VALID , retvalCheckMatrix[0].reason, 'Invalid data');
    }
    
    @isTest
    private static void processCutOffMatrixCheckUsingOrder_packaged() {
        
        Company__c testCompany = [SELECT Id, Name FROM Company__c WHERE Name = 'Test Company' LIMIT 1];

        Account testAccount = [SELECT Id, Name FROM Account WHERE Name = 'Test Account' LIMIT 1];
        
        cscfga__Product_Basket__c basket = [SELECT Id, Name FROM cscfga__Product_Basket__c LIMIT 1];
        Account storageLocation = [SELECT Id FROM Account WHERE Name = 'Test Account Storage Location' LIMIT 1];
        EP_Stock_Holding_Location__c siteLocation = [SELECT Id, Name FROM EP_Stock_Holding_Location__c LIMIT 1];

        Test.startTest();
        
        csord__Order__c testOrder = new csord__Order__c();
        testOrder.csord__Identification__c = 'SGH849S4G9AS4ASVA4S9VA';
        testOrder.EP_Delivery_Type__c = 'Delivery';
        testOrder.Delivery_From_Date__c = Date.newInstance(2018, 10, 25);
        testOrder.Delivery_To_Date__c = Date.newInstance(2018, 10, 25);
        testOrder.csord__Account__c = testAccount.Id;
        testOrder.EP_Stock_Holding_Location__c = storageLocation.Id;
        testOrder.Stock_Holding_Location__c = siteLocation.Id;
        testOrder.EP_Order_Product_Category__c = 'Packaged';
		testOrder.EP_Order_Epoch__c = 'Current';
		testOrder.EP_Requested_Delivery_Date__c = Date.newInstance(2018, 10, 25);
        
        insert testOrder;
        
        testOrder = refreshOrder(testOrder.Id);
        
        cscfga__Product_Basket__c testBasket = [SELECT Id FROM cscfga__Product_Basket__c LIMIT 1];
        
        // 10/25/2018 = Thursday
        Boolean retval = CutOffMatrixAPI.processCutOffMatrixCheckUsingOrder(testOrder, DateTime.newInstanceGmt(2018, 10, 25, 23, 00, 00), 0, false);
        List<CutOffMatrixAPI.ValidationHelper> retvalCheckMatrix = CutOffMatrixAPI.checkCutOffMatrix(testBasket.Id, DateTime.newInstanceGmt(2018, 10, 25, 23, 00, 00), 0, new List<Date> { Date.newInstance(2018, 10, 25) }, storageLocation.Id, testCompany.Id, 'Delivery', false);

        Test.stopTest();

        System.assertEquals(true, retval, 'Invalid data');
        System.assertEquals(CutOffMatrixAPI.CutOffReason.VALID , retvalCheckMatrix[0].reason, 'Invalid data');
    }

    @isTest
    // Portal users should only be able to cancel before cut off. 
    // Cancellation after cutoff time is not allowed for portal 
    // users, and if they try they should get error.
    private static void processCutOffMatrixCheckUsingOrder_portalAfterCutoff() {
        
        Company__c testCompany = [SELECT Id, Name FROM Company__c WHERE Name = 'Test Company' LIMIT 1];

        Account testAccount = [SELECT Id, Name FROM Account WHERE Name = 'Test Account' LIMIT 1];
        
        cscfga__Product_Basket__c basket = [SELECT Id, Name FROM cscfga__Product_Basket__c LIMIT 1];
        Account storageLocation = [SELECT Id FROM Account WHERE Name = 'Test Account Storage Location' LIMIT 1];
        EP_Stock_Holding_Location__c siteLocation = [SELECT Id, Name FROM EP_Stock_Holding_Location__c LIMIT 1];

        Test.startTest();
        
        csord__Order__c testOrder = new csord__Order__c();
        testOrder.csord__Identification__c = 'SGH849S4G9AS4ASVA4S9VA';
        testOrder.EP_Delivery_Type__c = 'Delivery';
        testOrder.Delivery_From_Date__c = Date.newInstance(2018, 10, 25);
        testOrder.Delivery_To_Date__c = Date.newInstance(2018, 10, 25);
        testOrder.csord__Account__c = testAccount.Id;
        testOrder.EP_Stock_Holding_Location__c = storageLocation.Id;
        testOrder.Stock_Holding_Location__c = siteLocation.Id;
        testOrder.EP_Order_Product_Category__c = 'Service';
		testOrder.EP_Order_Epoch__c = 'Current';
		testOrder.EP_Requested_Delivery_Date__c = Date.newInstance(2018, 10, 25);
        
        insert testOrder;
        
        // Reload Order in order to fetch related records
        testOrder = refreshOrder(testOrder.Id);
        
        cscfga__Product_Basket__c testBasket = [SELECT Id FROM cscfga__Product_Basket__c LIMIT 1];
        
        // 10/25/2018 = Thursday
        DateTime localDateTime = DateTime.newInstanceGmt(2018, 10, 25, 14, 00, 00);
        Boolean retval = CutOffMatrixAPI.processCutOffMatrixCheckUsingOrder(testOrder, localDateTime, 0, false);
        List<CutOffMatrixAPI.ValidationHelper> retvalCheckMatrix = CutOffMatrixAPI.checkCutOffMatrix(testBasket.Id, DateTime.newInstanceGmt(2018, 10, 25, 14, 00, 00), 0, new List<DateTime> { DateTime.newInstance(2018, 10, 25, 14, 00, 00) }, storageLocation.Id, testCompany.Id, 'Delivery', false);

        Test.stopTest();

        // Portal user can not cancel after cutoff
        System.assertEquals(false, retval, 'Invalid data');
        System.assertEquals(CutOffMatrixAPI.CutOffReason.DATE_IS_OUTSIDE_ALLOWABLE_DELIVERY , retvalCheckMatrix[0].reason, 'Invalid data');
    }

    @isTest
    public static void validateDayTime_positive() {
        
	    DeliveryDay__c deliveryDay = [SELECT Id, Name, CurrencyIsoCode,  Cut_Off_Matrix__c, 
                                            Delivery_Type__c, Friday__c, Monday__c, OrderDayNum__c, Order_Day_Name__c, Saturday__c, 
                                            Site_Location__c, Sunday__c, Thursday__c, Tuesday__c, Wednesday__c, 
                                            Cut_Off_Matrix__r.Site_Location__c, Cut_Off_Matrix__r.Delivery_Type__c,
                                            Cut_Off_Matrix__r.Time_Zone__c
                                        FROM DeliveryDay__c 
                                        WHERE Order_Day_Name__c = 'Thursday' LIMIT 1];

        
        DateTime dateInfo = DateTime.newInstanceGmt(2018, 10, 25, 10, 30, 0);
        
        Test.startTest();

        Boolean retval = CutOffMatrixAPI.validateDayTime(dateInfo, deliveryDay);

        Test.stopTest();
        
        System.assertEquals(true, retval, 'Invalid data');
    }
    
    @isTest
    public static void validateDayTime_positive_overflow() {
        
	    DeliveryDay__c deliveryDay = [SELECT Id, Name, CurrencyIsoCode,  Cut_Off_Matrix__c, 
                                            Delivery_Type__c, Friday__c, Monday__c, OrderDayNum__c, Order_Day_Name__c, Saturday__c, 
                                            Site_Location__c, Sunday__c, Thursday__c, Tuesday__c, Wednesday__c, 
                                            Cut_Off_Matrix__r.Site_Location__c, Cut_Off_Matrix__r.Delivery_Type__c,
                                            Cut_Off_Matrix__r.Time_Zone__c
                                        FROM DeliveryDay__c 
                                        WHERE Order_Day_Name__c = 'Thursday' LIMIT 1];

        
        DateTime dateInfo = DateTime.newInstanceGmt(2018, 10, 25, 22, 30, 0);
        
        Test.startTest();

        Boolean retval = CutOffMatrixAPI.validateDayTime(dateInfo,deliveryDay);

        Test.stopTest();
        
        System.assertEquals(true, retval, 'Invalid data');
    }
    
    @isTest
    public static void validateDayTime_negative() {
        
	    DeliveryDay__c deliveryDay = [SELECT Id, Name, CurrencyIsoCode,  Cut_Off_Matrix__c, 
                                            Delivery_Type__c, Friday__c, Monday__c, OrderDayNum__c, Order_Day_Name__c, Saturday__c, 
                                            Site_Location__c, Sunday__c, Thursday__c, Tuesday__c, Wednesday__c, 
                                            Cut_Off_Matrix__r.Site_Location__c, Cut_Off_Matrix__r.Delivery_Type__c,
                                            Cut_Off_Matrix__r.Time_Zone__c
                                        FROM DeliveryDay__c 
                                        WHERE Order_Day_Name__c = 'Thursday' LIMIT 1];

        
        DateTime dateInfo = DateTime.newInstanceGmt(2018, 10, 25, 14, 30, 0);
        
        Test.startTest();

        Boolean retval = CutOffMatrixAPI.validateDayTime(dateInfo, deliveryDay);

        Test.stopTest();
        
        System.assertEquals(false, retval, 'Invalid data');
    }

}