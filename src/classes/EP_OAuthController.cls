/* 
   @Author <Accenture>
   @name <EP_OAuthController>
   @CreateDate <02/01/2018>
   @Description <This class is used to get access token for OAuth authentication process>
   @Version <1.0>
*/
public virtual class EP_OAuthController {
    
    public string token_type;
    public string groupId{get;set;}
    public string reportId{get;set;}
    public String validateResult;

    public String application_name;
   
    public String PBIaccess_token { 
        get {
            Cookie pbi_AccessToken = ApexPages.currentPage().getCookies().get(EP_Common_Constant.ACCESSTOKEN);
            if(pbi_AccessToken <> null)
                return pbi_AccessToken.getValue();
                
            else
                return EP_Common_Constant.BLANK;
        } 
        set;
        }
        
    public String PBIrefresh_token { 
        get {
            Cookie pbi_RefreshToken = ApexPages.currentPage().getCookies().get(EP_Common_Constant.REFRESHTOKEN);
            if(pbi_RefreshToken <> null)
                return pbi_RefreshToken.getValue();
            else
                return EP_Common_Constant.BLANK;
        } 
        set;
        }
        
    public String PBIexpires_on { 
        get {
            Cookie pbi_ExpiresOn= ApexPages.currentPage().getCookies().get(EP_Common_Constant.EXPIRES_ON);
            if(pbi_ExpiresOn <> null)
                return pbi_ExpiresOn.getValue();
            else
                return EP_Common_Constant.BLANK;
        } 
        set;
        }
        
        
    /**
    * @author       Accenture
    * @name         refreshAccessToken
    * @date         02/01/2018
    * @description  This method Gets new access token through refresh token
    * @param        pagereference
    * @return       PageReference
    */ 
    public PageReference refreshAccessToken(PageReference location){
        EP_GeneralUtility.Log('public','EP_OAuthController','refreshAccessToken');
        Cookie accessTokenCookie = new Cookie(EP_Common_Constant.ACCESSTOKEN, '',null,0,false);
        Cookie expiresOn = new Cookie(EP_Common_Constant.EXPIRES_ON,'',null,0,false);
            
        ApexPages.currentPage().setCookies(new Cookie[]{accessTokenCookie,expiresOn}); 
            
        EP_FE_PowerBIResponse responseObj = EP_FE_PowerBIEndpoint.GetEmbedToken();
        groupId = responseObj.GROUPID;
        reportId = responseObj.REPORTID;
        accessTokenCookie = new Cookie(EP_Common_Constant.ACCESSTOKEN, EP_FE_PowerBIEndpoint.access_token,null,-1,false);
        Cookie refreshToken = new Cookie(EP_Common_Constant.REFRESHTOKEN, EP_FE_PowerBIEndpoint.refresh_token,null,-1,false);
        expiresOn = new Cookie(EP_Common_Constant.EXPIRES_ON,EP_FE_PowerBIEndpoint.expires_on,null,-1,false);
            
        ApexPages.currentPage().setCookies(new Cookie[]{accessTokenCookie,refreshToken,expiresOn});   
        
        return location;
       
    }
    
    /**
    * @author       Accenture
    * @name         EP_OAuthController
    * @date         02/01/2018
    * @description  Constructor
    * @param        NA
    * @return       NA
    */ 
    public EP_OAuthController() {
        EP_GeneralUtility.Log('public','EP_OAuthController','EP_OAuthController');
        token_type = apexPages.currentPage().getParameters().get('token_type');              
    
        if (EP_FE_OAuthApps__c.getValues(this.application_name) != null) {
            Cookie pbi_AccessToken = ApexPages.currentPage().getCookies().get(EP_Common_Constant.ACCESSTOKEN);
            Cookie pbi_RefreshToken = ApexPages.currentPage().getCookies().get(EP_Common_Constant.REFRESHTOKEN);
            Cookie pbi_ExpiresOn = ApexPages.currentPage().getCookies().get(EP_Common_Constant.EXPIRES_ON);
             
        }
    }

    /**
    * @author       Accenture
    * @name         getAuthUrl
    * @date         02/01/2018
    * @description  It gets the authorrization URL
    * @param        NA
    * @return       Auth Url
    */ 
    
    public String getAuthUrl() {
        EP_GeneralUtility.Log('public','EP_OAuthController','getAuthUrl');
        Map<String, String> urlParams = new Map<String, String> {
            EP_Common_Constant.PBI_CLIENTID => EP_FE_OAuthApps__c.getValues(this.application_name).Client_Id__c,
            EP_Common_Constant.REDIRECTURI => getPageUrl(),
            EP_Common_Constant.RESOURCE => EP_FE_OAuthApps__c.getValues(this.application_name).Resource_URI__c,
            EP_Common_Constant.RESPONSETYPE => EP_Common_Constant.CODE
        };
        String auth_url = EP_FE_OAuthApps__c.getValues(this.application_name).Authorization_URL__c;

        PageReference ref = new PageReference(auth_url);
        ref.getParameters().putAll(urlParams);
        system.debug('AUTH URL ==='+ref.getUrl());
        return ref.getUrl();
    }
    
    /**
    * @author       Accenture
    * @name         getPageUrl
    * @date         02/01/2018
    * @description  It gets the page URL
    * @param        NA
    * @return       current Page Url
    */ 
    @testVisible
    private String getPageUrl() {
        EP_GeneralUtility.Log('private','EP_OAuthController','getPageUrl');
        String host = ApexPages.currentPage().getHeaders().get(EP_Common_Constant.HOST);
        String path = ApexPages.currentPage().getUrl().split(EP_Common_Constant.SPLITSTRING).get(0);

        return EP_Common_Constant.HTTPS + host + path;
    }

    /**
    * Validates the callback code and generates the access and refresh tokens
    *
    * @param location Where to redirect to after success
    * @return null to refresh the page
    */
       public PageReference redirectOnCallback(PageReference location) {
           EP_GeneralUtility.Log('public','EP_OAuthController','redirectOnCallback');
           EP_FE_PowerBIEndpoint.tokenType = token_type;
           EP_FE_PowerBIResponse responseObj = EP_FE_PowerBIEndpoint.GetEmbedToken();
           groupId = responseObj.GROUPID;
           reportId = responseObj.REPORTID;
           Cookie accessTokenCookie = new Cookie(EP_Common_Constant.ACCESSTOKEN, EP_FE_PowerBIEndpoint.access_token,null,-1,false);
           Cookie refreshToken = new Cookie(EP_Common_Constant.REFRESHTOKEN, EP_FE_PowerBIEndpoint.refresh_token,null,-1,false);
           Cookie expiresOn = new Cookie(EP_Common_Constant.EXPIRES_ON,EP_FE_PowerBIEndpoint.expires_on,null,-1,false);
           system.debug('**************'+EP_FE_PowerBIEndpoint.access_token);
           ApexPages.currentPage().setCookies(new Cookie[]{accessTokenCookie,refreshToken,expiresOn}); 
            
            return location;
                 
       
    }
}