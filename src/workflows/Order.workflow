<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>EP_Load_Confirmation_Notification</fullName>
        <description>Load Confirmation Notification</description>
        <protected>false</protected>
        <recipients>
            <field>EP_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EP_Load_Confirmation_Notification</template>
    </alerts>
    <alerts>
        <fullName>EP_Non_Delivery_Notification_Re_Scheduled</fullName>
        <description>Non-Delivery Notification (Re-Scheduled)</description>
        <protected>false</protected>
        <recipients>
            <field>EP_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EP_Non_Delivery_Notification/EP_Non_Delivery_Notification_Re_Schedule</template>
    </alerts>
    <alerts>
        <fullName>EP_Non_Delivery_Notifications_To_be_decided</fullName>
        <description>Non Delivery Notifications(To be decided)</description>
        <protected>false</protected>
        <recipients>
            <recipient>All_CSC_Users</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EP_Non_Delivery_Notification_To_be_decided</template>
    </alerts>
    <alerts>
        <fullName>EP_Order_Cancellation_Notification</fullName>
        <description>Order Cancellation Notification</description>
        <protected>false</protected>
        <recipients>
            <field>EP_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EP_Order_Cancellation_Notification</template>
    </alerts>
    <alerts>
        <fullName>EP_Overdue_Amount_Notification</fullName>
        <description>EP_Overdue_Amount_Notification</description>
        <protected>false</protected>
        <recipients>
            <field>EP_Bill_To_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/EP_Overdue_Amount_Notification</template>
    </alerts>
    <alerts>
        <fullName>EP_Scheduling_Completion_Notification</fullName>
        <description>Scheduling Completion Notification</description>
        <protected>false</protected>
        <recipients>
            <field>EP_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EP_Scheduling_Completion_Notification</template>
    </alerts>
    <alerts>
        <fullName>Ex_Rack_Order_Pickup_notification</fullName>
        <description>Ex-Rack Order Pickup notification</description>
        <protected>false</protected>
        <recipients>
            <field>EP_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EP_Ex_Rack_Order_pickup_notification</template>
    </alerts>
    <alerts>
        <fullName>Non_Delivery_Notification_Cancelled</fullName>
        <description>Non-Delivery Notification (Cancelled)</description>
        <protected>false</protected>
        <recipients>
            <field>EP_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EP_Non_Delivery_Notification/EP_Non_Delivery_Notification_Cancellation</template>
    </alerts>
    <fieldUpdates>
        <fullName>EP_Inventory_Approved</fullName>
        <field>EP_Reason_For_Change__c</field>
        <literalValue>Inventory Approved</literalValue>
        <name>Inventory Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_Inventory_Rejected</fullName>
        <field>EP_Reason_For_Change__c</field>
        <literalValue>Inventory Rejected</literalValue>
        <name>Inventory Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_Order_Status_Cancelled</fullName>
        <field>Status</field>
        <literalValue>Cancelled</literalValue>
        <name>EP_Order_Status_Cancelled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_Set_Order_Status_Cancelled</fullName>
        <description>Set Order Status Cancelled</description>
        <field>Status</field>
        <literalValue>Cancelled</literalValue>
        <name>Set Order Status Cancelled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_Set_Order_Status_Submitted</fullName>
        <description>Set Order Status submitted</description>
        <field>Status</field>
        <literalValue>Submitted</literalValue>
        <name>Set Order Status Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_Track_status</fullName>
        <description>Field to update EP_Track_Status__c</description>
        <field>EP_Track_Status__c</field>
        <literalValue>1</literalValue>
        <name>Track status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_Track_status_Planned</fullName>
        <description>Track status Planned</description>
        <field>EP_Track_Status_Planned__c</field>
        <literalValue>1</literalValue>
        <name>Track status Planned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>EP_Load_Confirmation_Notification</fullName>
        <actions>
            <name>EP_Load_Confirmation_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>EP_Track_status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Load Confirmation Notification</description>
        <formula>IF(EP_Order_Record_Type__c!=&apos;Transfer Orders&apos;,  AND(ISPICKVAL(Status , &apos;Loaded&apos;), NOT(ISBLANK(EP_Estimated_Time_Range__c )),  NOT(EP_Track_Status__c) ),  false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EP_Non_Delivery_Notification_Cancelled</fullName>
        <actions>
            <name>Non_Delivery_Notification_Cancelled</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.EP_Action_for_Non_Delivery__c</field>
            <operation>equals</operation>
            <value>Cancelled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.EP_Delivery_Type__c</field>
            <operation>equals</operation>
            <value>Delivery</value>
        </criteriaItems>
        <description>Non Delivery Notification Cancelled</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EP_Non_Delivery_Notification_Rescheduled</fullName>
        <actions>
            <name>EP_Non_Delivery_Notification_Re_Scheduled</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.EP_Action_for_Non_Delivery__c</field>
            <operation>equals</operation>
            <value>Re-Scheduled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.EP_Delivery_Type__c</field>
            <operation>equals</operation>
            <value>Delivery</value>
        </criteriaItems>
        <description>Non Delivery Notification Rescheduled</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
