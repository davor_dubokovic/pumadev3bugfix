<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>EP_EM001_Send_Tank_Dip_Reminder_Email</fullName>
        <description>EP_EM001-Send Tank Dip Reminder Email CS</description>
        <protected>false</protected>
        <recipients>
            <field>EP_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EP_Missing_Tank_Dip_Notification_1_AU_New</template>
    </alerts>
    <alerts>
        <fullName>EP_EM002_Send_Tank_Dip_Reminder_Email</fullName>
        <description>EP_EM002-Send Tank Dip Reminder Email at the time</description>
        <protected>false</protected>
        <recipients>
            <field>EP_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EP_Missing_Tank_Dip_Notification_2_New</template>
    </alerts>
    <alerts>
        <fullName>EP_EM003_Send_Tank_Dip_Reminder_Email</fullName>
        <description>EP_EM003-Send Tank Dip Reminder Email at the time</description>
        <protected>false</protected>
        <recipients>
            <field>EP_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EP_Missing_Tank_Dip_Notification_3_New</template>
    </alerts>
    <alerts>
        <fullName>EP_EM004_Send_Tank_Dip_Reminder_Email</fullName>
        <description>Notification to be sent Approximately on the scheduler deadline time</description>
        <protected>false</protected>
        <recipients>
            <field>EP_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EP_Missing_Tank_Dip_Notification4</template>
    </alerts>
    <alerts>
        <fullName>EP_Email_to_notify_review_actions_completed</fullName>
        <description>Email to notify review actions completed</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/EP_Account_Notify_User_Review_Actions_Completed</template>
    </alerts>
    <alerts>
        <fullName>EP_Notify_Account_Owner</fullName>
        <description>Notify Account Owner</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/EP_Account_Notify_Account_Owner</template>
    </alerts>
    <alerts>
        <fullName>EP_Notify_Account_Owner_About_Account_Name_Change</fullName>
        <description>Notify Account Owner About Account Name Change</description>
        <protected>false</protected>
        <recipients>
            <field>EP_Salesperson__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EP_Notify_Account_Owner_About_Name_Change</template>
    </alerts>
    <alerts>
        <fullName>EP_Notify_Account_Owner_About_Approved_Credit_Limit</fullName>
        <description>Notify Account Owner About Approved Credit Limit</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EP_Notification_template_for_credit_limit_approval</template>
    </alerts>
    <alerts>
        <fullName>EP_Notify_Sell_To_Owner_when_Ship_to_Blocked</fullName>
        <description>Notify Sell To Owner when Ship to Blocked</description>
        <protected>false</protected>
        <recipients>
            <field>Sell_To_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Ep_Notify_sell_To_Owner_on_Ship_To_Blocked</template>
    </alerts>
    <alerts>
        <fullName>EP_Notify_Ship_To_owner_about_name_change</fullName>
        <description>Notify Ship To owner about name change</description>
        <protected>false</protected>
        <recipients>
            <field>EP_Parent_Account_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EP_Notify_Ship_To_Account_Owner_About_Name_Change</template>
    </alerts>
    <alerts>
        <fullName>EP_Notify_Ship_To_owner_about_status_change</fullName>
        <description>Notify Ship To owner about status change</description>
        <protected>false</protected>
        <recipients>
            <field>EP_Parent_Account_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EP_Notify_Account_Owner_About_Account_Status_Change</template>
    </alerts>
    <alerts>
        <fullName>EP_Send_mail_to_account_owner_for_kyc_review</fullName>
        <description>Send mail to account owner for kyc review</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/EP_Account_Sell_To_CSC_Review_Complete_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_Account_Owner_About_Name_Change</fullName>
        <description>Notify Account Owner About Name Change</description>
        <protected>false</protected>
        <recipients>
            <field>EP_Salesperson__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EP_Notify_Account_Owner_About_Name_Change</template>
    </alerts>
    <alerts>
        <fullName>Notify_Account_Owner_after_credit_approval</fullName>
        <description>Notify Account Owner after credit approval</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/EP_Customer_is_active_with_Credit_is_approved</template>
    </alerts>
    <alerts>
        <fullName>Notify_Account_Owner_on_Sync_Failure</fullName>
        <description>Notify Account Owner on Sync Failure</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/EP_Customer_activation_failed_due_to_synchronization_issues</template>
    </alerts>
    <alerts>
        <fullName>Notify_Sell_To_Owner_when_Status_to_Blocked</fullName>
        <description>Notify Sell To Owner when Status to Blocked</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Ep_Notify_sell_To_Owner_on_Status_To_Blocked</template>
    </alerts>
    <fieldUpdates>
        <fullName>EP_Account_Status_to_Active</fullName>
        <description>Account status should be active</description>
        <field>EP_Status__c</field>
        <literalValue>05-Active</literalValue>
        <name>EP_Account_Status_to_Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_Account_Status_to_blocked</fullName>
        <description>Change the Account Status to &apos;Blocked&apos; if checkbox &apos;Is Blocked Required&apos; is checked.</description>
        <field>EP_Status__c</field>
        <literalValue>06-Blocked</literalValue>
        <name>EP_Account_Status_to_blocked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_Activate_Dummy_Customer</fullName>
        <description>ACTIVATE DUMMY CUSTOMER WHEN STATUS CHANGES TO ACCOUNT SET-UP</description>
        <field>EP_Status__c</field>
        <literalValue>05-Active</literalValue>
        <name>Activate Dummy Customer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_FU003_UpdateAccountNumber</fullName>
        <description>It updates the account number</description>
        <field>AccountNumber</field>
        <formula>EP_Account_Number__c</formula>
        <name>EP-FU003-UpdateAccountNumber</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_Populate_Composit_ID</fullName>
        <field>EP_Composite_Id__c</field>
        <formula>IF(
OR(RecordType.DeveloperName == &apos;EP_Non_VMI_Ship_To&apos;,
RecordType.DeveloperName == &apos;EP_VMI_Ship_To&apos;,
RecordType.DeveloperName == &apos;EP_Storage_Ship_To&apos;),
 Parent.EP_Composite_Id__c +&apos;-&apos;+ IF(NOT(ISBLANK(EP_Legacy_Id__c)), EP_Legacy_Id__c,   EP_Account_Number__c)
,
 EP_Puma_Company_Code__c +&apos;-&apos;+IF(NOT(ISBLANK(EP_Legacy_Id__c)), EP_Legacy_Id__c,  EP_Account_Number__c)
)</formula>
        <name>Populate Composit ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_Populate_search_name_with_account_nam</fullName>
        <description>defect 83530</description>
        <field>EP_Search_Name__c</field>
        <formula>Name</formula>
        <name>Populate search name with account name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_Uncheck_Credit_Limit_Approved_Field</fullName>
        <description>Uncheck Credit Limit Approved check box field after email notification is send to the account owner.</description>
        <field>EP_Credit_Limit_Approved__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Credit Limit Approved Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_Update_Region_with_selected_country</fullName>
        <description>Update Region with the selected country</description>
        <field>EP_Region__c</field>
        <formula>EP_Country__r.EP_Region__c</formula>
        <name>Update Region with the selected country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_sell_to_Holding_Overdue_Balance_HCY</fullName>
        <description>Defect 83050</description>
        <field>EP_Holding_Overdue_Balance_LCY__c</field>
        <formula>0.0</formula>
        <name>Set sell to Holding Overdue Balance (HCY</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_sell_to_Holding_Overdue_Balance_LCY</fullName>
        <description>Defect 83050</description>
        <field>EP_Holding_Overdue_Balance_HCY__c</field>
        <formula>0.0</formula>
        <name>Set sell to Holding Overdue Balance (LCY</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_sell_to_available_funds_HCY</fullName>
        <description>Defect 83050</description>
        <field>EP_AvailableFunds__c</field>
        <formula>0.0</formula>
        <name>Set sell to available funds HCY</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_sell_to_available_funds_LCY</fullName>
        <description>Defect 83050</description>
        <field>EP_Available_Funds_LCY__c</field>
        <formula>0.0</formula>
        <name>Set sell to available funds LCY</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Activate Dummy Customer</fullName>
        <actions>
            <name>EP_Activate_Dummy_Customer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Bill To,Sell To</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.EP_Is_Dummy__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.EP_Status__c</field>
            <operation>equals</operation>
            <value>04-Account Set-up</value>
        </criteriaItems>
        <description>ACTIVATE DUMMY CUSTOMER WHEN STATUS CHANGES TO ACCOUNT SET-UP</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EP Auto populate search name with account name</fullName>
        <actions>
            <name>EP_Populate_search_name_with_account_nam</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Defect 83530</description>
        <formula>OR(isNEW(), AND( ISCHANGED( Name ) , Name !=null) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EP Reset Customer Funds Values Of Sell to</fullName>
        <actions>
            <name>Set_sell_to_Holding_Overdue_Balance_HCY</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_sell_to_Holding_Overdue_Balance_LCY</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_sell_to_available_funds_HCY</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_sell_to_available_funds_LCY</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Defect 83050, When a to sell-to (Child) is associated to Holding (Parent), SFDC will reset the values of fields Available funds HCY, Available LCY, Holding Overdue Balance (LCY), Holding Overdue Balance (HCY) as “0.0” on related Sell-To (child).</description>
        <formula>EP_Sell_To_Holding_Account__c!=null</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EP_Create_Composite_Id</fullName>
        <actions>
            <name>EP_FU003_UpdateAccountNumber</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>EP_Populate_Composit_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(OR(ISNEW(), ISCHANGED( EP_Legacy_Id__c), ISCHANGED( EP_Puma_Company__c),ISBLANK(AccountNumber)), VALUE(LEFT(TEXT(EP_Status__c),2 ))&lt;5,EP_Application_Record_Type_Name__c == $Label.EP_Record_Type_Name)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EP_NotifiyAccount_Owner_On_Customer_Activation_Sync_Failure</fullName>
        <actions>
            <name>Notify_Account_Owner_on_Sync_Failure</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 5 AND (3 OR  4)</booleanFilter>
        <criteriaItems>
            <field>Account.EP_Status__c</field>
            <operation>equals</operation>
            <value>02-Basic Data Setup</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeName__c</field>
            <operation>equals</operation>
            <value>Sell To</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.EP_Integration_Status__c</field>
            <operation>equals</operation>
            <value>ERROR-SENT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.EP_Integration_Status__c</field>
            <operation>equals</operation>
            <value>ERROR-SYNC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.EP_BSM_GM_Review_Completed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EP_Notify_Account_Owner_After_Account_Activation</fullName>
        <actions>
            <name>EP_Notify_Account_Owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify Account Owner after successful activation of sell to account.</description>
        <formula>AND(RecordType.DeveloperName = &apos;EP_Sell_To&apos;, ISCHANGED(EP_Status__c), OR(TEXT(PRIORVALUE(EP_Status__c))= &apos;02-Basic Data Setup&apos;,TEXT(PRIORVALUE(EP_Status__c))= &apos;06-Blocked&apos;), ISPICKVAL(EP_Status__c , &apos;05-Active&apos;)  ,ISBLANK(EP_Recommended_Credit_Limit__c),EP_RequestedPaymentTerms__r.EP_Payment_Term_Code__c == &quot;PP&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EP_Notify_Account_Owner_After_Account_Activation_2</fullName>
        <actions>
            <name>Notify_Account_Owner_after_credit_approval</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify Account Owner after successful activation of sell to account with credit</description>
        <formula>AND(RecordType.DeveloperName = &apos;EP_Sell_To&apos;,  ISCHANGED(EP_Status__c), OR(TEXT(PRIORVALUE(EP_Status__c))= &apos;02-Basic Data Setup&apos;,TEXT(PRIORVALUE(EP_Status__c))= &apos;06-Blocked&apos;),ISPICKVAL(EP_Status__c , &apos;05-Active&apos;), NOT(ISBLANK(EP_Credit_Limit__c)),EP_Credit_Limit__c&lt;&gt;0,EP_RequestedPaymentTerms__r.EP_Payment_Term_Code__c &lt;&gt; &quot;PP&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EP_Notify_Account_Owner_After_Credit_Limit_Approval</fullName>
        <actions>
            <name>EP_Notify_Account_Owner_About_Approved_Credit_Limit</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISNEW()),ISCHANGED( EP_Credit_Limit_Approved__c ), EP_Credit_Limit_Approved__c  = true,EP_Application_Record_Type_Name__c == $Label.EP_Record_Type_Name)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EP_Update_Region_On_Account</fullName>
        <actions>
            <name>EP_Update_Region_with_selected_country</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To Update region on account based on country selected</description>
        <formula>AND(NOT(ISNULL(EP_Region__c)),EP_Application_Record_Type_Name__c == $Label.EP_Record_Type_Name)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Ep_Notify sell To Owner on Ship To Blocked</fullName>
        <actions>
            <name>EP_Notify_Sell_To_Owner_when_Ship_to_Blocked</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(ISCHANGED(EP_Status__c),OR(ISPICKVAL(EP_Status__c , &apos;06-Blocked&apos;) , ISPICKVAL(EP_Status__c ,&apos;05-Active&apos;)),OR(RecordType.DeveloperName==&apos;EP_Non_VMI_Ship_To&apos;,RecordType.DeveloperName==&apos;EP_VMI_Ship_To&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Ep_Notify sell To Owner on Status Blocked</fullName>
        <actions>
            <name>Notify_Sell_To_Owner_when_Status_to_Blocked</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(EP_Status__c , &apos;06-Blocked&apos;)  &amp;&amp;  (RecordType.DeveloperName==&apos;EP_Sell_To&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Account Owner About Name Change</fullName>
        <actions>
            <name>Notify_Account_Owner_About_Name_Change</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED(Name) &amp;&amp; NOT(ISNEW()) &amp;&amp; EP_Application_Record_Type_Name__c == $Label.EP_Record_Type_Name</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify SM%2FTM For Account Name Change</fullName>
        <actions>
            <name>EP_Notify_Account_Owner_About_Account_Name_Change</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(
OR(RecordType.DeveloperName = &apos;EP_Sell_To&apos;, RecordType.DeveloperName = &apos;EP_CRM_Prospect&apos;),
VALUE(LEFT( TEXT(EP_Status__c),2))&gt;=5, ISCHANGED(Name),
NOT(ISNEW()))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify Ship To Owner about name change</fullName>
        <actions>
            <name>EP_Notify_Ship_To_owner_about_name_change</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(OR(RecordType.DeveloperName = &apos;EP_VMI_Ship_To&apos;,RecordType.DeveloperName = &apos;EP_Non_VMI_Ship_To&apos;)  ,VALUE(LEFT( TEXT(EP_Status__c),2 ))&gt;=5,ISCHANGED(Name))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
