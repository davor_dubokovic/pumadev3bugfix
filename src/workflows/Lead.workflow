<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>EP_Customer_Application_Rejected</fullName>
        <description>Customer Application Rejected</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EP_Customer_Application_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>EP_CRM_Lead_record_type_to_Business_Enq</fullName>
        <description>Field update to change lead record type to Business Enquiry when Status is updated from On Hold to any other value.</description>
        <field>RecordTypeId</field>
        <lookupValue>EP_CRM_Business_Enquiry</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>EP_CRM_Lead record type to Business Enq</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Lead_record_type_to_On_Hold</fullName>
        <description>Field update to change lead record type to Business Enquiry On Hold when status is updated to On Hold.</description>
        <field>RecordTypeId</field>
        <lookupValue>EP_CRM_Business_Enquiry_Onhold</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>EP_CRM_Lead record type to On Hold</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Update_Closed_Date</fullName>
        <description>Update field Closed date on lead when lead status is Closed</description>
        <field>EP_CRM_Closed_Date__c</field>
        <formula>NOW()</formula>
        <name>EP CRM Update Closed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Update_Count_On_hold_Days</fullName>
        <description>Calculates the days when lead status was onhold</description>
        <field>EP_CRM_Total_On_Hold_Days__c</field>
        <formula>IF((ISBLANK( PRIORVALUE(EP_CRM_Total_On_Hold_Days__c  )) || ISNULL(PRIORVALUE(EP_CRM_Total_On_Hold_Days__c  ))  ) &amp;&amp; (RecordType.DeveloperName ==  $Label.EP_CRM_LEAD_RECTYPE_BI  || RecordType.DeveloperName ==   $Label.EP_CRM_LEAD_RECTYPE_BI_ONHOLD ), 0, PRIORVALUE(EP_CRM_Total_On_Hold_Days__c) +( NOW() -  EP_CRM_On_Hold_Date__c))</formula>
        <name>EP CRM Count Total On hold Days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Update_OnHold_Date</fullName>
        <description>Update field on hold date on lead when lead status is Onhold</description>
        <field>EP_CRM_On_Hold_Date__c</field>
        <formula>now()</formula>
        <name>EP CRM Update OnHold Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>EP CRM Update Closed Date</fullName>
        <actions>
            <name>EP_CRM_Update_Closed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Business Enquiry,Business Enquiry Onhold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>Update Closed Date field when lead status is Closed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EP CRM Update Total On hold Days</fullName>
        <actions>
            <name>EP_CRM_Update_Count_On_hold_Days</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update field on Lead to capture how many days lead was in On Hold Status</description>
        <formula>AND ( ISCHANGED (Status), ISPICKVAL( PRIORVALUE (Status),   &apos;On Hold&apos;),  OR (RecordType.DeveloperName ==  $Label.EP_CRM_LEAD_RECTYPE_BI, RecordType.DeveloperName ==  $Label.EP_CRM_LEAD_RECTYPE_BI_ONHOLD))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EP CRM Update onHold Date</fullName>
        <actions>
            <name>EP_CRM_Update_OnHold_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Business Enquiry,Business Enquiry Onhold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>On Hold</value>
        </criteriaItems>
        <description>Update On Hold Date field when lead status is on Hold.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EP_CRM_Lead record type to Business Enq</fullName>
        <actions>
            <name>EP_CRM_Lead_record_type_to_Business_Enq</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Rule to convert lead record type from buiness enquiry on hold to business enquiry when status is changed from On Hold/ Closed values.</description>
        <formula>AND( OR(RecordType.DeveloperName  = $Label.EP_CRM_LEAD_RECTYPE_BI, RecordType.DeveloperName = $Label.EP_CRM_LEAD_RECTYPE_BI_ONHOLD), ISCHANGED( Status ), OR(ISPICKVAL(PRIORVALUE(Status),&apos;On Hold&apos;),ISPICKVAL(PRIORVALUE(Status),&apos;Closed&apos;)),OR(ISPICKVAL(Status, &apos;Open&apos;),ISPICKVAL(Status, &apos;Working&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EP_CRM_Lead record type to On Hold</fullName>
        <actions>
            <name>EP_CRM_Lead_record_type_to_On_Hold</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Closed,On Hold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Business Enquiry,Business Enquiry Onhold</value>
        </criteriaItems>
        <description>Workflow rule to convert lead record type to On Hold if status is updated to Closed/ On Hold.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
