<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>EP_CRM_Notify_ContractOpportunity_Owner_for_Contract_Expiry_Date</fullName>
        <description>EP CRM Notify ContractOpportunity Owner for Contract Expiry Date</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EP_CRM_Email_Temp_Folder/EP_CRM_Notification_to_Contract_s_Opportunity_Owner</template>
    </alerts>
    <alerts>
        <fullName>EP_CRM_Notify_Contract_AccountOwner_s_Manager_for_Contract_Expiry_Date</fullName>
        <description>EP CRM Notify Contract AccountOwner&apos;s Manager for Contract Expiry Date</description>
        <protected>false</protected>
        <recipients>
            <field>Account_Owner_s_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EP_CRM_Email_Temp_Folder/EP_CRM_Notification_to_Contract_s_Account_Owner_s_Manager</template>
    </alerts>
    <alerts>
        <fullName>EP_CRM_Notify_Contract_Account_Owner_for_Contract_Expiry_Date</fullName>
        <description>EP CRM Notify Contract Account Owner for Contract Expiry Date</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EP_CRM_Email_Temp_Folder/EP_CRM_Notification_to_Contract_s_Account_Owner</template>
    </alerts>
    <alerts>
        <fullName>EP_CRM_Notify_Contract_OpportOwner_s_Manager_for_Contract_Expiry_Date</fullName>
        <description>EP CRM Notify Contract OpportOwner&apos;s Manager for Contract Expiry Date</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_s_Owner_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EP_CRM_Email_Temp_Folder/EP_CRM_Notification_to_Contract_s_Account_Owner_s_Manager</template>
    </alerts>
    <alerts>
        <fullName>EP_CRM_Notify_Contract_Owner_for_Contract_Expiry_Date</fullName>
        <description>EP CRM Notify Contract Owner for Contract Expiry Date</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EP_CRM_Email_Temp_Folder/EP_CRM_Notification_to_Contract_Owner</template>
    </alerts>
    <alerts>
        <fullName>EP_CRM_Notify_Contract_Owner_s_Manager_for_Contract_Expiry_Date</fullName>
        <description>EP CRM Notify Contract Owner&apos;s Manager for Contract Expiry Date</description>
        <protected>false</protected>
        <recipients>
            <field>EP_CRM_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EP_CRM_Email_Temp_Folder/EP_CRM_Notification_to_Contract_Owner_s_Manager</template>
    </alerts>
    <fieldUpdates>
        <fullName>EP_CRM_Update_Accountowner_manager_email</fullName>
        <field>Account_Owner_s_Manager_Email__c</field>
        <formula>Account.Owner.Manager.Email</formula>
        <name>EP CRM Update Accountowner manager email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Update_Manager_Name</fullName>
        <description>Field update to store contract owner&apos;s manager name.</description>
        <field>EP_CRM_Manager_Name__c</field>
        <formula>Owner.Manager.FirstName  &amp;  Owner.Manager.LastName</formula>
        <name>EP CRM Update Manager Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Update_Opportowner_manager_email</fullName>
        <field>Opportunity_s_Owner_Manager_Email__c</field>
        <formula>Opportunity__r.Owner.Email</formula>
        <name>EP CRM Update Opportowner manager email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Update_Opportunity_owner_email</fullName>
        <field>Opportunity_s_Owner_Manager_Email__c</field>
        <formula>Opportunity__r.Owner.Manager.Email</formula>
        <name>EP CRM Update Opportunity owner email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Update_manager_email</fullName>
        <description>Field update to update owner&apos;s manager&apos;s email ID.</description>
        <field>EP_CRM_Manager_Email__c</field>
        <formula>Owner.Manager.Email</formula>
        <name>EP CRM Update manager email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>EP CRM Update Account and Opportunity Owner%27s details</fullName>
        <actions>
            <name>EP_CRM_Update_Accountowner_manager_email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>EP_CRM_Update_Opportowner_manager_email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>EP_CRM_Update_Opportunity_owner_email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>RecordType.DeveloperName = $Label.EP_CRM_Contract_RecordType</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EP CRM Update Manager Email</fullName>
        <actions>
            <name>EP_CRM_Update_Manager_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>EP_CRM_Update_manager_email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>RecordType.DeveloperName = $Label.EP_CRM_Contract_RecordType</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
