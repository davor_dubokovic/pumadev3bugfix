<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_CER_Status_as_Pending_Approval</fullName>
        <description>For Synced CER set the status to pending approval Defect 83333</description>
        <field>EP_Status__c</field>
        <literalValue>Pending Approval</literalValue>
        <name>Set CER Status as Pending Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set CER status to Pending Approval</fullName>
        <actions>
            <name>Set_CER_Status_as_Pending_Approval</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When CER is synced set the status of CER to pending approval, Defect 83333</description>
        <formula>AND(ISCHANGED(EP_Integration_Status__c),ISPICKVAL( EP_Integration_Status__c, &apos;SYNC&apos;),ISPICKVAL(EP_Status__c, &apos;&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
