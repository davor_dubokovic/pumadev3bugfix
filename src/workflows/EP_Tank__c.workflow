<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>EP_Notify_Sell_To_Account_Owner_When_Tank_Status_Changes</fullName>
        <description>Notify Sell To Account Owner When Tank Status Changes</description>
        <protected>false</protected>
        <recipients>
            <field>EP_Tank_Parent_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EP_Account_Notify_Sell_To_Owner_when_Tank_Status_Changes</template>
    </alerts>
    <fieldUpdates>
        <fullName>EP_Populate_Sell_To_Owner_Email</fullName>
        <field>EP_Tank_Parent_Owner_Email__c</field>
        <formula>EP_Ship_To__r.Parent.Owner.Email</formula>
        <name>Populate Sell To Owner Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_Update_Tank_BlockReason_To_Planned</fullName>
        <description>This is created to fix defect #72192.</description>
        <field>EP_Reason_Blocked__c</field>
        <literalValue>Planned Maintenance</literalValue>
        <name>Update Tank Block Reason To Planned Main</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_Update_Tank_Status_to_Operational</fullName>
        <field>EP_Tank_Status__c</field>
        <literalValue>Operational</literalValue>
        <name>Update Tank Status to Operational</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_Update_Tank_Status_to_Stopped</fullName>
        <field>EP_Tank_Status__c</field>
        <literalValue>Stopped</literalValue>
        <name>Update Tank Status to Stopped</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_tank_status_to_operational</fullName>
        <description>defect 78513 changes</description>
        <field>EP_Tank_Status__c</field>
        <literalValue>Operational</literalValue>
        <name>Set tank status to operational</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>EP Update Tank Status to Operational</fullName>
        <active>true</active>
        <formula>NOT( ISBLANK(  EP_Block_Till__c ) ) &amp;&amp; NOT( ISPICKVAL( EP_Tank_Status__c , &apos;Operational&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>EP_Update_Tank_Status_to_Operational</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>EP_Tank__c.EP_Block_Till__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>EP Update Tank Status to Stopped</fullName>
        <active>true</active>
        <formula>NOT( ISBLANK( EP_Block_From__c ) ) &amp;&amp; NOT( ISPICKVAL( EP_Tank_Status__c , &apos;Stopped&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>EP_Update_Tank_Status_to_Stopped</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>EP_Tank__c.EP_Block_From__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>EP_Notify_SellTo_Account_Owner_Tank_Changes</fullName>
        <actions>
            <name>EP_Notify_Sell_To_Account_Owner_When_Tank_Status_Changes</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify Sell to account owner when tank status changes.</description>
        <formula>AND(ISCHANGED( EP_Tank_Status__c ),OR( TEXT(PRIORVALUE(EP_Tank_Status__c ))=&apos;Operational&apos;, TEXT(PRIORVALUE(EP_Tank_Status__c ))=&apos;Decomissioned&apos;, TEXT(PRIORVALUE(EP_Tank_Status__c ))=&apos;Stopped&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EP_Populate_Sell_To_Owner_Email</fullName>
        <actions>
            <name>EP_Populate_Sell_To_Owner_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Tank as operational</fullName>
        <actions>
            <name>Set_tank_status_to_operational</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EP_Tank__c.EP_Integration_Status__c</field>
            <operation>equals</operation>
            <value>SYNC</value>
        </criteriaItems>
        <criteriaItems>
            <field>EP_Tank__c.EP_Tank_Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>defect 78513</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
