<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>EP_Notify_Change_Request_Owner</fullName>
        <description>Notify Change Request Owner</description>
        <protected>false</protected>
        <recipients>
            <field>EP_Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EP_Notify_Change_Request_Owner</template>
    </alerts>
    <alerts>
        <fullName>EP_Notify_Queue</fullName>
        <description>Notification of Change Request to the Queue</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EP_Notify_Change_Request_Queue</template>
    </alerts>
    <rules>
        <fullName>EP_Notify_Account_Owner_After_CR_Completion</fullName>
        <actions>
            <name>EP_Notify_Change_Request_Owner</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>EP_ChangeRequest__c.EP_Request_Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EP_Notify_Requestor_After_CR_Completion_Or_Rejection</fullName>
        <actions>
            <name>EP_Notify_Change_Request_Owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Defect 82524</description>
        <formula>AND( EP_Requestor__r.Contact.EP_Send_Change_Request_Notification__c = true, ISCHANGED(EP_Request_Status__c), OR(ISPICKVAL(EP_Request_Status__c, &apos;Completed&apos;) ,ISPICKVAL(EP_Request_Status__c, &apos;Rejected&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
