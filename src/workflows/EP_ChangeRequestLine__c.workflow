<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>EP_Do_not_commit_changes</fullName>
        <description>Update do not commit changes</description>
        <field>EP_Do_not_commit_changes__c</field>
        <literalValue>1</literalValue>
        <name>Do not commit changes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_Reason_for_Rejection</fullName>
        <description>Reset reason for rejection when status is approved</description>
        <field>EP_Reason_For_Rejection__c</field>
        <name>EP_Reason for Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Do Not Commit Changes</fullName>
        <actions>
            <name>EP_Do_not_commit_changes</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EP_ChangeRequestLine__c.EP_Review_Step__c</field>
            <operation>equals</operation>
            <value>Credit Review</value>
        </criteriaItems>
        <description>Check Do Not commit changes if Review Name is Credit Review</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EP_Reset_Rejection_Reason</fullName>
        <actions>
            <name>EP_Reason_for_Rejection</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Reset rejection reason to blank when status changes to approved</description>
        <formula>AND(ISCHANGED(EP_Request_Status__c ),UPPER(TEXT(PRIORVALUE( EP_Request_Status__c )))=&apos;REJECTED&apos;,UPPER(TEXT( EP_Request_Status__c ))&lt;&gt;&apos;REJECTED&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
