<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>EP_CRM_Notify_acc_owner_whenever_opp_is_created_on_acc_where_acc_owner_is_not_sa</fullName>
        <description>EP CRM Notify acc owner whenever opp is created on acc where acc owner is not same as opp owner</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EP_CRM_Email_Temp_Folder/EP_CRM_Notify_account_owner_when_a_new_oppty_is_created_on_that_account</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Closed_Won</fullName>
        <description>Opportunity Closed Won</description>
        <protected>false</protected>
        <recipients>
            <recipient>sandra.garcia@pumaenergy.com.na</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EP_CRM_Email_Temp_Folder/Opportunity_Closed_Won</template>
    </alerts>
    <fieldUpdates>
        <fullName>EP_CRM_Count_Total_On_Analysis_Days</fullName>
        <description>EP CRM Count Total On Analysis Days</description>
        <field>EP_CRM_Total_Days_in_Analysis_Stage__c</field>
        <formula>IF((ISBLANK( PRIORVALUE( EP_CRM_Total_Days_in_Analysis_Stage__c )) || ISNULL(PRIORVALUE(EP_CRM_Total_Days_in_Analysis_Stage__c )) ) &amp;&amp; (RecordType.DeveloperName == $Label.EP_CRM_Opportunity_RECTYPE ), 0, PRIORVALUE(EP_CRM_Total_Days_in_Analysis_Stage__c ) +( NOW() - EP_CRM_Analysis_Date__c ))</formula>
        <name>EP CRM Count Total On Analysis Days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Count_Total_On_Closed_Lost_Days</fullName>
        <description>Calculates the days when Opportunity Stage was Closed Lost</description>
        <field>EP_CRM_Total_Days_in_Closed_Lost_Stage__c</field>
        <formula>IF((ISBLANK( PRIORVALUE( EP_CRM_Total_Days_in_Closed_Lost_Stage__c )) || ISNULL(PRIORVALUE(EP_CRM_Total_Days_in_Closed_Lost_Stage__c )) ) &amp;&amp; (RecordType.DeveloperName == $Label.EP_CRM_Opportunity_RECTYPE ), 0, PRIORVALUE(EP_CRM_Total_Days_in_Closed_Lost_Stage__c ) +( NOW() - EP_CRM_Closed_Lost_Date__c ))</formula>
        <name>EP CRM Count Total On Closed Lost Days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Count_Total_On_Closed_Won_Days</fullName>
        <description>Calculates the days when Opportunity Stage was Closed Won</description>
        <field>EP_CRM_Total_Days_in_Closed_Won_Stage__c</field>
        <formula>IF((ISBLANK( PRIORVALUE( EP_CRM_Total_Days_in_Closed_Won_Stage__c )) || ISNULL(PRIORVALUE(EP_CRM_Total_Days_in_Closed_Won_Stage__c )) ) &amp;&amp; (RecordType.DeveloperName == $Label.EP_CRM_Opportunity_RECTYPE ), 0, PRIORVALUE(EP_CRM_Total_Days_in_Closed_Won_Stage__c ) +( NOW() - EP_CRM_Closed_Won_Date__c ))</formula>
        <name>EP CRM Count Total On Closed Won Days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Count_Total_On_Commit_Days</fullName>
        <description>Calculates the days when Opportunity Stage was Commit</description>
        <field>EP_CRM_Total_Days_in_Commit_Stage__c</field>
        <formula>IF((ISBLANK( PRIORVALUE( EP_CRM_Total_Days_in_Commit_Stage__c )) || ISNULL(PRIORVALUE(EP_CRM_Total_Days_in_Commit_Stage__c )) ) &amp;&amp; (RecordType.DeveloperName == $Label.EP_CRM_Opportunity_RECTYPE ), 0, PRIORVALUE(EP_CRM_Total_Days_in_Commit_Stage__c ) +( NOW() - EP_CRM_Commit_Date__c ))</formula>
        <name>EP CRM Count Total On Commit Days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Count_Total_On_Negotiation_Days</fullName>
        <description>Calculates the days when Opportunity Stage was Prospecting</description>
        <field>EP_CRM_Total_Days_in_Negotiation_Stage__c</field>
        <formula>IF((ISBLANK( PRIORVALUE( EP_CRM_Total_Days_in_Negotiation_Stage__c )) || ISNULL(PRIORVALUE(EP_CRM_Total_Days_in_Negotiation_Stage__c )) ) &amp;&amp; (RecordType.DeveloperName == $Label.EP_CRM_Opportunity_RECTYPE ), 0, PRIORVALUE(EP_CRM_Total_Days_in_Negotiation_Stage__c ) +( NOW() - EP_CRM_Negotiation_Date__c ))</formula>
        <name>EP CRM Count Total On Negotiation Days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Count_Total_On_Proposal_Days</fullName>
        <description>EP CRM Count Total On Proposal Days</description>
        <field>EP_CRM_Total_Days_in_Proposal_Stage__c</field>
        <formula>IF((ISBLANK( PRIORVALUE( EP_CRM_Total_Days_in_Proposal_Stage__c )) || ISNULL(PRIORVALUE(EP_CRM_Total_Days_in_Proposal_Stage__c )) ) &amp;&amp; (RecordType.DeveloperName == $Label.EP_CRM_Opportunity_RECTYPE ), 0, PRIORVALUE(EP_CRM_Total_Days_in_Proposal_Stage__c ) +( NOW() - EP_CRM_Proposal_Date__c ))</formula>
        <name>EP CRM Count Total On Proposal Days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Count_Total_On_Qualification_Days</fullName>
        <description>Calculates the days when Opportunity Stage was Qalification</description>
        <field>EP_CRM_Total_Days_in_Qualification_Stage__c</field>
        <formula>IF((ISBLANK( PRIORVALUE( EP_CRM_Total_Days_in_Qualification_Stage__c )) || ISNULL(PRIORVALUE(EP_CRM_Total_Days_in_Qualification_Stage__c )) ) &amp;&amp; (RecordType.DeveloperName == $Label.EP_CRM_Opportunity_RECTYPE ), 0, PRIORVALUE(EP_CRM_Total_Days_in_Qualification_Stage__c ) +( NOW() - EP_CRM_Qualification_Date__c ))</formula>
        <name>EP CRM Count Total On Qualification Days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Count_Total_Prospecting_Days</fullName>
        <description>Calculates the days when Opportunity Stage was Prospecting</description>
        <field>EP_CRM_Total_Days_in_Prospecting_Stage__c</field>
        <formula>IF((ISBLANK( PRIORVALUE( EP_CRM_Total_Days_in_Prospecting_Stage__c )) || ISNULL(PRIORVALUE(EP_CRM_Total_Days_in_Prospecting_Stage__c   ))  ) &amp;&amp; (RecordType.DeveloperName ==  $Label.EP_CRM_Opportunity_RECTYPE  ), 0, PRIORVALUE(EP_CRM_Total_Days_in_Prospecting_Stage__c ) +( NOW() - EP_CRM_Prospecting_Date__c ))</formula>
        <name>EP CRM Count Total Prospecting Days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Delete_Credit_Approved_Date</fullName>
        <field>EP_CRM_Credit_Approved_Date__c</field>
        <name>EP CRM Delete Credit Approved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Delete_Credit_Requested_Date</fullName>
        <field>EP_CRM_Credit_Approval_Requested_Date__c</field>
        <name>EP CRM Delete Credit Requested Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Delete_KYC_Approved_Date</fullName>
        <field>EP_CRM_KYC_Approved_Date__c</field>
        <name>EP CRM Delete KYC Approved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Delete_KYC_Requested_Date</fullName>
        <field>EP_CRM_KYC_Requested_Date__c</field>
        <name>EP CRM Delete KYC Requested Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_KYC_Requested_Date</fullName>
        <description>Update KYC Requested Date when KYC is requested.</description>
        <field>EP_CRM_KYC_Requested_Date__c</field>
        <formula>IF(ISBLANK(EP_CRM_KYC_Requested_Date__c), TODAY(), EP_CRM_KYC_Requested_Date__c)</formula>
        <name>EP_CRM_KYC_Requested_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Update_Analysis_Date</fullName>
        <description>Update Analysis Date when stage is &quot;Analysis&quot;</description>
        <field>EP_CRM_Analysis_Date__c</field>
        <formula>now()</formula>
        <name>EP_CRM_Update_Analysis_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Update_Closed_Lost_Date</fullName>
        <description>Update Closed Lost Date when opportunity Stage is Closed Lost</description>
        <field>EP_CRM_Closed_Lost_Date__c</field>
        <formula>now()</formula>
        <name>EP_CRM_Update_Closed_Lost_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Update_Closed_won_Date</fullName>
        <description>Update Closed Won Date on Opportunity when stage is Closed Won</description>
        <field>EP_CRM_Closed_Won_Date__c</field>
        <formula>now()</formula>
        <name>EP_CRM_Update_Closed_won_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Update_Commit_Date</fullName>
        <field>EP_CRM_Commit_Date__c</field>
        <formula>now()</formula>
        <name>EP_CRM_Update_Commit_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Update_CreditApprovalRequest_Date</fullName>
        <description>Update Credit Approval Requested Date when credit approval is requested.</description>
        <field>EP_CRM_Credit_Approval_Requested_Date__c</field>
        <formula>NOW()</formula>
        <name>EP_CRM_Update_CreditApprovalRequest_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Update_Credit_Approved_Date</fullName>
        <description>Update Credit Approved Date when credit is approved.</description>
        <field>EP_CRM_Credit_Approved_Date__c</field>
        <formula>NOW()</formula>
        <name>EP_CRM_Update_Credit_Approved_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Update_KYC_Approved_Date</fullName>
        <description>Update KYC Approved Date when KYC is approved.</description>
        <field>EP_CRM_KYC_Approved_Date__c</field>
        <formula>IF(ISBLANK(EP_CRM_KYC_Approved_Date__c), TODAY(), EP_CRM_KYC_Approved_Date__c)</formula>
        <name>EP_CRM_Update_KYC_Approved_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Update_Negotition_Date</fullName>
        <description>Update Negotiation Date when stage is Negotiation</description>
        <field>EP_CRM_Negotiation_Date__c</field>
        <formula>now()</formula>
        <name>EP_CRM_Update_Negotition_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Update_Opportunity_Quantity</fullName>
        <description>Field update to update opportunity quantity with volume field data.</description>
        <field>TotalOpportunityQuantity</field>
        <formula>EP_CRM_Volume__c</formula>
        <name>EP_CRM_Update Opportunity Quantity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Update_Proposal_Price_Date</fullName>
        <description>Update Proposal/Price Date on Opportunity when stage is Proposal/Price Quote</description>
        <field>EP_CRM_Proposal_Date__c</field>
        <formula>now()</formula>
        <name>EP_CRM_Update_Proposal_Price_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Update_Prospecting_Date</fullName>
        <description>Update field on Opportunity with Prospecting date when stage is &quot;Prospecting&quot;</description>
        <field>EP_CRM_Prospecting_Date__c</field>
        <formula>now()</formula>
        <name>EP_CRM_Update_Prospecting_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_CRM_Update_Qualification_Date</fullName>
        <field>EP_CRM_Qualification_Date__c</field>
        <formula>now()</formula>
        <name>EP_CRM_Update Qualification Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Activated_Contract_Checkbox</fullName>
        <field>Is_Activated_Contract__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Activated Contract Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Last_Activity_Date</fullName>
        <field>EP_CRM_Last_Activity_Date__c</field>
        <name>Update Last Activity Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>EP CRM Auto Notification Of New Opp To Account Owner</fullName>
        <actions>
            <name>EP_CRM_Notify_acc_owner_whenever_opp_is_created_on_acc_where_acc_owner_is_not_sa</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Auto notify account owner whenever new opportunity is created for an account where opp owner is not same as account owner</description>
        <formula>OwnerId  &lt;&gt;  Account.OwnerId &amp;&amp; RecordType.DeveloperName== $Label.EP_CRM_Opportunity_RECTYPE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EP CRM Delete Credit Approval Requested Date</fullName>
        <actions>
            <name>EP_CRM_Delete_Credit_Requested_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.EP_CRM_Credit_Approval_requested__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>CRM Opportunity</value>
        </criteriaItems>
        <description>If Credit Approval Requested is unchecked then Credit Approval Requested Date value should be deleted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EP CRM Delete Credit Approved Date</fullName>
        <actions>
            <name>EP_CRM_Delete_Credit_Approved_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.EP_CRM_Credit_Approval_Received__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>CRM Opportunity</value>
        </criteriaItems>
        <description>If Credit Approved is unchecked then Credit Approved Date value should be deleted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EP CRM Delete KYC Approved Date</fullName>
        <actions>
            <name>EP_CRM_Delete_KYC_Approved_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.EP_CRM_KYC_Done__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>CRM Opportunity</value>
        </criteriaItems>
        <description>If KYC Approved is unchecked then KYC Approved Date value should be deleted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EP CRM Delete KYC Requested Date</fullName>
        <actions>
            <name>EP_CRM_Delete_KYC_Requested_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.EP_CRM_KYC_Requested__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>CRM Opportunity</value>
        </criteriaItems>
        <description>If KYC Requested is unchecked then KYC Requested Date value should be deleted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EP CRM Reset Flags on Clone</fullName>
        <actions>
            <name>Uncheck_Activated_Contract_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Last_Activity_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EP CRM Update Analysis Date on Opportunity</fullName>
        <actions>
            <name>EP_CRM_Update_Analysis_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>CRM Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Analysis</value>
        </criteriaItems>
        <description>Capture date when opportunity stage is Analysis</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EP CRM Update Closed Lost Date on Opportunity</fullName>
        <actions>
            <name>EP_CRM_Update_Closed_Lost_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>CRM Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <description>Capture date when opportunity stage is Closed Lost</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EP CRM Update Closed Won Date on Opportunity</fullName>
        <actions>
            <name>EP_CRM_Update_Closed_won_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>CRM Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <description>Capture date when opportunity stage is Closed Won</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EP CRM Update Commit Date on Opportunity</fullName>
        <actions>
            <name>EP_CRM_Update_Commit_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>CRM Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Commit</value>
        </criteriaItems>
        <description>Capture date when opportunity stage is Commit</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EP CRM Update Credit Approval Requested Date on Opportunity</fullName>
        <actions>
            <name>EP_CRM_Update_CreditApprovalRequest_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.EP_CRM_Credit_Approval_requested__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>CRM Opportunity</value>
        </criteriaItems>
        <description>Capture date when credit approval was requested.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EP CRM Update Credit Approved Date on Opportunity</fullName>
        <actions>
            <name>EP_CRM_Update_Credit_Approved_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>CRM Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.EP_CRM_Credit_Approval_Received__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Capture date when credit was approved.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EP CRM Update KYC Approved Date on Opportunity</fullName>
        <actions>
            <name>EP_CRM_Update_KYC_Approved_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>CRM Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.EP_CRM_KYC_Done__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Capture date when KYC was approved.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EP CRM Update KYC Requested Date on Opportunity</fullName>
        <actions>
            <name>EP_CRM_KYC_Requested_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>CRM Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.EP_CRM_KYC_Requested__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Capture date when KYC was requested.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EP CRM Update Negotiation Date on Opportunity</fullName>
        <actions>
            <name>EP_CRM_Update_Negotition_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>CRM Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Negotiation</value>
        </criteriaItems>
        <description>Capture date when opportunity stage is Negotiation</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EP CRM Update Proposal%2FPrice Date on Opportunity</fullName>
        <actions>
            <name>EP_CRM_Update_Proposal_Price_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>CRM Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Proposal / Price Quote</value>
        </criteriaItems>
        <description>Capture date when opportunity stage is Proposal/Price</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EP CRM Update Prospecting Date on Opportunity</fullName>
        <actions>
            <name>EP_CRM_Update_Prospecting_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>CRM Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Prospecting</value>
        </criteriaItems>
        <description>Capture date when opportunity stage is Prospecting</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EP CRM Update Qualification Date on Opportunity</fullName>
        <actions>
            <name>EP_CRM_Update_Qualification_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>CRM Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Qualification</value>
        </criteriaItems>
        <description>Capture date when opportunity stage is Qualification</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EP_CRM_Opportunity Volume Updated</fullName>
        <actions>
            <name>EP_CRM_Update_Opportunity_Quantity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow rule to update opportunity quantity with Volume field data</description>
        <formula>AND( RecordType.DeveloperName = $Label.EP_CRM_Opportunity_RECTYPE, OR(ISCHANGED(EP_CRM_Volume__c),NOT(ISNULL(EP_CRM_Volume__c))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EP_CRM_Update_Total_Analysis_Days</fullName>
        <actions>
            <name>EP_CRM_Count_Total_On_Analysis_Days</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update field on Opportunity to capture how many days Opportunity was in Analysis Stage</description>
        <formula>AND ( ISCHANGED (StageName), ISPICKVAL( PRIORVALUE (StageName),   &apos;Analysis&apos;),  (RecordType.DeveloperName == 	 $Label.EP_CRM_Opportunity_RECTYPE ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EP_CRM_Update_Total_ClosedLost_Days</fullName>
        <actions>
            <name>EP_CRM_Count_Total_On_Closed_Lost_Days</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update field on Opportunity to capture how many days Opportunity was in Qualification Stage</description>
        <formula>AND ( ISCHANGED (StageName), ISPICKVAL( PRIORVALUE (StageName),   &apos;Qualification&apos;),  (RecordType.DeveloperName == 	 $Label.EP_CRM_Opportunity_RECTYPE ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EP_CRM_Update_Total_ClosedWon_Days</fullName>
        <actions>
            <name>EP_CRM_Count_Total_On_Closed_Won_Days</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update field on Opportunity to capture how many days Opportunity was in Closed Won Stage</description>
        <formula>AND ( ISCHANGED (StageName), ISPICKVAL( PRIORVALUE (StageName),   &apos;Closed Won&apos;),  (RecordType.DeveloperName == 	 $Label.EP_CRM_Opportunity_RECTYPE ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EP_CRM_Update_Total_Commit_Days</fullName>
        <actions>
            <name>EP_CRM_Count_Total_On_Commit_Days</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update field on Opportunity to capture how many days Opportunity was in Commit Stage</description>
        <formula>AND ( ISCHANGED (StageName), ISPICKVAL( PRIORVALUE (StageName),   &apos;Commit&apos;),  (RecordType.DeveloperName == 	 $Label.EP_CRM_Opportunity_RECTYPE ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EP_CRM_Update_Total_Negotiation_Days</fullName>
        <actions>
            <name>EP_CRM_Count_Total_On_Negotiation_Days</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update field on Opportunity to capture how many days Opportunity was in Negotiation Stage</description>
        <formula>AND ( ISCHANGED (StageName), ISPICKVAL( PRIORVALUE (StageName),   &apos;Negotiation&apos;),  (RecordType.DeveloperName == 	 $Label.EP_CRM_Opportunity_RECTYPE ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EP_CRM_Update_Total_Proposal_Days</fullName>
        <actions>
            <name>EP_CRM_Count_Total_On_Proposal_Days</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update field on Opportunity to capture how many days Opportunity was in Proposal Stage</description>
        <formula>AND ( ISCHANGED (StageName), ISPICKVAL( PRIORVALUE (StageName),   &apos;Proposal / Price Quote&apos;),  (RecordType.DeveloperName == 	 $Label.EP_CRM_Opportunity_RECTYPE ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EP_CRM_Update_Total_Prospecting_Days</fullName>
        <actions>
            <name>EP_CRM_Count_Total_Prospecting_Days</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update field on Opportunity to capture how many days Opportunity was in Prospecting Stage</description>
        <formula>AND ( ISCHANGED (StageName), ISPICKVAL( PRIORVALUE (StageName),   &apos;Prospecting&apos;),  (RecordType.DeveloperName == 	 $Label.EP_CRM_Opportunity_RECTYPE ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EP_CRM_Update_Total_Qualification_Days</fullName>
        <actions>
            <name>EP_CRM_Count_Total_On_Qualification_Days</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update field on Opportunity to capture how many days Opportunity was in Qualification Stage</description>
        <formula>AND ( ISCHANGED (StageName), ISPICKVAL( PRIORVALUE (StageName),   &apos;Qualification&apos;),  (RecordType.DeveloperName == 	 $Label.EP_CRM_Opportunity_RECTYPE ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
