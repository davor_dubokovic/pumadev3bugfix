<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Delivery_From_Date</fullName>
        <description>Update Delivery From Date to Requested Date</description>
        <field>EP_Requested_Delivery_Date__c</field>
        <name>Update Delivery From Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>EP_Update_PricingDate</fullName>
        <field>EP_Pricing_Date__c</field>
        <formula>DATETIMEVALUE( Delivery_From_Date__c )</formula>
        <name>EP Update PricingDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>Update_Transporter_Old</fullName>
        <field>EP_Transporter_Old__c</field>
        <formula>CASESAFEID(PRIORVALUE(EP_Transporter__c))</formula>
        <name>Update Transporter Old</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updated_Delivery_From_Date</fullName>
        <field>Delivery_From_Date__c</field>
        <formula>EP_Requested_Delivery_Date__c</formula>
        <name>Update Delivery From Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>Update_Order_Status_to_Invoiced</fullName>
        <field>csord__Status2__c</field>
        <formula>&apos;Invoiced&apos;</formula>
        <name>Update Order Status to Invoiced</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
     <fieldUpdates>
        <fullName>EP_Update_Inventory_Approval_Status</fullName>
        <field>Inventory_Approval_Status__c</field>
        <literalValue>Initialize</literalValue>
        <name>EP Update Inventory Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_Inventory_Reject_Draft_Status_Update</fullName>
        <field>csord__Status2__c</field>
        <formula>&apos;Draft&apos;</formula>
        <name>EP Inventory Reject Draft Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
     <alerts>
        <fullName>SendCancellationEmail</fullName>
        <description>SendCancellationEmail</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EP_Order_Cancellation_Notification</template>
    </alerts>
    <rules>
        <fullName>EP Inventory Reject Draft Status Update</fullName>
        <actions>
            <name>EP_Inventory_Reject_Draft_Status_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Order__c.Inventory_Approval_Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EP Update Inventory Approval Status</fullName>
        <actions>
            <name>EP_Update_Inventory_Approval_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Order__c.Inventory_Approval_Status__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>  
    <rules>
        <fullName>SendEmailForCancelledOrder</fullName>
        <actions>
            <name>SendCancellationEmail</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Order__c.csord__Status2__c</field>
            <operation>equals</operation>
            <value>Cancelled</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.IsPortalEnabled</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EP Update Pricing Date</fullName>
        <actions>
            <name>EP_Update_PricingDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Order__c.Delivery_From_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
	<rules>
        <fullName>Update Transporter Old</fullName>
        <actions>
            <name>Update_Transporter_Old</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Updated Delivery From date</fullName>
        <actions>
            <name>Updated_Delivery_From_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Delivery From Date to Requested date</description>
        <formula>ISNULL(Delivery_From_Date__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
	  <rules>
        <fullName>Update Status to Invoiced</fullName>
        <actions>
            <name>Update_Order_Status_to_Invoiced</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Number_of_Invoice_Line_Items__c  =  Number_of_Line_Items__c &amp;&amp;    NOT(ISNULL(Number_of_Line_Items__c)) &amp;&amp; Number_of_Invoice_Line_Items__c &gt; 0</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>