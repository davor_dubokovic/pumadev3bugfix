<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>ePuma - This object is used to capture Freight Prices. The freight price records are linked back to a Freight Matrix record</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>EP_Distance_UOM__c</fullName>
        <externalId>false</externalId>
        <label>Distance UOM</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Km</fullName>
                    <default>false</default>
                    <label>Km</label>
                </value>
                <value>
                    <fullName>Mi</fullName>
                    <default>false</default>
                    <label>Mi</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>EP_Freight_Matrix__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Freight Matrix</label>
        <referenceTo>EP_Freight_Matrix__c</referenceTo>
        <relationshipLabel>Freight Prices</relationshipLabel>
        <relationshipName>Matrix_Freight_Prices</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Freight_Price__c</fullName>
        <description>Freight Price</description>
        <externalId>false</externalId>
        <label>Freight Price</label>
        <precision>18</precision>
        <required>true</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>EP_Max_Distance__c</fullName>
        <description>Maximum Distance</description>
        <externalId>false</externalId>
        <label>Max Distance</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Max_Volume__c</fullName>
        <description>Maximum Volume</description>
        <externalId>false</externalId>
        <label>Max Volume</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Min_Distance__c</fullName>
        <description>Minimum Distance</description>
        <externalId>false</externalId>
        <label>Min Distance</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Min_Volume__c</fullName>
        <externalId>false</externalId>
        <label>Min Volume</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Product</description>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Freight Prices</relationshipLabel>
        <relationshipName>Product_Freight_Prices</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Volume_UOM__c</fullName>
        <externalId>false</externalId>
        <label>Volume UOM</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>LT</fullName>
                    <default>false</default>
                    <label>LT</label>
                </value>
                <value>
                    <fullName>USGAL</fullName>
                    <default>false</default>
                    <label>USGAL</label>
                </value>
                <value>
                    <fullName>IMPGAL</fullName>
                    <default>false</default>
                    <label>IMPGAL</label>
                </value>
                <value>
                    <fullName>HL</fullName>
                    <default>false</default>
                    <label>HL</label>
                </value>
                <value>
                    <fullName>M3</fullName>
                    <default>false</default>
                    <label>M3</label>
                </value>
                <value>
                    <fullName>KG</fullName>
                    <default>false</default>
                    <label>KG</label>
                </value>
                <value>
                    <fullName>MT</fullName>
                    <default>false</default>
                    <label>MT</label>
                </value>
                <value>
                    <fullName>PC</fullName>
                    <default>false</default>
                    <label>PC</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Freight Price</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>FPI-{000000}</displayFormat>
        <label>Freight Price ID</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Freight Prices</pluralLabel>
    <searchLayouts/>
    <sharingModel>Read</sharingModel>
    <validationRules>
        <fullName>EP_VAL001_Min_Vol_OR_Dist_required</fullName>
        <active>true</active>
        <description>Minimum distance or volume is required</description>
        <errorConditionFormula>AND(ISBLANK( EP_Min_Distance__c ),ISBLANK( EP_Min_Volume__c ))</errorConditionFormula>
        <errorMessage>Either Min Distance or Min Volume has to be populated</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>EP_VAL002_Max_Vol_OR_Dist_required</fullName>
        <active>true</active>
        <description>Maximum distance or volume is required</description>
        <errorConditionFormula>AND(ISBLANK( EP_Max_Distance__c ),ISBLANK( EP_Max_Volume__c ))</errorConditionFormula>
        <errorMessage>Either Maximum Distance or Maximum Volume has to be populated</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>EP_VAL003_Max_Dist_Less_Than_Min_Dist</fullName>
        <active>true</active>
        <description>Max Distance should not be less than the Min Distance</description>
        <errorConditionFormula>EP_Max_Distance__c  &lt;  EP_Min_Distance__c</errorConditionFormula>
        <errorMessage>Max Distance should not be less than the Min Distance</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>EP_VAL004_Max_Vol_Less_Than_Min_Vol</fullName>
        <active>true</active>
        <description>Max Vulume should not be less than the Min Volume</description>
        <errorConditionFormula>EP_Max_Volume__c  &lt;  EP_Min_Volume__c</errorConditionFormula>
        <errorMessage>Max Volume should not be less than the Min Volume</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>EP_VAL005_Distance_UOM_Required</fullName>
        <active>true</active>
        <description>Distance UOM Mandatory if Min Distance , Max distance is populated</description>
        <errorConditionFormula>AND(NOT(AND(ISBLANK( EP_Min_Distance__c ),ISBLANK( EP_Max_Distance__c ))), ISPICKVAL(EP_Distance_UOM__c,&apos;&apos;) )</errorConditionFormula>
        <errorMessage>Distance UOM required if Min Distance or Max distance is populated</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>EP_VAL006_Volume_UOM_Required</fullName>
        <active>true</active>
        <description>Volume UOM is required if Min or Max volume is specified</description>
        <errorConditionFormula>AND(NOT(AND(ISBLANK( EP_Min_Volume__c ),ISBLANK( EP_Max_Volume__c ))), ISPICKVAL( EP_Volume_UOM__c ,&apos;&apos;) )</errorConditionFormula>
        <errorMessage>Volume UOM is required if Min or Max volume is specified</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
