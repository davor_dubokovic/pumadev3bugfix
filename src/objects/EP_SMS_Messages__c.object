<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>ePuma - This object is used to capture the SMS messages pushed to the VMI site managers through Twilio.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>EP_Mobile_Phone__c</fullName>
        <description>This field is used to store the mobile phone that the SMS is sent to</description>
        <externalId>false</externalId>
        <label>Mobile Phone</label>
        <length>15</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Notification_Type__c</fullName>
        <description>This field is used to define the type of notification that the SMS Message record will generate</description>
        <externalId>false</externalId>
        <label>Notification Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Notification 1</fullName>
                    <default>false</default>
                    <label>Notification 1</label>
                </value>
                <value>
                    <fullName>Notification 2</fullName>
                    <default>false</default>
                    <label>Notification 2</label>
                </value>
                <value>
                    <fullName>Notification 3</fullName>
                    <default>false</default>
                    <label>Notification 3</label>
                </value>
                <value>
                    <fullName>Notification 4</fullName>
                    <default>false</default>
                    <label>Notification 4</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>EP_SMS_Message__c</fullName>
        <externalId>false</externalId>
        <label>SMS Message</label>
        <length>160</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Ship_To__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Ship-To</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>SMS Messages</relationshipLabel>
        <relationshipName>SMS_Messages</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>SMS Message</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>EP_Mobile_Phone__c</columns>
        <columns>EP_Ship_To__c</columns>
        <columns>EP_SMS_Message__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>EP_All_SMS_Notifications</fullName>
        <columns>NAME</columns>
        <columns>EP_Notification_Type__c</columns>
        <columns>EP_Ship_To__c</columns>
        <columns>EP_Mobile_Phone__c</columns>
        <columns>EP_SMS_Message__c</columns>
        <filterScope>Everything</filterScope>
        <label>All SMS Notifications</label>
    </listViews>
    <nameField>
        <displayFormat>SMS-{000000}</displayFormat>
        <label>SMS Message ID</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>SMS Messages</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
