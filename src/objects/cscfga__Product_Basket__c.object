<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Basket to supercede bundles...</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fieldSets>
        <fullName>csbb__Custom_Field_Set</fullName>
        <description>Used to incorporate custom fields into the Basket Builder page. Add fields to this fieldset to display them in the page.</description>
        <label>Custom Field Set</label>
    </fieldSets>
    <fields>
        <fullName>csbb__Accessibility_Status__c</fullName>
        <deprecated>false</deprecated>
        <description>Status of accessibility of the basket. The basket gets blocked when it is in the process of cloning (whether it is cloned or the clone itself).</description>
        <externalId>false</externalId>
        <inlineHelpText>Status of accessibility of the basket. The basket gets blocked when it is in the process of cloning (whether it is cloned or the clone itself).</inlineHelpText>
        <label>Accessibility Status</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>csbb__Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Product Baskets</relationshipLabel>
        <relationshipName>Product_Baskets</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>csbb__Includes_High_Level_Products__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Includes High Level Products</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>csbb__Optionals__c</fullName>
        <deprecated>false</deprecated>
        <description>Additional information on the Product Basket record, used by CSBB.</description>
        <externalId>false</externalId>
        <inlineHelpText>Additional information on the Product Basket record, used by CSBB.</inlineHelpText>
        <label>Optionals</label>
        <length>4096</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>csbb__Synchronised_With_Opportunity__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Synchronised With Opportunity</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>cscfga__Basket_Status__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Displays status of the Basket.</inlineHelpText>
        <label>Basket Status</label>
        <picklist>
            <picklistValues>
                <fullName>Is Valid</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Contains Errors</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ContainsExpiredProducts</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>cscfga__Opportunity__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Name of Opportunity this Basket is associated with.</inlineHelpText>
        <label>Opportunity</label>
        <referenceTo>Opportunity</referenceTo>
        <relationshipLabel>Product Baskets</relationshipLabel>
        <relationshipName>Product_Baskets</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>cscfga__Products_Id_Qty_In_Basket__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>List of product IDs contained in this Basket.</inlineHelpText>
        <label>Products Id Qty In Basket</label>
        <length>8000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>2</visibleLines>
    </fields>
    <fields>
        <fullName>cscfga__Products_In_Basket__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>List of Product Names contained in the Basket.</inlineHelpText>
        <label>Products Names In Basket</label>
        <length>4000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>2</visibleLines>
    </fields>
    <fields>
        <fullName>cscfga__Shared_Context_Configuration__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Product Configuration that is used as a Shared Context which is associated with this Basket.</inlineHelpText>
        <label>Shared Context Configuration</label>
        <referenceTo>cscfga__Product_Configuration__c</referenceTo>
        <relationshipLabel>Product Baskets</relationshipLabel>
        <relationshipName>Product_Baskets</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>cscfga__Total_Price__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Sum of non-recurring and one year&apos;s worth of recurring prices of all items contained in this Basket.</inlineHelpText>
        <label>Total Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>cscfga__User_Session__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Details about user session within which this Bundle was created.</inlineHelpText>
        <label>User Session</label>
        <referenceTo>cscfga__User_Session__c</referenceTo>
        <relationshipLabel>Product Baskets</relationshipLabel>
        <relationshipName>Product_Baskets</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>cscfga__total_contract_value__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Contract Value</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Product Baskets</relationshipLabel>
        <relationshipName>Product_Baskets</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Basket_Stage__c</fullName>
        <deprecated>false</deprecated>
        <description>In standalone basket scenario, use this picklist to trigger order generation</description>
        <externalId>false</externalId>
        <inlineHelpText>In standalone basket scenario, use this picklist to trigger order generation</inlineHelpText>
        <label>Basket Stage</label>
        <picklist>
            <picklistValues>
                <fullName>Prospecting</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Closed Won</fullName>
                <default>false</default>
            </picklistValues>
            <restrictedPicklist>true</restrictedPicklist>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Change_Type__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Change Type</label>
        <picklist>
            <picklistValues>
                <fullName>Add</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Upgrade</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Downgrade</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Relocation</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Order_Generation_Batch_Job_Id__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Order Generation Batch Job Id</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>csordtelcoa__Order_Under_Change__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Order that initiated creation of this product basket</description>
        <externalId>false</externalId>
        <inlineHelpText>Order that initiated creation of this product basket</inlineHelpText>
        <label>Order Under Change</label>
        <referenceTo>csord__Order__c</referenceTo>
        <relationshipLabel>Product Baskets</relationshipLabel>
        <relationshipName>Product_Baskets</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Previous_Product_Basket__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Product Basket that preceded this one. Used for in-flight changes</description>
        <externalId>false</externalId>
        <inlineHelpText>Product Basket that preceded this one. Used for in-flight changes</inlineHelpText>
        <label>Previous Product Basket</label>
        <referenceTo>cscfga__Product_Basket__c</referenceTo>
        <relationshipLabel>Following Product Baskets</relationshipLabel>
        <relationshipName>Following_Product_Baskets</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Process_Inflight_in_Batch__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Process Inflight in Batch</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Process_Order_Generation_In_Batch__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Indicates whether the opportunity is set to be processed in batch mode</description>
        <externalId>false</externalId>
        <inlineHelpText>Force batch processing of the opportunity</inlineHelpText>
        <label>Process Order Generation In Batch</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Product_Configuration_Clone_Batch_Job_Id__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Product Configuration Clone Batch Job Id</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>csordtelcoa__Product_Configuration_Clone_In_Batch__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Indicates whether the product configuration cloning is set to be processed in batch mode</description>
        <externalId>false</externalId>
        <inlineHelpText>Force generation of MACD basket in a batch</inlineHelpText>
        <label>Product Configuration Clone In Batch</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Subscription_To_Be_Added_To__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Subscription To Be Added To</label>
        <referenceTo>csord__Subscription__c</referenceTo>
        <relationshipLabel>Product Baskets (Subscription To Be Added To)</relationshipLabel>
        <relationshipName>Product_Baskets1</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Subscription_To_Be_Changed__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Subscription To Be Changed</label>
        <referenceTo>csord__Subscription__c</referenceTo>
        <relationshipLabel>Product Baskets</relationshipLabel>
        <relationshipName>Product_Baskets</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Synchronised_with_Opportunity__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Synchronised with Opportunity</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Product Basket</label>
    <listViews>
        <fullName>cscfga__All</fullName>
        <columns>NAME</columns>
        <columns>cscfga__User_Session__c</columns>
        <columns>CREATED_DATE</columns>
        <columns>LAST_UPDATE</columns>
        <columns>UPDATEDBY_USER</columns>
        <columns>cscfga__Total_Price__c</columns>
        <columns>cscfga__Opportunity__c</columns>
        <columns>cscfga__Basket_Status__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Product Basket Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Product Baskets</pluralLabel>
    <recordTypes>
        <fullName>csordtelcoa__Inflight_Change</fullName>
        <active>true</active>
        <description>Inflight change basket</description>
        <label>Inflight Change</label>
        <picklistValues>
            <picklist>cscfga__Basket_Status__c</picklist>
            <values>
                <fullName>Contains Errors</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>ContainsExpiredProducts</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Is Valid</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>csordtelcoa__Basket_Stage__c</picklist>
            <values>
                <fullName>Closed Won</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Prospecting</fullName>
                <default>true</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>csordtelcoa__Change_Type__c</picklist>
            <values>
                <fullName>Add</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Downgrade</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Relocation</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Upgrade</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
