<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Credit Memos will be created and managed in NAV. Credit Memos will be synced with Salesforce as and when they are created in NAV. Salesforce will store the Credit Memos in a Custom object call “Customer Credit Memo”.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>EP_Bill_To__c</fullName>
        <description>Master-Detail (Account)Filtered – (Only Sell-To or Bill-To)</description>
        <externalId>false</externalId>
        <label>Bill-To</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Account.RecordType.DeveloperName</field>
                <operation>equals</operation>
                <value>EP_Bill_To,EP_Sell_To</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Customer Credit Memos</relationshipLabel>
        <relationshipName>Customer_Credit_Memos</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>EP_Company__c</fullName>
        <description>Formula Fld. (Bill-To - Company)</description>
        <externalId>false</externalId>
        <formula>EP_Bill_To__r.EP_Account_Company_Name__c</formula>
        <label>Company</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Credit_Memo_Due_Date__c</fullName>
        <description>Date</description>
        <externalId>false</externalId>
        <label>Credit Memo Due Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>EP_Credit_Memo_Issue_Date__c</fullName>
        <description>Date</description>
        <externalId>false</externalId>
        <label>Credit Memo Issue Date</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>EP_Credit_Memo_Key__c</fullName>
        <caseSensitive>true</caseSensitive>
        <description>Credit Memo Key.Value generated on the fly:
Bill-To + “_” + Credit Memo Number + “_” + Credit Memo Issue Date</description>
        <externalId>true</externalId>
        <label>Credit Memo Key</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>EP_Credit_Memo_Number__c</fullName>
        <description>Unique (value pushed from NAV)</description>
        <externalId>false</externalId>
        <label>Credit Memo Number</label>
        <length>50</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Credit_Memo_Pull_Status__c</fullName>
        <description>Credit Memo Pull Status</description>
        <externalId>false</externalId>
        <label>Credit Memo Pull Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Incomplete</fullName>
                    <default>true</default>
                    <label>Incomplete</label>
                </value>
                <value>
                    <fullName>Completed</fullName>
                    <default>false</default>
                    <label>Completed</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>EP_Credit_Memo_Remaining_Balance__c</fullName>
        <description>Credit Memo Remaining Balance</description>
        <externalId>false</externalId>
        <label>Credit Memo Remaining Balance</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>EP_Credit_Memo_Status__c</fullName>
        <description>Credit Memo Status</description>
        <externalId>false</externalId>
        <label>Credit Memo Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Posted</fullName>
                    <default>true</default>
                    <label>Posted</label>
                </value>
                <value>
                    <fullName>Applied</fullName>
                    <default>false</default>
                    <label>Applied</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>EP_Credit_Memo_Total__c</fullName>
        <description>Credit Memo Total</description>
        <externalId>false</externalId>
        <label>Credit Memo Total</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>EP_Credit_Memo_Type__c</fullName>
        <description>If all Credit Memo Item G/L only then “Credit Memo Type” = “G/L Only” Else “Credit Memo Type” = “Item &amp; G/L”</description>
        <externalId>false</externalId>
        <label>Credit Memo Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Items &amp; G/L</fullName>
                    <default>false</default>
                    <label>Items &amp; G/L</label>
                </value>
                <value>
                    <fullName>G/L Only</fullName>
                    <default>false</default>
                    <label>G/L Only</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>EP_Customer_Number__c</fullName>
        <description>Formula Fld. (Bill-To – Customer Number)</description>
        <externalId>false</externalId>
        <formula>EP_Bill_To__r.AccountNumber</formula>
        <label>Customer Number</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Entry_Number__c</fullName>
        <description>Added field as per L4 #45304</description>
        <externalId>false</externalId>
        <label>Entry Number</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Invoice_Applied_To__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Lookup (Customer Invoice)</description>
        <externalId>false</externalId>
        <label>Invoice Applied To</label>
        <referenceTo>EP_Invoice__c</referenceTo>
        <relationshipLabel>Customer Credit Memos</relationshipLabel>
        <relationshipName>Customer_Credit_Memos</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Invoice_Issue_Date__c</fullName>
        <externalId>false</externalId>
        <formula>EP_Relates_To_Invoice__r.EP_Invoice_Issue_Date__c</formula>
        <label>Invoice Issue Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>EP_Is_Reversal__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Added field as per L4 #45304</description>
        <externalId>false</externalId>
        <label>Is Reversal</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>EP_NavSeqId__c</fullName>
        <description>Nav Sequence Id</description>
        <externalId>false</externalId>
        <label>NavSeqId</label>
        <length>70</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Payment_Term__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Payment Terms</label>
        <referenceTo>EP_Payment_Term__c</referenceTo>
        <relationshipLabel>Customer Credit Memos</relationshipLabel>
        <relationshipName>Customer_Credit_Memos</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Payment_Terms__c</fullName>
        <description>Payment Terms</description>
        <externalId>false</externalId>
        <label>Payment Terms</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Reason_for_Credit_Memo__c</fullName>
        <externalId>false</externalId>
        <label>Reason for Credit Memo</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Relates_To_Invoice__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Relates To Invoice</label>
        <referenceTo>EP_Invoice__c</referenceTo>
        <relationshipLabel>Customer Credit Memos (Relates To Invoice)</relationshipLabel>
        <relationshipName>Customer_Credit_Memos1</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Customer Credit Memo</label>
    <listViews>
        <fullName>EP_All_Customer_Credit_Memos</fullName>
        <columns>NAME</columns>
        <columns>EP_Credit_Memo_Number__c</columns>
        <columns>EP_Credit_Memo_Issue_Date__c</columns>
        <columns>EP_Credit_Memo_Due_Date__c</columns>
        <columns>EP_Bill_To__c</columns>
        <columns>EP_Credit_Memo_Total__c</columns>
        <columns>EP_Credit_Memo_Remaining_Balance__c</columns>
        <filterScope>Everything</filterScope>
        <label>All Customer Credit Memos</label>
    </listViews>
    <nameField>
        <displayFormat>CRN-{000000}</displayFormat>
        <label>Credit Memo ID</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Customer Credit Memos</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>EP_Credit_Memo_Number__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>EP_Credit_Memo_Issue_Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>EP_Credit_Memo_Due_Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>EP_Bill_To__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>EP_Credit_Memo_Total__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>EP_Credit_Memo_Remaining_Balance__c</customTabListAdditionalFields>
        <searchResultsAdditionalFields>EP_Credit_Memo_Number__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>EP_Credit_Memo_Issue_Date__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>EP_Credit_Memo_Due_Date__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>EP_Bill_To__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>EP_Credit_Memo_Total__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>EP_Credit_Memo_Remaining_Balance__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
