/* ================================================
 * @Class Name : EP_CRM_TaskTrigger 
 * @author : Kamendra Singh
 * @Purpose: This trigger is used to update Last Activity date on opportunity.
 * @created date: 08/07/2016
 ================================================*/
trigger EP_CRM_TaskTrigger on Task (before insert,before update,before delete) {
    try{
        if((trigger.isInsert ||trigger.isUpdate) && trigger.isbefore){
            EP_CRM_TaskTriggerHandler.beforeInsertUpdateTask(trigger.new);
        }
    
        if(trigger.isDelete && trigger.isBefore){
            EP_CRM_TaskTriggerHandler.beforeDeleteTask(trigger.old);
            //Defect #45313 - Start
            EP_CRM_TaskTriggerHandler.modifyCreditLimitTaskPermissionCheck(trigger.old);
            //Defect #45313 - End
        }
        //Defect #45313 - Start
        if(trigger.isUpdate && trigger.isbefore){
            EP_CRM_TaskTriggerHandler.modifyCreditLimitTaskPermissionCheck(trigger.new);
        }
        //Defect #45313 - End
    }
    catch(exception ex){
        ex.getmessage();
    }
}