/*
*  @Author <Accenture>
*  @Name <EP_Case_Trigger>
*  @CreateDate <07/05/2018>
*  @Description <Trigger for Case Object>
*  @Version <1.0>
*/
trigger EP_Case_Trigger on Case (before Update) {
    if(Trigger_Settings__c.getInstance('EP_Case_Trigger')!= null && Trigger_Settings__c.getInstance('EP_Case_Trigger').IsActive__c){
         if(trigger.isBefore){
             if(trigger.isUpdate){
                EP_CaseDomainObject CaseDomainObject = new EP_CaseDomainObject(trigger.new,trigger.oldmap);
                CaseDomainObject.doActionBeforeUpdate();
             }
         }
    }
}