/* 
   @Author <Accenture>
   @name <EP_NAVOrderUpdateHelper>
   @CreateDate <03/06/2017>
   @Description <This is a class to Transform JSON string data into SOBject(order) for Order Stauts Update Rest Service Request from NAV> 
   @Version <1.0>
*/
public with sharing class EP_NAVOrderUpdateHelper {
    @TestVisible private set<string> orderNumberSet = new set<string>();
    @TestVisible private map<string, csord__Order__c> orderNumberOrderMap = new  map<string, csord__Order__c>();
    @TestVisible private map<String, String> orderStatusMap =  new map<String, String>();
    @TestVisible private map<id, EP_OrderService> orderServiceMap = new map<id, EP_OrderService>();
    /**
    * @Author       Accenture
    * @Name         createDataMaps
    * @Date         03/24/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private void createDataMaps(list<EP_NAVOrderUpdateStub.orderWrapper> orderWrapperList) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderUpdateHelper','createDataMaps');
        createDataSets(orderWrapperList);
        system.debug('**orderNumberSet' + orderNumberSet);
        //orderNumberOrderMap = EP_WINDMSOrderUpdateHelper.getOrdersByOrderNumber(orderNumberSet);
        EP_OrderMapper orderMapper = new EP_OrderMapper();
        orderNumberOrderMap = orderMapper.getCsOrderMapByOrderNumber(orderNumberSet);
        orderStatusMap = EP_CustomSettingsUtil.getOrderStatusFromCustomSetting();
    }
    /**
    * @Author       Accenture
    * @Name         createDataSets
    * @Date         03/24/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private void createDataSets(list<EP_NAVOrderUpdateStub.orderWrapper> orderWrapperList) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderUpdateHelper','createDataSets');
        for(EP_NAVOrderUpdateStub.orderWrapper orderWrapper : orderWrapperList){
            if(string.isNotBlank(orderWrapper.identifier.orderId)) {
                orderNumberSet.add(orderWrapper.identifier.orderId);
            }
        }
    }
	
    /**
    * @Author       Accenture
    * @Name         setOrderObject
    * @Date         03/24/2017
    * @Description  
    * @Param        
    * @return        
    */   
    @TestVisible
    private void setOrderObject(EP_NAVOrderUpdateStub.orderWrapper orderWrapper) {
		EP_GeneralUtility.Log('Public','EP_NAVOrderUpdateHelper','setOrderObject');
        orderWrapper.sfOrder = orderNumberOrderMap.get(orderWrapper.identifier.orderId);
        orderWrapper.sfOrder.EP_SeqId__c = orderWrapper.seqId;
    }
	
    /**
     * @name <setOrderAttributes>
     * @description <This method is used to generate the list of orders for update .>
     * @return list < Order > - This List holds orders which needs to update their status.
    */
    public void setOrderAttributes(list<EP_NAVOrderUpdateStub.orderWrapper> orderWrapperList) {
        EP_GeneralUtility.Log('Public','EP_NAVOrderUpdateHelper','setOrderAttributes');
        createDataMaps(orderWrapperList);
        for(EP_NAVOrderUpdateStub.orderWrapper orderWrapper : orderWrapperList){
            try {
                if(isOrderNumberValid(orderWrapper)){
                    
                    setOrderObject(orderWrapper);
                    setOrderStatus(orderWrapper.sfOrder, orderStatusMap.get(orderWrapper.status));
                }
            } catch(exception exp) {
                orderWrapper.errorCode = exp.getTypeName();
                orderWrapper.errorDescription = exp.getMessage();
                EP_LoggingService.logServiceException(exp, UserInfo.getOrganizationId(), EP_Common_constant.EPUMA, 'setOrderAttributes', 'EP_NAVOrderUpdateHelper',  EP_Common_constant.ERROR, UserInfo.getUserId(), EP_Common_constant.TARGET_SF, EP_Common_Constant.BLANK, EP_Common_Constant.BLANK);
            }
        }
    }
	
    /**
    * @Author       Accenture
    * @Name         isOrderNumberValid
    * @Date         03/24/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private boolean isOrderNumberValid(EP_NAVOrderUpdateStub.orderWrapper orderWrapper) {
        EP_GeneralUtility.Log('Public','EP_NAVOrderUpdateHelper','isOrderNumberValid');
        boolean isOrderNumberValid = true;
        system.debug('***orderNumberOrderMap' + orderNumberOrderMap);
        if(!orderNumberOrderMap.containsKey(orderWrapper.identifier.orderId)) {
            isOrderNumberValid = false;
            orderWrapper.errorCode = EP_Common_Constant.NO_RECORDS_FOUND;
            orderWrapper.errorDescription = EP_Common_Constant.NO_RECORDS_FOUND;
        }
        return isOrderNumberValid;
    }
    
    /**
    * @Author       Accenture
    * @Name         setOrderStatus
    * @Date         03/24/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private void setOrderStatus(csord__Order__c sfOrder, String newStatus) {
        EP_GeneralUtility.Log('Private','EP_NAVOrderUpdateHelper','setOrderStatus');
        EP_OrderDomainObject orderDomain = new EP_OrderDomainObject(sfOrder);
        EP_OrderEvent orderEvent = new EP_OrderEvent(orderDomain.getStatus()+ EP_OrderConstant.OrderEvent_ToString + newStatus);
        system.debug('orderEvent**'+ orderEvent);
        system.debug('STATUS**'+ orderDomain.getStatus());
        system.debug('newStatus**'+ newStatus);
        /*PS:24/02/2018 Comment Start
        EP_OrderService orderService = new EP_OrderService(orderDomain);
        if(orderService.setOrderStatus(orderEvent)){
            orderServiceMap.put(sfOrder.id,orderService);
        }Comment End */
		system.debug('sf order status**'+ sfOrder.csord__Status2__c );
        if (sfOrder.csord__Status2__c != EP_Common_constant.ORDER_ITEM_CANCELLED_STATUS || sfOrder.csord__Status2__c != EP_Common_Constant.INVOICED_STATUS  || sfOrder.csord__Status2__c != (EP_Common_Constant.INVOICED_STATUS).toUpperCase()){
            
            String oldStatus = sfOrder.csord__Status2__c == null ? '' : sfOrder.csord__Status2__c;
            newStatus = newStatus == null ? '' : newStatus;

            if (!(oldStatus.equalsIgnoreCase(EP_Common_Constant.WINDMS_PLANNING) && newStatus.equalsIgnoreCase(EP_Common_Constant.ORDER_ACCEPTED_STATUS))) {
                sfOrder.csord__Status2__c = newStatus;
            }
        }  

        upsert sfOrder;
        system.debug('orderServiceMap**'+ orderServiceMap);
    }
    
    /**
    * @Author       Accenture
    * @Name         doPostActions
    * @Date         03/24/2017
    * @Description  This method returns the map of orderNumber with Order Object for given set of Order Number
    * @Param        set<string>
    * @return       map<string, Order>
    */
    public void doPostActions(set<id> orderIdSet){
		EP_GeneralUtility.Log('public','EP_NAVOrderUpdateHelper','doPostActions');
        for(Id orderId : orderIdSet) {
            if(orderServiceMap.containsKey(orderId)) {
            	orderServiceMap.get(orderId).doPostStatusChangeActions();                
            }
        }
    }
}