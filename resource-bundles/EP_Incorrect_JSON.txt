{
    "MSG": {
        "HeaderCommon": {
            "MsgID": "161212144847114-{B5896739-31B1-441E-96D2-0CDE6CDB10A8}",
            "InterfaceType": "NAV",
            "SourceGroupCompany": "",
            "DestinationGroupCompany": "",
            "SourceCompany": "AAF_PRC",
            "DestinationCompany": "",
            "CorrelationID": "",
            "DestinationAddress": "",
            "SourceResponseAddress": "https://integrationservice-uat01.eu.coolfurnace.net/api/ipaas/ack",
            "SourceUpdateStatusAddress": "https://integrationservice-uat01.eu.coolfurnace.net/api/ipaas/ack",
            "DestinationUpdateStatusAddress": "",
            "MiddlewareUrlForPush": "http://dtraflon2k105.global.trafigura.com/EPUMA_NavToIPaaS_PushAsync/EPUMA_NavToIPaaS_PushAsync.svc",
            "EmailNotification": "",
            "ErrorCode": "",
            "ErrorDescription": "",
            "ProcessingErrorDescription": "",
            "ContinueOnError": "true",
            "ComprehensiveLogging": "true",
            "TransportStatus": "Delivered",
            "ProcessStatus": "",
            "UpdateSourceOnReceive": "true",
            "UpdateSourceOnDelivery": "true",
            "UpdateSourceAfterProcessing": "true",
            "UpdateDestinationOnDelivery": "true",
            "CallDestinationForProcessing": "true",
            "ObjectType": "Table",
            "ObjectName": "Price Response Header Staging",
            "CommunicationType": "Async"
        },
        "Payload": {
            "any": {
                "pricingResponse": {
                    "": {
                        "seqId": "<seqId>",
                        "priceRequestSource": "",
                        "companyCode": "AAF",
                        "priceType": "Fuel and Freight Price",
                        "deliveryType": "Delivery",
                        "priceDate": "2016-12-15",
                        "applyOrderQuantity": "No",
                        "totalOrderQuantity": "1,122.00",
                        "customerId": "<custId>",
                        "shipToId": "<ShipToId>",
                        "supplyLocationId": "<supplyLocId>",
                        "transporterId": "<transporterId>",
                        "onRun": "No",
                        "priceLineItems": {
                            "orderId": "<OrderId>",
                            "lineItem": [{
                                "lineItemInfo": {
                                    "lineItemId": "<lineId>",
                                    "itemId": "<itemId>",
                                    "quantity": "1,122.00",
                                    "unitPrice": "2.00000"
                                },
                                "invoiceDetails": {
                                    "invoiceComponents": {
                                        "invoiceComponent": [{
                                            "type": "Discount",
                                            "name": "DISCOUNT",
                                            "amount": "-1.00000",
                                            "taxPercentage": "0.00",
                                            "taxAmount": "0.00000",
                                            "totalAmount": "-1.00000"
                                        }, {
                                            "type": "Freight",
                                            "name": "",
                                            "amount": "11.00000",
                                            "taxPercentage": "10.00",
                                            "taxAmount": "1.10000",
                                            "totalAmount": "12.10000"
                                        }, {
                                            "type": "",
                                            "name": "TAX",
                                            "amount": "1.00000",
                                            "taxPercentage": "1.00",
                                            "taxAmount": "1.00000",
                                            "totalAmount": "1.00000"
                                        }, {
                                            "type": "",
                                            "name": "FUEL",
                                            "amount": "2.00000",
                                            "taxPercentage": "0.00",
                                            "taxAmount": "0.00000",
                                            "totalAmount": "2.00000"
                                        }]
                                    }
                                },
                                "accountingDetails": {
                                    "acctComponents": {
                                        "acctComponent": [{
                                            "componentCode": "TAX",
                                            "glAccountNr": "",
                                            "baseAmount": "1.00000",
                                            "taxRate": "1.00",
                                            "taxAmount": "1.00000",
                                            "totalAmount": "1.00000",
                                            "isVAT": "No"
                                        }, {
                                            "componentCode": "",
                                            "glAccountNr": "7080300",
                                            "baseAmount": "11.00000",
                                            "taxRate": "10.00",
                                            "taxAmount": "1.10000",
                                            "totalAmount": "12.10000",
                                            "isVAT": "No"
                                        }, {
                                            "componentCode": "FUEL",
                                            "glAccountNr": "",
                                            "baseAmount": "2.00000",
                                            "taxRate": "0.00",
                                            "taxAmount": "0.00000",
                                            "totalAmount": "2.00000",
                                            "isVAT": "No"
                                        }, {
                                            "componentCode": "DISCOUNT",
                                            "glAccountNr": "",
                                            "baseAmount": "-1.00000",
                                            "taxRate": "0.00",
                                            "taxAmount": "0.00000",
                                            "totalAmount": "-1.00000",
                                            "isVAT": "No"
                                        }]
                                    }
                                }
                            }]
                        },
                        "Error": {
                            "ErrorCode": "",
                            "ErrorDescription": ""
                        }
                    }
                }
            }
        },
        "StatusPayload": "StatusPayload"
    }
}